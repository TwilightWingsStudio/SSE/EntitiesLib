/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

/*
 * Entity that can be controlled by player or AI.
 */
409
%{
  #include "StdH.h"
%}

class export CSpectatorPawnEntity : CPawnEntity {
name      "SpectatorPawn";
thumbnail "";
features "CanBePredictable";
properties:

{
}

resources:
functions:

procedures:
  // must have at least one procedure per class
  Main()
  {
    InitAsEditorModel();
    SetPhysicsFlags(EPF_MOVABLE);
    
    wait()
    {
      on (EBegin) : {
        resume;
      }
    }
  };
};
