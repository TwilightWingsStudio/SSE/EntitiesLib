/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

402
%{
  #include "StdH.h"

  #include "Game/SEColors.h"

  #include <Engine/Build.h>

  #include "Entities/Players/PlayerPawn.h"
  #include "Entities/Weapons/Bullet.h"
  #include "Entities/Tools/Switch.h"
  #include "Entities/Players/PlayerInventory.h"
  #include "Entities/Players/PlayerView.h"
  #include "Entities/Players/PlayerAnimator.h"
  #include "Entities/Brushes/MovingBrush.h"
  #include "Entities/Tools/MessageHolder.h"
  #include "Entities/Characters/EnemyBase.h"

  // Models
  #include "Models/Weapons/Knife/Knife.h"
  #include "Models/Weapons/Knife/KnifeItem.h"
  #include "Models/Weapons/Colt/Colt.h"
  #include "Models/Weapons/Colt/ColtMain.h"
  #include "Models/Weapons/SingleShotgun/SingleShotgun.h"
  #include "Models/Weapons/SingleShotgun/Barrels.h"
  #include "Models/Weapons/DoubleShotgun/DoubleShotgun.h"
  #include "Models/Weapons/DoubleShotgun/Dshotgunbarrels.h"
  #include "Models/Weapons/DoubleShotgun/HandWithAmmo.h"
  #include "Models/Weapons/TommyGun/TommyGun.h"
  #include "Models/Weapons/TommyGun/Body.h"
  #include "Models/Weapons/MiniGun/MiniGun.h"
  #include "Models/Weapons/MiniGun/Body.h"
  #include "Models/Weapons/GrenadeLauncher/GrenadeLauncher.h"
  #include "Models/Weapons/RocketLauncher/RocketLauncher.h"
  #include "Models/Weapons/Laser/Laser.h"
  #include "Models/Weapons/Laser/Barrel.h"
  #include "Models/Weapons/Cannon/Cannon.h"
  #include "Models/Weapons/Cannon/Body.h"
  // SSA
  #include "Models/Weapons/Beamgun/Beamgun.h"
  #include "Models/Weapons/Beamgun/Body.h"
  #include "Models/Weapons/Beamgun/Rotator.h"
  #include "Models/Weapons/Beamgun/Effect01.h"
  #include "Models/Weapons/Beamgun/EffectFlare01.h"
  // Mission Pack weapons
  #include "ModelsMP/Weapons/Sniper/Sniper.h"
  #include "ModelsMP/Weapons/Sniper/Body.h"
  #include "ModelsMP/Weapons/Flamer/Flamer.h"
  #include "ModelsMP/Weapons/Flamer/Body.h"
  #include "ModelsMP/Weapons/Flamer/FuelReservoir.h"
  #include "ModelsMP/Weapons/Flamer/Flame.h"
  #include "ModelsMP/Weapons/Chainsaw/Chainsaw.h"
  #include "ModelsMP/Weapons/Chainsaw/ChainSawForPlayer.h"
  #include "ModelsMP/Weapons/Chainsaw/Body.h"
  #include "ModelsMP/Weapons/Chainsaw/Blade.h"
  #include "ModelsMP/Weapons/Chainsaw/Teeth.h"

  // Mission Pack player body instead of the old one
  #include "ModelsMP/Player/SeriousSam/Body.h"
  #include "ModelsMP/Player/SeriousSam/Player.h"

  extern INDEX hud_bShowWeapon;

  extern const INDEX aiWeaponsRemap[E_WEAPON_MAX] = { 
    E_WEAPON_NONE,
    E_WEAPON_KNIFE,
    E_WEAPON_CHAINSAW,
    E_WEAPON_REVOLVER,
    E_WEAPON_SHOTGUN,
    E_WEAPON_SUPERSHOTGUN,
    E_WEAPON_MACHINEGUN,
    E_WEAPON_CHAINGUN,
    E_WEAPON_ROCKETLAUNCHER,
    E_WEAPON_GRENADELAUNCHER,
    E_WEAPON_FLAMER, 
    E_WEAPON_SNIPER,
    E_WEAPON_PLASMAGUN,
    E_WEAPON_BEAMGUN,
    E_WEAPON_CANNON,
    E_WEAPON_PLASMATHROWER};
%}

uses "Entities/Players/PlayerPawn";
uses "Entities/Players/PlayerInventory";
uses "Entities/Players/PlayerWeaponsEffects";
uses "Entities/Weapons/Projectile";
uses "Entities/Weapons/Bullet";
uses "Entities/Effects/BasicEffects";
uses "Entities/Items/WeaponItem";
uses "Entities/Items/AmmoItem";
uses "Entities/Items/AmmoPack";
uses "Entities/Models/ModelHolder2";
uses "Entities/Weapons/BallProjectile";
uses "Entities/Weapons/EnergyBeam";

// input parameter for weapons
event EWeaponsInit
{
  CEntityPointer penOwner,        // who owns it
};

// select weapon
event ESelectWeapon
{
  INDEX iWeapon,          // weapon to select
};

// boring weapon animations
event EBoringWeapon {};

// fire weapon
event EFireWeapon {};

// release weapon
event EReleaseWeapon {};

// reload weapon
event EReloadWeapon {};

// weapon changed - used to notify other entities
event EWeaponChanged {};

%{
  // AVAILABLE WEAPON MASK
  #define WEAPONS_ALLAVAILABLEMASK 0x3FFF
  #define MAX_WEAPONS 30


  // chaingun specific
  #define MINIGUN_STATIC      0
  #define MINIGUN_FIRE        1
  #define MINIGUN_SPINUP      2
  #define MINIGUN_SPINDOWN    3

  #define MINIGUN_SPINUPTIME      0.5f
  #define MINIGUN_SPINDNTIME      3.0f
  #define MINIGUN_SPINUPSOUND     0.5f
  #define MINIGUN_SPINDNSOUND     1.5f
  #define MINIGUN_FULLSPEED       500.0f
  #define MINIGUN_SPINUPACC       (MINIGUN_FULLSPEED/MINIGUN_SPINUPTIME)
  #define MINIGUN_SPINDNACC       (MINIGUN_FULLSPEED/MINIGUN_SPINDNTIME)
  #define CHAINGUN_TICKTIME        (_pTimer->TickQuantum)
  
  // [SSE] Weapon Animation Constants
  #define KNIFE_ANIM_ATTACK01_LENGTH 0.8f
  #define KNIFE_ANIM_ATTACK02_LENGTH 0.85f
  #define KNIFE_ANIM_PULL_LENGTH 0.4f // 0.375
  #define KNIFE_ANIM_PULLOUT_LENGTH 0.25F // 0.21
  
  #define CHAINSAW_ANIM_WAIT2FIRE_LENGTH 0.2F
  #define CHAINSAW_ANIM_FIRE2WAIT_LENGTH 0.2F
  #define CHAINSAW_ANIM_ACTIVATE_LENGTH 0.5f
  #define CHAINSAW_ANIM_DEACTIVATE_LENGTH 0.4F
  
  #define COLT_ANIM_RELOAD_LENGTH 1.1F
  #define COLT_ANIM_FIRE1_LENGTH 0.5F
  #define COLT_ANIM_FIRE2_LENGTH 0.5F
  #define COLT_ANIM_FIRE3_LENGTH 0.6F
  #define COLT_ANIM_ACTIVATE_LENGTH 0.4f
  #define COLT_ANIM_DEACTIVATE_LENGTH 0.25F
  
  #define SINGLESHOTGUN_ANIM_FIRE_LENGTH 1.1F
  #define SINGLESHOTGUN_ANIM_FIREFAST_LENGTH 0.825F
  #define SINGLESHOTGUN_ANIM_ACTIVATE_LENGTH 0.4f
  #define SINGLESHOTGUN_ANIM_DEACTIVATE_LENGTH 0.25F

  #define DOUBLESHOTGUN_ANIM_FIRE_LENGTH 1.75F
  #define DOUBLESHOTGUN_ANIM_FIREFAST_LENGTH 1.05F
  #define DOUBLESHOTGUN_ANIM_ACTIVATE_LENGTH 0.4f
  #define DOUBLESHOTGUN_ANIM_DEACTIVATE_LENGTH 0.25F
  
  #define TOMMYGUN_ANIM_ACTIVATE_LENGTH 0.4f
  #define TOMMYGUN_ANIM_DEACTIVATE_LENGTH 0.4F
  
  #define MINIGUN_ANIM_ACTIVATE_LENGTH 0.55F // 0.51
  #define MINIGUN_ANIM_DEACTIVATE_LENGTH 0.45F // 0.42f

  #define ROCKETLAUNCHER_ANIM_FIRE_LENGTH 0.7F // 0.665
  #define ROCKETLAUNCHER_ANIM_ACTIVATE_LENGTH 0.5F
  #define ROCKETLAUNCHER_ANIM_DEACTIVATE_LENGTH 0.3F
  
  #define GRENADELAUNCHER_ANIM_ACTIVATE_LENGTH 0.5F
  #define GRENADELAUNCHER_ANIM_DEACTIVATE_LENGTH 0.5F

  #define FLAMER_ANIM_FIRESTART_LENGTH 0.25F
  #define FLAMER_ANIM_FIREEND_LENGTH 0.25F
  #define FLAMER_ANIM_ACTIVATE_LENGTH 0.45F
  #define FLAMER_ANIM_DEACTIVATE_LENGTH 0.25F

  #define LASER_ANIM_ACTIVATE_LENGTH 0.5F
  #define LASER_ANIM_DEACTIVATE_LENGTH 0.3F

  #define BEAMGUN_ANIM_ACTIVATE_LENGTH 0.6F
  #define BEAMGUN_ANIM_DEACTIVATE_LENGTH 0.3F

  #define SNIPER_ANIM_ACTIVATE_LENGTH 0.4F
  #define SNIPER_ANIM_DEACTIVATE_LENGTH 0.4F

  #define CANNON_ANIM_FIRE_LENGTH 1.2F
  #define CANNON_ANIM_ACTIVATE_LENGTH 0.55F
  #define CANNON_ANIM_DEACTIVATE_LENGTH 0.55F // 0.52

  // colt specific
  #define COLT_MAG_SIZE 6

  // chainsaw specific
  #define CHAINSAW_UPDATETIME     0.05f

  // fire flare specific
  #define FLARE_REMOVE 1
  #define FLARE_ADD 2

  // animation light specific
  #define LIGHT_ANIM_MINIGUN 2
  #define LIGHT_ANIM_TOMMYGUN 3
  #define LIGHT_ANIM_COLT_SHOTGUN 4
  #define LIGHT_ANIM_NONE 5

  // mana for ammo adjustment (multiplier)
  #define MANA_AMMO (0.1f)

  // position of weapon model -- weapon 0 is never used
  static FLOAT wpn_fX[MAX_WEAPONS+1];
  static FLOAT wpn_fY[MAX_WEAPONS+1];
  static FLOAT wpn_fZ[MAX_WEAPONS+1];
  static FLOAT wpn_fFOV[MAX_WEAPONS+1];
  static FLOAT wpn_fFX[MAX_WEAPONS+1];  // firing source
  static FLOAT wpn_fFY[MAX_WEAPONS+1];
  //static FLOAT wpn_fFZ[MAX_WEAPONS+1];
  static INDEX wpn_iCurrent;
  extern FLOAT hud_tmWeaponsOnScreen;
  extern FLOAT wpn_fRecoilSpeed[17];
  extern FLOAT wpn_fRecoilLimit[17];
  extern FLOAT wpn_fRecoilDampUp[17];
  extern FLOAT wpn_fRecoilDampDn[17];
  extern FLOAT wpn_fRecoilOffset[17];
  extern FLOAT wpn_fRecoilFactorP[17];
  extern FLOAT wpn_fRecoilFactorZ[17];
  
  extern COLOR HUD_GetCrosshairColor(FLOAT fHealthFactor);

  // bullet positions
  static FLOAT afSingleShotgunPellets[] =
  {     -0.3f,+0.1f,    +0.0f,+0.1f,   +0.3f,+0.1f,
    -0.4f,-0.1f,  -0.1f,-0.1f,  +0.1f,-0.1f,  +0.4f,-0.1f
  };
  static FLOAT afSuperShotgunPellets[] =
  {
        -0.3f,+0.15f, +0.0f,+0.15f, +0.3f,+0.15f,
    -0.4f,+0.05f, -0.1f,+0.05f, +0.1f,+0.05f, +0.4f,+0.05f,
        -0.3f,-0.05f, +0.0f,-0.05f, +0.3f,-0.05f,
    -0.4f,-0.15f, -0.1f,-0.15f, +0.1f,-0.15f, +0.4f,-0.15f
  };

  // sniper discrete zoom values - 4 (1x,2x,4x,6x)
  // synchronize this with sniper properties (properties 233-237)
  static INDEX iSniperDiscreteZoomLevels = 4;
  static FLOAT afSniperZoom[] =
  {
        90.0f,1.0f, 53.1f, 2.0f, 28.0f,4.0f, 14.2f,6.0f,
         //7.2f,8.0f, 3.56f,10.0f ,1.8f,12.0f
  };

  // crosshair console variables
  static INDEX hud_bCrosshairFixed    = FALSE;
  static INDEX hud_bCrosshairColoring = TRUE;
  static FLOAT hud_fCrosshairScale    = 1.0f;
  static FLOAT hud_fCrosshairOpacity  = 1.0f;
  static FLOAT hud_fCrosshairRatio    = 0.5f;  // max distance size ratio
  // misc HUD vars
  static INDEX hud_bShowPlayerName = TRUE;
  static INDEX hud_bShowCoords     = FALSE;
  static FLOAT plr_tmSnoopingDelay = 1.0f; // seconds
  extern FLOAT plr_tmSnoopingTime  = 1.0f; // seconds

  // some static vars
  static INDEX _iLastCrosshairType=-1;
  static CTextureObject _toCrosshair;

  // must do this to keep dependency catcher happy
  CTFileName fn1 = CTFILENAME("Textures\\Interface\\Crosshairs\\Crosshair1.tex");
  CTFileName fn2 = CTFILENAME("Textures\\Interface\\Crosshairs\\Crosshair2.tex");
  CTFileName fn3 = CTFILENAME("Textures\\Interface\\Crosshairs\\Crosshair3.tex");
  CTFileName fn4 = CTFILENAME("Textures\\Interface\\Crosshairs\\Crosshair4.tex");
  CTFileName fn5 = CTFILENAME("Textures\\Interface\\Crosshairs\\Crosshair5.tex");
  CTFileName fn6 = CTFILENAME("Textures\\Interface\\Crosshairs\\Crosshair6.tex");
  CTFileName fn7 = CTFILENAME("Textures\\Interface\\Crosshairs\\Crosshair7.tex");

  void CPlayerWeaponEntity_Precache(ULONG ulAvailable)
  {
    CLibEntityClass *pdec = &CPlayerWeaponEntity_DLLClass;

    // precache general stuff always
    pdec->PrecacheClass(CLASS_BULLET);
    pdec->PrecacheSound(SOUND_SILENCE);

    // precache other weapons if available
    if (ulAvailable & (1 << (E_WEAPON_KNIFE - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_KNIFE_CFG);
      pdec->PrecacheSound(SOUND_KNIFE_BACK            );
      pdec->PrecacheSound(SOUND_KNIFE_HIGH            );
      pdec->PrecacheSound(SOUND_KNIFE_LONG            );
      pdec->PrecacheSound(SOUND_KNIFE_LOW             );
    }

    if (ulAvailable & (1 << (E_WEAPON_REVOLVER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_COLT_CFG);
      pdec->PrecacheSound(SOUND_COLT_FIRE           );
      pdec->PrecacheSound(SOUND_COLT_RELOAD           );
    }

    if (ulAvailable & (1 << (E_WEAPON_SHOTGUN - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_SINGLESHOTGUN_CFG);
      pdec->PrecacheSound(SOUND_SINGLESHOTGUN_FIRE);
    }

    if (ulAvailable & (1 << (E_WEAPON_SUPERSHOTGUN - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_DOUBLESHOTGUN_CFG);
      pdec->PrecacheModelConfig(MODEL_DS_HANDWITHAMMO_CFG);
      pdec->PrecacheSound(SOUND_DOUBLESHOTGUN_FIRE   );
      pdec->PrecacheSound(SOUND_DOUBLESHOTGUN_RELOAD );
    }

    if (ulAvailable & (1 << (E_WEAPON_MACHINEGUN - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_TOMMYGUN_CFG);
      pdec->PrecacheSound(SOUND_TOMMYGUN_FIRE         );
    }

    if (ulAvailable & (1 << (E_WEAPON_SNIPER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_SNIPER_CFG);
      pdec->PrecacheSound(SOUND_SNIPER_FIRE     );
    }

    if (ulAvailable & (1 << (E_WEAPON_CHAINGUN - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_MINIGUN_CFG);
      pdec->PrecacheSound(SOUND_MINIGUN_FIRE     );
      pdec->PrecacheSound(SOUND_MINIGUN_ROTATE   );
      pdec->PrecacheSound(SOUND_MINIGUN_SPINUP   );
      pdec->PrecacheSound(SOUND_MINIGUN_SPINDOWN );
      pdec->PrecacheSound(SOUND_MINIGUN_CLICK    );
    }

    if (ulAvailable & (1 << (E_WEAPON_ROCKETLAUNCHER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_ROCKETLAUNCHER_CFG);
      pdec->PrecacheSound(SOUND_ROCKETLAUNCHER_FIRE);
      pdec->PrecacheClass(CLASS_PROJECTILE, PRT_ROCKET);
    }

    if (ulAvailable & (1 << (E_WEAPON_GRENADELAUNCHER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_GRENADELAUNCHER_CFG);
      pdec->PrecacheSound(SOUND_GRENADELAUNCHER_FIRE );
      pdec->PrecacheClass(CLASS_PROJECTILE, PRT_GRENADE);
    }

    if (ulAvailable & (1 << (E_WEAPON_CHAINSAW - 1))) {
      pdec->PrecacheModelConfig(MODEL_CHAINSAW_CFG);
      pdec->PrecacheSound(SOUND_CS_FIRE      );
      pdec->PrecacheSound(SOUND_CS_BEGINFIRE );
      pdec->PrecacheSound(SOUND_CS_ENDFIRE   );
      pdec->PrecacheSound(SOUND_CS_BRINGUP   );
      pdec->PrecacheSound(SOUND_CS_BRINGDOWN );
      pdec->PrecacheSound(SOUND_CS_IDLE      );
    }

    if (ulAvailable&(1 << (E_WEAPON_FLAMER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_FLAMER_CFG);
      pdec->PrecacheSound(SOUND_FL_FIRE     );
      pdec->PrecacheSound(SOUND_FL_START    );
      pdec->PrecacheSound(SOUND_FL_STOP     );
      pdec->PrecacheClass(CLASS_PROJECTILE, PRT_FLAME);
    }

    if (ulAvailable&(1 << (E_WEAPON_PLASMAGUN - 1))) {
      pdec->PrecacheModelConfig(MODEL_LASER_CFG);
      pdec->PrecacheSound(SOUND_LASER_FIRE);
      pdec->PrecacheClass(CLASS_PROJECTILE, PRT_LASER_RAY);
    }
    
    // [SSE] Beamgun
    if (ulAvailable&(1 << (E_WEAPON_BEAMGUN - 1))) {
      pdec->PrecacheModelConfig(MODEL_BEAMGUN_CFG);
      pdec->PrecacheSound(SOUND_BG_FIRE         );
      pdec->PrecacheClass(CLASS_ENERGYBEAM);
    }

    if (ulAvailable&(1 << (E_WEAPON_CANNON - 1))) {
      pdec->PrecacheModelConfig(MODEL_CANNON_CFG);
      pdec->PrecacheSound(SOUND_CANNON    );
      pdec->PrecacheSound(SOUND_CANNON_PREPARE);
      pdec->PrecacheClass(CLASS_CANNONBALL);
    }

    // [SEE] Plasmathrower
    if (ulAvailable&(1 << (E_WEAPON_PLASMATHROWER - 1))) {
      pdec->PrecacheModelConfig(MODEL_PLASMATHROWER_CFG);
      pdec->PrecacheSound(SOUND_PLASMATHROWER_FIRE);
      pdec->PrecacheClass(CLASS_PROJECTILE, PRT_LASER_RAY);
    }

    // [SEE] Minelayer
    if (ulAvailable & (1 << (E_WEAPON_MINELAYER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_MINELAYER_CFG);
      pdec->PrecacheSound(SOUND_MINELAYER_FIRE );
      pdec->PrecacheClass(CLASS_PROJECTILE, PRT_GRENADE);
    }

    // precache animator too
    extern void CPlayerAnimatorEntity_Precache(ULONG ulAvailable);
    CPlayerAnimatorEntity_Precache(ulAvailable);
  }

  void CPlayerWeaponEntity_Init(void)
  {
    // declare weapon position controls
    _pShell->DeclareSymbol("user INDEX wpn_iCurrent;", &wpn_iCurrent);
    
    for (INDEX i = E_WEAPON_NONE; i < E_WEAPON_MAX; i++)
    {
      wpn_fX[i] = wpn_fY[i] = wpn_fZ[i] = 0.0F;
      wpn_fFOV[i] = 45.0F;
      wpn_fFX[i] = wpn_fFY[i] = 0.0F;
    }

    #include <Entities/Common/WeaponPositions.h>

    // declare crosshair and its coordinates
    _pShell->DeclareSymbol("persistent user INDEX hud_bCrosshairFixed;",    &hud_bCrosshairFixed);
    _pShell->DeclareSymbol("persistent user INDEX hud_bCrosshairColoring;", &hud_bCrosshairColoring);
    _pShell->DeclareSymbol("persistent user FLOAT hud_fCrosshairScale;",    &hud_fCrosshairScale);
    _pShell->DeclareSymbol("persistent user FLOAT hud_fCrosshairRatio;",    &hud_fCrosshairRatio);
    _pShell->DeclareSymbol("persistent user FLOAT hud_fCrosshairOpacity;",  &hud_fCrosshairOpacity);

    _pShell->DeclareSymbol("persistent user INDEX hud_bShowPlayerName;", &hud_bShowPlayerName);
    _pShell->DeclareSymbol("persistent user INDEX hud_bShowCoords;",     &hud_bShowCoords);

    _pShell->DeclareSymbol("persistent user FLOAT plr_tmSnoopingTime;",  &plr_tmSnoopingTime);
    _pShell->DeclareSymbol("persistent user FLOAT plr_tmSnoopingDelay;", &plr_tmSnoopingDelay);

    // precache base weapons
    CPlayerWeaponEntity_Precache(0x03);
  }

  // weapons positions for raycasting and firing
  /*
  static FLOAT afKnifePos[4] = { -0.01f, 0.25f, 0.0f};
  static FLOAT afColtPos[4] = { -0.01f, 0.1f, 0.0f};
  static FLOAT afDoubleColtPos[4] = { -0.01f, 0.1f, 0.0f};
  static FLOAT afSingleShotgunPos[4] = { 0.0f, 0.1f, 0.0f};
  static FLOAT afDoubleShotgunPos[4] = {  0.0f, 0.1f, 0.0f};
  static FLOAT afTommygunPos[4] = { 0.0f, 0.1f, 0.0f};
  static FLOAT afChaingunPos[4] = { 0.0f, -0.075f, 0.0f};
  static FLOAT afRocketLauncherPos[4] = { -0.175f, 0.19f, -0.23f};
  static FLOAT afGrenadeLauncherPos[4] = { 0.0f, 0.16f, -1.42f};
  static FLOAT afPipebombPos[4] = { 0.01f, 0.04f, -0.44f};
  static FLOAT afFlamerPos[4] = { 0.0f, 0.18f, -0.62f};
  static FLOAT afLaserPos[4] = {  0.0f, -0.095f, -0.65f};
  static FLOAT afGhostBusterPos[4] = { 0.0f, 0.0f, -0.74f};
  static FLOAT afCannonPos[4] = { 0.0f, 0.0f, -0.74f};
  */

  // extra weapon positions for shells dropout
  static FLOAT afSingleShotgunShellPos[3] = { 0.2f, 0.0f, -0.31f};
  static FLOAT afDoubleShotgunShellPos[3] = { 0.0f, 0.0f, -0.5f};
  static FLOAT afTommygunShellPos[3] = { 0.2f, 0.0f, -0.31f};
  static FLOAT afChaingunShellPos[3] = { 0.2f, 0.0f, -0.31f};
  static FLOAT afChaingunShellPos3rdView[3] = { 0.2f, 0.2f, -0.31f};
  static FLOAT afSniperShellPos[3] = { 0.2f, 0.0f, -0.15f};

  static FLOAT afRightColtPipe[3] = { 0.07f, -0.05f, -0.26f};
  static FLOAT afSingleShotgunPipe[3] = { 0.2f, 0.0f, -1.25f};
  static FLOAT afSuperShotgunPipe[3] = { 0.2f, 0.0f, -1.25f};
  static FLOAT afTommygunPipe[3] = { -0.06f, 0.1f, -0.6f};
  static FLOAT afChaingunPipe[3] = { -0.06f, 0.0f, -0.6f};
  static FLOAT afChaingunPipe3rdView[3] = { 0.25f, 0.3f, -2.5f};

  //static FLOAT afLaserPos[4] = {  0.0f, -0.095f, -0.65f};
  //static FLOAT afLaser1Pos[4] = { -0.115f, -0.05f, -0.65f};
  //static FLOAT afLaser2Pos[4] = {  0.115f, -0.05f, -0.65f};
  //static FLOAT afLaser3Pos[4] = { -0.145f, -0.14f, -0.8f};
  //static FLOAT afLaser4Pos[4] = {  0.145f, -0.14f, -0.8f};

  #define TM_START m_aChaingun
  #define F_OFFSET_CHG m_aChaingunLast
  #define F_TEMP m_aChaingunSpeed
%}

class export CPlayerWeaponEntity : CRationalEntity {
name      "Player Weapons";
thumbnail "";
features "CanBePredictable";

properties:

  1 CEntityPointer m_penPlayer,       // player which owns it
  2 BOOL m_bFireWeapon = FALSE,       // weapon is firing
  3 BOOL m_bHasAmmo    = FALSE,       // weapon has ammo

 12 BOOL  m_bChangeWeapon = FALSE,      // change current weapon
 13 BOOL  m_bReloadWeapon = FALSE,      // reload weapon
 14 BOOL  m_bMirrorFire   = FALSE,      // fire with mirror model
 15 INDEX m_iAnim         = 0,          // temporary anim variable
 16 FLOAT m_fAnimWaitTime = 0.0f,       // animation wait time
 17 FLOAT m_tmRangeSoundSpawned = 0.0f, // for not spawning range sounds too often
 23 BOOL  m_bSniperZoom = FALSE,        // zoom sniper
 24 FLOAT m_fSniperFOV      = 90.0f,    // sniper FOV
 28 FLOAT m_fSniperFOVlast  = 90.0f,    // sniper FOV for lerping

 18 CTString m_strLastTarget   = "",      // string for last target
 19 FLOAT m_tmTargetingStarted = -99.0f,  // when targeting started
 20 FLOAT m_tmLastTarget       = -99.0f,  // when last target was seen
 21 FLOAT m_tmSnoopingStarted  = -99.0f,  // is player spying another player
 22 CEntityPointer m_penTargeting,        // who is the target

 25 CModelObject m_moWeapon,               // current weapon model
 26 CModelObject m_moWeaponSecond,         // current weapon second (additional) model
 27 FLOAT m_tmWeaponChangeRequired = 0.0f, // time when weapon change was required

 30 CEntityPointer m_penRayHit,         // entity hit by ray
 31 FLOAT m_fRayHitDistance = 100.0f,   // distance from hit point
 32 FLOAT m_fEnemyHealth    = 0.0f,     // normalized health of enemy in target (for coloring of crosshair)
 33 FLOAT3D m_vRayHit     = FLOAT3D(0, 0, 0), // coordinates where ray hit
 34 FLOAT3D m_vRayHitLast = FLOAT3D(0, 0, 0), // for lerping
 35 FLOAT3D m_vBulletSource = FLOAT3D(0, 0, 0), // bullet launch position remembered here
 36 FLOAT3D m_vBulletTarget = FLOAT3D(0, 0, 0), // bullet hit (if hit) position remembered here

// weapons specific

// colt
115 INDEX m_iColtBullets = COLT_MAG_SIZE,

// chaingun
120 FLOAT m_aChaingun = 0.0f,
121 FLOAT m_aChaingunLast = 0.0f,
122 FLOAT m_aChaingunSpeed = 0.0f,

// lerped bullets fire
130 FLOAT3D m_iLastBulletPosition = FLOAT3D(32000.0f, 32000.0f, 32000.0f),
131 INDEX m_iBulletsOnFireStart = 0,

// sniper
133 FLOAT m_fSniperMaxFOV = 90.0f,
134 FLOAT m_fSniperMinFOV = 14.2f,
135 FLOAT m_fSnipingZoomSpeed = 2.0f,
136 BOOL  m_bSniping = FALSE,
137 FLOAT m_fMinimumZoomFOV  = 53.1f,
138 FLOAT m_tmLastSniperFire = 0.0f,

// flamer
140 CEntityPointer m_penFlame,

// laser
145 INDEX m_iLaserBarrel = 0,

// fire flare
151 INDEX m_iFlare = FLARE_REMOVE,       // 0-none, 1-remove, 2-add
152 INDEX m_iSecondFlare = FLARE_REMOVE, // 0-none, 1-remove, 2-add

// cannon
160 FLOAT m_fWeaponDrawPowerOld = 0,
161 FLOAT m_fWeaponDrawPower = 0,
162 FLOAT m_tmDrawStartTime = 0.0f,

170 FLOAT m_tmFlamerStart=1e6,
171 FLOAT m_tmFlamerStop=1e9,
172 FLOAT m_tmLastChainsawSpray = 0.0f,

200 enum PlayerHand m_eHand = E_HAND_MAIN,

// [SSE] Beamgun
250 CEntityPointer m_penEnergyBeam, 

{
  CEntity *penBullet;
  CPlacement3D plBullet;
  FLOAT3D vBulletDestination;
}

resources:

  1 class   CLASS_PROJECTILE        "Classes\\Projectile.ecl",
  2 class   CLASS_BULLET            "Classes\\Bullet.ecl",
  3 class   CLASS_WEAPONEFFECT      "Classes\\PlayerWeaponsEffects.ecl",
  4 class   CLASS_PIPEBOMB          "Classes\\Pipebomb.ecl",
  5 class   CLASS_ENERGYBEAM        "Classes\\EnergyBeam.ecl",
  6 class   CLASS_CANNONBALL        "Classes\\BallProjectile.ecl",
  7 class   CLASS_WEAPONITEM        "Classes\\WeaponItem.ecl",
  8 class   CLASS_BASIC_EFFECT      "Classes\\BasicEffect.ecl",

// ************** MODELS **************
 20 modelcfg MODEL_KNIFE_CFG           "Models\\Weapons\\Knife\\Knife_FP.vmc",
 21 modelcfg MODEL_COLT_CFG            "Models\\Weapons\\Colt\\Colt_FP.vmc",
 22 modelcfg MODEL_SINGLESHOTGUN_CFG   "Models\\Weapons\\SingleShotgun\\SingleShotgun_FP.vmc",
 23 modelcfg MODEL_DOUBLESHOTGUN_CFG   "Models\\Weapons\\DoubleShotgun\\DoubleShotgun_FP.vmc",
 24 modelcfg MODEL_TOMMYGUN_CFG        "Models\\Weapons\\TommyGun\\TommyGun_FP.vmc",
 25 modelcfg MODEL_MINIGUN_CFG         "Models\\Weapons\\MiniGun\\MiniGun_FP.vmc",
 26 modelcfg MODEL_ROCKETLAUNCHER_CFG  "Models\\Weapons\\RocketLauncher\\RocketLauncher_FP.vmc",
 27 modelcfg MODEL_GRENADELAUNCHER_CFG "Models\\Weapons\\GrenadeLauncher\\GrenadeLauncher_FP.vmc",
 28 modelcfg MODEL_SNIPER_CFG          "Models\\Weapons\\Sniper\\Sniper_FP.vmc",
 29 modelcfg MODEL_FLAMER_CFG          "Models\\Weapons\\Flamer\\Flamer_FP.vmc",
 30 modelcfg MODEL_LASER_CFG           "Models\\Weapons\\Laser\\Laser_FP.vmc",
 31 modelcfg MODEL_CHAINSAW_CFG        "Models\\Weapons\\Chainsaw\\Chainsaw_FP.vmc",
 32 modelcfg MODEL_CANNON_CFG          "Models\\Weapons\\Cannon\\Cannon_FP.vmc",
 33 modelcfg MODEL_BEAMGUN_CFG         "Models\\Weapons\\Beamgun\\Beamgun_FP.vmc",
 34 modelcfg MODEL_PLASMATHROWER_CFG   "Models\\Weapons\\Plasmathrower\\Plasmathrower_FP.vmc",
 35 modelcfg MODEL_MINELAYER_CFG       "Models\\Weapons\\Minelayer\\Minelayer_FP.vmc",

 40 modelcfg MODEL_DS_HANDWITHAMMO_CFG "Models\\Weapons\\DoubleShotgun\\HandWithAmmo.vmc",

// ************** KNIFE **************
 60 sound   SOUND_KNIFE_BACK            "Models\\Weapons\\Knife\\Sounds\\Back.wav",
 61 sound   SOUND_KNIFE_HIGH            "Models\\Weapons\\Knife\\Sounds\\High.wav",
 62 sound   SOUND_KNIFE_LONG            "Models\\Weapons\\Knife\\Sounds\\Long.wav",
 63 sound   SOUND_KNIFE_LOW             "Models\\Weapons\\Knife\\Sounds\\Low.wav",

// ************** REVOLVER **************
 70 sound   SOUND_COLT_FIRE             "Models\\Weapons\\Colt\\Sounds\\Fire.wav",
 71 sound   SOUND_COLT_RELOAD           "Models\\Weapons\\Colt\\Sounds\\Reload.wav",

// ************** SINGLE SHOTGUN ************
 80 sound   SOUND_SINGLESHOTGUN_FIRE    "Models\\Weapons\\SingleShotgun\\Sounds\\_Fire.wav",

// ************** DOUBLE SHOTGUN **************
 90 sound   SOUND_DOUBLESHOTGUN_FIRE    "Models\\Weapons\\DoubleShotgun\\Sounds\\Fire.wav",
 91 sound   SOUND_DOUBLESHOTGUN_RELOAD  "Models\\Weapons\\DoubleShotgun\\Sounds\\Reload.wav",

// ************** TOMMYGUN **************
100 sound   SOUND_TOMMYGUN_FIRE         "Models\\Weapons\\TommyGun\\Sounds\\_Fire.wav",

// ************** CHAINGUN **************
110 sound   SOUND_MINIGUN_FIRE          "Models\\Weapons\\MiniGun\\Sounds\\Fire.wav",
111 sound   SOUND_MINIGUN_ROTATE        "Models\\Weapons\\MiniGun\\Sounds\\Rotate.wav",
112 sound   SOUND_MINIGUN_SPINUP        "Models\\Weapons\\MiniGun\\Sounds\\RotateUp.wav",
113 sound   SOUND_MINIGUN_SPINDOWN      "Models\\Weapons\\MiniGun\\Sounds\\RotateDown.wav",
114 sound   SOUND_MINIGUN_CLICK         "Models\\Weapons\\MiniGun\\Sounds\\Click.wav",

// ************** ROCKET LAUNCHER **************
120 sound   SOUND_ROCKETLAUNCHER_FIRE   "Models\\Weapons\\RocketLauncher\\Sounds\\_Fire.wav",

// ************** GRENADE LAUNCHER **************
130 sound   SOUND_GRENADELAUNCHER_FIRE  "Models\\Weapons\\GrenadeLauncher\\Sounds\\_Fire.wav",

// ************** SNIPER **************
140 sound   SOUND_SNIPER_FIRE           "ModelsMP\\Weapons\\Sniper\\Sounds\\Fire.wav",
//141 sound   SOUND_SNIPER_RELOAD         "ModelsMP\\Weapons\\Sniper\\Sounds\\Reload.wav",
//142 sound   SOUND_SNIPER_ZOOM           "ModelsMP\\Weapons\\Sniper\\Sounds\\Zoom.wav",

// ************** FLAMER **************
150 sound   SOUND_FL_FIRE               "ModelsMP\\Weapons\\Flamer\\Sounds\\Fire.wav",
151 sound   SOUND_FL_START              "ModelsMP\\Weapons\\Flamer\\Sounds\\Start.wav",
152 sound   SOUND_FL_STOP               "ModelsMP\\Weapons\\Flamer\\Sounds\\Stop.wav",

// ************** PLASMAGUN **************
160 sound   SOUND_LASER_FIRE            "Models\\Weapons\\Laser\\Sounds\\_Fire.wav",

// ************** CHAINSAW **************
170 sound   SOUND_CS_FIRE               "ModelsMP\\Weapons\\Chainsaw\\Sounds\\Fire.wav",
171 sound   SOUND_CS_BEGINFIRE          "ModelsMP\\Weapons\\Chainsaw\\Sounds\\BeginFire.wav",
172 sound   SOUND_CS_ENDFIRE            "ModelsMP\\Weapons\\Chainsaw\\Sounds\\EndFire.wav",
173 sound   SOUND_CS_BRINGUP            "ModelsMP\\Weapons\\Chainsaw\\Sounds\\BringUp.wav",
174 sound   SOUND_CS_IDLE               "ModelsMP\\Weapons\\Chainsaw\\Sounds\\Idle.wav",
175 sound   SOUND_CS_BRINGDOWN          "ModelsMP\\Weapons\\Chainsaw\\Sounds\\BringDown.wav",

// ************** CANNON **************
180 sound   SOUND_CANNON                "Models\\Weapons\\Cannon\\Sounds\\Fire.wav",
181 sound   SOUND_CANNON_PREPARE        "Models\\Weapons\\Cannon\\Sounds\\Prepare.wav",

// ************** BEAMGUN **************
190 sound   SOUND_BG_FIRE               "Models\\Weapons\\Beamgun\\Sounds\\_Fire.wav",

// ************** PLASMATHROWER **************
200 sound   SOUND_PLASMATHROWER_FIRE    "Models\\Weapons\\Plasmathrower\\Sounds\\_Fire.wav",

// ************** MINELAYER **************
210 sound   SOUND_MINELAYER_FIRE        "Models\\Weapons\\MineLayer\\Sounds\\_Fire.wav",


250 sound   SOUND_SILENCE               "Sounds\\Misc\\Silence.wav",

functions:

 // add to prediction any entities that this entity depends on
  void AddDependentsToPrediction(void)
  {
    m_penPlayer->AddToPrediction();
    m_penFlame->AddToPrediction();
  }

  void Precache(void)
  {
    CPlayerWeaponEntity_Precache(GetInventory()->GetWeaponsMask());
  }

  PlayerHand GetHand() const
  {
    return m_eHand;
  }

  CPlayerPawnEntity *GetPlayer(void)
  {
    ASSERT(m_penPlayer != NULL);
    return static_cast<CPlayerPawnEntity *>(&*m_penPlayer);
  }

  const CPlayerPawnEntity *GetPlayer(void) const
  {
    ASSERT(m_penPlayer != NULL);
    return static_cast<const CPlayerPawnEntity *>(&*m_penPlayer);
  }

  CPlayerAnimatorEntity *GetAnimator(void)
  {
    ASSERT(m_penPlayer != NULL);
    return static_cast<CPlayerAnimatorEntity *>(&*GetPlayer()->m_penAnimator);
  }

  // [SSE] Player Inventory Entity
  CPlayerInventoryEntity *GetInventory(void)
  {
    ASSERT(m_penPlayer != NULL);
    return static_cast<CPlayerInventoryEntity *>(&*GetPlayer()->m_penInventory);
  }

  // [SSE] Player Inventory Entity
  const CPlayerInventoryEntity *GetInventory(void) const
  {
    ASSERT(m_penPlayer != NULL);
    return static_cast<const CPlayerInventoryEntity *>(&*GetPlayer()->m_penInventory);
  }

  // [SSE] Beamgun
  class CEnergyBeamEntity *GetEnergyBeam()
  {
    ASSERT(m_penEnergyBeam != NULL);
    return static_cast<CEnergyBeamEntity *>(&*m_penEnergyBeam);
  }

  CSoundObject &GetWeaponSound(enum WeaponSound eSound)
  {
    return GetPlayer()->GetWeaponSound(GetHand(), eSound);
  }

  CModelObject *GetChainSawTeeth(void)
  {
    CPlayerPawnEntity *ppl=GetPlayer();

    if (ppl==NULL) { return NULL;}
    CModelObject *pmoPlayer = ppl->GetModelObject();

    if (pmoPlayer==NULL) { return NULL;}
    CAttachmentModelObject *pamoTorso = pmoPlayer->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO);

    if (pamoTorso==NULL) { return NULL;}
    CAttachmentModelObject *pamoChainSaw = pamoTorso->amo_moModelObject.GetAttachmentModel(BODY_ATTACHMENT_MINIGUN);

    if (pamoChainSaw==NULL) { return NULL;}
    CAttachmentModelObject *pamoBlade = pamoChainSaw->amo_moModelObject.GetAttachmentModel(CHAINSAWFORPLAYER_ATTACHMENT_BLADE);

    if (pamoBlade==NULL) { return NULL;}
    CAttachmentModelObject *pamoTeeth = pamoBlade->amo_moModelObject.GetAttachmentModel(BLADE_ATTACHMENT_TEETH);

    if (pamoTeeth==NULL) { return NULL;}
    return &pamoTeeth->amo_moModelObject;
  }

  const CTFileName &GetComputerMessageName(WeaponIndex eWeaponIndex)
  {
    static DECLARE_CTFILENAME(fnmNone, "");
    static DECLARE_CTFILENAME(fnmChainsaw, "Data\\Messages\\Weapons\\chainsaw.txt");
    static DECLARE_CTFILENAME(fnmColt, "Data\\Messages\\Weapons\\colt.txt");
    static DECLARE_CTFILENAME(fnmSingleShotgun, "Data\\Messages\\Weapons\\singleshotgun.txt");
    static DECLARE_CTFILENAME(fnmSuperShotgun, "Data\\Messages\\Weapons\\doubleshotgun.txt");
    static DECLARE_CTFILENAME(fnmTommygun, "Data\\Messages\\Weapons\\tommygun.txt");
    static DECLARE_CTFILENAME(fnmChaingun, "Data\\Messages\\Weapons\\minigun.txt");
    static DECLARE_CTFILENAME(fnmSniper, "DataMP\\Messages\\Weapons\\sniper.txt");
    static DECLARE_CTFILENAME(fnmRocketLauncher, "Data\\Messages\\Weapons\\rocketlauncher.txt");
    static DECLARE_CTFILENAME(fnmGrenadeLauncher, "Data\\Messages\\Weapons\\grenadelauncher.txt");
    static DECLARE_CTFILENAME(fnmFlamer, "DataMP\\Messages\\Weapons\\flamer.txt");
    static DECLARE_CTFILENAME(fnmPlasmagun, "Data\\Messages\\Weapons\\laser.txt");
    static DECLARE_CTFILENAME(fnmCannon, "Data\\Messages\\Weapons\\cannon.txt");

    switch (eWeaponIndex)
    {
      case E_WEAPON_CHAINSAW: return fnmChainsaw;
      case E_WEAPON_REVOLVER: return fnmColt;
      case E_WEAPON_SHOTGUN: return fnmSingleShotgun;
      case E_WEAPON_SUPERSHOTGUN: return fnmSuperShotgun;
      case E_WEAPON_MACHINEGUN: return fnmTommygun;
      case E_WEAPON_CHAINGUN: return fnmChaingun;
      case E_WEAPON_SNIPER: return fnmSniper;
      case E_WEAPON_ROCKETLAUNCHER: return fnmRocketLauncher;
      case E_WEAPON_GRENADELAUNCHER: return fnmGrenadeLauncher;
      case E_WEAPON_FLAMER: return fnmFlamer;
      case E_WEAPON_PLASMAGUN: return fnmPlasmagun;
      case E_WEAPON_CANNON: return fnmCannon;
    }

    return fnmNone;
  }

  INDEX ModelForWeaponIndex(WeaponIndex eWeaponIndex)
  {
    switch (eWeaponIndex)
    {
      case E_WEAPON_KNIFE: return MODEL_KNIFE_CFG;
      case E_WEAPON_CHAINSAW: return MODEL_CHAINSAW_CFG;
      case E_WEAPON_REVOLVER: return MODEL_COLT_CFG;
      case E_WEAPON_SHOTGUN: return MODEL_SINGLESHOTGUN_CFG;
      case E_WEAPON_SUPERSHOTGUN: return MODEL_DOUBLESHOTGUN_CFG;
      case E_WEAPON_MACHINEGUN: return MODEL_TOMMYGUN_CFG;
      case E_WEAPON_CHAINGUN: return MODEL_MINIGUN_CFG;
      case E_WEAPON_SNIPER: return MODEL_SNIPER_CFG;
      case E_WEAPON_ROCKETLAUNCHER: return MODEL_ROCKETLAUNCHER_CFG;
      case E_WEAPON_GRENADELAUNCHER: return MODEL_GRENADELAUNCHER_CFG;
      case E_WEAPON_FLAMER: return MODEL_FLAMER_CFG;
      case E_WEAPON_PLASMAGUN: return MODEL_LASER_CFG;
      case E_WEAPON_CANNON: return MODEL_CANNON_CFG;
      case E_WEAPON_BEAMGUN: return MODEL_BEAMGUN_CFG;
      case E_WEAPON_PLASMATHROWER: return MODEL_PLASMATHROWER_CFG;
      case E_WEAPON_MINELAYER: return MODEL_MINELAYER_CFG;
    }

    return -1;
  }

  INDEX SecondModelForWeaponIndex(WeaponIndex eWeaponIndex)
  {
    switch (eWeaponIndex)
    {
      case E_WEAPON_SUPERSHOTGUN: return MODEL_DS_HANDWITHAMMO_CFG;
    }

    return -1;
  }

  INDEX GetDefaultAnim(WeaponIndex eWeaponIndex)
  {
    switch (eWeaponIndex)
    {
      case E_WEAPON_KNIFE: return KNIFE_ANIM_WAIT1;
      case E_WEAPON_CHAINSAW: return CHAINSAW_ANIM_WAIT1;
      case E_WEAPON_REVOLVER: return COLT_ANIM_WAIT1;
      case E_WEAPON_SHOTGUN: return SINGLESHOTGUN_ANIM_WAIT1;
      case E_WEAPON_SUPERSHOTGUN: return DOUBLESHOTGUN_ANIM_WAIT1;
      case E_WEAPON_MACHINEGUN: return TOMMYGUN_ANIM_WAIT1;
      case E_WEAPON_SNIPER: return SNIPER_ANIM_WAIT01;
      case E_WEAPON_CHAINGUN: return MINIGUN_ANIM_WAIT1;
      case E_WEAPON_ROCKETLAUNCHER: return ROCKETLAUNCHER_ANIM_WAIT1;
      case E_WEAPON_GRENADELAUNCHER: return GRENADELAUNCHER_ANIM_WAIT1;
      case E_WEAPON_FLAMER: return FLAMER_ANIM_WAIT01;
      case E_WEAPON_PLASMAGUN: return LASER_ANIM_WAIT01;
      case E_WEAPON_CANNON: return CANNON_ANIM_WAIT01;
        
      // [SSE] Weapons
      case E_WEAPON_BEAMGUN: return BEAMGUN_ANIM_WAIT03;
      case E_WEAPON_PLASMATHROWER: return LASER_ANIM_WAIT01;
      case E_WEAPON_MINELAYER: return GRENADELAUNCHER_ANIM_WAIT1;
    }

    return -1;
  }
  
  FLOAT GetMovementSpeedBonus(WeaponIndex eWeaponIndex) const
  {
    return GetWeaponParams(eWeaponIndex)->GetMovementSpeedBonus();
  }

  FLOAT GetMovementSpeedBonus() const
  {
    return GetMovementSpeedBonus(GetCurrentWeapon());
  }

  // recoil
  void DoRecoil(void)
  {
//    CPlayerAnimatorEntity &plan = *GetAnimator();
//    plan.m_fRecoilSpeed += wpn_fRecoilSpeed[GetCurrentWeapon()];
  }

  //
  BOOL HoldingFire(void)
  {
    return m_bFireWeapon && !m_bChangeWeapon;
  }
  
  BOOL CanZoom()
  {
    return GetWeaponParams(GetCurrentWeapon())->IsZooming();
  }

  // render weapon model(s)
  void RenderWeaponModel(CPerspectiveProjection3D &prProjection, CDrawPort *pdp,
                          FLOAT3D vViewerLightDirection, COLOR colViewerLight, COLOR colViewerAmbient,
                          BOOL bRender, INDEX iEye)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();

    _mrpModelRenderPrefs.SetRenderType(RT_TEXTURE|RT_SHADING_PHONG);

    // flare attachment
    ControlFlareAttachment();
    
    CPlayerSettings *pSettings = GetPlayer()->GetSettings();
    BOOL bHideWeapon = pSettings != NULL && pSettings->ps_ulFlags&PSF_HIDEWEAPON;

    if (!bRender || eCurrentWeapon == E_WEAPON_NONE || bHideWeapon) { return; }

    // nuke and iron cannons have the same view settings
    INDEX iWeaponData = eCurrentWeapon;

    // store FOV for Crosshair
    const FLOAT fFOV = ((CPerspectiveProjection3D &)prProjection).FOVL();
    CPlacement3D plView;
    plView = ((CPlayerPawnEntity&)*m_penPlayer).en_plViewpoint;
    plView.RelativeToAbsolute(m_penPlayer->GetPlacement());
    
    CWeaponPositionParams *pPosParams = GetWeaponPositionParams((WeaponIndex)iWeaponData, GetPlayer()->IsDualWielding());

    // added: chainsaw shaking
    CPlacement3D plWeapon(pPosParams->GetPosition(), pPosParams->GetOrientation());

    if (eCurrentWeapon == E_WEAPON_CHAINSAW) {
      FLOAT3D vDelta(0.0F, 0.0F, 0.0F);
      vDelta(1) = GetPlayer()->m_fChainsawShakeDX*0.35f;
      vDelta(2) = GetPlayer()->m_fChainsawShakeDY*0.35f;

      plWeapon.pl_PositionVector += vDelta;
    }

    // make sure that weapon will be bright enough
    UBYTE ubLR,ubLG,ubLB, ubAR,ubAG,ubAB;
    ColorToRGB(colViewerLight,   ubLR,ubLG,ubLB);
    ColorToRGB(colViewerAmbient, ubAR,ubAG,ubAB);
    INDEX iMinDL = Min(Min(ubLR,ubLG),ubLB) -32;
    INDEX iMinDA = Min(Min(ubAR,ubAG),ubAB) -32;

    if (iMinDL<0) {
      ubLR = ClampUp(ubLR-iMinDL, (INDEX)255);
      ubLG = ClampUp(ubLG-iMinDL, (INDEX)255);
      ubLB = ClampUp(ubLB-iMinDL, (INDEX)255);
    }

    if (iMinDA<0) {
      ubAR = ClampUp(ubAR-iMinDA, (INDEX)255);
      ubAG = ClampUp(ubAG-iMinDA, (INDEX)255);
      ubAB = ClampUp(ubAB-iMinDA, (INDEX)255);
    }

    const COLOR colLight   = RGBToColor(ubLR,ubLG,ubLB);
    const COLOR colAmbient = RGBToColor(ubAR,ubAG,ubAB);
    const FLOAT tmNow = _pTimer->GetLerpedCurrentTick();

    UBYTE ubBlend = INVISIBILITY_ALPHA_LOCAL;
    
    CStatusEffectInstance *pInvisibility = GetInventory()->GetStrongestStatusEffectInstance(E_STEF_INVISIBILITY);
    FLOAT tmInvisibility = pInvisibility ? pInvisibility->GetDuration() : 0.0F;
    //FLOAT tmSeriousDamage = ((CPlayerPawnEntity *)&*m_penPlayer)->m_tmSeriousDamage;
    //FLOAT tmInvulnerability = ((CPlayerPawnEntity *)&*m_penPlayer)->m_tmInvulnerability;

    // TODO:

    if (tmInvisibility > 0.0F) {
      FLOAT fIntensity = 0.0f;
      if (tmInvisibility < 3.0f)
      {
        fIntensity = 0.5f-0.5f*cos((tmInvisibility-tmNow)*(6.0f*3.1415927f/3.0f));
        ubBlend =(INDEX)(INVISIBILITY_ALPHA_LOCAL+(FLOAT)(254-INVISIBILITY_ALPHA_LOCAL)*fIntensity);
      }
    }

    // DRAW WEAPON MODEL
    //  Double colt - second colt in mirror
    //  Double shotgun - hand with ammo
    if (iWeaponData==E_WEAPON_SUPERSHOTGUN )
    {
      // prepare render model structure and projection
      CRenderModel rmMain;
      CPerspectiveProjection3D prMirror = prProjection;
      prMirror.ViewerPlacementL() =  plView;
      prMirror.FrontClipDistanceL() = pPosParams->GetFrontClipDistance();
      prMirror.DepthBufferNearL() = 0.0f;
      prMirror.DepthBufferFarL() = 0.1f;

      CPlacement3D plWeaponMirror(pPosParams->GetPosition(), pPosParams->GetOrientation());

      if (GetHand() == E_HAND_SECOND) {
        FLOATmatrix3D mRotation;
        MakeRotationMatrixFast(mRotation, plView.pl_OrientationAngle);
        plWeaponMirror.pl_PositionVector(1) = -plWeaponMirror.pl_PositionVector(1);
        plWeaponMirror.pl_OrientationAngle(1) = -plWeaponMirror.pl_OrientationAngle(1);
        plWeaponMirror.pl_OrientationAngle(3) = -plWeaponMirror.pl_OrientationAngle(3);
      }

      ((CPerspectiveProjection3D &)prMirror).FOVL() = pPosParams->GetFieldOfView();
      CAnyProjection3D apr;
      apr = prMirror;
      Stereo_AdjustProjection(*apr, iEye, 0.1f);
      BeginModelRenderingView(apr, pdp);

      WeaponMovingOffset(plWeaponMirror.pl_PositionVector);
      plWeaponMirror.RelativeToAbsoluteSmooth(plView);
      rmMain.SetObjectPlacement(plWeaponMirror);

      rmMain.rm_colLight   = colLight;
      rmMain.rm_colAmbient = colAmbient;
      rmMain.rm_vLightDirection = vViewerLightDirection;
      rmMain.rm_ulFlags |= RMF_WEAPON; // TEMP: for Truform

      if (tmInvisibility > 0.0F) {
        rmMain.rm_colBlend = (rmMain.rm_colBlend&0xffffff00)|ubBlend;
      }

      m_moWeaponSecond.SetupModelRendering(rmMain);
      m_moWeaponSecond.RenderModel(rmMain);
      EndModelRenderingView();
    }

    // chaingun specific (update rotation)
    if (iWeaponData==E_WEAPON_CHAINGUN) { RotateChaingun(); }

    // prepare render model structure
    CRenderModel rmMain;
    prProjection.ViewerPlacementL() =  plView;
    prProjection.FrontClipDistanceL() = pPosParams->GetFrontClipDistance();
    prProjection.DepthBufferNearL() = 0.0f;
    prProjection.DepthBufferFarL() = 0.1f;
    
    if (GetHand() == E_HAND_SECOND) {
      FLOATmatrix3D mRotation;
      MakeRotationMatrixFast(mRotation, plView.pl_OrientationAngle);
      plWeapon.pl_PositionVector(1) = -plWeapon.pl_PositionVector(1);
      plWeapon.pl_OrientationAngle(1) = -plWeapon.pl_OrientationAngle(1);
      plWeapon.pl_OrientationAngle(3) = -plWeapon.pl_OrientationAngle(3);
    }

    ((CPerspectiveProjection3D &)prProjection).FOVL() = pPosParams->GetFieldOfView();

    CAnyProjection3D apr;
    apr = prProjection;
    Stereo_AdjustProjection(*apr, iEye, 0.1f);
    BeginModelRenderingView(apr, pdp);

    WeaponMovingOffset(plWeapon.pl_PositionVector);
    plWeapon.RelativeToAbsoluteSmooth(plView);
    rmMain.SetObjectPlacement(plWeapon);

    rmMain.rm_colLight   = colLight;
    rmMain.rm_colAmbient = colAmbient;
    rmMain.rm_vLightDirection = vViewerLightDirection;
    rmMain.rm_ulFlags |= RMF_WEAPON; // TEMP: for Truform

    if (tmInvisibility > 0.0F) {
      rmMain.rm_colBlend = (rmMain.rm_colBlend&0xffffff00)|ubBlend;
    }

    m_moWeapon.SetupModelRendering(rmMain);
    m_moWeapon.RenderModel(rmMain);

    /*if (tmSeriousDamage>tmNow && tmInvulnerability>tmNow) {
      Particle_PrepareSystem(pdp, apr);
      Particle_PrepareEntity(1, 0, 0, NULL);
      Particles_ModelGlow2(&m_moWeapon, plWeapon, Max(tmSeriousDamage, tmInvulnerability),PT_STAR08, 0.025f, 2, 0.01f, 0xff00ff00);
      Particle_EndSystem();
    } else if (tmInvulnerability>tmNow) {
      Particle_PrepareSystem(pdp, apr);
      Particle_PrepareEntity(1, 0, 0, NULL);
      Particles_ModelGlow2(&m_moWeapon, plWeapon, tmInvulnerability, PT_STAR05, 0.025f, tmp_ai[1], 0.01f, 0x3333ff00);
      Particle_EndSystem();
    } else if (tmSeriousDamage>tmNow) {
      Particle_PrepareSystem(pdp, apr);
      Particle_PrepareEntity(1, 0, 0, NULL);
      Particles_ModelGlow2(&m_moWeapon, plWeapon, tmSeriousDamage, PT_STAR08, 0.025f, 2, 0.01f, 0xff777700);
      Particle_EndSystem();
    }*/

    EndModelRenderingView();

    // restore FOV for Crosshair
    ((CPerspectiveProjection3D &)prProjection).FOVL() = fFOV;
  };


  // Weapon moving offset
  void WeaponMovingOffset(FLOAT3D &plPos)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();

    CPlayerAnimatorEntity &plan = *GetAnimator();
    FLOAT fXOffset = Lerp(plan.m_fMoveLastBanking, plan.m_fMoveBanking, _pTimer->GetLerpFactor()) * -0.02f;
    FLOAT fYOffset = Lerp(plan.m_fWeaponYLastOffset, plan.m_fWeaponYOffset, _pTimer->GetLerpFactor()) * 0.15f;
    fYOffset += (fXOffset * fXOffset) * 30.0f;
    plPos(1) += fXOffset;
    plPos(2) += fYOffset;

    // apply grenade launcher pumping
    if (eCurrentWeapon == E_WEAPON_GRENADELAUNCHER)
    {
      // obtain moving part attachment
      CAttachmentModelObject *amo = m_moWeapon.GetAttachmentModel(GRENADELAUNCHER_ATTACHMENT_MOVING_PART);
      FLOAT fLerpedMovement = Lerp(m_fWeaponDrawPowerOld, m_fWeaponDrawPower, _pTimer->GetLerpFactor());
      amo->amo_plRelative.pl_PositionVector(3) = fLerpedMovement;
      plPos(3) += fLerpedMovement/2.0f;

      if (m_tmDrawStartTime != 0.0f)
      {
        FLOAT tmPassed = _pTimer->GetLerpedCurrentTick()-m_tmDrawStartTime;
        plPos(1) += Sin(tmPassed*360.0f*10)*0.0125f*tmPassed/6.0f;
        plPos(2) += Sin(tmPassed*270.0f*8)*0.01f*tmPassed/6.0f;
      }
    }

    // apply cannon draw
    else if (eCurrentWeapon == E_WEAPON_CANNON)
    {
      FLOAT fLerpedMovement = Lerp(m_fWeaponDrawPowerOld, m_fWeaponDrawPower, _pTimer->GetLerpFactor());
      plPos(3) += fLerpedMovement;

      if (m_tmDrawStartTime != 0.0f)
      {
        FLOAT tmPassed = _pTimer->GetLerpedCurrentTick()-m_tmDrawStartTime;
        plPos(1) += Sin(tmPassed*360.0f*10)*0.0125f*tmPassed/2.0f;
        plPos(2) += Sin(tmPassed*270.0f*8)*0.01f*tmPassed/2.0f;
      }
    }
  };

  // check target for time prediction updating
  void CheckTargetPrediction(CEntity *penTarget)
  {
    // if target is not predictable
    if (!penTarget->IsPredictable()) {
      return; // do nothing
    }

    extern FLOAT cli_tmPredictFoe;
    extern FLOAT cli_tmPredictAlly;
    extern FLOAT cli_tmPredictEnemy;

    // get your and target's bases for prediction
    CEntity *penMe = GetPlayer();
    if (IsPredictor()) {
      penMe = penMe->GetPredicted();
    }
    CEntity *penYou = penTarget;
    if (penYou->IsPredictor()) {
      penYou = penYou->GetPredicted();
    }

    // if player
    if (penTarget->IsPlayerControlled()) {
      // if ally player
      if (GetSP()->sp_bCooperative) {
        // if ally prediction is on and this player is local
        if (cli_tmPredictAlly>0 && _pNetwork->IsPlayerLocal(penMe)) {
          // predict the ally
          penYou->SetPredictionTime(cli_tmPredictAlly);
        }

      // if foe player
      } else {
        // if foe prediction is on
        if (cli_tmPredictFoe>0) {
          // if this player is local
          if (_pNetwork->IsPlayerLocal(penMe)) {
            // predict the foe
            penYou->SetPredictionTime(cli_tmPredictFoe);
          }
          // if the target is local
          if (_pNetwork->IsPlayerLocal(penYou)) {
            // predict self
            penMe->SetPredictionTime(cli_tmPredictFoe);
          }
        }
      }

    } else {
      // if enemy prediction is on an it is an enemy
      if (cli_tmPredictEnemy>0 && IsDerivedFromClass(penYou, &CEnemyBaseEntity_DLLClass)) {
        // if this player is local
        if (_pNetwork->IsPlayerLocal(penMe)) {
          // set enemy prediction time
          penYou->SetPredictionTime(cli_tmPredictEnemy);
        }
      }
    }
  }

  // cast a ray from weapon
  void UpdateTargetingInfo(void)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon);
    FLOAT3D vFirePos = FLOAT3D(0, 0, 0);

    // crosshair start position from weapon
    CPlacement3D plCrosshair;

    if (GetPlayer()->m_iViewState != PVT_3RDPERSONVIEW) {
      vFirePos = FLOAT3D(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2), 0);
    }

    CalcWeaponPosition(vFirePos, plCrosshair, FALSE);

    // cast ray
    CCastRay crRay(m_penPlayer, plCrosshair);
    crRay.cr_bHitTranslucentPortals = FALSE;
    crRay.cr_bPhysical = FALSE;
    crRay.cr_ttHitModels = CCastRay::TT_COLLISIONBOX;
    GetWorld()->CastRay(crRay);

    // store required cast ray results
    m_vRayHitLast = m_vRayHit;  // for lerping purposes
    m_vRayHit   = crRay.cr_vHit;
    m_penRayHit = crRay.cr_penHit;
    m_fRayHitDistance = crRay.cr_fHitDistance;
    m_fEnemyHealth = 0.0f;

    // set some targeting properties (snooping and such...)
    TIME tmNow = _pTimer->CurrentTick();
    if (m_penRayHit != NULL)
    {
      CEntity *pen = m_penRayHit;
      
      if (pen->IsPawn()) {
        m_fEnemyHealth = static_cast<CPawnEntity*>(pen)->GetHealthFactor();
      }
      
      // if alive
      if (pen->GetFlags()&ENF_ALIVE)
      {
        // check the target for time prediction updating
        CheckTargetPrediction(pen);

        // if player
        if (IsDerivedFromClass(pen, &CPlayerPawnEntity_DLLClass)) {
          // rememer when targeting begun
          if (m_tmTargetingStarted == 0) {
            m_penTargeting = pen;
            m_tmTargetingStarted = tmNow;
          }

          // keep player name, mana and health for eventual printout or coloring
          m_strLastTarget.PrintF("%s", ((CPlayerPawnEntity*)pen)->GetPlayerName());

          if (GetSP()->sp_gmGameMode == CSessionProperties::GM_SCOREMATCH) {
            // add mana to player name
            CTString strMana="";
            strMana.PrintF(" (%d)", ((CPlayerPawnEntity*)pen)->m_iMana);
            m_strLastTarget += strMana;
          }

          if (hud_bShowPlayerName) {
            m_tmLastTarget = tmNow + 1.5f;
          }

        // not targeting player
        } else {
          // reset targeting
          m_tmTargetingStarted = 0;
        }

        // cannot snoop while firing
        if (m_bFireWeapon) {
          m_tmTargetingStarted = 0;
        }

      // if not alive
      } else {
        // not targeting player
        m_tmTargetingStarted = 0;

        // check switch relaying by moving brush
        if (IsOfClass(pen, &CMovingBrushEntity_DLLClass)) {
          CEntity *penRelayedSwitch = ((CMovingBrushEntity *)pen)->m_penSwitch;
          
          if (penRelayedSwitch != NULL) {
            pen = penRelayedSwitch;
          }
        }

        // if switch and near enough
        if (IsOfClass(pen, &CSwitchEntity_DLLClass)) {
          CSwitchEntity &enSwitch = (CSwitchEntity&)*pen;

          // if switch is useable
          if ((m_fRayHitDistance < enSwitch.GetDistance()) && enSwitch.m_bUseable) {
            // show switch message
            if (enSwitch.m_strMessage != "") {
              m_strLastTarget = enSwitch.m_strMessage;
            } else {
              m_strLastTarget = TRANS("Use");
            }

            m_tmLastTarget = tmNow + 0.5f;
          }
        }

        // if analyzable
        if (IsOfClass(pen, &CMessageHolderEntity_DLLClass)) {
          CMessageHolderEntity *penMH = static_cast<CMessageHolderEntity *>(pen);
          
          if ((m_fRayHitDistance < penMH->GetDistance()) && penMH->m_bActive) {
            const CTFileName &fnmMessage = penMH->m_fnmMessage;

            // if player doesn't have that message it database
            if (!GetPlayer()->HasMessage(fnmMessage)) {
              // show analyse message
              m_strLastTarget = TRANS("Analyze");
              m_tmLastTarget  = tmNow + 0.5f;
            }
          }
        }
      }

    // if didn't hit anything
    } else {
      // not targeting player
      m_tmTargetingStarted = 0;

      // remember position ahead
      FLOAT3D vDir = crRay.cr_vTarget - crRay.cr_vOrigin;
      vDir.Normalize();
      m_vRayHit = crRay.cr_vOrigin + vDir * 50.0f;
    }

    // determine snooping time
    TIME tmDelta = tmNow - m_tmTargetingStarted;
    if (m_tmTargetingStarted > 0 && plr_tmSnoopingDelay > 0 && tmDelta > plr_tmSnoopingDelay) {
      m_tmSnoopingStarted = tmNow;
    }
  }

  // Render Crosshair
  void RenderCrosshair(CProjection3D &prProjection, CDrawPort *pdp, CPlacement3D &plViewSource)
  {
    CPlayerSettings *pSettings = GetPlayer()->GetSettings();
    INDEX iCrossHair = pSettings != NULL ? pSettings->ps_iCrossHairType + 1 : 1;

    // adjust crosshair type
    if (iCrossHair<=0) {
      iCrossHair  = 0;
      _iLastCrosshairType = 0;
    }

    // create new crosshair texture (if needed)
    if (_iLastCrosshairType != iCrossHair) {
      _iLastCrosshairType = iCrossHair;
      CTString fnCrosshair;
      fnCrosshair.PrintF("Textures\\Interface\\Crosshairs\\Crosshair%d.tex", iCrossHair);

      try {
        // load new crosshair texture
        _toCrosshair.SetData_t(fnCrosshair);
      } catch(char *strError) {
        // didn't make it! - reset crosshair
        CErrorF(strError);
        iCrossHair = 0;
        return;
      }
    }

    COLOR colCrosshair = C_WHITE;
    TIME  tmNow = _pTimer->CurrentTick();

    // if hit anything
    FLOAT3D vOnScreen;
    FLOAT   fDistance = m_fRayHitDistance;
    //const FLOAT3D vRayHit = Lerp(m_vRayHitLast, m_vRayHit, _pTimer->GetLerpFactor());
    const FLOAT3D vRayHit = m_vRayHit;  // lerping doesn't seem to work ???

    // if hit anything
    if (m_penRayHit!=NULL) {

      CEntity *pen = m_penRayHit;
      // do screen projection
      prProjection.ViewerPlacementL() = plViewSource;
      prProjection.ObjectPlacementL() = CPlacement3D(FLOAT3D(0.0f, 0.0f, 0.0f), ANGLE3D(0, 0, 0));
      prProjection.Prepare();
      prProjection.ProjectCoordinate(vRayHit, vOnScreen);

      // if required, show enemy health thru crosshair color
      if (hud_bCrosshairColoring && m_fEnemyHealth > 0) {
        colCrosshair = HUD_GetCrosshairColor(m_fEnemyHealth);
      }

    // if didn't hit anything
    } else {
      // far away in screen center
      vOnScreen(1) = (FLOAT)pdp->GetWidth()  *0.5f;
      vOnScreen(2) = (FLOAT)pdp->GetHeight() *0.5f;
      fDistance    = 100.0f;
    }

    // if croshair should be of fixed position
    if (hud_bCrosshairFixed || GetPlayer()->m_iViewState == PVT_3RDPERSONVIEW) {
      // reset it to screen center
      vOnScreen(1) = (FLOAT)pdp->GetWidth()  *0.5f;
      vOnScreen(2) = (FLOAT)pdp->GetHeight() *0.5f;
      //fDistance    = 100.0f;
    }

    // clamp console variables
    hud_fCrosshairScale   = Clamp(hud_fCrosshairScale,   0.1f, 2.0f);
    hud_fCrosshairRatio   = Clamp(hud_fCrosshairRatio,   0.1f, 1.0f);
    hud_fCrosshairOpacity = Clamp(hud_fCrosshairOpacity, 0.1f, 1.0f);
    const ULONG ulAlpha = NormFloatToByte(hud_fCrosshairOpacity);

    // draw crosshair if needed
    if (iCrossHair>0) {
      // determine crosshair size
      const FLOAT fMinD =   1.0f;
      const FLOAT fMaxD = 100.0f;
      fDistance = Clamp(fDistance, fMinD, fMaxD);
      const FLOAT fRatio   = (fDistance-fMinD) / (fMaxD-fMinD);
      const FLOAT fMaxSize = (FLOAT)pdp->GetWidth() / 640.0f;
      const FLOAT fMinSize = fMaxSize * hud_fCrosshairRatio;
      const FLOAT fSize    = 16 * Lerp(fMaxSize, fMinSize, fRatio) * hud_fCrosshairScale;

      // draw crosshair
      const FLOAT fI0 = + (PIX)vOnScreen(1) - fSize;
      const FLOAT fI1 = + (PIX)vOnScreen(1) + fSize;
      const FLOAT fJ0 = - (PIX)vOnScreen(2) - fSize +pdp->GetHeight();
      const FLOAT fJ1 = - (PIX)vOnScreen(2) + fSize +pdp->GetHeight();
      pdp->InitTexture(&_toCrosshair);
      pdp->AddTexture(fI0, fJ0, fI1, fJ1, colCrosshair|ulAlpha);
      pdp->FlushRenderingQueue();
    }

    // if there is still time
    TIME tmDelta = m_tmLastTarget - tmNow;
    if (tmDelta>0) {
      // printout current target info
      SLONG slDPWidth  = pdp->GetWidth();
      SLONG slDPHeight = pdp->GetHeight();
      FLOAT fScaling   = (FLOAT)slDPWidth/640.0f;

      // set font and scale
      pdp->SetFont(_pfdDisplayFont);
      pdp->SetTextScaling(fScaling);
      pdp->SetTextAspect(1.0f);

      // do faded printout
      ULONG ulA = (FLOAT)ulAlpha * Clamp(2*tmDelta, 0.0f, 1.0f);
      pdp->PutTextC(m_strLastTarget, slDPWidth*0.5f, slDPHeight*0.75f, SE_COL_BLUE_NEUTRAL|ulA);
    }

    // printout crosshair world coordinates if needed
    if (hud_bShowCoords) {
      CTString strCoords;
      SLONG slDPWidth  = pdp->GetWidth();
      SLONG slDPHeight = pdp->GetHeight();

      // set font and scale
      pdp->SetFont(_pfdDisplayFont);
      pdp->SetTextAspect(1.0f);
      pdp->SetTextScaling((FLOAT)slDPWidth/640.0f);

      // do printout only if coordinates are valid
      const FLOAT fMax = Max(Max(vRayHit(1), vRayHit(2)), vRayHit(3));
      const FLOAT fMin = Min(Min(vRayHit(1), vRayHit(2)), vRayHit(3));

      if (fMax<+100000 && fMin>-100000) {
        strCoords.PrintF("%.0f,%.0f,%.0f", vRayHit(1), vRayHit(2), vRayHit(3));
        pdp->PutTextC(strCoords, slDPWidth*0.5f, slDPHeight*0.10f, C_WHITE|CT_OPAQUE);
      }
      
      if (m_penRayHit) {
        if (HEALTH_VALUE_MULTIPLIER > 1) {
          strCoords.PrintF("%s - %d.%02d", m_penRayHit->GetName(), m_penRayHit->GetHealth() / HEALTH_VALUE_MULTIPLIER, m_penRayHit->GetHealth() % HEALTH_VALUE_MULTIPLIER);
        } else {
          strCoords.PrintF("%s - %d", m_penRayHit->GetName(), m_penRayHit->GetHealth());
        }
        pdp->PutTextC(strCoords, slDPWidth*0.5f, slDPHeight*0.15f, C_WHITE|CT_OPAQUE);
      }
    }
  };

/************************************************************
 *                      FIRE FLARE                          *
 ************************************************************/
  // show flare
  void ShowFlare(CModelObject &moWeapon, INDEX iAttachObject, INDEX iAttachFlare, FLOAT fSize)
  {
    CModelObject *pmo = &(moWeapon.GetAttachmentModel(iAttachObject)->amo_moModelObject);
    CAttachmentModelObject *pamo = pmo->GetAttachmentModel(iAttachFlare);
    pamo->amo_plRelative.pl_OrientationAngle(3) = (rand()*360.0f)/RAND_MAX;
    pmo = &(pamo->amo_moModelObject);
    pmo->StretchModel(FLOAT3D(fSize, fSize, fSize));
  };

  // hide flare
  void HideFlare(CModelObject &moWeapon, INDEX iAttachObject, INDEX iAttachFlare)
  {
    CModelObject *pmo = &(moWeapon.GetAttachmentModel(iAttachObject)->amo_moModelObject);
    pmo = &(pmo->GetAttachmentModel(iAttachFlare)->amo_moModelObject);
    pmo->StretchModel(FLOAT3D(0, 0, 0));
  };

  void SetFlare(INDEX iFlare, INDEX iAction)
  {
    // if not a prediction head then do nothing
    if (!IsPredictionHead()) {
      return;
    }

    // get your prediction tail
    CPlayerWeaponEntity *pen = (CPlayerWeaponEntity*)GetPredictionTail();

    if (iFlare==0) {
      pen->m_iFlare = iAction;
      pen->GetPlayer()->GetPlayerAnimator()->m_iFlare = iAction;
    } else {
      pen->m_iSecondFlare = iAction;
      pen->GetPlayer()->GetPlayerAnimator()->m_iSecondFlare = iAction;
    }
  }

  // flare attachment
  void ControlFlareAttachment(void)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();

    // if not a prediction head
/*    if (!IsPredictionHead()) {
      // do nothing
      return;
    }
    */

    // get your prediction tail
    CPlayerWeaponEntity *pen = (CPlayerWeaponEntity *)GetPredictionTail();

    // add flare
    if (pen->m_iFlare==FLARE_ADD) {
      pen->m_iFlare = FLARE_REMOVE;

      switch (eCurrentWeapon)
      {
        case E_WEAPON_REVOLVER:
          ShowFlare(m_moWeapon, COLT_ATTACHMENT_COLT, COLTMAIN_ATTACHMENT_FLARE, 0.75f);
          break;
        case E_WEAPON_SHOTGUN:
          ShowFlare(m_moWeapon, SINGLESHOTGUN_ATTACHMENT_BARRELS, BARRELS_ATTACHMENT_FLARE, 1.0f);
          break;
        case E_WEAPON_SUPERSHOTGUN:
          ShowFlare(m_moWeapon, DOUBLESHOTGUN_ATTACHMENT_BARRELS, DSHOTGUNBARRELS_ATTACHMENT_FLARE, 1.75f);
          break;
        case E_WEAPON_MACHINEGUN:
          ShowFlare(m_moWeapon, TOMMYGUN_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE, 0.5f);
          break;
        case E_WEAPON_SNIPER:
          ShowFlare(m_moWeapon, SNIPER_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE, 0.5f);
          break;
        case E_WEAPON_CHAINGUN:
          ShowFlare(m_moWeapon, MINIGUN_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE, 1.25f);
          break;
      }

    // remove
    } else if (pen->m_iFlare==FLARE_REMOVE) {
      switch (eCurrentWeapon)
      {
        case E_WEAPON_REVOLVER:
          HideFlare(m_moWeapon, COLT_ATTACHMENT_COLT, COLTMAIN_ATTACHMENT_FLARE);
          break;
        case E_WEAPON_SHOTGUN:
          HideFlare(m_moWeapon, SINGLESHOTGUN_ATTACHMENT_BARRELS, BARRELS_ATTACHMENT_FLARE);
          break;
        case E_WEAPON_SUPERSHOTGUN:
          HideFlare(m_moWeapon, DOUBLESHOTGUN_ATTACHMENT_BARRELS, DSHOTGUNBARRELS_ATTACHMENT_FLARE);
          break;
        case E_WEAPON_MACHINEGUN:
          HideFlare(m_moWeapon, TOMMYGUN_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE);
          break;
        case E_WEAPON_SNIPER:
          HideFlare(m_moWeapon, SNIPER_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE);
          break;
        case E_WEAPON_CHAINGUN:
          HideFlare(m_moWeapon, MINIGUN_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE);
          break;
      }
    } else {
      ASSERT(FALSE);
    }
  };


  // play light animation
  void PlayLightAnim(INDEX iAnim, ULONG ulFlags)
  {
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;

    if (pl.m_aoLightAnimation.GetData()!=NULL) {
      pl.m_aoLightAnimation.PlayAnim(iAnim, ulFlags);
    }
  };

  void SetWeaponModel()
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const INDEX idMainModel = ModelForWeaponIndex(eCurrentWeapon);
    const INDEX idSecondModel = SecondModelForWeaponIndex(eCurrentWeapon);

    if (idMainModel != -1) {
      SetAnyModel(m_moWeapon, idMainModel);

      if (GetHand() == E_HAND_SECOND) {
        m_moWeapon.StretchModel(FLOAT3D(-1, 1, 1));
      } else {
        m_moWeapon.StretchModel(FLOAT3D(1, 1, 1));
      }
    }

    if (idSecondModel != -1) {
      SetAnyModel(m_moWeaponSecond, idSecondModel);

      if (GetHand() == E_HAND_SECOND) {
        m_moWeaponSecond.StretchModel(FLOAT3D(-1, 1, 1));
      } else {
        m_moWeaponSecond.StretchModel(FLOAT3D(1, 1, 1));
      }
    }

    // WARNING !!! ---> Order of attachment must be the same with order in RenderWeaponModel()
    switch (eCurrentWeapon)
    {
      // knife
      case E_WEAPON_KNIFE:
        m_moWeapon.PlayAnim(KNIFE_ANIM_WAIT1, 0);
        break;

      // revolver
      case E_WEAPON_REVOLVER: {
        m_moWeapon.PlayAnim(COLT_ANIM_WAIT1, 0);
        break;
      }

      case E_WEAPON_SHOTGUN: {
        m_moWeapon.PlayAnim(SINGLESHOTGUN_ANIM_WAIT1, 0);
        break;
      }

      case E_WEAPON_SUPERSHOTGUN: {
        m_moWeapon.PlayAnim(DOUBLESHOTGUN_ANIM_WAIT1, 0);
        break;
      }
    }
  }

  /*
   *  >>>---  ROTATE CHAINGUN  ---<<<
   */
  void RotateChaingun(void)
  {
    ANGLE aAngle = Lerp(m_aChaingunLast, m_aChaingun, _pTimer->GetLerpFactor());
    // rotate chaingun barrels
    CAttachmentModelObject *amo = m_moWeapon.GetAttachmentModel(MINIGUN_ATTACHMENT_BARRELS);
    amo->amo_plRelative.pl_OrientationAngle(3) = aAngle;
  };

  /*
   *  >>>---  SUPPORT (COMMON) FUNCTIONS  ---<<<
   */

  // calc weapon position for 3rd person view
  void CalcWeaponPosition3rdPersonView(FLOAT3D vPos, CPlacement3D &plPos, BOOL bResetZ)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    plPos.pl_PositionVector = pPosParams->GetPosition();
    plPos.pl_OrientationAngle = ANGLE3D(0, 0, 0);


    // weapon handle
    if (GetHand() != E_HAND_MAIN) {
      plPos.pl_PositionVector(1) = -plPos.pl_PositionVector(1); // Invert axis.
    }

    // weapon offset
    plPos.RelativeToAbsoluteSmooth(CPlacement3D(vPos, ANGLE3D(0, 0, 0)));

    plPos.pl_PositionVector(1) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);
    plPos.pl_PositionVector(2) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);
    plPos.pl_PositionVector(3) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);

    if (bResetZ) {
      plPos.pl_PositionVector(3) = 0.0f;
    }

    // player view and absolute position
    CPlacement3D plView = ((CPlayerPawnEntity &)*m_penPlayer).en_plViewpoint;
    plView.pl_PositionVector(2)= 1.25118f;
    plPos.RelativeToAbsoluteSmooth(plView);
    plPos.RelativeToAbsoluteSmooth(m_penPlayer->GetPlacement());
  };

  // calc weapon position
  void CalcWeaponPosition(FLOAT3D vPos, CPlacement3D &plPos, BOOL bResetZ)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    plPos.pl_PositionVector = pPosParams->GetPosition();
    plPos.pl_OrientationAngle = ANGLE3D(0, 0, 0);

    // weapon handle
    if (GetHand() == E_HAND_MAIN) {
      if (m_bSniping) {
        plPos.pl_PositionVector = FLOAT3D(0.0f, 0.0f, 0.0f);
      }
    } else {
      plPos.pl_PositionVector(1) = -plPos.pl_PositionVector(1); // Invert axis.
    }

    plPos.RelativeToAbsoluteSmooth(CPlacement3D(vPos, ANGLE3D(0, 0, 0)));

    plPos.pl_PositionVector(1) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);
    plPos.pl_PositionVector(2) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);
    plPos.pl_PositionVector(3) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);

    if (bResetZ) {
      plPos.pl_PositionVector(3) = 0.0f;
    }

    // player view and absolute position
    CPlacement3D plView = ((CPlayerPawnEntity &)*m_penPlayer).en_plViewpoint;
    plView.pl_PositionVector(2) += GetAnimator()->m_fEyesYOffset;
    plPos.RelativeToAbsoluteSmooth(plView);
    plPos.RelativeToAbsoluteSmooth(m_penPlayer->GetPlacement());
  };

  // calc lerped weapon position
  void CalcLerpedWeaponPosition(FLOAT3D vPos, CPlacement3D &plPos, BOOL bResetZ)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    plPos.pl_PositionVector = pPosParams->GetPosition();
    plPos.pl_OrientationAngle = ANGLE3D(0, 0, 0);

    // weapon handle
    if (GetHand() == E_HAND_MAIN) {
      if (m_bSniping) {
        plPos.pl_PositionVector = FLOAT3D(0.0f, 0.0f, 0.0f );
      }
    } else {
      plPos.pl_PositionVector(1) = -plPos.pl_PositionVector(1); // Invert axis.
    }

    // weapon offset
    plPos.RelativeToAbsoluteSmooth(CPlacement3D(vPos, ANGLE3D(0, 0, 0)));

    plPos.pl_PositionVector(1) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);
    plPos.pl_PositionVector(2) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);
    plPos.pl_PositionVector(3) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);

    if (bResetZ) {
      plPos.pl_PositionVector(3) = 0.0f;
    }

    // player view and absolute position
    CPlacement3D plRes;
    GetPlayer()->GetLerpedWeaponPosition(plPos.pl_PositionVector, plRes);
    plPos=plRes;
  };

  // calc weapon position
  void CalcWeaponPositionImprecise (FLOAT3D vPos, CPlacement3D &plPos, BOOL bResetZ, FLOAT fImprecissionAngle)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    plPos.pl_PositionVector = pPosParams->GetPosition();
    plPos.pl_OrientationAngle = ANGLE3D((FRnd()-0.5f)*fImprecissionAngle, (FRnd()-0.5f)*fImprecissionAngle, 0);

    // weapon handle
    if (GetHand() == E_HAND_MAIN) {
      if (m_bSniping) {
        plPos.pl_PositionVector = FLOAT3D(0.0f, 0.0f, 0.0f );
      }
    } else {
      plPos.pl_PositionVector(1) = -plPos.pl_PositionVector(1); // Invert axis.
    }

    // weapon offset
    plPos.RelativeToAbsoluteSmooth(CPlacement3D(vPos, ANGLE3D(0, 0, 0)));

    plPos.pl_PositionVector(1) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);
    plPos.pl_PositionVector(2) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);
    plPos.pl_PositionVector(3) *= SinFast(pPosParams->GetFieldOfView()/2) / SinFast(90.0f/2);

    if (bResetZ) {
      plPos.pl_PositionVector(3) = 0.0f;
    }

    // player view and absolute position
    CPlacement3D plView = ((CPlayerPawnEntity &)*m_penPlayer).en_plViewpoint;
    plView.pl_PositionVector(2) += GetAnimator()->m_fEyesYOffset;
    plPos.RelativeToAbsoluteSmooth(plView);
    plPos.RelativeToAbsoluteSmooth(m_penPlayer->GetPlacement());
  };

  // setup 3D sound parameters
  void Setup3DSoundParameters(void)
  {
    // initialize sound 3D parameters
    GetWeaponSound(E_WEAPON_SOUND_0).Set3DParameters(50.0f, 5.0f, 1.0f, 1.0f);
    GetWeaponSound(E_WEAPON_SOUND_1).Set3DParameters(50.0f, 5.0f, 1.0f, 1.0f);
    GetWeaponSound(E_WEAPON_SOUND_2).Set3DParameters(50.0f, 5.0f, 1.0f, 1.0f);
    GetWeaponSound(E_WEAPON_SOUND_3).Set3DParameters(50.0f, 5.0f, 1.0f, 1.0f);
    GetWeaponSound(E_WEAPON_SOUND_AMBIENT).Set3DParameters(30.0f, 3.0f, 0.0f, 1.0f);
  };

  /*
   *  >>>---  FIRE FUNCTIONS  ---<<<
   */

  // cut in front of you with knife
  BOOL CutWithKnife(FLOAT fX, FLOAT fY, FLOAT fRange, FLOAT fWide, FLOAT fThickness, FLOAT fDamage)
  {
    // knife start position
    CPlacement3D plKnife;
    CalcWeaponPosition(FLOAT3D(fX, fY, 0), plKnife, TRUE);

    // create a set of rays to test
    const FLOAT3D &vBase = plKnife.pl_PositionVector;
    FLOATmatrix3D m;
    MakeRotationMatrixFast(m, plKnife.pl_OrientationAngle);
    FLOAT3D vRight = m.GetColumn(1)*fWide;
    FLOAT3D vUp    = m.GetColumn(2)*fWide;
    FLOAT3D vFront = -m.GetColumn(3)*fRange;

    FLOAT3D vDest[5];
    vDest[0] = vBase+vFront;
    vDest[1] = vBase+vFront+vUp;
    vDest[2] = vBase+vFront-vUp;
    vDest[3] = vBase+vFront+vRight;
    vDest[4] = vBase+vFront-vRight;

    CEntity *penClosest = NULL;
    FLOAT fDistance = UpperLimit(0.0f);
    FLOAT3D vHit;
    FLOAT3D vDir;

    // for each ray
    for (INDEX i=0; i<5; i++)
    {
      // cast a ray to find if any model
      CCastRay crRay(m_penPlayer, vBase, vDest[i]);
      crRay.cr_bHitTranslucentPortals = FALSE;
      crRay.cr_fTestR = fThickness;
      crRay.cr_ttHitModels = CCastRay::TT_COLLISIONBOX;
      GetWorld()->CastRay(crRay);

      // if hit something
      if (crRay.cr_penHit!=NULL /*&& crRay.cr_penHit->GetRenderType()==RT_MODEL*/ && crRay.cr_fHitDistance<fDistance) {
        penClosest = crRay.cr_penHit;
        fDistance = crRay.cr_fHitDistance;
        vDir = vDest[i]-vBase;
        vHit = crRay.cr_vHit;

        if (i==0) {
          if (crRay.cr_penHit->GetRenderType()==RT_BRUSH) {
            INDEX iSurfaceType=crRay.cr_pbpoBrushPolygon->bpo_bppProperties.bpp_ubSurfaceType;
            EffectParticlesType eptType=GetParticleEffectTypeForSurface(iSurfaceType);

            FLOAT3D vNormal=crRay.cr_pbpoBrushPolygon->bpo_pbplPlane->bpl_plAbsolute;
            FLOAT3D vReflected = vDir-vNormal*(2.0f*(vNormal%vDir));
            ((CPlayerPawnEntity&)*m_penPlayer).AddBulletSpray(vBase+vFront, eptType, vReflected);

          } else if (crRay.cr_penHit->GetRenderType()==RT_MODEL) {
            BOOL bRender=TRUE;
            FLOAT3D vSpillDir=-((CPlayerPawnEntity&)*m_penPlayer).en_vGravityDir*0.5f;
            SprayParticlesType sptType=SPT_NONE;
            COLOR colParticles=C_WHITE|CT_OPAQUE;
            FLOAT fPower=4.0f;
            if (IsOfClass(crRay.cr_penHit, &CModelHolder2Entity_DLLClass))
            {
              bRender=FALSE;
              CModelDestructionEntity *penDestruction = ((CModelHolder2Entity&)*crRay.cr_penHit).GetDestruction();
              if (penDestruction!=NULL)
              {
                bRender=TRUE;
                sptType= penDestruction->m_sptType;
              }
              CModelHolder2Entity *pmh2=(CModelHolder2Entity*)crRay.cr_penHit;
              colParticles=pmh2->m_colBurning;
            }
            FLOATaabbox3D boxCutted=FLOATaabbox3D(FLOAT3D(0, 0, 0),FLOAT3D(1,1,1));
            if (bRender)
            {
              crRay.cr_penHit->en_pmoModelObject->GetCurrentFrameBBox(boxCutted);
              ((CPlayerPawnEntity&)*m_penPlayer).AddGoreSpray(vBase+vFront, vHit, sptType,
                vSpillDir, boxCutted, fPower, colParticles);
            }
          }

          // don't search any more
          break;
        }
      }
    }

    // if any model hit
    if (penClosest!=NULL) {
      // in deathmatches check for backstab
      if (!(GetSP()->sp_bCooperative) && penClosest->IsPlayerControlled()) {
        FLOAT3D vToTarget = penClosest->GetPlacement().pl_PositionVector - m_penPlayer->GetPlacement().pl_PositionVector;
        FLOAT3D vTargetHeading = FLOAT3D(0.0, 0.0, -1.0f)*penClosest->GetRotationMatrix();
        vToTarget.Normalize(); vTargetHeading.Normalize();
        if (vToTarget%vTargetHeading>0.64279) //CosFast(50.0f)
        {
          PrintCenterMessage(this, m_penPlayer, TRANS("Backstab!"), 4.0f, MSS_NONE);
          fDamage *= 4.0f;
        }
      }

      const FLOAT fDamageMul = GetSeriousDamageMultiplier(m_penPlayer);
      InflictDirectDamage(penClosest, m_penPlayer, DMT_CLOSERANGE, fDamage*fDamageMul, vHit, vDir);
      return TRUE;
    }

    return FALSE;
  };


  // cut in front of you with the chainsaw
  BOOL CutWithChainsaw(FLOAT fX, FLOAT fY, FLOAT fRange, FLOAT fWide, FLOAT fThickness, INDEX iDamageAmount)
  {
    CPlayerPawnEntity *penPlayer = GetPlayer();
    
    // knife start position
    CPlacement3D plKnife;
    CalcWeaponPosition(FLOAT3D(fX, fY, 0), plKnife, TRUE);

    // create a set of rays to test
    const FLOAT3D &vBase = plKnife.pl_PositionVector;
    FLOATmatrix3D m;
    MakeRotationMatrixFast(m, plKnife.pl_OrientationAngle);
    FLOAT3D vRight = m.GetColumn(1) * fWide;
    FLOAT3D vUp    = m.GetColumn(2) * fWide;
    FLOAT3D vFront = -m.GetColumn(3) * fRange;

    FLOAT3D vDest[3];
    vDest[0] = vBase + vFront;
    vDest[1] = vBase + vFront + vRight;
    vDest[2] = vBase + vFront - vRight;

    CEntity *penClosest = NULL;
    FLOAT fDistance = UpperLimit(0.0f);
    FLOAT3D vHit;
    FLOAT3D vDir;

    // for each ray
    for (INDEX i = 0; i < 3; i++)
    {
      // cast a ray to find if any model
      CCastRay crRay(m_penPlayer, vBase, vDest[i]);
      crRay.cr_bHitTranslucentPortals = FALSE;
      crRay.cr_fTestR = fThickness;
      crRay.cr_ttHitModels = CCastRay::TT_COLLISIONBOX;
      GetWorld()->CastRay(crRay);

      // if hit something
      if (crRay.cr_penHit == NULL) {
        // because we're firing, add just a slight shake
        penPlayer->m_fChainShakeStrength = 0.1f;
        penPlayer->m_fChainShakeFreqMod = 1.0f;
        penPlayer->m_tmChainShakeEnd = _pTimer->CurrentTick() + CHAINSAW_UPDATETIME * 1.5f;
        continue;
      }

      penClosest = crRay.cr_penHit;
      fDistance = crRay.cr_fHitDistance;
      vDir = vDest[i] - vBase;
      vDir.Normalize();
      vHit = crRay.cr_vHit;

      if (i == 0)
      {
        if (crRay.cr_penHit->GetRenderType() == RT_BRUSH)
        {
          INDEX iSurfaceType = crRay.cr_pbpoBrushPolygon->bpo_bppProperties.bpp_ubSurfaceType;
          EffectParticlesType eptType = GetParticleEffectTypeForSurface(iSurfaceType);

          FLOAT3D vNormal = crRay.cr_pbpoBrushPolygon->bpo_pbplPlane->bpl_plAbsolute;
          FLOAT3D vReflected = vDir - vNormal * (2.0f * (vNormal % vDir));
          penPlayer->AddBulletSpray(vBase+vFront, eptType, vReflected);

          // shake view
          penPlayer->m_fChainShakeStrength = 0.85f;
          penPlayer->m_fChainShakeFreqMod = 1.0f;
          penPlayer->m_tmChainShakeEnd = _pTimer->CurrentTick() + CHAINSAW_UPDATETIME*1.5f;

        }
        else if (crRay.cr_penHit->GetRenderType() == RT_MODEL)
        {
          BOOL bRender = TRUE;
          FLOAT3D vSpillDir = -penPlayer->en_vGravityDir * 0.5f;
          SprayParticlesType sptType = SPT_NONE;
          COLOR colParticles = C_WHITE|CT_OPAQUE;
          FLOAT fPower = 4.0f;
          
          if (IsDerivedFromClass(crRay.cr_penHit, &CEnemyBaseEntity_DLLClass)) {
            CEnemyBaseEntity *penEB = static_cast<CEnemyBaseEntity *>(crRay.cr_penHit);
            
            sptType = penEB->m_sptType;
            
            if (IsOfClass(crRay.cr_penHit, "Boneman") || IsOfClass(crRay.cr_penHit, "AirElemental")) {
              fPower = 6.0f;
            }

            if (IsOfClass(crRay.cr_penHit, "Woman") || IsOfClass(crRay.cr_penHit, "Elemental")) {
              fPower = 3.0f;
            }

            if (IsOfClass(crRay.cr_penHit, "Walker")) {
              fPower = 30.0f;
            }
          }
          

          if (IsOfClass(crRay.cr_penHit, &CModelHolder2Entity_DLLClass))
          {
            bRender = FALSE;
            CModelHolder2Entity *pmh2 = static_cast<CModelHolder2Entity *>(crRay.cr_penHit);
            CModelDestructionEntity *penDestruction = pmh2->GetDestruction();
            colParticles = pmh2->m_colBurning;

            if (penDestruction != NULL)
            {
              bRender = TRUE;
              sptType = penDestruction->m_sptType;

              if (sptType == SPT_COLOREDSTONE)
              {
                colParticles = MulColors(colParticles, penDestruction->m_colParticles);
              }
            }
          }
          FLOATaabbox3D boxCutted = FLOATaabbox3D(FLOAT3D(0, 0, 0), FLOAT3D(1, 1, 1));
          if (bRender && m_tmLastChainsawSpray + 0.2f < _pTimer->CurrentTick())
          {
            crRay.cr_penHit->en_pmoModelObject->GetCurrentFrameBBox(boxCutted);
            penPlayer->AddGoreSpray(vBase + vFront, vHit, sptType, vSpillDir, boxCutted, fPower, colParticles);
            m_tmLastChainsawSpray = _pTimer->CurrentTick();
          }

          // shake view
          penPlayer->m_fChainShakeStrength = 1.1f;
          penPlayer->m_fChainShakeFreqMod = 1.0f;
          penPlayer->m_tmChainShakeEnd = _pTimer->CurrentTick() + CHAINSAW_UPDATETIME * 1.5f;
        }
      }

      if (crRay.cr_penHit->GetRenderType() == RT_MODEL && crRay.cr_fHitDistance <= fDistance)
      {
        // if this is primary ray then don't search any more
        if (i == 0) {
          break;
        }
      }
    }

    // if any model hit
    if (penClosest != NULL) {
      InflictDirectDamage(penClosest, m_penPlayer, DMT_CHAINSAW, iDamageAmount, vHit, vDir);
      return TRUE;
    }

    return FALSE;
  };

  // prepare Bullet
  void PrepareSniperBullet(FLOAT fX, FLOAT fY, INDEX iDamage, FLOAT fImprecission)
  {
    // bullet start position
    CalcWeaponPositionImprecise(FLOAT3D(fX, fY, 0), plBullet, TRUE, fImprecission);

    // create bullet
    penBullet = CreateEntity(plBullet, CLASS_BULLET);
    m_vBulletSource = plBullet.pl_PositionVector;

	// init bullet
    EBulletInit eInit;
    eInit.penOwner = m_penPlayer;
    eInit.iDamage = iDamage;
    penBullet->Initialize(eInit);
  };

  // prepare Bullet
  void PrepareBullet(FLOAT fX, FLOAT fY, INDEX iDamage)
  {
    // bullet start position
    CalcWeaponPosition(FLOAT3D(fX, fY, 0), plBullet, TRUE);

    // create bullet
    penBullet = CreateEntity(plBullet, CLASS_BULLET);

    // init bullet
    EBulletInit eInit;
    eInit.penOwner = m_penPlayer;
    eInit.iDamage = iDamage;
    penBullet->Initialize(eInit);
  };

  // fire one bullet
  void FireSniperBullet(FLOAT fX, FLOAT fY, FLOAT fRange, INDEX iDamage, FLOAT fImprecission)
  {
    PrepareSniperBullet(fX, fY, iDamage, fImprecission);
    ((CBulletEntity&)*penBullet).CalcTarget(fRange);
    ((CBulletEntity&)*penBullet).m_fBulletSize = 0.1f;

    // launch bullet
    ((CBulletEntity&)*penBullet).LaunchBullet(TRUE, FALSE, TRUE);

    if (((CBulletEntity&)*penBullet).m_vHitPoint != FLOAT3D(0.0f, 0.0f, 0.0f)) {
      m_vBulletTarget = ((CBulletEntity&)*penBullet).m_vHitPoint;
    } else if (TRUE) {
      m_vBulletTarget = m_vBulletSource + FLOAT3D(0.0f, 0.0f, -500.0f)*((CBulletEntity&)*penBullet).GetRotationMatrix();
    }

    // spawn bullet effect
    /*ESpawnEffect ese;
    ese.colMuliplier = C_WHITE|CT_OPAQUE;
    ese.betType = BET_SNIPER_RESIDUE;
    ese.vStretch = FLOAT3D(1.0f, 1.0f, 1.0f);
    ese.vNormal = m_vBulletSource;
    ese.vDirection = ((CBulletEntity&)*penBullet).m_vHitPoint;
    CPlacement3D pl = CPlacement3D(GetPlacement().pl_PositionVector, ANGLE3D(0.0f, 0.0f, 0.0f));
    CEntityPointer penFX = CreateEntity(pl, CLASS_BASIC_EFFECT);
    penFX->Initialize(ese);*/

	  // bullet no longer needed
	  ((CBulletEntity&)*penBullet).DestroyBullet();
  };

  // fire one bullet
  void FireOneBullet(FLOAT fX, FLOAT fY, FLOAT fRange, INDEX iDamage)
  {
    PrepareBullet(fX, fY, iDamage);
    ((CBulletEntity&)*penBullet).CalcTarget(fRange);
    ((CBulletEntity&)*penBullet).m_fBulletSize = 0.1f;

    // launch bullet
    ((CBulletEntity&)*penBullet).LaunchBullet(TRUE, FALSE, TRUE);
    ((CBulletEntity&)*penBullet).DestroyBullet();
  };

  // fire bullets (x offset is used for double shotgun)
  void FireBullets(FLOAT fX, FLOAT fY, FLOAT fRange, INDEX iDamage, INDEX iBullets, FLOAT *afPositions, FLOAT fStretch, FLOAT fJitter)
  {
    PrepareBullet(fX, fY, iDamage);
    ((CBulletEntity&)*penBullet).CalcTarget(fRange);
    ((CBulletEntity&)*penBullet).m_fBulletSize = GetSP()->sp_bCooperative ? 0.1f : 0.3f;

    // launch slugs
    INDEX iSlug;

    for (iSlug=0; iSlug<iBullets; iSlug++)
    {
      // launch bullet
      ((CBulletEntity&)*penBullet).CalcJitterTargetFixed(
        afPositions[iSlug*2+0]*fRange*fStretch, afPositions[iSlug*2+1]*fRange*fStretch,
        fJitter*fRange*fStretch);
      ((CBulletEntity&)*penBullet).LaunchBullet(iSlug<2, FALSE, TRUE);
    }

    ((CBulletEntity&)*penBullet).DestroyBullet();
  };

  // fire one bullet for machine guns (tommygun and chaingun)
  void FireMachineBullet(FLOAT fX, FLOAT fY, FLOAT fRange, INDEX iDamage,
    FLOAT fJitter, FLOAT fBulletSize)
  {
    fJitter*=fRange;  // jitter relative to range
    PrepareBullet(fX, fY, iDamage);
    ((CBulletEntity&)*penBullet).CalcTarget(fRange);
    ((CBulletEntity&)*penBullet).m_fBulletSize = fBulletSize;
    ((CBulletEntity&)*penBullet).CalcJitterTarget(fJitter);
    ((CBulletEntity&)*penBullet).LaunchBullet(TRUE, FALSE, TRUE);
    ((CBulletEntity&)*penBullet).DestroyBullet();
  }

  // fire grenade
  void FireGrenade(INDEX iPower)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    // grenade start position
    CPlacement3D plGrenade;
    CalcWeaponPosition(
      FLOAT3D(pPosParams->GetFirePos()(1),pPosParams->GetFirePos()(2), 0),
      plGrenade, TRUE);

    // create grenade
    CEntityPointer penGrenade = CreateEntity(plGrenade, CLASS_PROJECTILE);

    // init and launch grenade
    ELaunchProjectile eLaunch;
    eLaunch.penLauncher = m_penPlayer;
    eLaunch.prtType = PRT_GRENADE;
    eLaunch.fSpeed = 20.0f+iPower*5.0f;
    penGrenade->Initialize(eLaunch);
  };

  // fire rocket
  void FireRocket(void)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    // rocket start position
    CPlacement3D plRocket;
    CalcWeaponPosition(
      FLOAT3D(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2), 0),
      plRocket, TRUE);

    // create rocket
    CEntityPointer penRocket = CreateEntity(plRocket, CLASS_PROJECTILE);

    // init and launch rocket
    ELaunchProjectile eLaunch;
    eLaunch.penLauncher = m_penPlayer;
    eLaunch.prtType = PRT_ROCKET;
    penRocket->Initialize(eLaunch);
  };

  // flamer source
  void GetFlamerSourcePlacement(CPlacement3D &plSource, CPlacement3D &plInFrontOfPipe)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    CalcLerpedWeaponPosition(
      FLOAT3D(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2), -0.15f),
      plSource, FALSE);
    plInFrontOfPipe=plSource;
    FLOAT3D vFront;
    AnglesToDirectionVector(plSource.pl_OrientationAngle, vFront);
    plInFrontOfPipe.pl_PositionVector=plSource.pl_PositionVector+vFront*1.0f;
  };

  // fire flame
  void FireFlame(void)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    // flame start position
    CPlacement3D plFlame;

    CalcWeaponPosition(FLOAT3D(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2), -0.15f), plFlame, TRUE);

    // create flame
    CEntityPointer penFlame = CreateEntity(plFlame, CLASS_PROJECTILE);

    // init and launch flame
    ELaunchProjectile eLaunch;
    eLaunch.penLauncher = m_penPlayer;
    eLaunch.prtType = PRT_FLAME;
    penFlame->Initialize(eLaunch);

    // link last flame with this one (if not NULL or deleted)
    if (m_penFlame!=NULL && !(m_penFlame->GetFlags()&ENF_DELETED)) {
      ((CProjectileEntity&)*m_penFlame).m_penParticles = penFlame;
    }

    // link to player weapons
    ((CProjectileEntity&)*penFlame).m_penParticles = this;

    // store last flame
    m_penFlame = penFlame;
  };

  // fire laser ray
  void FirePlasmaProjectile(enum ProjectileType eProjectileType)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    // laser start position
    CPlacement3D plLaserRay;
    FLOAT fFX = pPosParams->GetFirePos()(1);  // get laser center position
    FLOAT fFY = pPosParams->GetFirePos()(2);

    if (GetHand() != E_HAND_MAIN) {
      fFX = -fFX;
    }

    // [Cecil] Choose horizontal size depending on weapon
    const FLOAT fSide = (m_eHand != E_HAND_MAIN ? -1.0f : 1.0f);

    FLOAT fUpX = 0.25f * fSide;
    FLOAT fUpY = 0.15f;

    FLOAT fDnX = 0.3f * fSide;
    FLOAT fDnY = 0.15f;

    if (GetPlayer()->m_pstState == PST_CROUCH) {
      fUpY = -0.25f;
      fDnY = -0.25f;
    }

    // [Cecil] Relative laser ray position
    FLOAT3D vFire;

    switch (m_iLaserBarrel) {
      // barrel lu (*o-oo)
      case 0: vFire = FLOAT3D(fFX-fUpX, fFY+fUpY, 0.0f); break;

      // barrel ld (oo-*o)
      case 1: vFire = FLOAT3D(fFX-fDnX, fFY-fDnY, 0.0f); break;

      // barrel ru (o*-oo)
      case 2: vFire = FLOAT3D(fFX+fUpX, fFY+fUpY, 0.0f); break;

      // barrel rd (oo-o*)
      case 3: vFire = FLOAT3D(fFX+fDnX, fFY-fDnY, 0.0f); break;
    }

    CalcWeaponPosition(vFire, plLaserRay, TRUE);

    // create laser projectile
    CEntityPointer penLaser = CreateEntity(plLaserRay, CLASS_PROJECTILE);

    // init and launch laser projectile
    ELaunchProjectile eLaunch;
    eLaunch.penLauncher = m_penPlayer;
    eLaunch.prtType = eProjectileType;
    penLaser->Initialize(eLaunch);
  };

  void PlasmathrowerBarrelAnim()
  {
    // activate barrel anim
    switch (m_iLaserBarrel)
    {
      case 0: {   // barrel lu
        CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(LASER_ATTACHMENT_LEFTUP)->amo_moModelObject);
        pmo->PlayAnim(BARREL_ANIM_FIRE, 0);
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_PLASMATHROWER_FIRE, SOF_3D|SOF_VOLUMETRIC);
        break; }
      case 1: {   // barrel ld
        CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(LASER_ATTACHMENT_LEFTDOWN)->amo_moModelObject);
        pmo->PlayAnim(BARREL_ANIM_FIRE, 0);
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_2), SOUND_PLASMATHROWER_FIRE, SOF_3D|SOF_VOLUMETRIC);
        break; }
      case 2: {   // barrel ru
        CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(LASER_ATTACHMENT_RIGHTUP)->amo_moModelObject);
        pmo->PlayAnim(BARREL_ANIM_FIRE, 0);
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_3), SOUND_PLASMATHROWER_FIRE, SOF_3D|SOF_VOLUMETRIC);
        break; }
    }
  }
  
  // [SSE] Beamgun
  // beamgun source
  void GetBeamgunSourcePlacement(CPlacement3D &plSource)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());
    
    CalcWeaponPosition(
      FLOAT3D(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2), 0), 
      plSource, TRUE);
  };
  
  // destroy ray
  void DestroyEnergyBeam()
  {
    if (m_penEnergyBeam != NULL) {
      GetEnergyBeam()->DestroyEnergyBeam();   
    }
  }

  // fire ghost buster ray
  void FireEnergyBeam(void)
  {
    // ray start position
    CPlacement3D plRay;
    GetBeamgunSourcePlacement(plRay);

    // fire ray
    GetEnergyBeam()->Fire(plRay);
  };

  // fire cannon ball
  void FireCannonBall(INDEX iPower)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon, GetPlayer()->IsDualWielding());

    // cannon ball start position
    CPlacement3D plBall;
    CalcWeaponPosition(
      FLOAT3D(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2), 0),
      plBall, TRUE);

    // create cannon ball
    CEntityPointer penBall = CreateEntity(plBall, CLASS_CANNONBALL);

    // init and launch cannon ball
    ELaunchBallProjectile eLaunch;
    eLaunch.penLauncher = m_penPlayer;
    eLaunch.fLaunchPower = 60.0f+iPower*4.0f; // ranges from 60-140 (since iPower can be max 20)
    eLaunch.fSize = 3.0f;
    eLaunch.bptType = BPT_IRON;

    penBall->Initialize(eLaunch);
  };

  // weapon sound when firing
  void SpawnRangeSound(FLOAT fRange)
  {
    if (_pTimer->CurrentTick()>m_tmRangeSoundSpawned+0.5f) {
      m_tmRangeSoundSpawned = _pTimer->CurrentTick();
      ::SpawnRangeSound(m_penPlayer, m_penPlayer, SNDT_PLAYER, fRange);
    }
  };

  /*
   *  >>>---  WEAPON INTERFACE FUNCTIONS  ---<<<
   */

  void ResetWeaponMovingOffset(void)
  {
    // reset weapon draw offset
    m_fWeaponDrawPowerOld = m_fWeaponDrawPower = m_tmDrawStartTime = 0;
  }

  // initialize weapons
  void InitializeWeapons()
  {
    ResetWeaponMovingOffset();

    // precache eventual new weapons
    Precache();

    // clear temp variables for some weapons
    m_aChaingun = 0;
    m_aChaingunLast = 0;
    m_aChaingunSpeed = 0;

    // select weapon
    SetCurrentWeapon(E_WEAPON_NONE);
    SetWantedWeapon(E_WEAPON_NONE);

    if (GetHand() == E_HAND_MAIN) {
      SelectNewWeapon();
    }

    SetCurrentWeapon(GetWantedWeapon());
    wpn_iCurrent = GetCurrentWeapon();
    m_bChangeWeapon = FALSE;

    // set weapon model for current weapon
    SetWeaponModel();
    PlayDefaultAnim();

    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->RemoveWeapon();
      GetAnimator()->SetWeapon();
    }
  };

  INDEX GetAmmoDrain()
  {
    return GetWeaponParams(GetCurrentWeapon())->GetAmmoDrain();
  }

  void DrainAmmo()
  {
    const WeaponIndex eWeaponIndex = GetCurrentWeapon();
    const FWeaponParams *pWeaponParams = GetWeaponParams(eWeaponIndex);
    const AmmoIndex eAmmoIndex = pWeaponParams->GetDefaultAmmoType();
    const INDEX iQty = pWeaponParams->GetAmmoDrain();

    // If special code used?
    if (eAmmoIndex == E_AMMO_INVALID || iQty <= 0) {
      return;
    }

    GetInventory()->DrainAmmo(eAmmoIndex, iQty);
  }

  // get weapon ammo
  INDEX GetAmmoQty(void)
  {
    const WeaponIndex eWeaponIndex = GetCurrentWeapon();
    const AmmoIndex eAmmoIndex = GetWeaponParams(eWeaponIndex)->GetDefaultAmmoType();

    if (eWeaponIndex == E_WEAPON_REVOLVER) {
      return m_iColtBullets;
    }

    if (eAmmoIndex == E_AMMO_INVALID) {
      return 0;
    }

    return GetInventory()->GetAmmoQty(eAmmoIndex);
  };

  // get weapon max ammo (capacity)
  INDEX GetMaxAmmo(void)
  {
    const WeaponIndex eWeaponIndex = GetCurrentWeapon();
    const AmmoIndex eAmmoIndex = GetWeaponParams(eWeaponIndex)->GetDefaultAmmoType();

    if (eWeaponIndex == E_WEAPON_REVOLVER) {
      return 6;
    }

    if (eAmmoIndex == E_AMMO_INVALID) {
      return 0;
    }

    return GetInventory()->GetMaxAmmo(eAmmoIndex);
  };

  void CheatOpen(void)
  {
    if (IsOfClass(m_penRayHit, &CMovingBrushEntity_DLLClass)) {
      m_penRayHit->SendEvent(ETrigger());
    }
  }

  /*
   *  >>>---  RECEIVE FUNCTIONS  ---<<<
   */
  
  INDEX GetWeaponItemTypeForIndex(WeaponIndex eWeaponIndex)
  {
    switch (eWeaponIndex)
    {
      case E_WEAPON_KNIFE: return WIT_KNIFE;
      case E_WEAPON_CHAINSAW: return WIT_CHAINSAW;
      case E_WEAPON_REVOLVER: return WIT_COLT;
      case E_WEAPON_SHOTGUN: return WIT_SINGLESHOTGUN;
      case E_WEAPON_SUPERSHOTGUN: return WIT_DOUBLESHOTGUN;
      case E_WEAPON_MACHINEGUN: return WIT_TOMMYGUN;
      case E_WEAPON_CHAINGUN: return WIT_MINIGUN;
      case E_WEAPON_PLASMAGUN: return WIT_LASER;
      case E_WEAPON_FLAMER: return WIT_FLAMER;
      case E_WEAPON_ROCKETLAUNCHER: return WIT_ROCKETLAUNCHER;
      case E_WEAPON_GRENADELAUNCHER: return WIT_GRENADELAUNCHER;
      case E_WEAPON_SNIPER: return WIT_SNIPER;
      case E_WEAPON_CANNON: return WIT_CANNON;
    }
    
    return WIT_KNIFE;
  }

  // drop current weapon (in deathmatch)
  void DropWeapon(void)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    INDEX iType = GetWeaponItemTypeForIndex(eCurrentWeapon);
    
    if (iType == 0) {
      return;
    }
    
    CEntityPointer penWeapon = CreateEntity(GetPlayer()->GetPlacement(), CLASS_WEAPONITEM);
    CWeaponItemEntity *pwi = (CWeaponItemEntity*)&*penWeapon;
    pwi->m_EwitType = (WeaponItemType)iType;
    pwi->m_bDropped = TRUE;
    pwi->CEntity::Initialize();

    const FLOATmatrix3D &m = GetPlayer()->GetRotationMatrix();
    FLOAT3D vSpeed = FLOAT3D(5.0f, 10.0f, -7.5f);
    pwi->GiveImpulseTranslationAbsolute(vSpeed*m);
  }

  /*
   *  >>>---  WEAPON CHANGE FUNCTIONS  ---<<<
   */

  // get secondary weapon from selected one
  WeaponIndex GetAltWeapon(WeaponIndex eWeaponIndex)
  {
    switch (eWeaponIndex)
    {
      case E_WEAPON_KNIFE: return E_WEAPON_CHAINSAW;
      case E_WEAPON_CHAINSAW: return E_WEAPON_KNIFE;
      case E_WEAPON_REVOLVER: return E_WEAPON_REVOLVER;
      case E_WEAPON_SHOTGUN: return E_WEAPON_SUPERSHOTGUN;
      case E_WEAPON_SUPERSHOTGUN: return E_WEAPON_SHOTGUN;
      case E_WEAPON_MACHINEGUN: return E_WEAPON_CHAINGUN;
      case E_WEAPON_CHAINGUN: return E_WEAPON_MACHINEGUN;
      case E_WEAPON_ROCKETLAUNCHER: return E_WEAPON_GRENADELAUNCHER;
      case E_WEAPON_GRENADELAUNCHER: return E_WEAPON_ROCKETLAUNCHER;
      case E_WEAPON_FLAMER: return E_WEAPON_PLASMAGUN;
      case E_WEAPON_PLASMAGUN: return E_WEAPON_FLAMER;
      case E_WEAPON_SNIPER: return E_WEAPON_SNIPER;
      case E_WEAPON_CANNON: return E_WEAPON_CANNON;
    }

    return E_WEAPON_NONE;
  };

  // select new weapon if possible
  BOOL WeaponSelectOk(WeaponIndex eNewWeapon)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();

    // if player has weapon and has enough ammo
    if (HasWeapon(eNewWeapon) && HasEnoughAmmo(eNewWeapon)) {
      // if different weapon
      if (eNewWeapon != eCurrentWeapon) {
        // initiate change
        //m_bHasAmmo = FALSE;
        SetWantedWeapon(eNewWeapon);
        m_bChangeWeapon = TRUE;
      }
      // selection ok
      return TRUE;

    } else {
      return FALSE; // if no weapon or not enough ammo selection not ok
    }
  }

  // select new weapon when no more ammo
  void SelectNewWeapon()
  {
    switch (GetCurrentWeapon())
    {
      case E_WEAPON_NONE:
      case E_WEAPON_KNIFE: case E_WEAPON_REVOLVER:
      case E_WEAPON_SHOTGUN: case E_WEAPON_SUPERSHOTGUN:
      case E_WEAPON_MACHINEGUN: case E_WEAPON_CHAINGUN: case E_WEAPON_SNIPER:
        WeaponSelectOk(E_WEAPON_CHAINGUN)||
        WeaponSelectOk(E_WEAPON_MACHINEGUN)||
        WeaponSelectOk(E_WEAPON_SUPERSHOTGUN)||
        WeaponSelectOk(E_WEAPON_SHOTGUN)||
        WeaponSelectOk(E_WEAPON_REVOLVER)||
        WeaponSelectOk(E_WEAPON_KNIFE);
        break;

      case E_WEAPON_CANNON:
        WeaponSelectOk(E_WEAPON_ROCKETLAUNCHER)||
        WeaponSelectOk(E_WEAPON_GRENADELAUNCHER)||
        WeaponSelectOk(E_WEAPON_CHAINGUN)||
        WeaponSelectOk(E_WEAPON_MACHINEGUN)||
        WeaponSelectOk(E_WEAPON_SUPERSHOTGUN)||
        WeaponSelectOk(E_WEAPON_SHOTGUN)||
        WeaponSelectOk(E_WEAPON_REVOLVER)||
        WeaponSelectOk(E_WEAPON_KNIFE);
        break;

      case E_WEAPON_MINELAYER:
      case E_WEAPON_ROCKETLAUNCHER:
      case E_WEAPON_GRENADELAUNCHER:
        WeaponSelectOk(E_WEAPON_ROCKETLAUNCHER)||
        WeaponSelectOk(E_WEAPON_GRENADELAUNCHER)||
        WeaponSelectOk(E_WEAPON_CHAINGUN)||
        WeaponSelectOk(E_WEAPON_MACHINEGUN)||
        WeaponSelectOk(E_WEAPON_SUPERSHOTGUN)||
        WeaponSelectOk(E_WEAPON_SHOTGUN)||
        WeaponSelectOk(E_WEAPON_REVOLVER)||
        WeaponSelectOk(E_WEAPON_KNIFE);
        break;

      case E_WEAPON_PLASMAGUN: case E_WEAPON_FLAMER: case E_WEAPON_CHAINSAW: case E_WEAPON_BEAMGUN: case E_WEAPON_PLASMATHROWER:
        WeaponSelectOk(E_WEAPON_PLASMATHROWER)||
        WeaponSelectOk(E_WEAPON_BEAMGUN)||
        WeaponSelectOk(E_WEAPON_PLASMAGUN)||
        WeaponSelectOk(E_WEAPON_FLAMER)||
        WeaponSelectOk(E_WEAPON_CHAINGUN)||
        WeaponSelectOk(E_WEAPON_MACHINEGUN)||
        WeaponSelectOk(E_WEAPON_SUPERSHOTGUN)||
        WeaponSelectOk(E_WEAPON_SHOTGUN)||
        WeaponSelectOk(E_WEAPON_REVOLVER)||
        WeaponSelectOk(E_WEAPON_KNIFE);
        break;

      default:
        WeaponSelectOk(E_WEAPON_KNIFE);
        ASSERT(FALSE);
    }
  };

  // does weapon have ammo
  BOOL HasAmmo(WeaponIndex eWeaponIndex)
  {
    AmmoIndex eAmmoIndex = GetWeaponParams(eWeaponIndex)->GetDefaultAmmoType();

    if (eAmmoIndex == E_AMMO_INVALID) {
      return TRUE;
    }

    return GetInventory()->GetAmmoQty(eAmmoIndex) > 0;
  };

  // check if weapon has enough ammo to fire
  BOOL HasEnoughAmmo(WeaponIndex eWeaponIndex)
  {
    const FWeaponParams *pWeaponParams = GetWeaponParams(eWeaponIndex);
    const AmmoIndex eAmmoIndex = pWeaponParams->GetDefaultAmmoType();
    const INDEX iAmmoDrain = pWeaponParams->GetAmmoDrain();

    if (eAmmoIndex == E_AMMO_INVALID) {
      return TRUE;
    }

    return GetInventory()->GetAmmoQty(eAmmoIndex) >= iAmmoDrain;
  }

  BOOL HasEnoughAmmo()
  {
    return HasEnoughAmmo(GetCurrentWeapon());
  }

  BOOL HasWeapon(WeaponIndex eWeaponIndex)
  {
    return GetInventory()->HasWeapon(eWeaponIndex);
  }

  WeaponIndex GetCurrentWeapon() const
  {
    return GetInventory()->GetCurrentWeapon(GetHand());
  }

  void SetCurrentWeapon(WeaponIndex eCurrentWeapon)
  {
    GetInventory()->SetCurrentWeapon(GetHand(), eCurrentWeapon);
  }
  
  WeaponIndex GetWantedWeapon()
  {
    return GetInventory()->GetWantedWeapon(GetHand());
  }

  void SetWantedWeapon(WeaponIndex eWantedWeapon)
  {
    GetInventory()->SetWantedWeapon(GetHand(), eWantedWeapon);
  }

  /*
   *  >>>---   DEFAULT ANIM   ---<<<
   */
  void PlayDefaultAnim(void)
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const INDEX idDefaultAnimation = GetDefaultAnim(eCurrentWeapon);

    if (eCurrentWeapon == E_WEAPON_NONE || idDefaultAnimation == -1) {
      return;
    }

    m_moWeapon.PlayAnim(idDefaultAnimation, AOF_LOOPING|AOF_NORESTART|AOF_SMOOTHCHANGE);
  };

  /*
   *  >>>---   WEAPON BORING   ---<<<
   */
  FLOAT KnifeBoring(void)
  {
    INDEX iAnim = KNIFE_ANIM_WAIT1;

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT RevolverBoring(void)
  {
    INDEX iAnim;

    switch (IRnd()%2)
    {
      case 0: iAnim = COLT_ANIM_WAIT3; break;
      case 1: iAnim = COLT_ANIM_WAIT4; break;
    }

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT SingleShotgunBoring(void)
  {
    // play boring anim
    INDEX iAnim;

    switch (IRnd()%2)
    {
      case 0: iAnim = SINGLESHOTGUN_ANIM_WAIT2; break;
      case 1: iAnim = SINGLESHOTGUN_ANIM_WAIT3; break;
    }

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE);
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT SuperShotgunBoring(void)
  {
    INDEX iAnim;

    switch (IRnd()%3)
    {
      case 0: iAnim = DOUBLESHOTGUN_ANIM_WAIT2; break;
      case 1: iAnim = DOUBLESHOTGUN_ANIM_WAIT3; break;
      case 2: iAnim = DOUBLESHOTGUN_ANIM_WAIT4; break;
    }

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT TommyGunBoring(void)
  {
    INDEX iAnim;

    switch (IRnd()%2)
    {
      case 0: iAnim = TOMMYGUN_ANIM_WAIT2; break;
      case 1: iAnim = TOMMYGUN_ANIM_WAIT3; break;
    }

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT SniperBoring(void)
  {
    INDEX iAnim;
    iAnim = SNIPER_ANIM_WAIT01;
    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT ChaingunBoring(void)
  {
    INDEX iAnim;

    switch (IRnd()%3)
    {
      case 0: iAnim = MINIGUN_ANIM_WAIT2; break;
      case 1: iAnim = MINIGUN_ANIM_WAIT3; break;
      case 2: iAnim = MINIGUN_ANIM_WAIT4; break;
    }

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT RocketLauncherBoring(void)
  {
    m_moWeapon.PlayAnim(ROCKETLAUNCHER_ANIM_WAIT2, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(ROCKETLAUNCHER_ANIM_WAIT2);
  };

  FLOAT GrenadeLauncherBoring(void)
  {
    m_moWeapon.PlayAnim(GRENADELAUNCHER_ANIM_WAIT2, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(GRENADELAUNCHER_ANIM_WAIT2);
  };

  FLOAT FlamerBoring(void)
  {
    INDEX iAnim;

    switch (IRnd()%4)
    {
      case 0: iAnim = FLAMER_ANIM_WAIT02; break;
      case 1: iAnim = FLAMER_ANIM_WAIT03; break;
      case 2: iAnim = FLAMER_ANIM_WAIT04; break;
      case 3: iAnim = FLAMER_ANIM_WAIT05; break;
    }

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT ChainsawBoring(void)
  {
    INDEX iAnim;

    switch (IRnd()%3)
    {
      case 0: iAnim = CHAINSAW_ANIM_WAIT2; break;
      case 1: iAnim = CHAINSAW_ANIM_WAIT3; break;
      case 2: iAnim = CHAINSAW_ANIM_WAIT4; break;
    }

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE); // play boring anim
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT PlasmagunBoring(void)
  {
    // play boring anim
    INDEX iAnim;
    iAnim = LASER_ANIM_WAIT02;
    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE);
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT CannonBoring(void)
  {
    // play boring anim
    INDEX iAnim;

    switch (IRnd()%3)
    {
      case 0: iAnim = CANNON_ANIM_WAIT02; break;
      case 1: iAnim = CANNON_ANIM_WAIT03; break;
      case 2: iAnim = CANNON_ANIM_WAIT04; break;
    }

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE);
    return m_moWeapon.GetAnimLength(iAnim);
  };

  FLOAT BeamgunBoring(void)
  {
    // play boring anim
    INDEX iAnim;
    switch (IRnd()%3)
    {
      case 0: iAnim = BEAMGUN_ANIM_WAIT01; break;
      case 1: iAnim = BEAMGUN_ANIM_WAIT02; break;
      case 2: iAnim = BEAMGUN_ANIM_WAIT04; break;
    }

    m_moWeapon.PlayAnim(iAnim, AOF_SMOOTHCHANGE);
    return m_moWeapon.GetAnimLength(iAnim);
  };

  // find the weapon position in the remap array
  WeaponIndex FindRemapedPos(WeaponIndex eWeaponIndex)
  {
    for (INDEX i = 0; i < E_WEAPON_MAX; i++)
    {
      if (aiWeaponsRemap[i] == eWeaponIndex) {
        return (WeaponIndex)i;
      }
    }

    ASSERT("Non-existant weapon in remap array!");
    return (WeaponIndex)0;
  }


  /*
   *  >>>---   WEAPON CHANGE FUNCTIONS   ---<<<
   */
   
  WeaponIndex GetStrongestInSlot(INDEX iSlot)
  {
    WeaponIndex eResultIndex = E_WEAPON_NONE;
    
    for (INDEX iWeapon = E_WEAPON_NONE + 1; iWeapon < E_WEAPON_MAX; iWeapon++)
    {
      WeaponIndex eWeaponIndex = (WeaponIndex)iWeapon;
      
      if (GetWeaponParams(eWeaponIndex)->GetSelectionSlot() != iSlot) {
        continue;
      }
      
      if (!HasWeapon(eWeaponIndex) || !HasEnoughAmmo(eWeaponIndex)) {
        continue;
      }
      
      // Solo weapons available only for main hand.
      if (GetHand() == E_HAND_SECOND && GetWeaponParams(eWeaponIndex)->IsSolo()) {
        continue;
      }

      if (GetWeaponParams(eWeaponIndex)->GetSelectionValue() > GetWeaponParams(eResultIndex)->GetSelectionValue()) {
        eResultIndex = eWeaponIndex;
      }
    }
    
    return eResultIndex;
  }
  
  WeaponIndex FindWeakerInSlot(INDEX iSlot)
  {
    if (!GetInventory()->HasAnyWeapon()) {
      return E_WEAPON_NONE;
    }

    WeaponIndex eResultIndex = E_WEAPON_NONE;
    INDEX iSourceValue = GetWeaponParams(GetWantedWeapon())->GetSelectionValue();

    for (INDEX iWeapon = E_WEAPON_NONE + 1; iWeapon < E_WEAPON_MAX; iWeapon++)
    {
      WeaponIndex eWeaponIndex = (WeaponIndex)iWeapon;

      // Ignore same weapon.
      if (eWeaponIndex == GetWantedWeapon()) {
        continue;
      }
      
      // Ignore weapons from another slots.
      if (GetWeaponParams(eWeaponIndex)->GetSelectionSlot() != iSlot) {
        continue;
      }
      
      if (!HasWeapon(eWeaponIndex) || !HasEnoughAmmo(eWeaponIndex)) {
        continue;
      }
      
      // Solo weapons available only for main hand.
      if (GetHand() == E_HAND_SECOND && GetWeaponParams(eWeaponIndex)->IsSolo()) {
        continue;
      }

      INDEX iFoundValue = GetWeaponParams(eWeaponIndex)->GetSelectionValue();

      // Ignore stronger weapons.
      if (iFoundValue > iSourceValue) {
        continue;
      }
      
      // If found any weaker than source.
      if (eResultIndex == E_WEAPON_NONE && iFoundValue < iSourceValue) {
        eResultIndex = eWeaponIndex;
        continue;
      }
      
      // If we  have something found and found stronger than found.
      if (eResultIndex != E_WEAPON_NONE && iFoundValue > GetWeaponParams(eResultIndex)->GetSelectionValue()) {
        eResultIndex = eWeaponIndex;
      }
    }
    
    return eResultIndex == E_WEAPON_NONE ? GetWantedWeapon() : eResultIndex;
  }

  WeaponIndex FindWeaponInDirection(INDEX iDir)
  {
    if (!GetInventory()->HasAnyWeapon()) {
      return E_WEAPON_NONE;
    }
    
    INDEX wtOrg = FindRemapedPos(GetWantedWeapon());
    INDEX wti = wtOrg;

    FOREVER
    {
      (INDEX&)wti += iDir;
      if (wti < E_WEAPON_NONE + 1) {
        wti = E_WEAPON_CANNON;
      }

      if (wti>14) {
        wti = E_WEAPON_KNIFE;
      }

      if (wti==wtOrg) {
        break;
      }

      WeaponIndex eWeaponIndex = (WeaponIndex) aiWeaponsRemap[wti];

      // Solo weapons available only for main hand.
      if (GetHand() == E_HAND_SECOND && GetWeaponParams(eWeaponIndex)->IsSolo()) {
        continue;
      }

      if (HasWeapon(eWeaponIndex) && HasEnoughAmmo(eWeaponIndex)) {
        return eWeaponIndex;
      }
    }

    return GetWantedWeapon();
  }

  // select new weapon
  void SelectWeaponChange(INDEX iSelect)
  {
    WeaponIndex eTempIndex;

    // mark that weapon change is required
    m_tmWeaponChangeRequired = _pTimer->CurrentTick();

    // if storing current weapon
    if (iSelect == 0) {
      m_bChangeWeapon = TRUE;
      SetWantedWeapon(E_WEAPON_NONE);
      return;
    }

    // Copy from main hand.
    if (iSelect == -5 && GetHand() == E_HAND_SECOND) {  
      WeaponIndex eAtMainHand = GetPlayer()->GetWeapons(E_HAND_MAIN)->GetCurrentWeapon();
      SetWantedWeapon(eAtMainHand);
      m_bChangeWeapon = TRUE;
      return;
    }

    // if restoring best weapon
    if (iSelect==-4) {
      SelectNewWeapon() ;
      return;
    }

    if (iSelect > 0 && iSelect & SELECT_WEAPON_DIRECT) {
      eTempIndex = WeaponIndex(iSelect & ~SELECT_WEAPON_DIRECT);
    
    // if flipping weapon
    } else if (iSelect==-3) {
      eTempIndex = GetAltWeapon(GetWantedWeapon());

    // if selecting previous weapon
    } else if (iSelect==-2) {
      eTempIndex = FindWeaponInDirection(-1);

    // if selecting next weapon
    } else if (iSelect==-1) {
      eTempIndex = FindWeaponInDirection(+1);

    // if selecting directly
    } else {
      // flip current weapon
      if (iSelect == GetWeaponParams(GetWantedWeapon())->GetSelectionSlot()) {
        eTempIndex = FindWeakerInSlot(iSelect);
        
        // Cycle.
        if (eTempIndex == GetWantedWeapon()) {
          eTempIndex = GetStrongestInSlot(iSelect);
        }

      // change to wanted weapon
      } else {
        eTempIndex = GetStrongestInSlot(iSelect);

        // if weapon don't exist or don't have ammo flip it
        if (!HasWeapon(eTempIndex) || !HasEnoughAmmo(eTempIndex)) {
          eTempIndex = FindWeakerInSlot(iSelect);
        }
      }
    }

    // wanted weapon exist and has ammo
    BOOL bChange = HasWeapon(eTempIndex) && HasEnoughAmmo(eTempIndex);

    if (bChange) {
      if (GetHand() == E_HAND_MAIN && GetWeaponParams(eTempIndex)->IsSolo()) {
        GetPlayer()->SelectWeapon(E_HAND_SECOND, 0,FALSE); // Hid weapon.
      }
      
      SetWantedWeapon(eTempIndex);
      m_bChangeWeapon = TRUE;
    }
  };

  void ChaingunSmoke()
  {
    if (!hud_bShowWeapon) {
      return;
    }

    // smoke
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;

    if (pl.m_pstState!=PST_DIVE)
    {
      BOOL b3rdPersonView = TRUE;

      if (pl.m_penCamera==NULL && pl.m_pen3rdPersonView==NULL)
      {
        b3rdPersonView = FALSE;
      }

      AmmoIndex eAmmoIndex = GetWeaponParams(E_WEAPON_CHAINGUN)->GetDefaultAmmoType();
      INDEX ctBulletsFired = ClampUp(m_iBulletsOnFireStart-GetInventory()->GetAmmoQty(eAmmoIndex), INDEX(200));

      for (INDEX iSmoke=0; iSmoke<ctBulletsFired/10; iSmoke++)
      {
        ShellLaunchData *psldSmoke = &pl.m_asldData[pl.m_iFirstEmptySLD];
        CPlacement3D plPipe;

        if (b3rdPersonView)
        {
          CalcWeaponPosition3rdPersonView(FLOAT3D(afChaingunPipe3rdView[0],
            afChaingunPipe3rdView[1], afChaingunPipe3rdView[2]), plPipe, FALSE);
        } else {
          CalcWeaponPosition(FLOAT3D(afChaingunPipe[0], afChaingunPipe[1], afChaingunPipe[2]), plPipe, FALSE);
        }

        FLOATmatrix3D m;
        MakeRotationMatrixFast(m, plPipe.pl_OrientationAngle);
        psldSmoke->sld_vPos = plPipe.pl_PositionVector+pl.en_vCurrentTranslationAbsolute*iSmoke*_pTimer->TickQuantum;
        FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
        psldSmoke->sld_vUp = vUp;
        psldSmoke->sld_tmLaunch = _pTimer->CurrentTick()+iSmoke*_pTimer->TickQuantum;
        psldSmoke->sld_estType = ESL_BULLET_SMOKE;
        psldSmoke->sld_fSize = 0.75f+ctBulletsFired/50.0f;
        FLOAT3D vSpeedRelative = FLOAT3D(-0.06f, FRnd()/4.0f, -0.06f);
        psldSmoke->sld_vSpeed = vSpeedRelative*m+pl.en_vCurrentTranslationAbsolute;
        pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
      }
    }
  };

  BOOL SniperZoomDiscrete(INDEX iDirection, BOOL &bZoomChanged)
  {
    bZoomChanged = FALSE;

    // zoom in one zoom level
    if (iDirection>0) {
      for (INDEX i=0; i<iSniperDiscreteZoomLevels ; i++)
      {
        if (afSniperZoom[2*i]<m_fSniperFOV) {
          m_fSniperFOV=afSniperZoom[2*i];
          m_fSniperFOVlast=m_fSniperFOV;
          bZoomChanged = TRUE;
          break;
        }
      }

    // zoom out one zoom level
    } else {
      for (INDEX i=iSniperDiscreteZoomLevels ; i>0; i--)
      {
        if (afSniperZoom[2*i]>m_fSniperFOV) {
          m_fSniperFOV=afSniperZoom[2*i];
          m_fSniperFOVlast=m_fSniperFOV;
          bZoomChanged = TRUE;
          break;
        }
      }
    }

    if (m_fSniperFOV<90.0f) {
      m_bSniping = TRUE;
    } else {
      m_bSniping = FALSE;
    }

    return m_bSniping;
  };

procedures:
  /*
   *  >>>---   WEAPON CHANGE PROCEDURE  ---<<<
   */
  ChangeWeapon()
  {
    // if really changing weapon, make sure sniping is off and notify owner of the change
    if (GetCurrentWeapon() != GetWantedWeapon()) {
      m_fSniperFOV = m_fSniperFOVlast = m_fSniperMaxFOV;
      m_bSniping = FALSE;
      m_penPlayer->SendEvent(EWeaponChanged());
    }

    // weapon is changed
    m_bChangeWeapon = FALSE;

    // if this is not current weapon change it
    if (GetCurrentWeapon() != GetWantedWeapon()) {
      autocall PutDown() EEnd;

      // set new weapon
      SetCurrentWeapon(GetWantedWeapon());

      // remember current weapon for console usage
      wpn_iCurrent = GetCurrentWeapon();
      autocall BringUp() EEnd;

      // start engine sound if chainsaw
      if (GetCurrentWeapon() == E_WEAPON_CHAINSAW) {
        CSoundObject &so = GetWeaponSound(E_WEAPON_SOUND_AMBIENT);
        so.Set3DParameters(30.0f, 3.0f, 1.0f, 1.0f);
        PlaySound(so, SOUND_CS_IDLE, SOF_3D|SOF_VOLUMETRIC|SOF_LOOP|SOF_SMOOTHCHANGE);
        if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("ChainsawIdle");}
      }
    }

    jump Idle();
  };

  // put weapon down
  PutDown()
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();

    FLOAT tmAnimLength = _pTimer->TickQuantum; // wait at least tick

    // start weapon put down animation
    switch (eCurrentWeapon)
    {
      case E_WEAPON_NONE:
        break;

      case E_WEAPON_KNIFE:
        m_iAnim = KNIFE_ANIM_PULLOUT;
        tmAnimLength = KNIFE_ANIM_PULLOUT_LENGTH;
        break;
        
      case E_WEAPON_CHAINSAW: {
        CSoundObject &so = GetWeaponSound(E_WEAPON_SOUND_AMBIENT);
        PlaySound(so, SOUND_CS_BRINGDOWN, SOF_3D|SOF_VOLUMETRIC|SOF_SMOOTHCHANGE);
        if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_StopEffect("ChainsawIdle");}
        m_iAnim = CHAINSAW_ANIM_DEACTIVATE;
        tmAnimLength = CHAINSAW_ANIM_DEACTIVATE_LENGTH;
        break; }

      case E_WEAPON_REVOLVER:
        m_iAnim = COLT_ANIM_DEACTIVATE;
        tmAnimLength = COLT_ANIM_DEACTIVATE_LENGTH;
        break;

      case E_WEAPON_SHOTGUN:
        m_iAnim = SINGLESHOTGUN_ANIM_DEACTIVATE;
        tmAnimLength = SINGLESHOTGUN_ANIM_DEACTIVATE_LENGTH;
        break;

      case E_WEAPON_SUPERSHOTGUN:
        m_iAnim = DOUBLESHOTGUN_ANIM_DEACTIVATE;
        tmAnimLength = DOUBLESHOTGUN_ANIM_DEACTIVATE_LENGTH;
        break;

      case E_WEAPON_MACHINEGUN:
        m_iAnim = TOMMYGUN_ANIM_DEACTIVATE;
        tmAnimLength = TOMMYGUN_ANIM_DEACTIVATE_LENGTH;
        break;

      case E_WEAPON_CHAINGUN:
        m_iAnim = MINIGUN_ANIM_DEACTIVATE;
        tmAnimLength = MINIGUN_ANIM_DEACTIVATE_LENGTH;
        break;

      case E_WEAPON_ROCKETLAUNCHER:
        m_iAnim = ROCKETLAUNCHER_ANIM_DEACTIVATE;
        tmAnimLength = ROCKETLAUNCHER_ANIM_DEACTIVATE_LENGTH;
        break;

      case E_WEAPON_GRENADELAUNCHER:
        m_iAnim = GRENADELAUNCHER_ANIM_DEACTIVATE;
        tmAnimLength = GRENADELAUNCHER_ANIM_DEACTIVATE_LENGTH;
        break;

      case E_WEAPON_FLAMER:
        m_iAnim = FLAMER_ANIM_DEACTIVATE;
        tmAnimLength = FLAMER_ANIM_DEACTIVATE_LENGTH;
        break;

      case E_WEAPON_PLASMAGUN:
        m_iAnim = LASER_ANIM_DEACTIVATE;
        tmAnimLength = LASER_ANIM_DEACTIVATE_LENGTH;
        break;
        
      case E_WEAPON_SNIPER:
        m_iAnim = SNIPER_ANIM_DEACTIVATE;
        tmAnimLength = SNIPER_ANIM_DEACTIVATE_LENGTH;
        break;
        
      // [SSE] Beamgun
      case E_WEAPON_BEAMGUN:
        m_iAnim = BEAMGUN_ANIM_DEACTIVATE;
        tmAnimLength = BEAMGUN_ANIM_DEACTIVATE_LENGTH;
        break;

      case E_WEAPON_CANNON:
        m_iAnim = CANNON_ANIM_DEACTIVATE;
        tmAnimLength = CANNON_ANIM_DEACTIVATE_LENGTH;
        break;

      // [SSE] Plasmathrower
      case E_WEAPON_PLASMATHROWER:
        m_iAnim = LASER_ANIM_DEACTIVATE;
        tmAnimLength = LASER_ANIM_DEACTIVATE_LENGTH;
        break;

      // [SSE] Minelayer
      case E_WEAPON_MINELAYER:
        m_iAnim = GRENADELAUNCHER_ANIM_DEACTIVATE;
        tmAnimLength = GRENADELAUNCHER_ANIM_DEACTIVATE_LENGTH;
        break;

      default: ASSERTALWAYS("Unknown weapon.");
    }

    // start animator
    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->BodyPushAnimation();
    }
    
    if (eCurrentWeapon == E_WEAPON_NONE) {
      return EEnd();
    }

    // reload colts automagicaly when puting them away
    BOOL bNowColt = eCurrentWeapon == E_WEAPON_REVOLVER;
    BOOL bWantedColt = GetWantedWeapon() == E_WEAPON_REVOLVER;

    if (bNowColt && !bWantedColt) {
      m_iColtBullets = COLT_MAG_SIZE;
    }

    m_moWeapon.PlayAnim(m_iAnim, 0);
    //autowait(m_moWeapon.GetAnimLength(m_iAnim));
    autowait(tmAnimLength);
    return EEnd();
  };

  // bring up weapon
  BringUp()
  {
    // reset weapon draw offset
    ResetWeaponMovingOffset();

    // set weapon model for current weapon
    SetWeaponModel();
    
    FLOAT tmAnimLength = _pTimer->TickQuantum; // wait at least tick

    // start current weapon bring up animation
    switch (GetCurrentWeapon())
    {
      case E_WEAPON_KNIFE:
        m_iAnim = KNIFE_ANIM_PULL;
        tmAnimLength = KNIFE_ANIM_PULL_LENGTH;
        break;
        
      case E_WEAPON_CHAINSAW: {
        m_iAnim = CHAINSAW_ANIM_ACTIVATE;
        tmAnimLength = CHAINSAW_ANIM_ACTIVATE_LENGTH;
        CSoundObject &so = GetWeaponSound(E_WEAPON_SOUND_AMBIENT);
        so.Set3DParameters(30.0f, 3.0f, 1.0f, 1.0f);
        PlaySound(so, SOUND_CS_BRINGUP, SOF_3D|SOF_VOLUMETRIC|SOF_LOOP);
      } break;

      case E_WEAPON_REVOLVER:
        m_iAnim = COLT_ANIM_ACTIVATE;
        tmAnimLength = COLT_ANIM_ACTIVATE_LENGTH;
        SetFlare(0, FLARE_REMOVE);
        SetFlare(1, FLARE_REMOVE);
        break;

      case E_WEAPON_SHOTGUN:
        m_iAnim = SINGLESHOTGUN_ANIM_ACTIVATE;
        tmAnimLength = SINGLESHOTGUN_ANIM_ACTIVATE_LENGTH;
        SetFlare(0, FLARE_REMOVE);
        break;

      case E_WEAPON_SUPERSHOTGUN:
        m_iAnim = DOUBLESHOTGUN_ANIM_ACTIVATE;
        tmAnimLength = DOUBLESHOTGUN_ANIM_ACTIVATE_LENGTH;
        SetFlare(0, FLARE_REMOVE);
        break;

      case E_WEAPON_MACHINEGUN:
        m_iAnim = TOMMYGUN_ANIM_ACTIVATE;
        tmAnimLength = TOMMYGUN_ANIM_ACTIVATE_LENGTH;
        SetFlare(0, FLARE_REMOVE);
        break;

      case E_WEAPON_CHAINGUN: {
        CAttachmentModelObject *amo = m_moWeapon.GetAttachmentModel(MINIGUN_ATTACHMENT_BARRELS);
        m_aChaingunLast = m_aChaingun = amo->amo_plRelative.pl_OrientationAngle(3);
        m_iAnim = MINIGUN_ANIM_ACTIVATE;
        tmAnimLength = MINIGUN_ANIM_ACTIVATE_LENGTH;
        SetFlare(0, FLARE_REMOVE);
        break; }

      case E_WEAPON_ROCKETLAUNCHER:
        m_iAnim = ROCKETLAUNCHER_ANIM_ACTIVATE;
        tmAnimLength = ROCKETLAUNCHER_ANIM_ACTIVATE_LENGTH;
        break;

      case E_WEAPON_GRENADELAUNCHER:
        m_iAnim = GRENADELAUNCHER_ANIM_ACTIVATE;
        tmAnimLength = GRENADELAUNCHER_ANIM_ACTIVATE_LENGTH;
        break;

      case E_WEAPON_FLAMER:
        m_iAnim = FLAMER_ANIM_ACTIVATE;
        tmAnimLength = FLAMER_ANIM_ACTIVATE_LENGTH;
        break;

      case E_WEAPON_SNIPER:
        m_iAnim = SNIPER_ANIM_ACTIVATE;
        tmAnimLength = SNIPER_ANIM_ACTIVATE_LENGTH;
        SetFlare(0, FLARE_REMOVE);
        break;

      case E_WEAPON_PLASMAGUN:
        m_iAnim = LASER_ANIM_ACTIVATE;
        tmAnimLength = LASER_ANIM_ACTIVATE_LENGTH;
        break;
        
      // [SSE] Beamgun
      case E_WEAPON_BEAMGUN:
        m_iAnim = BEAMGUN_ANIM_ACTIVATE;
        tmAnimLength = BEAMGUN_ANIM_ACTIVATE_LENGTH;
        break;

      case E_WEAPON_CANNON:
        m_iAnim = CANNON_ANIM_ACTIVATE;
        tmAnimLength = CANNON_ANIM_ACTIVATE_LENGTH;
        break;

      // [SSE] Plasmathrower
      case E_WEAPON_PLASMATHROWER:
        m_iAnim = LASER_ANIM_ACTIVATE;
        tmAnimLength = LASER_ANIM_ACTIVATE_LENGTH;
        break;

      // [SSE] Minelayer
      case E_WEAPON_MINELAYER:
        m_iAnim = GRENADELAUNCHER_ANIM_ACTIVATE;
        tmAnimLength = GRENADELAUNCHER_ANIM_ACTIVATE_LENGTH;
        break;

      case WEAPON_NONE:
        break;

      default: ASSERTALWAYS("Unknown weapon.");
    }

    // start animator
    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->BodyPullAnimation();
    }

    m_moWeapon.PlayAnim(m_iAnim, 0);
    //autowait(m_moWeapon.GetAnimLength(m_iAnim));
    autowait(tmAnimLength);

    // mark that weapon change has ended
    m_tmWeaponChangeRequired -= hud_tmWeaponsOnScreen/2;

    return EEnd();
  };


  /*
   *  >>>---   FIRE WEAPON   ---<<<
   */
  Fire()
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();

    CSoundObject &soWeapon0 = GetWeaponSound(E_WEAPON_SOUND_0);
    PlaySound(soWeapon0, SOUND_SILENCE, SOF_3D|SOF_VOLUMETRIC);      // stop possible sounds

    // force ending of weapon change
    m_tmWeaponChangeRequired = 0;

    m_bFireWeapon = TRUE;
    m_bHasAmmo = HasEnoughAmmo(eCurrentWeapon);

    // if has no ammo select new weapon
    if (!m_bHasAmmo) {
      SelectNewWeapon();

      if (GetWantedWeapon() != eCurrentWeapon) { // [SSE]
        jump Idle();
      }
    }

    // setup 3D sound parameters
    Setup3DSoundParameters();

    if (GetCurrentWeapon() == E_WEAPON_MACHINEGUN) {
      autocall TommyGunStart() EEnd;
    // [SSE] Beamgun
    } else if (GetCurrentWeapon() == E_WEAPON_BEAMGUN) {
      autocall BeamgunStart() EEnd;
    }

    switch (GetCurrentWeapon())
    {
      case E_WEAPON_CHAINGUN:
      {
        jump ChaingunSpinUp();
        break;
      }

      case E_WEAPON_FLAMER:
      {
        jump FlamerStart();
        break;
      }

      case E_WEAPON_CHAINSAW:
      {
        jump ChainsawFire();
        break;
      }

      case E_WEAPON_PLASMAGUN:
      {
        if (GetHand() == E_HAND_MAIN) {
          GetAnimator()->FireAnimation(BODY_ANIM_SHOTGUN_FIRESHORT, AOF_LOOPING);
        }

        break;
      }

      case E_WEAPON_CANNON:
      { 
        jump CannonFireStart();
        break;
      }
    }


    // clear last lerped bullet position
    m_iLastBulletPosition = FLOAT3D(32000.0f, 32000.0f, 32000.0f);

    // reset laser barrel (to start shooting always from left up barrel)
    m_iLaserBarrel = 0;
    
    // [SSE] No ammo? No shooting!
    if (!m_bHasAmmo) {
      m_bFireWeapon = FALSE;
    }

    while (HoldingFire() && m_bHasAmmo)
    {
      // boring animation
      GetAnimator()->m_fLastActionTime = _pTimer->CurrentTick();

      wait()
      {
        on (EBegin) : {
          // fire one shot
          switch (GetCurrentWeapon())
          {
            case E_WEAPON_NONE: call FireNone(); break;
            case E_WEAPON_KNIFE: call SwingKnife(); break;
            case E_WEAPON_REVOLVER: call FireRevolver(); break;
            case E_WEAPON_SHOTGUN: call FireSingleShotgun(); break;
            case E_WEAPON_SUPERSHOTGUN: call FireSuperShotgun(); break;
            case E_WEAPON_MACHINEGUN: call FireTommyGun(); break;
            case E_WEAPON_SNIPER: call FireSniper(); break;
            case E_WEAPON_ROCKETLAUNCHER: call FireRocketLauncher(); break;
            case E_WEAPON_GRENADELAUNCHER: call FireGrenadeLauncher(); break;
            case E_WEAPON_PLASMAGUN: call FirePlasmagun(); break;
            case E_WEAPON_BEAMGUN: call FireBeamgun(); break; // [SSE] Beamgun
            case E_WEAPON_PLASMATHROWER: call FirePlasmathrower(); break; // [SSE] Plasmathrower
            case E_WEAPON_MINELAYER: call FireGrenadeLauncher(); break; // [SSE] Minelayer
            default: ASSERTALWAYS("Unknown weapon.");
          }

          resume;
        }

        on (EEnd) : {
          stop;
        }
      }
    }

    // stop weapon firing animation for continuous firing
    switch (GetCurrentWeapon())
    {
      case E_WEAPON_MACHINEGUN: { jump TommyGunStop(); break; }
      case E_WEAPON_CHAINGUN: { jump ChaingunSpinDown(); break; }
      case E_WEAPON_FLAMER: { jump FlamerStop(); break; }
      case E_WEAPON_PLASMAGUN: {
        if (GetHand() == E_HAND_MAIN) {
          GetAnimator()->FireAnimationOff();
        }

        jump Idle();
      }

      case E_WEAPON_BEAMGUN: { jump BeamgunStop(); break; } // [SSE] Beamgun

      default: { jump Idle(); }
    }
  };
  
  FireNone()
  {
    m_bFireWeapon = m_bHasAmmo = FALSE;

    return EEnd();
  };

  // ***************** SWING KNIFE *****************
  SwingKnife()
  {
    INDEX iSwing;

    // animator swing
    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimation(BODY_ANIM_KNIFE_ATTACK, 0);
    }
  
    // depending on stand choose random attack

    iSwing = IRnd()%2;
    switch (iSwing)
    {
      case 0:
        m_iAnim = KNIFE_ANIM_ATTACK01;
        m_fAnimWaitTime = 0.25f;
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_KNIFE_BACK, SOF_3D|SOF_VOLUMETRIC);
        if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Knife_back");}
        break;
      case 1:
        m_iAnim = KNIFE_ANIM_ATTACK02;
        m_fAnimWaitTime = 0.35f;
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_KNIFE_BACK, SOF_3D|SOF_VOLUMETRIC);
        if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Knife_back");}
        break;
    }

    m_moWeapon.PlayAnim(m_iAnim, 0);

    if (CutWithKnife(0, 0, 3.0f, 2.0f, 0.5f, ((GetSP()->sp_bCooperative) ? 100 : 50) * HEALTH_VALUE_MULTIPLIER)) {
      autowait(m_fAnimWaitTime);
    } else if (TRUE) {
      autowait(m_fAnimWaitTime/2);
      CutWithKnife(0, 0, 3.0f, 2.0f, 0.5f, ((GetSP()->sp_bCooperative) ? 100 : 50) * HEALTH_VALUE_MULTIPLIER);
      autowait(m_fAnimWaitTime/2);
    }

    FLOAT tmAnimLength;
    
    switch (m_iAnim)
    {
      case KNIFE_ANIM_ATTACK01:
        tmAnimLength = KNIFE_ANIM_ATTACK01_LENGTH;
        break;
      case KNIFE_ANIM_ATTACK02:
        tmAnimLength = KNIFE_ANIM_ATTACK02_LENGTH;
        break;
    }

    if (tmAnimLength - m_fAnimWaitTime >= _pTimer->TickQuantum) {
      autowait(tmAnimLength - m_fAnimWaitTime);
    }

    return EEnd();
  };

  // ***************** FIRE COLT *****************
  FireRevolver()
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon);
    
    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimation(BODY_ANIM_COLT_FIRERIGHT, 0);
    }

    // fire bullet
    INDEX iBulletDamage = ((GetSP()->sp_bCooperative) ? 10 : 20) * HEALTH_VALUE_MULTIPLIER;
    FireOneBullet(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2), 500.0f, iBulletDamage);

    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Colt_fire");}
    DoRecoil();
    SpawnRangeSound(40.0f);
    m_iColtBullets--;
    SetFlare(0, FLARE_ADD);
    PlayLightAnim(LIGHT_ANIM_COLT_SHOTGUN, 0);

    // sound
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_COLT_FIRE, SOF_3D|SOF_VOLUMETRIC);

    /*
    if (pl.m_pstState!=PST_DIVE)
    {
      // smoke
      ShellLaunchData &sldRight = pl.m_asldData[pl.m_iFirstEmptySLD];
      sldRight.sld_vPos = FLOAT3D(afRightColtPipe[0], afRightColtPipe[1], afRightColtPipe[2]);
      sldRight.sld_tmLaunch = _pTimer->CurrentTick();
      sldRight.sld_estType = ESL_COLT_SMOKE;
      pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
    }
    */

    FLOAT tmAnimLength;

    // random colt fire
    INDEX iAnim;
    switch (IRnd()%3) {
      case 0:
        iAnim = COLT_ANIM_FIRE1;
        tmAnimLength = COLT_ANIM_FIRE1_LENGTH;
        break;
      
      case 1:
        iAnim = COLT_ANIM_FIRE2;
        tmAnimLength = COLT_ANIM_FIRE2_LENGTH;
        break;

      case 2:
        iAnim = COLT_ANIM_FIRE3;
        tmAnimLength = COLT_ANIM_FIRE3_LENGTH;
        break;
    }

    m_moWeapon.PlayAnim(iAnim, 0);
    autowait(tmAnimLength - 0.05f);
    m_moWeapon.PlayAnim(COLT_ANIM_WAIT1, AOF_LOOPING|AOF_NORESTART);

    // no more bullets in colt -> reload
    if (m_iColtBullets == 0) {
      jump ReloadRevolver();
    }

    return EEnd();
  };

  // reload colt
  ReloadRevolver()
  {
    if (m_iColtBullets >= COLT_MAG_SIZE) {
      return EEnd();
    }

    // sound
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_COLT_RELOAD, SOF_3D|SOF_VOLUMETRIC);

    m_moWeapon.PlayAnim(COLT_ANIM_RELOAD, 0);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Colt_reload");}
    autowait(COLT_ANIM_RELOAD_LENGTH);
    m_iColtBullets = COLT_MAG_SIZE;
    return EEnd();
  };

  // ***************** FIRE SINGLESHOTGUN *****************
  FireSingleShotgun()
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon);
    
    if (!HasEnoughAmmo()) {
      //ASSERTALWAYS("SingleShotgun - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
      return EEnd();
    }

    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimation(BODY_ANIM_SHOTGUN_FIRELONG, 0);
    }

    INDEX iBulletDamage = 10 * HEALTH_VALUE_MULTIPLIER;
    FireBullets(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2),
      500.0f, iBulletDamage, 7, afSingleShotgunPellets, 0.2f, 0.03f);
    DoRecoil();
    SpawnRangeSound(60.0f);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Snglshotgun_fire");}
    DrainAmmo();
    SetFlare(0, FLARE_ADD);
    PlayLightAnim(LIGHT_ANIM_COLT_SHOTGUN, 0);
    m_moWeapon.PlayAnim(GetSP()->sp_bCooperative ? SINGLESHOTGUN_ANIM_FIRE1 : SINGLESHOTGUN_ANIM_FIRE1FAST, 0);

    // sound
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_SINGLESHOTGUN_FIRE, SOF_3D|SOF_VOLUMETRIC);

    if (hud_bShowWeapon)
    {
      CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;

      if (pl.m_pstState==PST_DIVE) {
        // bubble
        ShellLaunchData &sldBubble = pl.m_asldData[pl.m_iFirstEmptySLD];
        CPlacement3D plShell;
        CalcWeaponPosition(FLOAT3D(afSingleShotgunShellPos[0],
          afSingleShotgunShellPos[1], afSingleShotgunShellPos[2]), plShell, FALSE);
        FLOATmatrix3D m;
        MakeRotationMatrixFast(m, plShell.pl_OrientationAngle);
        FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
        sldBubble.sld_vPos = plShell.pl_PositionVector;
        sldBubble.sld_vUp = vUp;
        sldBubble.sld_tmLaunch = _pTimer->CurrentTick();
        sldBubble.sld_estType = ESL_BUBBLE;
        FLOAT3D vSpeedRelative = FLOAT3D(0.3f, 0.0f, 0.0f);
        sldBubble.sld_vSpeed = vSpeedRelative*m;
        pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
      } else {
        // smoke
        ShellLaunchData &sldPipe1 = pl.m_asldData[pl.m_iFirstEmptySLD];
        CPlacement3D plPipe;
        CalcWeaponPosition(FLOAT3D(afSingleShotgunPipe[0], afSingleShotgunPipe[1], afSingleShotgunPipe[2]), plPipe, FALSE);
        FLOATmatrix3D m;
        MakeRotationMatrixFast(m, plPipe.pl_OrientationAngle);
        FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
        sldPipe1.sld_vPos = plPipe.pl_PositionVector;
        sldPipe1.sld_vUp = vUp;
        sldPipe1.sld_tmLaunch = _pTimer->CurrentTick();
        sldPipe1.sld_estType = ESL_SHOTGUN_SMOKE;
        FLOAT3D vSpeedRelative = FLOAT3D(0, 0.0f, -12.5f);
        sldPipe1.sld_vSpeed = vSpeedRelative*m;
        pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
      }
    }

    autowait(GetSP()->sp_bCooperative ? 0.5f : 0.375);
    /* drop shell */

    /* add one empty bullet shell */
    CPlacement3D plShell;
    CalcWeaponPosition(FLOAT3D(afSingleShotgunShellPos[0], afSingleShotgunShellPos[1], afSingleShotgunShellPos[2]), plShell, FALSE);

    FLOATmatrix3D mRot;
    MakeRotationMatrixFast(mRot, plShell.pl_OrientationAngle);

    if (hud_bShowWeapon)
    {
      CPlayerPawnEntity *penPlayer = GetPlayer();
      ShellLaunchData &sld = penPlayer->m_asldData[penPlayer->m_iFirstEmptySLD];
      sld.sld_vPos = plShell.pl_PositionVector;
      FLOAT3D vSpeedRelative = FLOAT3D(FRnd()+2.0f, FRnd()+5.0f, -FRnd()-2.0f);
      sld.sld_vSpeed = vSpeedRelative*mRot;

      const FLOATmatrix3D &m = penPlayer->GetRotationMatrix();
      FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
      sld.sld_vUp = vUp;
      sld.sld_tmLaunch = _pTimer->CurrentTick();
      sld.sld_estType = ESL_SHOTGUN;
      // move to next shell position
      penPlayer->m_iFirstEmptySLD = (penPlayer->m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
    }

    FLOAT fAnimLength = GetSP()->sp_bCooperative ? SINGLESHOTGUN_ANIM_FIRE_LENGTH : SINGLESHOTGUN_ANIM_FIREFAST_LENGTH;  

    /* drop shell */
    autowait(fAnimLength - (GetSP()->sp_bCooperative ? 0.5f : 0.375f) );

    // no ammo -> change weapon
    if (!HasEnoughAmmo()) { SelectNewWeapon(); }

    return EEnd();
  };

  // ***************** FIRE DOUBLESHOTGUN *****************
  FireSuperShotgun()
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon);

    if (!HasEnoughAmmo()) {
      //ASSERTALWAYS("SuperShotgun - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
      return EEnd();
    }

    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimation(BODY_ANIM_SHOTGUN_FIRELONG, 0);
    }

    INDEX iBulletDamage = 10 * HEALTH_VALUE_MULTIPLIER;
    FireBullets(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2),
      500.0f, iBulletDamage, 14, afSuperShotgunPellets, 0.3f, 0.03f);
    DoRecoil();
    SpawnRangeSound(70.0f);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Dblshotgun_fire");}
    DrainAmmo();
    SetFlare(0, FLARE_ADD);
    PlayLightAnim(LIGHT_ANIM_COLT_SHOTGUN, 0);
    m_moWeapon.PlayAnim(GetSP()->sp_bCooperative ? DOUBLESHOTGUN_ANIM_FIRE : DOUBLESHOTGUN_ANIM_FIREFAST, 0);
    m_moWeaponSecond.PlayAnim(GetSP()->sp_bCooperative ? HANDWITHAMMO_ANIM_FIRE : HANDWITHAMMO_ANIM_FIREFAST, 0);

    // sound
    CSoundObject &so = GetWeaponSound(E_WEAPON_SOUND_0);
    so.Set3DParameters(50.0f, 5.0f, 1.5f, 1.0f);      // fire
    PlaySound(so, SOUND_DOUBLESHOTGUN_FIRE, SOF_3D|SOF_VOLUMETRIC);

    if (hud_bShowWeapon)
    {
      CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;

      if (pl.m_pstState==PST_DIVE) {
        // bubble (pipe 1)
        ShellLaunchData &sldBubble1 = pl.m_asldData[pl.m_iFirstEmptySLD];
        CPlacement3D plShell;
        CalcWeaponPosition(FLOAT3D(-0.11f, 0.1f, -0.3f), plShell, FALSE);
        /*CalcWeaponPosition(FLOAT3D(afSuperShotgunShellPos[0],
          afSuperShotgunShellPos[1], afSuperShotgunShellPos[2]), plShell, FALSE);*/
        FLOATmatrix3D m;
        MakeRotationMatrixFast(m, plShell.pl_OrientationAngle);
        FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
        sldBubble1.sld_vPos = plShell.pl_PositionVector;
        sldBubble1.sld_vUp = vUp;
        sldBubble1.sld_tmLaunch = _pTimer->CurrentTick();
        sldBubble1.sld_estType = ESL_BUBBLE;
        FLOAT3D vSpeedRelative = FLOAT3D(-0.1f, 0.0f, 0.01f);
        sldBubble1.sld_vSpeed = vSpeedRelative*m;
        pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
        ShellLaunchData &sldBubble2 = pl.m_asldData[pl.m_iFirstEmptySLD];
        // bubble (pipe 2)
        sldBubble2 = sldBubble1;
        vSpeedRelative = FLOAT3D(0.1f, 0.0f, -0.2f);
        sldBubble2.sld_vSpeed = vSpeedRelative*m;
        pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;

      } else {
        // smoke (pipe 1)
        ShellLaunchData &sldPipe1 = pl.m_asldData[pl.m_iFirstEmptySLD];
        CPlacement3D plPipe;
        CalcWeaponPosition(FLOAT3D(afSuperShotgunPipe[0], afSuperShotgunPipe[1], afSuperShotgunPipe[2]), plPipe, FALSE);
        FLOATmatrix3D m;
        MakeRotationMatrixFast(m, plPipe.pl_OrientationAngle);
        FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
        sldPipe1.sld_vPos = plPipe.pl_PositionVector;
        sldPipe1.sld_vUp = vUp;
        sldPipe1.sld_tmLaunch = _pTimer->CurrentTick();
        sldPipe1.sld_estType = ESL_SHOTGUN_SMOKE;
        FLOAT3D vSpeedRelative = FLOAT3D(-1, 0.0f, -12.5f);
        sldPipe1.sld_vSpeed = vSpeedRelative*m;
        pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
        // smoke (pipe 2)
        ShellLaunchData &sldPipe2 = pl.m_asldData[pl.m_iFirstEmptySLD];
        sldPipe2 = sldPipe1;
        vSpeedRelative = FLOAT3D(1, 0.0f, -12.5f);
        sldPipe2.sld_vSpeed = vSpeedRelative*m;
        pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
      }
    }

    autowait(GetSP()->sp_bCooperative ? 0.25f : 0.15f);

    if (HasEnoughAmmo()) {
      PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_DOUBLESHOTGUN_RELOAD, SOF_3D|SOF_VOLUMETRIC);
    }

    FLOAT fAnimLength = GetSP()->sp_bCooperative ? DOUBLESHOTGUN_ANIM_FIRE_LENGTH : DOUBLESHOTGUN_ANIM_FIREFAST_LENGTH;

    autowait( fAnimLength - (GetSP()->sp_bCooperative ? 0.25f : 0.15f) );

    // no ammo -> change weapon
    if (!HasEnoughAmmo()) { SelectNewWeapon(); }

    return EEnd();
  };

  // ***************** FIRE TOMMYGUN *****************
  TommyGunStart()
  {
    m_iBulletsOnFireStart = GetInventory()->GetAmmoQty(GetWeaponParams(E_WEAPON_MACHINEGUN)->GetDefaultAmmoType());

    CSoundObject &so = GetWeaponSound(E_WEAPON_SOUND_0);
    PlaySound(so, SOUND_SILENCE, SOF_3D|SOF_VOLUMETRIC);      // stop possible sounds
    so.Set3DParameters(50.0f, 5.0f, 1.5f, 1.0f);      // fire
    PlaySound(so, SOUND_TOMMYGUN_FIRE, SOF_LOOP|SOF_3D|SOF_VOLUMETRIC);
    PlayLightAnim(LIGHT_ANIM_TOMMYGUN, AOF_LOOPING);
    
    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimation(BODY_ANIM_SHOTGUN_FIRESHORT, AOF_LOOPING);
    }

    return EEnd();
  };

  TommyGunStop()
  {
    // smoke
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;

    if (pl.m_pstState!=PST_DIVE && hud_bShowWeapon)
    {
      INDEX ctCurrentBullets = GetInventory()->GetAmmoQty(GetWeaponParams(E_WEAPON_MACHINEGUN)->GetDefaultAmmoType());
      INDEX ctBulletsFired = ClampUp(m_iBulletsOnFireStart - ctCurrentBullets, INDEX(100));

      for (INDEX iSmoke=0; iSmoke<ctBulletsFired/6.0; iSmoke++)
      {
        ShellLaunchData *psldSmoke = &pl.m_asldData[pl.m_iFirstEmptySLD];
        CPlacement3D plPipe;
        CalcWeaponPosition(FLOAT3D(afTommygunPipe[0], afTommygunPipe[1], afTommygunPipe[2]), plPipe, FALSE);
        FLOATmatrix3D m;
        MakeRotationMatrixFast(m, plPipe.pl_OrientationAngle);
        psldSmoke->sld_vPos = plPipe.pl_PositionVector+pl.en_vCurrentTranslationAbsolute*iSmoke*_pTimer->TickQuantum;
        FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
        psldSmoke->sld_vUp = vUp;
        psldSmoke->sld_tmLaunch = _pTimer->CurrentTick()+iSmoke*_pTimer->TickQuantum;
        psldSmoke->sld_estType = ESL_BULLET_SMOKE;
        psldSmoke->sld_fSize = 0.5f+ctBulletsFired/75.0f;
        FLOAT3D vSpeedRelative = FLOAT3D(-0.06f, 0.0f, -0.06f);
        psldSmoke->sld_vSpeed = vSpeedRelative*m+pl.en_vCurrentTranslationAbsolute;
        pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
      }
    }

    GetWeaponSound(E_WEAPON_SOUND_0).Set3DParameters(50.0f, 5.0f, 0.0f, 1.0f);      // mute fire
    PlayLightAnim(LIGHT_ANIM_NONE, 0);

    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimationOff();
    }

    jump Idle();
  };

  FireTommyGun()
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon);
    
    if (!HasEnoughAmmo()) {
      //ASSERTALWAYS("TommyGun - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
      return EEnd();
    }

    INDEX iBulletDamage = 10 * HEALTH_VALUE_MULTIPLIER;
    FireMachineBullet(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2),
      500.0f, iBulletDamage, ((GetSP()->sp_bCooperative) ? 0.01f : 0.03f),
      ((GetSP()->sp_bCooperative) ? 0.5f : 0.0f));
    SpawnRangeSound(50.0f);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Tommygun_fire");}
    DrainAmmo();
    SetFlare(0, FLARE_ADD);
    m_moWeapon.PlayAnim(TOMMYGUN_ANIM_FIRE, AOF_LOOPING|AOF_NORESTART);

    // firing FX
    CPlacement3D plShell;
    CalcWeaponPosition(FLOAT3D(afTommygunShellPos[0], afTommygunShellPos[1], afTommygunShellPos[2]), plShell, FALSE);
    FLOATmatrix3D mRot;
    MakeRotationMatrixFast(mRot, plShell.pl_OrientationAngle);

    if (hud_bShowWeapon)
    {
      // empty bullet shell
      CPlayerPawnEntity &pl = *GetPlayer();
      ShellLaunchData &sld = pl.m_asldData[pl.m_iFirstEmptySLD];
      sld.sld_vPos = plShell.pl_PositionVector;
      FLOAT3D vSpeedRelative = FLOAT3D(FRnd()+2.0f, FRnd()+5.0f, -FRnd()-2.0f);
      const FLOATmatrix3D &m = pl.GetRotationMatrix();
      FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
      sld.sld_vUp = vUp;
      sld.sld_vSpeed = vSpeedRelative*mRot;
      sld.sld_tmLaunch = _pTimer->CurrentTick();
      sld.sld_estType = ESL_BULLET;
      pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;

      // bubble
      if (pl.m_pstState==PST_DIVE)
      {
        ShellLaunchData &sldBubble = pl.m_asldData[pl.m_iFirstEmptySLD];
        CalcWeaponPosition(FLOAT3D(afTommygunShellPos[0], afTommygunShellPos[1], afTommygunShellPos[2]), plShell, FALSE);
        MakeRotationMatrixFast(mRot, plShell.pl_OrientationAngle);
        sldBubble.sld_vPos = plShell.pl_PositionVector;
        sldBubble.sld_vUp = vUp;
        sldBubble.sld_tmLaunch = _pTimer->CurrentTick();
        sldBubble.sld_estType = ESL_BUBBLE;
        vSpeedRelative = FLOAT3D(0.3f, 0.0f, 0.0f);
        sldBubble.sld_vSpeed = vSpeedRelative*mRot;
        pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
      }
    }

    autowait(0.05f);
    autowait(0.05f);

    // no ammo -> change weapon
    if (!HasEnoughAmmo()) { SelectNewWeapon(); }

    return EEnd();
  };

  // ***************** FIRE SNIPER *****************
  FireSniper()
  {
    const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
    const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon);

    if (!HasEnoughAmmo()) {
      //ASSERTALWAYS("Sniper - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
      return EEnd();
    }

    // fire one bullet
    if (m_bSniping) {
      INDEX iBulletDamage = ((GetSP()->sp_bCooperative) ? 300 : 90) * HEALTH_VALUE_MULTIPLIER;
      FireSniperBullet(0.0f, 0.0f, 1500.0f, iBulletDamage, 0.0f);
    } else {
      INDEX iBulletDamage = ((GetSP()->sp_bCooperative) ? 75 : 30) * HEALTH_VALUE_MULTIPLIER;
      FireSniperBullet(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2), 1000.0f, iBulletDamage, 5.0f);
    }

    m_tmLastSniperFire = _pTimer->CurrentTick();

    SpawnRangeSound(50.0f);
    DrainAmmo();

    if (!m_bSniping) {
      SetFlare(0, FLARE_ADD);
    }

    PlayLightAnim(LIGHT_ANIM_COLT_SHOTGUN, 0);

    // sound
    CSoundObject &so = GetWeaponSound(E_WEAPON_SOUND_0);

    if (GetSP()->sp_bCooperative) {
      so.Set3DParameters(50.0f, 5.0f, 1.5f, 1.0f);
    } else if (TRUE) {
      so.Set3DParameters(250.0f, 75.0f, 1.5f, 1.0f);
    }

    PlaySound(so, SOUND_SNIPER_FIRE, SOF_3D|SOF_VOLUMETRIC);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("SniperFire");}

    // animation
    m_moWeapon.PlayAnim(SNIPER_ANIM_FIRE, 0);

    autowait(1.0f);

    // firing FX
    CPlacement3D plShell;
    CalcWeaponPosition(FLOAT3D(afSniperShellPos[0], afSniperShellPos[1], afSniperShellPos[2]), plShell, FALSE);
    FLOATmatrix3D mRot;
    MakeRotationMatrixFast(mRot, plShell.pl_OrientationAngle);

    if (hud_bShowWeapon)
    {
      CPlayerPawnEntity *penPlayer = GetPlayer();
      ShellLaunchData &sld = penPlayer->m_asldData[penPlayer->m_iFirstEmptySLD];
      sld.sld_vPos = plShell.pl_PositionVector;
      FLOAT3D vSpeedRelative = FLOAT3D(FRnd()+2.0f, FRnd()+5.0f, -FRnd()-2.0f);
      sld.sld_vSpeed = vSpeedRelative*mRot;

      const FLOATmatrix3D &m = penPlayer->GetRotationMatrix();
      FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
      sld.sld_vUp = vUp;
      sld.sld_tmLaunch = _pTimer->CurrentTick();
      sld.sld_estType = ESL_BULLET;
      // move to next shell position
      penPlayer->m_iFirstEmptySLD = (penPlayer->m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;

      // bubble
      if (penPlayer->m_pstState==PST_DIVE)
      {
        ShellLaunchData &sldBubble = penPlayer->m_asldData[penPlayer->m_iFirstEmptySLD];
        CalcWeaponPosition(FLOAT3D(afTommygunShellPos[0], afTommygunShellPos[1], afTommygunShellPos[2]), plShell, FALSE);
        MakeRotationMatrixFast(mRot, plShell.pl_OrientationAngle);
        sldBubble.sld_vPos = plShell.pl_PositionVector;
        sldBubble.sld_vUp = vUp;
        sldBubble.sld_tmLaunch = _pTimer->CurrentTick();
        sldBubble.sld_estType = ESL_BUBBLE;
        vSpeedRelative = FLOAT3D(0.3f, 0.0f, 0.0f);
        sldBubble.sld_vSpeed = vSpeedRelative*mRot;
        penPlayer->m_iFirstEmptySLD = (penPlayer->m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
      }
    }

    autowait(1.35f - 1.0f);

    // no ammo -> change weapon
    if (!HasEnoughAmmo()) { SelectNewWeapon(); }

    return EEnd();
  };


  // ***************** FIRE CHAINGUN *****************
  ChaingunSpinUp()
  {
    // steady anim
    m_moWeapon.PlayAnim(MINIGUN_ANIM_WAIT1, AOF_LOOPING|AOF_NORESTART);

    // no boring animation
    GetAnimator()->m_fLastActionTime = _pTimer->CurrentTick();

    // clear last lerped bullet position
    m_iLastBulletPosition = FLOAT3D(32000.0f, 32000.0f, 32000.0f);
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_SILENCE, SOF_3D|SOF_VOLUMETRIC);      // stop possible sounds

    // initialize sound 3D parameters
    GetWeaponSound(E_WEAPON_SOUND_0).Set3DParameters(50.0f, 5.0f, 2.0f, 1.0f);      // fire
    GetWeaponSound(E_WEAPON_SOUND_1).Set3DParameters(50.0f, 5.0f, 1.0f, 1.0f);      // spinup/spindown/spin
    GetWeaponSound(E_WEAPON_SOUND_2).Set3DParameters(50.0f, 5.0f, 1.0f, 1.0f);      // turn on/off click

    // spin start sounds
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_2), SOUND_MINIGUN_CLICK, SOF_3D|SOF_VOLUMETRIC);
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_MINIGUN_SPINUP, SOF_3D|SOF_VOLUMETRIC);
    //GetWeaponSound(E_WEAPON_SOUND_1).SetOffset((m_aChaingunSpeed/MINIGUN_FULLSPEED)*MINIGUN_SPINUPSOUND);

    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Minigun_rotateup");}

    // while not at full speed and fire is held
    while (m_aChaingunSpeed<MINIGUN_FULLSPEED && HoldingFire())
    {
      // every tick
      autowait(CHAINGUN_TICKTIME);
      // increase speed
      m_aChaingunLast = m_aChaingun;
      m_aChaingun+=m_aChaingunSpeed*CHAINGUN_TICKTIME;
      m_aChaingunSpeed+=MINIGUN_SPINUPACC*CHAINGUN_TICKTIME;
    }

    // do not go over full speed
    m_aChaingunSpeed = ClampUp(m_aChaingunSpeed, MINIGUN_FULLSPEED);

    // if not holding fire anymore
    if (!HoldingFire()) {
      // start spindown
      jump ChaingunSpinDown();
    }

    // start firing
    jump ChaingunFire();
  }

  ChaingunFire()
  {
    // spinning sound
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_MINIGUN_ROTATE, SOF_3D|SOF_LOOP|SOF_VOLUMETRIC|SOF_SMOOTHCHANGE);

    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Minigun_rotate");}

  // if firing
    if (HoldingFire() && HasEnoughAmmo()) {
      // play fire sound
      PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_MINIGUN_FIRE, SOF_3D|SOF_LOOP|SOF_VOLUMETRIC);
      PlayLightAnim(LIGHT_ANIM_TOMMYGUN, AOF_LOOPING);
      
      if (GetHand() == E_HAND_MAIN) {
        GetAnimator()->FireAnimation(BODY_ANIM_MINIGUN_FIRESHORT, AOF_LOOPING);
      }
    }

    m_iBulletsOnFireStart = GetInventory()->GetAmmoQty(GetWeaponParams(E_WEAPON_CHAINGUN)->GetDefaultAmmoType());

    // while holding fire
    while (HoldingFire())
    {
      // check for ammo pickup during empty spinning
      if (!m_bHasAmmo && HasEnoughAmmo()) {
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_MINIGUN_FIRE, SOF_3D|SOF_LOOP|SOF_VOLUMETRIC);
        if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Minigun_fire");}
        PlayLightAnim(LIGHT_ANIM_TOMMYGUN, AOF_LOOPING);

        if (GetHand() == E_HAND_MAIN) {
          GetAnimator()->FireAnimation(BODY_ANIM_MINIGUN_FIRESHORT, AOF_LOOPING);
        }

        m_bHasAmmo = TRUE;
      }

      // if has ammo
      if (HasEnoughAmmo()) {
        const WeaponIndex eCurrentWeapon = GetCurrentWeapon();
        const CWeaponPositionParams *pPosParams = GetWeaponPositionParams(eCurrentWeapon);

        // fire a bullet
        INDEX iBulletDamage = 10 * HEALTH_VALUE_MULTIPLIER;
        FireMachineBullet(pPosParams->GetFirePos()(1), pPosParams->GetFirePos()(2),
          750.0f, iBulletDamage, (GetSP()->sp_bCooperative) ? 0.01f : 0.03f,
          ((GetSP()->sp_bCooperative) ? 0.5f : 0.0f));

        DoRecoil();
        SpawnRangeSound(60.0f);
        DrainAmmo();
        SetFlare(0, FLARE_ADD);

        /* add one empty bullet shell */
        CPlacement3D plShell;

        // if 1st person view
        CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;

        if (pl.m_penCamera==NULL && pl.m_pen3rdPersonView==NULL) {
          CalcWeaponPosition(FLOAT3D(afChaingunShellPos[0], afChaingunShellPos[1], afChaingunShellPos[2]), plShell, FALSE);

        // if 3rd person view
        } else {
          /*CalcWeaponPosition3rdPersonView(FLOAT3D(tmp_af[0], tmp_af[1], tmp_af[2]), plShell, FALSE);*/
          CalcWeaponPosition3rdPersonView(FLOAT3D(afChaingunShellPos3rdView[0],
            afChaingunShellPos3rdView[1], afChaingunShellPos3rdView[2]), plShell, FALSE);
        }

        FLOATmatrix3D mRot;
        MakeRotationMatrixFast(mRot, plShell.pl_OrientationAngle);

        if (hud_bShowWeapon)
        {
          CPlayerPawnEntity &pl = *GetPlayer();
          ShellLaunchData &sld = pl.m_asldData[pl.m_iFirstEmptySLD];
          sld.sld_vPos = plShell.pl_PositionVector;
          FLOAT3D vSpeedRelative = FLOAT3D(FRnd()+2.0f, FRnd()+5.0f, -FRnd()-2.0f);
          const FLOATmatrix3D &m = pl.GetRotationMatrix();
          FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
          sld.sld_vUp = vUp;
          sld.sld_vSpeed = vSpeedRelative*mRot;
          sld.sld_tmLaunch = _pTimer->CurrentTick();
          sld.sld_estType = ESL_BULLET;
          // move to next shell position
          pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;

          // bubble
          if (pl.m_pstState==PST_DIVE)
          {
            ShellLaunchData &sldBubble = pl.m_asldData[pl.m_iFirstEmptySLD];
            CalcWeaponPosition(FLOAT3D(afChaingunShellPos[0], afChaingunShellPos[1], afChaingunShellPos[2]), plShell, FALSE);
            MakeRotationMatrixFast(mRot, plShell.pl_OrientationAngle);
            sldBubble.sld_vPos = plShell.pl_PositionVector;
            sldBubble.sld_vUp = vUp;
            sldBubble.sld_tmLaunch = _pTimer->CurrentTick();
            sldBubble.sld_estType = ESL_BUBBLE;
            vSpeedRelative = FLOAT3D(0.3f, 0.0f, 0.0f);
            sldBubble.sld_vSpeed = vSpeedRelative*mRot;
            pl.m_iFirstEmptySLD = (pl.m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
          }
        }
      // if no ammo
      } else {
        if (m_bHasAmmo) {
          ChaingunSmoke();
        }
        // stop fire sound
        m_bHasAmmo = FALSE;
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_SILENCE, SOF_3D|SOF_VOLUMETRIC);      // stop possible sounds
        PlayLightAnim(LIGHT_ANIM_NONE, AOF_LOOPING);
        
        if (GetHand() == E_HAND_MAIN) {
          GetAnimator()->FireAnimationOff();
        }
      }

      autowait(CHAINGUN_TICKTIME);

      // spin
      m_aChaingunLast = m_aChaingun;
      m_aChaingun+=m_aChaingunSpeed*CHAINGUN_TICKTIME;
    }

    if (m_bHasAmmo) {
      ChaingunSmoke();
    }

    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimationOff();
    }

    // stop fire sound
    GetWeaponSound(E_WEAPON_SOUND_0).Set3DParameters(50.0f, 5.0f, 0.0f, 1.0f);      // mute fire
    PlayLightAnim(LIGHT_ANIM_NONE, AOF_LOOPING);

    // start spin down
    jump ChaingunSpinDown();
  }

  ChaingunSpinDown()
  {
    // spin down sounds
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_3), SOUND_MINIGUN_CLICK, SOF_3D|SOF_VOLUMETRIC);
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_MINIGUN_SPINDOWN, SOF_3D|SOF_VOLUMETRIC|SOF_SMOOTHCHANGE);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_StopEffect("Minigun_rotate");}
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Minigun_rotatedown");}
    //GetWeaponSound(E_WEAPON_SOUND_1).SetOffset((1-m_aChaingunSpeed/MINIGUN_FULLSPEED)*MINIGUN_SPINDNSOUND);

    // while still spinning and should not fire
    while (m_aChaingunSpeed>0 && (!HoldingFire() || !HasEnoughAmmo()))
    {
      autowait(CHAINGUN_TICKTIME);

      // spin
      m_aChaingunLast = m_aChaingun;
      m_aChaingun+=m_aChaingunSpeed*CHAINGUN_TICKTIME;
      m_aChaingunSpeed-=MINIGUN_SPINDNACC*CHAINGUN_TICKTIME;

      if (!HasEnoughAmmo()) {
        SelectNewWeapon();
      }

      // if weapon should be changed
      if (m_bChangeWeapon) {
        // stop spinning immediately
        m_aChaingunSpeed = 0.0f;
        m_aChaingunLast = m_aChaingun;

        if (GetHand() == E_HAND_MAIN) {
          GetAnimator()->FireAnimationOff();
        }

        jump Idle();
      }
    }

    // clamp some
    m_aChaingunSpeed = ClampDn(m_aChaingunSpeed, 0.0f);
    m_aChaingunLast = m_aChaingun;

    // if should fire
    if (HoldingFire() && HasEnoughAmmo()) {
      // start spinup
      jump ChaingunSpinUp();
    }

    // no boring animation
    GetAnimator()->m_fLastActionTime = _pTimer->CurrentTick();

    // if out of ammo
    if (!HasEnoughAmmo()) {
      // can wait without changing while holding fire - specific for movie sequence
      while (HoldingFire() && !HasEnoughAmmo()) {
        autowait(0.1f);
      }

      if (!HasEnoughAmmo()) {
        // select new weapon
        SelectNewWeapon();
      }
    }

    jump Idle();
  };

  // ***************** FIRE ROCKETLAUNCHER *****************
  FireRocketLauncher()
  {
    if (!HasEnoughAmmo()) {
      //ASSERTALWAYS("RocketLauncher - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
      return EEnd();
    }

    // fire one grenade
    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimation(BODY_ANIM_MINIGUN_FIRELONG, 0);
    }

    m_moWeapon.PlayAnim(ROCKETLAUNCHER_ANIM_FIRE, 0);
    FireRocket();

    DoRecoil();
    SpawnRangeSound(20.0f);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Rocketlauncher_fire");}
    DrainAmmo();

    CSoundObject &soWeapon0 = GetWeaponSound(E_WEAPON_SOUND_0);

    // sound
    if (soWeapon0.IsPlaying()) {
      PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_ROCKETLAUNCHER_FIRE, SOF_3D|SOF_VOLUMETRIC);
    } else {
      PlaySound(soWeapon0, SOUND_ROCKETLAUNCHER_FIRE, SOF_3D|SOF_VOLUMETRIC);
    }

    autowait(0.05f);

    CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(ROCKETLAUNCHER_ATTACHMENT_ROCKET1)->amo_moModelObject);
    pmo->StretchModel(FLOAT3D(0, 0, 0));

    autowait(ROCKETLAUNCHER_ANIM_FIRE_LENGTH);

    CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(ROCKETLAUNCHER_ATTACHMENT_ROCKET1)->amo_moModelObject);
    pmo->StretchModel(FLOAT3D(1, 1, 1));

    // no ammo -> change weapon
    if (!HasEnoughAmmo()) { SelectNewWeapon(); }

    return EEnd();
  };

  // ***************** FIRE GRENADELAUNCHER *****************
  FireGrenadeLauncher()
  {
    if (!HasEnoughAmmo()) {
      m_bFireWeapon = m_bHasAmmo = FALSE;
      jump Idle();
    }
    
    TM_START = _pTimer->CurrentTick();

    // remember time for spring release
    F_TEMP = _pTimer->CurrentTick();

    F_OFFSET_CHG = 0.0f;
    m_fWeaponDrawPower = 0.0f;
    m_tmDrawStartTime = _pTimer->CurrentTick();

    while (HoldingFire() && ((_pTimer->CurrentTick()-TM_START)<0.75f) )
    {
      autowait(_pTimer->TickQuantum);
      INDEX iPower = INDEX((_pTimer->CurrentTick()-TM_START)/_pTimer->TickQuantum);
      F_OFFSET_CHG = 0.125f/(iPower+2);
      m_fWeaponDrawPowerOld = m_fWeaponDrawPower;
      m_fWeaponDrawPower += F_OFFSET_CHG;
    }

    m_tmDrawStartTime = 0.0f;

    // release spring and fire one grenade
    if (HasEnoughAmmo())
    {
      // fire grenade
      INDEX iPower = INDEX((_pTimer->CurrentTick()-F_TEMP)/_pTimer->TickQuantum);
      FireGrenade(iPower);
      SpawnRangeSound(10.0f);
      if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Gnadelauncher");}
      DrainAmmo();

      // sound
      PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_GRENADELAUNCHER_FIRE, SOF_3D|SOF_VOLUMETRIC);

      if (GetHand() == E_HAND_MAIN) {
        GetAnimator()->FireAnimation(BODY_ANIM_MINIGUN_FIRELONG, 0);
      }

      // release spring
      TM_START = _pTimer->CurrentTick();
      m_fWeaponDrawPowerOld = m_fWeaponDrawPower;
      while (m_fWeaponDrawPower>0.0f)
      {
        autowait(_pTimer->TickQuantum);
        m_fWeaponDrawPowerOld = m_fWeaponDrawPower;
        m_fWeaponDrawPower -= F_OFFSET_CHG;
        m_fWeaponDrawPower = ClampDn(m_fWeaponDrawPower, 0.0f);
        F_OFFSET_CHG = F_OFFSET_CHG*10;
      }

      // reset moving part's offset
      ResetWeaponMovingOffset();

      // no ammo -> change weapon
      if (!HasEnoughAmmo()) {
        SelectNewWeapon();
      } else if (TRUE) {
        autowait(0.25f);
      }

    } else {
      //ASSERTALWAYS("GrenadeLauncher - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
    }

    return EEnd();
  };

  // ***************** FIRE FLAMER *****************
  FlamerStart()
  {
    if (!HasEnoughAmmo()) {
      m_bFireWeapon = m_bHasAmmo = FALSE;
      jump Idle();
    }
    
    m_tmFlamerStart=_pTimer->CurrentTick();
    m_tmFlamerStop=1e9;

    m_moWeapon.PlayAnim(FLAMER_ANIM_FIRESTART, 0);
    autowait(FLAMER_ANIM_FIRESTART_LENGTH);

    CSoundObject &soWeapon0 = GetWeaponSound(E_WEAPON_SOUND_0);
    CSoundObject &soWeapon2 = GetWeaponSound(E_WEAPON_SOUND_2);

    // play fire sound
    soWeapon0.Set3DParameters(50.0f, 5.0f, 2.0f, 0.31f);
    soWeapon2.Set3DParameters(50.0f, 5.0f, 2.0f, 0.3f);
    PlaySound(soWeapon0, SOUND_FL_FIRE, SOF_3D|SOF_LOOP|SOF_VOLUMETRIC);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("FlamethrowerFire");}
    PlaySound(soWeapon2, SOUND_FL_START, SOF_3D|SOF_VOLUMETRIC);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("FlamethrowerStart");}
    FireFlame();
    DrainAmmo();
    autowait(0.05f);
    jump FlamerFire();
  };

  FlamerFire()
  {
    // while holding fire
    while (HoldingFire() && HasEnoughAmmo())
    {
      // fire
      FireFlame();
      DrainAmmo();
      SpawnRangeSound(30.0f);
      autowait(0.05f);
      autowait(0.05f);
    }

    if (!HasEnoughAmmo()) {
      m_bHasAmmo = FALSE;
    }

    jump FlamerStop();
  };

  FlamerStop()
  {
    m_tmFlamerStop=_pTimer->CurrentTick();
    PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_FL_STOP, SOF_3D|SOF_VOLUMETRIC|SOF_SMOOTHCHANGE);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_StopEffect("FlamethrowerFire");}
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("FlamethrowerStop");}
    //PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_FL_STOP, SOF_3D|SOF_VOLUMETRIC|SOF_SMOOTHCHANGE);
    FireFlame();

    // link last flame with nothing (if not NULL or deleted)
    if (m_penFlame!=NULL && !(m_penFlame->GetFlags()&ENF_DELETED)) {
      ((CProjectileEntity&)*m_penFlame).m_penParticles = NULL;
      m_penFlame = NULL;
    }

    m_moWeapon.PlayAnim(FLAMER_ANIM_FIREEND, 0);
    autowait(FLAMER_ANIM_FIREEND_LENGTH);

    if (!HasEnoughAmmo()) {
      SelectNewWeapon(); // select new weapon
    }

    jump Idle();
  };

  // ***************** FIRE CHAINSAW *****************
  ChainsawFire()
  {
    // set the firing sound level
    CSoundObject &soWeapon0 = GetWeaponSound(E_WEAPON_SOUND_0);
    soWeapon0.Set3DParameters(50.0f, 5.0f, 1.5f, 1.0f);
    PlaySound(soWeapon0, SOUND_CS_BEGINFIRE, SOF_3D|SOF_VOLUMETRIC);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("ChainsawBeginFire");}

    // bring the chainsaw down to cutting height (fire position)
    m_moWeapon.PlayAnim(CHAINSAW_ANIM_WAIT2FIRE, 0);
    autowait(CHAINSAW_ANIM_WAIT2FIRE_LENGTH - 0.05f);

    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimation(BODY_ANIM_MINIGUN_FIRELONG, 0);
    }

    CModelObject *pmoTeeth=GetChainSawTeeth();
    if (pmoTeeth!=NULL) {
      pmoTeeth->PlayAnim(TEETH_ANIM_ROTATE, AOF_LOOPING|AOF_NORESTART);
    }

    // mute the chainsaw engine sound
    GetWeaponSound(E_WEAPON_SOUND_AMBIENT).Set3DParameters(30.0f, 3.0f, 0.5f, 1.0f);

    PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_CS_FIRE, SOF_3D|SOF_LOOP|SOF_VOLUMETRIC|SOF_SMOOTHCHANGE);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_StopEffect("ChainsawIdle");}
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("ChainsawFire");}

    m_moWeapon.PlayAnim(CHAINSAW_ANIM_FIRE, AOF_LOOPING|AOF_NORESTART);

    // start teeth rotation
    CModelObject *pmo1 = &(m_moWeapon.GetAttachmentModel(CHAINSAW_ATTACHMENT_BLADE)->amo_moModelObject);
    CModelObject *pmo2 = &(pmo1->GetAttachmentModel(BLADE_ATTACHMENT_TEETH)->amo_moModelObject);
    pmo2->PlayAnim(TEETH_ANIM_ROTATE, AOF_LOOPING);

    while (HoldingFire())// && m_iNapalm>0)
    {
      autowait(CHAINSAW_UPDATETIME);
      
      // 200 damage per second
      INDEX iCutDamage = ((GetSP()->sp_bCooperative) ? 200 : 250) * CHAINSAW_UPDATETIME * HEALTH_VALUE_MULTIPLIER;
      CutWithChainsaw(0, 0, 3.0f, 2.0f, 1.0f, iCutDamage);
    }

    // bring it back to idle position

    PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_CS_ENDFIRE, SOF_3D|SOF_VOLUMETRIC|SOF_SMOOTHCHANGE);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_StopEffect("ChainsawFire");}
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("ChainsawEnd");}
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("ChainsawIdle");}
//    PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_SILENCE, SOF_3D|SOF_VOLUMETRIC/*|SOF_SMOOTHCHANGE*/);

    // restore volume to engine sound
    GetWeaponSound(E_WEAPON_SOUND_AMBIENT).Set3DParameters(30.0f, 3.0f, 1.0f, 1.0f);

    m_moWeapon.PlayAnim(CHAINSAW_ANIM_FIRE2WAIT, 0);
    autowait(CHAINSAW_ANIM_FIRE2WAIT_LENGTH);

    // stop teeth rotation
    CModelObject *pmo1 = &(m_moWeapon.GetAttachmentModel(CHAINSAW_ATTACHMENT_BLADE)->amo_moModelObject);
    CModelObject *pmo2 = &(pmo1->GetAttachmentModel(BLADE_ATTACHMENT_TEETH)->amo_moModelObject);
    pmo2->PlayAnim(TEETH_ANIM_DEFAULT, 0);

    CModelObject *pmoTeeth=GetChainSawTeeth();
    if (pmoTeeth!=NULL) {
      pmoTeeth->PlayAnim(TEETH_ANIM_DEFAULT, 0);
    }

    jump Idle();
  };

  ChainsawBringUp()
  {
    // bring it back to idle position
    m_moWeapon.PlayAnim(CHAINSAW_ANIM_FIRE2WAIT, 0);
    autowait(CHAINSAW_ANIM_FIRE2WAIT_LENGTH);
    jump Idle();
  }

  // ***************** FIRE LASER *****************
  FirePlasmagun()
  {
    if (!HasEnoughAmmo()) {
      //ASSERTALWAYS("Plasmagun - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
      return EEnd();
    }

    autowait(0.05F);
    autowait(0.05F);
    m_moWeapon.PlayAnim(LASER_ANIM_FIRE, AOF_LOOPING|AOF_NORESTART);
    FirePlasmaProjectile(PRT_LASER_RAY);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Laser_fire");}
    DrainAmmo();

    // sound
    SpawnRangeSound(20.0f);

    // activate barrel anim
    switch (m_iLaserBarrel)
    {
      case 0: {   // barrel lu
        CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(LASER_ATTACHMENT_LEFTUP)->amo_moModelObject);
        pmo->PlayAnim(BARREL_ANIM_FIRE, 0);
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_0), SOUND_LASER_FIRE, SOF_3D|SOF_VOLUMETRIC);
        break; }
      case 3: {   // barrel rd
        CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(LASER_ATTACHMENT_RIGHTDOWN)->amo_moModelObject);
        pmo->PlayAnim(BARREL_ANIM_FIRE, 0);
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_1), SOUND_LASER_FIRE, SOF_3D|SOF_VOLUMETRIC);
        break; }
      case 1: {   // barrel ld
        CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(LASER_ATTACHMENT_LEFTDOWN)->amo_moModelObject);
        pmo->PlayAnim(BARREL_ANIM_FIRE, 0);
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_2), SOUND_LASER_FIRE, SOF_3D|SOF_VOLUMETRIC);
        break; }
      case 2: {   // barrel ru
        CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(LASER_ATTACHMENT_RIGHTUP)->amo_moModelObject);
        pmo->PlayAnim(BARREL_ANIM_FIRE, 0);
        PlaySound(GetWeaponSound(E_WEAPON_SOUND_3), SOUND_LASER_FIRE, SOF_3D|SOF_VOLUMETRIC);
        break; }
    }

    // next barrel
    m_iLaserBarrel = (m_iLaserBarrel+1)&3;

    // no ammo -> change weapon
    if (!HasEnoughAmmo()) { SelectNewWeapon(); }

    return EEnd();
  };
  
  // ***************** FIRE BEAMGUN *****************
  BeamgunStart()
  {
    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimation(BODY_ANIM_SHOTGUN_FIRESHORT, AOF_LOOPING);
    }

    // spawn beam
    m_penEnergyBeam = CreateEntity(GetPlacement(), CLASS_ENERGYBEAM);
    EEnergyBeam eeb;
    eeb.penOwner = this;
    m_penEnergyBeam->Initialize(eeb);

    // play anim
    m_moWeapon.PlayAnim(BEAMGUN_ANIM_FIRE, AOF_LOOPING|AOF_NORESTART);

    // play fire sound
    CSoundObject &soWeapon0 = GetWeaponSound(E_WEAPON_SOUND_0);
    soWeapon0.Set3DParameters(50.0f, 5.0f, 1.0f, 1.0f);      // fire
    PlaySound(soWeapon0, SOUND_BG_FIRE, SOF_3D|SOF_LOOP|SOF_VOLUMETRIC);
    return EEnd();
  };

  BeamgunStop()
  {
    if (GetHand() == E_HAND_MAIN) {
      GetAnimator()->FireAnimationOff();
    }

    DestroyEnergyBeam();

    GetWeaponSound(E_WEAPON_SOUND_0).Stop();
    jump Idle();
  };

  FireBeamgun()
  {
    if (!HasEnoughAmmo()) {
      //ASSERTALWAYS("Beamgun - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
      return EEnd();
    }

    // fire one cell
    FireEnergyBeam();
    DrainAmmo();
    SpawnRangeSound(20.0f);
    autowait(0.05f);

    // no napalm -> change weapon
    if (!HasEnoughAmmo()) { SelectNewWeapon(); }

    return EEnd();
  };

  // ***************** FIRE CANNON *****************

  CannonFireStart()
  {
    if (!HasEnoughAmmo()) {
      m_bFireWeapon = m_bHasAmmo = FALSE;
      jump Idle();
    }
    
    m_tmDrawStartTime = _pTimer->CurrentTick();
    TM_START = _pTimer->CurrentTick();
    F_OFFSET_CHG = 0.0f;
    m_fWeaponDrawPower = 0.0f;

    if (GetInventory()->GetAmmoQty(GetWeaponParams(E_WEAPON_CANNON)->GetDefaultAmmoType())&1) {
      CSoundObject &so = GetWeaponSound(E_WEAPON_SOUND_0);
      so.Set3DParameters(50.0f, 5.0f, 3.0f, 1.0f);
      PlaySound(so, SOUND_CANNON_PREPARE, SOF_3D|SOF_VOLUMETRIC);
    } else {
      CSoundObject &so = GetWeaponSound(E_WEAPON_SOUND_1);
      so.Set3DParameters(50.0f, 5.0f, 3.0f, 1.0f);
      PlaySound(so, SOUND_CANNON_PREPARE, SOF_3D|SOF_VOLUMETRIC);
    }

    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Canon_prepare");}

    while (HoldingFire() && ((_pTimer->CurrentTick()-TM_START)<1.0f) )
    {
      autowait(_pTimer->TickQuantum);
      INDEX iPower = INDEX((_pTimer->CurrentTick()-TM_START)/_pTimer->TickQuantum);
      F_OFFSET_CHG = 0.25f/(iPower+2);
      m_fWeaponDrawPowerOld = m_fWeaponDrawPower;
      m_fWeaponDrawPower += F_OFFSET_CHG;
    }

    m_tmDrawStartTime = 0.0f;

    if (GetInventory()->GetAmmoQty(GetWeaponParams(E_WEAPON_CANNON)->GetDefaultAmmoType())&1) {
      // turn off the sound
      GetWeaponSound(E_WEAPON_SOUND_0).Set3DParameters(50.0f, 5.0f, 0.0f, 1.0f);
    } else {
      // turn off the sound
      GetWeaponSound(E_WEAPON_SOUND_1).Set3DParameters(50.0f, 5.0f, 0.0f, 1.0f);
    }

    // fire one ball
    if (HasEnoughAmmo() && (GetCurrentWeapon() == E_WEAPON_CANNON))
    {
      INDEX iPower = INDEX((_pTimer->CurrentTick()-TM_START)/_pTimer->TickQuantum);
      
      if (GetHand() == E_HAND_MAIN) {
        GetAnimator()->FireAnimation(BODY_ANIM_MINIGUN_FIRELONG, 0);
      }

      FLOAT fRange, fFalloff;
      if (GetSP()->sp_bCooperative) {
        fRange = 100.0f;
        fFalloff = 25.0f;
      } else if (TRUE) {
        fRange = 150.0f;
        fFalloff = 30.0f;
      }

      // adjust volume of cannon firing acording to launch power
      if (GetInventory()->GetAmmoQty(GetWeaponParams(E_WEAPON_CANNON)->GetDefaultAmmoType())&1) {
        CSoundObject &so =  GetWeaponSound(E_WEAPON_SOUND_2);
        so.Set3DParameters(fRange, fFalloff, 2.0f+iPower*0.05f, 1.0f);
        PlaySound(so, SOUND_CANNON, SOF_3D|SOF_VOLUMETRIC);
      } else {
        CSoundObject &so =  GetWeaponSound(E_WEAPON_SOUND_3);
        so.Set3DParameters(fRange, fFalloff, 2.0f+iPower*0.05f, 1.0f);
        PlaySound(so, SOUND_CANNON, SOF_3D|SOF_VOLUMETRIC);
      }

      m_moWeapon.PlayAnim(CANNON_ANIM_FIRE, 0);
      FireCannonBall(iPower);

      if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Canon");}
      DrainAmmo();

      SpawnRangeSound(30.0f);

      TM_START = _pTimer->CurrentTick();
      m_fWeaponDrawPowerOld = m_fWeaponDrawPower;

      while (m_fWeaponDrawPower > 0.0f ||
        ((_pTimer->CurrentTick()-TM_START) < CANNON_ANIM_FIRE_LENGTH) )
      {
        autowait(_pTimer->TickQuantum);
        m_fWeaponDrawPowerOld = m_fWeaponDrawPower;
        m_fWeaponDrawPower -= F_OFFSET_CHG;
        m_fWeaponDrawPower = ClampDn(m_fWeaponDrawPower, 0.0f);
        F_OFFSET_CHG = F_OFFSET_CHG*2;
      }

      // reset moving part's offset
      ResetWeaponMovingOffset();

      // no cannon balls -> change weapon
      if (!HasEnoughAmmo() && (GetCurrentWeapon() == E_WEAPON_CANNON) )
      {
        SelectNewWeapon();
      }

    } else {
      //ASSERTALWAYS("Cannon - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
    }

    jump Idle();
  };

  // ***************** PLASMATHROWER *****************
  FirePlasmathrower()
  {
    if (!HasEnoughAmmo()) {
      //ASSERTALWAYS("Plasmathrower - Auto weapon change not working.");
      m_bFireWeapon = m_bHasAmmo = FALSE;
      return EEnd();
    }

    if (m_iLaserBarrel == 0) {
      autowait(0.05F);
    }

    autowait(0.05F);


    m_moWeapon.PlayAnim(LASER_ANIM_FIRE, AOF_LOOPING|AOF_NORESTART);
    FirePlasmaProjectile(PRT_LASER_RAY);
    if (_pNetwork->IsPlayerLocal(m_penPlayer)) {IFeel_PlayEffect("Laser_fire");}
    DrainAmmo();

    // sound
    SpawnRangeSound(20.0f);

    PlasmathrowerBarrelAnim();

    // next barrel
    m_iLaserBarrel = (++m_iLaserBarrel) % 3;

    // no ammo -> change weapon
    if (!HasEnoughAmmo()) { SelectNewWeapon(); }

    return EEnd();
  };

  /*
   *  >>>---   RELOAD WEAPON   ---<<<
   */
  Reload()
  {
    m_bReloadWeapon = FALSE;

    // reload
    if (GetCurrentWeapon() == E_WEAPON_REVOLVER) {
      autocall ReloadRevolver() EEnd;
    }

    jump Idle();
  };

  /*
   *  >>>---   BORING WEAPON ANIMATION   ---<<<
   */
  BoringWeaponAnimation()
  {
    // select new mode change animation
    FLOAT fWait = 0.0f;
    switch (GetCurrentWeapon())
    {
      case E_WEAPON_KNIFE: fWait = KnifeBoring(); break;
      case E_WEAPON_REVOLVER: fWait = RevolverBoring(); break;
      case E_WEAPON_SHOTGUN: fWait = SingleShotgunBoring(); break;
      case E_WEAPON_SUPERSHOTGUN: fWait = SuperShotgunBoring(); break;
      case E_WEAPON_MACHINEGUN: fWait = TommyGunBoring(); break;
      case E_WEAPON_SNIPER: fWait = SniperBoring(); break;
      case E_WEAPON_CHAINGUN: fWait = ChaingunBoring(); break;
      case E_WEAPON_ROCKETLAUNCHER: fWait = RocketLauncherBoring(); break;
      case E_WEAPON_GRENADELAUNCHER: fWait = GrenadeLauncherBoring(); break;
      case E_WEAPON_FLAMER: fWait = FlamerBoring(); break;
      case E_WEAPON_CHAINSAW: fWait = ChainsawBoring(); break;
      case E_WEAPON_PLASMAGUN: fWait = PlasmagunBoring(); break;
      case E_WEAPON_BEAMGUN: fWait = BeamgunBoring(); break; // [SSE] Beamgun
      case E_WEAPON_CANNON: fWait = CannonBoring(); break;
      default: ASSERTALWAYS("Unknown weapon.");
    }

    if (fWait > 0.0f) { autowait(fWait); }

    return EBegin();
  };



  /*
   *  >>>---   NO WEAPON ACTION   ---<<<
   */
  Idle()
  {
    wait()
    {
      on (EBegin) : {
        // play default anim
        PlayDefaultAnim();

        // weapon changed
        if (m_bChangeWeapon) {
          jump ChangeWeapon();
        }

        // fire pressed start firing
        if (m_bFireWeapon) {
          jump Fire();
        }

        // reload pressed
        if (m_bReloadWeapon) {
          jump Reload();
        }

        resume;
      }

      // select weapon
      on (ESelectWeapon eSelect) : {
        // try to change weapon
        SelectWeaponChange(eSelect.iWeapon);

        if (m_bChangeWeapon) {
          jump ChangeWeapon();
        }

        resume;
      }

      // fire pressed
      on (EFireWeapon) : {
        jump Fire();
      }

      // reload pressed
      on (EReloadWeapon) : {
        jump Reload();
      }

      // boring weapon animation
      on (EBoringWeapon) : {
        call BoringWeaponAnimation();
      }
    }
  };

  // weapons wait here while player is dead, so that stupid animations wouldn't play
  Stopped()
  {
    // make sure we restore all rockets if we are holding the rocket launcher
    if (GetCurrentWeapon() == E_WEAPON_ROCKETLAUNCHER) {
      CModelObject *pmo = &(m_moWeapon.GetAttachmentModel(ROCKETLAUNCHER_ATTACHMENT_ROCKET1)->amo_moModelObject);
      if (pmo) { pmo->StretchModel(FLOAT3D(1, 1, 1)); }
    }

    // kill all possible sounds, animations, etc
    ResetWeaponMovingOffset();
    GetWeaponSound(E_WEAPON_SOUND_0).Stop();
    GetWeaponSound(E_WEAPON_SOUND_1).Stop();
    GetWeaponSound(E_WEAPON_SOUND_2).Stop();
    GetWeaponSound(E_WEAPON_SOUND_3).Stop();
    PlayLightAnim(LIGHT_ANIM_NONE, 0);

    wait()
    {
      // after level change
      on (EPostLevelChange) : { return EBegin(); };
      on (EStart) : { return EBegin(); };
      otherwise() : { resume; };
    }
  }



  /*
   *  >>>---   M  A  I  N   ---<<<
   */
  Main(EWeaponsInit eInit)
  {
    // remember the initial parameters
    ASSERT(eInit.penOwner!=NULL);
    m_penPlayer = eInit.penOwner;

    // declare yourself as a void
    InitAsVoid();
    SetFlags(GetFlags()|ENF_CROSSESLEVELS|ENF_NOTIFYLEVELCHANGE);
    SetPhysicsFlags(EPF_MODEL_IMMATERIAL);
    SetCollisionFlags(ECF_IMMATERIAL);

    // set weapon model for current weapon
    SetWeaponModel();

    // play default anim
    PlayDefaultAnim();

    wait()
    {
      on (EBegin) : { call Idle(); }

      on (ESelectWeapon eSelect) : {
        // try to change weapon
        SelectWeaponChange(eSelect.iWeapon);
        resume;
      };

      // before level change
      on (EPreLevelChange) : {
        // stop everything
        m_bFireWeapon = FALSE;
        call Stopped();
        resume;
      }

      on (EFireWeapon) : {
        // start firing
        m_bFireWeapon = TRUE;
        resume;
      }

      on (EReleaseWeapon) : {
        // stop firing
        m_bFireWeapon = FALSE;
        resume;
      }

      on (EReloadWeapon) : {
        // reload wepon
        m_bReloadWeapon = TRUE;
        resume;
      }

      on (EStop) : { call Stopped(); }

      on (EEnd) : { stop; }
    }

    // cease to exist
    Destroy();
    return;
  };
};
