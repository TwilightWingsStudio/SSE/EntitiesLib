/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

400
%{
  #include "StdH.h"
  
  #include "Game/SEColors.h"
  
  #include "Entities/Common/PlayerControls.h"
  #include "Entities/Players/PlayerPawn.h"
%}

%{
  enum PlayerControllerFlags
  {
    PCF_INITIALIZED = (1L << 0),
  };
  
  #define PLF_DONTRENDER            (1UL<<5)
  
  static struct PlayerControls pctlCurrent;
  
  static FLOAT _tmLastUseOrCompPressed = -10.0f;  // for computer doubleclick
  
  // these define address and size of player controls structure
  DECL_DLL extern void *ctl_pvPlayerControls = &pctlCurrent;
  DECL_DLL extern const SLONG ctl_slPlayerControlsSize = sizeof(pctlCurrent);
  
  static FLOAT ctl_tmComputerDoubleClick = 0.5f; // double click delay for calling computer
  
  // modifier for axis strafing
  static FLOAT ctl_fAxisStrafingModifier = 1.0f;
  
  // speeds for button rotation
  static FLOAT ctl_fButtonRotationSpeedH = 300.0f;
  static FLOAT ctl_fButtonRotationSpeedP = 150.0f;
  static FLOAT ctl_fButtonRotationSpeedB = 150.0f;
  
  extern FLOAT plr_fSpeedForward;
  extern FLOAT plr_fSpeedBackward;
  extern FLOAT plr_fSpeedSide;
  extern FLOAT plr_fSpeedUp;
  
  static FLOAT net_tmLatencyAvg;

  extern FLOAT hud_tmLatencySnapshot = 1.0f;

  // called to compose action packet from current controls
  DECL_DLL void ctl_ComposeActionPacket(const CPlayerCharacter &pc, CPlayerAction &paAction, BOOL bPreScan)
  {
    // allow double axis controls
    paAction.pa_aRotation += paAction.pa_aViewRotation;

    CPlayerSettings *pps = (CPlayerSettings *)pc.pc_aubAppearance;

  //  CDebugF("compose: prescan %d, x:%g\n", bPreScan, paAction.pa_aRotation(1));
    // if strafing
    if (pctlCurrent.bStrafe) {
      // move rotation left/right into translation left/right
      paAction.pa_vTranslation(1) = -paAction.pa_aRotation(1)*ctl_fAxisStrafingModifier;
      paAction.pa_aRotation(1) = 0;
    }

    // if centering view
    if (pctlCurrent.bCenterView) {
      // don't allow moving view up/down
      paAction.pa_aRotation(2) = 0;
    }

    // multiply axis actions with speed
    paAction.pa_vTranslation(1) *= plr_fSpeedSide;
    paAction.pa_vTranslation(2) *= plr_fSpeedUp;

    if (paAction.pa_vTranslation(3)<0) {
      paAction.pa_vTranslation(3) *= plr_fSpeedForward;
    } else {
      paAction.pa_vTranslation(3) *= plr_fSpeedBackward;
    }

    // find local player, if any
    CPlayerPawnEntity *penThis = NULL;
    INDEX ctPlayers = CEntity::GetMaxPlayers();

    for (INDEX iPlayer = 0; iPlayer<ctPlayers; iPlayer++)
    {
      CEntity *pen = CEntity::GetPlayerEntity(iPlayer);
      
      if (pen == NULL) {
        continue;
      }

      CPawnEntity *penPawn = static_cast<CPawnEntity *>(pen);
      CPlayerControllerEntity *penController = static_cast<CPlayerControllerEntity *>(penPawn->GetController());

      if (penController->en_pcCharacter == pc) {
        penThis = static_cast<CPlayerPawnEntity *>(penPawn);
        break;
      }
    }

    // if not found then do nothing
    if (penThis==NULL) {
      return;
    }

    ANGLE3D &aLocalRot = penThis->m_aLocalRotation;
    ANGLE3D &aLocalViewRot = penThis->m_aLocalViewRotation;
    FLOAT3D &vLocalTrans = penThis->m_vLocalTranslation;

    // accumulate local rotation
    aLocalRot     += paAction.pa_aRotation;
    aLocalViewRot += paAction.pa_aViewRotation;
    vLocalTrans   += paAction.pa_vTranslation;

    // if prescanning then no button checking
    if (bPreScan) {
      return;
    }

    // add button movement/rotation/look actions to the axis actions
    if (pctlCurrent.bMoveForward  ) paAction.pa_vTranslation(3) -= plr_fSpeedForward;
    if (pctlCurrent.bMoveBackward ) paAction.pa_vTranslation(3) += plr_fSpeedBackward;
    if (pctlCurrent.bMoveLeft  || pctlCurrent.bStrafe&&pctlCurrent.bTurnLeft) paAction.pa_vTranslation(1) -= plr_fSpeedSide;
    if (pctlCurrent.bMoveRight || pctlCurrent.bStrafe&&pctlCurrent.bTurnRight) paAction.pa_vTranslation(1) += plr_fSpeedSide;
    if (pctlCurrent.bMoveUp       ) paAction.pa_vTranslation(2) += plr_fSpeedUp;
    if (pctlCurrent.bMoveDown     ) paAction.pa_vTranslation(2) -= plr_fSpeedUp;

    const FLOAT fQuantum = _pTimer->TickQuantum;
    if (pctlCurrent.bTurnLeft  && !pctlCurrent.bStrafe) aLocalRot(1) += ctl_fButtonRotationSpeedH*fQuantum;
    if (pctlCurrent.bTurnRight && !pctlCurrent.bStrafe) aLocalRot(1) -= ctl_fButtonRotationSpeedH*fQuantum;
    if (pctlCurrent.bTurnUp           ) aLocalRot(2) += ctl_fButtonRotationSpeedP*fQuantum;
    if (pctlCurrent.bTurnDown         ) aLocalRot(2) -= ctl_fButtonRotationSpeedP*fQuantum;
    if (pctlCurrent.bTurnBankingLeft  ) aLocalRot(3) += ctl_fButtonRotationSpeedB*fQuantum;
    if (pctlCurrent.bTurnBankingRight ) aLocalRot(3) -= ctl_fButtonRotationSpeedB*fQuantum;

    if (pctlCurrent.bLookLeft         ) aLocalViewRot(1) += ctl_fButtonRotationSpeedH*fQuantum;
    if (pctlCurrent.bLookRight        ) aLocalViewRot(1) -= ctl_fButtonRotationSpeedH*fQuantum;
    if (pctlCurrent.bLookUp           ) aLocalViewRot(2) += ctl_fButtonRotationSpeedP*fQuantum;
    if (pctlCurrent.bLookDown         ) aLocalViewRot(2) -= ctl_fButtonRotationSpeedP*fQuantum;
    if (pctlCurrent.bLookBankingLeft  ) aLocalViewRot(3) += ctl_fButtonRotationSpeedB*fQuantum;
    if (pctlCurrent.bLookBankingRight ) aLocalViewRot(3) -= ctl_fButtonRotationSpeedB*fQuantum;

    // use current accumulated rotation
    paAction.pa_aRotation     = aLocalRot;
    paAction.pa_aViewRotation = aLocalViewRot;
    //paAction.pa_vTranslation  = vLocalTrans;

    // if walking
    if (pctlCurrent.bWalk) {
      // make forward/backward and sidestep speeds slower
      paAction.pa_vTranslation(3) /= 2.0f;
      paAction.pa_vTranslation(1) /= 2.0f;
    }

    // reset all button actions
    paAction.pa_ulButtons = 0;

    // set weapon selection bits
    for (INDEX i=1; i<MAX_WEAPONS; i++)
    {
      if (pctlCurrent.bSelectWeapon[i]) {
        paAction.pa_ulButtons = i<<PLACT_SELECT_WEAPON_SHIFT;
        break;
      }
    }

    // set weapon selection bits
    for (INDEX i = 1; i < MAX_WEAPONS; i++)
    {
      if (pctlCurrent.bFavoriteWeapon[i]) {
        paAction.pa_ulButtons = i << PLACT_FAVORITE_WEAPON_SHIFT;
        break;
      }
    }

    // set button pressed flags
    if (pctlCurrent.bWeaponNext)    paAction.pa_ulButtons |= PLACT_WEAPON_NEXT;
    if (pctlCurrent.bWeaponPrev)    paAction.pa_ulButtons |= PLACT_WEAPON_PREV;
    if (pctlCurrent.bWeaponFlip)    paAction.pa_ulButtons |= PLACT_WEAPON_FLIP;
    if (pctlCurrent.bFire)          paAction.pa_ulButtons |= PLACT_FIRE;
    if (pctlCurrent.bSecondaryFire) paAction.pa_ulButtons |= PLACT_SECONDARY_FIRE;
    if (pctlCurrent.bReload)        paAction.pa_ulButtons |= PLACT_RELOAD;
    if (pctlCurrent.bUse)           paAction.pa_ulButtons |= PLACT_USE;
    if (pctlCurrent.bSecondaryUse)  paAction.pa_ulButtons |= PLACT_SECONDARY_USE;
    if (pctlCurrent.bComputer)      paAction.pa_ulButtons |= PLACT_COMPUTER;
    if (pctlCurrent.b3rdPersonView) paAction.pa_ulButtons |= PLACT_3RD_PERSON_VIEW;
    if (pctlCurrent.bCenterView)    paAction.pa_ulButtons |= PLACT_CENTER_VIEW;
    if (pctlCurrent.bAimDownSight)  paAction.pa_ulButtons |= PLACT_AIM_DOWN_SIGHT;

    // is 'use' being held?
    if (pctlCurrent.bSniperZoomIn)  paAction.pa_ulButtons |= PLACT_SNIPER_ZOOMIN;
    if (pctlCurrent.bSniperZoomOut) paAction.pa_ulButtons |= PLACT_SNIPER_ZOOMOUT;
    if (pctlCurrent.bGadget)        paAction.pa_ulButtons |= PLACT_GADGET;
    if (pctlCurrent.bWeaponSelectionModifier) paAction.pa_ulButtons |= PLACT_SELECT_WEAPON_MOD;

    // if userorcomp just pressed
    if (pctlCurrent.bUseOrComputer && !pctlCurrent.bUseOrComputerLast) {
      // if double-click is off
      if (ctl_tmComputerDoubleClick==0 || (pps->ps_ulFlags&PSF_COMPSINGLECLICK)) {
        // press both
        paAction.pa_ulButtons |= PLACT_USE|PLACT_COMPUTER;
      // if double-click is on
      } else {
        // if double click
        if (_pTimer->GetRealTimeTick()<=_tmLastUseOrCompPressed+ctl_tmComputerDoubleClick) {
          // computer pressed
          paAction.pa_ulButtons |= PLACT_COMPUTER;
        // if single click
        } else {
          // use pressed
          paAction.pa_ulButtons |= PLACT_USE;
        }
      }
      _tmLastUseOrCompPressed = _pTimer->GetRealTimeTick();
    }

    // remember old userorcomp pressed state
    pctlCurrent.bUseOrComputerLast = pctlCurrent.bUseOrComputer;
  };
  
  void CMyPlayerControllerEntity_OnInitClass(void)
  {
    // clear current player controls
    memset(&pctlCurrent, 0, sizeof(pctlCurrent));

    // declare player control variables
    _pShell->DeclareSymbol("user INDEX ctl_bMoveForward;",  &pctlCurrent.bMoveForward);
    _pShell->DeclareSymbol("user INDEX ctl_bMoveBackward;", &pctlCurrent.bMoveBackward);
    _pShell->DeclareSymbol("user INDEX ctl_bMoveLeft;",     &pctlCurrent.bMoveLeft);
    _pShell->DeclareSymbol("user INDEX ctl_bMoveRight;",    &pctlCurrent.bMoveRight);
    _pShell->DeclareSymbol("user INDEX ctl_bMoveUp;",       &pctlCurrent.bMoveUp);
    _pShell->DeclareSymbol("user INDEX ctl_bMoveDown;",     &pctlCurrent.bMoveDown);
    _pShell->DeclareSymbol("user INDEX ctl_bTurnLeft;",         &pctlCurrent.bTurnLeft);
    _pShell->DeclareSymbol("user INDEX ctl_bTurnRight;",        &pctlCurrent.bTurnRight);
    _pShell->DeclareSymbol("user INDEX ctl_bTurnUp;",           &pctlCurrent.bTurnUp);
    _pShell->DeclareSymbol("user INDEX ctl_bTurnDown;",         &pctlCurrent.bTurnDown);
    _pShell->DeclareSymbol("user INDEX ctl_bTurnBankingLeft;",  &pctlCurrent.bTurnBankingLeft);
    _pShell->DeclareSymbol("user INDEX ctl_bTurnBankingRight;", &pctlCurrent.bTurnBankingRight);
    _pShell->DeclareSymbol("user INDEX ctl_bCenterView;",       &pctlCurrent.bCenterView);
    _pShell->DeclareSymbol("user INDEX ctl_bLookLeft;",         &pctlCurrent.bLookLeft);
    _pShell->DeclareSymbol("user INDEX ctl_bLookRight;",        &pctlCurrent.bLookRight);
    _pShell->DeclareSymbol("user INDEX ctl_bLookUp;",           &pctlCurrent.bLookUp);
    _pShell->DeclareSymbol("user INDEX ctl_bLookDown;",         &pctlCurrent.bLookDown);
    _pShell->DeclareSymbol("user INDEX ctl_bLookBankingLeft;",  &pctlCurrent.bLookBankingLeft);
    _pShell->DeclareSymbol("user INDEX ctl_bLookBankingRight;", &pctlCurrent.bLookBankingRight );
    _pShell->DeclareSymbol("user INDEX ctl_bWalk;",           &pctlCurrent.bWalk);
    _pShell->DeclareSymbol("user INDEX ctl_bStrafe;",         &pctlCurrent.bStrafe);
    _pShell->DeclareSymbol("user INDEX ctl_bFire;",           &pctlCurrent.bFire);
    _pShell->DeclareSymbol("user INDEX ctl_bSecondaryFire;",  &pctlCurrent.bSecondaryFire); // [SSE]
    _pShell->DeclareSymbol("user INDEX ctl_bReload;",         &pctlCurrent.bReload);
    _pShell->DeclareSymbol("user INDEX ctl_bAimDownSight;",   &pctlCurrent.bAimDownSight); // [SSE]
    _pShell->DeclareSymbol("user INDEX ctl_bUse;",            &pctlCurrent.bUse);
    _pShell->DeclareSymbol("user INDEX ctl_bSecondaryUse;",   &pctlCurrent.bSecondaryUse);
    _pShell->DeclareSymbol("user INDEX ctl_bComputer;",       &pctlCurrent.bComputer);
    _pShell->DeclareSymbol("user INDEX ctl_bUseOrComputer;",  &pctlCurrent.bUseOrComputer);
    _pShell->DeclareSymbol("user INDEX ctl_b3rdPersonView;",  &pctlCurrent.b3rdPersonView);
    _pShell->DeclareSymbol("user INDEX ctl_bWeaponNext;",         &pctlCurrent.bWeaponNext);
    _pShell->DeclareSymbol("user INDEX ctl_bWeaponPrev;",         &pctlCurrent.bWeaponPrev);
    _pShell->DeclareSymbol("user INDEX ctl_bWeaponFlip;",         &pctlCurrent.bWeaponFlip);
    _pShell->DeclareSymbol("user INDEX ctl_bSelectWeapon[30+1];", &pctlCurrent.bSelectWeapon);
    _pShell->DeclareSymbol("user INDEX ctl_bWeaponSelectionModifier;", &pctlCurrent.bWeaponSelectionModifier);
    _pShell->DeclareSymbol("user INDEX ctl_bFavoriteWeapon[30+1];", &pctlCurrent.bFavoriteWeapon);
    _pShell->DeclareSymbol("persistent user FLOAT ctl_tmComputerDoubleClick;", &ctl_tmComputerDoubleClick);
    _pShell->DeclareSymbol("persistent user FLOAT ctl_fButtonRotationSpeedH;", &ctl_fButtonRotationSpeedH);
    _pShell->DeclareSymbol("persistent user FLOAT ctl_fButtonRotationSpeedP;", &ctl_fButtonRotationSpeedP);
    _pShell->DeclareSymbol("persistent user FLOAT ctl_fButtonRotationSpeedB;", &ctl_fButtonRotationSpeedB);
    _pShell->DeclareSymbol("persistent user FLOAT ctl_fAxisStrafingModifier;", &ctl_fAxisStrafingModifier);

    //new
    _pShell->DeclareSymbol("user INDEX ctl_bSniperZoomIn;",         &pctlCurrent.bSniperZoomIn);
    _pShell->DeclareSymbol("user INDEX ctl_bSniperZoomOut;",        &pctlCurrent.bSniperZoomOut);
    _pShell->DeclareSymbol("user INDEX ctl_bGadget;",               &pctlCurrent.bGadget);
    
    _pShell->DeclareSymbol("user const FLOAT net_tmLatencyAvg;", &net_tmLatencyAvg);
    
    _pShell->DeclareSymbol("persistent user FLOAT hud_tmLatencySnapshot;",  &hud_tmLatencySnapshot);
  }
  
  void CMyPlayerControllerEntity_OnEndClass(void)
  {
  }
%}

class export CMyPlayerControllerEntity : CPlayerControllerEntity {
name      "MyPlayerController";
thumbnail "";
features "ImplementsOnInitClass", "ImplementsOnEndClass", "CanBePredictable";

properties:
 1 INDEX m_ulFlags = 0,
 
 89 FLOAT m_tmLatency = 0.0f,               // player-server latency (in seconds)
 // for latency averaging
 88 FLOAT m_tmLatencyLastAvg = 0.0f,
 87 FLOAT m_tmLatencyAvgSum = 0.0f,
 86 INDEX m_ctLatencyAvg = 0,

{
}

resources:

  1 class   CLASS_SPECTATOR "Classes\\SpectatorPawn.ecl",
  2 class   CLASS_PLAYER    "Classes\\PlayerPawn.ecl",
  
functions:

  // add to prediction any entities that this entity depends on
  void AddDependentsToPrediction(void)
  {
    if (GetPawn()) {
      GetPawn()->AddToPrediction();
    }
  }
  
  CPlayerSettings *GetSettings(void)
  {
    return (CPlayerSettings *)en_pcCharacter.pc_aubAppearance;
  }
  
  // update smoothed (average latency)
  void UpdateLatency(FLOAT tmLatencyNow)
  {
    TIME tmNow = _pTimer->GetHighPrecisionTimer().GetSeconds();

    // if not enough time passed
    if (tmNow<m_tmLatencyLastAvg+hud_tmLatencySnapshot) {
      // just sum
      m_tmLatencyAvgSum += tmLatencyNow;
      m_ctLatencyAvg++;

    // if enough time passed
    } else {
      // calculate average
      m_tmLatency = m_tmLatencyAvgSum/m_ctLatencyAvg;
      // reset counters
      m_tmLatencyAvgSum = 0.0f;
      m_ctLatencyAvg = 0;
      m_tmLatencyLastAvg = tmNow;
    }

    if (_pNetwork->IsPlayerLocal(this)) {
      en_tmPing = m_tmLatency;
      net_tmLatencyAvg = en_tmPing;
    }
  }

  //! Apply the action packet to the entity movement.
  export virtual void ApplyAction(const CPlayerAction &pa, FLOAT tmLatency)
  {
    if (!(m_ulFlags&PCF_INITIALIZED) || GetPawn() == NULL) {
      return;
    }
    
    CPlayerPawnEntity *penPlayer = static_cast<CPlayerPawnEntity *>(GetPawn());

    // adjust prediction for remote players only
    CEntity *penMe = this;
    if (IsPredictor()) {
      penMe = penMe->GetPredicted();
    }

    SetPredictable(!_pNetwork->IsPlayerLocal(penMe));
    
    penPlayer->ApplyAction(pa, tmLatency);
    
    // keep latency for eventual printout
    UpdateLatency(tmLatency);
  };
  
  //! Called when player is disconnected.
  export virtual void Disconnect(void)
  {
    // clear the character, so we don't get re-connected to same entity
    en_pcCharacter = CPlayerCharacter();

    if (GetPawn() == NULL) {
      return;
    }
    
    CPlayerPawnEntity *penPlayer = static_cast<CPlayerPawnEntity *>(GetPawn());
    
    penPlayer->Disconnect();
  };
  
  //! Called when player character is changed.
  export virtual void CharacterChanged(const CPlayerCharacter &pcNew)
  {
    // remember original character
    CPlayerCharacter pcOrg = en_pcCharacter;

    // set the new character
    en_pcCharacter = pcNew;
    //ValidateCharacter();

    // if the name has changed
    if (pcOrg.GetName() != pcNew.GetName()) {
      // report that
      CInfoF(TRANS("%s is now known as %s\n"), pcOrg.GetNameForPrinting(), pcNew.GetNameForPrinting());
    }

    // if the team has changed
    if (pcOrg.GetTeam() != pcNew.GetTeam()) {
      // report that
      CInfoF(TRANS("%s switched to team %s\n"), pcNew.GetNameForPrinting(), pcNew.GetTeamForPrinting());
    }
    
    CPlayerPawnEntity *penPlayer = static_cast<CPlayerPawnEntity *>(GetPawn());
    penPlayer->CharacterChanged(pcOrg, pcNew);

    CPlayerControllerEntity::CharacterChanged(pcNew);
  };
  
  //! Provide info for GameAgent enumeration.
  export virtual void GetGameAgentPlayerInfo(INDEX iPlayer, CTString &strOut)
  {
    if (GetPawn() == NULL) {
      return;
    }
    
    CPlayerPawnEntity *penPlayer = static_cast<CPlayerPawnEntity *>(GetPawn());
    penPlayer->GetGameAgentPlayerInfo(iPlayer, strOut);
  };

  //! Provide info for MSLegacy enumeration.
  export virtual void GetMSLegacyPlayerInf(INDEX iPlayer, CTString &strOut)
  {
    if (GetPawn() == NULL) {
      return;
    }
    
    CPlayerPawnEntity *penPlayer = static_cast<CPlayerPawnEntity *>(GetPawn());
    penPlayer->GetMSLegacyPlayerInf(iPlayer, strOut);
  };
  
  void RenderGameView(CDrawPort *pdp, void *pvUserData)
  {
    BOOL bNotInitialized = !(m_ulFlags&PCF_INITIALIZED);
    
    // if not yet initialized
    if (bNotInitialized || GetPawn() == NULL || static_cast<CPlayerPawnEntity *>(GetPawn())->m_ulFlags&PLF_DONTRENDER) {
      pdp->Unlock();

      // render dummy view on the right drawport
      CDrawPort dpView(pdp, TRUE);

      if (dpView.Lock()) {
        RenderDummyView(&dpView);
        dpView.Unlock();
      }

      pdp->Lock();
      return;
    }

    GetPawn()->RenderGameView(pdp, pvUserData);
  }
  
  // render dummy view (not connected yet)
  void RenderDummyView(CDrawPort *pdp)
  {
    // clear screen
    pdp->Fill(C_BLACK|CT_OPAQUE);

    // if not single player
    if (!GetSP()->sp_bSinglePlayer) {
      // print a message
      PIX pixDPWidth  = pdp->GetWidth();
      PIX pixDPHeight = pdp->GetHeight();
      FLOAT fScale = (FLOAT)pixDPWidth/640.0f;
      pdp->SetFont(_pfdDisplayFont);
      pdp->SetTextScaling(fScale);
      pdp->SetTextAspect(1.0f);
      CTString strMsg;
      strMsg.PrintF(TRANS("%s connected"), GetPlayerName());
      pdp->PutTextCXY(strMsg, pixDPWidth*0.5f, pixDPHeight*0.5f, SE_COL_BLUE_NEUTRAL_LT|CT_OPAQUE);
    }
  }
  
  void InitializePlayer()
  {
    m_ulFlags &= PCF_INITIALIZED;
  }

procedures:
  // must have at least one procedure per class
  Main()
  {
    InitAsVoid();
    
    SetFlags(GetFlags()|ENF_CROSSESLEVELS|ENF_NOTIFYLEVELCHANGE);
    
    // wait a bit to allow other entities to start
    // this is 4 ticks, it has to be at least more than musicchanger for enemy counting
    wait(0.2f)
    {
      on (EBegin) : { resume; }
      on (ETimer) : { stop; }
      on (EDisconnected) : {
        Destroy();
        return;
      }
    }

    // do not use predictor if not yet initialized
    if (IsPredictor()) { // !!!!####
      Destroy();
      return;
    }
    
    m_ulFlags |= PCF_INITIALIZED;
    
    CEntity *penPawn = CreateEntity(GetPlacement(), CLASS_PLAYER);
    ConnectToPawn(penPawn); // Connect before initialization or we'll get crash!
    penPawn->Initialize();   

    wait()
    {
      on (EBegin) : {
        resume;
      }
      
      on (EPreLevelChange) : {
        m_ulFlags &= ~PCF_INITIALIZED;
        resume;
      }
      
      on (EPostLevelChange) :
      {
        m_ulFlags |= PCF_INITIALIZED;
        resume;
      }
    }
  };
};
