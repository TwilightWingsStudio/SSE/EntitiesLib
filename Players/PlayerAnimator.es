/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

406
%{
  #include "StdH.h"

  #include "ModelsMP/Player/SeriousSam/Player.h"
  #include "ModelsMP/Player/SeriousSam/Body.h"
  #include "ModelsMP/Player/SeriousSam/Head.h"

  #include "Models/Weapons/Knife/KnifeItem.h"
  #include "Models/Weapons/Colt/ColtItem.h"
  #include "Models/Weapons/Colt/ColtMain.h"
  #include "Models/Weapons/SingleShotgun/SingleShotgunItem.h"
  #include "Models/Weapons/SingleShotgun/Barrels.h"
  #include "Models/Weapons/DoubleShotgun/DoubleShotgunItem.h"
  #include "Models/Weapons/DoubleShotgun/Dshotgunbarrels.h"
  #include "Models/Weapons/TommyGun/TommyGunItem.h"
  #include "Models/Weapons/TommyGun/Body.h"
  #include "Models/Weapons/MiniGun/MiniGunItem.h"
  #include "Models/Weapons/MiniGun/Body.h"
  #include "Models/Weapons/GrenadeLauncher/GrenadeLauncherItem.h"
  #include "Models/Weapons/RocketLauncher/RocketLauncherItem.h"
  #include "ModelsMP/Weapons/Sniper/SniperItem.h"
  #include "ModelsMP/Weapons/Sniper/Sniper.h"
  #include "ModelsMP/Weapons/Flamer/FlamerItem.h"
  #include "ModelsMP/Weapons/Flamer/Body.h"
  //#include "ModelsMP/Weapons/Chainsaw/ChainsawItem.h"
  #include "ModelsMP/Weapons/Chainsaw/ChainsawForPlayer.h"
  #include "ModelsMP/Weapons/Chainsaw/BladeForPlayer.h"
  #include "ModelsMP/Weapons/Chainsaw/Body.h"
  #include "Models/Weapons/Laser/LaserItem.h"
  #include "Models/Weapons/Cannon/Cannon.h"
  
  #include "Models/Weapons/Beamgun/BeamgunItem.h"
  #include "Models/Weapons/Beamgun/Effect01.h"
%}

uses "Entities/Players/PlayerPawn";
uses "Entities/Players/PlayerWeapon";

// input parameter for animator
event EAnimatorInit
{
  CEntityPointer penPlayer,            // player owns it
};

%{
  // animator action
  enum AnimatorAction
  {
    AA_JUMPDOWN = 0,
    AA_CROUCH,
    AA_RISE,
    AA_PULLWEAPON,
    AA_ATTACK,
  };

  // fire flare specific
  #define FLARE_NONE 0
  #define FLARE_REMOVE 1
  #define FLARE_ADD 2

  extern FLOAT plr_fBreathingStrength;
  extern FLOAT plr_fViewDampFactor;
  extern FLOAT plr_fViewDampLimitGroundUp;
  extern FLOAT plr_fViewDampLimitGroundDn;
  extern FLOAT plr_fViewDampLimitWater;
  extern FLOAT wpn_fRecoilSpeed[17];
  extern FLOAT wpn_fRecoilLimit[17];
  extern FLOAT wpn_fRecoilDampUp[17];
  extern FLOAT wpn_fRecoilDampDn[17];
  extern FLOAT wpn_fRecoilOffset[17];
  extern FLOAT wpn_fRecoilFactorP[17];
  extern FLOAT wpn_fRecoilFactorZ[17];

  void CPlayerAnimatorEntity_Precache(ULONG ulAvailable)
  {
    CLibEntityClass *pdec = &CPlayerAnimatorEntity_DLLClass;

    pdec->PrecacheTexture(TEX_SPEC_MEDIUM          );
    pdec->PrecacheModel(MODEL_GOLDAMON);
    pdec->PrecacheTexture(TEXTURE_GOLDAMON);
    pdec->PrecacheTexture(TEX_REFL_GOLD01);
    pdec->PrecacheClass(CLASS_REMINDER);

    // precache other weapons if available
    if (ulAvailable & (1 << (E_WEAPON_KNIFE - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_KNIFE_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_REVOLVER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_COLT_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_SHOTGUN - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_SINGLESHOTGUN_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_SUPERSHOTGUN - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_DOUBLESHOTGUN_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_MACHINEGUN - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_TOMMYGUN_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_SNIPER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_SNIPER_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_CHAINGUN - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_MINIGUN_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_ROCKETLAUNCHER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_ROCKETLAUNCHER_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_GRENADELAUNCHER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_GRENADELAUNCHER_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_CHAINSAW - 1))) {
      pdec->PrecacheModelConfig(MODEL_CHAINSAW_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_FLAMER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_FLAMER_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_PLASMAGUN - 1))) {
      pdec->PrecacheModelConfig(MODEL_LASER_CFG);
    }
    
    // [SSE] Beamgun
    if (ulAvailable & (1 << (E_WEAPON_BEAMGUN - 1))) {
      pdec->PrecacheModelConfig(MODEL_BEAMGUN_CFG);
    }

    if (ulAvailable & (1 << (E_WEAPON_CANNON - 1))) {
      pdec->PrecacheModelConfig(MODEL_CANNON_CFG);
    }

    // [SEE] Plasmathrower
    if (ulAvailable & (1 << (E_WEAPON_PLASMATHROWER - 1))) {
      pdec->PrecacheModelConfig(MODEL_LASER_CFG);
    }

    // [SEE] Minelayer
    if (ulAvailable & (1 << (E_WEAPON_MINELAYER - 1)) ) {
      pdec->PrecacheModelConfig(MODEL_MINELAYER_CFG);
    }

    // precache shells that drop when firing
    extern void CPlayerWeaponsEffectsEntity_Precache(void);
    CPlayerWeaponsEffectsEntity_Precache();
  }
%}

class export CPlayerAnimatorEntity: CRationalEntity {
name      "Player Animator";
thumbnail "";
features "CanBePredictable";

properties:

  1 CEntityPointer m_penPlayer,               // player which owns it

  5 BOOL m_bReference=FALSE,                  // player has reference (floor)
  6 FLOAT m_fLastActionTime = 0.0f,           // last action time for boring weapon animations
  7 INDEX m_iContent = 0,                     // content type index
  8 BOOL m_bWaitJumpAnim = FALSE,             // wait legs anim (for jump end)
  9 BOOL m_bCrouch = FALSE,                   // player crouch state
 10 BOOL m_iCrouchDownWait = FALSE,           // wait for crouch down
 11 BOOL m_iRiseUpWait = FALSE,               // wait for rise up
 12 BOOL m_bChangeWeapon = FALSE,             // wait for weapon change
 13 BOOL m_bSwim = FALSE,                     // player in water
 14 INDEX m_iFlare = FLARE_REMOVE,            // 0-none, 1-remove, 2-add
 15 INDEX m_iSecondFlare = FLARE_REMOVE,      // 0-none, 1-remove, 2-add
 16 BOOL m_bAttacking = FALSE,                // currently firing weapon/swinging knife
 19 FLOAT m_tmAttackingDue = -1.0f,           // when firing animation is due
 17 FLOAT m_tmFlareAdded = -1.0f,             // for better flare add/remove
 18 BOOL m_bDisableAnimating = FALSE,

// player soft eyes on Y axis
 20 FLOAT3D m_vLastPlayerPosition = FLOAT3D(0, 0, 0), // last player position for eyes movement
 21 FLOAT m_fEyesYLastOffset = 0.0f,                 // eyes offset from player position
 22 FLOAT m_fEyesYOffset = 0.0f,
 23 FLOAT m_fEyesYSpeed = 0.0f,                      // eyes speed
 27 FLOAT m_fWeaponYLastOffset = 0.0f,                 // eyes offset from player position
 28 FLOAT m_fWeaponYOffset = 0.0f,
 29 FLOAT m_fWeaponYSpeed = 0.0f,                      // eyes speed
 // recoil pitch
// 24 FLOAT m_fRecoilLastOffset = 0.0f,   // eyes offset from player position
// 25 FLOAT m_fRecoilOffset = 0.0f,
// 26 FLOAT m_fRecoilSpeed = 0.0f,        // eyes speed

// player banking when moving
 30 BOOL m_bMoving = FALSE,
 31 FLOAT m_fMoveLastBanking = 0.0f,
 32 FLOAT m_fMoveBanking = 0.0f,
 33 BOOL m_iMovingSide = 0,
 34 BOOL m_bSidestepBankingLeft = FALSE,
 35 BOOL m_bSidestepBankingRight = FALSE,
 36 FLOAT m_fSidestepLastBanking = 0.0f,
 37 FLOAT m_fSidestepBanking = 0.0f,
 38 INDEX m_iWeaponLast = -1,
 39 FLOAT m_fBodyAnimTime = -1.0f,

{
  CModelObject *pmoModel;
}

resources:
  1 class   CLASS_REMINDER              "Classes\\Reminder.ecl",

 20 modelcfg MODEL_KNIFE_CFG           "Models\\Weapons\\Knife\\Knife_Item.vmc",
 21 modelcfg MODEL_COLT_CFG            "Models\\Weapons\\Colt\\Colt_Item.vmc",
 22 modelcfg MODEL_SINGLESHOTGUN_CFG   "Models\\Weapons\\SingleShotgun\\SingleShotgun_Item.vmc",
 23 modelcfg MODEL_DOUBLESHOTGUN_CFG   "Models\\Weapons\\DoubleShotgun\\DoubleShotgun_Item.vmc",
 24 modelcfg MODEL_TOMMYGUN_CFG        "Models\\Weapons\\TommyGun\\TommyGun_Item.vmc",
 25 modelcfg MODEL_MINIGUN_CFG         "Models\\Weapons\\MiniGun\\MiniGun_Item.vmc",
 26 modelcfg MODEL_ROCKETLAUNCHER_CFG  "Models\\Weapons\\RocketLauncher\\RocketLauncher_Item.vmc",
 27 modelcfg MODEL_GRENADELAUNCHER_CFG "Models\\Weapons\\GrenadeLauncher\\GrenadeLauncher_Item.vmc",
 28 modelcfg MODEL_SNIPER_CFG          "Models\\Weapons\\Sniper\\Sniper_Item.vmc",
 29 modelcfg MODEL_FLAMER_CFG          "Models\\Weapons\\Flamer\\Flamer_Item.vmc",
 30 modelcfg MODEL_LASER_CFG           "Models\\Weapons\\Laser\\Laser_Item.vmc",
 31 modelcfg MODEL_CHAINSAW_CFG        "Models\\Weapons\\Chainsaw\\Chainsaw_Item.vmc",
 32 modelcfg MODEL_CANNON_CFG          "Models\\Weapons\\Cannon\\Cannon_Item.vmc",
 33 modelcfg MODEL_BEAMGUN_CFG         "Models\\Weapons\\Beamgun\\Beamgun_Item.vmc",
 34 modelcfg MODEL_PLASMATHROWER_CFG   "Models\\Weapons\\Plasmathrower\\Plasmathrower_Item.vmc",
 35 modelcfg MODEL_MINELAYER_CFG       "Models\\Weapons\\Minelayer\\Minelayer_Item.vmc",

// ************** AMON STATUE **************
180 model   MODEL_GOLDAMON                "Models\\Ages\\Egypt\\Gods\\Amon\\AmonGold.mdl",
181 texture TEXTURE_GOLDAMON              "Models\\Ages\\Egypt\\Gods\\Amon\\AmonGold.tex",

// ************** REFLECTIONS **************
206 texture TEX_REFL_GOLD01               "Models\\ReflectionTextures\\Gold01.tex",

// ************** SPECULAR **************
211 texture TEX_SPEC_MEDIUM             "Models\\SpecularTextures\\Medium.tex",


functions:

  /* Read from stream. */
  void Read_t(CTStream *istr) // throw char *
  {
    CRationalEntity::Read_t(istr);
  }

  void Precache(void)
  {
    CPlayerAnimatorEntity_Precache(GetPlayer()->GetInventory()->GetWeaponsMask());
  }

  CPlayerPawnEntity *GetPlayer(void)
  {
    return static_cast<CPlayerPawnEntity*>(&*m_penPlayer);
  }

  CModelObject *GetBody(void)
  {
    CAttachmentModelObject *pamoBody = GetPlayer()->GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO);

    if (pamoBody==NULL) {
      return NULL;
    }

    return &pamoBody->amo_moModelObject;
  }

  CModelObject *GetBodyRen(void)
  {
    CAttachmentModelObject *pamoBody = GetPlayer()->m_moRender.GetAttachmentModel(PLAYER_ATTACHMENT_TORSO);

    if (pamoBody==NULL) {
      return NULL;
    }

    return &pamoBody->amo_moModelObject;
  }

  INDEX BodyAttachmentForWeapon(WeaponIndex eWeaponIndex)
  {
    switch (eWeaponIndex)
    {
      case E_WEAPON_KNIFE: return BODY_ATTACHMENT_KNIFE;
      case E_WEAPON_REVOLVER: return BODY_ATTACHMENT_COLT_RIGHT;
      case E_WEAPON_SHOTGUN: return BODY_ATTACHMENT_SINGLE_SHOTGUN;
      case E_WEAPON_SUPERSHOTGUN: return BODY_ATTACHMENT_DOUBLE_SHOTGUN;
      case E_WEAPON_MACHINEGUN: return BODY_ATTACHMENT_TOMMYGUN;
      case E_WEAPON_SNIPER: return BODY_ATTACHMENT_FLAMER;
      case E_WEAPON_CHAINGUN: return BODY_ATTACHMENT_MINIGUN;
      case E_WEAPON_ROCKETLAUNCHER: return BODY_ATTACHMENT_ROCKET_LAUNCHER;
      case E_WEAPON_GRENADELAUNCHER: return BODY_ATTACHMENT_GRENADE_LAUNCHER;
      case E_WEAPON_FLAMER: return BODY_ATTACHMENT_FLAMER;
      case E_WEAPON_CHAINSAW: return BODY_ATTACHMENT_MINIGUN;
      case E_WEAPON_PLASMAGUN: return BODY_ATTACHMENT_LASER;
      case E_WEAPON_CANNON: return BODY_ATTACHMENT_CANNON;

      // [SSE] Weapons
      case E_WEAPON_BEAMGUN: return BODY_ATTACHMENT_LASER;
      case E_WEAPON_PLASMATHROWER: return BODY_ATTACHMENT_LASER;
      case E_WEAPON_MINELAYER: return BODY_ATTACHMENT_GRENADE_LAUNCHER;
    }

    return -1;
  }

  INDEX ResourceForWeapon(WeaponIndex eWeaponIndex)
  {
    switch (eWeaponIndex)
    {
      case E_WEAPON_KNIFE: return MODEL_KNIFE_CFG;
      case E_WEAPON_REVOLVER: return MODEL_COLT_CFG;
      case E_WEAPON_SHOTGUN: return MODEL_SINGLESHOTGUN_CFG;
      case E_WEAPON_SUPERSHOTGUN: return MODEL_DOUBLESHOTGUN_CFG;
      case E_WEAPON_MACHINEGUN: return MODEL_TOMMYGUN_CFG;
      case E_WEAPON_SNIPER: return MODEL_SNIPER_CFG;
      case E_WEAPON_CHAINGUN: return MODEL_MINIGUN_CFG;
      case E_WEAPON_ROCKETLAUNCHER: return MODEL_ROCKETLAUNCHER_CFG;
      case E_WEAPON_GRENADELAUNCHER: return MODEL_GRENADELAUNCHER_CFG;
      case E_WEAPON_FLAMER: return MODEL_FLAMER_CFG;
      case E_WEAPON_CHAINSAW: return MODEL_CHAINSAW_CFG;
      case E_WEAPON_PLASMAGUN: return MODEL_LASER_CFG;
      case E_WEAPON_CANNON: return MODEL_CANNON_CFG;

      // [SSE] Weapons
      case E_WEAPON_BEAMGUN: return MODEL_BEAMGUN_CFG;
      case E_WEAPON_PLASMATHROWER: return MODEL_PLASMATHROWER_CFG;
      case E_WEAPON_MINELAYER: return MODEL_MINELAYER_CFG;
    }

    return 0;
  }

  // Set components
  void SetResource(CModelObject *mo, ULONG ulIDModel, ULONG ulIDTexture,
                     ULONG ulIDReflectionTexture, ULONG ulIDSpecularTexture, ULONG ulIDBumpTexture) {
    // model data
    mo->SetData(GetModelDataForResource(ulIDModel));
    // texture data
    mo->mo_toTexture.SetData(GetTextureDataForResource(ulIDTexture));

    // reflection texture data
    if (ulIDReflectionTexture>0) {
      mo->mo_toReflection.SetData(GetTextureDataForResource(ulIDReflectionTexture));
    } else {
      mo->mo_toReflection.SetData(NULL);
    }

    // specular texture data
    if (ulIDSpecularTexture>0) {
      mo->mo_toSpecular.SetData(GetTextureDataForResource(ulIDSpecularTexture));
    } else {
      mo->mo_toSpecular.SetData(NULL);
    }

    // bump texture data
    if (ulIDBumpTexture>0) {
      mo->mo_toBump.SetData(GetTextureDataForResource(ulIDBumpTexture));
    } else {
      mo->mo_toBump.SetData(NULL);
    }

    ModelChangeNotify();
  };

  // Add attachment model
  void AddAttachmentModel(CModelObject *mo, INDEX iAttachment, ULONG ulIDModel, ULONG ulIDTexture,
                          ULONG ulIDReflectionTexture, ULONG ulIDSpecularTexture, ULONG ulIDBumpTexture) {
    SetResource(&mo->AddAttachmentModel(iAttachment)->amo_moModelObject, ulIDModel,
                  ulIDTexture, ulIDReflectionTexture, ulIDSpecularTexture, ulIDBumpTexture);
  };

  // Add weapon attachment
  void AddWeaponAttachment(INDEX iAttachment, ULONG ulIDModel, ULONG ulIDTexture,
                           ULONG ulIDReflectionTexture, ULONG ulIDSpecularTexture, ULONG ulIDBumpTexture) {
    AddAttachmentModel(pmoModel, iAttachment, ulIDModel, ulIDTexture,
                       ulIDReflectionTexture, ulIDSpecularTexture, ulIDBumpTexture);
  };

  // set active attachment (model)
  void SetAttachment(INDEX iAttachment)
  {
    pmoModel = &(pmoModel->GetAttachmentModel(iAttachment)->amo_moModelObject);
  };

  // synchronize any possible weapon attachment(s) with default appearance
  void SyncWeapon(void)
  {
    CModelObject *pmoBodyRen = GetBodyRen();
    CModelObject *pmoBodyDef = GetBody();

    // for each weapon attachment
    for (INDEX iWeapon = BODY_ATTACHMENT_COLT_RIGHT; iWeapon<=BODY_ATTACHMENT_ITEM; iWeapon++)
    {
      CAttachmentModelObject *pamoWeapDef = pmoBodyDef->GetAttachmentModel(iWeapon);
      CAttachmentModelObject *pamoWeapRen = pmoBodyRen->GetAttachmentModel(iWeapon);

      // if it doesn't exist in either
      if (pamoWeapRen==NULL && pamoWeapDef==NULL) {
        // just skip it
        NOTHING;

      // if exists only in rendering model
      } else if (pamoWeapRen!=NULL && pamoWeapDef==NULL) {
        pmoBodyRen->RemoveAttachmentModel(iWeapon); // remove it from rendering

      // if exists only in default
      } else if (pamoWeapRen==NULL && pamoWeapDef!=NULL) {
        // add it to rendering
        pamoWeapRen = pmoBodyRen->AddAttachmentModel(iWeapon);
        pamoWeapRen->amo_plRelative = pamoWeapDef->amo_plRelative;
        pamoWeapRen->amo_moModelObject.Copy(pamoWeapDef->amo_moModelObject);

      // if exists in both
      } else {
        // just synchronize
        pamoWeapRen->amo_plRelative = pamoWeapDef->amo_plRelative;
        pamoWeapRen->amo_moModelObject.Synchronize(pamoWeapDef->amo_moModelObject);
      }
    }
  }

  // update weapon model
  void SetWeapon(void)
  {
    CPlayerPawnEntity &pl = *GetPlayer();
    CModelObject *pmoBody = GetBody();

    RemoveBodyChildren(pmoBody);

    const WeaponIndex eFirstWeapon = pl.GetWeapons(E_HAND_MAIN)->GetCurrentWeapon();
    const INDEX iBodyAttachment = BodyAttachmentForWeapon(eFirstWeapon);
    const INDEX idModelResource = ResourceForWeapon(eFirstWeapon);
    m_iWeaponLast = eFirstWeapon;

    if (eFirstWeapon != E_WEAPON_NONE) {
      AddVtxChild(this, pmoBody, idModelResource, "Item", iBodyAttachment);
    }

    // sync apperances
    SyncWeapon();
  };

  // set item
  void SetItem(CModelObject *pmo)
  {
    pmoModel = &(GetPlayer()->GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject);
    AddWeaponAttachment(BODY_ATTACHMENT_ITEM, MODEL_GOLDAMON,
                        TEXTURE_GOLDAMON, TEX_REFL_GOLD01, TEX_SPEC_MEDIUM, 0);

    if (pmo!=NULL) {
      CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
      CAttachmentModelObject *pamo = pl.GetModelObject()->GetAttachmentModelList(PLAYER_ATTACHMENT_TORSO, BODY_ATTACHMENT_ITEM, -1);
      pmoModel = &(pamo->amo_moModelObject);
      pmoModel->Copy(*pmo);
      pmoModel->StretchModel(FLOAT3D(1,1,1));
      pamo->amo_plRelative = CPlacement3D(FLOAT3D(0, 0, 0), ANGLE3D(0,0,0));
    }

    // sync apperances
    SyncWeapon();
  }

  // set player body animation
  void SetBodyAnimation(INDEX iAnimation, ULONG ulFlags)
  {
    // on weapon change skip anim
    if (m_bChangeWeapon) { return; }

    // on firing skip anim
    if (m_bAttacking) { return; }

    // play body anim
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    CModelObject &moBody = pl.GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(iAnimation, ulFlags);
    m_fBodyAnimTime = moBody.GetAnimLength(iAnimation);     // anim length
  };

/************************************************************
 *                      INITIALIZE                          *
 ************************************************************/
  void Initialize(void)
  {
    // set internal properties
    m_bReference = TRUE;
    m_bWaitJumpAnim = FALSE;
    m_bCrouch = FALSE;
    m_iCrouchDownWait = 0;
    m_iRiseUpWait = 0;
    m_bChangeWeapon = FALSE;
    m_bSwim = FALSE;
    m_bAttacking = FALSE;

    // clear eyes offsets
    m_fEyesYLastOffset = 0.0f;
    m_fEyesYOffset = 0.0f;
    m_fEyesYSpeed = 0.0f;
    m_fWeaponYLastOffset = 0.0f;
    m_fWeaponYOffset = 0.0f;
    m_fWeaponYSpeed = 0.0f;
//    m_fRecoilLastOffset = 0.0f;
//    m_fRecoilOffset = 0.0f;
//    m_fRecoilSpeed = 0.0f;

    // clear moving banking
    m_bMoving = FALSE;
    m_fMoveLastBanking = 0.0f;
    m_fMoveBanking = 0.0f;
    m_iMovingSide = 0;
    m_bSidestepBankingLeft = FALSE;
    m_bSidestepBankingRight = FALSE;
    m_fSidestepLastBanking = 0.0f;
    m_fSidestepBanking = 0.0f;

    // weapon
    SetWeapon();
    SetBodyAnimation(BODY_ANIM_COLT_STAND, AOF_LOOPING|AOF_NORESTART);
  };


/************************************************************
 *                ANIMATE BANKING AND SOFT EYES             *
 ************************************************************/
  // store for lerping
  void StoreLast(void)
  {
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    m_vLastPlayerPosition = pl.GetPlacement().pl_PositionVector;  // store last player position
    m_fEyesYLastOffset = m_fEyesYOffset;                          // store last eyes offset
    m_fWeaponYLastOffset = m_fWeaponYOffset;
//    m_fRecoilLastOffset = m_fRecoilOffset;
    m_fMoveLastBanking = m_fMoveBanking;                          // store last banking for lerping
    m_fSidestepLastBanking = m_fSidestepBanking;
  };

  // animate banking
  void AnimateBanking(void)
  {
    // moving -> change banking
    if (m_bMoving) {
      // move banking left
      if (m_iMovingSide == 0) {
        m_fMoveBanking += 0.35f;
        if (m_fMoveBanking > 1.0f) {
          m_fMoveBanking = 1.0f;
          m_iMovingSide = 1;
        }

      // move banking right
      } else {
        m_fMoveBanking -= 0.35f;
        if (m_fMoveBanking < -1.0f) {
          m_fMoveBanking = -1.0f;
          m_iMovingSide = 0;
        }
      }

      const FLOAT fBankingSpeed = 0.4f;

      // sidestep banking left
      if (m_bSidestepBankingLeft) {
        m_fSidestepBanking += fBankingSpeed;
        if (m_fSidestepBanking > 1.0f) { m_fSidestepBanking = 1.0f; }
      }

      // sidestep banking right
      if (m_bSidestepBankingRight) {
        m_fSidestepBanking -= fBankingSpeed;
        if (m_fSidestepBanking < -1.0f) { m_fSidestepBanking = -1.0f; }
      }

    // restore banking
    } else {
      // move banking
      if (m_fMoveBanking > 0.0f) {
        m_fMoveBanking -= 0.1f;
        if (m_fMoveBanking < 0.0f) { m_fMoveBanking = 0.0f; }

      } else if (m_fMoveBanking < 0.0f) {
        m_fMoveBanking += 0.1f;
        if (m_fMoveBanking > 0.0f) { m_fMoveBanking = 0.0f; }
      }

      // sidestep banking
      if (m_fSidestepBanking > 0.0f) {
        m_fSidestepBanking -= 0.4f;
        if (m_fSidestepBanking < 0.0f) { m_fSidestepBanking = 0.0f; }

      } else if (m_fSidestepBanking < 0.0f) {
        m_fSidestepBanking += 0.4f;
        if (m_fSidestepBanking > 0.0f) { m_fSidestepBanking = 0.0f; }
      }
    }

    CPlayerSettings *pSettings = GetPlayer()->GetSettings();
    BOOL bNoBobbing = pSettings != NULL && pSettings->ps_ulFlags&PSF_NOBOBBING;
    if (bNoBobbing) {
      m_fSidestepBanking = m_fMoveBanking = 0.0f;
    }
  };

  // animate soft eyes
  void AnimateSoftEyes(void)
  {
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    // find eyes offset and speed (differential formula realized in numerical mathematics)
    FLOAT fRelY = (pl.GetPlacement().pl_PositionVector-m_vLastPlayerPosition) %
                  FLOAT3D(pl.en_mRotation(1, 2), pl.en_mRotation(2, 2), pl.en_mRotation(3, 2));

    // if just jumped
    if (pl.en_tmJumped>_pTimer->CurrentTick()-0.5f) {
      fRelY = ClampUp(fRelY, 0.0f);
    }
    m_fEyesYOffset -= fRelY;
    m_fWeaponYOffset -= ClampUp(fRelY, 0.0f);

    plr_fViewDampFactor      = Clamp(plr_fViewDampFactor      ,0.0f,1.0f);
    plr_fViewDampLimitGroundUp = Clamp(plr_fViewDampLimitGroundUp ,0.0f,2.0f);
    plr_fViewDampLimitGroundDn = Clamp(plr_fViewDampLimitGroundDn ,0.0f,2.0f);
    plr_fViewDampLimitWater  = Clamp(plr_fViewDampLimitWater  ,0.0f,2.0f);

    m_fEyesYSpeed = (m_fEyesYSpeed - m_fEyesYOffset*plr_fViewDampFactor) * (1.0f-plr_fViewDampFactor);
    m_fEyesYOffset += m_fEyesYSpeed;

    m_fWeaponYSpeed = (m_fWeaponYSpeed - m_fWeaponYOffset*plr_fViewDampFactor) * (1.0f-plr_fViewDampFactor);
    m_fWeaponYOffset += m_fWeaponYSpeed;

    if (m_bSwim) {
      m_fEyesYOffset = Clamp(m_fEyesYOffset, -plr_fViewDampLimitWater,  +plr_fViewDampLimitWater);
      m_fWeaponYOffset = Clamp(m_fWeaponYOffset, -plr_fViewDampLimitWater,  +plr_fViewDampLimitWater);
    } else {
      m_fEyesYOffset = Clamp(m_fEyesYOffset, -plr_fViewDampLimitGroundDn,  +plr_fViewDampLimitGroundUp);
      m_fWeaponYOffset = Clamp(m_fWeaponYOffset, -plr_fViewDampLimitGroundDn,  +plr_fViewDampLimitGroundUp);
    }
  };

  /*
  // animate view pitch (for recoil)
  void AnimateRecoilPitch(void)
  {
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    INDEX iWeapon = ((CPlayerWeaponEntity&)*pl.m_penWeaponsFirst).m_iCurrentWeapon;

    wpn_fRecoilDampUp[iWeapon] = Clamp(wpn_fRecoilDampUp[iWeapon],0.0f,1.0f);
    wpn_fRecoilDampDn[iWeapon] = Clamp(wpn_fRecoilDampDn[iWeapon],0.0f,1.0f);

    FLOAT fDamp;
    if (m_fRecoilSpeed>0) {
      fDamp = wpn_fRecoilDampUp[iWeapon];
    } else {
      fDamp = wpn_fRecoilDampDn[iWeapon];
    }
    m_fRecoilSpeed = (m_fRecoilSpeed - m_fRecoilOffset*fDamp)* (1.0f-fDamp);

    m_fRecoilOffset += m_fRecoilSpeed;

    if (m_fRecoilOffset<0.0f) {
      m_fRecoilOffset = 0.0f;
    }
    if (m_fRecoilOffset>wpn_fRecoilLimit[iWeapon]) {
      m_fRecoilOffset = wpn_fRecoilLimit[iWeapon];
      m_fRecoilSpeed = 0.0f;
    }
  };
  */

  // change view
  void ChangeView(CPlacement3D &pl)
  {
    TIME tmNow = _pTimer->GetLerpedCurrentTick();

    CPlayerSettings *pSettings = GetPlayer()->GetSettings();
    BOOL bNoBobbing = pSettings != NULL && pSettings->ps_ulFlags&PSF_NOBOBBING;
    if (!bNoBobbing) {
      // banking
      FLOAT fBanking = Lerp(m_fMoveLastBanking, m_fMoveBanking, _pTimer->GetLerpFactor());
      fBanking = fBanking * fBanking * Sgn(fBanking) * 0.25f;
      fBanking += Lerp(m_fSidestepLastBanking, m_fSidestepBanking, _pTimer->GetLerpFactor());
      fBanking = Clamp(fBanking, -5.0f, 5.0f);
      pl.pl_OrientationAngle(3) += fBanking;
    }

/*
    // recoil pitch
    INDEX iWeapon = ((CPlayerWeaponEntity&)*((CPlayerPawnEntity&)*m_penPlayer).m_penWeaponsFirst).m_iCurrentWeapon;
    FLOAT fRecoil = Lerp(m_fRecoilLastOffset, m_fRecoilOffset, _pTimer->GetLerpFactor());
    FLOAT fRecoilP = wpn_fRecoilFactorP[iWeapon]*fRecoil;
    pl.pl_OrientationAngle(2) += fRecoilP;
    // adjust recoil pitch handle
    FLOAT fRecoilH = wpn_fRecoilOffset[iWeapon];
    FLOAT fDY = fRecoilH*(1.0f-Cos(fRecoilP));
    FLOAT fDZ = fRecoilH*Sin(fRecoilP);
    pl.pl_PositionVector(2)-=fDY;
    pl.pl_PositionVector(3)+=fDZ+wpn_fRecoilFactorZ[iWeapon]*fRecoil;
    */

    // swimming
    if (m_bSwim) {
      pl.pl_OrientationAngle(1) += sin(tmNow*0.9)*2.0f;
      pl.pl_OrientationAngle(2) += sin(tmNow*1.7)*2.0f;
      pl.pl_OrientationAngle(3) += sin(tmNow*2.5)*2.0f;
    }

    // eyes up/down for jumping and breathing
    FLOAT fEyesOffsetY = Lerp(m_fEyesYLastOffset, m_fEyesYOffset, _pTimer->GetLerpFactor());
    fEyesOffsetY+= sin(tmNow*1.5)*0.05f * plr_fBreathingStrength;
    fEyesOffsetY = Clamp(fEyesOffsetY, -1.0f, 1.0f);
    pl.pl_PositionVector(2) += fEyesOffsetY;
  }

/************************************************************
 *                     ANIMATE PLAYER                       *
 ************************************************************/
  // body and head animation
  void BodyAndHeadOrientation(CPlacement3D &plView)
  {
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    CAttachmentModelObject *pamoBody = pl.GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO);
    ANGLE3D a = plView.pl_OrientationAngle;
    if (!(pl.GetFlags()&ENF_ALIVE)) {
      a = ANGLE3D(0,0,0);
    }

    pamoBody->amo_plRelative.pl_OrientationAngle = a;
    pamoBody->amo_plRelative.pl_OrientationAngle(3) *= 4.0f;

    CAttachmentModelObject *pamoHead = (pamoBody->amo_moModelObject).GetAttachmentModel(BODY_ATTACHMENT_HEAD);
    pamoHead->amo_plRelative.pl_OrientationAngle = a;
    pamoHead->amo_plRelative.pl_OrientationAngle(1) = 0.0f;
    pamoHead->amo_plRelative.pl_OrientationAngle(2) = 0.0f;
    pamoHead->amo_plRelative.pl_OrientationAngle(3) *= 4.0f;

    // forbid players from cheating by kissing their @$$
    const FLOAT fMaxBanking = 5.0f;
    pamoBody->amo_plRelative.pl_OrientationAngle(3) = Clamp(pamoBody->amo_plRelative.pl_OrientationAngle(3), -fMaxBanking, fMaxBanking);
    pamoHead->amo_plRelative.pl_OrientationAngle(3) = Clamp(pamoHead->amo_plRelative.pl_OrientationAngle(3), -fMaxBanking, fMaxBanking);
  };

  // animate player
  void AnimatePlayer(void)
  {
    if (m_bDisableAnimating) {
      return;
    }

    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;

    FLOAT3D vDesiredTranslation = pl.en_vDesiredTranslationRelative;
    FLOAT3D vCurrentTranslation = pl.en_vCurrentTranslationAbsolute * !pl.en_mRotation;
    ANGLE3D aDesiredRotation = pl.en_aDesiredRotationRelative;
    ANGLE3D aCurrentRotation = pl.en_aCurrentRotationAbsolute;

    // if player is moving
    if (vDesiredTranslation.ManhattanNorm()>0.01f
      ||aDesiredRotation.ManhattanNorm()>0.01f) {
      // prevent idle weapon animations
      m_fLastActionTime = _pTimer->CurrentTick();
    }

    // swimming
    if (m_bSwim) {
      if (vDesiredTranslation.Length()>1.0f && vCurrentTranslation.Length()>1.0f) {
        pl.StartModelAnim(PLAYER_ANIM_SWIM, AOF_LOOPING|AOF_NORESTART);
      } else {
        pl.StartModelAnim(PLAYER_ANIM_SWIMIDLE, AOF_LOOPING|AOF_NORESTART);
      }
      BodyStillAnimation();

    // stand
    } else {
      // has reference (floor)
      if (m_bReference) {
        // jump
        if (pl.en_tmJumped+_pTimer->TickQuantum>=_pTimer->CurrentTick() &&
            pl.en_tmJumped<=_pTimer->CurrentTick()) {
          m_bReference = FALSE;
          pl.StartModelAnim(PLAYER_ANIM_JUMPSTART, AOF_NORESTART);
          BodyStillAnimation();
          m_fLastActionTime = _pTimer->CurrentTick();

        // not in jump anim and in stand mode change
        } else if (!m_bWaitJumpAnim && m_iCrouchDownWait==0 && m_iRiseUpWait==0) {

          // standing
          if (!m_bCrouch) {
            // running anim
            if (vDesiredTranslation.Length()>5.0f && vCurrentTranslation.Length()>5.0f) {
              if (vCurrentTranslation(3)<0) {
                pl.StartModelAnim(PLAYER_ANIM_RUN, AOF_LOOPING|AOF_NORESTART);
              } else {
                pl.StartModelAnim(PLAYER_ANIM_BACKPEDALRUN, AOF_LOOPING|AOF_NORESTART);
              }
              BodyStillAnimation();
              m_fLastActionTime = _pTimer->CurrentTick();

            // walking anim
            } else if (vDesiredTranslation.Length()>2.0f && vCurrentTranslation.Length()>2.0f) {
              if (vCurrentTranslation(3)<0) {
                pl.StartModelAnim(PLAYER_ANIM_NORMALWALK, AOF_LOOPING|AOF_NORESTART);
              } else {
                pl.StartModelAnim(PLAYER_ANIM_BACKPEDAL, AOF_LOOPING|AOF_NORESTART);
              }
              BodyStillAnimation();
              m_fLastActionTime = _pTimer->CurrentTick();

            // left rotation anim
            } else if (aDesiredRotation(1)>0.5f) {
              pl.StartModelAnim(PLAYER_ANIM_TURNLEFT, AOF_LOOPING|AOF_NORESTART);
              BodyStillAnimation();
              m_fLastActionTime = _pTimer->CurrentTick();

            // right rotation anim
            } else if (aDesiredRotation(1)<-0.5f) {
              pl.StartModelAnim(PLAYER_ANIM_TURNRIGHT, AOF_LOOPING|AOF_NORESTART);
              BodyStillAnimation();
              m_fLastActionTime = _pTimer->CurrentTick();

            // standing anim
            } else {
              pl.StartModelAnim(PLAYER_ANIM_STAND, AOF_LOOPING|AOF_NORESTART);
              BodyStillAnimation();
            }

          // crouch
          } else {

            // walking anim
            if (vDesiredTranslation.Length()>2.0f && vCurrentTranslation.Length()>2.0f) {
              if (vCurrentTranslation(3)<0) {
                pl.StartModelAnim(PLAYER_ANIM_CROUCH_WALK, AOF_LOOPING|AOF_NORESTART);
              } else {
                pl.StartModelAnim(PLAYER_ANIM_CROUCH_WALKBACK, AOF_LOOPING|AOF_NORESTART);
              }

              BodyStillAnimation();
              m_fLastActionTime = _pTimer->CurrentTick();

            // left rotation anim
            } else if (aDesiredRotation(1)>0.5f) {
              pl.StartModelAnim(PLAYER_ANIM_CROUCH_TURNLEFT, AOF_LOOPING|AOF_NORESTART);
              BodyStillAnimation();
              m_fLastActionTime = _pTimer->CurrentTick();

            // right rotation anim
            } else if (aDesiredRotation(1)<-0.5f) {
              pl.StartModelAnim(PLAYER_ANIM_CROUCH_TURNRIGHT, AOF_LOOPING|AOF_NORESTART);
              BodyStillAnimation();
              m_fLastActionTime = _pTimer->CurrentTick();

            // standing anim
            } else {
              pl.StartModelAnim(PLAYER_ANIM_CROUCH_IDLE, AOF_LOOPING|AOF_NORESTART);
              BodyStillAnimation();
            }
          }

        }

      // no reference (in air)
      } else {
        // touched reference
        if (pl.en_penReference!=NULL) {
          m_bReference = TRUE;
          pl.StartModelAnim(PLAYER_ANIM_JUMPEND, AOF_NORESTART);
          BodyStillAnimation();
          SpawnReminder(this, pl.GetModelObject()->GetAnimLength(PLAYER_ANIM_JUMPEND), (INDEX) AA_JUMPDOWN);
          m_bWaitJumpAnim = TRUE;
        }
      }
    }

    // boring weapon animation
    if (_pTimer->CurrentTick()-m_fLastActionTime > 10.0f) {
      m_fLastActionTime = _pTimer->CurrentTick();
      ((CPlayerWeaponEntity&)*pl.m_penWeaponsFirst).SendEvent(EBoringWeapon());
    }

    // moving view change
    // translating -> change banking
    if (m_bReference != NULL && vDesiredTranslation.Length()>1.0f && vCurrentTranslation.Length()>1.0f) {
      m_bMoving = TRUE;

      // sidestep banking
      FLOAT vSidestepSpeedDesired = vDesiredTranslation(1);
      FLOAT vSidestepSpeedCurrent = vCurrentTranslation(1);

      // right
      if (vSidestepSpeedDesired>1.0f && vSidestepSpeedCurrent>1.0f) {
        m_bSidestepBankingRight = TRUE;
        m_bSidestepBankingLeft = FALSE;

      // left
      } else if (vSidestepSpeedDesired<-1.0f && vSidestepSpeedCurrent<-1.0f) {
        m_bSidestepBankingLeft = TRUE;
        m_bSidestepBankingRight = FALSE;

      // none
      } else {
        m_bSidestepBankingLeft = FALSE;
        m_bSidestepBankingRight = FALSE;
      }

    // in air (space) or not moving
    } else {
      m_bMoving = FALSE;
      m_bSidestepBankingLeft = FALSE;
      m_bSidestepBankingRight = FALSE;
    }
  };

  // crouch
  void Crouch(void)
  {
    if (m_bDisableAnimating) {
      return;
    }

    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    pl.StartModelAnim(PLAYER_ANIM_CROUCH, AOF_NORESTART);
    SpawnReminder(this, pl.GetModelObject()->GetAnimLength(PLAYER_ANIM_CROUCH), (INDEX) AA_CROUCH);
    m_iCrouchDownWait++;
    m_bCrouch = TRUE;
  };

  // rise
  void Rise(void)
  {
    if (m_bDisableAnimating) {
      return;
    }

    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    pl.StartModelAnim(PLAYER_ANIM_RISE, AOF_NORESTART);
    SpawnReminder(this, pl.GetModelObject()->GetAnimLength(PLAYER_ANIM_RISE), (INDEX) AA_RISE);
    m_iRiseUpWait++;
    m_bCrouch = FALSE;
  };

  // fall
  void Fall(void)
  {
    if (m_bDisableAnimating) {
      return;
    }

    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    pl.StartModelAnim(PLAYER_ANIM_JUMPSTART, AOF_NORESTART);
    if (_pNetwork->ga_ulDemoMinorVersion>6) { m_bCrouch = FALSE; }
    m_bReference = FALSE;
  };

  // swim
  void Swim(void)
  {
    if (m_bDisableAnimating) {
      return;
    }

    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    pl.StartModelAnim(PLAYER_ANIM_SWIM, AOF_LOOPING|AOF_NORESTART);
    if (_pNetwork->ga_ulDemoMinorVersion>2) { m_bCrouch = FALSE; }
    m_bSwim = TRUE;
  };

  // stand
  void Stand(void)
  {
    if (m_bDisableAnimating) {
      return;
    }

    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    pl.StartModelAnim(PLAYER_ANIM_STAND, AOF_LOOPING|AOF_NORESTART);
    if (_pNetwork->ga_ulDemoMinorVersion>2) { m_bCrouch = FALSE; }
    m_bSwim = FALSE;
  };

  // fire/attack
  void FireAnimation(INDEX iAnim, ULONG ulFlags)
  {
    if (m_bSwim) {
      WeaponIndex eFirstWeapon = GetPlayer()->GetWeapons(E_HAND_MAIN)->GetCurrentWeapon();

      switch (eFirstWeapon)
      {
        case E_WEAPON_NONE:
          break;
        case E_WEAPON_KNIFE: case E_WEAPON_REVOLVER:
        //case WEAPON_DOUBLECOLT:
          iAnim += BODY_ANIM_COLT_SWIM_STAND-BODY_ANIM_COLT_STAND;
          break;
        case E_WEAPON_SHOTGUN: case E_WEAPON_SUPERSHOTGUN: case E_WEAPON_MACHINEGUN:
        case E_WEAPON_SNIPER: case E_WEAPON_PLASMAGUN: case E_WEAPON_BEAMGUN: case E_WEAPON_FLAMER: case E_WEAPON_PLASMATHROWER:
          iAnim += BODY_ANIM_SHOTGUN_SWIM_STAND-BODY_ANIM_SHOTGUN_STAND;
          break;
        case E_WEAPON_CHAINGUN: case E_WEAPON_ROCKETLAUNCHER: case E_WEAPON_GRENADELAUNCHER:
        case E_WEAPON_CANNON: case E_WEAPON_CHAINSAW: case E_WEAPON_MINELAYER:
          iAnim += BODY_ANIM_MINIGUN_SWIM_STAND-BODY_ANIM_MINIGUN_STAND;
          break;
      }
    }

    m_bAttacking = FALSE;
    m_bChangeWeapon = FALSE;
    SetBodyAnimation(iAnim, ulFlags);

    if (!(ulFlags&AOF_LOOPING)) {
      SpawnReminder(this, m_fBodyAnimTime, (INDEX) AA_ATTACK);
      m_tmAttackingDue = _pTimer->CurrentTick()+m_fBodyAnimTime;
    }

    m_bAttacking = TRUE;
  };

  void FireAnimationOff(void)
  {
    m_bAttacking = FALSE;
  };

/************************************************************
 *                  CHANGE BODY ANIMATION                   *
 ************************************************************/
  // body animation template
  void BodyAnimationTemplate(INDEX iNone, INDEX iColt, INDEX iShotgun, INDEX iMinigun, ULONG ulFlags)
  {
    WeaponIndex eFirstWeapon = GetPlayer()->GetWeapons(E_HAND_MAIN)->GetCurrentWeapon();

    switch (eFirstWeapon)
    {
      case E_WEAPON_NONE:
        SetBodyAnimation(iNone, ulFlags);
        break;

      case E_WEAPON_KNIFE: case E_WEAPON_REVOLVER:
      //case WEAPON_DOUBLECOLT:
        if (m_bSwim) { iColt += BODY_ANIM_COLT_SWIM_STAND-BODY_ANIM_COLT_STAND; }
        SetBodyAnimation(iColt, ulFlags);
        break;

      case E_WEAPON_SHOTGUN: case E_WEAPON_SUPERSHOTGUN: case E_WEAPON_MACHINEGUN:
      case E_WEAPON_SNIPER: case E_WEAPON_PLASMAGUN: case E_WEAPON_BEAMGUN: case E_WEAPON_FLAMER: case E_WEAPON_PLASMATHROWER:
        if (m_bSwim) { iShotgun += BODY_ANIM_SHOTGUN_SWIM_STAND-BODY_ANIM_SHOTGUN_STAND; }
        SetBodyAnimation(iShotgun, ulFlags);
        break;

      case E_WEAPON_CHAINGUN: case E_WEAPON_ROCKETLAUNCHER: case E_WEAPON_GRENADELAUNCHER:
      case E_WEAPON_CANNON: case E_WEAPON_CHAINSAW: case E_WEAPON_MINELAYER:
        if (m_bSwim) { iMinigun+=BODY_ANIM_MINIGUN_SWIM_STAND-BODY_ANIM_MINIGUN_STAND; }
        SetBodyAnimation(iMinigun, ulFlags);
        break;

      default: ASSERTALWAYS("Player Animator - Unknown weapon");
    }
  };

  // walk
  void BodyWalkAnimation()
  {
    BodyAnimationTemplate(BODY_ANIM_NORMALWALK,
      BODY_ANIM_COLT_STAND, BODY_ANIM_SHOTGUN_STAND, BODY_ANIM_MINIGUN_STAND,
      AOF_LOOPING|AOF_NORESTART);
  };

  // stand
  void BodyStillAnimation()
  {
    BodyAnimationTemplate(BODY_ANIM_WAIT,
      BODY_ANIM_COLT_STAND, BODY_ANIM_SHOTGUN_STAND, BODY_ANIM_MINIGUN_STAND,
      AOF_LOOPING|AOF_NORESTART);
  };

  // push weapon
  void BodyPushAnimation()
  {
    m_bAttacking = FALSE;
    m_bChangeWeapon = FALSE;
    BodyAnimationTemplate(BODY_ANIM_WAIT,
      BODY_ANIM_COLT_REDRAW, BODY_ANIM_SHOTGUN_REDRAW, BODY_ANIM_MINIGUN_REDRAW, 0);
    m_bChangeWeapon = TRUE;
  };

  // will remove everything except your head.
  void RemoveBodyChildren(CModelObject *pmoModel)
  {
    for (INDEX iWeapon = BODY_ATTACHMENT_COLT_RIGHT; iWeapon <= BODY_ATTACHMENT_ITEM; iWeapon++)
    {
      pmoModel->RemoveAttachmentModel(iWeapon); // remove it from rendering
    }
  }

  // remove weapon attachment
  void RemoveWeapon(void)
  {
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    pmoModel = &(pl.GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject);
    const INDEX iBodyAttachment = BodyAttachmentForWeapon((WeaponIndex)m_iWeaponLast);

    if (m_iWeaponLast != E_WEAPON_NONE) {
      pmoModel->RemoveAttachmentModel(iBodyAttachment);
    }

    // sync apperances
    SyncWeapon();
  }

  // pull weapon
  void BodyPullAnimation()
  {
    // remove old weapon
    RemoveWeapon();

    // set new weapon
    SetWeapon();

    // pull weapon
    m_bChangeWeapon = FALSE;
    BodyAnimationTemplate(BODY_ANIM_WAIT,
      BODY_ANIM_COLT_DRAW, BODY_ANIM_SHOTGUN_DRAW, BODY_ANIM_MINIGUN_DRAW, 0);

    WeaponIndex eFirstWeapon = GetPlayer()->GetWeapons(E_HAND_MAIN)->GetCurrentWeapon();

    if (eFirstWeapon != E_WEAPON_NONE) {
      m_bChangeWeapon = TRUE;
      SpawnReminder(this, m_fBodyAnimTime, (INDEX) AA_PULLWEAPON);
    }

    // sync apperances
    SyncWeapon();
  };

  // pull item
  void BodyPullItemAnimation()
  {
    // remove old weapon
    RemoveWeapon();

    // pull item
    m_bChangeWeapon = FALSE;
    SetBodyAnimation(BODY_ANIM_STATUE_PULL, 0);
    m_bChangeWeapon = TRUE;
    SpawnReminder(this, m_fBodyAnimTime, (INDEX) AA_PULLWEAPON);

    // sync apperances
    SyncWeapon();
  };

  // pick item
  void BodyPickItemAnimation()
  {
    // remove old weapon
    RemoveWeapon();

    // pick item
    m_bChangeWeapon = FALSE;
    SetBodyAnimation(BODY_ANIM_KEYLIFT, 0);
    m_bChangeWeapon = TRUE;
    SpawnReminder(this, m_fBodyAnimTime, (INDEX) AA_PULLWEAPON);

    // sync apperances
    SyncWeapon();
  };

  // remove item
  void BodyRemoveItem()
  {
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    pmoModel = &(pl.GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject);
    pmoModel->RemoveAttachmentModel(BODY_ATTACHMENT_ITEM);

    // sync apperances
    SyncWeapon();
  };

/************************************************************
 *                      FIRE FLARE                          *
 ************************************************************/
  void OnPreRender(void)
  {
    ControlFlareAttachment();

    // Minigun Specific
    CPlayerWeaponEntity &plw = *GetPlayer()->GetWeapons(E_HAND_MAIN);

    if (plw.GetCurrentWeapon() == E_WEAPON_CHAINGUN) {
      ANGLE aAngle = Lerp(plw.m_aChaingunLast, plw.m_aChaingun, _pTimer->GetLerpFactor());

      // rotate minigun barrels
      CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
      CAttachmentModelObject *pamo = pl.GetModelObject()->GetAttachmentModelList(
        PLAYER_ATTACHMENT_TORSO, BODY_ATTACHMENT_MINIGUN, MINIGUNITEM_ATTACHMENT_BARRELS, -1);

      if (pamo!=NULL) {
        pamo->amo_plRelative.pl_OrientationAngle(3) = aAngle;
      }
    }
  };

  // show flare
  void ShowFlare(INDEX iAttachWeapon, INDEX iAttachObject, INDEX iAttachFlare)
  {
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    CAttachmentModelObject *pamo = pl.GetModelObject()->GetAttachmentModelList(
      PLAYER_ATTACHMENT_TORSO, iAttachWeapon, iAttachObject, iAttachFlare, -1);

    if (pamo!=NULL) {
      pamo->amo_plRelative.pl_OrientationAngle(3) = (rand()*360.0f)/RAND_MAX;
      CModelObject &mo = pamo->amo_moModelObject;
      mo.StretchModel(FLOAT3D(1, 1, 1));
    }
  };

  // hide flare
  void HideFlare(INDEX iAttachWeapon, INDEX iAttachObject, INDEX iAttachFlare)
  {
    CPlayerPawnEntity &pl = (CPlayerPawnEntity&)*m_penPlayer;
    CAttachmentModelObject *pamo = pl.GetModelObject()->GetAttachmentModelList(
      PLAYER_ATTACHMENT_TORSO, iAttachWeapon, iAttachObject, iAttachFlare, -1);

    if (pamo!=NULL) {
      CModelObject &mo = pamo->amo_moModelObject;
      mo.StretchModel(FLOAT3D(0, 0, 0));
    }
  };

  // flare attachment
  void ControlFlareAttachment(void)
  {
/*    if (!IsPredictionHead()) {
      return;
    }
    */
    
    // get your prediction tail
    CPlayerAnimatorEntity *pen = (CPlayerAnimatorEntity *)GetPredictionTail();
    WeaponIndex eWeaponIndex = pen->GetPlayer()->GetWeapons(E_HAND_MAIN)->GetCurrentWeapon();

    // second colt only
    /*
    if (iWeapon==WEAPON_DOUBLECOLT) {
      // add flare
      if (pen->m_iSecondFlare==FLARE_ADD) {
        pen->m_iSecondFlare = FLARE_REMOVE;
        ShowFlare(BODY_ATTACHMENT_COLT_LEFT, COLTITEM_ATTACHMENT_BODY, COLTMAIN_ATTACHMENT_FLARE);
      // remove flare
      } else if (m_iSecondFlare==FLARE_REMOVE) {
        HideFlare(BODY_ATTACHMENT_COLT_LEFT, COLTITEM_ATTACHMENT_BODY, COLTMAIN_ATTACHMENT_FLARE);
      }
    }
    */

    // add flare
    if (pen->m_iFlare==FLARE_ADD) {
      pen->m_iFlare = FLARE_REMOVE;
      pen->m_tmFlareAdded = _pTimer->CurrentTick();

      switch (eWeaponIndex)
      {
        //case WEAPON_DOUBLECOLT:
        case E_WEAPON_REVOLVER:
          ShowFlare(BODY_ATTACHMENT_COLT_RIGHT, COLTITEM_ATTACHMENT_BODY, COLTMAIN_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_SHOTGUN:
          ShowFlare(BODY_ATTACHMENT_SINGLE_SHOTGUN, SINGLESHOTGUNITEM_ATTACHMENT_BARRELS, BARRELS_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_SUPERSHOTGUN:
          ShowFlare(BODY_ATTACHMENT_DOUBLE_SHOTGUN, DOUBLESHOTGUNITEM_ATTACHMENT_BARRELS, DSHOTGUNBARRELS_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_MACHINEGUN:
          ShowFlare(BODY_ATTACHMENT_TOMMYGUN, TOMMYGUNITEM_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_SNIPER:
          ShowFlare(BODY_ATTACHMENT_FLAMER, SNIPERITEM_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_CHAINGUN:
          ShowFlare(BODY_ATTACHMENT_MINIGUN, MINIGUNITEM_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE);
          break;
      }

    // remove
    } else if (m_iFlare==FLARE_REMOVE &&
      _pTimer->CurrentTick()>pen->m_tmFlareAdded+_pTimer->TickQuantum) {
      switch (eWeaponIndex)
      {
        //case WEAPON_DOUBLECOLT:
        case E_WEAPON_REVOLVER:
          HideFlare(BODY_ATTACHMENT_COLT_RIGHT, COLTITEM_ATTACHMENT_BODY, COLTMAIN_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_SHOTGUN:
          HideFlare(BODY_ATTACHMENT_SINGLE_SHOTGUN, SINGLESHOTGUNITEM_ATTACHMENT_BARRELS, BARRELS_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_SUPERSHOTGUN:
          HideFlare(BODY_ATTACHMENT_DOUBLE_SHOTGUN, DOUBLESHOTGUNITEM_ATTACHMENT_BARRELS, DSHOTGUNBARRELS_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_MACHINEGUN:
          HideFlare(BODY_ATTACHMENT_TOMMYGUN, TOMMYGUNITEM_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_SNIPER:
          HideFlare(BODY_ATTACHMENT_FLAMER, SNIPERITEM_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE);
          break;

        case E_WEAPON_CHAINGUN:
          HideFlare(BODY_ATTACHMENT_MINIGUN, MINIGUNITEM_ATTACHMENT_BODY, BODY_ATTACHMENT_FLARE);
          break;
      }
    }
  };

/************************************************************
 *                      PROCEDURES                          *
 ************************************************************/
procedures:

  ReminderAction(EReminder er)
  {
    switch (er.iValue)
    {
      case AA_JUMPDOWN: m_bWaitJumpAnim = FALSE; break;
      case AA_CROUCH: m_iCrouchDownWait--; ASSERT(m_iCrouchDownWait>=0); break;
      case AA_RISE: m_iRiseUpWait--; ASSERT(m_iRiseUpWait>=0); break;
      case AA_PULLWEAPON: m_bChangeWeapon = FALSE; break;
      case AA_ATTACK: if (m_tmAttackingDue<=_pTimer->CurrentTick()) { m_bAttacking = FALSE; } break;
      default: ASSERTALWAYS("Animator - unknown reminder action.");
    }

    return EBegin();
  };

  Main(EAnimatorInit eInit)
  {
    // remember the initial parameters
    ASSERT(eInit.penPlayer!=NULL);
    m_penPlayer = eInit.penPlayer;

    // declare yourself as a void
    InitAsVoid();
    SetFlags(GetFlags()|ENF_CROSSESLEVELS);
    SetPhysicsFlags(EPF_MODEL_IMMATERIAL);
    SetCollisionFlags(ECF_IMMATERIAL);

    // last action time for boring weapon animation
    m_fLastActionTime = _pTimer->CurrentTick();

    wait()
    {
      on (EBegin) : { resume; }
      on (EReminder er) : { call ReminderAction(er); }
      on (EEnd) : { stop; }
    }

    // cease to exist
    Destroy();

    return;
  };
};

