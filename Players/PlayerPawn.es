/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

401
%{
  #include "StdH.h"

  #include "Game/SEColors.h"

  #include <Engine/Build.h>
  #include <Engine/Network/Network.h>
  #include <locale.h>

  #include "ModelsMP/Player/SeriousSam/Player.h"
  #include "ModelsMP/Player/SeriousSam/Body.h"
  #include "ModelsMP/Player/SeriousSam/Head.h"

  #include "Entities/Common/PlayerControls.h"

  #include "Entities/Players/PlayerController.h"
  
  #include "Entities/Players/GameInfo.h"
  #include "Entities/Players/PlayerMarker.h"
  #include "Entities/Players/PlayerWeapon.h"
  #include "Entities/Players/PlayerInventory.h"
  #include "Entities/Players/PlayerAnimator.h"
  #include "Entities/Players/PlayerView.h"
  #include "Entities/Brushes/MovingBrush.h"
  #include "Entities/Tools/Switch.h"
  #include "Entities/Tools/MessageHolder.h"
  #include "Entities/Tools/Camera.h"
  #include "Entities/Tools/WorldLink.h"
  #include "Entities/Tools/MusicHolder.h"
  #include "Entities/Characters/EnemyBase.h"
  #include "Entities/Players/PlayerActionMarker.h"
  #include "Entities/Effects/BasicEffects.h"
  #include "Entities/Tools/BackgroundViewer.h"
  #include "Entities/Effects/WorldSettingsController.h"
  #include "Entities/Tools/ScrollHolder.h"
  #include "Entities/Effects/TextFXHolder.h"
  #include "Entities/Weapons/SeriousBomb.h"
  #include "Entities/Tools/CreditsHolder.h"
  #include "Entities/Effects/HudPicHolder.h"
  #include "Entities/Items/HealthItem.h"
  #include "Entities/Items/ArmorItem.h"
  #include "Entities/Items/WeaponItem.h"
  #include "Entities/Items/AmmoItem.h"
  #include "Entities/Items/PowerUpItem.h"
  #include "Entities/Items/MessageItem.h"
  #include "Entities/Items/AmmoPack.h"
  #include "Entities/Items/KeyItem.h"
  #include "Entities/Items/TreasureItem.h"

  extern void JumpFromBouncer(CEntity *penToBounce, CEntity *penBouncer);
  // from game
  #define GRV_SHOWEXTRAS  (1L<<0)   // add extra stuff like console, weapon, pause

  #define GENDER_MALE     0
  #define GENDER_FEMALE   1
  #define GENDEROFFSET    100   // sound components for genders are offset by this value
%}

enum PlayerViewType
{
  0 PVT_PLAYEREYES      "",
  1 PVT_PLAYERAUTOVIEW  "",
  2 PVT_SCENECAMERA     "",
  3 PVT_3RDPERSONVIEW   "",
};

enum PlayerState
{
  0 PST_STAND     "",
  1 PST_CROUCH    "",
  2 PST_SWIM      "",
  3 PST_DIVE      "",
  4 PST_FALL      "",
};

enum PlayerHand
{
  0 E_HAND_INVALID "",
  1 E_HAND_MAIN  "",
  2 E_HAND_SECOND "",
  3 E_HAND_THIRD "",
  4 E_HAND_FOURTH "",
  5 E_HAND_BOTH  "",
};

enum WeaponSound
{
  0 E_WEAPON_SOUND_0       "",
  1 E_WEAPON_SOUND_1       "",
  2 E_WEAPON_SOUND_2       "",
  3 E_WEAPON_SOUND_3       "",
  4 E_WEAPON_SOUND_AMBIENT "",
};

// event for starting cinematic camera sequence
event ECameraStart
{
  CEntityPointer penCamera,   // the camera
};

// event for ending cinematic camera sequence
event ECameraStop
{
  CEntityPointer penCamera,   // the camera
};

// sent when needs to rebirth
event ERebirth {};

// sent when player was disconnected from game
event EDisconnected {};

// starts automatic player actions
event EAutoAction
{
  CEntityPointer penFirstMarker,
};

event EToggleCheat
{
  INDEX iFlag,
};

event EGiveAllCheat
{
  INDEX iFlags,
};

event EGiveWeaponCheat
{
  INDEX iWeapon,
};

event EGoToMarkerCheat
{
  INDEX iMarker,
};

event EShieldPickup
{
  INDEX iQuantity,      // health to receive
  BOOL bOverTopValue,  // can be received over top value
};

%{  
  extern void DrawHUD(const CPawnEntity *penPlayerCurrent, CDrawPort *pdpCurrent, BOOL bSnooping, const CPlayerPawnEntity *penPlayerOwner);
  extern void InitHUD(void);
  extern void EndHUD(void);

  static CTimerValue _tvProbingLast;

  // used to render certain entities only for certain players (like picked items, etc.)
  extern ULONG _ulPlayerRenderingMask = 0;

  // temporary BOOL used to discard calculating of 3rd view when calculating absolute view placement
  BOOL _bDiscard3rdView=FALSE;

  #define NAME name

  const FLOAT _fBlowUpAmmount = 70.0f;

  // computer message adding flags
  #define CMF_READ       (1L<<0)
  #define CMF_ANALYZE    (1L<<1)

  struct MarkerDistance {
  public:
    FLOAT md_fMinD;
    CPlayerMarkerEntity *md_ppm;
    void Clear(void);
  };

  // export current player projection
  CAnyProjection3D prPlayerProjection;


  int qsort_CompareMarkerDistance(const void *pv0, const void *pv1)
  {
    MarkerDistance &md0 = *(MarkerDistance*)pv0;
    MarkerDistance &md1 = *(MarkerDistance*)pv1;
    if (md0.md_fMinD<md1.md_fMinD) return +1;
    else if (md0.md_fMinD>md1.md_fMinD) return -1;
    else                                return  0;
  }

  static inline FLOAT IntensityAtDistance(FLOAT fFallOff, FLOAT fHotSpot, FLOAT fDistance)
  {
    // intensity is zero if further than fall-off range
    if (fDistance>fFallOff) return 0.0f;
    // intensity is maximum if closer than hot-spot range
    if (fDistance<fHotSpot) return 1.0f;
    // interpolate if between fall-off and hot-spot range
    return (fFallOff-fDistance)/(fFallOff-fHotSpot);
  }

  static CTString MakeEmptyString(INDEX ctLen, char ch=' ')
  {
    char ach[2];
    ach[0] = ch;
    ach[1] = 0;
    CTString strSpaces;
    for (INDEX i=0; i<ctLen; i++) {
      strSpaces+=ach;
    }

    return strSpaces;
  }

  // take a two line string and align into one line of minimum given length
  static INDEX _ctAlignWidth = 20;
  static CTString AlignString(const CTString &strOrg)
  {
    // split into two lines
    CTString strL = strOrg;
    strL.OnlyFirstLine();
    CTString strR = strOrg;
    strR.RemovePrefix(strL);
    strR.DeleteChar(0);

    // get their lengths
    INDEX iLenL = strL.LengthNaked();
    INDEX iLenR = strR.LengthNaked();

    // find number of spaces to insert
    INDEX ctSpaces = _ctAlignWidth-(iLenL+iLenR);
    if (ctSpaces<1) {
      ctSpaces=1;
    }

    // make aligned string
    return strL+MakeEmptyString(ctSpaces)+strR;
  }

  static CTString CenterString(const CTString &str)
  {
    INDEX ctSpaces = (_ctAlignWidth-str.LengthNaked())/2;

    if (ctSpaces<0) {
      ctSpaces=0;
    }

    return MakeEmptyString(ctSpaces)+str;
  }

  static CTString PadStringRight(const CTString &str, INDEX iLen)
  {
    INDEX ctSpaces = iLen-str.LengthNaked();

    if (ctSpaces<0) {
      ctSpaces=0;
    }

    return str+MakeEmptyString(ctSpaces);
  }

  static CTString PadStringLeft(const CTString &str, INDEX iLen)
  {
    INDEX ctSpaces = iLen-str.LengthNaked();

    if (ctSpaces<0) {
      ctSpaces=0;
    }

    return MakeEmptyString(ctSpaces)+str;
  }

  static void KillAllEnemies(CEntity *penKiller)
  {
    // for each entity in the world
    {FOREACHINDYNAMICCONTAINER(penKiller->GetWorld()->wo_cenPawnEntities, CEntity, iten) {
      CEntity *pen = iten;

      if (CEntity::IsDerivedFromClass(pen, &CEnemyBaseEntity_DLLClass) && !CEntity::IsOfClass(pen, "Devil")) {
        CEnemyBaseEntity *penEnemy = (CEnemyBaseEntity *)pen;
        if (penEnemy->m_penEnemy==NULL) {
          continue;
        }

        FLOAT3D vHitPoint = pen->GetPlacement().pl_PositionVector;
        INDEX iKillDamage = penEnemy->GetHealth() + 1;
        penKiller->InflictDirectDamage(pen, penKiller, DMT_DAMAGER, iKillDamage, vHitPoint, FLOAT3D(0,1,0));
      }
    }}
  }


  #define HEADING_MAX      45.0f
  #define PITCH_MAX        90.0f
  #define BANKING_MAX      45.0f

  // player flags
  enum PlayerFlags
  {
    PLF_INITIALIZED         = (1UL << 0),   // set when player entity is ready to function
    PLF_VIEWROTATIONCHANGED = (1UL << 1),   // for adjusting view rotation separately from legs
    PLF_JUMPALLOWED         = (1UL << 2),   // if jumping is allowed
    PLF_SYNCWEAPON          = (1UL << 3),   // weapon model needs to be synchronized before rendering
    PLF_AUTOMOVEMENTS       = (1UL << 4),   // complete automatic control of movements
    PLF_DONTRENDER          = (1UL << 5),   // don't render view (used at end of level)
    PLF_CHANGINGLEVEL       = (1UL << 6),   // mark that we next are to appear at start of new level
    PLF_APPLIEDACTION       = (1UL << 7),   // used to detect when player is not connected
    PLF_NOTCONNECTED        = (1UL << 8),   // set if the player is not connected
    PLF_LEVELSTARTED        = (1UL << 9),   // marks that level start time was recorded
    PLF_ISZOOMING           = (1UL << 10),  // marks that player is zoomed in with the sniper
    PLF_RESPAWNINPLACE      = (1UL << 11),  // don't move to marker when respawning (for current death only)
  };

  #define PICKEDREPORT_TIME   (2.0f)  // how long (picked-up) message stays on screen

  // is player spying another player
  //extern TIME _tmSnoopingStarted;
  //extern CEntity *_penTargeting;
  
  // mana for ammo adjustment (multiplier)
  #define MANA_AMMO (0.1f)

  // cheats
  extern INDEX cht_bGod          = FALSE;
  extern INDEX cht_bFly          = FALSE;
  extern INDEX cht_bTurbo        = FALSE;
  extern INDEX cht_bGhost        = FALSE;
  extern INDEX cht_bInfiniteAmmo = FALSE;
  extern INDEX cht_bInvisible  = FALSE;
  static INDEX cht_bKillAll      = FALSE;
  static INDEX cht_bOpen       = FALSE;
  static INDEX cht_bRefresh    = FALSE;
  extern INDEX cht_iGiveAll    = 0;
  extern INDEX cht_iGiveWeapon = 0;
  static INDEX cht_iGoToMarker = -1;

  // interface control
  static INDEX hud_bShowAll	    = TRUE; // used internaly in menu/console
  extern INDEX hud_bShowWeapon  = TRUE;
  extern INDEX hud_bShowMessages = TRUE;
  extern INDEX hud_bShowInfo    = TRUE;
  extern INDEX hud_bShowLatency = FALSE;
  extern INDEX hud_iShowPlayers = -1;   // auto
  extern INDEX hud_iSortPlayers = -1;   // auto
  extern FLOAT hud_fOpacity     = 0.9f;
  extern FLOAT hud_fScaling     = 1.0f;
  extern FLOAT hud_tmWeaponsOnScreen = 3.0f;
  extern INDEX hud_bShowMatchInfo = TRUE;

  extern FLOAT plr_fBreathingStrength = 0.0f;
  extern FLOAT plr_tmSnoopingTime;
  extern INDEX cht_bKillFinalBoss = FALSE;
  INDEX cht_bDebugFinalBoss = FALSE;
  INDEX cht_bDumpFinalBossData = FALSE;
  INDEX cht_bDebugFinalBossAnimations = FALSE;
  INDEX cht_bDumpPlayerShading = FALSE;

  extern FLOAT wpn_fRecoilSpeed[17]   = {0};
  extern FLOAT wpn_fRecoilLimit[17]   = {0};
  extern FLOAT wpn_fRecoilDampUp[17]  = {0};
  extern FLOAT wpn_fRecoilDampDn[17]  = {0};
  extern FLOAT wpn_fRecoilOffset[17]  = {0};
  extern FLOAT wpn_fRecoilFactorP[17] = {0};
  extern FLOAT wpn_fRecoilFactorZ[17] = {0};

  // misc
  static FLOAT plr_fAcceleration  = 100.0f;
  static FLOAT plr_fDeceleration  = 60.0f;
  
  extern FLOAT plr_fSpeedForward  = 10.0f;
  extern FLOAT plr_fSpeedBackward = 10.0f;
  extern FLOAT plr_fSpeedSide     = 10.0f;
  extern FLOAT plr_fSpeedUp       = 11.0f;

  static FLOAT plr_fViewHeightStand  = 1.9f;
  static FLOAT plr_fViewHeightCrouch = 0.7f;
  static FLOAT plr_fViewHeightSwim   = 0.4f;
  static FLOAT plr_fViewHeightDive   = 0.0f;
  extern FLOAT plr_fViewDampFactor        = 0.4f;
  extern FLOAT plr_fViewDampLimitGroundUp = 0.1f;
  extern FLOAT plr_fViewDampLimitGroundDn = 0.4f;
  extern FLOAT plr_fViewDampLimitWater    = 0.1f;
  static FLOAT plr_fFrontClipDistance = 0.25f;
  static FLOAT plr_fFOV = 90.0f;
  extern INDEX plr_bRenderPicked = FALSE;
  extern INDEX plr_bRenderPickedParticles = FALSE;
  extern INDEX plr_bOnlySam = FALSE;
  extern INDEX ent_bReportBrokenChains = FALSE;
  extern FLOAT ent_tmMentalIn   = 0.5f;
  extern FLOAT ent_tmMentalOut  = 0.75f;
  extern FLOAT ent_tmMentalFade = 0.5f;

  extern FLOAT gfx_fEnvParticlesDensity = 1.0f;
  extern FLOAT gfx_fEnvParticlesRange = 1.0f;

  // prediction control vars
  extern FLOAT cli_fPredictPlayersRange = 0.0f;
  extern FLOAT cli_fPredictItemsRange = 3.0f;
  extern FLOAT cli_tmPredictFoe = 10.0f;
  extern FLOAT cli_tmPredictAlly = 10.0f;
  extern FLOAT cli_tmPredictEnemy  = 10.0f;

  static FLOAT plr_fSwimSoundDelay = 0.8f;
  static FLOAT plr_fDiveSoundDelay = 1.6f;
  static FLOAT plr_fWalkSoundDelay = 0.5f;
  static FLOAT plr_fRunSoundDelay  = 0.3f;

  // !=NULL if some player wants to call computer
  DECL_DLL extern class CPlayerPawnEntity *cmp_ppenPlayer = NULL;
  // !=NULL for rendering computer on secondary display in dualhead
  DECL_DLL extern class CPlayerPawnEntity *cmp_ppenDHPlayer = NULL;
  // set to update current message in background mode (for dualhead)
  DECL_DLL extern BOOL cmp_bUpdateInBackground = FALSE;
  // set for initial calling computer without rendering game
  DECL_DLL extern BOOL cmp_bInitialStart = FALSE;

  // game sets this for player hud and statistics and hiscore sound playing
  DECL_DLL extern INDEX plr_iHiScore = 0.0f;

  void CPlayerPawnEntity_Precache(void)
  {
    CLibEntityClass *pdec = &CPlayerPawnEntity_DLLClass;

    // precache view
    extern void CPlayerViewEntity_Precache(void);
    CPlayerViewEntity_Precache();

    // precache all player sounds
    pdec->PrecacheSound(SOUND_WATER_ENTER        );
    pdec->PrecacheSound(SOUND_WATER_LEAVE        );
    pdec->PrecacheSound(SOUND_WALK_L             );
    pdec->PrecacheSound(SOUND_WALK_R             );
    pdec->PrecacheSound(SOUND_WALK_SAND_L        );
    pdec->PrecacheSound(SOUND_WALK_SAND_R        );
    pdec->PrecacheSound(SOUND_SWIM_L             );
    pdec->PrecacheSound(SOUND_SWIM_R             );
    pdec->PrecacheSound(SOUND_DIVE_L             );
    pdec->PrecacheSound(SOUND_DIVE_R             );
    pdec->PrecacheSound(SOUND_DIVEIN             );
    pdec->PrecacheSound(SOUND_DIVEOUT            );
    pdec->PrecacheSound(SOUND_DROWN              );
    pdec->PrecacheSound(SOUND_INHALE0            );
    pdec->PrecacheSound(SOUND_JUMP               );
    pdec->PrecacheSound(SOUND_LAND               );
    pdec->PrecacheSound(SOUND_WOUNDWEAK          );
    pdec->PrecacheSound(SOUND_WOUNDMEDIUM        );
    pdec->PrecacheSound(SOUND_WOUNDSTRONG        );
    pdec->PrecacheSound(SOUND_WOUNDWATER         );
    pdec->PrecacheSound(SOUND_DEATH              );
    pdec->PrecacheSound(SOUND_DEATHWATER         );
    pdec->PrecacheSound(SOUND_WATERAMBIENT       );
    pdec->PrecacheSound(SOUND_WATERBUBBLES       );
    pdec->PrecacheSound(SOUND_WATERWALK_L        );
    pdec->PrecacheSound(SOUND_WATERWALK_R        );
    pdec->PrecacheSound(SOUND_INHALE1            );
    pdec->PrecacheSound(SOUND_INHALE2            );
    pdec->PrecacheSound(SOUND_INFO               );
    pdec->PrecacheSound(SOUND_WALK_GRASS_L       );
    pdec->PrecacheSound(SOUND_WALK_GRASS_R       );
    pdec->PrecacheSound(SOUND_WALK_WOOD_L        );
    pdec->PrecacheSound(SOUND_WALK_WOOD_R        );
    pdec->PrecacheSound(SOUND_WALK_SNOW_L        );
    pdec->PrecacheSound(SOUND_WALK_SNOW_R        );
  //pdec->PrecacheSound(SOUND_HIGHSCORE          );
    pdec->PrecacheSound(SOUND_SNIPER_ZOOM        );
    pdec->PrecacheSound(SOUND_SNIPER_QZOOM       );
    pdec->PrecacheSound(SOUND_SILENCE            );
    pdec->PrecacheSound(SOUND_POWERUP_BEEP       );

    pdec->PrecacheSound(SOUND_F_WATER_ENTER        );
    pdec->PrecacheSound(SOUND_F_WATER_LEAVE        );
    pdec->PrecacheSound(SOUND_F_WALK_L             );
    pdec->PrecacheSound(SOUND_F_WALK_R             );
    pdec->PrecacheSound(SOUND_F_WALK_SAND_L        );
    pdec->PrecacheSound(SOUND_F_WALK_SAND_R        );
    pdec->PrecacheSound(SOUND_F_SWIM_L             );
    pdec->PrecacheSound(SOUND_F_SWIM_R             );
    pdec->PrecacheSound(SOUND_F_DIVE_L             );
    pdec->PrecacheSound(SOUND_F_DIVE_R             );
    pdec->PrecacheSound(SOUND_F_DIVEIN             );
    pdec->PrecacheSound(SOUND_F_DIVEOUT            );
    pdec->PrecacheSound(SOUND_F_DROWN              );
    pdec->PrecacheSound(SOUND_F_INHALE0            );
    pdec->PrecacheSound(SOUND_F_JUMP               );
    pdec->PrecacheSound(SOUND_F_LAND               );
    pdec->PrecacheSound(SOUND_F_WOUNDWEAK          );
    pdec->PrecacheSound(SOUND_F_WOUNDMEDIUM        );
    pdec->PrecacheSound(SOUND_F_WOUNDSTRONG        );
    pdec->PrecacheSound(SOUND_F_WOUNDWATER         );
    pdec->PrecacheSound(SOUND_F_DEATH              );
    pdec->PrecacheSound(SOUND_F_DEATHWATER         );
    pdec->PrecacheSound(SOUND_F_WATERWALK_L        );
    pdec->PrecacheSound(SOUND_F_WATERWALK_R        );
    pdec->PrecacheSound(SOUND_F_INHALE1            );
    pdec->PrecacheSound(SOUND_F_INHALE2            );
    pdec->PrecacheSound(SOUND_F_WALK_GRASS_L       );
    pdec->PrecacheSound(SOUND_F_WALK_GRASS_R       );
    pdec->PrecacheSound(SOUND_F_WALK_WOOD_L        );
    pdec->PrecacheSound(SOUND_F_WALK_WOOD_R        );
    pdec->PrecacheSound(SOUND_F_WALK_SNOW_L        );
    pdec->PrecacheSound(SOUND_F_WALK_SNOW_R        );
  //pdec->PrecacheSound(SOUND_F_HIGHSCORE          );
    pdec->PrecacheSound(SOUND_BLOWUP               );

    pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_TELEPORT);
    pdec->PrecacheClass(CLASS_SERIOUSBOMB);

    pdec->PrecacheModel(MODEL_FLESH);
    pdec->PrecacheModel(MODEL_FLESH_APPLE);
    pdec->PrecacheModel(MODEL_FLESH_BANANA);
    pdec->PrecacheModel(MODEL_FLESH_BURGER);
    pdec->PrecacheTexture(TEXTURE_FLESH_RED);
    pdec->PrecacheTexture(TEXTURE_FLESH_GREEN);
    pdec->PrecacheTexture(TEXTURE_FLESH_APPLE);
    pdec->PrecacheTexture(TEXTURE_FLESH_BANANA);
    pdec->PrecacheTexture(TEXTURE_FLESH_BURGER);
    pdec->PrecacheTexture(TEXTURE_FLESH_LOLLY);
    pdec->PrecacheTexture(TEXTURE_FLESH_ORANGE);

    pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_BLOODSPILL);
    pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_BLOODSTAIN);
    pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_BLOODSTAINGROW);
    pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_BLOODEXPLODE);
  }

  void CPlayerPawnEntity_OnInitClass(void)
  {
    _pShell->DeclareSymbol("user FLOAT plr_fSwimSoundDelay;", &plr_fSwimSoundDelay);
    _pShell->DeclareSymbol("user FLOAT plr_fDiveSoundDelay;", &plr_fDiveSoundDelay);
    _pShell->DeclareSymbol("user FLOAT plr_fWalkSoundDelay;", &plr_fWalkSoundDelay);
    _pShell->DeclareSymbol("user FLOAT plr_fRunSoundDelay;",  &plr_fRunSoundDelay);

    _pShell->DeclareSymbol("persistent user FLOAT cli_fPredictPlayersRange;",&cli_fPredictPlayersRange);
    _pShell->DeclareSymbol("persistent user FLOAT cli_fPredictItemsRange;",  &cli_fPredictItemsRange  );
    _pShell->DeclareSymbol("persistent user FLOAT cli_tmPredictFoe;",        &cli_tmPredictFoe        );
    _pShell->DeclareSymbol("persistent user FLOAT cli_tmPredictAlly;",       &cli_tmPredictAlly       );
    _pShell->DeclareSymbol("persistent user FLOAT cli_tmPredictEnemy;",      &cli_tmPredictEnemy      );

    _pShell->DeclareSymbol("     INDEX hud_bShowAll;",     &hud_bShowAll);
    _pShell->DeclareSymbol("user INDEX hud_bShowInfo;",    &hud_bShowInfo);
    _pShell->DeclareSymbol("persistent user INDEX hud_bShowLatency;", &hud_bShowLatency);
    _pShell->DeclareSymbol("persistent user INDEX hud_iShowPlayers;", &hud_iShowPlayers);
    _pShell->DeclareSymbol("persistent user INDEX hud_iSortPlayers;", &hud_iSortPlayers);
    _pShell->DeclareSymbol("persistent user INDEX hud_bShowWeapon;",  &hud_bShowWeapon);
    _pShell->DeclareSymbol("persistent user INDEX hud_bShowMessages;",&hud_bShowMessages);
    _pShell->DeclareSymbol("persistent user FLOAT hud_fScaling;",     &hud_fScaling);
    _pShell->DeclareSymbol("persistent user FLOAT hud_fOpacity;",     &hud_fOpacity);
    _pShell->DeclareSymbol("persistent user FLOAT hud_tmWeaponsOnScreen;",  &hud_tmWeaponsOnScreen);
    _pShell->DeclareSymbol("persistent user FLOAT plr_fBreathingStrength;", &plr_fBreathingStrength);
    _pShell->DeclareSymbol("INDEX cht_bKillFinalBoss;",  &cht_bKillFinalBoss);
    _pShell->DeclareSymbol("INDEX cht_bDebugFinalBoss;", &cht_bDebugFinalBoss);
    _pShell->DeclareSymbol("INDEX cht_bDumpFinalBossData;", &cht_bDumpFinalBossData);
    _pShell->DeclareSymbol("INDEX cht_bDebugFinalBossAnimations;", &cht_bDebugFinalBossAnimations);
    _pShell->DeclareSymbol("INDEX cht_bDumpPlayerShading;", &cht_bDumpPlayerShading);
    _pShell->DeclareSymbol("persistent user INDEX hud_bShowMatchInfo;", &hud_bShowMatchInfo);

    _pShell->DeclareSymbol("persistent user FLOAT wpn_fRecoilSpeed[17];",   &wpn_fRecoilSpeed);
    _pShell->DeclareSymbol("persistent user FLOAT wpn_fRecoilLimit[17];",   &wpn_fRecoilLimit);
    _pShell->DeclareSymbol("persistent user FLOAT wpn_fRecoilDampUp[17];",  &wpn_fRecoilDampUp);
    _pShell->DeclareSymbol("persistent user FLOAT wpn_fRecoilDampDn[17];",  &wpn_fRecoilDampDn);
    _pShell->DeclareSymbol("persistent user FLOAT wpn_fRecoilOffset[17];",  &wpn_fRecoilOffset);
    _pShell->DeclareSymbol("persistent user FLOAT wpn_fRecoilFactorP[17];", &wpn_fRecoilFactorP);
    _pShell->DeclareSymbol("persistent user FLOAT wpn_fRecoilFactorZ[17];", &wpn_fRecoilFactorZ);

    // cheats
    _pShell->DeclareSymbol("user INDEX cht_bGod;",          &cht_bGod);
    _pShell->DeclareSymbol("user INDEX cht_bFly;",          &cht_bFly);
    _pShell->DeclareSymbol("user INDEX cht_bTurbo;",        &cht_bTurbo);
    _pShell->DeclareSymbol("user INDEX cht_bGhost;",        &cht_bGhost);
    _pShell->DeclareSymbol("user INDEX cht_bInfiniteAmmo;", &cht_bInfiniteAmmo);
    _pShell->DeclareSymbol("user INDEX cht_bInvisible;", &cht_bInvisible);
    _pShell->DeclareSymbol("user INDEX cht_bKillAll;",      &cht_bKillAll);
    _pShell->DeclareSymbol("user INDEX cht_bOpen;",      &cht_bOpen);
    _pShell->DeclareSymbol("user INDEX cht_bRefresh;", &cht_bRefresh);

    _pShell->DeclareSymbol("user INDEX cht_iGiveAll;",      &cht_iGiveAll);
    _pShell->DeclareSymbol("user INDEX cht_iGiveWeapon;",   &cht_iGiveWeapon);
    _pShell->DeclareSymbol("user INDEX cht_iGoToMarker;", &cht_iGoToMarker);
    
    /*
    // this one is masqueraded cheat enable variable
    _pShell->DeclareSymbol("INDEX cht_bEnable;", &cht_bEnable);
    */

    // player speed and view parameters, not declared except in internal build
    #if 0
      _pShell->DeclareSymbol("user FLOAT plr_fViewHeightStand;", &plr_fViewHeightStand);
      _pShell->DeclareSymbol("user FLOAT plr_fViewHeightCrouch;",&plr_fViewHeightCrouch);
      _pShell->DeclareSymbol("user FLOAT plr_fViewHeightSwim;",  &plr_fViewHeightSwim);
      _pShell->DeclareSymbol("user FLOAT plr_fViewHeightDive;",  &plr_fViewHeightDive);
      _pShell->DeclareSymbol("user FLOAT plr_fViewDampFactor;",         &plr_fViewDampFactor);
      _pShell->DeclareSymbol("user FLOAT plr_fViewDampLimitGroundUp;",  &plr_fViewDampLimitGroundUp);
      _pShell->DeclareSymbol("user FLOAT plr_fViewDampLimitGroundDn;",  &plr_fViewDampLimitGroundDn);
      _pShell->DeclareSymbol("user FLOAT plr_fViewDampLimitWater;",     &plr_fViewDampLimitWater);
      _pShell->DeclareSymbol("user FLOAT plr_fAcceleration;",  &plr_fAcceleration);
      _pShell->DeclareSymbol("user FLOAT plr_fDeceleration;",  &plr_fDeceleration);
      _pShell->DeclareSymbol("user FLOAT plr_fSpeedForward;",  &plr_fSpeedForward);
      _pShell->DeclareSymbol("user FLOAT plr_fSpeedBackward;", &plr_fSpeedBackward);
      _pShell->DeclareSymbol("user FLOAT plr_fSpeedSide;",     &plr_fSpeedSide);
      _pShell->DeclareSymbol("user FLOAT plr_fSpeedUp;",       &plr_fSpeedUp);
    #endif

    _pShell->DeclareSymbol("persistent user FLOAT plr_fFOV;", &plr_fFOV);
    _pShell->DeclareSymbol("persistent user FLOAT plr_fFrontClipDistance;", &plr_fFrontClipDistance);
    _pShell->DeclareSymbol("persistent user INDEX plr_bRenderPicked;", &plr_bRenderPicked);
    _pShell->DeclareSymbol("persistent user INDEX plr_bRenderPickedParticles;", &plr_bRenderPickedParticles);
    _pShell->DeclareSymbol("persistent user INDEX plr_bOnlySam;", &plr_bOnlySam);
    _pShell->DeclareSymbol("persistent user INDEX ent_bReportBrokenChains;", &ent_bReportBrokenChains);
    _pShell->DeclareSymbol("persistent user FLOAT ent_tmMentalIn  ;", &ent_tmMentalIn  );
    _pShell->DeclareSymbol("persistent user FLOAT ent_tmMentalOut ;", &ent_tmMentalOut );
    _pShell->DeclareSymbol("persistent user FLOAT ent_tmMentalFade;", &ent_tmMentalFade);
    _pShell->DeclareSymbol("persistent user FLOAT gfx_fEnvParticlesDensity;", &gfx_fEnvParticlesDensity);
    _pShell->DeclareSymbol("persistent user FLOAT gfx_fEnvParticlesRange;", &gfx_fEnvParticlesRange);

    // player appearance interface
    _pShell->DeclareSymbol("INDEX SetPlayerAppearance(INDEX, INDEX, INDEX, INDEX);", &SetPlayerAppearance);

    // call player weapons persistant variable initialization
    extern void CPlayerWeaponEntity_Init(void);
    CPlayerWeaponEntity_Init();

    // initialize HUD
    InitHUD();

    // precache
    CPlayerPawnEntity_Precache();
  }

  // clean up
  void CPlayerPawnEntity_OnEndClass(void)
  {
    EndHUD();
  }

  CTString GetDifficultyString(void)
  {
    if (GetSP()->sp_bMental) { return TRANS("Mental"); }

    switch (GetSP()->sp_gdGameDifficulty)
    {
      case CSessionProperties::GD_TOURIST:  return TRANS("Tourist");
      case CSessionProperties::GD_EASY:     return TRANS("Easy");
      default:
      case CSessionProperties::GD_NORMAL:   return TRANS("Normal");
      case CSessionProperties::GD_HARD:     return TRANS("Hard");
      case CSessionProperties::GD_EXTREME:  return TRANS("Serious");
    }
  }
  // armor & health constants getters

  FLOAT MaxArmor(void)
  {
    if (GetSP()->sp_gdGameDifficulty<=CSessionProperties::GD_EASY) {
      return 300 * HEALTH_VALUE_MULTIPLIER;
    } else {
      return 200 * HEALTH_VALUE_MULTIPLIER;
    }
  }

  FLOAT TopArmor(void)
  {
    if (GetSP()->sp_gdGameDifficulty<=CSessionProperties::GD_EASY) {
      return 200 * HEALTH_VALUE_MULTIPLIER;
    } else {
      return 100 * HEALTH_VALUE_MULTIPLIER;
    }
  }

  FLOAT MaxHealth(void)
  {
    if (GetSP()->sp_gdGameDifficulty<=CSessionProperties::GD_EASY) {
      return 300 * HEALTH_VALUE_MULTIPLIER;
    } else {
      return 200 * HEALTH_VALUE_MULTIPLIER;
    }
  }

  FLOAT TopHealth(void)
  {
    if (GetSP()->sp_gdGameDifficulty<=CSessionProperties::GD_EASY) {
      return 200 * HEALTH_VALUE_MULTIPLIER;
    } else {
      return 100 * HEALTH_VALUE_MULTIPLIER;
    }
  }

  // info structure
  static EntityInfo eiPlayerGround = {
    EIBT_FLESH, 80.0f,
    0.0f, 1.7f, 0.0f,     // source (eyes)
    0.0f, 1.0f, 0.0f,     // target (body)
  };
  static EntityInfo eiPlayerCrouch = {
    EIBT_FLESH, 80.0f,
    0.0f, 1.2f, 0.0f,     // source (eyes)
    0.0f, 0.7f, 0.0f,     // target (body)
  };
  static EntityInfo eiPlayerSwim = {
    EIBT_FLESH, 40.0f,
    0.0f, 0.0f, 0.0f,     // source (eyes)
    0.0f, 0.0f, 0.0f,     // target (body)
  };


  // animation light specific
  #define LIGHT_ANIM_MINIGUN 2
  #define LIGHT_ANIM_TOMMYGUN 3
  #define LIGHT_ANIM_COLT_SHOTGUN 4
  #define LIGHT_ANIM_NONE 5

  const char *NameForState(PlayerState pst)
  {
    switch (pst)
    {
      case PST_STAND: return "stand";
      case PST_CROUCH: return "crouch";
      case PST_FALL: return "fall";
      case PST_SWIM: return "swim";
      case PST_DIVE: return "dive";
      default: return "???";
    };
  }


  // print explanation on how a player died
  void PrintPlayerDeathMessage(CPlayerPawnEntity *ppl, const EDeath &eDeath)
  {
    CTString strMyName = ppl->GetPlayerName();
    CEntity *penKiller = eDeath.eLastDamage.penInflictor;

    // if killed by a valid entity
    if (penKiller!=NULL) {
      // if killed by a player
      if (CEntity::IsDerivedFromClass(penKiller, &CPlayerPawnEntity_DLLClass)) {
        // if not self
        if (penKiller!=ppl) {
          CTString strKillerName = ((CPlayerPawnEntity*)penKiller)->GetPlayerName();

          if (eDeath.eLastDamage.iDamageType==DMT_TELEPORT) {
            CInfoF(TRANS("%s telefragged %s\n"), strKillerName, strMyName);
          } else if (eDeath.eLastDamage.iDamageType==DMT_CLOSERANGE) {
            CInfoF(TRANS("%s cut %s into pieces\n"), strKillerName, strMyName);
          } else if (eDeath.eLastDamage.iDamageType==DMT_CHAINSAW) {
            CInfoF(TRANS("%s cut %s into pieces\n"), strKillerName, strMyName);
          } else if (eDeath.eLastDamage.iDamageType==DMT_BULLET) {
            CInfoF(TRANS("%s poured lead into %s\n"), strKillerName, strMyName);
          } else if (eDeath.eLastDamage.iDamageType==DMT_PROJECTILE || eDeath.eLastDamage.iDamageType==DMT_EXPLOSION) {
            CInfoF(TRANS("%s blew %s away\n"), strKillerName, strMyName);
          } else if (eDeath.eLastDamage.iDamageType==DMT_CANNONBALL) {
            CInfoF(TRANS("%s smashed %s with a cannon\n"), strKillerName, strMyName);
          } else if (eDeath.eLastDamage.iDamageType==DMT_CANNONBALL_EXPLOSION) {
            CInfoF(TRANS("%s nuked %s\n"), strKillerName, strMyName);
          } else {
            CInfoF(TRANS("%s killed %s\n"), strKillerName, strMyName);
          }

        } else {
          // make message from damage type
          switch (eDeath.eLastDamage.iDamageType)
          {
            case DMT_DROWNING:  CInfoF(TRANS("%s drowned\n"), strMyName); break;
            case DMT_BURNING:   CInfoF(TRANS("%s burst into flames\n"), strMyName); break;
            case DMT_SPIKESTAB: CInfoF(TRANS("%s fell into a spike-hole\n"), strMyName); break;
            case DMT_FREEZING:  CInfoF(TRANS("%s has frozen\n"), strMyName); break;
            case DMT_ACID:      CInfoF(TRANS("%s dissolved\n"), strMyName); break;
            case DMT_PROJECTILE:
            case DMT_EXPLOSION:
              CInfoF(TRANS("%s blew himself away\n"), strMyName); break;
            default:            CInfoF(TRANS("%s has committed suicide\n"), strMyName);
          }
        }

      // if killed by an enemy
      } else if (CEntity::IsDerivedFromClass(penKiller, &CEnemyBaseEntity_DLLClass)) {
        // check for telefrag first
        if (eDeath.eLastDamage.iDamageType==DMT_TELEPORT) {
          CInfoF(TRANS("%s was telefragged\n"), strMyName);
          return;
        }
        // describe how this enemy killed player
        CInfoF("%s\n", (const char*)((CEnemyBaseEntity*)penKiller)->GetPlayerKillDescription(strMyName, eDeath));

      // if killed by some other entity
      } else {
        // make message from damage type
        switch (eDeath.eLastDamage.iDamageType)
        {
          case DMT_SPIKESTAB: CInfoF(TRANS("%s was pierced\n"), strMyName); break;
          case DMT_BRUSH:     CInfoF(TRANS("%s was squashed\n"), strMyName); break;
          case DMT_ABYSS:     CInfoF(TRANS("%s went over the edge\n"), strMyName); break;
          case DMT_IMPACT:    CInfoF(TRANS("%s swashed\n"), strMyName); break;
          case DMT_HEAT:      CInfoF(TRANS("%s stood in the sun for too long\n"), strMyName); break;
          default:            CInfoF(TRANS("%s passed away\n"), strMyName);
        }
      }
    // if no entity pointer (shouldn't happen)
    } else {
      CInfoF(TRANS("%s is missing in action\n"), strMyName);
    }
  }
  
  extern const char *GetNameForAmmoIndex(AmmoIndex eAmmoIndex);
  extern INDEX GetManaForAmmoIndex(AmmoIndex eAmmoIndex);
  
  extern const char *GetWeaponNameForIndex(WeaponIndex eWeaponIndex);
  extern const char *GetStatusEffectNameForIndex(StatusEffectIndex eStatusEffectIndex);
  extern const char *GetGadgetNameForIndex(GadgetIndex eGadgetIndex);
%}

class export CPlayerPawnEntity : CPawnEntity {
name      "Player";
thumbnail "";
features  "ImplementsOnInitClass", "ImplementsOnEndClass", "CanBePredictable";

properties:
  1 CTString m_strName "Name" = "<unnamed player>",
  2 COLOR m_ulLastButtons = 0x0,              // buttons last pressed
  4 INDEX m_iArmor = 0,                       // armor
  5 INDEX m_iShield = 0,
  6 CTString m_strGroup = "",                 // group name for world change
  7 INDEX m_ulFlags = 0,                      // various flags
  8 INDEX m_ulCheatFlags = 0,
  9 INDEX m_iGoToMarker = -1,

 14 CEntityPointer m_penInventory,            // [SSE] Player Inventory Entity
 15 CEntityPointer m_penWeaponsFirst,         // Main Hand
 16 CEntityPointer m_penWeaponsSecond,        // Off Hand
 17 CEntityPointer m_penAnimator,             // player animator
 18 CEntityPointer m_penView,                 // player view
 19 CEntityPointer m_pen3rdPersonView,        // player 3rd person view
 20 INDEX m_iViewState=PVT_PLAYEREYES,        // view state
 21 INDEX m_iLastViewState=PVT_PLAYEREYES,    // last view state

 26 CAnimObject m_aoLightAnimation,           // light animation object
 27 INDEX m_iDamageAmount = 0.0f,            // how much was last wound
 28 FLOAT m_tmWoundedTime  = 0.0f,            // when was last wound
 29 FLOAT m_tmScreamTime   = 0.0f,            // when was last wound sound played

 33 INDEX m_iGender = GENDER_MALE,            // male/female offset in various tables
 34 enum PlayerState m_pstState = PST_STAND,  // current player state
 35 FLOAT m_fFallTime = 0.0f,                 // time passed when falling
 36 FLOAT m_fSwimTime = 0.0f,                 // time when started swimming
 45 FLOAT m_tmOutOfWater = 0.0f,              // time when got out of water last time
 37 FLOAT m_tmMoveSound = 0.0f,           // last time move sound was played
 38 BOOL  m_bMoveSoundLeft = TRUE,        // left or right walk channel is current
 39 FLOAT m_tmNextAmbientOnce = 0.0f,     // next time to play local ambient sound
 43 FLOAT m_tmMouthSoundLast = 0.0f,      // time last played some repeating mouth sound

 40 CEntityPointer m_penCamera,           // camera for current cinematic sequence, or null
 41 CTString m_strCenterMessage="",       // center message
 42 FLOAT m_tmCenterMessageEnd = 0.0f,    // last time to show centered message
 48 BOOL m_bPendingMessage = FALSE,   // message sound pending to be played
 47 FLOAT m_tmMessagePlay = 0.0f,     // when to play the message sound
 49 FLOAT m_tmAnalyseEnd = 0.0f,      // last time to show analysation
 50 BOOL m_bComputerInvoked = FALSE,  // set if computer was invoked at least once
 57 FLOAT m_tmAnimateInbox = -100.0f,      // show animation of inbox icon animation

 44 CEntityPointer m_penMainMusicHolder,

 51 FLOAT m_tmLastDamage = -1.0f,
 52 INDEX m_iMaxDamageAmount = 0,
 53 FLOAT3D m_vDamage = FLOAT3D(0, 0, 0),
 54 FLOAT m_tmSpraySpawned = -1.0f,
 55 INDEX m_iSprayDamage = 0,
 56 CEntityPointer m_penSpray,

 // sounds
 59 CSoundObject m_soWeapon0,
 60 CSoundObject m_soWeapon1,
 61 CSoundObject m_soWeapon2,
 62 CSoundObject m_soWeapon3,
 63 CSoundObject m_soWeaponAmbient,

 64 CSoundObject m_soSecondWeapon0,
 65 CSoundObject m_soSecondWeapon1,
 66 CSoundObject m_soSecondWeapon2,
 67 CSoundObject m_soSecondWeapon3,
 68 CSoundObject m_soSecondWeaponAmbient,

 69 CSoundObject m_soPowerUpBeep,

 70 CSoundObject m_soMouth,     // breating, yelling etc.
 71 CSoundObject m_soFootL,     // walking etc.
 72 CSoundObject m_soFootR,
 73 CSoundObject m_soBody,          // splashing etc.
 74 CSoundObject m_soLocalAmbientLoop,  // local ambient that only this player hears
 75 CSoundObject m_soLocalAmbientOnce,  // local ambient that only this player hears
 76 CSoundObject m_soMessage,  // message sounds
 77 CSoundObject m_soHighScore, // high score sound
 78 CSoundObject m_soSpeech,    // for quotes
 79 CSoundObject m_soSniperZoom, // for sniper zoom sound

 81 INDEX m_iMana    = 0,        // current score worth for killed player
 94 FLOAT m_fManaFraction = 0.0f,// fractional part of mana, for slow increase with time
 84 INDEX m_iHighScore = 0,      // internal hiscore for demo playing
 85 INDEX m_iBeatenHighScore = 0,    // hiscore that was beaten

 96 BOOL  m_bEndOfLevel = FALSE,
 97 BOOL  m_bEndOfGame  = FALSE,
 98 INDEX m_iMayRespawn = 0,     // must get to 2 to be able to respawn
 99 FLOAT m_tmSpawned = 0.0f,   // when player was spawned
 100 FLOAT3D m_vDied = FLOAT3D(0, 0, 0),  // where player died (for respawn in-place)
 101 FLOAT3D m_aDied = FLOAT3D(0, 0, 0),

 // statistics
 103 FLOAT m_tmEstTime  = 0.0f,   // time estimated for this level
 105 INDEX m_iTimeScore = 0,
 106 INDEX m_iStartTime = 0,      // game start time (ansi c time_t type)
 107 INDEX m_iEndTime   = 0,      // game end time (ansi c time_t type)
 108 FLOAT m_tmLevelStarted = 0.0f,  // game time when level started
 93 CTString m_strLevelStats = "",  // detailed statistics for each level

 // auto action vars
 110 CEntityPointer m_penActionMarker,  // current marker for auto actions
 111 FLOAT m_fAutoSpeed = 0.0f, // speed to go towards the marker
 112 INDEX m_iAutoOrgWeapon = 0, // original weapon for autoactions
 113 FLOAT3D m_vAutoSpeed = FLOAT3D(0, 0, 0),
 114 FLOAT m_tmSpiritStart = 0.0f,
 115 FLOAT m_tmFadeStart = 0.0f,

 // 'picked up' display vars
 120 FLOAT m_tmLastPicked = -10000.0f,  // when something was last picked up
 121 CTString m_strPickedName = "",     // name of item picked
 122 INDEX m_iPickedAmount = 0.0f,     // total picked ammount
 123 FLOAT m_fPickedMana = 0.0f,        // total picked mana

 // shaker values
 130 INDEX m_iLastHealth = 0,
 131 INDEX m_iLastArmor  = 0,
 132 INDEX m_iLastAmmo   = 0,
 133 INDEX m_iLastAmmo2nd   = 0,
 135 FLOAT m_tmHealthChanged = -9,
 136 FLOAT m_tmArmorChanged  = -9,
 137 FLOAT m_tmAmmoChanged   = -9,
 138 FLOAT m_tmAmmoChanged2nd = -9,

 139 FLOAT m_tmMinigunAutoFireStart = -1.0f,

 150 FLOAT3D m_vLastStain  = FLOAT3D(0, 0, 0), // where last stain was left

 // for mouse lag elimination via prescanning
 151 ANGLE3D m_aLastRotation = FLOAT3D(0, 0, 0),
 152 ANGLE3D m_aLastViewRotation = FLOAT3D(0, 0, 0),
 153 FLOAT3D m_vLastTranslation = FLOAT3D(0, 0, 0),

 180 FLOAT m_tmChainShakeEnd = 0.0f, // used to determine when to stop shaking due to chainsaw damage
 181 FLOAT m_fChainShakeStrength = 1.0f, // strength of shaking
 182 FLOAT m_fChainShakeFreqMod = 1.0f,  // shaking frequency modifier
 183 FLOAT m_fChainsawShakeDX = 0.0f,
 184 FLOAT m_fChainsawShakeDY = 0.0f,

 190 CEntityPointer m_penGameInfo,
 
 // Gadgets.
 200 FLOAT m_tmGadgetActivated = -10.0f,  // when the bomb was last fired
 202 FLOAT m_tmGadgetSelection = 0.0F,
 203 INDEX m_iLastActivatedGadget = E_GADGET_INVALID,

 210 FLOAT m_tmDualWieldSelection = 0.0F,

 215 FLOAT m_tmFavoriteSelection = 0.0F,
 
 220 FLOAT m_fHealthRegen = 0.0F,

 // Various statistics.
 230 CUSTOMDATA m_cdLevelStats features(EPROPF_SIMULATIONONLY),
 231 CUSTOMDATA m_cdLevelTotalStats features(EPROPF_SIMULATIONONLY),
 232 CUSTOMDATA m_cdGameStats features(EPROPF_SIMULATIONONLY),
 233 CUSTOMDATA m_cdGameTotalStats features(EPROPF_SIMULATIONONLY),

 241 CUSTOMDATA m_cdMessages features(EPROPF_SIMULATIONONLY),
 
{
  ShellLaunchData ShellLaunchData_array;  // array of data describing flying empty shells
  INDEX m_iFirstEmptySLD;                         // index of last added empty shell

  BulletSprayLaunchData BulletSprayLaunchData_array;  // array of data describing flying bullet sprays
  INDEX m_iFirstEmptyBSLD;                            // index of last added bullet spray

  GoreSprayLaunchData GoreSprayLaunchData_array;   // array of data describing gore sprays
  INDEX m_iFirstEmptyGSLD;                         // index of last added gore spray

  ULONG ulButtonsNow;  ULONG ulButtonsBefore;
  ULONG ulNewButtons;
  ULONG ulReleasedButtons;

  BOOL  bAimButtonHeld;

  // listener
  CSoundListener sliSound;
  // light
  CLightSource m_lsLightSource;

  TIME m_tmPredict;  // time to predict the entity to

  // all messages in the inbox
  CDynamicStackArray<CCompMessageID> m_acmiMessages;
  INDEX m_ctUnreadMessages;

  // statistics
  PlayerStats m_psLevelStats;
  PlayerStats m_psLevelTotal;
  PlayerStats m_psGameStats;
  PlayerStats m_psGameTotal;

  CModelObject m_moRender;                  // model object to render - this one can be customized
}

resources:

  1 class   CLASS_PLAYER_WEAPONS   "Classes\\PlayerWeapon.ecl",
  2 class   CLASS_PLAYER_ANIMATOR  "Classes\\PlayerAnimator.ecl",
  3 class   CLASS_PLAYER_VIEW      "Classes\\PlayerView.ecl",
  4 class   CLASS_BASIC_EFFECT     "Classes\\BasicEffect.ecl",
  5 class   CLASS_BLOOD_SPRAY      "Classes\\BloodSpray.ecl",
  6 class   CLASS_SERIOUSBOMB      "Classes\\SeriousBomb.ecl",
  7 class   CLASS_PLAYER_INVENTORY "Classes\\PlayerInventory.ecl",

// gender specific sounds - make sure that offset is exactly 100
 50 sound SOUND_WATER_ENTER     "Sounds\\Player\\WaterEnter.wav",
 51 sound SOUND_WATER_LEAVE     "Sounds\\Player\\WaterLeave.wav",
 52 sound SOUND_WALK_L          "Sounds\\Player\\WalkL.wav",
 53 sound SOUND_WALK_R          "Sounds\\Player\\WalkR.wav",
 54 sound SOUND_SWIM_L          "Sounds\\Player\\SwimL.wav",
 55 sound SOUND_SWIM_R          "Sounds\\Player\\SwimR.wav",
 56 sound SOUND_DIVE_L          "Sounds\\Player\\Dive.wav",
 57 sound SOUND_DIVE_R          "Sounds\\Player\\Dive.wav",
 58 sound SOUND_DIVEIN          "Sounds\\Player\\DiveIn.wav",
 59 sound SOUND_DIVEOUT         "Sounds\\Player\\DiveOut.wav",
 60 sound SOUND_DROWN           "Sounds\\Player\\Drown.wav",
 61 sound SOUND_INHALE0         "Sounds\\Player\\Inhale00.wav",
 62 sound SOUND_JUMP            "Sounds\\Player\\Jump.wav",
 63 sound SOUND_LAND            "Sounds\\Player\\Land.wav",
 66 sound SOUND_DEATH           "Sounds\\Player\\Death.wav",
 67 sound SOUND_DEATHWATER      "Sounds\\Player\\DeathWater.wav",
 70 sound SOUND_WATERWALK_L     "Sounds\\Player\\WalkWaterL.wav",
 71 sound SOUND_WATERWALK_R     "Sounds\\Player\\WalkWaterR.wav",
 72 sound SOUND_INHALE1         "Sounds\\Player\\Inhale01.wav",
 73 sound SOUND_INHALE2         "Sounds\\Player\\Inhale02.wav",
 75 sound SOUND_WALK_SAND_L     "Sounds\\Player\\WalkSandL.wav",
 76 sound SOUND_WALK_SAND_R     "Sounds\\Player\\WalkSandR.wav",
//178 sound SOUND_HIGHSCORE       "Sounds\\Player\\HighScore.wav",
 80 sound SOUND_WOUNDWEAK       "Sounds\\Player\\WoundWeak.wav",
 81 sound SOUND_WOUNDMEDIUM     "Sounds\\Player\\WoundMedium.wav",
 82 sound SOUND_WOUNDSTRONG     "Sounds\\Player\\WoundStrong.wav",
 85 sound SOUND_WOUNDWATER      "Sounds\\Player\\WoundWater.wav",
 86 sound SOUND_WALK_GRASS_L    "SoundsMP\\Player\\WalkGrassL.wav",
 87 sound SOUND_WALK_GRASS_R    "SoundsMP\\Player\\WalkGrassR.wav",
 88 sound SOUND_WALK_WOOD_L     "SoundsMP\\Player\\WalkWoodL.wav",
 89 sound SOUND_WALK_WOOD_R     "SoundsMP\\Player\\WalkWoodR.wav",
 90 sound SOUND_WALK_SNOW_L     "SoundsMP\\Player\\WalkSnowL.wav",
 91 sound SOUND_WALK_SNOW_R     "SoundsMP\\Player\\WalkSnowR.wav",
 92 sound SOUND_BLOWUP          "SoundsMP\\Player\\BlowUp.wav",


150 sound SOUND_F_WATER_ENTER   "SoundsMP\\Player\\Female\\WaterEnter.wav",
151 sound SOUND_F_WATER_LEAVE   "SoundsMP\\Player\\Female\\WaterLeave.wav",
152 sound SOUND_F_WALK_L        "SoundsMP\\Player\\Female\\WalkL.wav",
153 sound SOUND_F_WALK_R        "SoundsMP\\Player\\Female\\WalkR.wav",
154 sound SOUND_F_SWIM_L        "SoundsMP\\Player\\Female\\SwimL.wav",
155 sound SOUND_F_SWIM_R        "SoundsMP\\Player\\Female\\SwimR.wav",
156 sound SOUND_F_DIVE_L        "SoundsMP\\Player\\Female\\Dive.wav",
157 sound SOUND_F_DIVE_R        "SoundsMP\\Player\\Female\\Dive.wav",
158 sound SOUND_F_DIVEIN        "SoundsMP\\Player\\Female\\DiveIn.wav",
159 sound SOUND_F_DIVEOUT       "SoundsMP\\Player\\Female\\DiveOut.wav",
160 sound SOUND_F_DROWN         "SoundsMP\\Player\\Female\\Drown.wav",
161 sound SOUND_F_INHALE0       "SoundsMP\\Player\\Female\\Inhale00.wav",
162 sound SOUND_F_JUMP          "SoundsMP\\Player\\Female\\Jump.wav",
163 sound SOUND_F_LAND          "SoundsMP\\Player\\Female\\Land.wav",
166 sound SOUND_F_DEATH         "SoundsMP\\Player\\Female\\Death.wav",
167 sound SOUND_F_DEATHWATER    "SoundsMP\\Player\\Female\\DeathWater.wav",
170 sound SOUND_F_WATERWALK_L   "SoundsMP\\Player\\Female\\WalkWaterL.wav",
171 sound SOUND_F_WATERWALK_R   "SoundsMP\\Player\\Female\\WalkWaterR.wav",
172 sound SOUND_F_INHALE1       "SoundsMP\\Player\\Female\\Inhale01.wav",
173 sound SOUND_F_INHALE2       "SoundsMP\\Player\\Female\\Inhale02.wav",
175 sound SOUND_F_WALK_SAND_L   "SoundsMP\\Player\\Female\\WalkSandL.wav",
176 sound SOUND_F_WALK_SAND_R   "SoundsMP\\Player\\Female\\WalkSandR.wav",
// 78 sound SOUND_F_HIGHSCORE     "SoundsMP\\Player\\Female\\HighScore.wav",
180 sound SOUND_F_WOUNDWEAK     "SoundsMP\\Player\\Female\\WoundWeak.wav",
181 sound SOUND_F_WOUNDMEDIUM   "SoundsMP\\Player\\Female\\WoundMedium.wav",
182 sound SOUND_F_WOUNDSTRONG   "SoundsMP\\Player\\Female\\WoundStrong.wav",
185 sound SOUND_F_WOUNDWATER    "SoundsMP\\Player\\Female\\WoundWater.wav",
186 sound SOUND_F_WALK_GRASS_L  "SoundsMP\\Player\\Female\\WalkGrassL.wav",
187 sound SOUND_F_WALK_GRASS_R  "SoundsMP\\Player\\Female\\WalkGrassR.wav",
188 sound SOUND_F_WALK_WOOD_L   "SoundsMP\\Player\\Female\\WalkWoodL.wav",
189 sound SOUND_F_WALK_WOOD_R   "SoundsMP\\Player\\Female\\WalkWoodR.wav",
190 sound SOUND_F_WALK_SNOW_L   "SoundsMP\\Player\\Female\\WalkSnowL.wav",
191 sound SOUND_F_WALK_SNOW_R   "SoundsMP\\Player\\Female\\WalkSnowR.wav",

// gender-independent sounds
200 sound SOUND_SILENCE         "Sounds\\Misc\\Silence.wav",
201 sound SOUND_SNIPER_ZOOM     "ModelsMP\\Weapons\\Sniper\\Sounds\\Zoom.wav",
206 sound SOUND_SNIPER_QZOOM    "ModelsMP\\Weapons\\Sniper\\Sounds\\QuickZoom.wav",
202 sound SOUND_INFO            "Sounds\\Player\\Info.wav",
203 sound SOUND_WATERAMBIENT    "Sounds\\Player\\Underwater.wav",
204 sound SOUND_WATERBUBBLES    "Sounds\\Player\\Bubbles.wav",
205 sound SOUND_POWERUP_BEEP    "SoundsMP\\Player\\PowerUpBeep.wav",

// ************** FLESH PARTS **************
210 model   MODEL_FLESH          "Models\\Effects\\Debris\\Flesh\\Flesh.mdl",
211 model   MODEL_FLESH_APPLE    "Models\\Effects\\Debris\\Fruits\\Apple.mdl",
212 model   MODEL_FLESH_BANANA   "Models\\Effects\\Debris\\Fruits\\Banana.mdl",
213 model   MODEL_FLESH_BURGER   "Models\\Effects\\Debris\\Fruits\\CheeseBurger.mdl",
214 model   MODEL_FLESH_LOLLY    "Models\\Effects\\Debris\\Fruits\\LollyPop.mdl",
215 model   MODEL_FLESH_ORANGE   "Models\\Effects\\Debris\\Fruits\\Orange.mdl",

220 texture TEXTURE_FLESH_RED    "Models\\Effects\\Debris\\Flesh\\FleshRed.tex",
221 texture TEXTURE_FLESH_GREEN  "Models\\Effects\\Debris\\Flesh\\FleshGreen.tex",
222 texture TEXTURE_FLESH_APPLE  "Models\\Effects\\Debris\\Fruits\\Apple.tex",
223 texture TEXTURE_FLESH_BANANA "Models\\Effects\\Debris\\Fruits\\Banana.tex",
224 texture TEXTURE_FLESH_BURGER "Models\\Effects\\Debris\\Fruits\\CheeseBurger.tex",
225 texture TEXTURE_FLESH_LOLLY  "Models\\Effects\\Debris\\Fruits\\LollyPop.tex",
226 texture TEXTURE_FLESH_ORANGE "Models\\Effects\\Debris\\Fruits\\Orange.tex",


functions:

  export virtual void OnControllerConnected(CPawnControllerEntity *pNewController)
  {
    CPawnEntity::OnControllerConnected(pNewController);
    
    CInfoF("Connected!\n");
  }

  //! Get name of this player.
  export CTString GetPlayerName(void)
  {
    CPlayerControllerEntity *penController = static_cast<CPlayerControllerEntity *>(GetController());
    
    return penController->GetPlayerName();
  }
  
  /* Get index of this player in the game. */
  export INDEX GetMyPlayerIndex(void)
  {
    CEntity *penMe = this;
    if (IsPredictor()) {
      penMe = GetPredicted();
    }
    for (INDEX iPlayer=0; iPlayer<GetMaxPlayers(); iPlayer++) {
      // if this is ME (this)
      if (GetPlayerEntity(iPlayer)==penMe) {
        return iPlayer;
      }
    }
    // must find my self
    return 15;  // if not found, still return a relatively logical value
  }

  // add a given amount of mana to the player
  void AddMana(INDEX iMana)
  {
    m_iMana += iMana;
    m_fPickedMana += iMana;
  }
  
  virtual FLOAT GetHealthFactor() const
  {
    FLOAT fHealthFactor = DOUBLE(GetHealth()) / DOUBLE(TopHealth());
    return fHealthFactor;
  }

  virtual INDEX GetVitality(VitalityType eType) const
  {
    switch (eType)
    {
      case VT_ARMOR: return m_iArmor;
      case VT_SHIELD: return m_iShield;
    }

    return CPawnEntity::GetVitality(eType);
  }

  virtual void SetVitality(VitalityType eType, INDEX iPoints)
  {
    switch (eType)
    {
      case VT_ARMOR:
        m_iArmor = iPoints;
        break;

      case VT_SHIELD:
        m_iShield = iPoints;
        break;
    }

    CPawnEntity::SetVitality(eType, iPoints);
  }

  INDEX GenderSound(INDEX iSound)
  {
    return iSound+m_iGender*GENDEROFFSET;
  }

  void AddBouble(FLOAT3D vPos, FLOAT3D vSpeedRelative)
  {
    ShellLaunchData &sld = m_asldData[m_iFirstEmptySLD];
    sld.sld_vPos = vPos;

    const FLOATmatrix3D &m = GetRotationMatrix();
    FLOAT3D vUp(m(1,2), m(2,2), m(3,2));
    sld.sld_vUp = vUp;
    sld.sld_vSpeed = vSpeedRelative*m;
    sld.sld_tmLaunch = _pTimer->CurrentTick();
    sld.sld_estType = ESL_BUBBLE;

    // move to next shell position
    m_iFirstEmptySLD = (m_iFirstEmptySLD+1) % MAX_FLYING_SHELLS;
  }

  void ClearShellLaunchData(void)
  {
    // clear flying shells data array
    m_iFirstEmptySLD = 0;

    for (INDEX iShell=0; iShell<MAX_FLYING_SHELLS; iShell++)
    {
      m_asldData[iShell].sld_tmLaunch = -100.0f;
    }
  }

  void AddBulletSpray(FLOAT3D vPos, EffectParticlesType eptType, FLOAT3D vStretch)
  {
    BulletSprayLaunchData &bsld = m_absldData[m_iFirstEmptyBSLD];
    bsld.bsld_vPos = vPos;
    bsld.bsld_vG = en_vGravityDir;
    bsld.bsld_eptType=eptType;
    bsld.bsld_iRndBase=FRnd()*123456;
    bsld.bsld_tmLaunch = _pTimer->CurrentTick();
    bsld.bsld_vStretch=vStretch;

    // move to bullet spray position
    m_iFirstEmptyBSLD = (m_iFirstEmptyBSLD+1) % MAX_BULLET_SPRAYS;
  }

  void ClearBulletSprayLaunchData(void)
  {
    m_iFirstEmptyBSLD = 0;

    for (INDEX iBulletSpray=0; iBulletSpray<MAX_BULLET_SPRAYS; iBulletSpray++)
    {
      m_absldData[iBulletSpray].bsld_tmLaunch = -100.0f;
    }
  }

  void AddGoreSpray(FLOAT3D vPos, FLOAT3D v3rdPos, SprayParticlesType sptType, FLOAT3D vSpilDirection,
    FLOATaabbox3D boxHitted, FLOAT fDamagePower, COLOR colParticles)
  {
    GoreSprayLaunchData &gsld = m_agsldData[m_iFirstEmptyGSLD];
    gsld.gsld_vPos = vPos;
    gsld.gsld_v3rdPos = v3rdPos;
    gsld.gsld_vG = en_vGravityDir;
    gsld.gsld_fGA = en_fGravityA;
    gsld.gsld_sptType = sptType;
    gsld.gsld_boxHitted = boxHitted;
    gsld.gsld_vSpilDirection = vSpilDirection;
    gsld.gsld_fDamagePower=fDamagePower;
    gsld.gsld_tmLaunch = _pTimer->CurrentTick();
    gsld.gsld_colParticles = colParticles;

    // move to bullet spray position
    m_iFirstEmptyGSLD = (m_iFirstEmptyGSLD+1) % MAX_GORE_SPRAYS;
  }

  void ClearGoreSprayLaunchData(void)
  {
    m_iFirstEmptyGSLD = 0;

    for (INDEX iGoreSpray=0; iGoreSpray<MAX_GORE_SPRAYS; iGoreSpray++)
    {
      m_agsldData[iGoreSpray].gsld_tmLaunch = -100.0f;
    }
  }

  void CPlayerPawnEntity(void)
  {
    // clear flying shells data array
    bAimButtonHeld = FALSE;

    ClearShellLaunchData();
    ClearBulletSprayLaunchData();
    ClearGoreSprayLaunchData();
    m_tmPredict = 0;

    // add all messages from First Encounter
    //CheatAllMessagesDir("Data\\Messages\\weapons\\", CMF_READ);
    //CheatAllMessagesDir("Data\\Messages\\enemies\\", CMF_READ);
    // ... or not
  }

  class CPlayerInventoryEntity *GetInventory(void)
  {
    ASSERT(m_penInventory != NULL);
    return static_cast<CPlayerInventoryEntity *>(&*m_penInventory);
  }
  
  class CPlayerWeaponEntity *GetWeapons(enum PlayerHand eHand)
  {
    if (eHand == E_HAND_MAIN) {
      return static_cast<CPlayerWeaponEntity *>(&*m_penWeaponsFirst);
    }

    return static_cast<CPlayerWeaponEntity *>(&*m_penWeaponsSecond);
  }

  class CPlayerViewEntity *GetView()
  {
  	ASSERT(m_penView != NULL);
    return static_cast<CPlayerViewEntity *>(&*m_penView);
  }
  
  class CPlayerViewEntity *GetView3rd()
  {
  	ASSERT(m_pen3rdPersonView != NULL);
    return static_cast<CPlayerViewEntity *>(&*m_pen3rdPersonView);
  }

  void UpdateWeaponTargetting()
  {
    if (GetWeapons(E_HAND_MAIN) != NULL) {
      GetWeapons(E_HAND_MAIN)->UpdateTargetingInfo();
    }
    
    if (GetWeapons(E_HAND_SECOND) != NULL) {
      GetWeapons(E_HAND_SECOND)->UpdateTargetingInfo();
    }
  }
  
  void EnableWeaponZoom()
  {
    CPlayerWeaponEntity *penWeapon = GetWeapons(E_HAND_MAIN);
    penWeapon->m_bSniping = TRUE;
    m_ulFlags |= PLF_ISZOOMING;
    penWeapon->m_fSniperFOVlast = penWeapon->m_fSniperFOV = penWeapon->m_fMinimumZoomFOV;
  }
  
  void ResetWeaponZoom()
  {
    CPlayerWeaponEntity *penWeapon = GetWeapons(E_HAND_MAIN);
    m_ulFlags &= ~PLF_ISZOOMING;
    penWeapon->m_bSniping = FALSE;
    penWeapon->m_fSniperFOVlast = penWeapon->m_fSniperFOV = penWeapon->m_fSniperMaxFOV;
  }
  
  CSoundObject &GetWeaponSound(enum PlayerHand eHand, enum WeaponSound eSound)
  {
    if (eHand == E_HAND_MAIN) {
      switch (eSound)
      {
        case E_WEAPON_SOUND_0: return m_soWeapon0;
        case E_WEAPON_SOUND_1: return m_soWeapon1;
        case E_WEAPON_SOUND_2: return m_soWeapon2;
        case E_WEAPON_SOUND_3: return m_soWeapon3;
        case E_WEAPON_SOUND_AMBIENT: return m_soWeaponAmbient;
      }
    }

    switch (eSound)
    {
      case E_WEAPON_SOUND_0: return m_soSecondWeapon0;
      case E_WEAPON_SOUND_1: return m_soSecondWeapon1;
      case E_WEAPON_SOUND_2: return m_soSecondWeapon2;
      case E_WEAPON_SOUND_3: return m_soSecondWeapon3;
    }
      
    return m_soSecondWeaponAmbient;
  }

  void SendToWeapon(enum PlayerHand eHand, const CEntityEvent &ee)
  {
    const BOOL bDoFirst = eHand == E_HAND_MAIN || eHand == E_HAND_BOTH;
    const BOOL bDoSecond = eHand == E_HAND_SECOND || eHand == E_HAND_BOTH;
    
    if (bDoFirst && GetWeapons(E_HAND_MAIN) != NULL) {
      GetWeapons(E_HAND_MAIN)->SendEvent(ee);
    }
    
    if (bDoSecond && GetWeapons(E_HAND_SECOND) != NULL) {
      GetWeapons(E_HAND_SECOND)->SendEvent(ee);
    }
  }

  void FireWeapon(enum PlayerHand eHand)
  {
    SendToWeapon(eHand, EFireWeapon());
  }
  
  void ReleaseWeapon(enum PlayerHand eHand)
  {
    SendToWeapon(eHand, EReleaseWeapon());
  }
  
  void ReloadWeapon(enum PlayerHand eHand)
  {
    SendToWeapon(eHand, EReloadWeapon());
  }
  
  void StartWeapons()
  {
    SendToWeapon(E_HAND_BOTH, EStart());
  }

  void StopWeapons()
  {
    SendToWeapon(E_HAND_BOTH, EStop());
  }
  
  void InitializeWeapons()
  {
    if (GetWeapons(E_HAND_MAIN) != NULL) {
      GetWeapons(E_HAND_MAIN)->InitializeWeapons();
    }
    
    if (GetWeapons(E_HAND_SECOND) != NULL) {
      GetWeapons(E_HAND_SECOND)->InitializeWeapons();
    }
  }
  
  void SelectWeapon(enum PlayerHand eHand, INDEX iWeapon, BOOL bDirect)
  {
    BOOL bDoFirst = eHand == E_HAND_MAIN || eHand == E_HAND_BOTH;
    BOOL bDoSecond = eHand == E_HAND_SECOND || eHand == E_HAND_BOTH;

    ESelectWeapon eSelect;
    eSelect.iWeapon = iWeapon;
    
    if (bDirect) {
      eSelect.iWeapon |= SELECT_WEAPON_DIRECT;
    }

    SendToWeapon(eHand, eSelect);
  }
  
  void ToggleDualWield()
  {
    if (GetWeapons(E_HAND_SECOND) == NULL) {
      return;
    }
    
    WeaponIndex eAtMainHand = GetWeapons(E_HAND_MAIN)->GetCurrentWeapon();
    
    if (GetWeapons(E_HAND_SECOND)->GetCurrentWeapon() != E_WEAPON_NONE) {
      SelectWeapon(E_HAND_SECOND, 0, FALSE);
    } else {
      if (!GetWeaponParams(eAtMainHand)->IsSolo()) {
        SelectWeapon(E_HAND_SECOND, -5, FALSE);
      }
    }
  }
  
  BOOL IsDualWielding()
  {
    CPlayerWeaponEntity *pWeapons = GetWeapons(E_HAND_SECOND);
    
    if (pWeapons == NULL) {
      return FALSE;
    }
    
    return pWeapons->GetCurrentWeapon() != E_WEAPON_NONE;
  }
  
  void CreateComponents()
  {
    // [SSE] Inventory
    m_penInventory = CreateEntity(GetPlacement(), CLASS_PLAYER_INVENTORY);
    EInventoryInit eInitInventory;
    eInitInventory.penOwner = this;
    m_penInventory->Initialize(eInitInventory);

    // spawn weapons
    m_penWeaponsFirst = CreateEntity(GetPlacement(), CLASS_PLAYER_WEAPONS);
    m_penWeaponsSecond = CreateEntity(GetPlacement(), CLASS_PLAYER_WEAPONS);
    EWeaponsInit eInitWeapons;
    eInitWeapons.penOwner = this;
    m_penWeaponsFirst->Initialize(eInitWeapons);

    if (m_penWeaponsSecond != NULL) {
      m_penWeaponsSecond->Initialize(eInitWeapons);
      GetWeapons(E_HAND_SECOND)->m_eHand = E_HAND_SECOND;
    }

    // spawn animator
    m_penAnimator = CreateEntity(GetPlacement(), CLASS_PLAYER_ANIMATOR);
    EAnimatorInit eInitAnimator;
    eInitAnimator.penPlayer = this;
    m_penAnimator->Initialize(eInitAnimator);
  }

  void DestroyComponents()
  {
    if (m_penInventory != NULL) {
      m_penInventory->Destroy();
    }

    if (m_penWeaponsFirst != NULL) {
      m_penWeaponsFirst->Destroy();
    }
    
    if (m_penWeaponsSecond != NULL) {
      m_penWeaponsSecond->Destroy();
    }
    
    if (m_penAnimator != NULL) {
      m_penAnimator->Destroy();
    }
    
    if (m_penView != NULL) {
      m_penView->Destroy();
    }

    if (m_pen3rdPersonView != NULL) {
      m_pen3rdPersonView->Destroy();
    }
  }

  class CPlayerAnimatorEntity *GetPlayerAnimator(void)
  {
    ASSERT(m_penAnimator != NULL);
    return static_cast<CPlayerAnimatorEntity *>(&*m_penAnimator);
  }
  
  class CGameInfoEntity *GetGameInfo()
  {
    return static_cast<CGameInfoEntity *>(&*m_penGameInfo);
  }
  
  class CMusicHolderEntity *GetMusicHolder()
  {
    return static_cast<CMusicHolderEntity *>(&*m_penMainMusicHolder);
  }

  CPlayerSettings *GetSettings(void)
  {
    if (GetController() == NULL) {
      return NULL;
    }
    
    CMyPlayerControllerEntity *penController = static_cast<CMyPlayerControllerEntity *>(GetController());
    return penController->GetSettings();
  }
  
  CPlayerCharacter *GetCharacter(void)
  {
    if (GetController() == NULL) {
      return NULL;
    }
    
    CPlayerControllerEntity *penController = static_cast<CPlayerControllerEntity *>(GetController());
    return &penController->en_pcCharacter;
  }
  
  FLOAT GetLatency() const
  {
    if (en_penController == NULL) {
      return 0;
    }
    
    CMyPlayerControllerEntity *penController = static_cast<CMyPlayerControllerEntity *>(&*en_penController);
    return penController->m_tmLatency;
  }

  export void Copy(CEntity &enOther, ULONG ulFlags)
  {
    CPawnEntity::Copy(enOther, ulFlags);
    CPlayerPawnEntity *penOther = (CPlayerPawnEntity *)(&enOther);
    m_moRender.Copy(penOther->m_moRender);

    // if creating predictor
    if (ulFlags&COPY_PREDICTOR)
    {
      // copy positions of launched empty shells
      memcpy(m_asldData, penOther->m_asldData, sizeof(m_asldData));
      m_iFirstEmptySLD = penOther->m_iFirstEmptySLD;
      // all messages in the inbox
      m_acmiMessages.Clear();
      m_ctUnreadMessages = 0;
      //m_lsLightSource;
      SetupLightSource(); //? is this ok !!!!

    // if normal copying
    } else {
      // copy messages
      m_acmiMessages = penOther->m_acmiMessages;
      m_ctUnreadMessages = penOther->m_ctUnreadMessages;
    }
  }

  // check character data for invalid values
  void ValidateCharacter(void)
  {
    // if in single player or flyover
    if (GetSP()->sp_bSinglePlayer) {
      // always use default model
      CPlayerSettings *pps = GetSettings();
      memset(pps->ps_achModelFile, 0, sizeof(pps->ps_achModelFile));
    }
  }

  // parse gender from your name
  void ParseGender(CTString &strName)
  {
    if (strName.RemovePrefix("#male#")) {
      m_iGender = GENDER_MALE;
    } else if (strName.RemovePrefix("#female#")) {
      m_iGender = GENDER_FEMALE;
    } else {
      m_iGender = GENDER_MALE;
    }
  }

  void CheckHighScore(void)
  {
    // if not playing a demo
    if (!_pNetwork->IsPlayingDemo()) {
      // update our local high score with the external
      if (plr_iHiScore>m_iHighScore) {
        m_iHighScore = plr_iHiScore;
      }
    }

    // if current score is better than highscore
    if (m_psGameStats.ps_iScore>m_iHighScore) {
      // if it is a highscore greater than the last one beaten
      if (m_iHighScore>m_iBeatenHighScore) {
        // remember that it was beaten
        m_iBeatenHighScore = m_iHighScore;
        // tell that to player
        m_soHighScore.Set3DParameters(25.0f, 5.0f, 1.0f, 1.0f);
        //PlaySound(m_soHighScore, SOUND_HIGHSCORE, 0); !!!!####!!!!
      }
    }
  }

  CTString GetPredictName(void) const
  {
    if (IsPredicted()) {
      return "PREDICTED";
    } else if (IsPredictor()) {
      return "predictor";
    } else if (GetFlags()&ENF_WILLBEPREDICTED) {
      return "WILLBEPREDICTED";
    } else {
      return "no prediction";
    }
  }

  /* Write to stream. */
  void Write_t(CTStream *ostr) // throw char *
  {
    CPawnEntity::Write_t(ostr);
    
    (*ostr) << en_plViewpoint;
  }

  /* Read from stream. */
  void Read_t(CTStream *istr) // throw char *
  {
    CPawnEntity::Read_t(istr);
    
    (*istr) >> en_plViewpoint;
    en_plLastViewpoint = en_plViewpoint;

    // clear flying shells data array
    ClearShellLaunchData();
    ClearBulletSprayLaunchData();
    ClearGoreSprayLaunchData();

    // set your real appearance if possible
    ValidateCharacter();
    CTString strDummy;
    SetPlayerAppearance(&m_moRender, GetCharacter(), strDummy, /*bPreview=*/FALSE);
    ParseGender(strDummy);
    m_ulFlags |= PLF_SYNCWEAPON;

    // setup light source
    SetupLightSource();
  };
  
  virtual void ReadCustomData_t(CTStream &istrm, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();
    
    if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdLevelStats)) {
      istrm.Read_t(&m_psLevelStats, sizeof(m_psLevelStats));

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdLevelTotalStats)) {
      istrm.Read_t(&m_psLevelTotal, sizeof(m_psLevelTotal));

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdGameStats)) {
      istrm.Read_t(&m_psGameStats, sizeof(m_psGameStats));

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdGameTotalStats)) {
      istrm.Read_t(&m_psGameTotal, sizeof(m_psGameTotal));
      
    // load array of messages
    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdMessages)) {
      istrm.ExpectID_t("MSGS");
      INDEX ctMsg;
      istrm >> ctMsg;
      m_acmiMessages.Clear();
      m_ctUnreadMessages = 0;

      if (ctMsg > 0) {
        m_acmiMessages.Push(ctMsg);

        for (INDEX iMsg = 0; iMsg < ctMsg; iMsg++)
        {
          m_acmiMessages[iMsg].Read_t(istrm);

          if (!m_acmiMessages[iMsg].cmi_bRead) {
            m_ctUnreadMessages++;
          }
        }
      }
    }
  };

  virtual void WriteCustomData_t(CTStream &ostrm, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();

    if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdLevelStats)) {
      ostrm.Write_t(&m_psLevelStats, sizeof(m_psLevelStats));

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdLevelTotalStats)) {
      ostrm.Write_t(&m_psLevelTotal, sizeof(m_psLevelTotal));

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdGameStats)) {
      ostrm.Write_t(&m_psGameStats, sizeof(m_psGameStats));

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdGameTotalStats)) {
      ostrm.Write_t(&m_psGameTotal, sizeof(m_psGameTotal));

    // save array of messages
    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdMessages)) {
      ostrm.WriteID_t("MSGS");
      INDEX ctMsg = m_acmiMessages.Count();
      ostrm << ctMsg;

      for (INDEX iMsg = 0; iMsg < ctMsg; iMsg++)
      {
        m_acmiMessages[iMsg].Write_t(ostrm);
      }
    }
  };

  virtual void CopyCustomData(CEntity &enOther, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();
    CPlayerPawnEntity *penOther = static_cast<CPlayerPawnEntity *>(&enOther);
    
    if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdLevelStats)) {
      m_psLevelStats = penOther->m_psLevelStats;

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdLevelTotalStats)) {
      m_psLevelTotal = penOther->m_psLevelTotal;

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdGameStats)) {
      m_psGameStats = penOther->m_psGameStats;

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdGameTotalStats)) {
      m_psGameTotal = penOther->m_psGameTotal;

    } else if (slPropertyOffset == offsetof(CPlayerPawnEntity, m_cdMessages)) {
      // NOTE: It has very specific code at ::Copy.
    }
  }

  /* Get static light source information. */
  CLightSource *GetLightSource(void)
  {
    if (!IsPredictor()) {
      return &m_lsLightSource;
    } else {
      return NULL;
    }
  };

  // called by other entities to set time prediction parameter
  void SetPredictionTime(TIME tmAdvance)   // give time interval in advance to set
  {
    m_tmPredict = _pTimer->CurrentTick()+tmAdvance;
  }

  // called by engine to get the upper time limit
  TIME GetPredictionTime(void)   // return moment in time up to which to predict this entity
  {
    return m_tmPredict;
  }

  // get maximum allowed range for predicting this entity
  FLOAT GetPredictionRange(void)
  {
    return cli_fPredictPlayersRange;
  }

  // add to prediction any entities that this entity depends on
  void AddDependentsToPrediction(void)
  {
    m_penInventory->AddToPrediction(); // [SSE] Player Inventory Entity
    m_penAnimator->AddToPrediction();
    m_penView->AddToPrediction();
    m_pen3rdPersonView->AddToPrediction();

    if (m_penWeaponsFirst != NULL) {
      m_penWeaponsFirst->AddToPrediction();
    }

    if (m_penWeaponsSecond != NULL) {
      m_penWeaponsSecond->AddToPrediction();
    }
  }

  // get in-game time for statistics
  TIME GetStatsInGameTimeLevel(void)
  {
    if (m_bEndOfLevel) {
      return m_psLevelStats.ps_tmTime;
    } else {
      return _pNetwork->GetGameTime()-m_tmLevelStarted;
    }
  }

  TIME GetStatsInGameTimeGame(void)
  {
    if (m_bEndOfLevel) {
      return m_psGameStats.ps_tmTime;
    } else {
      return m_psGameStats.ps_tmTime + (_pNetwork->GetGameTime()-m_tmLevelStarted);
    }
  }

  FLOAT GetStatsRealWorldTime(void)
  {
    time_t timeNow;
    if (m_bEndOfLevel) {
      timeNow = m_iEndTime;
    } else {
      time(&timeNow);
    }

    return (FLOAT)difftime(timeNow, m_iStartTime);
  }

  CTString GetStatsRealWorldStarted(void)
  {
    struct tm *newtime;
    newtime = localtime((const time_t*)&m_iStartTime);

    setlocale(LC_ALL, "");
    CTString strTimeline;
    char achTimeLine[256];
    strftime(achTimeLine, sizeof(achTimeLine)-1, "%a %x %H:%M", newtime);
    strTimeline = achTimeLine;
    setlocale(LC_ALL, "C");
    return strTimeline;
  }

  // fill in player statistics
  export void GetStats(CTString &strStats, const CompStatType csType, INDEX ctCharsPerRow)
  {
    // get proper type of stats
    if (csType==CST_SHORT) {
      GetShortStats(strStats);
    } else {
      ASSERT(csType==CST_DETAIL);

      strStats = "\n";
      _ctAlignWidth = Min(ctCharsPerRow, INDEX(60));

      if (GetSP()->sp_bCooperative) {
        if (GetSP()->sp_bSinglePlayer) {
          GetDetailStatsSP(strStats, 0);
        } else {
          GetDetailStatsCoop(strStats);
        }
      } else {
        GetDetailStatsDM(strStats);
      }
    }
  }

  // get short one-line statistics - used for savegame descriptions and similar
  void GetShortStats(CTString &strStats)
  {
    strStats.PrintF(TRANS("%s %s Score: %d Kills: %d/%d"),
                     GetDifficultyString(), TimeToString(GetStatsInGameTimeLevel()),
                     m_psLevelStats.ps_iScore, m_psLevelStats.ps_iKills, m_psLevelTotal.ps_iKills);
  }

  // get detailed statistics for deathmatch game
  void GetDetailStatsDM(CTString &strStats)
  {
    extern INDEX SetAllPlayersStats(INDEX iSortKey);
    extern CPlayerPawnEntity *_apenPlayers[NET_MAXGAMEPLAYERS];
    // determine type of game
    const BOOL bFragMatch = GetSP()->sp_bUseFrags;

    // fill players table
    const INDEX ctPlayers = SetAllPlayersStats(bFragMatch?5:3); // sort by frags or by score

    // get time elapsed since the game start
    strStats+=AlignString(CTString(0, "^cFFFFFF%s:^r\n%s", TRANS("TIME"), TimeToString(_pNetwork->GetGameTime())));
    strStats+="\n";

    // find maximum frags/score that one player has
    INDEX iMaxFrags = LowerLimit(INDEX(0));
    INDEX iMaxScore = LowerLimit(INDEX(0));
    {for (INDEX iPlayer=0; iPlayer<ctPlayers; iPlayer++) {
      CPlayerPawnEntity *penPlayer = _apenPlayers[iPlayer];
      iMaxFrags = Max(iMaxFrags, penPlayer->m_psLevelStats.ps_iKills);
      iMaxScore = Max(iMaxScore, penPlayer->m_psLevelStats.ps_iScore);
    }}

    // print game limits
    const CSessionProperties &sp = *GetSP();

    if (sp.sp_iTimeLimit>0) {
      FLOAT fTimeLeft = ClampDn(sp.sp_iTimeLimit*60.0f - _pNetwork->GetGameTime(), 0.0f);
      strStats+=AlignString(CTString(0, "^cFFFFFF%s:^r\n%s", TRANS("TIME LEFT"), TimeToString(fTimeLeft)));
      strStats+="\n";
    }

    if (bFragMatch && sp.sp_iFragLimit>0) {
      INDEX iFragsLeft = ClampDn(sp.sp_iFragLimit-iMaxFrags, INDEX(0));
      strStats+=AlignString(CTString(0, "^cFFFFFF%s:^r\n%d", TRANS("FRAGS LEFT"), iFragsLeft));
      strStats+="\n";
    }

    if (!bFragMatch && sp.sp_iScoreLimit>0) {
      INDEX iScoreLeft = ClampDn(sp.sp_iScoreLimit-iMaxScore, INDEX(0));
      strStats+=AlignString(CTString(0, "^cFFFFFF%s:^r\n%d", TRANS("SCORE LEFT"), iScoreLeft));
      strStats+="\n";
    }

    strStats += "\n";

    CTString strRank = TRANS("NO.");
    CTString strFrag = bFragMatch ? TRANS("FRAGS"):TRANS("SCORE");
    CTString strPing = TRANS("PING");
    CTString strName = TRANS("PLAYER");
    ULONG ctRankChars = Max(strRank.Length(), ULONG(3)) ;
    ULONG ctFragChars = Max(strFrag.Length(), ULONG(7)) ;
    ULONG ctPingChars = Max(strPing.Length(), ULONG(5)) ;
    ULONG ctNameChars = Max(strName.Length(), ULONG(20));

    // header
    strStats += "^cFFFFFF";
    strStats += PadStringRight(strRank, ctRankChars)+" ";
    strStats += PadStringLeft (strFrag, ctFragChars)+" ";
    strStats += PadStringLeft (strPing, ctPingChars)+" ";
    strStats += PadStringRight(strName, ctNameChars)+" ";
    strStats += "^r";
    strStats += "\n\n";

    {for (INDEX iPlayer=0; iPlayer<ctPlayers; iPlayer++) {
      CTString strLine;
      CPlayerPawnEntity *penPlayer = _apenPlayers[iPlayer];
      INDEX iPing = ceil(penPlayer->GetController()->GetPing()*1000.0f);
      INDEX iScore = bFragMatch ? penPlayer->m_psLevelStats.ps_iKills : penPlayer->m_psLevelStats.ps_iScore;
      CTString strName = penPlayer->GetPlayerName();

      strStats += PadStringRight(CTString(0, "%d", iPlayer+1), ctRankChars)+" ";
      strStats += PadStringLeft (CTString(0, "%d", iScore),    ctFragChars)+" ";
      strStats += PadStringLeft (CTString(0, "%d", iPing),     ctPingChars)+" ";
      strStats += PadStringRight(strName,                      ctNameChars)+" ";
      strStats += "\n";
    }}
  }

  // get singleplayer statistics
  void GetDetailStatsCoop(CTString &strStats)
  {
    // first put in your full stats
    strStats += "^b"+CenterString(TRANS("YOUR STATS"))+"^r\n";
    strStats+="\n";
    GetDetailStatsSP(strStats, 1);

    // get stats from all players
    extern INDEX SetAllPlayersStats(INDEX iSortKey);
    extern CPlayerPawnEntity *_apenPlayers[NET_MAXGAMEPLAYERS];
    const INDEX ctPlayers = SetAllPlayersStats(3); // sort by score

    // for each player
    PlayerStats psSquadLevel = PlayerStats();
    PlayerStats psSquadGame  = PlayerStats();

    {for (INDEX iPlayer=0; iPlayer<ctPlayers; iPlayer++) {
      CPlayerPawnEntity *penPlayer = _apenPlayers[iPlayer];
      // add values to squad stats
      ASSERT(penPlayer!=NULL);
      PlayerStats psLevel = penPlayer->m_psLevelStats;
      PlayerStats psGame  = penPlayer->m_psGameStats ;
      psSquadLevel.ps_iScore   += psLevel.ps_iScore   ;
      psSquadLevel.ps_iKills   += psLevel.ps_iKills   ;
      psSquadLevel.ps_iDeaths  += psLevel.ps_iDeaths  ;
      psSquadLevel.ps_iSecrets += psLevel.ps_iSecrets ;
      psSquadGame.ps_iScore    += psGame.ps_iScore   ;
      psSquadGame.ps_iKills    += psGame.ps_iKills   ;
      psSquadGame.ps_iDeaths   += psGame.ps_iDeaths  ;
      psSquadGame.ps_iSecrets  += psGame.ps_iSecrets ;
    }}

    // add squad stats
    strStats+="\n";
    strStats += "^b"+CenterString(TRANS("SQUAD TOTAL"))+"^r\n";
    strStats+="\n";
    strStats+=CTString(0, "^cFFFFFF%s^r", TranslateConst(en_pwoWorld->GetName(), 0));
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d", TRANS("SCORE"), psSquadLevel.ps_iScore));
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d", TRANS("DEATHS"), psSquadLevel.ps_iDeaths));
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d/%d", TRANS("KILLS"), psSquadLevel.ps_iKills, m_psLevelTotal.ps_iKills));
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d/%d", TRANS("SECRETS"), psSquadLevel.ps_iSecrets, m_psLevelTotal.ps_iSecrets));
    strStats+="\n";
    strStats+="\n";
    strStats+=CTString("^cFFFFFF")+TRANS("TOTAL")+"^r\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d", TRANS("SCORE"), psSquadGame.ps_iScore));
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d", TRANS("DEATHS"), psSquadGame.ps_iDeaths));
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d/%d", TRANS("KILLS"), psSquadGame.ps_iKills, m_psGameTotal.ps_iKills));
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d/%d", TRANS("SECRETS"), psSquadGame.ps_iSecrets, m_psGameTotal.ps_iSecrets));
    strStats+="\n";
    strStats+="\n";


    strStats+="\n";
    strStats += "^b"+CenterString(TRANS("OTHER PLAYERS"))+"^r\n";
    strStats+="\n";

    // for each player
    {for (INDEX iPlayer=0; iPlayer<ctPlayers; iPlayer++) {
      CPlayerPawnEntity *penPlayer = _apenPlayers[iPlayer];
      // if this one
      if (penPlayer==this) {
        // skip it
        continue;
      }
      // add his stats short
      strStats+="^cFFFFFF"+CenterString(penPlayer->GetPlayerName())+"^r\n\n";
      penPlayer->GetDetailStatsSP(strStats, 2);
      strStats+="\n";
    }}
  }

  // get singleplayer statistics
  void GetDetailStatsSP(CTString &strStats, INDEX iCoopType)
  {
    if (iCoopType<=1) {
      if (m_bEndOfGame) {
        if (GetSP()->sp_gdGameDifficulty==CSessionProperties::GD_EXTREME) {
          strStats+=TRANS("^f4SERIOUS GAME FINISHED,\nMENTAL MODE IS NOW ENABLED!^F\n\n");
        } else if (GetSP()->sp_bMental) {
          strStats+=TRANS("^f4YOU HAVE MASTERED THE GAME!^F\n\n");
        }
      }
    }

    if (iCoopType<=1) {
      // report total score info
      strStats+=AlignString(CTString(0, "^cFFFFFF%s:^r\n%d", TRANS("TOTAL SCORE"), m_psGameStats.ps_iScore));
      strStats+="\n";
      strStats+=AlignString(CTString(0, "^cFFFFFF%s:^r\n%s", TRANS("DIFFICULTY"), GetDifficultyString()));
      strStats+="\n";
      strStats+=AlignString(CTString(0, "^cFFFFFF%s:^r\n%s", TRANS("STARTED"), GetStatsRealWorldStarted()));
      strStats+="\n";
      strStats+=AlignString(CTString(0, "^cFFFFFF%s:^r\n%s", TRANS("PLAYING TIME"), TimeToString(GetStatsRealWorldTime())));
      strStats+="\n";

      if (m_psGameStats.ps_iScore<=plr_iHiScore) {
        strStats+=AlignString(CTString(0, "^cFFFFFF%s:^r\n%d", TRANS("HI-SCORE"), plr_iHiScore));
      } else {
        strStats+=TRANS("YOU BEAT THE HI-SCORE!");
      }

      strStats+="\n\n";
    }

    // report this level statistics
    strStats+=CTString(0, "^cFFFFFF%s^r", TranslateConst(en_pwoWorld->GetName(), 0));
    strStats+="\n";
    if (iCoopType<=1) {
      if (m_bEndOfLevel) {
        strStats+=AlignString(CTString(0, "  %s:\n%s", TRANS("ESTIMATED TIME"), TimeToString(m_tmEstTime)));
        strStats+="\n";
        strStats+=AlignString(CTString(0, "  %s:\n%d", TRANS("TIME BONUS"), m_iTimeScore));
        strStats+="\n";
        strStats+="\n";
      }
//    } else {
//      strStats+=CTString("^cFFFFFF")+TRANS("THIS LEVEL")+"^r\n";
    }
    strStats+=AlignString(CTString(0, "  %s:\n%d", TRANS("SCORE"), m_psLevelStats.ps_iScore));
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d/%d", TRANS("KILLS"), m_psLevelStats.ps_iKills, m_psLevelTotal.ps_iKills));
    strStats+="\n";
    if (iCoopType>=1) {
      strStats+=AlignString(CTString(0, "  %s:\n%d", TRANS("DEATHS"), m_psLevelStats.ps_iDeaths, m_psLevelTotal.ps_iDeaths));
      strStats+="\n";
    }
    strStats+=AlignString(CTString(0, "  %s:\n%d/%d", TRANS("SECRETS"), m_psLevelStats.ps_iSecrets, m_psLevelTotal.ps_iSecrets));
    strStats+="\n";
    if (iCoopType<=1) {
      strStats+=AlignString(CTString(0, "  %s:\n%s", TRANS("TIME"), TimeToString(GetStatsInGameTimeLevel())));
      strStats+="\n";
    }
    strStats+="\n";

    // report total game statistics
    strStats+=CTString("^cFFFFFF")+TRANS("TOTAL")+"^r";
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d", TRANS("SCORE"), m_psGameStats.ps_iScore));
    strStats+="\n";
    strStats+=AlignString(CTString(0, "  %s:\n%d/%d", TRANS("KILLS"), m_psGameStats.ps_iKills, m_psGameTotal.ps_iKills));
    strStats+="\n";
    if (iCoopType>=1) {
      strStats+=AlignString(CTString(0, "  %s:\n%d", TRANS("DEATHS"), m_psGameStats.ps_iDeaths, m_psGameTotal.ps_iDeaths));
      strStats+="\n";
    }
    strStats+=AlignString(CTString(0, "  %s:\n%d/%d", TRANS("SECRETS"), m_psGameStats.ps_iSecrets, m_psGameTotal.ps_iSecrets));
    strStats+="\n";
    if (iCoopType<=1) {
      strStats+=AlignString(CTString(0, "  %s:\n%s", TRANS("GAME TIME"), TimeToString(GetStatsInGameTimeGame())));
      strStats+="\n";
    }
    strStats+="\n";

    // set per level outputs
    if (iCoopType<1) {
      if (m_strLevelStats!="") {
        strStats += CTString("^cFFFFFF")+TRANS("Per level statistics") +"^r\n\n" + m_strLevelStats;
      }
    }
  }

  // provide info for GameAgent enumeration
  void GetGameAgentPlayerInfo(INDEX iPlayer, CTString &strOut)
  {
    CTString strPlayerName = GetPlayerName();
    INDEX iLen = strlen(strPlayerName);

    for (INDEX i=0; i<iLen; i++)
    {
      if (strPlayerName[i] == '\r' || strPlayerName[i] == '\n') {
        // newline in name!
        strPlayerName = "\x11";
        break;
      } else if (strPlayerName[i] < 32) {
        // invalid character in name!
        strPlayerName = "\x12";
        break;
      }
    }

    CTString strKey;
    strKey.PrintF("player_%d\x02%s\x03", iPlayer, (const char*)strPlayerName);
    strOut+=strKey;

    if (GetSP()->sp_bUseFrags) {
      strKey.PrintF("frags_%d\x02%d\x03", iPlayer, m_psLevelStats.ps_iKills);
      strOut+=strKey;
    } else {
      strKey.PrintF("frags_%d\x02%d\x03", iPlayer, m_psLevelStats.ps_iScore);
      strOut+=strKey;
    }

    strKey.PrintF("ping_%d\x02%d\x03", iPlayer, INDEX(ceil(GetController()->GetPing()*1000.0f)));
    strOut+=strKey;
  };

  // provide info for MSLegacy enumeration
  void GetMSLegacyPlayerInf(INDEX iPlayer, CTString &strOut)
  {
    CTString strKey;
    strKey.PrintF("\\player_%d\\%s", iPlayer, (const char*)GetPlayerName());
	  strOut+=strKey;

    if (GetSP()->sp_bUseFrags) {
      strKey.PrintF("\\frags_%d\\%d", iPlayer, m_psLevelStats.ps_iKills);
	    strOut+=strKey;
    } else {
      strKey.PrintF("\\frags_%d\\%d", iPlayer, m_psLevelStats.ps_iScore);
	    strOut+=strKey;
    }

    strKey.PrintF("\\ping_%d\\%d", iPlayer, INDEX(ceil(GetController()->GetPing()*1000.0f)));
    strOut+=strKey;
  };

  // check if message is in inbox
  BOOL HasMessage(const CTFileName &fnmMessage)
  {
    ULONG ulHash = fnmMessage.GetHash();
    INDEX ctMsg = m_acmiMessages.Count();

    for (INDEX iMsg=0; iMsg<ctMsg; iMsg++)
    {
      if (m_acmiMessages[iMsg].cmi_ulHash      == ulHash &&
          m_acmiMessages[iMsg].cmi_fnmFileName == fnmMessage) {
        return TRUE;
      }
    }

    return FALSE;
  }

  // receive a computer message and put it in inbox if not already there
  void ReceiveComputerMessage(const CTFileName &fnmMessage, ULONG ulFlags)
  {
    // if already received
    if (HasMessage(fnmMessage)) {
      // do nothing
      return;
    }

    // add it to array
    CCompMessageID &cmi = m_acmiMessages.Push();
    cmi.NewMessage(fnmMessage);
    cmi.cmi_bRead = ulFlags&CMF_READ;

    if (!(ulFlags&CMF_READ)) {
      m_ctUnreadMessages++;
      cmp_bUpdateInBackground = TRUE;
    }

    if (!(ulFlags&CMF_READ) && (ulFlags&CMF_ANALYZE)) {
      m_tmAnalyseEnd = _pTimer->CurrentTick()+2.0f;
      m_soMessage.Set3DParameters(25.0f, 5.0f, 1.0f, 1.0f);
      PlaySound(m_soMessage, SOUND_INFO, SOF_3D|SOF_VOLUMETRIC|SOF_LOCAL);
    }
  }

  void SayVoiceMessage(const CTFileName &fnmMessage)
  {
    BOOL bNoQuotes = GetSettings() != NULL && GetSettings()->ps_ulFlags&PSF_NOQUOTES;
    
    if (bNoQuotes) {
      return;
    }

    SetSpeakMouthPitch();
    PlaySound(m_soSpeech, fnmMessage, SOF_3D|SOF_VOLUMETRIC);
  }

  // receive all messages in one directory - cheat
  void CheatAllMessagesDir(const CTString &strDir, ULONG ulFlags)
  {
    // list the directory
    CDynamicStackArray<CTFileName> afnmDir;
    MakeDirList(afnmDir, strDir, "*.txt", DLI_RECURSIVE);

    // for each file in the directory
    for (INDEX i=0; i<afnmDir.Count(); i++)
    {
      CTFileName fnm = afnmDir[i];
      // add the message
      ReceiveComputerMessage(fnm, ulFlags);
    }
  }

  // mark that an item was picked
  void ItemPicked(const CTString &strName, INDEX iQuantity)
  {
    // if nothing picked too long
    if (_pTimer->CurrentTick() > m_tmLastPicked+PICKEDREPORT_TIME) {
      m_strPickedName = ""; // kill the name
      m_fPickedMana = 0; // reset picked mana
    }

    // if different than last picked
    if (m_strPickedName != strName) {
      m_strPickedName = strName; // remember name
      m_iPickedAmount = 0; // reset picked ammount
    }

    // increase ammount
    m_iPickedAmount += iQuantity;
    m_tmLastPicked = _pTimer->CurrentTick();
  }

  // Setup light source
  void SetupLightSource(void)
  {
    // setup light source
    CLightSource lsNew;
    lsNew.ls_ulFlags = LSF_NONPERSISTENT|LSF_DYNAMIC;
    lsNew.ls_rHotSpot = 1.0f;
    lsNew.ls_colColor = C_WHITE;
    lsNew.ls_rFallOff = 2.5f;
    lsNew.ls_plftLensFlare = NULL;
    lsNew.ls_ubPolygonalMask = 0;
    lsNew.ls_paoLightAnimation = &m_aoLightAnimation;

    m_lsLightSource.ls_penEntity = this;
    m_lsLightSource.SetLightSource(lsNew);
  };

  // play light animation
  void PlayLightAnim(INDEX iAnim, ULONG ulFlags)
  {
    if (m_aoLightAnimation.GetData()!=NULL) {
      m_aoLightAnimation.PlayAnim(iAnim, ulFlags);
    }
  };

  BOOL AdjustShadingParameters(FLOAT3D &vLightDirection, COLOR &colLight, COLOR &colAmbient)
  {
    if (cht_bDumpPlayerShading)
    {
      ANGLE3D a3dHPB;
      DirectionVectorToAngles(-vLightDirection, a3dHPB);
      UBYTE ubAR, ubAG, ubAB;
      UBYTE ubCR, ubCG, ubCB;
      ColorToRGB(colAmbient, ubAR, ubAG, ubAB);
      ColorToRGB(colLight, ubCR, ubCG, ubCB);
      CDebugF("Ambient: %d,%d,%d, Color: %d,%d,%d, Direction HPB (%g,%g,%g)\n",
        ubAR, ubAG, ubAB, ubCR, ubCG, ubCB, a3dHPB(1), a3dHPB(2), a3dHPB(3));
    }

    // make models at least a bit bright in deathmatch
    if (!GetSP()->sp_bCooperative) {
      UBYTE ubH, ubS, ubV;
      ColorToHSV(colAmbient, ubH, ubS, ubV);

      if (ubV<22) {
        ubV = 22;
        colAmbient = HSVToColor(ubH, ubS, ubV);
      }
    }

    return CPawnEntity::AdjustShadingParameters(vLightDirection, colLight, colAmbient);
  };

  // get a different model object for rendering
  CModelObject *GetModelForRendering(void)
  {
    // if not yet initialized
    if (!(m_ulFlags&PLF_INITIALIZED)) {
      return GetModelObject(); // return base model
    }

    // lerp player viewpoint
    CPlacement3D plView;
    plView.Lerp(en_plLastViewpoint, en_plViewpoint, _pTimer->GetLerpFactor());

    // body and head attachment animation
    GetPlayerAnimator()->BodyAndHeadOrientation(plView);
    GetPlayerAnimator()->OnPreRender();

    // synchronize your appearance with the default model
    m_moRender.Synchronize(*GetModelObject());
    if (m_ulFlags&PLF_SYNCWEAPON) {
      m_ulFlags &= ~PLF_SYNCWEAPON;
      GetPlayerAnimator()->SyncWeapon();
    }

    FLOAT tmNow = _pTimer->GetLerpedCurrentTick();

    FLOAT fFading = 1.0f;
    if (m_tmFadeStart!=0) {
      FLOAT fFactor = (tmNow-m_tmFadeStart)/5.0f;
      fFactor = Clamp(fFactor, 0.0f, 1.0f);
      fFading*=fFactor;
    }

    // if invunerable after spawning
    INDEX iProtectionAmplifier = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_SPAWN_PROTECTION);
    
    if (iProtectionAmplifier > 0) {
      // blink fast
      FLOAT fDelta = tmNow - m_tmSpawned;
      fFading *= 0.75f+0.25f*Sin(fDelta/0.5f*360);
    }

    COLOR colAlpha = m_moRender.mo_colBlendColor;
    colAlpha = (colAlpha&0xffffff00) + (COLOR(fFading*0xff)&0xff);
    m_moRender.mo_colBlendColor = colAlpha;
    
    CStatusEffectInstance *pInvisibility = GetInventory()->GetStrongestStatusEffectInstance(E_STEF_INVISIBILITY);

    if (pInvisibility) {
      FLOAT tmInvisibility = pInvisibility->GetDuration();

      // if not connected
      if (m_ulFlags&PLF_NOTCONNECTED) {
        fFading *= 0.25f+0.25f*Sin(tmNow/2.0f*360); // pulse slowly

      // if invisible
      } else if (tmInvisibility > 0.0F) {
        FLOAT fIntensity=0.0f;

        if (tmInvisibility < 3.0f)
        {
          fIntensity = 0.5f-0.5f*cos(tmInvisibility * (6.0f*3.1415927f/3.0f));
        }

        if (_ulPlayerRenderingMask == 1 << GetMyPlayerIndex()) {
          colAlpha = (colAlpha&0xffffff00)|(INDEX)(INVISIBILITY_ALPHA_LOCAL+(FLOAT)(254-INVISIBILITY_ALPHA_LOCAL)*fIntensity);
        } else if (TRUE) {
          if (tmInvisibility < 1.28f) {
            colAlpha = (colAlpha&0xffffff00)|(INDEX)(INVISIBILITY_ALPHA_REMOTE+(FLOAT)(254-INVISIBILITY_ALPHA_REMOTE)*fIntensity);
          } else if (TRUE) {
            colAlpha = (colAlpha&0xffffff00)|INVISIBILITY_ALPHA_REMOTE;
          }
        }

        m_moRender.mo_colBlendColor = colAlpha;
      }
    }

    // use the appearance for rendering
    return &m_moRender;
  }

  // wrapper for action marker getting
  class CPlayerActionMarkerEntity *GetActionMarker(void)
  {
    return (CPlayerActionMarkerEntity *)&*m_penActionMarker;
  }
  
  CEntity *GetEntityOfClass(const CTString &strName, INDEX iEntityWithThatName)
  {
    INDEX ctEntities = 0;
    CEntity *pen = NULL;

    FOREACHINDYNAMICCONTAINER(GetWorld()->wo_cenEntities, CEntity, iten)
    {
      if (IsOfClass(iten, strName)) {
        pen = iten;

        if (ctEntities == iEntityWithThatName) {
          break;
        }

        ctEntities++;
      }
    }

    return pen;
  }
  
  void FindGameInfo(void)
  {
    if (m_penGameInfo == NULL) {
      m_penGameInfo = GetEntityOfClass("GameInfo", 0);
    }
  }

  // find main music holder if not remembered
  void FindMusicHolder(void)
  {
    if (m_penMainMusicHolder==NULL) {
      m_penMainMusicHolder = _pNetwork->GetEntityWithName("MusicHolder", 0);
    }
  }

  // update per-level stats
  void UpdateLevelStats(void)
  {
    // clear stats for this level
    m_psLevelStats = PlayerStats();

    // get music holder
    if (m_penMainMusicHolder==NULL) {
      return;
    }

    CMusicHolderEntity *pmh = GetMusicHolder();

    // assure proper count enemies in current world
    if (pmh->m_ctEnemiesInWorld==0) {
      pmh->CountEnemies();
    }

    // set totals for level and increment for game
    m_psLevelTotal.ps_iKills = pmh->m_ctEnemiesInWorld;
    m_psGameTotal.ps_iKills += pmh->m_ctEnemiesInWorld;
    m_psLevelTotal.ps_iSecrets = pmh->m_ctSecretsInWorld;
    m_psGameTotal.ps_iSecrets += pmh->m_ctSecretsInWorld;
  }

  // check if there is fuss
  BOOL IsFuss(void)
  {
    // if no music holder
    if (m_penMainMusicHolder==NULL) {
      // no fuss
      return FALSE;
    }

    // if no enemies - no fuss
    return GetMusicHolder()->m_cenFussMakers.Count()>0;
  }

  void SetDefaultMouthPitch(void)
  {
    m_soMouth.Set3DParameters(50.0f, 10.0f, 1.0f, 1.0f);
  }

  void SetRandomMouthPitch(FLOAT fMin, FLOAT fMax)
  {
    m_soMouth.Set3DParameters(50.0f, 10.0f, 1.0f, Lerp(fMin, fMax, FRnd()));
  }

  void SetSpeakMouthPitch(void)
  {
    m_soSpeech.Set3DParameters(50.0f, 10.0f, 2.0f, 1.0f);
  }

  // added: also shake view because of chainsaw firing
  void ApplyShaking(CPlacement3D &plViewer)
  {
    // chainsaw shaking
    FLOAT fT = _pTimer->GetLerpedCurrentTick();

    if (fT<m_tmChainShakeEnd)
    {
      m_fChainsawShakeDX = 0.03f*m_fChainShakeStrength*SinFast(fT*m_fChainShakeFreqMod*3300.0f);
      m_fChainsawShakeDY = 0.03f*m_fChainShakeStrength*SinFast(fT*m_fChainShakeFreqMod*2900.0f);

      plViewer.pl_PositionVector(1) += m_fChainsawShakeDX;
      plViewer.pl_PositionVector(3) += m_fChainsawShakeDY;
    }

    CWorldSettingsControllerEntity *pwsc = GetWSC(this);
    if (pwsc==NULL || pwsc->m_tmShakeStarted<0) {
      return;
    }

    TIME tm = _pTimer->GetLerpedCurrentTick()-pwsc->m_tmShakeStarted;
    if (tm<0) {
      return;
    }

    FLOAT fDistance = (plViewer.pl_PositionVector-pwsc->m_vShakePos).Length();
    FLOAT fIntensity = IntensityAtDistance(pwsc->m_fShakeFalloff, 0, fDistance);
    FLOAT fShakeY, fShakeB, fShakeZ;

    if (!pwsc->m_bShakeFadeIn) {
      fShakeY = SinFast(tm*pwsc->m_tmShakeFrequencyY*360.0f)*
        exp(-tm*(pwsc->m_fShakeFade))*
        fIntensity*pwsc->m_fShakeIntensityY;
      fShakeB = SinFast(tm*pwsc->m_tmShakeFrequencyB*360.0f)*
        exp(-tm*(pwsc->m_fShakeFade))*
        fIntensity*pwsc->m_fShakeIntensityB;
      fShakeZ = SinFast(tm*pwsc->m_tmShakeFrequencyZ*360.0f)*
        exp(-tm*(pwsc->m_fShakeFade))*
        fIntensity*pwsc->m_fShakeIntensityZ;
    } else {
      FLOAT ootm = 1.0f/tm;
      fShakeY = SinFast(tm*pwsc->m_tmShakeFrequencyY*360.0f)*
        exp((tm-2)*ootm*(pwsc->m_fShakeFade))*
        fIntensity*pwsc->m_fShakeIntensityY;
      fShakeB = SinFast(tm*pwsc->m_tmShakeFrequencyB*360.0f)*
        exp((tm-2)*ootm*(pwsc->m_fShakeFade))*
        fIntensity*pwsc->m_fShakeIntensityB;
      fShakeZ = SinFast(tm*pwsc->m_tmShakeFrequencyZ*360.0f)*
        exp((tm-2)*ootm*(pwsc->m_fShakeFade))*
        fIntensity*pwsc->m_fShakeIntensityZ;
    }

    plViewer.pl_PositionVector(2) += fShakeY;
    plViewer.pl_PositionVector(3) += fShakeZ;
    plViewer.pl_OrientationAngle(3) += fShakeB;

  }

  COLOR GetWorldGlaring(void)
  {
    CWorldSettingsControllerEntity *pwsc = GetWSC(this);

    if (pwsc==NULL || pwsc->m_tmGlaringStarted<0) {
      return 0;
    }

    TIME tm = _pTimer->GetLerpedCurrentTick();
    FLOAT fRatio = CalculateRatio(tm, pwsc->m_tmGlaringStarted, pwsc->m_tmGlaringEnded,
      pwsc->m_fGlaringFadeInRatio,  pwsc->m_fGlaringFadeOutRatio);
    COLOR colResult = (pwsc->m_colGlade&0xFFFFFF00)|(UBYTE(fRatio*255.0f));

    return colResult;
  }

  void RenderScroll(CDrawPort *pdp)
  {
    CWorldSettingsControllerEntity *pwsc = GetWSC(this);

    if (pwsc!=NULL && pwsc->m_penScrollHolder!=NULL)
    {
      CScrollHolderEntity &sch = (CScrollHolderEntity &) *pwsc->m_penScrollHolder;
      sch.Credits_Render(&sch, pdp);
    }
  }

  void RenderCredits(CDrawPort *pdp)
  {
    CWorldSettingsControllerEntity *pwsc = GetWSC(this);

    if (pwsc!=NULL && pwsc->m_penCreditsHolder!=NULL)
    {
      CCreditsHolderEntity &cch = (CCreditsHolderEntity &) *pwsc->m_penCreditsHolder;
      cch.Credits_Render(&cch, pdp);
    }
  }

  void RenderTextFX(CDrawPort *pdp)
  {
    CWorldSettingsControllerEntity *pwsc = GetWSC(this);

    if (pwsc!=NULL && pwsc->m_penTextFXHolder!=NULL)
    {
      CTextFXHolderEntity &tfx = (CTextFXHolderEntity &) *pwsc->m_penTextFXHolder;
      tfx.TextFX_Render(&tfx, pdp);
    }
  }

  void RenderHudPicFX(CDrawPort *pdp)
  {
    CWorldSettingsControllerEntity *pwsc = GetWSC(this);

    if (pwsc!=NULL && pwsc->m_penHudPicFXHolder!=NULL)
    {
      CHudPicHolderEntity &hpfx = (CHudPicHolderEntity &) *pwsc->m_penHudPicFXHolder;
      hpfx.HudPic_Render(&hpfx, pdp);
    }
  }

/************************************************************
 *                    RENDER GAME VIEW                      *
 ************************************************************/

  // setup viewing parameters for viewing from player or camera
  void SetupView(CDrawPort *pdp, CAnyProjection3D &apr, CEntity *&penViewer,
    CPlacement3D &plViewer, COLOR &colBlend, BOOL bCamera)
  {
    CPlayerWeaponEntity *penWeapons = GetWeapons(E_HAND_MAIN);
    
    // read the exact placement of the view for this tick
    GetLerpedAbsoluteViewPlacement(plViewer);
    ASSERT(IsValidFloat(plViewer.pl_OrientationAngle(1))&&IsValidFloat(plViewer.pl_OrientationAngle(2))&&IsValidFloat(plViewer.pl_OrientationAngle(3)) );

    // get current entity that the player views from
    penViewer = GetViewEntity();

    INDEX iViewState = m_iViewState;

    if (m_penCamera!=NULL && bCamera) {
      iViewState = PVT_SCENECAMERA;
      plViewer = m_penCamera->GetLerpedPlacement();
      penViewer = m_penCamera;
    }

    // init projection parameters
    CPerspectiveProjection3D prPerspectiveProjection;
    plr_fFOV = Clamp(plr_fFOV, 1.0f, 160.0f);
    ANGLE aFOV = plr_fFOV;

    // disable zoom in deathmatch
    if (!GetSP()->sp_bCooperative) {
      aFOV = 90.0f;
    }

    // if sniper active
    if (penWeapons->CanZoom() && m_ulFlags&PLF_ISZOOMING)
    {
      aFOV = Lerp(penWeapons->m_fSniperFOVlast, penWeapons->m_fSniperFOV, _pTimer->GetLerpFactor());
    }

    if (m_pstState==PST_DIVE && iViewState == PVT_PLAYEREYES) {
      TIME tmNow = _pTimer->GetLerpedCurrentTick();
      aFOV+=sin(tmNow*0.79f)*2.0f;
    }

    ApplyShaking(plViewer);

    colBlend = 0;

    if (iViewState == PVT_SCENECAMERA) {
      CCameraEntity *pcm = (CCameraEntity*)&*m_penCamera;
      prPerspectiveProjection.FOVL() =
        Lerp(pcm->m_fLastFOV, pcm->m_fFOV, _pTimer->GetLerpFactor());

      if (pcm->m_tmDelta>0.001f) {
        FLOAT fFactor = (_pTimer->GetLerpedCurrentTick()-pcm->m_tmAtMarker)/pcm->m_tmDelta;
        fFactor = Clamp(fFactor, 0.0f, 1.0f);
        colBlend = LerpColor(pcm->m_colFade0, pcm->m_colFade1, fFactor);
      } else {
        colBlend = pcm->m_colFade0;
      }
    } else {
      prPerspectiveProjection.FOVL() = aFOV;
    }

    prPerspectiveProjection.ScreenBBoxL() = FLOATaabbox2D(
      FLOAT2D(0.0f, 0.0f),
      FLOAT2D((FLOAT)pdp->GetWidth(), (FLOAT)pdp->GetHeight())
    );

    // determine front clip plane
    plr_fFrontClipDistance = Clamp(plr_fFrontClipDistance, 0.05f, 0.50f);
    FLOAT fFCD = plr_fFrontClipDistance;

    // adjust front clip plane if swimming
    if (m_pstState==PST_SWIM && iViewState==PVT_PLAYEREYES) { fFCD *= 0.6666f; }
    prPerspectiveProjection.FrontClipDistanceL() = fFCD;
    prPerspectiveProjection.AspectRatioL() = 1.0f;

    // set up viewer position
    apr = prPerspectiveProjection;
    apr->ViewerPlacementL() = plViewer;
    apr->ObjectPlacementL() = CPlacement3D(FLOAT3D(0, 0, 0), ANGLE3D(0,0,0));
    prPlayerProjection = apr;
    prPlayerProjection->Prepare();
  }

  // listen from a given viewer
  void ListenFromEntity(CEntity *penListener, const CPlacement3D &plSound)
  {
    FLOATmatrix3D mRotation;
    MakeRotationMatrixFast(mRotation, plSound.pl_OrientationAngle);
    sliSound.sli_vPosition = plSound.pl_PositionVector;
    sliSound.sli_mRotation = mRotation;
    sliSound.sli_fVolume = 1.0f;
    sliSound.sli_vSpeed = en_vCurrentTranslationAbsolute;
    sliSound.sli_penEntity = penListener;

    if (m_pstState == PST_DIVE) {
      sliSound.sli_fFilter = 20.0f;
    } else {
      sliSound.sli_fFilter = 0.0f;
    }

    INDEX iEnv = 0;

    CBrushSector *pbsc = penListener->GetSectorFromPoint(plSound.pl_PositionVector);

    // for each sector around listener
    if (pbsc!=NULL) {
      iEnv = pbsc->GetEnvironmentType();
    }

    // get the environment
    CEnvironmentType &et = GetWorld()->wo_aetEnvironmentTypes[iEnv];
    sliSound.sli_iEnvironmentType = et.et_iType;
    sliSound.sli_fEnvironmentSize = et.et_fSize;
    _pSound->Listen(sliSound);
  }

  // render view from player
  void RenderPlayerView(CDrawPort *pdp, BOOL bShowExtras)
  {
    CAnyProjection3D apr;
    CEntity *penViewer;
    CPlacement3D plViewer;
    COLOR colBlend;

    // for each eye
    for (INDEX iEye=STEREO_LEFT; iEye<=(Stereo_IsEnabled()?STEREO_RIGHT:STEREO_LEFT); iEye++)
    {
      // setup view settings
      SetupView(pdp, apr, penViewer, plViewer, colBlend, FALSE);

      // setup stereo rendering
      Stereo_SetBuffer(iEye);
      Stereo_AdjustProjection(*apr, iEye, 1);

      // render the view
      ASSERT(IsValidFloat(plViewer.pl_OrientationAngle(1))&&IsValidFloat(plViewer.pl_OrientationAngle(2))&&IsValidFloat(plViewer.pl_OrientationAngle(3)));
      _ulPlayerRenderingMask = 1 << GetMyPlayerIndex();
      RenderView(*en_pwoWorld, *penViewer, apr, *pdp);
      _ulPlayerRenderingMask = 0;

      if (iEye==STEREO_LEFT) {
        // listen from here
        ListenFromEntity(this, plViewer);
      }

      RenderScroll(pdp);
      RenderTextFX(pdp);
      RenderCredits(pdp);
      RenderHudPicFX(pdp);

      if (hud_bShowAll && bShowExtras) {
        // let the player entity render its interface
        CPlacement3D plLight(_vViewerLightDirection, ANGLE3D(0,0,0));
        plLight.AbsoluteToRelative(plViewer);
        RenderHUD(*(CPerspectiveProjection3D *)(CProjection3D *)apr, pdp,
          plLight.pl_PositionVector, _colViewerLight, _colViewerAmbient,
          penViewer==this && (GetFlags()&ENF_ALIVE), iEye);
      }
    }

    Stereo_SetBuffer(STEREO_BOTH);

    // determine and cache main drawport, size and relative scale
    PIX pixDPWidth  = pdp->GetWidth();
    PIX pixDPHeight = pdp->GetHeight();
    FLOAT fScale = (FLOAT)pixDPWidth/640.0f;

    // print center message
    if (_pTimer->CurrentTick()<m_tmCenterMessageEnd) {
      pdp->SetFont(_pfdDisplayFont);
      pdp->SetTextScaling(fScale);
      pdp->SetTextAspect(1.0f);
      pdp->PutTextCXY(m_strCenterMessage, pixDPWidth*0.5f, pixDPHeight*0.85f, C_WHITE|0xDD);

    // print picked item
    } else if (_pTimer->CurrentTick()<m_tmLastPicked+PICKEDREPORT_TIME) {
      pdp->SetFont(_pfdDisplayFont);
      pdp->SetTextScaling(fScale);
      pdp->SetTextAspect(1.0f);
      CTString strPicked;

      if (m_iPickedAmount == 0) {
        strPicked = m_strPickedName;
      } else {
        strPicked.PrintF("%s +%d", m_strPickedName, m_iPickedAmount);
      }

      pdp->PutTextCXY(strPicked, pixDPWidth*0.5f, pixDPHeight*0.82f, C_WHITE|0xDD);

      if (!GetSP()->sp_bCooperative && !GetSP()->sp_bUseFrags && m_fPickedMana>=1) {
        CTString strValue;
        strValue.PrintF("%s +%d", TRANS("Value"), INDEX(m_fPickedMana));
        pdp->PutTextCXY(strValue, pixDPWidth*0.5f, pixDPHeight*0.85f, C_WHITE|0xDD);
      }
    }

    if (_pTimer->CurrentTick()<m_tmAnalyseEnd) {
      pdp->SetFont(_pfdDisplayFont);
      pdp->SetTextScaling(fScale);
      pdp->SetTextAspect(1.0f);
      UBYTE ubA = int(sin(_pTimer->CurrentTick()*10.0f)*127+128);
      pdp->PutTextCXY(TRANS("Analyzing..."), pixDPWidth*0.5f, pixDPHeight*0.2f, SE_COL_BLUE_NEUTRAL_LT|ubA);
    }
  }

  // render view from camera
  void RenderCameraView(CDrawPort *pdp, BOOL bListen)
  {
    CDrawPort dpCamera;
    CDrawPort *pdpCamera = pdp;

    if (m_penCamera!=NULL && ((CCameraEntity&)*m_penCamera).m_bWideScreen) {
      pdp->MakeWideScreen(&dpCamera);
      pdpCamera = &dpCamera;
    }

    pdp->Unlock();
    pdpCamera->Lock();

    CAnyProjection3D apr;
    CEntity *penViewer;
    CPlacement3D plViewer;
    COLOR colBlend;

    // for each eye
    for (INDEX iEye=STEREO_LEFT; iEye<=(Stereo_IsEnabled()?STEREO_RIGHT:STEREO_LEFT); iEye++)
    {
      // setup view settings
      SetupView(pdpCamera, apr, penViewer, plViewer, colBlend, TRUE);

      // setup stereo rendering
      Stereo_SetBuffer(iEye);
      Stereo_AdjustProjection(*apr, iEye, 1);

      // render the view
      ASSERT(IsValidFloat(plViewer.pl_OrientationAngle(1))&&IsValidFloat(plViewer.pl_OrientationAngle(2))&&IsValidFloat(plViewer.pl_OrientationAngle(3)));
      _ulPlayerRenderingMask = 1 << GetMyPlayerIndex();
      RenderView(*en_pwoWorld, *penViewer, apr, *pdpCamera);
      _ulPlayerRenderingMask = 0;

      // listen from there if needed
      if (bListen && iEye==STEREO_LEFT) {
        ListenFromEntity(penViewer, plViewer);
      }
    }

    Stereo_SetBuffer(STEREO_BOTH);

    RenderScroll(pdpCamera);
    RenderTextFX(pdpCamera);
    RenderCredits(pdpCamera);
    RenderHudPicFX(pdpCamera);

    // add world glaring
    {
      COLOR colGlare = GetWorldGlaring();
      UBYTE ubR, ubG, ubB, ubA;
      ColorToRGBA(colGlare, ubR, ubG, ubB, ubA);

      if (ubA!=0) {
        pdpCamera->dp_ulBlendingRA += ULONG(ubR)*ULONG(ubA);
        pdpCamera->dp_ulBlendingGA += ULONG(ubG)*ULONG(ubA);
        pdpCamera->dp_ulBlendingBA += ULONG(ubB)*ULONG(ubA);
        pdpCamera->dp_ulBlendingA  += ULONG(ubA);
      }

      // do all queued screen blendings
      pdpCamera->BlendScreen();
    }

    pdpCamera->Unlock();
    pdp->Lock();

    // camera fading
    if ((colBlend&CT_AMASK)!=0) {
      pdp->Fill(colBlend);
    }

    // print center message
    if (_pTimer->CurrentTick()<m_tmCenterMessageEnd) {
      PIX pixDPWidth  = pdp->GetWidth();
      PIX pixDPHeight = pdp->GetHeight();
      FLOAT fScale = (FLOAT)pixDPWidth/640.0f;
      pdp->SetFont(_pfdDisplayFont);
      pdp->SetTextScaling(fScale);
      pdp->SetTextAspect(1.0f);
      pdp->PutTextCXY(m_strCenterMessage, pixDPWidth*0.5f, pixDPHeight*0.85f, C_WHITE|0xDD);
    }
  }

  void RenderGameView(CDrawPort *pdp, void *pvUserData)
  {
    BOOL bInitialized = m_ulFlags & PLF_INITIALIZED;
    if (!bInitialized) {
      return;
    }
    
    pdp->Unlock();
    
    BOOL bShowExtras = (ULONG(pvUserData)&GRV_SHOWEXTRAS);

    // if rendering real game view (not thumbnail, or similar)
    if (pvUserData!=0) {
      CTimerValue tvNow = _pTimer->GetHighPrecisionTimer();

      // if rendered a game view recently
      if ((tvNow-_tvProbingLast).GetSeconds()<0.1) {
        // allow probing
        _pGfx->gl_bAllowProbing = TRUE;
      }

      _tvProbingLast = tvNow;
    }

    //CDebugF("%s: render\n", GetPredictName());

    // check for dualhead
    BOOL bDualHead =
      pdp->IsDualHead() &&
      GetSP()->sp_gmGameMode!=CSessionProperties::GM_FLYOVER &&
      m_penActionMarker==NULL;

    // if dualhead, or no camera active
    if (bDualHead||m_penCamera==NULL) {
      // make left player view
      CDrawPort dpView(pdp, TRUE);

      if (dpView.Lock()) {
        RenderPlayerView(&dpView, bShowExtras); // draw it
        dpView.Unlock();
      }
    }

    // if camera active
    if (m_penCamera!=NULL) {
      // make left or right camera view
      CDrawPort dpView(pdp, m_penActionMarker!=NULL);

      if (dpView.Lock()) {
        // draw it, listen if not dualhead
        RenderCameraView(&dpView, !bDualHead);
        dpView.Unlock();
      }

    // if camera is not active
    } else {
      // if dualhead
      if (bDualHead) {
        // render computer on secondary display
        cmp_ppenDHPlayer = this;
      }
    }

    // all done - lock back the original drawport
    pdp->Lock();
  };

/************************************************************
 *                   PRE/DO/POST MOVING                     *
 ************************************************************/

  // premoving for soft player up-down movement
  void PreMoving(void)
  {
    /*CDebugF("pos(%s): %g,%g,%g\n", GetPredictName(),
      GetPlacement().pl_PositionVector(1),
      GetPlacement().pl_PositionVector(2),
      GetPlacement().pl_PositionVector(3));
      */

    GetPlayerAnimator()->StoreLast();
    CPawnEntity::PreMoving();
  };

  // do moving
  void DoMoving(void)
  {
    CPawnEntity::DoMoving();
    GetPlayerAnimator()->AnimateBanking();

    if (m_penView != NULL) {
      GetView()->DoMoving();
    }

    if (m_pen3rdPersonView != NULL) {
      GetView3rd()->DoMoving();
    }
  };

  // postmoving for soft player up-down movement
  void PostMoving(void)
  {
    CPawnEntity::PostMoving();

    // never allow a player to be removed from the list of movers
    en_ulFlags &= ~ENF_INRENDERING;

    GetPlayerAnimator()->AnimateSoftEyes();
    //GetPlayerAnimator()->AnimateRecoilPitch();

    // slowly increase mana with time, faster if player is not moving; (only if alive)
    if (GetFlags()&ENF_ALIVE)
    {
      m_fManaFraction +=
        ClampDn(1.0f-en_vCurrentTranslationAbsolute.Length()/20.0f, 0.0f) * 20.0f
        * _pTimer->TickQuantum;
      INDEX iNewMana = m_fManaFraction;
      m_iMana         += iNewMana;
      m_fManaFraction -= iNewMana;
    }

    // if in tourist mode
    if (GetSP()->sp_gdGameDifficulty==CSessionProperties::GD_TOURIST && GetFlags()&ENF_ALIVE) {
      m_fHealthRegen += 1.0F * _pTimer->TickQuantum;
    }

    // May be NULL during level change.
    if (m_penInventory != NULL) {
      INDEX iRegenAmplifier = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_REGENERATION);
      if (iRegenAmplifier > 0) {
        m_fHealthRegen += iRegenAmplifier * _pTimer->TickQuantum;
      }
      
      if (!(GetFlags()&ENF_ALIVE)) {
        m_fHealthRegen = 0.0F;
      }

      if (m_fHealthRegen >= 1.0F) {
        FLOAT fIntPart = 0.0F;
        m_fHealthRegen = modff(m_fHealthRegen, &fIntPart);
        
        ReceiveHealth(fIntPart * HEALTH_VALUE_MULTIPLIER, TopHealth());
      }

      GetInventory()->OnStep();
    }

    // update ray hit for weapon target
    UpdateWeaponTargetting();

    if (m_pen3rdPersonView != NULL) {
      GetView3rd()->PostMoving();
    }

    if (m_penView != NULL) {
      GetView()->PostMoving();
    }

    // if didn't have any action in this tick
    if (!(m_ulFlags&PLF_APPLIEDACTION)) {
      SetUnconnected(); // means we are not connected
    }

    // clear action indicator
    m_ulFlags&=~PLF_APPLIEDACTION;
  }

  // set player parameters for unconnected state (between the server loads and player reconnects)
  void SetUnconnected(void)
  {
    if (m_ulFlags&PLF_NOTCONNECTED) {
      return;
    }

    m_ulFlags |= PLF_NOTCONNECTED;

    // reset to a dummy state
    ForceFullStop();
    SetPhysicsFlags(GetPhysicsFlags() & ~(EPF_TRANSLATEDBYGRAVITY|EPF_ORIENTEDBYGRAVITY));
    SetCollisionFlags(GetCollisionFlags() & ~((ECBI_BRUSH|ECBI_MODEL)<<ECB_TEST));
    en_plLastViewpoint.pl_OrientationAngle = en_plViewpoint.pl_OrientationAngle = ANGLE3D(0,0,0);

    StartModelAnim(PLAYER_ANIM_STAND, 0);
    GetPlayerAnimator()->BodyAnimationTemplate(
      BODY_ANIM_NORMALWALK, BODY_ANIM_COLT_STAND, BODY_ANIM_SHOTGUN_STAND, BODY_ANIM_MINIGUN_STAND,
      AOF_LOOPING|AOF_NORESTART);
  }

  // set player parameters for connected state
  void SetConnected(void)
  {
    if (!(m_ulFlags&PLF_NOTCONNECTED)) {
      return;
    }

    m_ulFlags &= ~PLF_NOTCONNECTED;

    SetPhysicsFlags(GetPhysicsFlags() | (EPF_TRANSLATEDBYGRAVITY|EPF_ORIENTEDBYGRAVITY));
    SetCollisionFlags(GetCollisionFlags() | ((ECBI_BRUSH|ECBI_MODEL)<<ECB_TEST));
  }

  // check if player is connected or not
  BOOL IsConnected(void) const
  {
    return !(m_ulFlags&PLF_NOTCONNECTED);
  }

  // create a checksum value for sync-check
  void ChecksumForSync(ULONG &ulCRC, INDEX iExtensiveSyncCheck)
  {
    CPawnEntity::ChecksumForSync(ulCRC, iExtensiveSyncCheck);
    CRC_AddLONG(ulCRC, m_psLevelStats.ps_iScore);
    CRC_AddLONG(ulCRC, m_iMana);

    if (iExtensiveSyncCheck>0) {
      CRC_AddFLOAT(ulCRC, m_fManaFraction);
    }

    CRC_AddLONG(ulCRC, m_iArmor);
  }

  // dump sync data to text file
  void DumpSync_t(CTStream &strm, INDEX iExtensiveSyncCheck)  // throw char *
  {
    CPawnEntity::DumpSync_t(strm, iExtensiveSyncCheck);
    strm.FPrintF_t("Score: %d\n", m_psLevelStats.ps_iScore);
    strm.FPrintF_t("m_iMana:  %d\n", m_iMana);
    strm.FPrintF_t("m_fManaFraction: %g(%08x)\n", m_fManaFraction, (ULONG&)m_fManaFraction);
    strm.FPrintF_t("m_iArmor: %g(%08x)\n", m_iArmor, (ULONG&)m_iArmor);
  }

/************************************************************
 *         DAMAGE OVERRIDE (PLAYER HAS ARMOR)               *
 ************************************************************/
  // leave stain
  virtual void LeaveStain(BOOL bGrow)
  {
    ESpawnEffect ese;
    FLOAT3D vPoint;
    FLOATplane3D vPlaneNormal;
    FLOAT fDistanceToEdge;
    // get your size
    FLOATaabbox3D box;
    GetBoundingBox(box);

    // on plane
    if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge)) {
      // if near to polygon and away from last stain point
      if ((vPoint-GetPlacement().pl_PositionVector).Length()<0.5f
        && (m_vLastStain-vPoint).Length()>1.0f ) {
        m_vLastStain = vPoint;
        FLOAT fStretch = box.Size().Length();
        ese.colMuliplier = C_WHITE|CT_OPAQUE;

        // stain
        if (bGrow) {
          ese.betType    = BET_BLOODSTAINGROW;
          ese.vStretch   = FLOAT3D(fStretch*1.5f, fStretch*1.5f, 1.0f);
        } else {
          ese.betType    = BET_BLOODSTAIN;
          ese.vStretch   = FLOAT3D(fStretch*0.75f, fStretch*0.75f, 1.0f);
        }

        ese.vNormal    = FLOAT3D(vPlaneNormal);
        ese.vDirection = FLOAT3D(0, 0, 0);
        FLOAT3D vPos = vPoint+ese.vNormal/50.0f*(FRnd()+0.5f);
        CEntityPointer penEffect = CreateEntity(CPlacement3D(vPos, ANGLE3D(0,0,0)), CLASS_BASIC_EFFECT);
        penEffect->Initialize(ese);
      }
    }
  };

  // TODO: Check it out!
  void DamageImpact(INDEX iDamageType, INDEX iDamageAmount, const FLOAT3D &vHitPoint, const FLOAT3D &vDirection)
  {
    // if exploded then do nothing
    if (GetRenderType()!=RT_MODEL) {
      return;
    }

    if (iDamageType == DMT_ABYSS || iDamageType == DMT_SPIKESTAB) {
      return;
    }

    iDamageAmount = Clamp(iDamageAmount, INDEX(0), INDEX(5000) * HEALTH_VALUE_MULTIPLIER);

    FLOAT fKickDamage = FLOAT(iDamageAmount) / HEALTH_VALUE_MULTIPLIER;
    iDamageAmount /= HEALTH_VALUE_MULTIPLIER;

    if ((iDamageType == DMT_EXPLOSION) || (iDamageType == DMT_IMPACT) || (iDamageType == DMT_CANNONBALL_EXPLOSION) )
    {
      fKickDamage*=1.5;
    }

    if (iDamageType==DMT_DROWNING || iDamageType==DMT_CLOSERANGE) {
      fKickDamage /= 10;
    }

    if (iDamageType==DMT_CHAINSAW)
    {
      fKickDamage /= 10;
    }

    // get passed time since last damage
    TIME tmNow = _pTimer->CurrentTick();
    TIME tmDelta = tmNow-m_tmLastDamage;
    m_tmLastDamage = tmNow;

    // fade damage out
    if (tmDelta>=_pTimer->TickQuantum*3) {
      m_vDamage=FLOAT3D(0, 0, 0);
    }

    // add new damage
    FLOAT3D vDirectionFixed;
    if (vDirection.ManhattanNorm()>0.5f) {
      vDirectionFixed = vDirection;
    } else {
      vDirectionFixed = -en_vGravityDir;
    }

    FLOAT3D vDamageOld = m_vDamage;
    m_vDamage+=(vDirectionFixed/*-en_vGravityDir/2*/)*fKickDamage;

    FLOAT fOldLen = vDamageOld.Length();
    FLOAT fNewLen = m_vDamage.Length();
    FLOAT fOldRootLen = Sqrt(fOldLen);
    FLOAT fNewRootLen = Sqrt(fNewLen);

    FLOAT fMassFactor = 200.0f/((EntityInfo*)GetEntityInfo())->fMass;

    if (!(en_ulFlags & ENF_ALIVE))
    {
      fMassFactor /= 3;
    }

    switch (iDamageType)
    {
      case DMT_CLOSERANGE:
      case DMT_CHAINSAW:
      case DMT_DROWNING:
      case DMT_IMPACT:
      case DMT_BRUSH:
      case DMT_BURNING:
        // do nothing
        break;
      default:
      {
        if (fOldLen != 0.0f)
        {
          // cancel last push
          GiveImpulseTranslationAbsolute(-vDamageOld/fOldRootLen*fMassFactor);
        }

        /*
        FLOAT3D vImpuls = m_vDamage/fNewRootLen*fMassFactor;
        CDebugF("Applied absolute translation impuls: (%g%g%g)\n",
          vImpuls(1),vImpuls(2),vImpuls(3));*/

        // push it back
        GiveImpulseTranslationAbsolute(m_vDamage/fNewRootLen*fMassFactor);
      }
    }

    if (m_iMaxDamageAmount < iDamageAmount)
    {
      m_iMaxDamageAmount = iDamageAmount;
    }

    // if it has no spray, or if this damage overflows it
    if ((m_tmSpraySpawned<=_pTimer->CurrentTick()-_pTimer->TickQuantum*8 ||
      m_iSprayDamage + iDamageAmount > 50)) {

      // spawn blood spray
      CPlacement3D plSpray = CPlacement3D(vHitPoint, ANGLE3D(0, 0, 0));
      m_penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
      m_penSpray->SetParent(this);
      ESpawnSpray eSpawnSpray;
      eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;

      if (m_iMaxDamageAmount > 10) {
        eSpawnSpray.fDamagePower = 3.0f;

      } else if (m_iSprayDamage + iDamageAmount > 50) {
        eSpawnSpray.fDamagePower = 2.0f;

      } else {
        eSpawnSpray.fDamagePower = 1.0f;
      }

      eSpawnSpray.sptType = SPT_BLOOD;
      eSpawnSpray.fSizeMultiplier = 1.0f;

      // setup direction of spray
      FLOAT3D vHitPointRelative = vHitPoint - GetPlacement().pl_PositionVector;
      FLOAT3D vReflectingNormal;
      GetNormalComponent(vHitPointRelative, en_vGravityDir, vReflectingNormal);
      vReflectingNormal.Normalize();

      vReflectingNormal(1)/=5.0f;

      FLOAT3D vProjectedComponent = vReflectingNormal*(vDirection%vReflectingNormal);
      FLOAT3D vSpilDirection = vDirection-vProjectedComponent*2.0f-en_vGravityDir*0.5f;

      eSpawnSpray.vDirection = vSpilDirection;
      eSpawnSpray.penOwner = this;

      // initialize spray
      m_penSpray->Initialize(eSpawnSpray);
      m_tmSpraySpawned = _pTimer->CurrentTick();
      m_iSprayDamage = 0;
      m_iMaxDamageAmount = 0;
    }

    m_iSprayDamage += iDamageAmount;
  }
  
  INDEX PowerUpsAbsorb(INDEX iDamageAmount, INDEX eDamageType)
  {
    INDEX iInvulnerabilityAmp = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_INVULNERABILITY);
    INDEX iResistAmp = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_RESISTANCE);
    INDEX iFireResistAmp = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_FIRE_RESISTANCE);
    INDEX iSpawnProtectionAmp = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_SPAWN_PROTECTION);

    // if invulnerable, nothing can harm you except telefrag or abyss    
    if (iInvulnerabilityAmp && eDamageType != DMT_ABYSS && eDamageType != DMT_TELEPORT) {
      return 0.0F;
    }

    // if invunerable after spawning then ignore damage
    if (iSpawnProtectionAmp > 0) {
      return 0.0F;
    }

    if (iFireResistAmp > 0 && eDamageType == DMT_BURNING) {
      return 0.0F;
    }

    if (iResistAmp > 0) {
      iDamageAmount *= 0.33F;
    }

    return iDamageAmount;
  }
  
  //! TODO: Rewrite when integer-based damage system will be done.
  INDEX ArmorAbsorb(INDEX iDamageAmount)
  {
    INDEX iSubArmor, iSubHealth;

    if (GetArmor() <= 0) {
      return iDamageAmount;
    }

    // damage and armor
    iSubArmor  = iDamageAmount * 2 / 3;      // 2/3 on armor damage
    iSubHealth = iDamageAmount - iSubArmor;    // 1/3 on health damage
    m_iArmor  -= iSubArmor;                     // decrease armor

    if (m_iArmor < 0) {                          // armor below zero -> add difference to health damage
      iSubHealth -= m_iArmor;
      m_iArmor    = 0;
    }

    return iSubHealth;
  }

  /* Receive damage */
  void ReceiveDamage(CEntity *penInflictor, INDEX iDamageType,
                      INDEX iDamageAmount, const FLOAT3D &vHitPoint, const FLOAT3D &vDirection)
  {
    // don't harm yourself with knife or with rocket in easy/tourist mode
    if (penInflictor==this && (iDamageType==DMT_CLOSERANGE || iDamageType==DMT_CHAINSAW ||
        ((iDamageType==DMT_EXPLOSION||iDamageType==DMT_CANNONBALL_EXPLOSION||iDamageType==DMT_PROJECTILE) &&
          GetSP()->sp_gdGameDifficulty<=CSessionProperties::GD_EASY)) ) {
      return;
    }

    // if not connected then noone can harm you
    if (m_ulFlags&PLF_NOTCONNECTED) {
      return;
    }

    // god mode -> no one can harm you
    if (m_ulCheatFlags & CHT_FLAG_GOD) {
      return;
    }

    iDamageAmount = PowerUpsAbsorb(iDamageAmount, iDamageType);

    // check for friendly fire
    if (!GetSP()->sp_bFriendlyFire && GetSP()->sp_bCooperative) {
      if (IsDerivedFromClass(penInflictor, &CPlayerPawnEntity_DLLClass) && penInflictor!=this) {
        return;
      }
    }

    // ignore heat damage if dead
    if (iDamageType == DMT_HEAT && !(GetFlags()&ENF_ALIVE)) {
      return;
    }

    // adjust for difficulty
    FLOAT fDifficultyDamage = GetSP()->sp_fDamageStrength;
    if (fDifficultyDamage<=1.0f || penInflictor!=this) {
      iDamageAmount *= fDifficultyDamage;
    }

    // ignore zero damages
    if (iDamageAmount <= 0) {
      return;
    }

    INDEX iSubHealth;
    if (iDamageType == DMT_DROWNING) { // drowning
      iSubHealth = iDamageAmount;
    } else {
      iSubHealth = ArmorAbsorb(iDamageAmount);
    }

    // if any damage on health.
    if (iSubHealth > 0) {
      // if camera is active
      if (m_penCamera!=NULL) {
        // if the camera has onbreak
        CEntity *penOnBreak = ((CCameraEntity&)*m_penCamera).m_penOnBreak;
        if (penOnBreak!=NULL) {
          // trigger it
          SendToTarget(penOnBreak, EET_TRIGGER, this);

        // if it doesn't then just deactivate camera
        } else {
          m_penCamera = NULL;
        }
      }

    }

    // if the player is doing autoactions then ignore all damage
    if (m_penActionMarker!=NULL) {
      return;
    }

    DamageImpact(iDamageType, iSubHealth, vHitPoint, vDirection);

    // receive damage
    CPawnEntity::ReceiveDamage(penInflictor, iDamageType, iSubHealth, vHitPoint, vDirection);

    // red screen and hit translation
    if (iDamageAmount > 1) {
// !!!! this is obsolete, DamageImpact is used instead!
      if (iDamageType==DMT_EXPLOSION || iDamageType==DMT_PROJECTILE || iDamageType==DMT_BULLET
       || iDamageType==DMT_IMPACT    || iDamageType==DMT_CANNONBALL || iDamageType==DMT_CANNONBALL_EXPLOSION) {
//        GiveImpulseTranslationAbsolute(vDirection*(fDamageAmmount/7.5f)
//                                        -en_vGravityDir*(fDamageAmmount/15.0f));
      }

      if (GetFlags()&ENF_ALIVE) {
        m_iDamageAmount += iDamageAmount;
        m_tmWoundedTime   = _pTimer->CurrentTick();
      }
    }

    // yell (this hurts)
    ESound eSound;
    eSound.EsndtSound = SNDT_PLAYER;
    eSound.penTarget  = this;
    SendEventInRange(eSound, FLOATaabbox3D(GetPlacement().pl_PositionVector, 10.0f));

    // play hurting sound
    if (iDamageType==DMT_DROWNING) {
      SetRandomMouthPitch(0.9f, 1.1f);
      PlaySound(m_soMouth, GenderSound(SOUND_DROWN), SOF_3D);

      if (_pNetwork->IsPlayerLocal(this)) {IFeel_PlayEffect("WoundWater");}
      m_tmMouthSoundLast = _pTimer->CurrentTick();
      PlaySound(m_soLocalAmbientOnce, SOUND_WATERBUBBLES, SOF_3D|SOF_VOLUMETRIC|SOF_LOCAL);
      m_soLocalAmbientOnce.Set3DParameters(25.0f, 5.0f, 2.0f, Lerp(0.5f, 1.5f, FRnd()) );
      SpawnBubbles(10+INDEX(FRnd()*10));

    } else if (m_iDamageAmount > 1) {
      // if not dead
      if (GetFlags()&ENF_ALIVE) {
        // determine corresponding sound
        INDEX iSound;
        char *strIFeel = NULL;

        if (m_iDamageAmount < 5 * HEALTH_VALUE_MULTIPLIER) {
          iSound = GenderSound(SOUND_WOUNDWEAK);
          strIFeel = "WoundWeak";
        } else if (m_iDamageAmount < 25 * HEALTH_VALUE_MULTIPLIER) {
          iSound = GenderSound(SOUND_WOUNDMEDIUM);
          strIFeel = "WoundMedium";
        } else {
          iSound = GenderSound(SOUND_WOUNDSTRONG);
          strIFeel = "WoundStrong";
        }

        if (m_pstState==PST_DIVE) {
          iSound = GenderSound(SOUND_WOUNDWATER);
          strIFeel = "WoundWater";
        } // override for diving

        SetRandomMouthPitch(0.9f, 1.1f);

        // give some pause inbetween screaming
        TIME tmNow = _pTimer->CurrentTick();

        if ((tmNow-m_tmScreamTime) > 1.0f) {
          m_tmScreamTime = tmNow;
          PlaySound(m_soMouth, iSound, SOF_3D);
          if (_pNetwork->IsPlayerLocal(this)) {IFeel_PlayEffect(strIFeel);}
        }
      }
    }
  };

  // should this player blow up (spawn debris)
  BOOL ShouldBlowUp(void)
  {
    // blow up if
    return
      // allowed
      GetSP()->sp_bGibs &&
      // dead and
      (GetHealth()<=0) &&
      // has received large enough damage lately and
      (m_vDamage.Length() > _fBlowUpAmmount) &&
      // is not blown up already
      GetRenderType()==RT_MODEL;
  };

  // spawn body parts
  void BlowUp(void)
  {
    FLOAT3D vNormalizedDamage = m_vDamage-m_vDamage*(_fBlowUpAmmount/m_vDamage.Length());
    vNormalizedDamage /= Sqrt(vNormalizedDamage.Length());
    vNormalizedDamage *= 0.75f;

    FLOAT3D vBodySpeed = en_vCurrentTranslationAbsolute-en_vGravityDir*(en_vGravityDir%en_vCurrentTranslationAbsolute);
    const FLOAT fBlowUpSize = 2.0f;

    // readout blood type
    const INDEX iBloodType = GetSP()->sp_iBlood;
    // determine debris texture (color)
    ULONG ulFleshTexture = TEXTURE_FLESH_GREEN;
    ULONG ulFleshModel   = MODEL_FLESH;
    if (iBloodType==2) { ulFleshTexture = TEXTURE_FLESH_RED; }

    // spawn debris
    Debris_Begin(EIBT_FLESH, DPT_BLOODTRAIL, BET_BLOODSTAIN, fBlowUpSize, vNormalizedDamage, vBodySpeed, 1.0f, 0.0f);

    for (INDEX iDebris=0; iDebris<4; iDebris++)
    {
      // flowerpower mode?
      if (iBloodType==3) {
        switch (IRnd()%5)
        {
          case 1:  { ulFleshModel = MODEL_FLESH_APPLE;   ulFleshTexture = TEXTURE_FLESH_APPLE;   break; }
          case 2:  { ulFleshModel = MODEL_FLESH_BANANA;  ulFleshTexture = TEXTURE_FLESH_BANANA;  break; }
          case 3:  { ulFleshModel = MODEL_FLESH_BURGER;  ulFleshTexture = TEXTURE_FLESH_BURGER;  break; }
          case 4:  { ulFleshModel = MODEL_FLESH_LOLLY;   ulFleshTexture = TEXTURE_FLESH_LOLLY;   break; }
          default: { ulFleshModel = MODEL_FLESH_ORANGE;  ulFleshTexture = TEXTURE_FLESH_ORANGE;  break; }
        }
      }

      Debris_Spawn(this, this, ulFleshModel, ulFleshTexture, 0, 0, 0, IRnd()%4, 0.5f,
                    FLOAT3D(FRnd()*0.6f+0.2f, FRnd()*0.6f+0.2f, FRnd()*0.6f+0.2f));
    }

    // leave a stain beneath
    LeaveStain(FALSE);

    PlaySound(m_soBody, SOUND_BLOWUP, SOF_3D);

    // hide yourself (must do this after spawning debris)
    SwitchToEditorModel();

    FLOAT fSpeedOrg = en_vCurrentTranslationAbsolute.Length();
    const FLOAT fSpeedMax = 30.0f;
    if (fSpeedOrg>fSpeedMax) {
      en_vCurrentTranslationAbsolute *= fSpeedMax/fSpeedOrg;
    }

//    SetPhysicsFlags(EPF_MODEL_IMMATERIAL);
//    SetCollisionFlags(ECF_IMMATERIAL);
  };

/************************************************************
 *                 OVERRIDEN FUNCTIONS                      *
 ************************************************************/
  /* Entity info */
  void *GetEntityInfo(void)
  {
    switch (m_pstState)
    {
      case PST_STAND: case PST_FALL:
        return &eiPlayerGround;
        break;

      case PST_CROUCH:
        return &eiPlayerCrouch;
        break;

      case PST_SWIM: case PST_DIVE:
        return &eiPlayerSwim;
        break;
    }

    return &eiPlayerGround;
  };

  BOOL ReceiveHealthPickup(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EHealthPickup);

    EHealthPickup &evt = (EHealthPickup&)ee;
    
    INDEX iQuantity = evt.iQuantity * HEALTH_VALUE_MULTIPLIER;
    BOOL bReceived = ReceiveHealth(iQuantity, evt.bOverTopHealth ? MaxHealth() : TopHealth());

    // if value can be changed
    if (bReceived) {
      ItemPicked(TRANS("Health"), evt.iQuantity);
      AddMana(iQuantity);
      return TRUE;
    }

    return FALSE;
  }

  BOOL ReceiveArmorPickup(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EArmorPickup);

    EArmorPickup &evt = (EArmorPickup&)ee;

    INDEX iQuantity = evt.iQuantity * HEALTH_VALUE_MULTIPLIER;
    BOOL bReceived = ReceiveArmor(iQuantity, evt.bOverTopArmor ? MaxArmor() : TopArmor());

    // if value can be changed
    if (bReceived) {
      ItemPicked(TRANS("Armor"), evt.iQuantity); // Display value without fractional part.
      AddMana(evt.iQuantity);
      return TRUE;
    }

    return FALSE;
  }

  BOOL ReceiveShieldPickup(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EShieldPickup);

    const EShieldPickup &evt = (EShieldPickup&)ee;

    INDEX iQuantity = evt.iQuantity * HEALTH_VALUE_MULTIPLIER;
    BOOL bReceived = ReceiveShield(iQuantity, evt.bOverTopValue ? MaxArmor() : TopArmor());

    // if value can be changed
    if (bReceived) {
      ItemPicked(TRANS("Shield"), evt.iQuantity); // Display value without fractional part.
      AddMana(evt.iQuantity);
      return TRUE;
    }

    return FALSE;
  }

  BOOL ReceiveMessageItem(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EMessageItem);

    EMessageItem &evt = (EMessageItem &)ee;

    ReceiveComputerMessage(evt.fnmMessage, CMF_ANALYZE);
    ItemPicked(TRANS("Ancient papyrus"), 0);

    return TRUE;
  }

  BOOL ReceiveAmmoPickup(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EAmmoPickup);

    EAmmoPickup &evt = (EAmmoPickup&)ee;
    BOOL bPicked = GetInventory()->ReceiveAmmo(evt.eAmmoIndex, evt.iQuantity);

    if (bPicked) {
      CTString strKey = GetNameForAmmoIndex(evt.eAmmoIndex);
      ItemPicked(strKey, evt.iQuantity);
      AddMana(evt.iQuantity * GetManaForAmmoIndex(evt.eAmmoIndex) * MANA_AMMO);
    }

    return bPicked;
  }

  BOOL ReceivePackAmmo(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EAmmoPackPickup);

    BOOL bPicked = GetInventory()->ReceivePackAmmo(ee);

    if (bPicked) {
      ItemPicked(TRANS("Ammo pack"), 0);
    }

    return bPicked;
  }

  BOOL ReceiveKeyPickup(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EKeyPickup);

    // don't pick up key if in auto action mode
    if (m_penActionMarker != NULL) {
      return FALSE;
    }

    EKeyPickup &evt = (EKeyPickup&)ee;
    BOOL bPicked = GetInventory()->ReceiveKey(evt.kitType);

    if (evt.kitType == KIT_HAWKWINGS01DUMMY || evt.kitType == KIT_HAWKWINGS02DUMMY
      || evt.kitType == KIT_TABLESDUMMY || evt.kitType == KIT_JAGUARGOLDDUMMY
      || evt.kitType == KIT_ANKHGOLDDUMMY || evt.kitType == KIT_SCARABDUMMY
      || evt.kitType > 31)
    {
      bPicked = TRUE;
    }
    
    if (!bPicked) {
      return FALSE;
    }

    CTString strKey = GetKeyName(evt.kitType);
    ItemPicked(strKey, 0);

    // if in cooperative
    if (GetSP()->sp_bCooperative && !GetSP()->sp_bSinglePlayer) {
      CInfoF(TRANS("^cFFFFFF%s - %s^r\n"), GetPlayerName(), strKey);
    }

    return TRUE;
  }

  BOOL ReceivePowerUp(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EPowerUpPickup);

    EPowerUpPickup &evt = (EPowerUpPickup&)ee;

    CTString strKey = GetStatusEffectNameForIndex(evt.eStatusEffectIndex);
    ItemPicked(strKey, 0);

    // If we have an effect then received it!
    if (evt.eStatusEffectIndex != E_STEF_INVALID) {
      FLOAT fDuration = GetInventory()->GetStatusEffectMaxDuration(evt.eStatusEffectIndex);
      CStatusEffectInstance stef(evt.eStatusEffectIndex, evt.iAmplifier, fDuration);
      GetInventory()->AddStatusEffect(stef, fDuration);
    }

    return TRUE;
  }
  
  BOOL ReceiveTreasurePickup(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_ETreasurePickup);

    ETreasurePickup &evt = (ETreasurePickup&)ee;

    EReceiveScore eScore;
    eScore.iPoints = evt.iQuantity;
    SendEvent(eScore);
    ItemPicked(TRANS("Score"), evt.iQuantity);

    return TRUE;
  }

  BOOL ReceiveGadgetPickup(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EGadgetPickup);
    
    EGadgetPickup &evt = (EGadgetPickup&)ee;
    
    BOOL bPicked = GetInventory()->ReceiveGadget(evt.eGadgetIndex, 1);

    if (bPicked) {
      CTString strKey = GetGadgetNameForIndex(evt.eGadgetIndex);
      ItemPicked(strKey, 1);

      if (GetSP()->sp_bCooperative) {
        EComputerMessage eMsg;
        eMsg.fnmMessage = CTFILENAME("DataMP\\Messages\\Weapons\\seriousbomb.txt");
        this->SendEvent(eMsg);
      }

      if (GetInventory()->GetCurrentGadget() == E_GADGET_INVALID) {
        SelectNewGadget();
      }
    }

    return bPicked;
  }

  BOOL ReceiveWeaponPickup(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EWeaponPickup);

    EWeaponPickup &evt = (EWeaponPickup&)ee;
    BOOL bPicked = GetInventory()->ReceiveWeapon(evt.eWeaponIndex);
    AmmoIndex eAmmoIndex = GetWeaponParams(evt.eWeaponIndex)->GetDefaultAmmoType();
    INDEX iAmmoToAdd = evt.iAmmo > 0 ? evt.iAmmo : GetWeaponParams(evt.eWeaponIndex)->GetDefaultAmmoQty();

    // If only we added new weapon.
    if (bPicked) {
      // precache eventual new weapons
      GetWeapons(E_HAND_MAIN)->Precache();

      CTString strKey = GetWeaponNameForIndex(evt.eWeaponIndex);
      ItemPicked(strKey, 0);

      // send computer message
      if (GetSP()->sp_bCooperative) {
        const CTFileName &fnm = GetWeapons(E_HAND_MAIN)->GetComputerMessageName(evt.eWeaponIndex);

        if (fnm != "") {
          EComputerMessage eMsg;
          eMsg.fnmMessage = fnm;
          SendEvent(eMsg);
        }
      }

      if (eAmmoIndex != E_AMMO_INVALID && iAmmoToAdd > 0) {
        GetInventory()->ReceiveAmmo(eAmmoIndex, iAmmoToAdd);
        
        AddMana(iAmmoToAdd * GetManaForAmmoIndex(eAmmoIndex) * MANA_AMMO);
      }

      // TODO: Fix autoselection. Call it here.

      return TRUE;
    }

    // Give only ammo.
    if (eAmmoIndex != E_AMMO_INVALID && iAmmoToAdd > 0) {
      EAmmoPickup eAmmoPickup;
      eAmmoPickup.eAmmoIndex = eAmmoIndex;
      eAmmoPickup.iQuantity = iAmmoToAdd;
      ReceiveItem(eAmmoPickup);
    }

    return TRUE;
  }

  //! Receive item.
  BOOL ReceiveItem(const CEntityEvent &ee)
  {
    switch (ee.ee_slEvent)
    {
      case EVENTCODE_EHealthPickup: return ReceiveHealthPickup(ee);
      case EVENTCODE_EArmorPickup: return ReceiveArmorPickup(ee);
      case EVENTCODE_EShieldPickup: return ReceiveShieldPickup(ee);
      case EVENTCODE_EMessageItem: return ReceiveMessageItem(ee);
      case EVENTCODE_EWeaponPickup: return ReceiveWeaponPickup(ee);
      case EVENTCODE_EAmmoPickup: return ReceiveAmmoPickup(ee);
      case EVENTCODE_EAmmoPackPickup: return ReceivePackAmmo(ee);
      case EVENTCODE_EKeyPickup: return ReceiveKeyPickup(ee);
      case EVENTCODE_EPowerUpPickup: return ReceivePowerUp(ee);
      case EVENTCODE_EGadgetPickup: return ReceiveGadgetPickup(ee);
      case EVENTCODE_ETreasurePickup: return ReceiveTreasurePickup(ee);
    }

    return FALSE; // nothing picked
  };

  // Change Player view
  void ChangePlayerView()
  {
    // change from eyes to 3rd person
    if (m_iViewState == PVT_PLAYEREYES) {
      // spawn 3rd person view camera
      ASSERT(m_pen3rdPersonView == NULL);

      if (m_pen3rdPersonView == NULL) {
        m_pen3rdPersonView = CreateEntity(GetPlacement(), CLASS_PLAYER_VIEW);
        EViewInit eInit;
        eInit.penOwner = this;
        eInit.penCamera = NULL;
        eInit.vtView = VT_3RDPERSONVIEW;
        eInit.bDeathFixed = FALSE;
        m_pen3rdPersonView->Initialize(eInit);
      }

      m_iViewState = PVT_3RDPERSONVIEW;

    // change from 3rd person to eyes
    } else if (m_iViewState == PVT_3RDPERSONVIEW) {
      m_iViewState = PVT_PLAYEREYES;

      // kill 3rd person view
      if (m_pen3rdPersonView != NULL) {
        m_pen3rdPersonView->SendEvent(EEnd());
        m_pen3rdPersonView = NULL;
      }
    }
  };

  // if computer is pressed
  void ComputerPressed(void)
  {
    // call computer if not holding sniper
//    if (GetPlayerWeapons()->m_iCurrentWeapon!=WEAPON_SNIPER) {
      if (cmp_ppenPlayer==NULL && _pNetwork->IsPlayerLocal(this)) {
        cmp_ppenPlayer = this;
      }

      m_bComputerInvoked = TRUE;
      // clear analyses message
      m_tmAnalyseEnd = 0;
      m_bPendingMessage = FALSE;
      m_tmMessagePlay = 0;
//    }
  }


  // if use is pressed
  void UsePressed(BOOL bOrComputer)
  {
    // cast ray from weapon
    CPlayerWeaponEntity *penWeapons = GetWeapons(E_HAND_MAIN);
    CEntity *pen = penWeapons->m_penRayHit;
    BOOL bSomethingToUse = FALSE;

    // if hit
    if (pen!=NULL) {
      // check switch/messageholder relaying by moving brush
      if (IsOfClass(pen, "Moving Brush")) {
        CMovingBrushEntity &enMovingBrush = (CMovingBrushEntity&)*pen;

        if (enMovingBrush.m_penSwitch != NULL) {
          pen = enMovingBrush.m_penSwitch;
        }
      }

      // if switch and near enough
      if (IsOfClass(pen, "Switch")) {
        CSwitchEntity &enSwitch = (CSwitchEntity&)*pen;

        // if switch is useable
        if (penWeapons->m_fRayHitDistance < enSwitch.GetDistance() && enSwitch.m_bUseable) {
          // send it a trigger event
          SendToTarget(pen, EET_TRIGGER, this);
          bSomethingToUse = TRUE;
        }
      }

      // if analyzable
      if (IsOfClass(pen, "MessageHolder")) {
        CMessageHolderEntity &enMessageHolder = (CMessageHolderEntity&)*pen;

        if (penWeapons->m_fRayHitDistance < enMessageHolder.GetDistance() && enMessageHolder.m_bActive) {
          const CTFileName &fnmMessage = ((CMessageHolderEntity*)&*pen)->m_fnmMessage;

          // if player doesn't have that message in database
          if (!HasMessage(fnmMessage)) {
            // add the message
            ReceiveComputerMessage(fnmMessage, CMF_ANALYZE);
            bSomethingToUse = TRUE;
          }
        }
      }
    }

    // if nothing usable under cursor, and may call computer
    if (!bSomethingToUse && bOrComputer) {
      // call computer
      ComputerPressed();
    }
  }

  void AimDownSightPressed()
  {
    CPlayerWeaponEntity *penWeapon = GetWeapons(E_HAND_MAIN);

    // penWeapon->m_iWantedWeapon==WEAPON_SNIPER) =>
    // make sure that weapon transition is not in progress
    if (penWeapon->CanZoom() && penWeapon->GetCurrentWeapon() == penWeapon->GetWantedWeapon()) {
      if (m_ulFlags&PLF_ISZOOMING) {
        ResetWeaponZoom();
        PlaySound(m_soSniperZoom, SOUND_SILENCE, SOF_3D);
        if (_pNetwork->IsPlayerLocal(this)) {IFeel_StopEffect("SniperZoom");}
      } else {
        EnableWeaponZoom();
        PlaySound(m_soSniperZoom, SOUND_SNIPER_ZOOM, SOF_3D|SOF_LOOP);
        if (_pNetwork->IsPlayerLocal(this)) {IFeel_PlayEffect("SniperZoom");}
      }
    }
  }


/************************************************************
 *                      PLAYER ACTIONS                      *
 ************************************************************/
  void SetGameEnd(void)
  {
    _pNetwork->SetGameFinished();

    // start console for first player possible
    for (INDEX iPlayer=0; iPlayer<GetMaxPlayers(); iPlayer++)
    {
      CEntity *pen = GetPlayerEntity(iPlayer);

      if (pen!=NULL) {
        if (cmp_ppenPlayer==NULL && _pNetwork->IsPlayerLocal(pen)) {
          cmp_ppenPlayer = (CPlayerPawnEntity*)pen;
        }
      }
    }
  }
  // check if game should be finished
  void CheckGameEnd(void)
  {
    BOOL bFinished = FALSE;

    // if time limit is out
    INDEX iTimeLimit = GetSP()->sp_iTimeLimit;
    if (iTimeLimit>0 && _pTimer->CurrentTick()>=iTimeLimit*60.0f) {
      bFinished = TRUE;
    }

    // if frag limit is out
    INDEX iFragLimit = GetSP()->sp_iFragLimit;
    if (iFragLimit>0 && m_psLevelStats.ps_iKills>=iFragLimit) {
      bFinished = TRUE;
    }

    // if score limit is out
    INDEX iScoreLimit = GetSP()->sp_iScoreLimit;
    if (iScoreLimit>0 && m_psLevelStats.ps_iScore>=iScoreLimit) {
      bFinished = TRUE;
    }

    if (bFinished) {
      SetGameEnd();
    }
  }

  // Preapply the action packet for local mouselag elimination
  void PreapplyAction(const CPlayerAction &paAction)
  {
  }

  // Called to apply player action to player entity each tick.
  void ApplyAction(const CPlayerAction &paOriginal, FLOAT tmLatency)
  {
    if (!(m_ulFlags&PLF_INITIALIZED)) { return; }
//    CDebugF("---APPLY: %g\n", paOriginal.pa_aRotation(1));

    // if was not connected
    if (m_ulFlags&PLF_NOTCONNECTED) {
      // set connected state
      SetConnected();
    }
    // mark that the player is connected
    m_ulFlags |= PLF_APPLIEDACTION;

    // make a copy of action for adjustments
    CPlayerAction paAction = paOriginal;
    //CDebugF("applying(%s-%08x): %g\n", GetPredictName(), int(paAction.pa_llCreated),
    //  paAction.pa_vTranslation(3));

    // calculate delta from last received actions
    ANGLE3D aDeltaRotation     = paAction.pa_aRotation    -m_aLastRotation;
    ANGLE3D aDeltaViewRotation = paAction.pa_aViewRotation-m_aLastViewRotation;

    if (m_ulFlags&PLF_ISZOOMING) {
      FLOAT fRotationDamping = GetWeapons(E_HAND_MAIN)->m_fSniperFOV / GetWeapons(E_HAND_MAIN)->m_fSniperMaxFOV;
      aDeltaRotation *= fRotationDamping;
      aDeltaViewRotation *= fRotationDamping;
    }

    //FLOAT3D vDeltaTranslation  = paAction.pa_vTranslation -m_vLastTranslation;
    m_aLastRotation     = paAction.pa_aRotation;
    m_aLastViewRotation = paAction.pa_aViewRotation;
    //m_vLastTranslation  = paAction.pa_vTranslation;
    paAction.pa_aRotation     = aDeltaRotation;
    paAction.pa_aViewRotation = aDeltaViewRotation;
    //paAction.pa_vTranslation  = vDeltaTranslation;

    // adjust rotations per tick
    paAction.pa_aRotation /= _pTimer->TickQuantum;
    paAction.pa_aViewRotation /= _pTimer->TickQuantum;

    // adjust prediction for remote players only
    CEntity *penMe = this;
    if (IsPredictor()) {
      penMe = penMe->GetPredicted();
    }
    SetPredictable(!_pNetwork->IsPlayerLocal(penMe));

    // check for end of game
    if (!IsPredictor()) {
      CheckGameEnd();
    }

    // limit speeds against abusing
    paAction.pa_vTranslation(1) = Clamp(paAction.pa_vTranslation(1), -plr_fSpeedSide,    plr_fSpeedSide);
    paAction.pa_vTranslation(2) = Clamp(paAction.pa_vTranslation(2), -plr_fSpeedUp,      plr_fSpeedUp);
    paAction.pa_vTranslation(3) = Clamp(paAction.pa_vTranslation(3), -plr_fSpeedForward, plr_fSpeedBackward);

    // if speeds are like walking
    if (Abs(paAction.pa_vTranslation(3))< plr_fSpeedForward/1.99f
      &&Abs(paAction.pa_vTranslation(1))< plr_fSpeedSide/1.99f) {
      // don't allow falling
      en_fStepDnHeight = 1.5f;

    // if speeds are like running
    } else {
      // allow falling
      en_fStepDnHeight = -1;
    }

    // limit diagonal speed against abusing
    FLOAT3D &v = paAction.pa_vTranslation;
    FLOAT fDiag = Sqrt(v(1)*v(1)+v(3)*v(3));
    if (fDiag>0.01f) {
      FLOAT fDiagLimited = Min(fDiag, plr_fSpeedForward);
      FLOAT fFactor = fDiagLimited/fDiag;
      v(1)*=fFactor;
      v(3)*=fFactor;
    }

    ulButtonsNow = paAction.pa_ulButtons;
    ulButtonsBefore = m_ulLastButtons;
    ulNewButtons = ulButtonsNow&~ulButtonsBefore;
    ulReleasedButtons = (~ulButtonsNow)&(ulButtonsBefore);

    m_ulLastButtons = ulButtonsNow;         // remember last buttons
    en_plLastViewpoint = en_plViewpoint;    // remember last view point for lerping

    // sniper zooming
    CPlayerWeaponEntity *penWeapon = GetWeapons(E_HAND_MAIN);
    if (penWeapon->CanZoom())
    {
      if (bAimButtonHeld && m_ulFlags&PLF_ISZOOMING)
      {
        penWeapon->m_fSniperFOVlast = penWeapon->m_fSniperFOV;
        penWeapon->m_fSniperFOV -= penWeapon->m_fSnipingZoomSpeed;

        if (penWeapon->m_fSniperFOV < penWeapon->m_fSniperMinFOV)
        {
          penWeapon->m_fSniperFOVlast = penWeapon->m_fSniperFOV = penWeapon->m_fSniperMinFOV;
          PlaySound(m_soSniperZoom, SOUND_SILENCE, SOF_3D);
          if (_pNetwork->IsPlayerLocal(this)) {IFeel_StopEffect("SniperZoom");}
        }
      }

      if (ulReleasedButtons & PLACT_AIM_DOWN_SIGHT)
      {
         penWeapon->m_fSniperFOVlast = penWeapon->m_fSniperFOV;
         PlaySound(m_soSniperZoom, SOUND_SILENCE, SOF_3D);
         if (_pNetwork->IsPlayerLocal(this)) {IFeel_StopEffect("SniperZoom");}
      }
    }

    // if alive
    if (GetFlags() & ENF_ALIVE) {
      // if not in auto-action mode
      if (m_penActionMarker==NULL) {
        // apply actions
        AliveActions(paAction);
      // if in auto-action mode
      } else {
        // do automatic actions
        AutoActions(paAction);
      }

    // if not alive rotate camera view and rebirth on fire
    } else {
      DeathActions(paAction);
    }

    if (Abs(_pTimer->CurrentTick()-m_tmAnalyseEnd)<_pTimer->TickQuantum*2) {
      m_tmAnalyseEnd = 0;
      m_bPendingMessage = TRUE;
      m_tmMessagePlay = 0;
    }

    if (m_bPendingMessage && !IsFuss()) {
      m_bPendingMessage = FALSE;
      m_tmMessagePlay = _pTimer->CurrentTick()+1.0f;
      m_tmAnimateInbox = _pTimer->CurrentTick();
    }

    if (Abs(_pTimer->CurrentTick()-m_tmMessagePlay)<_pTimer->TickQuantum*2) {
      m_bPendingMessage = FALSE;
      m_tmAnalyseEnd = 0;

      if (!m_bComputerInvoked && GetSP()->sp_bSinglePlayer) {
        PrintCenterMessage(this, this,
          TRANS("Press USE to read the message!"), 5.0f, MSS_NONE);
      }
    }

    // wanna cheat a bit?
    if (_pNetwork->IsPlayerLocal(this) && _pNetwork->IsServer() && m_penActionMarker == NULL) {
      RequestCheats();
    }
    
    DoCheats();



    // check if highscore has changed
    CheckHighScore();
  };


  // Called when player is disconnected
  void Disconnect(void)
  {
    // remember name
    m_strName = GetPlayerName();

    // make main loop exit
    SendEvent(EDisconnected()); // make main loop exit
  };

  // Called when player character is changed
  void CharacterChanged(const CPlayerCharacter &pcOrg, const CPlayerCharacter &pcNew)
  {

    ValidateCharacter();

    // if appearance changed
    CPlayerSettings *ppsOrg = (CPlayerSettings *)pcOrg.pc_aubAppearance;
    CPlayerSettings *ppsNew = (CPlayerSettings *)pcNew.pc_aubAppearance;

    if (memcmp(ppsOrg->ps_achModelFile, ppsNew->ps_achModelFile, sizeof(ppsOrg->ps_achModelFile))!=0) {
      // update your real appearance if possible
      CTString strNewLook;
      BOOL bSuccess = SetPlayerAppearance(&m_moRender, GetCharacter(), strNewLook, FALSE);

      // if succeeded
      if (bSuccess) {
        ParseGender(strNewLook);
        // report that
        CInfoF(TRANS("%s now appears as %s\n"), pcNew.GetNameForPrinting(), strNewLook);

      // if failed
      } else {
        // report that
        CWarningF(TRANS("Cannot change appearance for %s: setting '%s' is unavailable\n"),
          pcNew.GetNameForPrinting(), (const char*)ppsNew->GetModelFilename());
      }

      // attach weapon to new appearance
      GetPlayerAnimator()->SyncWeapon();
    }

    BOOL b3RDPersonOld = ppsOrg->ps_ulFlags&PSF_PREFER3RDPERSON;
    BOOL b3RDPersonNew = ppsNew->ps_ulFlags&PSF_PREFER3RDPERSON;
    if ((b3RDPersonOld && !b3RDPersonNew && m_iViewState==PVT_3RDPERSONVIEW)
      ||(b3RDPersonNew && !b3RDPersonOld && m_iViewState==PVT_PLAYEREYES) ) {
      ChangePlayerView();
    }
  };

  // Alive actions
  void AliveActions(const CPlayerAction &pa)
  {
    CPlayerAction paAction = pa;

    // if camera is active
    if (m_penCamera!=NULL) {
      // ignore keyboard/mouse/joystick commands
      paAction.pa_vTranslation  = FLOAT3D(0, 0, 0);
      paAction.pa_aRotation     = ANGLE3D(0,0,0);
      paAction.pa_aViewRotation = ANGLE3D(0,0,0);

      // if fire or use is pressed
      if (ulNewButtons&(PLACT_FIRE|PLACT_USE)) {
        // stop camera
        m_penCamera=NULL;
      }

    } else {
      ButtonsActions(paAction);
    }

    // do the actions
    ActiveActions(paAction);

    // if less than few seconds elapsed since last damage
    FLOAT tmSinceWounding = _pTimer->CurrentTick() - m_tmWoundedTime;

    if (tmSinceWounding<4.0f) {
      m_iDamageAmount *= 1.0f - tmSinceWounding/4.0f; // decrease damage ammount
    } else {
      m_iDamageAmount = 0.0f; // reset damage ammount
    }
  }

  // Auto-actions
  void AutoActions(const CPlayerAction &pa)
  {
    // if fire, use or computer is pressed
    if (ulNewButtons&(PLACT_FIRE|PLACT_USE|PLACT_COMPUTER)) {
      if (m_penCamera!=NULL) {
        CEntity *penOnBreak = ((CCameraEntity&)*m_penCamera).m_penOnBreak;
        if (penOnBreak!=NULL) {
          SendToTarget(penOnBreak, EET_TRIGGER, this);
        }
      }
    }

    CPlayerAction paAction = pa;

    // ignore keyboard/mouse/joystick commands
    paAction.pa_vTranslation  = FLOAT3D(0, 0, 0);
    paAction.pa_aRotation     = ANGLE3D(0,0,0);
    paAction.pa_aViewRotation = ANGLE3D(0,0,0);

    // if moving towards the marker is enabled
    if (m_fAutoSpeed>0) {
      FLOAT3D vDelta = m_penActionMarker->GetPlacement().pl_PositionVector - GetPlacement().pl_PositionVector;
      FLOAT fDistance = vDelta.Length();

      if (fDistance>0.1f) {
        vDelta/=fDistance;
        ANGLE aDH = GetRelativeHeading(vDelta);

        // if should hit the marker exactly
        FLOAT fSpeed = m_fAutoSpeed;
        if (GetActionMarker()->m_paaAction==PAA_RUNANDSTOP) {
          fSpeed = Min(fSpeed, fDistance/_pTimer->TickQuantum); // adjust speed
        }

        // adjust rotation
        if (Abs(aDH)>5.0f) {
          if (fSpeed>m_fAutoSpeed-0.1f) {
            aDH = Clamp(aDH, -30.0f, 30.0f);
          }

          paAction.pa_aRotation = ANGLE3D(aDH/_pTimer->TickQuantum,0,0);
        }

        // set forward speed
        paAction.pa_vTranslation = FLOAT3D(0,0,-fSpeed);
      }
    } else {
      paAction.pa_vTranslation = m_vAutoSpeed;
    }

    CPlayerActionMarkerEntity *ppam = GetActionMarker();
    ASSERT(ppam != NULL);
    if (ppam->m_paaAction == PAA_LOGO_FIRE_MINIGUN || ppam->m_paaAction == PAA_LOGO_FIRE_INTROSE)
    {
      if (m_tmMinigunAutoFireStart != -1)
      {
        FLOAT tmDelta = _pTimer->CurrentTick()-m_tmMinigunAutoFireStart;
        FLOAT aDH=0.0f;
        FLOAT aDP=0.0f;

        if (tmDelta>=0.0f && tmDelta<=0.75f) {
          aDH = 0.0f;

        } else if (tmDelta>=0.75f) {
          FLOAT fDT = tmDelta-0.75f;
          aDH = 1.0f*cos(fDT+PI/2.0f);
          aDP = 0.5f*cos(fDT);
        }

        if (ppam->m_paaAction == PAA_LOGO_FIRE_INTROSE)
        {
          FLOAT fRatio=CalculateRatio(tmDelta,0.25,5,0.1f,0.1f);
          aDP=2.0f*sin(tmDelta*200.0f)*fRatio;

          if (tmDelta>2.5f)
          {
            aDP+=(tmDelta-2.5f)*4.0f;
          }
        }

        paAction.pa_aRotation = ANGLE3D(aDH/_pTimer->TickQuantum, aDP/_pTimer->TickQuantum,0);
      }
    }

    // do the actions
    if (!(m_ulFlags&PLF_AUTOMOVEMENTS)) {
      ActiveActions(paAction);
    }
  }

  void GetLerpedWeaponPosition(FLOAT3D vRel, CPlacement3D &pl)
  {
    pl = CPlacement3D(vRel, ANGLE3D(0,0,0));
    CPlacement3D plView;
    _bDiscard3rdView=GetViewEntity()!=this;
    GetLerpedAbsoluteViewPlacement(plView);
    pl.RelativeToAbsolute(plView);
  }

  void SpawnBubbles(INDEX ctBubbles)
  {
    for (INDEX iBouble=0; iBouble<ctBubbles; iBouble++)
    {
      FLOAT3D vRndRel = FLOAT3D((FRnd()-0.5f)*0.25f, -0.25f, -0.5f+FRnd()/10.0f);
      ANGLE3D aDummy = ANGLE3D(0,0,0);
      CPlacement3D plMouth = CPlacement3D(vRndRel, aDummy);

      plMouth.RelativeToAbsolute(en_plViewpoint);
      plMouth.RelativeToAbsolute(GetPlacement());
      FLOAT3D vRndSpd = FLOAT3D((FRnd()-0.5f)*0.25f, (FRnd()-0.5f)*0.25f, (FRnd()-0.5f)*0.25f);
      AddBouble(plMouth.pl_PositionVector, vRndSpd);
    }
  }

  void PlayPowerUpSound(void)
  {
    m_soPowerUpBeep.Set3DParameters(50.0f, 10.0f, 4.0f, 1.0f);
    PlaySound(m_soPowerUpBeep, SOUND_POWERUP_BEEP, SOF_3D|SOF_VOLUMETRIC|SOF_LOCAL);
  }

  void ActiveActions(const CPlayerAction &paAction)
  {
    CPlayerWeaponEntity *penWeapons = GetWeapons(E_HAND_MAIN);
    
    // translation
    FLOAT3D vTranslation = paAction.pa_vTranslation;

    // turbo speed cheat
    if (m_ulCheatFlags & CHT_FLAG_TURBO) {
      vTranslation *= 2.0F;
    }

    vTranslation += vTranslation * penWeapons->GetMovementSpeedBonus();

    // enable faster moving (but not higher jumping!) if having SerousSpeed powerup
    //const TIME tmDelta = GetInventory()->GetStatusEffect(E_STEF_SPEED);//m_tmSeriousSpeed - _pTimer->CurrentTick();
    INDEX iSpeedAmplifier = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_SPEED);
    INDEX iSlownessAmplifier = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_SLOWNESS);
    INDEX iJumpBoostAmplifier = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_JUMP_BOOST)
;
    if (iSpeedAmplifier > 0) {
      vTranslation(1) += vTranslation(1) * 0.2F * iSpeedAmplifier;
      vTranslation(3) += vTranslation(3) * 0.2F * iSpeedAmplifier;
    }
    
    if (iSlownessAmplifier > 0) {
      FLOAT3D vTransDelta(0.0F, 0.0F, 0.0F);
      vTransDelta(1) = -vTranslation(1) * 0.15F * iSlownessAmplifier;
      vTransDelta(3) = -vTranslation(3) * 0.15F * iSlownessAmplifier;

      vTranslation(1) = vTranslation(1) > 0 ? ClampDn(vTranslation(1) + vTransDelta(1), 0.0F) : ClampUp(vTranslation(1) + vTransDelta(1), 0.0F);
      vTranslation(3) = vTranslation(3) > 0 ? ClampDn(vTranslation(3) + vTransDelta(3), 0.0F) : ClampUp(vTranslation(3) + vTransDelta(3), 0.0F);
    }
    
    if (iJumpBoostAmplifier > 0) {
      vTranslation(2) += vTranslation(2) * 0.2F * iJumpBoostAmplifier;
    }

    en_fAcceleration = plr_fAcceleration;
    en_fDeceleration = plr_fDeceleration;
    if (!GetSP()->sp_bCooperative)
    {
      vTranslation(1) *= 1.35f;
      vTranslation(3) *= 1.35f;
    //en_fDeceleration *= 0.8f;
    }

    CContentType &ctUp = GetWorld()->wo_actContentTypes[en_iUpContent];
    CContentType &ctDn = GetWorld()->wo_actContentTypes[en_iDnContent];
    PlayerState pstWanted = PST_STAND;
    BOOL bUpSwimable = (ctUp.ct_ulFlags&CTF_SWIMABLE) && en_fImmersionFactor<=0.99f;
    BOOL bDnSwimable = (ctDn.ct_ulFlags&CTF_SWIMABLE) && en_fImmersionFactor>=0.5f;

    // if considerably inside swimable content
    if (bUpSwimable || bDnSwimable) {
      // allow jumping
      m_ulFlags|=PLF_JUMPALLOWED;
      //CDebugF("swimable %f", en_fImmersionFactor);

      // if totaly inside
      if (en_fImmersionFactor>=0.99f || bUpSwimable) {
        // want to dive
        pstWanted = PST_DIVE;
      // if only partially inside
      } else {
        pstWanted = PST_SWIM; // want to swim
      }

    // if not in swimable content
    } else {
      // if has reference
      if (en_penReference!=NULL) {
        m_fFallTime = 0.0f; // reset fall timer

      // if no reference
      } else {
        m_fFallTime += _pTimer->TickQuantum; // increase fall time
      }

      // if not wanting to jump
      if (vTranslation(2)<0.1f) {
        // allow jumping
        m_ulFlags|=PLF_JUMPALLOWED;
      }

      // if falling then wants to fall
      if (m_fFallTime >= 0.5f) {
        pstWanted = PST_FALL;

      // if not falling
      } else {
        // if holding down and really not in air
        if (vTranslation(2)<-0.01f/* && m_fFallTime<0.5f*/) {
          // wants to crouch
          pstWanted = PST_CROUCH;
        // if not holding down
        } else {
          // wants to stand
          pstWanted = PST_STAND;
        }
      }
    }
    //CDebugF("c - %s w - %s", NameForState(m_pstState), NameForState(pstWanted));

    // flying mode - rotate whole player
    if (!(GetPhysicsFlags()&EPF_TRANSLATEDBYGRAVITY)) {
      SetDesiredRotation(paAction.pa_aRotation);
      StartModelAnim(PLAYER_ANIM_STAND, AOF_LOOPING|AOF_NORESTART);
      SetDesiredTranslation(vTranslation);

    // normal mode
    } else {
      PlayerState pstOld = m_pstState;

      // if different state needed
      if (pstWanted!=m_pstState) {
        // check state wanted
        switch (pstWanted)
        {
          // if wanting to stand
          case PST_STAND: {
            // if can stand here
            if (ChangeCollisionBoxIndexNow(PLAYER_COLLISION_BOX_STAND)) {
              en_plViewpoint.pl_PositionVector(2) = plr_fViewHeightStand;

          if (m_pstState==PST_CROUCH) {
                GetPlayerAnimator()->Rise();
              } else {
                GetPlayerAnimator()->Stand();
              }

              m_pstState = PST_STAND;
            }
          } break;

          // if wanting to crouch
          case PST_CROUCH: {
            // if can crouch here
            if (ChangeCollisionBoxIndexNow(PLAYER_COLLISION_BOX_CROUCH)) {
              m_pstState = PST_CROUCH;
              en_plViewpoint.pl_PositionVector(2) = plr_fViewHeightCrouch;
              GetPlayerAnimator()->Crouch();
            }
          } break;

          // if wanting to swim
          case PST_SWIM: {
            // if can swim here
            if (ChangeCollisionBoxIndexNow(PLAYER_COLLISION_BOX_SWIMSMALL)) {
              ChangeCollisionBoxIndexWhenPossible(PLAYER_COLLISION_BOX_SWIM);
              m_pstState = PST_SWIM;
              en_plViewpoint.pl_PositionVector(2) = plr_fViewHeightSwim;
              GetPlayerAnimator()->Swim();
              m_fSwimTime = _pTimer->CurrentTick();
            }
          } break;

          // if wanting to dive
          case PST_DIVE: {
            // if can dive here
            if (ChangeCollisionBoxIndexNow(PLAYER_COLLISION_BOX_SWIMSMALL)) {
              ChangeCollisionBoxIndexWhenPossible(PLAYER_COLLISION_BOX_SWIM);
              m_pstState = PST_DIVE;
              en_plViewpoint.pl_PositionVector(2) = plr_fViewHeightDive;
              GetPlayerAnimator()->Swim();
            }
          } break;

          // if wanting to fall
          case PST_FALL: {
            // if can fall here
            if (ChangeCollisionBoxIndexNow(PLAYER_COLLISION_BOX_STAND)) {
              m_pstState = PST_FALL;
              en_plViewpoint.pl_PositionVector(2) = plr_fViewHeightStand;
              GetPlayerAnimator()->Fall();
            }
          } break;
        }
      }

      // if state changed
      if (m_pstState!=pstOld) {
        // check water entering/leaving
        BOOL bWasInWater = (pstOld==PST_SWIM||pstOld==PST_DIVE);
        BOOL bIsInWater = (m_pstState==PST_SWIM||m_pstState==PST_DIVE);

        // if entered water
        if (bIsInWater && !bWasInWater) {
          PlaySound(m_soBody, GenderSound(SOUND_WATER_ENTER), SOF_3D);

        // if left water
        } else if (!bIsInWater && bWasInWater) {
          PlaySound(m_soBody, GenderSound(SOUND_WATER_LEAVE), SOF_3D);
          m_tmOutOfWater = _pTimer->CurrentTick();
          //CInfoF("gotout ");

        // if in water
        } else if (bIsInWater) {
          // if dived in
          if (pstOld==PST_SWIM && m_pstState == PST_DIVE) {
            PlaySound(m_soFootL, GenderSound(SOUND_DIVEIN), SOF_3D);
            if (_pNetwork->IsPlayerLocal(this)) {IFeel_PlayEffect("DiveIn");}
            m_bMoveSoundLeft = TRUE;
            m_tmMoveSound = _pTimer->CurrentTick();

          // if dived out
          } else if (m_pstState==PST_SWIM && pstOld==PST_DIVE) {
            PlaySound(m_soFootL, GenderSound(SOUND_DIVEOUT), SOF_3D);
            m_bMoveSoundLeft = TRUE;
            m_tmMoveSound = _pTimer->CurrentTick();
          }
        }

        // if just fell to ground
        if (pstOld==PST_FALL && (m_pstState==PST_STAND||m_pstState==PST_CROUCH)) {
          PlaySound(m_soFootL, GenderSound(SOUND_LAND), SOF_3D);
          if (_pNetwork->IsPlayerLocal(this)) {IFeel_PlayEffect("Land");}
        }

        // change ambience sounds
        if (m_pstState==PST_DIVE) {
          m_soLocalAmbientLoop.Set3DParameters(50.0f, 10.0f, 0.25f, 1.0f);
          PlaySound(m_soLocalAmbientLoop, SOUND_WATERAMBIENT,
            SOF_LOOP|SOF_3D|SOF_VOLUMETRIC|SOF_LOCAL);

        } else if (pstOld==PST_DIVE) {
          m_soLocalAmbientLoop.Stop();
        }
      }

      // if just jumped
      if (en_tmJumped+_pTimer->TickQuantum>=_pTimer->CurrentTick() &&
          en_tmJumped<=_pTimer->CurrentTick() && en_penReference==NULL) {
        // play jump sound
        SetDefaultMouthPitch();
        PlaySound(m_soMouth, GenderSound(SOUND_JUMP), SOF_3D);
        if (_pNetwork->IsPlayerLocal(this)) {IFeel_PlayEffect("Jump");}

        // disallow jumping
        m_ulFlags&=~PLF_JUMPALLOWED;
      }

      // set density
      if (m_pstState == PST_SWIM || pstWanted == PST_SWIM || (pstWanted == PST_DIVE && m_pstState != pstWanted)) {
        en_fDensity = 500.0f;  // lower density than water
      } else {
        en_fDensity = 1000.0f; // same density as water
      }

      if (_pTimer->CurrentTick()>=m_tmNextAmbientOnce)
      {
        if (m_pstState == PST_DIVE)
        {
          PlaySound(m_soLocalAmbientOnce, SOUND_WATERBUBBLES,
            SOF_3D|SOF_VOLUMETRIC|SOF_LOCAL);
          m_soLocalAmbientOnce.Set3DParameters(25.0f, 5.0f, 2.0f, Lerp(0.5f, 1.5f, FRnd()) );
          SpawnBubbles(5+INDEX(FRnd()*5));
        }

        m_tmNextAmbientOnce = _pTimer->CurrentTick()+5.0f+FRnd();
      }

      // if crouching
      if (m_pstState == PST_CROUCH) {
        // go slower
        vTranslation /= 2.5f;
        // don't go down
        vTranslation(2) = 0.0f;
      }

      // if diving
      if (m_pstState == PST_DIVE) {
        // translate up/down with view pitch
        FLOATmatrix3D mPitch;
        MakeRotationMatrixFast(mPitch, FLOAT3D(0,en_plViewpoint.pl_OrientationAngle(2),0));
        FLOAT fZ = vTranslation(3);
        vTranslation(3) = 0.0f;
        vTranslation += FLOAT3D(0,0,fZ)*mPitch;

      // if swimming
      } else if (m_pstState == PST_SWIM) {
        // translate down with view pitch if large
        FLOATmatrix3D mPitch;
        FLOAT fPitch = en_plViewpoint.pl_OrientationAngle(2);

        if (fPitch>-30.0f) {
          fPitch = 0;
        }

        MakeRotationMatrixFast(mPitch, FLOAT3D(0,fPitch,0));
        FLOAT fZ = vTranslation(3);
        vTranslation(3) = 0.0f;
        vTranslation += FLOAT3D(0,0,fZ)*mPitch;
      }

      // if swimming or diving
      if (m_pstState == PST_SWIM || m_pstState == PST_DIVE) {
        // up/down is slower than on ground
        vTranslation(2)*=0.5f;
      }

      // if just started swimming
      if (m_pstState == PST_SWIM && _pTimer->CurrentTick()<m_fSwimTime+0.5f
        ||_pTimer->CurrentTick()<m_tmOutOfWater+0.5f) {
        // no up/down change
        vTranslation(2)=0;
        //CInfoF(" noup");
      }

      //CInfoF("\n");

      // disable consecutive jumps
      if (!(m_ulFlags&PLF_JUMPALLOWED) && vTranslation(2)>0) {
        vTranslation(2) = 0.0f;
      }

      // set translation
      SetDesiredTranslation(vTranslation);

      // set pitch and banking from the normal rotation into the view rotation
      en_plViewpoint.Rotate_HPB(ANGLE3D(
        (ANGLE)((FLOAT)paAction.pa_aRotation(1)*_pTimer->TickQuantum),
        (ANGLE)((FLOAT)paAction.pa_aRotation(2)*_pTimer->TickQuantum),
        (ANGLE)((FLOAT)paAction.pa_aRotation(3)*_pTimer->TickQuantum)));

      // pitch and banking boundaries
      RoundViewAngle(en_plViewpoint.pl_OrientationAngle(2), PITCH_MAX);
      RoundViewAngle(en_plViewpoint.pl_OrientationAngle(3), BANKING_MAX);

      // translation rotate player for heading
      if (vTranslation.Length() > 0.1f) {
        SetDesiredRotation(ANGLE3D(en_plViewpoint.pl_OrientationAngle(1)/_pTimer->TickQuantum, 0.0f, 0.0f));

        if (m_ulFlags&PLF_VIEWROTATIONCHANGED) {
          m_ulFlags&=~PLF_VIEWROTATIONCHANGED;
          FLOATmatrix3D mViewRot;
          MakeRotationMatrixFast(mViewRot, ANGLE3D(en_plViewpoint.pl_OrientationAngle(1),0,0));
          FLOAT3D vTransRel = vTranslation*mViewRot;
          SetDesiredTranslation(vTransRel);
        }

        en_plViewpoint.pl_OrientationAngle(1) = 0.0f;

      // rotate head, body and legs
      } else {
        m_ulFlags |= PLF_VIEWROTATIONCHANGED;
        SetDesiredRotation(ANGLE3D(0.0f, 0.0f, 0.0f));
        ANGLE aDiff = en_plViewpoint.pl_OrientationAngle(1) - HEADING_MAX;

        if (aDiff > 0.0f) {
          SetDesiredRotation(ANGLE3D(aDiff/_pTimer->TickQuantum, 0.0f, 0.0f));
        }

        aDiff = en_plViewpoint.pl_OrientationAngle(1) + HEADING_MAX;

        if (aDiff < 0.0f) {
          SetDesiredRotation(ANGLE3D(aDiff/_pTimer->TickQuantum, 0.0f, 0.0f));
        }

        RoundViewAngle(en_plViewpoint.pl_OrientationAngle(1), HEADING_MAX);
      }

      // play moving sounds
      FLOAT fWantSpeed = en_vDesiredTranslationRelative.Length();
      FLOAT fGoesSpeed = en_vCurrentTranslationAbsolute.Length();
      BOOL bOnGround = (m_pstState == PST_STAND)||(m_pstState == PST_CROUCH);
      BOOL bRunning = bOnGround && fWantSpeed>5.0f && fGoesSpeed>5.0f;
      BOOL bWalking = bOnGround && !bRunning && fWantSpeed>2.0f && fGoesSpeed>2.0f;
      BOOL bSwimming = (m_pstState == PST_SWIM) && fWantSpeed>2.0f && fGoesSpeed>2.0f;
      BOOL bDiving = (m_pstState == PST_DIVE) && fWantSpeed>2.0f && fGoesSpeed>2.0f;
      TIME tmNow = _pTimer->CurrentTick();
      INDEX iSoundWalkL = SOUND_WALK_L;
      INDEX iSoundWalkR = SOUND_WALK_R;

      if ((ctDn.ct_ulFlags&CTF_SWIMABLE) && en_fImmersionFactor>=0.1f) {
        iSoundWalkL = SOUND_WATERWALK_L;
        iSoundWalkR = SOUND_WATERWALK_R;

      } else if (en_pbpoStandOn!=NULL &&
        en_pbpoStandOn->bpo_bppProperties.bpp_ubSurfaceType==SURFACE_SAND) {
        iSoundWalkL = SOUND_WALK_SAND_L;
        iSoundWalkR = SOUND_WALK_SAND_R;

      } else if (en_pbpoStandOn!=NULL &&
        en_pbpoStandOn->bpo_bppProperties.bpp_ubSurfaceType==SURFACE_RED_SAND) {
        iSoundWalkL = SOUND_WALK_SAND_L;
        iSoundWalkR = SOUND_WALK_SAND_R;

      } else if (en_pbpoStandOn!=NULL &&
        (en_pbpoStandOn->bpo_bppProperties.bpp_ubSurfaceType==SURFACE_GRASS ||
         en_pbpoStandOn->bpo_bppProperties.bpp_ubSurfaceType==SURFACE_GRASS_SLIDING ||
         en_pbpoStandOn->bpo_bppProperties.bpp_ubSurfaceType==SURFACE_GRASS_NOIMPACT )) {
        iSoundWalkL = SOUND_WALK_GRASS_L;
        iSoundWalkR = SOUND_WALK_GRASS_R;

      } else if (en_pbpoStandOn!=NULL &&
        en_pbpoStandOn->bpo_bppProperties.bpp_ubSurfaceType==SURFACE_WOOD) {
        iSoundWalkL = SOUND_WALK_WOOD_L;
        iSoundWalkR = SOUND_WALK_WOOD_R;

      } else if (en_pbpoStandOn!=NULL &&
        en_pbpoStandOn->bpo_bppProperties.bpp_ubSurfaceType==SURFACE_SNOW) {
        iSoundWalkL = SOUND_WALK_SNOW_L;
        iSoundWalkR = SOUND_WALK_SNOW_R;
      } else {
      }

      iSoundWalkL+=m_iGender*GENDEROFFSET;
      iSoundWalkR+=m_iGender*GENDEROFFSET;

      if (bRunning) {
        if (tmNow>m_tmMoveSound+plr_fRunSoundDelay) {
          m_tmMoveSound = tmNow;
          m_bMoveSoundLeft = !m_bMoveSoundLeft;

          if (m_bMoveSoundLeft) {
            PlaySound(m_soFootL, iSoundWalkL, SOF_3D);
          } else {
            PlaySound(m_soFootR, iSoundWalkR, SOF_3D);
          }
        }

      } else if (bWalking) {
        if (tmNow>m_tmMoveSound+plr_fWalkSoundDelay) {
          m_tmMoveSound = tmNow;
          m_bMoveSoundLeft = !m_bMoveSoundLeft;

          if (m_bMoveSoundLeft) {
            PlaySound(m_soFootL, iSoundWalkL, SOF_3D);
          } else {
            PlaySound(m_soFootR, iSoundWalkR, SOF_3D);
          }
        }

      } else if (bDiving) {
        if (tmNow>m_tmMoveSound+plr_fDiveSoundDelay) {
          m_tmMoveSound = tmNow;
          m_bMoveSoundLeft = !m_bMoveSoundLeft;

          if (m_bMoveSoundLeft) {
            PlaySound(m_soFootL, GenderSound(SOUND_DIVE_L), SOF_3D);
          } else {
            PlaySound(m_soFootR, GenderSound(SOUND_DIVE_R), SOF_3D);
          }
        }

      } else if (bSwimming) {
        if (tmNow>m_tmMoveSound+plr_fSwimSoundDelay) {
          m_tmMoveSound = tmNow;
          m_bMoveSoundLeft = !m_bMoveSoundLeft;

          if (m_bMoveSoundLeft) {
            PlaySound(m_soFootL, GenderSound(SOUND_SWIM_L), SOF_3D);
          } else {
            PlaySound(m_soFootR, GenderSound(SOUND_SWIM_R), SOF_3D);
          }
        }
      }

      // if player is almost out of air
      TIME tmBreathDelay = tmNow-en_tmLastBreathed;

      if (en_tmMaxHoldBreath-tmBreathDelay<20.0f) {
        // play drowning sound once in a while
        if (m_tmMouthSoundLast+2.0f<tmNow) {
          m_tmMouthSoundLast = tmNow;
          SetRandomMouthPitch(0.9f, 1.1f);
          PlaySound(m_soMouth, GenderSound(SOUND_DROWN), SOF_3D);
        }
      }

      // animate player
      GetPlayerAnimator()->AnimatePlayer();
    }
  };

  // Round view angle
  void RoundViewAngle(ANGLE &aViewAngle, ANGLE aRound)
  {
    if (aViewAngle > aRound) {
      aViewAngle = aRound;
    }

    if (aViewAngle < -aRound) {
      aViewAngle = -aRound;
    }
  };
  

  BOOL IsGadgetUsable(GadgetIndex eGadgetIndex)
  {
    switch (eGadgetIndex)
    {
      case E_GADGET_NUKE: return TRUE;
      case E_GADGET_STIMPACK: return TRUE;
      case E_GADGET_MEDKIT: return TRUE;
      case E_GADGET_SHIELD_CELL: return TRUE;
      case E_GADGET_SHIELD_BATTERY: return TRUE;
    }
    
    return FALSE;
  }

  void SelectNewGadget()
  {
    for (INDEX i = E_GADGET_INVALID + 1; i < E_GADGET_MAX; i++)
    {
      const GadgetIndex eGadgetIndex = (GadgetIndex)i;
      
      if (IsGadgetUsable(eGadgetIndex) && GetInventory()->GetGadgetQty(eGadgetIndex)) {
        GetInventory()->SetCurrentGadget(eGadgetIndex);
        break;
      }
    }
  }

  GadgetIndex FindGadgetInDirection(INDEX iDir)
  {
    INDEX wtOrg = GetInventory()->GetCurrentGadget();
    INDEX wti = wtOrg;
    
    for (INDEX i = 0; i < E_GADGET_MAX; i++)
    {
      (INDEX&)wti += iDir;
      if (wti < E_GADGET_INVALID + 1) {
        wti = E_GADGET_MAX - 1;
      }

      if (wti > E_GADGET_MAX - 1) {
        wti = E_GADGET_INVALID + 1;
      }

      if (wti == wtOrg) {
        break;
      }

      GadgetIndex eGadgetIndex = (GadgetIndex)wti;

      if (IsGadgetUsable(eGadgetIndex) && GetInventory()->GetGadgetQty(eGadgetIndex) > 0) {
        return eGadgetIndex;
      }
    }
    
    return GetInventory()->GetCurrentGadget();
  }
  
  void UseGadget(GadgetIndex eGadgetIndex)
  {
    const INDEX ctGadgets = GetInventory()->GetGadgetQty(eGadgetIndex);
    
    if (ctGadgets > 0 && m_tmGadgetActivated + 4.0f < _pTimer->CurrentTick()) {
      
      BOOL bGadgetUsed = FALSE;
      
      if (eGadgetIndex == E_GADGET_NUKE) {
        ESeriousBomb esb;
        esb.penOwner = this;
        CEntityPointer penBomb = CreateEntity(GetPlacement(), CLASS_SERIOUSBOMB);
        penBomb->Initialize(esb);
        bGadgetUsed = TRUE;

      } else if (eGadgetIndex == E_GADGET_STIMPACK) {
        EHealthPickup evt;
        evt.iQuantity = 25;
        evt.bOverTopHealth = FALSE;

        if (ReceiveItem(evt)) {
          bGadgetUsed = TRUE;
        }

      } else if (eGadgetIndex == E_GADGET_MEDKIT) {
        EHealthPickup evt;
        evt.iQuantity = 100;
        evt.bOverTopHealth = FALSE;

        if (ReceiveItem(evt)) {
          bGadgetUsed = TRUE;
        }

      } else if (eGadgetIndex == E_GADGET_SHIELD_CELL) {
        EShieldPickup evt;
        evt.iQuantity = 25;
        evt.bOverTopValue = FALSE;

        if (ReceiveItem(evt)) {
          bGadgetUsed = TRUE;
        }

      } else if (eGadgetIndex == E_GADGET_SHIELD_BATTERY) {
        EShieldPickup evt;
        evt.iQuantity = 100;
        evt.bOverTopValue = FALSE;

        if (ReceiveItem(evt)) {
          bGadgetUsed = TRUE;
        }
      }

      // If gadget was used then waste gadget and set cooldown.
      if (bGadgetUsed) {
        m_iLastActivatedGadget = eGadgetIndex;
        m_tmGadgetActivated = _pTimer->CurrentTick();
        GetInventory()->DrainGadget(eGadgetIndex);
      }
    }

    if (GetInventory()->GetGadgetQty(eGadgetIndex) <= 0) {
      SelectNewGadget();
    }
  }
  
  INDEX GetLivesAny()
  {
    BOOL bSharedLives = GetSP()->sp_ulCoopFlags & CPF_SHARED_LIVES;
    return bSharedLives ? GetGameInfo()->GetSharedLives() : GetInventory()->GetCurrencyQty(E_CURRENCY_EXTRA_LIFE);
  }
  
  void DrainLifeAny()
  {
    BOOL bSharedLives = GetSP()->sp_ulCoopFlags & CPF_SHARED_LIVES;

    if (bSharedLives) {
      GetGameInfo()->DrainLife();
      return;
    }

    GetInventory()->DrainCurrency(E_CURRENCY_EXTRA_LIFE, 1);
  }

  void TryToRespawn()
  {
    BOOL bSinglePlayer = GetSP()->sp_bSinglePlayer;
    BOOL bCooperative = GetSP()->sp_bCooperative;
    INDEX iInitialLives = GetSP()->sp_ctInitialLives;
    BOOL bUseLives = (GetSP()->sp_ulCoopFlags & CPF_USE_LIVES) && iInitialLives != -1;
    
    // Single
    if (bSinglePlayer) {
      _pShell->Execute("gam_bQuickLoad=1;");
      return;
    }
    
    // Versus/TDM
    if (!bCooperative) {
      SendEvent(EEnd());
      return;
    }

    // if holding down reload button then forbid respawning in-place
    if (m_ulLastButtons&PLACT_RELOAD) {
      m_ulFlags &= ~PLF_RESPAWNINPLACE;
    }

    // We do not use lives - we can respawn.
    if (!bUseLives) {
      CInfoF(TRANS("%s is riding the gun again\n"), GetPlayerName());
      SendEvent(EEnd());
      return;
    }

    // if playing on finite lives and have at least one life.
    if (bUseLives && GetLivesAny() > 0) {
      DrainLifeAny();

      // initiate respawn
      CInfoF(TRANS("%s is riding the gun again\n"), GetPlayerName());
      SendEvent(EEnd());

      // report number of credits left
      if (GetLivesAny() <= 0) {
        CInfoF(TRANS("  no more lives left!\n"));
      } else {
        CInfoF(TRANS("  %d lives left\n"), GetLivesAny());
      }

    // if no more credits left
    } else {
      // report that you cannot respawn
      CInfoF(TRANS("%s rests in peace - out of lives\n"), GetPlayerName());
    }
  }

  // Death actions
  void DeathActions(const CPlayerAction &paAction)
  {
    // set heading, pitch and banking from the normal rotation into the camera view rotation
    if (m_penView != NULL) {
      ASSERT(IsPredicted() && m_penView->IsPredicted()||IsPredictor()&&m_penView->IsPredictor()||!IsPredicted()&&!m_penView->IsPredicted()&&!IsPredictor()&&!m_penView->IsPredictor());
      en_plViewpoint.pl_PositionVector = FLOAT3D(0, 1, 0);
      en_plViewpoint.pl_OrientationAngle += (ANGLE3D(
        (ANGLE)((FLOAT)paAction.pa_aRotation(1)*_pTimer->TickQuantum),
        (ANGLE)((FLOAT)paAction.pa_aRotation(2)*_pTimer->TickQuantum),
        (ANGLE)((FLOAT)paAction.pa_aRotation(3)*_pTimer->TickQuantum)));
    }

    // if death is finished and fire just released again and this is not a predictor
    if (m_iMayRespawn==2 && (ulReleasedButtons&PLACT_FIRE) && !IsPredictor()) {
      TryToRespawn();
    }

    // check fire released once after death
    if (m_iMayRespawn==1 && !(ulButtonsNow&PLACT_FIRE)) {
      m_iMayRespawn=2;
    }
  };


  // Buttons actions
  void ButtonsActions(CPlayerAction &paAction)
  {
    CPlayerWeaponEntity *penWeaponsFirst = GetWeapons(E_HAND_MAIN);
    CPlayerWeaponEntity *penWeaponsSecond = GetWeapons(E_HAND_SECOND);
    
    FLOAT tmSinceDualWieldPressed = m_tmDualWieldSelection == 0.0F ? 0.0F : _pTimer->CurrentTick() - m_tmDualWieldSelection;
    BOOL bDualWieldSelection = tmSinceDualWieldPressed > 0.5F;

    BOOL bSniping = penWeaponsFirst->m_bSniping;
    FLOAT tmSinceGadgetPressed = m_tmGadgetSelection == 0.0F ? 0.0F : _pTimer->CurrentTick() - m_tmGadgetSelection;
    
    // if selecting a new weapon select it
    if ((ulNewButtons&PLACT_SELECT_WEAPON_MASK)!=0) {
      INDEX iWeapon = (ulNewButtons&PLACT_SELECT_WEAPON_MASK)>>PLACT_SELECT_WEAPON_SHIFT;
      SelectWeapon(bDualWieldSelection ? E_HAND_SECOND : E_HAND_MAIN, iWeapon, FALSE);
    }

    const FLOAT tmFavoritePressed = m_tmFavoriteSelection <= 0.0F ? 0.0F : _pTimer->CurrentTick() - m_tmFavoriteSelection;
    const INDEX iSelectFavoriteOld = (ulButtonsBefore & PLACT_FAVORITE_WEAPON_MASK) >> PLACT_FAVORITE_WEAPON_SHIFT;
    const INDEX iSelectFavoriteNow = (ulButtonsNow & PLACT_FAVORITE_WEAPON_MASK) >> PLACT_FAVORITE_WEAPON_SHIFT;

    // If started pressing any.
    if (iSelectFavoriteNow != 0 && iSelectFavoriteOld == 0) {
      m_tmFavoriteSelection = _pTimer->CurrentTick();
    }

    // Released.
    if (iSelectFavoriteNow == 0 && iSelectFavoriteOld != 0){
      const WeaponIndex eFavoriteWeapon = GetInventory()->GetFavoriteWeapon(iSelectFavoriteOld - 1);

      if (tmFavoritePressed <= 0.5F) {
        if (eFavoriteWeapon > E_WEAPON_NONE) {
          /*
          if (_pNetwork->IsPlayerLocal(this)) {
            CDebugF("Favorite weapon %d select %d\n", iSelectFavoriteOld, eFavoriteWeapon);
          }
          */

          SelectWeapon(E_HAND_MAIN, eFavoriteWeapon, TRUE);
        }
      } else {
        const WeaponIndex eCurrentWeapon = GetWeapons(E_HAND_MAIN)->GetCurrentWeapon();

        if (_pNetwork->IsPlayerLocal(this)) {
          CInfoF("Favorite weapon %d set %d -> %d\n", iSelectFavoriteOld, eFavoriteWeapon, eCurrentWeapon);
        }

        GetInventory()->SetFavoriteWeapon(iSelectFavoriteOld - 1, eCurrentWeapon);
      }
    }

    // next weapon zooms out when in sniping mode
    if (ulNewButtons&PLACT_WEAPON_NEXT) {
      // Sniper zoom.
      if (bSniping) {
        ApplySniperZoom(0);
      } else if (tmSinceGadgetPressed > 0.25) {
        GetInventory()->SetCurrentGadget(FindGadgetInDirection(+1));
      } else if (TRUE) {
        INDEX iWeapon = -1;
        SelectWeapon(bDualWieldSelection ? E_HAND_SECOND : E_HAND_MAIN, iWeapon,FALSE);
      }
    }

    // previous weapon zooms in when in sniping mode
    if (ulNewButtons&PLACT_WEAPON_PREV) {
      // Sniper zoom.
      if (bSniping) {
        ApplySniperZoom(1);
      } else if (tmSinceGadgetPressed > 0.25) {
        GetInventory()->SetCurrentGadget(FindGadgetInDirection(-1));
      } else if (TRUE) {
        INDEX iWeapon = -2;
        SelectWeapon(bDualWieldSelection ? E_HAND_SECOND : E_HAND_MAIN, iWeapon, FALSE);
      }
    }

    if (ulNewButtons&PLACT_WEAPON_FLIP) {
        INDEX iWeapon = -2;
        SelectWeapon(bDualWieldSelection ? E_HAND_SECOND : E_HAND_MAIN, iWeapon, FALSE);
    }

    // if fire is pressed
    if (ulNewButtons&PLACT_FIRE) {
      FireWeapon(E_HAND_MAIN);
    }

    // if fire is released
    if (ulReleasedButtons&PLACT_FIRE) {
      ReleaseWeapon(E_HAND_MAIN);
    }
    
    if (ulNewButtons & PLACT_SECONDARY_FIRE) {
      if (IsDualWielding()) {
        FireWeapon(E_HAND_SECOND);
      }
    }
    
    if (ulReleasedButtons & PLACT_SECONDARY_FIRE) {
      if (IsDualWielding()) {
        ReleaseWeapon(E_HAND_SECOND);
      }
    }

    // if reload is pressed
    if (ulReleasedButtons&PLACT_RELOAD) {
      ReloadWeapon(E_HAND_BOTH);
    }

    // If gadget is pressed...
    if (ulNewButtons&PLACT_GADGET) {
      m_tmGadgetSelection = _pTimer->CurrentTick();
    }

    // If released gadget button...
    if (ulReleasedButtons&PLACT_GADGET) {
      // If short press.
      if (tmSinceGadgetPressed < 0.5F) {
        UseGadget(GetInventory()->GetCurrentGadget());
      }
      
      m_tmGadgetSelection = 0.0F;
    }
    
    // For dual wielding.
    if (ulNewButtons & PLACT_SELECT_WEAPON_MOD) {
      m_tmDualWieldSelection = _pTimer->CurrentTick();
    }
    
    if (ulReleasedButtons & PLACT_SELECT_WEAPON_MOD) {
      if (!bDualWieldSelection) {
        ToggleDualWield();
      }
      
      m_tmDualWieldSelection = 0.0F;
    }

    if (ulNewButtons & PLACT_AIM_DOWN_SIGHT) {
      AimDownSightPressed();
      bAimButtonHeld = TRUE;
    }

    if (ulReleasedButtons &PLACT_AIM_DOWN_SIGHT) {
      bAimButtonHeld = FALSE;
    }
    
    // if use is pressed
    if (ulNewButtons&PLACT_USE) {
      UsePressed(ulNewButtons&PLACT_COMPUTER);

    // if computer is pressed
    } else if (ulNewButtons&PLACT_COMPUTER) {
      ComputerPressed();
    }

    // if sniper zoomin is pressed
    if (ulNewButtons&PLACT_SNIPER_ZOOMIN) {
      ApplySniperZoom(1);
    }

    // if sniper zoomout is pressed
    if (ulNewButtons&PLACT_SNIPER_ZOOMOUT) {
      ApplySniperZoom(0);
    }

    // if 3rd person view is pressed
    if (ulNewButtons&PLACT_3RD_PERSON_VIEW) {
      ChangePlayerView();
    }

    // apply center view
    if (ulButtonsNow&PLACT_CENTER_VIEW) {
      // center view with speed of 45 degrees per 1/20 seconds
      paAction.pa_aRotation(2) += Clamp(-en_plViewpoint.pl_OrientationAngle(2)/_pTimer->TickQuantum, -900.0f, +900.0f);
    }
  };

  void ApplySniperZoom(BOOL bZoomIn )
  {
    CPlayerWeaponEntity *penWeapons = GetWeapons(E_HAND_MAIN);
    
    // do nothing if not holding sniper and if not in sniping mode
    if (!penWeapons->CanZoom() || penWeapons->m_bSniping == FALSE) {
      return;
    }

    BOOL bZoomChanged;

    if (penWeapons->SniperZoomDiscrete(bZoomIn, bZoomChanged)) {
      if (bZoomChanged) {
        PlaySound(m_soSniperZoom, SOUND_SNIPER_QZOOM, SOF_3D);
      }

      m_ulFlags|=PLF_ISZOOMING;

    } else {
      m_ulFlags&=~PLF_ISZOOMING;
      PlaySound(m_soSniperZoom, SOUND_SILENCE, SOF_3D);
      if (_pNetwork->IsPlayerLocal(this)) {IFeel_StopEffect("SniperZoom");}
    }
  }

  void RequestCheats(void)
  {
    if (cht_bGod) {
      cht_bGod = FALSE;

      EToggleCheat eCheat;
      eCheat.iFlag = CHT_FLAG_GOD;
      _pNetwork->SendEventNet(this, eCheat);
    }
    
    if (cht_bFly) {
      cht_bFly = FALSE;
      
      EToggleCheat eCheat;
      eCheat.iFlag = CHT_FLAG_FLY;
      _pNetwork->SendEventNet(this, eCheat);
    }
    
    if (cht_bTurbo) {
      cht_bTurbo = FALSE;
      
      EToggleCheat eCheat;
      eCheat.iFlag = CHT_FLAG_TURBO;
      _pNetwork->SendEventNet(this, eCheat);
    }
    
    if (cht_bGhost) {
      cht_bGhost = FALSE;
      
      EToggleCheat eCheat;
      eCheat.iFlag = CHT_FLAG_GHOST;
      _pNetwork->SendEventNet(this, eCheat);
    }
    
    if (cht_bInfiniteAmmo) {
      cht_bInfiniteAmmo = FALSE;
      
      EToggleCheat eCheat;
      eCheat.iFlag = CHT_FLAG_INFINITE_AMMO;
      _pNetwork->SendEventNet(this, eCheat);
    }
    
    if (cht_bKillAll) {
      cht_bKillAll = FALSE;

      EToggleCheat eCheat;
      eCheat.iFlag = CHT_FLAG_KILLALL;
      _pNetwork->SendEventNet(this, eCheat);
    }
    
    if (cht_bOpen) {
      cht_bOpen = FALSE;

      EToggleCheat eCheat;
      eCheat.iFlag = CHT_FLAG_OPEN;
      _pNetwork->SendEventNet(this, eCheat);
    }
    
    if (cht_bRefresh) {
      cht_bRefresh = FALSE;

      EToggleCheat eCheat;
      eCheat.iFlag = CHT_FLAG_REFRESH;
      _pNetwork->SendEventNet(this, eCheat);
    }
    
    if (cht_iGiveAll) {
      EGiveAllCheat eCheat;
      eCheat.iFlags = cht_iGiveAll;
      _pNetwork->SendEventNet(this, eCheat);

      cht_iGiveAll = 0;
    }

    if (cht_iGiveWeapon) {
      EGiveWeaponCheat eCheat;
      eCheat.iWeapon = cht_iGiveWeapon;
      _pNetwork->SendEventNet(this, eCheat);

      cht_iGiveWeapon = 0;
    }
    
    if (cht_iGoToMarker > -1) {
      EGoToMarkerCheat eCheat;
      eCheat.iMarker = cht_iGoToMarker;
      _pNetwork->SendEventNet(this, eCheat);
      
      cht_iGoToMarker = -1;
    }
  }

  // Cheats
  void DoCheats(void)
  { 
    if (m_ulCheatFlags & CHT_FLAG_KILLALL) {
      m_ulCheatFlags &= ~CHT_FLAG_KILLALL;
      OnKillAllCheat();
    }
    
    if (m_ulCheatFlags & CHT_FLAG_OPEN) {
      m_ulCheatFlags &= ~CHT_FLAG_OPEN;
      OnOpenCheat();
    }
    
    if (m_ulCheatFlags & CHT_FLAG_REFRESH) {
      m_ulCheatFlags &= ~CHT_FLAG_REFRESH;
      OnRefreshCheat();
    }
  
    BOOL bGhostCheat = m_ulCheatFlags & CHT_FLAG_GHOST;
    BOOL bFlyOn = (m_ulCheatFlags & CHT_FLAG_FLY) || bGhostCheat;

    // fly mode
    BOOL bIsFlying = !(GetPhysicsFlags() & EPF_TRANSLATEDBYGRAVITY);

    if (bFlyOn && !bIsFlying) {
      SetPhysicsFlags(GetPhysicsFlags() & ~(EPF_TRANSLATEDBYGRAVITY|EPF_ORIENTEDBYGRAVITY));
      en_plViewpoint.pl_OrientationAngle = ANGLE3D(0, 0, 0);
    } else if (!bFlyOn && bIsFlying) {
      SetPhysicsFlags(GetPhysicsFlags() | EPF_TRANSLATEDBYGRAVITY|EPF_ORIENTEDBYGRAVITY);
      en_plViewpoint.pl_OrientationAngle = ANGLE3D(0, 0, 0);
    }

    // ghost mode
    BOOL bIsGhost = !(GetCollisionFlags() & ((ECBI_BRUSH|ECBI_MODEL)<<ECB_TEST));
    if (bGhostCheat && !bIsGhost) {
      SetCollisionFlags(GetCollisionFlags() & ~((ECBI_BRUSH|ECBI_MODEL)<<ECB_TEST));
    } else if (!bGhostCheat && bIsGhost) {
      SetCollisionFlags(GetCollisionFlags() | ((ECBI_BRUSH|ECBI_MODEL)<<ECB_TEST));
    }

    // invisible mode
    INDEX iInvisAmp = GetInventory()->GetStrongestStatusEffectAmp(E_STEF_INVISIBILITY);

    if (m_ulCheatFlags & CHT_FLAG_INVISIBLE || iInvisAmp > 0) {
      SetFlags(GetFlags() | ENF_INVISIBLE);
    } else {
      SetFlags(GetFlags() & ~ENF_INVISIBLE);
    }
    
    // if teleporting to marker (this cheat is enabled in all versions)
    if (m_iGoToMarker > 0 && (GetFlags()&ENF_ALIVE)) {
      // rebirth player, and it will teleport
      m_iLastViewState = m_iViewState;
      SendEvent(ERebirth());
    }
  };


/************************************************************
 *                 END OF PLAYER ACTIONS                    *
 ************************************************************/
  // Get current placement that the player views from in absolute space.
  void GetLerpedAbsoluteViewPlacement(CPlacement3D &plView)
  {
    if (!(m_ulFlags&PLF_INITIALIZED)) {
      plView = GetPlacement();
      _bDiscard3rdView=FALSE;
      return;
    }

    BOOL bSharpTurning = GetSettings() != NULL && GetSettings()->ps_ulFlags&PSF_SHARPTURNING;
    bSharpTurning = bSharpTurning && _pNetwork->IsPlayerLocal((CPlayerPawnEntity*)GetPredictionTail());

    // lerp player viewpoint
    FLOAT fLerpFactor = _pTimer->GetLerpFactor();
    plView.Lerp(en_plLastViewpoint, en_plViewpoint, fLerpFactor);

    // moving banking and soft eyes
    GetPlayerAnimator()->ChangeView(plView);

    // body and head attachment animation
    GetPlayerAnimator()->BodyAndHeadOrientation(plView);

    // return player eyes view
    if (m_iViewState == PVT_PLAYEREYES || _bDiscard3rdView) {
      CPlacement3D plPosLerped = GetLerpedPlacement();

      if (bSharpTurning) {
        // get your prediction tail
        CPlayerPawnEntity *pen = (CPlayerPawnEntity*)GetPredictionTail();

        // add local rotation
        if (m_ulFlags&PLF_ISZOOMING) {
          FLOAT fRotationDamping = GetWeapons(E_HAND_MAIN)->m_fSniperFOV / GetWeapons(E_HAND_MAIN)->m_fSniperMaxFOV;
          plView.pl_OrientationAngle = pen->en_plViewpoint.pl_OrientationAngle + (pen->m_aLocalRotation-pen->m_aLastRotation)*fRotationDamping;
        } else {
          plView.pl_OrientationAngle = pen->en_plViewpoint.pl_OrientationAngle + (pen->m_aLocalRotation-pen->m_aLastRotation);
        }

        // make sure it doesn't go out of limits
        RoundViewAngle(plView.pl_OrientationAngle(2), PITCH_MAX);
        RoundViewAngle(plView.pl_OrientationAngle(3), BANKING_MAX);

        // compensate for rotations that happen to the player without his/hers will
        // (rotating brushes, weird gravities...)
        // (these need to be lerped)
        ANGLE3D aCurr = pen->GetPlacement().pl_OrientationAngle;
        ANGLE3D aLast = pen->en_plLastPlacement.pl_OrientationAngle;
        ANGLE3D aDesired = pen->en_aDesiredRotationRelative*_pTimer->TickQuantum;
        FLOATmatrix3D mCurr;      MakeRotationMatrixFast(mCurr, aCurr);
        FLOATmatrix3D mLast;      MakeRotationMatrixFast(mLast, aLast);
        FLOATmatrix3D mDesired;   MakeRotationMatrixFast(mDesired, aDesired);
        mDesired = en_mRotation*(mDesired*!en_mRotation);
        FLOATmatrix3D mForced = !mDesired*mCurr*!mLast; // = aCurr-aLast-aDesired;
        ANGLE3D aForced; DecomposeRotationMatrixNoSnap(aForced, mForced);

        if (aForced.MaxNorm()<1E-2) {
          aForced = ANGLE3D(0,0,0);
        }

        FLOATquat3D qForced; qForced.FromEuler(aForced);
        FLOATquat3D qZero;   qZero.FromEuler(ANGLE3D(0,0,0));
        FLOATquat3D qLerped = Slerp(fLerpFactor, qZero, qForced);
        FLOATmatrix3D m;
        qLerped.ToMatrix(m);
        m=m*mDesired*mLast;
        DecomposeRotationMatrixNoSnap(plPosLerped.pl_OrientationAngle, m);
      }

      plView.RelativeToAbsoluteSmooth(plPosLerped);

    // 3rd person view
    } else if (m_iViewState == PVT_3RDPERSONVIEW) {
      plView = m_pen3rdPersonView->GetLerpedPlacement();

    // camera view for player auto actions
    } else if (m_iViewState == PVT_PLAYERAUTOVIEW) {
      plView = m_penView->GetLerpedPlacement();

    // camera view for stored sequences
    } else {
      ASSERTALWAYS("Unknown player view");
    }

    _bDiscard3rdView=FALSE;
  };

  // Get current entity that the player views from.
  CEntity *GetViewEntity(void)
  {
    // player eyes
    if (m_iViewState == PVT_PLAYEREYES) {
      return this;

    // 3rd person view
    } else if (m_iViewState == PVT_3RDPERSONVIEW) {
      if (m_ulFlags&PLF_ISZOOMING) {
        return this;
      }

      if (GetView3rd()->m_fDistance > 2.0f) {
        return m_pen3rdPersonView;
      } else {
        return this;
      }

    // camera
    } else if (m_iViewState == PVT_PLAYERAUTOVIEW) {
      if (GetView()->m_fDistance > 2.0f) {
        return m_penView;
      } else {
        return this;
      }

    // invalid view
    } else {
      ASSERTALWAYS("Unknown player view");
      return NULL;
    }
  };

  void RenderChainsawParticles(BOOL bThird)
  {
    FLOAT fStretch=1.0f;

    if (bThird)
    {
      fStretch=0.4f;
    }

    // render chainsaw cutting brush particles
    FLOAT tmNow = _pTimer->GetLerpedCurrentTick();

    for (INDEX iSpray=0; iSpray<MAX_BULLET_SPRAYS; iSpray++)
    {
      BulletSprayLaunchData &bsld = m_absldData[iSpray];
      FLOAT fLife=1.25f;

      if (tmNow > (bsld.bsld_tmLaunch+fLife)) { continue;}

      Particles_BulletSpray(bsld.bsld_iRndBase, bsld.bsld_vPos, bsld.bsld_vG,
        bsld.bsld_eptType, bsld.bsld_tmLaunch, bsld.bsld_vStretch*fStretch, 1.0f);
    }

    // render chainsaw cutting model particles
    for (INDEX iGore=0; iGore<MAX_GORE_SPRAYS; iGore++)
    {
      GoreSprayLaunchData &gsld = m_agsldData[iGore];
      FLOAT fLife=2.0f;

      if (tmNow > (gsld.gsld_tmLaunch+fLife)) { continue;}
      FLOAT3D vPos=gsld.gsld_vPos;

      if (bThird)
      {
        vPos=gsld.gsld_v3rdPos;
      }

      Particles_BloodSpray(gsld.gsld_sptType, vPos, gsld.gsld_vG, gsld.gsld_fGA,
        gsld.gsld_boxHitted, gsld.gsld_vSpilDirection,
        gsld.gsld_tmLaunch, gsld.gsld_fDamagePower*fStretch, gsld.gsld_colParticles);
    }
  }

  // Draw player interface on screen.
  void RenderHUD(CPerspectiveProjection3D &prProjection, CDrawPort *pdp,
                  FLOAT3D vViewerLightDirection, COLOR colViewerLight, COLOR colViewerAmbient,
                  BOOL bRenderWeapon, INDEX iEye)
  {
    CPlayerWeaponEntity *penWeapons = GetWeapons(E_HAND_MAIN);
    CPlayerWeaponEntity *penWeaponsSecond = GetWeapons(E_HAND_SECOND);
    
    CPlacement3D plViewOld = prProjection.ViewerPlacementR();
    BOOL bSniping = penWeapons->m_bSniping;

    // render weapon models if needed
    // do not render weapon if sniping
    BOOL bRenderModels = _pShell->GetINDEX("gfx_bRenderModels");
    if (hud_bShowWeapon && bRenderModels && !bSniping) {
      // render weapons only if view is from player eyes
      penWeapons->RenderWeaponModel(prProjection, pdp, vViewerLightDirection, colViewerLight, colViewerAmbient, bRenderWeapon, iEye);
      
      if (penWeaponsSecond) {
        penWeaponsSecond->RenderWeaponModel(prProjection, pdp, vViewerLightDirection, colViewerLight, colViewerAmbient, bRenderWeapon, iEye);
      }
    }

    // if is first person
    if (m_iViewState == PVT_PLAYEREYES)
    {
      prProjection.ViewerPlacementL() = plViewOld;
      prProjection.Prepare();
      CAnyProjection3D apr;
      apr = prProjection;
      Stereo_AdjustProjection(*apr, iEye, 1);
      Particle_PrepareSystem(pdp, apr);
      Particle_PrepareEntity(2.0f, FALSE, FALSE, this);
      RenderChainsawParticles(FALSE);
      Particle_EndSystem();
    }

    // render crosshair if sniper zoom not active
    CPlacement3D plView;

    if (m_iViewState == PVT_PLAYEREYES) {
      // player view
      plView = en_plViewpoint;
      plView.RelativeToAbsolute(GetPlacement());
    } else if (m_iViewState == PVT_3RDPERSONVIEW) {
      // camera view
      plView = GetView3rd()->GetPlacement();
    }

    if (!bSniping) {
      penWeapons->RenderCrosshair(prProjection, pdp, plView);
      
      if (penWeaponsSecond && penWeaponsSecond->GetCurrentWeapon() != E_WEAPON_NONE) {
        penWeaponsSecond->RenderCrosshair(prProjection, pdp, plView);
      }
    }

    // get your prediction tail
    CPlayerPawnEntity *pen = (CPlayerPawnEntity*)GetPredictionTail();

    // do screen blending
    ULONG ulR=255, ulG=0, ulB=0; // red for wounding
    ULONG ulA = pen->m_iDamageAmount / HEALTH_VALUE_MULTIPLIER * 5;

    // if less than few seconds elapsed since last damage
    FLOAT tmSinceWounding = _pTimer->CurrentTick() - pen->m_tmWoundedTime;
    if (tmSinceWounding<4.0f) {
      // decrease damage ammount
      if (tmSinceWounding<0.001f) { ulA = (ulA+64)/2; }
    }

    // add rest of blend ammount
    ulA = ClampUp(ulA, (ULONG)224);
    if (m_iViewState == PVT_PLAYEREYES) {
      pdp->dp_ulBlendingRA += ulR*ulA;
      pdp->dp_ulBlendingGA += ulG*ulA;
      pdp->dp_ulBlendingBA += ulB*ulA;
      pdp->dp_ulBlendingA  += ulA;
    }

    // add world glaring
    {
      COLOR colGlare = GetWorldGlaring();
      UBYTE ubR, ubG, ubB, ubA;
      ColorToRGBA(colGlare, ubR, ubG, ubB, ubA);
      if (ubA!=0) {
        pdp->dp_ulBlendingRA += ULONG(ubR)*ULONG(ubA);
        pdp->dp_ulBlendingGA += ULONG(ubG)*ULONG(ubA);
        pdp->dp_ulBlendingBA += ULONG(ubB)*ULONG(ubA);
        pdp->dp_ulBlendingA  += ULONG(ubA);
      }
    }

    // do all queued screen blendings
    pdp->BlendScreen();

    // render status info line (if needed)
    if (hud_bShowInfo) {
      // get player or its predictor
      BOOL bSnooping = FALSE;
      CPlayerPawnEntity *penHUDPlayer = this;
      CPlayerPawnEntity *penHUDOwner  = this;

      if (penHUDPlayer->IsPredicted()) {
        penHUDPlayer = (CPlayerPawnEntity *)penHUDPlayer->GetPredictor();
      }

      // check if snooping is needed
      CPlayerWeaponEntity *pen = (CPlayerWeaponEntity*)&*penHUDPlayer->m_penWeaponsFirst;
      TIME tmDelta = _pTimer->CurrentTick() - pen->m_tmSnoopingStarted;

      if (tmDelta<plr_tmSnoopingTime) {
        ASSERT(pen->m_penTargeting!=NULL);
        penHUDPlayer = (CPlayerPawnEntity*)&*pen->m_penTargeting;
        bSnooping = TRUE;
      }

      DrawHUD(penHUDPlayer, pdp, bSnooping, penHUDOwner);
    }
  }


/************************************************************
 *                  SPECIAL FUNCTIONS                       *
 ************************************************************/
  // try to find start marker for deathmatch (re)spawning
  CEntity *GetDeathmatchStartMarker(void)
  {
    // get number of markers
    CTString strPlayerStart = "Player Start - ";
    INDEX ctMarkers = _pNetwork->GetNumberOfEntitiesWithName(strPlayerStart);

    // if none then fail
    if (ctMarkers==0) {
      return NULL;
    }

    // if only one then get that one
    if (ctMarkers==1) {
      return _pNetwork->GetEntityWithName(strPlayerStart, 0);
    }

    // if at least two markers found...

    // create tables of markers and their distances from players
    CStaticArray<MarkerDistance> amdMarkers;
    amdMarkers.New(ctMarkers);

    // for each marker
    for (INDEX iMarker=0; iMarker<ctMarkers; iMarker++)
    {
      amdMarkers[iMarker].md_ppm = (CPlayerMarkerEntity*)_pNetwork->GetEntityWithName(strPlayerStart, iMarker);
      if (amdMarkers[iMarker].md_ppm==NULL) {
        return NULL;  // (if there is any invalidity, fail completely)
      }

      // get min distance from any player
      FLOAT fMinD = UpperLimit(0.0f);
      for (INDEX iPlayer=0; iPlayer<GetMaxPlayers(); iPlayer++)
      {
        CPlayerPawnEntity *ppl = (CPlayerPawnEntity *)&*GetPlayerEntity(iPlayer);

        if (ppl==NULL) {
          continue;
        }

        FLOAT fD =
          (amdMarkers[iMarker].md_ppm->GetPlacement().pl_PositionVector-
           ppl->GetPlacement().pl_PositionVector).Length();

        if (fD<fMinD) {
          fMinD = fD;
        }
      }

      amdMarkers[iMarker].md_fMinD = fMinD;
    }

    // now sort the list
    qsort(&amdMarkers[0], ctMarkers, sizeof(amdMarkers[0]), &qsort_CompareMarkerDistance);
    ASSERT(amdMarkers[0].md_fMinD>=amdMarkers[ctMarkers-1].md_fMinD);

    // choose marker among one of the 50% farthest
    INDEX ctFarMarkers = ctMarkers/2;
    ASSERT(ctFarMarkers>0);
    INDEX iStartMarker = IRnd()%ctFarMarkers;

    // find first next marker that was not used lately
    INDEX iMarker=iStartMarker;
    FOREVER
    {
      if (_pTimer->CurrentTick()>amdMarkers[iMarker].md_ppm->m_tmLastSpawned+1.0f) {
        break;
      }

      iMarker = (iMarker+1)%ctMarkers;

      if (iMarker==iStartMarker) {
        break;
      }
    }

    // return that
    return amdMarkers[iMarker].md_ppm;
  }

/************************************************************
 *                  INITIALIZE PLAYER                       *
 ************************************************************/

  void InitializePlayer()
  {
    // set viewpoint position inside the entity
    en_plViewpoint.pl_OrientationAngle = ANGLE3D(0,0,0);
    en_plViewpoint.pl_PositionVector = FLOAT3D(0.0f, plr_fViewHeightStand, 0.0f);
    en_plLastViewpoint = en_plViewpoint;

    // clear properties
    m_ulFlags &= PLF_INITIALIZED|PLF_LEVELSTARTED|PLF_RESPAWNINPLACE;  // must not clear initialized flag
    m_fFallTime = 0.0f;
    m_pstState = PST_STAND;
    m_iDamageAmount = 0.0f;
    m_tmWoundedTime  = 0.0f;

    GetInventory()->ResetStatusEffects();

    // initialize animator
    GetPlayerAnimator()->Initialize();

    // restart weapons if needed
    StartWeapons();

    // initialise last positions for particles
    Particles_AfterBurner_Prepare(this);

    // set flags
    SetPhysicsFlags(EPF_MODEL_WALKING|EPF_HASLUNGS);
    SetCollisionFlags(ECF_MODEL|((ECBI_PLAYER)<<ECB_IS));
    SetFlags(GetFlags()|ENF_ALIVE);

    // animation
    StartModelAnim(PLAYER_ANIM_STAND, AOF_LOOPING);
    TeleportPlayer(WLT_FIXED);
  };


  FLOAT3D GetTeleportingOffset(void)
  {
    // find player index
    INDEX iPlayer = GetMyPlayerIndex();

    // create offset from marker
    const FLOAT fOffsetY = 0.1f;  // how much to offset up (as precaution not to spawn in floor)
    FLOAT3D vOffsetRel = FLOAT3D(0,fOffsetY,0);

    if (GetSP()->sp_bCooperative && !GetSP()->sp_bSinglePlayer) {
      INDEX iRow = iPlayer/4;
      INDEX iCol = iPlayer%4;
      vOffsetRel = FLOAT3D(-3.0f+iCol*2.0f, fOffsetY, -3.0f+iRow*2.0f);
    }

    return vOffsetRel;
  }


  void RemapLevelNames(INDEX &iLevel)
  {
	  switch (iLevel)
    {
      case 10:
        iLevel = 1;
        break;

      case 11:
        iLevel = 2;
        break;

      case 12:
        iLevel = 3;
        break;

      case 13:
        iLevel = 4;
        break;

      case 14:
        iLevel = 5;
        break;

      case 15:
        iLevel = 6;
        break;

      case 21:
        iLevel = 7;
        break;

      case 22:
        iLevel = 8;
        break;

      case 23:
        iLevel = 9;
        break;

      case 24:
        iLevel = 10;
        break;

      case 31:
        iLevel = 11;
        break;

      case 32:
        iLevel = 12;
        break;

      case 33:
        iLevel = 13;
        break;

      default:
        iLevel = -1;
        break;
	  }
  }


  void TeleportPlayer(enum WorldLinkType EwltType)
  {
    INDEX iLevel = -1;
    CTString strLevelName = GetWorld()->wo_fnmFileName.FileName();

    //strLevelName.ScanF("%02d_", &iLevel);
    INDEX u, v;
    u = v = -1;
    strLevelName.ScanF("%01d_%01d_", &u, &v);
    iLevel = u*10+v;

	  RemapLevelNames(iLevel);

    if (iLevel>0) {
      ((CSessionProperties*)GetSP())->sp_ulLevelsMask|=1<<(iLevel-1);
    }

    // find player index
    INDEX iPlayer = GetMyPlayerIndex();
    // player placement
    CPlacement3D plSet = GetPlacement();
    // teleport in dummy space to avoid auto teleport frag
    Teleport(CPlacement3D(FLOAT3D(32000.0f+100.0f*iPlayer, 32000.0f, 0), ANGLE3D(0, 0, 0)));

    // force yourself to standing state
    ForceCollisionBoxIndexChange(PLAYER_COLLISION_BOX_STAND);
    en_plViewpoint.pl_PositionVector(2) = plr_fViewHeightStand;
    GetPlayerAnimator()->m_bDisableAnimating = FALSE;
    GetPlayerAnimator()->Stand();
    m_pstState = PST_STAND;

    // create offset from marker
    FLOAT3D vOffsetRel = GetTeleportingOffset();

    // no player start initially
    BOOL bSetHealth = FALSE;      // for getting health from marker
    BOOL bAdjustHealth = FALSE;   // for getting adjusting health to 50-100 interval
    CEntity *pen = NULL;

    if (GetSP()->sp_bCooperative) {
      if (m_iGoToMarker > 0) {
        // try to find fast go marker
        CTString strPlayerStart;
        strPlayerStart.PrintF("Player Start - %d", (INDEX)m_iGoToMarker);
        pen = _pNetwork->GetEntityWithName(strPlayerStart, 0);
        pen->SendEvent(ETrigger());
        m_iGoToMarker = -1;
        bSetHealth = TRUE;
        bAdjustHealth = FALSE;

      // if there is coop respawn marker
      } else
      if (m_penMainMusicHolder!=NULL && !(m_ulFlags&PLF_CHANGINGLEVEL)) {
        CMusicHolderEntity *pmh = GetMusicHolder();

        if (pmh->m_penRespawnMarker!=NULL) {
          // get it
          pen = pmh->m_penRespawnMarker;
          bSetHealth = TRUE;
          bAdjustHealth = FALSE;
        }
      }

      // if quick start is enabled (in wed)
      if (pen==NULL && GetSP()->sp_bQuickTest && m_strGroup=="") {
        // try to find quick start marker
        CTString strPlayerStart;
        strPlayerStart.PrintF("Player Quick Start");
        pen = _pNetwork->GetEntityWithName(strPlayerStart, 0);
        bSetHealth = TRUE;
        bAdjustHealth = FALSE;
      }

      // if no start position yet
      if (pen==NULL) {
        // try to find normal start marker
        CTString strPlayerStart;
        strPlayerStart.PrintF("Player Start - %s", m_strGroup);
        pen = _pNetwork->GetEntityWithName(strPlayerStart, 0);

        if (m_strGroup=="") {
          bSetHealth = TRUE;
          bAdjustHealth = FALSE;
        } else {
          if (EwltType==WLT_FIXED) {
            bSetHealth = FALSE;
            bAdjustHealth = TRUE;
          } else {
            bSetHealth = FALSE;
            bAdjustHealth = FALSE;
          }
        }
      }

      // if no start position yet
      if (pen==NULL) {
        // try to find normal start marker without group anyway
        CTString strPlayerStart;
        strPlayerStart.PrintF("Player Start - ");
        pen = _pNetwork->GetEntityWithName(strPlayerStart, 0);
        bSetHealth = TRUE;
        bAdjustHealth = FALSE;
      }

    } else {
      bSetHealth = TRUE;
      bAdjustHealth = FALSE;

      // try to find start marker by random
      pen = GetDeathmatchStartMarker();

      if (pen!=NULL) {
        ((CPlayerMarkerEntity&)*pen).m_tmLastSpawned = _pTimer->CurrentTick();
      }
    }

    // if respawning in place
    if ((m_ulFlags&PLF_RESPAWNINPLACE) && pen!=NULL && !((CPlayerMarkerEntity*)&*pen)->m_bNoRespawnInPlace) {
      m_ulFlags &= ~PLF_RESPAWNINPLACE;

      // set default params
      SetHealthInit(TopHealth());
      m_iMana  = GetSP()->sp_iInitialMana;
      m_iArmor = 0;

      // teleport where you were when you were killed
      Teleport(CPlacement3D(m_vDied, m_aDied));

    // if start marker is found
    } else if (pen!=NULL) {
      // if there is no respawn marker yet
      if (m_penMainMusicHolder!=NULL) {
        CMusicHolderEntity *pmh = GetMusicHolder();

        if (pmh->m_penRespawnMarker==NULL) {
          // set it
          pmh->m_penRespawnMarker = pen;
        }
      }

      CPlayerMarkerEntity &CpmStart = (CPlayerMarkerEntity&)*pen;

      // set player characteristics
      if (bSetHealth) {
        SetHealthInit(CpmStart.m_fHealth/100.0f * TopHealth());
        m_iMana  = GetSP()->sp_iInitialMana;
        m_iArmor = CpmStart.m_fShield * 100;

      } else if (bAdjustHealth) {
        FLOAT fHealth = GetHealth();
        FLOAT fTopHealth = TopHealth();

        if (fHealth < fTopHealth) {
          SetHealth(ClampUp(fHealth+fTopHealth/2.0f, fTopHealth));
        }
      }

      // if should start in computer
      if (CpmStart.m_bStartInComputer && GetSP()->sp_bSinglePlayer) {
        // mark that
        if (_pNetwork->IsPlayerLocal(this)) {
          cmp_ppenPlayer = this;
        }

        cmp_bInitialStart = TRUE;
      }

      // start with first message linked to the marker
      CMessageHolderEntity *penMessage = (CMessageHolderEntity *)&*CpmStart.m_penMessage;
      // while there are some messages to add
      while (penMessage!=NULL && IsOfClass(penMessage, "MessageHolder"))
      {
        const CTFileName &fnmMessage = penMessage->m_fnmMessage;

        // if player doesn't have that message in database
        if (!HasMessage(fnmMessage)) {
          ReceiveComputerMessage(fnmMessage, 0); // add the message
        }

        // go to next message holder in list
        penMessage = (CMessageHolderEntity *)&*penMessage->m_penNext;
      }

      INDEX iGiveWeapons = CpmStart.m_iGiveWeapons;
      INDEX iTakeWeapons = CpmStart.m_iTakeWeapons;
      INDEX iTakeAmmo = GetSP()->sp_bInfiniteAmmo ? 0 : CpmStart.m_iTakeAmmo;

      if (GetSP()->sp_bCooperative) {
        GetInventory()->InitializeWeapons(iGiveWeapons, iTakeWeapons, iTakeAmmo, CpmStart.m_fMaxAmmoRatio);
      } else {
        GetInventory()->InitializeWeapons(iGiveWeapons, 0, 0, CpmStart.m_fMaxAmmoRatio);
      }

      InitializeWeapons();

      // start position relative to link
      if (EwltType == WLT_RELATIVE) {
        plSet.AbsoluteToRelative(_SwcWorldChange.plLink);   // relative to link position
        plSet.RelativeToAbsolute(CpmStart.GetPlacement());  // absolute to start marker position
        Teleport(plSet);

      // fixed start position
      } else if (EwltType == WLT_FIXED) {
        CPlacement3D plNew = CpmStart.GetPlacement();
        vOffsetRel*=CpmStart.en_mRotation;
        plNew.pl_PositionVector += vOffsetRel;
        Teleport(plNew);

      // error -> teleport to zero
      } else {
        ASSERTALWAYS("Unknown world link type");
        Teleport(CPlacement3D(FLOAT3D(0, 0, 0)+vOffsetRel, ANGLE3D(0, 0, 0)));
      }

      // if there is a start trigger target
      if (CpmStart.m_penTarget!=NULL) {
        SendToTarget(CpmStart.m_penTarget, EET_TRIGGER, this);
      }

    // default start position
    } else {
      // set player characteristics
      SetHealthInit(TopHealth());
      m_iMana = GetSP()->sp_iInitialMana;
      m_iArmor = 0;

      // set weapons
      GetInventory()->InitializeWeapons(0, 0, 0, 0);
      InitializeWeapons();

      // start position
      Teleport(CPlacement3D(FLOAT3D(0, 0, 0)+vOffsetRel, ANGLE3D(0, 0, 0)));
    }

    // send teleport event to all entities in range
    SendEventInRange(ETeleport(), FLOATaabbox3D(GetPlacement().pl_PositionVector, 200.0f));

    // stop moving
    ForceFullStop();

    // if in singleplayer mode
    if (GetSP()->sp_bSinglePlayer && GetSP()->sp_gmGameMode!=CSessionProperties::GM_FLYOVER) {
      CWorldSettingsControllerEntity *pwsc = GetWSC(this);

      if (pwsc!=NULL && pwsc->m_bNoSaveGame) {
        NOTHING;
      } else {
        // save quick savegame
        _pShell->Execute("gam_bQuickSave=1;");
      }
    }

    // remember level start time
    if (!(m_ulFlags&PLF_LEVELSTARTED)) {
      m_ulFlags |= PLF_LEVELSTARTED;
      m_tmLevelStarted = _pNetwork->GetGameTime();
    }

    // reset model appearance
    CTString strDummy;
    SetPlayerAppearance(GetModelObject(), NULL, strDummy, /*bPreview=*/FALSE);
    ValidateCharacter();
    SetPlayerAppearance(&m_moRender, GetCharacter(), strDummy, /*bPreview=*/FALSE);
    ParseGender(strDummy);

    GetPlayerAnimator()->SetWeapon();
    m_ulFlags |= PLF_SYNCWEAPON;

    // spawn teleport effect
    SpawnTeleport();

    // return from editor model (if was fragged into pieces)
    SwitchToModel();
    m_tmSpawned = _pTimer->CurrentTick();

    FLOAT tmProtection = GetSP()->sp_tmSpawnInvulnerability;
    CStatusEffectInstance stef(E_STEF_SPAWN_PROTECTION, 0, tmProtection);
    GetInventory()->AddStatusEffect(stef, 0.0F);

    en_tmLastBreathed = _pTimer->CurrentTick()+0.1f;  // do not take breath when spawned in air
  };

  // note: set estimated time in advance
  void RecordEndOfLevelData(void)
  {
    // must not be called multiple times
    ASSERT(!m_bEndOfLevel);

    // clear analyses message
    m_tmAnalyseEnd = 0;
    m_bPendingMessage = FALSE;
    m_tmMessagePlay = 0;

    // mark end of level
    m_iMayRespawn = 0;
    m_bEndOfLevel = TRUE;

    // remember end time
    time((time_t*)&m_iEndTime);

    // add time score
    TIME tmLevelTime = _pTimer->CurrentTick()-m_tmLevelStarted;
    m_psLevelStats.ps_tmTime = tmLevelTime;
    m_psGameStats.ps_tmTime += tmLevelTime;
    FLOAT fTimeDelta = ClampDn((FLOAT)(floor(m_tmEstTime)-floor(tmLevelTime)), 0.0f);
    m_iTimeScore = floor(fTimeDelta*100.0f);
    m_psLevelStats.ps_iScore+=m_iTimeScore;
    m_psGameStats.ps_iScore+=m_iTimeScore;

    // record stats for this level and add to global table
    CTString strStats;
    strStats.PrintF(TRANS("%s\n  Time:   %s\n  Score: %9d\n  Kills:   %03d/%03d\n  Secrets:   %02d/%02d\n"),
        TranslateConst(en_pwoWorld->GetName(), 0), TimeToString(tmLevelTime),
        m_psLevelStats.ps_iScore,
        m_psLevelStats.ps_iKills, m_psLevelTotal.ps_iKills,
        m_psLevelStats.ps_iSecrets, m_psLevelTotal.ps_iSecrets);
    m_strLevelStats += strStats;
  }

  // spawn teleport effect
  void SpawnTeleport(void)
  {
    // if in singleplayer
    if (GetSP()->sp_bSinglePlayer) {
      // no spawn effects
      return;
    }

    ESpawnEffect ese;
    ese.colMuliplier = C_WHITE|CT_OPAQUE;
    ese.betType = BET_TELEPORT;
    ese.vNormal = FLOAT3D(0,1,0);
    FLOATaabbox3D box;
    GetBoundingBox(box);
    FLOAT fEntitySize = box.Size().MaxNorm()*2;
    ese.vStretch = FLOAT3D(fEntitySize, fEntitySize, fEntitySize);
    CEntityPointer penEffect = CreateEntity(GetPlacement(), CLASS_BASIC_EFFECT);
    penEffect->Initialize(ese);
  }

  // render particles
  void RenderParticles(void)
  {
    FLOAT tmNow = _pTimer->GetLerpedCurrentTick();

    // render empty shells
    Particles_EmptyShells(this, m_asldData);

    if (Particle_GetViewer()==this) {
      Particles_ViewerLocal(this);

    } else {
      // if is not first person
      RenderChainsawParticles(TRUE);

      // glowing powerups
      if (GetFlags()&ENF_ALIVE) {
        CStatusEffectInstance *pDamage = GetInventory()->GetStrongestStatusEffectInstance(E_STEF_DAMAGE);
        CStatusEffectInstance *pSpeed = GetInventory()->GetStrongestStatusEffectInstance(E_STEF_SPEED);
        CStatusEffectInstance *pInvulnerability = GetInventory()->GetStrongestStatusEffectInstance(E_STEF_INVULNERABILITY);

        //CDebugF("dmg = %d, speed = %d, invul = %d\n", iDamageId, iSpeedId, iInvulId);

        FLOAT tmSeriousDamage = pDamage ? pDamage->GetDuration() : 0.0F;
        FLOAT tmSeriousSpeed = pSpeed ? pSpeed->GetDuration() : 0.0F;
        FLOAT tmInvulnerability = pInvulnerability ? pInvulnerability->GetDuration() : 0.0F;

        if (tmSeriousDamage > 0.0F && tmInvulnerability > 0.0F) {
          Particles_ModelGlow(this, Max(tmSeriousDamage + tmNow, tmInvulnerability + tmNow), PT_STAR08, 0.15f, 2, 0.03f, 0xff00ff00);

        } else if (tmInvulnerability > 0.0F) {
          Particles_ModelGlow(this, tmInvulnerability + tmNow, PT_STAR05, 0.15f, 2, 0.03f, 0x3333ff00);

        } else if (tmSeriousDamage > 0.0F) {
          Particles_ModelGlow(this, tmSeriousDamage + tmNow, PT_STAR08, 0.15f, 2, 0.03f, 0xff777700);
        }

        if (tmSeriousSpeed > 0.0F) {
          Particles_RunAfterBurner(this, tmNow + tmSeriousSpeed, 0.3f, 0);
        }

        if (!GetSP()->sp_bCooperative) {
          CPlayerWeaponEntity *wpn = GetWeapons(E_HAND_MAIN);

          if (wpn->m_tmLastSniperFire == _pTimer->CurrentTick())
          {
            CAttachmentModelObject &amoBody = *GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO);
            FLOATmatrix3D m;
            MakeRotationMatrix(m, amoBody.amo_plRelative.pl_OrientationAngle);
            FLOAT3D vSource = wpn->m_vBulletSource + FLOAT3D(0.0f, 0.1f, -0.4f)*GetRotationMatrix()*m;
            Particles_SniperResidue(this, vSource , wpn->m_vBulletTarget);
          }
        }
      }
    }

    // spirit particles
    if (m_tmSpiritStart != 0.0f)
    {
      Particles_Appearing(this, m_tmSpiritStart);
    }
  }

  void TeleportToAutoMarker(CPlayerActionMarkerEntity *ppam)
  {
    // if we are in coop
    if (GetSP()->sp_bCooperative && !GetSP()->sp_bSinglePlayer) {
      // for each player
      for (INDEX iPlayer=0; iPlayer<GetMaxPlayers(); iPlayer++)
      {
        CPlayerPawnEntity *ppl = (CPlayerPawnEntity*)GetPlayerEntity(iPlayer);

        if (ppl!=NULL) {
          // put it at marker
          CPlacement3D pl = ppam->GetPlacement();
          FLOAT3D vOffsetRel = ppl->GetTeleportingOffset();
          pl.pl_PositionVector += vOffsetRel*ppam->en_mRotation;
          ppl->Teleport(pl, FALSE);

          // remember new respawn place
          ppl->m_vDied = pl.pl_PositionVector;
          ppl->m_aDied = pl.pl_OrientationAngle;
        }
      }

    // otherwise
    } else {
      // put yourself at marker
      CPlacement3D pl = ppam->GetPlacement();
      FLOAT3D vOffsetRel = GetTeleportingOffset();
      pl.pl_PositionVector += vOffsetRel*ppam->en_mRotation;
      Teleport(pl, FALSE);
    }
  }

  // check whether this time we respawn in place or on marker
  void CheckDeathForRespawnInPlace(EDeath eDeath)
  {
    BOOL bRespawnInPlace = GetSP()->sp_ulCoopFlags & CPF_RESPAWN_IN_PLACE;
    
    // If respawning in place is not allowed then skip further checks.
    if (!bRespawnInPlace) {
      return;
    }

    // if killed by a player or enemy
    CEntity *penKiller = eDeath.eLastDamage.penInflictor;

    if (IsDerivedFromClass(penKiller, &CPlayerPawnEntity_DLLClass) || IsDerivedFromClass(penKiller, &CEnemyBaseEntity_DLLClass)) {
      // mark for respawning in place
      m_ulFlags |= PLF_RESPAWNINPLACE;
      m_vDied = GetPlacement().pl_PositionVector;
      m_aDied = GetPlacement().pl_OrientationAngle;
    }
  }

  void TransferKeys()
  {
    // find first live player
    CPlayerPawnEntity *penNextPlayer = NULL;

    for (INDEX iPlayer = 0; iPlayer < GetMaxPlayers(); iPlayer++)
    {
      CPlayerPawnEntity *pen = (CPlayerPawnEntity*)&*GetPlayerEntity(iPlayer);

      if (pen != NULL && pen != this && (pen->GetFlags()&ENF_ALIVE) && !(pen->GetFlags()&ENF_DELETED) ) {
        penNextPlayer = pen;
      }
    }

    // if any found
    if (penNextPlayer != NULL) {
      // transfer keys to that player
      CInfoF(TRANS("%s leaving, all keys transfered to %s\n"),
        (const char*)m_strName, (const char*)penNextPlayer->GetPlayerName());
      penNextPlayer->GetInventory()->m_ulKeys |= GetInventory()->m_ulKeys;
      return;
    }
    
    // Transfer keys to CGameInfoEntity.
    if (GetGameInfo() != NULL) {
      CInfoF(TRANS("%s leaving, all keys transfered to CGameInfoEntity instance.\n"), (const char*)m_strName);
      GetGameInfo()->StoreKeys(GetInventory()->m_ulKeys);
    }
  }

  void OnOpenCheat()
  {
    GetWeapons(E_HAND_MAIN)->CheatOpen();
  }

  void OnGiveAllCheat(ULONG ulFlags)
  {
    GetInventory()->OnGiveAllCheat(ulFlags);

    if (ulFlags & GACF_WEAPONS) {
      GetWeapons(E_HAND_MAIN)->Precache();
    }

    if (ulFlags & GACF_GADGETS) {
      SelectNewGadget();
    }

    if (ulFlags & GACF_MESSAGES) {
      //CheatAllMessagesDir("Data\\Messages\\information\\");
      //CheatAllMessagesDir("Data\\Messages\\background\\");
      //CheatAllMessagesDir("Data\\Messages\\statistics\\");
      CheatAllMessagesDir("Data\\Messages\\weapons\\", 0);
      CheatAllMessagesDir("Data\\Messages\\enemies\\", 0);
      CheatAllMessagesDir("DataMP\\Messages\\enemies\\", 0);
      CheatAllMessagesDir("DataMP\\Messages\\information\\", 0);
      CheatAllMessagesDir("DataMP\\Messages\\statistics\\", 0);
      CheatAllMessagesDir("DataMP\\Messages\\weapons\\", 0);
      CheatAllMessagesDir("DataMP\\Messages\\background\\", 0);
    }
    
    if (ulFlags & GACF_BUFFS) {
      for (INDEX i = E_STEF_INVALID + 1; i < E_STEF_MAX; i++)
      {
        const CStatusEffectDefinition *pEffectDefintion = GetStatusEffectDefinition((StatusEffectIndex)i);
        
        if (!pEffectDefintion->IsBeneficial()) {
          continue;
        }

        CStatusEffectInstance stef((StatusEffectIndex)i, 0, 30.0F);
        GetInventory()->AddStatusEffect(stef, 0.0F);
      }
    }
  }
  
  void OnRefreshCheat()
  {
    SetHealth(TopHealth());
  }
  
  void OnKillAllCheat()
  {
    KillAllEnemies(this);
  }

procedures:
/************************************************************
 *                       WOUNDED                            *
 ************************************************************/
  Wounded(EDamage eDamage)
  {
    return;
  };

/************************************************************
 *                     WORLD CHANGE                         *
 ************************************************************/
  WorldChange()
  {
    // if in single player then mark world as visited
    if (GetSP()->sp_bSinglePlayer) {
      CTString strDummy("1");
      SaveStringVar(GetWorld()->wo_fnmFileName.NoExt()+".vis", strDummy);
    }

    // find music holder on new world
    FindMusicHolder();
    FindGameInfo();

    // store group name
    m_strGroup = _SwcWorldChange.strGroup;
    TeleportPlayer((WorldLinkType)_SwcWorldChange.iType);

    // setup light source
    SetupLightSource();

    // make sure we discontinue zooming
    ResetWeaponZoom();

    // turn off possible chainsaw engine sound
    PlaySound(m_soWeaponAmbient, SOUND_SILENCE, SOF_3D);

    // update per-level stats
    UpdateLevelStats();
    m_ulFlags |= PLF_INITIALIZED;
    m_ulFlags &= ~PLF_CHANGINGLEVEL;
    return;
  };

  WorldChangeDead()
  {
    // forbid respawning in-place when changing levels while dead
    m_ulFlags &= ~PLF_RESPAWNINPLACE;

    // if in single player
    if (GetSP()->sp_bSinglePlayer) {
      // mark world as visited
      CTString strDummy("1");
      SaveStringVar(GetWorld()->wo_fnmFileName.NoExt()+".vis", strDummy);
    }

    // find music holder on new world
    FindMusicHolder();
    FindGameInfo();
    
    // store group name

    autocall Rebirth() EReturn;

    // setup light source
    SetupLightSource();

    // update per-level stats
    UpdateLevelStats();
    m_ulFlags |= PLF_INITIALIZED;
    m_ulFlags &= ~PLF_CHANGINGLEVEL;
    return;
  }

/************************************************************
 *                       D E A T H                          *
 ************************************************************/

  Death(EDeath eDeath)
  {
    // stop firing when dead
    ReleaseWeapon(E_HAND_BOTH);

    // stop all looping ifeel effects
    if (_pNetwork->IsPlayerLocal(this))
    {
      IFeel_StopEffect("ChainsawFire");
      IFeel_StopEffect("FlamethrowerFire");
      IFeel_StopEffect("ChainsawIdle");
      IFeel_StopEffect("SniperZoom");
      IFeel_StopEffect("Minigun_rotate");
    }

    // make sure sniper zoom is stopped
    ResetWeaponZoom();

    // stop weapon sounds
    PlaySound(m_soSniperZoom, SOUND_SILENCE, SOF_3D);
    PlaySound(m_soWeaponAmbient, SOUND_SILENCE, SOF_3D);

    // stop rotating chaingun
    GetWeapons(E_HAND_MAIN)->m_aChaingunLast = GetWeapons(E_HAND_MAIN)->m_aChaingun;
    GetWeapons(E_HAND_SECOND)->m_aChaingunLast = GetWeapons(E_HAND_SECOND)->m_aChaingun;
    
    GetWeapons(E_HAND_MAIN)->DestroyEnergyBeam();
    GetWeapons(E_HAND_SECOND)->DestroyEnergyBeam();

    // if in single player, or if this is a predictor entity
    if (GetSP()->sp_bSinglePlayer || IsPredictor()) {
      // do not print anything
      NOTHING;

    // if in cooperative, but not single player
    } else if (GetSP()->sp_bCooperative) {
      // just print death message, no score updating
      PrintPlayerDeathMessage(this, eDeath);

      // check whether this time we respawn in place or on marker
      CheckDeathForRespawnInPlace(eDeath);

      // increase number of deaths
      m_psLevelStats.ps_iDeaths += 1;
      m_psGameStats.ps_iDeaths += 1;

    // if not in cooperative, and not single player
    } else {
      // print death message
      PrintPlayerDeathMessage(this, eDeath);

      // get the killer pointer
      CEntity *penKiller = eDeath.eLastDamage.penInflictor;

      // initially, not killed by a player
      CPlayerPawnEntity *pplKillerPlayer = NULL;

      // if killed by some entity
      if (penKiller!=NULL) {
        // if killed by player
        if (IsDerivedFromClass(penKiller, &CPlayerPawnEntity_DLLClass)) {

          // if someone other then you
          if (penKiller!=this) {
            pplKillerPlayer = (CPlayerPawnEntity*)penKiller;
            EReceiveScore eScore;
            eScore.iPoints = m_iMana;
            eDeath.eLastDamage.penInflictor->SendEvent(eScore);
            eDeath.eLastDamage.penInflictor->SendEvent(EKilledEnemy());

          // if it was yourself
          } else {
            m_psLevelStats.ps_iScore -= m_iMana;
            m_psGameStats.ps_iScore -= m_iMana;
            m_psLevelStats.ps_iKills -= 1;
            m_psGameStats.ps_iKills -= 1;
          }

        // if killed by non-player
        } else {
          m_psLevelStats.ps_iScore -= m_iMana;
          m_psGameStats.ps_iScore -= m_iMana;
          m_psLevelStats.ps_iKills -= 1;
          m_psGameStats.ps_iKills -= 1;
        }

      // if killed by NULL (shouldn't happen, but anyway)
      } else {
        m_psLevelStats.ps_iScore -= m_iMana;
        m_psGameStats.ps_iScore -= m_iMana;
        m_psLevelStats.ps_iKills -= 1;
        m_psGameStats.ps_iKills -= 1;
      }

      // if playing scorematch
      if (!GetSP()->sp_bUseFrags) {
        // if killed by a player
        if (pplKillerPlayer!=NULL) {
          // print how much that player gained
          CInfoF(TRANS("  %s: +%d points\n"), pplKillerPlayer->GetPlayerName(), m_iMana);

        // if it was a suicide, or an accident
        } else {
          // print how much you lost
          CInfoF(TRANS("  %s: -%d points\n"), GetPlayerName(), m_iMana);
        }
      }

      // increase number of deaths
      m_psLevelStats.ps_iDeaths += 1;
      m_psGameStats.ps_iDeaths += 1;
    }

    // store last view
    m_iLastViewState = m_iViewState;

    // mark player as death
    SetFlags(GetFlags()&~ENF_ALIVE);

    // stop player
    SetDesiredTranslation(FLOAT3D(0.0f, 0.0f, 0.0f));
    SetDesiredRotation(ANGLE3D(0.0f, 0.0f, 0.0f));

    // remove weapon from hand
    GetPlayerAnimator()->RemoveWeapon();

    // kill weapon animations
    StopWeapons();

    // if in deathmatch
    if (!GetSP()->sp_bCooperative) {
      // drop current weapon as item so others can pick it
      GetWeapons(E_HAND_MAIN)->DropWeapon();
    }

    // play death
    INDEX iAnim1;
    INDEX iAnim2;

    if (m_pstState == PST_SWIM || m_pstState == PST_DIVE) {
      iAnim1 = PLAYER_ANIM_DEATH_UNDERWATER;
      iAnim2 = BODY_ANIM_DEATH_UNDERWATER;

    } else if (eDeath.eLastDamage.iDamageType==DMT_SPIKESTAB) {
      iAnim1 = PLAYER_ANIM_DEATH_SPIKES;
      iAnim2 = BODY_ANIM_DEATH_SPIKES;

    } else if (eDeath.eLastDamage.iDamageType==DMT_ABYSS) {
      iAnim1 = PLAYER_ANIM_ABYSSFALL;
      iAnim2 = BODY_ANIM_ABYSSFALL;

    } else {
      FLOAT3D vFront;
      GetHeadingDirection(0, vFront);
      FLOAT fDamageDir = m_vDamage%vFront;

      if (fDamageDir<0) {
        if (Abs(fDamageDir)<10.0f) {
          iAnim1 = PLAYER_ANIM_DEATH_EASYFALLBACK;
          iAnim2 = BODY_ANIM_DEATH_EASYFALLBACK;
        } else {
          iAnim1 = PLAYER_ANIM_DEATH_BACK;
          iAnim2 = BODY_ANIM_DEATH_BACK;
        }

      } else {
        if (Abs(fDamageDir)<10.0f) {
          iAnim1 = PLAYER_ANIM_DEATH_EASYFALLFORWARD;
          iAnim2 = BODY_ANIM_DEATH_EASYFALLFORWARD;
        } else {
          iAnim1 = PLAYER_ANIM_DEATH_FORWARD;
          iAnim2 = BODY_ANIM_DEATH_FORWARD;
        }
      }
    }
    en_plViewpoint.pl_OrientationAngle = ANGLE3D(0,0,0);
    StartModelAnim(iAnim1, 0);
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(iAnim2, 0);

    // set physic flags
    SetPhysicsFlags(EPF_MODEL_CORPSE);
    SetCollisionFlags(ECF_CORPSE);

    // set density to float out of water
    en_fDensity = 400.0f;

    // play sound
    if (m_pstState==PST_DIVE) {
      SetDefaultMouthPitch();
      PlaySound(m_soMouth, GenderSound(SOUND_DEATHWATER), SOF_3D);
      if (_pNetwork->IsPlayerLocal(this)) {IFeel_PlayEffect("DeathWater");}

    } else {
      SetDefaultMouthPitch();
      PlaySound(m_soMouth, GenderSound(SOUND_DEATH), SOF_3D);
      if (_pNetwork->IsPlayerLocal(this)) {IFeel_PlayEffect("Death");}
    }

    // initialize death camera view
    ASSERT(m_penView == NULL);
    if (m_penView == NULL) {
      m_penView = CreateEntity(GetPlacement(), CLASS_PLAYER_VIEW);
      EViewInit eInit;
      eInit.penOwner = this;
      eInit.penCamera = NULL;
      eInit.vtView = VT_PLAYERDEATH;
      eInit.bDeathFixed = eDeath.eLastDamage.iDamageType==DMT_ABYSS;
      m_penView->Initialize(eInit);
    }

    if (ShouldBlowUp()) {
      BlowUp();
    } else {
      // leave a stain beneath
      LeaveStain(TRUE);
    }

    m_iMayRespawn = 0;

    // wait for anim of death
    wait (1.2f)
    {
      on (EBegin) : {
        // set new view status
        m_iViewState = PVT_PLAYERAUTOVIEW;
        resume;
      }

      // when anim is finished
      on (ETimer) : {
        // allow respawning
        m_iMayRespawn = 1;
        resume;
      }

      // when damaged
      on (EDamage eDamage) :
      {
        if (eDamage.iDamageType == DMT_ABYSS) {
          if (m_penView != NULL) {
            GetView()->m_bFixed = TRUE;
          }
        }

        // if should blow up now (and not already blown up)
        if (ShouldBlowUp()) {
          // do it
          BlowUp();
        }

        resume;
      }

      on (EDeath) : { resume; }

      // if player pressed fire
      on (EEnd) : {
        // NOTE: predictors must never respawn since player markers for respawning are not predicted
        // if this is not predictor
        if (!IsPredictor()) {
          // stop waiting
          stop;
        }
      }

      // if autoaction is received
      on (EAutoAction eAutoAction) :
      {
        // if we are in coop
        if (GetSP()->sp_bCooperative && !GetSP()->sp_bSinglePlayer) {
          // if the marker is teleport marker
          if (eAutoAction.penFirstMarker!=NULL &&
            ((CPlayerActionMarkerEntity*)&*eAutoAction.penFirstMarker)->m_paaAction == PAA_TELEPORT) {
            // teleport there
            TeleportToAutoMarker((CPlayerActionMarkerEntity*)&*eAutoAction.penFirstMarker);
          }
        }

        // ignore the actions
        resume;
      }

      on (EDisconnected) : { pass; }
      on (EReceiveScore) : { pass; }
      on (EKilledEnemy) : { pass; }
      on (EPreLevelChange) : { pass; }
      on (EPostLevelChange) : { pass; }
      otherwise() : { resume; }
    }

    return ERebirth();
  };

  TheEnd()
  {
    // if not playing demo
    if (!_pNetwork->IsPlayingDemo()) {
      // record high score in single player only
      if (GetSP()->sp_bSinglePlayer) {
        _pShell->Execute("gam_iRecordHighScore=0;");
      }
    }

    // if current difficulty is serious
    if (GetSP()->sp_gdGameDifficulty==CSessionProperties::GD_EXTREME) {
      // activate the mental mode
      _pShell->Execute("sam_bMentalActivated=1;");
    }

    // stop firing when end
    ReleaseWeapon(E_HAND_BOTH);

    // mark player as dead
    SetFlags(GetFlags()&~ENF_ALIVE);

    // stop player
    SetDesiredTranslation(FLOAT3D(0.0f, 0.0f, 0.0f));
    SetDesiredRotation(ANGLE3D(0.0f, 0.0f, 0.0f));

    // look straight
    StartModelAnim(PLAYER_ANIM_STAND, 0);
    GetPlayerAnimator()->BodyAnimationTemplate(
      BODY_ANIM_NORMALWALK, BODY_ANIM_COLT_STAND, BODY_ANIM_SHOTGUN_STAND, BODY_ANIM_MINIGUN_STAND,
      AOF_LOOPING|AOF_NORESTART);

    en_plViewpoint.pl_OrientationAngle = ANGLE3D(0,0,0);

    // call computer
    m_bEndOfGame = TRUE;
    SetGameEnd();

    wait ()
    {
      on (EBegin) : { resume; }
      on (EReceiveScore) : { pass; }
      on (EKilledEnemy) : { pass; }
      on (ECenterMessage) : { pass; }
      otherwise() : { resume; }
    }
  };

/************************************************************
 *                      R E B I R T H                       *
 ************************************************************/
  FirstInit()
  {
    // clear use button and zoom flag
    bAimButtonHeld = FALSE;

    // restore last view
    m_iViewState = m_iLastViewState;

    // stop and kill camera
    if (m_penView != NULL) {
      m_penView->SendEvent(EEnd());
      m_penView = NULL;
    }

    FindMusicHolder();
    FindGameInfo();

    // update per-level stats
    UpdateLevelStats();

    // initialize player (from PlayerMarker)
    InitializePlayer();

    // add statistics message
    ReceiveComputerMessage(CTFILENAME("Data\\Messages\\Statistics\\Statistics.txt"), CMF_READ);

    if (GetSettings() != NULL && GetSettings()->ps_ulFlags&PSF_PREFER3RDPERSON) {
      ChangePlayerView();
    }
    
    GetGameInfo()->GiveStoredKeys(this);

    return;
  };

  Rebirth()
  {
    bAimButtonHeld = FALSE;

    // restore last view
    m_iViewState = m_iLastViewState;

    // clear ammunition
    if (!(m_ulFlags&PLF_RESPAWNINPLACE)) {
      GetInventory()->ClearAmmo();
      GetInventory()->ClearWeapons();
    }

    // stop and kill camera
    if (m_penView != NULL) {
      m_penView->SendEvent(EEnd());
      m_penView = NULL;
    }

    // stop and kill flame
    CEntityPointer penFlame = GetChildOfClass("Flame");
    if (penFlame!=NULL)
    {
      // send the event to stop burning
      EStopFlaming esf;
      esf.m_bNow=TRUE;
      penFlame->SendEvent(esf);
    }

    if (m_penView != NULL) {
      m_penView->SendEvent(EEnd());
      m_penView = NULL;
    }

    FindMusicHolder();
    FindGameInfo();

    // initialize player (from PlayerMarker)
    InitializePlayer();

    return EReturn();
  };


  // auto action - go to current marker
  AutoGoToMarker(EVoid)
  {
    ULONG ulFlags = AOF_LOOPING|AOF_NORESTART;

    INDEX iAnim = GetModelObject()->GetAnim();

    if (iAnim!=PLAYER_ANIM_STAND)
    {
      ulFlags |= AOF_SMOOTHCHANGE;
    }

    GetPlayerAnimator()->m_bAttacking = FALSE;
    GetPlayerAnimator()->BodyWalkAnimation();

    if (m_fAutoSpeed>plr_fSpeedForward/2) {
      StartModelAnim(PLAYER_ANIM_RUN, ulFlags);
    } else {
      StartModelAnim(PLAYER_ANIM_NORMALWALK, ulFlags);
    }

    // while not at marker
    while(
      (m_penActionMarker->GetPlacement().pl_PositionVector-
       GetPlacement().pl_PositionVector).Length()>1.0f) {

      // wait a bit
      autowait(_pTimer->TickQuantum);
    }

    // return to auto-action loop
    return EReturn();
  }

  // auto action - go to current marker and stop there
  AutoGoToMarkerAndStop(EVoid)
  {
    ULONG ulFlags = AOF_LOOPING|AOF_NORESTART;

    INDEX iAnim = GetModelObject()->GetAnim();
    if (iAnim!=PLAYER_ANIM_STAND)
    {
      ulFlags |= AOF_SMOOTHCHANGE;
    }

    GetPlayerAnimator()->BodyWalkAnimation();

    if (m_fAutoSpeed>plr_fSpeedForward/2) {
      StartModelAnim(PLAYER_ANIM_RUN, ulFlags);
    } else {
      StartModelAnim(PLAYER_ANIM_NORMALWALK, ulFlags);
    }

    // while not at marker
    while (
      (m_penActionMarker->GetPlacement().pl_PositionVector-
       GetPlacement().pl_PositionVector).Length()>m_fAutoSpeed*_pTimer->TickQuantum*2.00f) {
      // wait a bit
      autowait(_pTimer->TickQuantum);
    }

    // disable auto speed
    m_fAutoSpeed = 0.0f;

    GetPlayerAnimator()->BodyStillAnimation();
    StartModelAnim(PLAYER_ANIM_STAND, AOF_LOOPING|AOF_NORESTART);

    // stop moving
    ForceFullStop();

    // return to auto-action loop
    return EReturn();
  }

  // auto action - use an item
  AutoUseItem(EVoid)
  {
    // start pulling the item
    GetPlayerAnimator()->BodyPullItemAnimation();
    //StartModelAnim(PLAYER_ANIM_STATUE_PULL, 0);

    autowait(0.2f);

    // item appears
    CPlayerActionMarkerEntity *ppam = GetActionMarker();
    if (IsOfClass(ppam->m_penItem, "KeyItem")) {
      CModelObject &moItem = ppam->m_penItem->GetModelObject()->GetAttachmentModel(0)->amo_moModelObject;
      GetPlayerAnimator()->SetItem(&moItem);
    }

    autowait(2.20f-0.2f);

    // the item is in place
    GetPlayerAnimator()->BodyRemoveItem();

    // if marker points to a trigger
    if (GetActionMarker()->m_penTrigger!=NULL) {
      // trigger it
      SendToTarget(GetActionMarker()->m_penTrigger, EET_TRIGGER, this);
    }

    // fake that player has passed through the door controller
    if (GetActionMarker()->m_penDoorController!=NULL) {
      EPass ePass;
      ePass.penOther = this;
      GetActionMarker()->m_penDoorController->SendEvent(ePass);
    }

    autowait(3.25f-2.20f);

    GetPlayerAnimator()->BodyRemoveItem();

    // return to auto-action loop
    return EReturn();
  }

  // auto action - pick an item
  AutoPickItem(EVoid)
  {
    // start pulling the item
    GetPlayerAnimator()->BodyPickItemAnimation();
    StartModelAnim(PLAYER_ANIM_KEYLIFT, 0);

    autowait(1.2f);

    // if marker points to a trigger
    if (GetActionMarker()->m_penTrigger!=NULL) {
      // trigger it
      SendToTarget(GetActionMarker()->m_penTrigger, EET_TRIGGER, this);
    }

    // item appears
    CPlayerActionMarkerEntity *ppam = GetActionMarker();
    if (IsOfClass(ppam->m_penItem, "KeyItem")) {
      CModelObject &moItem = ppam->m_penItem->GetModelObject()->GetAttachmentModel(0)->amo_moModelObject;
      GetPlayerAnimator()->SetItem(&moItem);
      EPass ePass;
      ePass.penOther = this;
      ppam->m_penItem->SendEvent(ePass);
    }

    autowait(3.6f-1.2f+GetActionMarker()->m_tmWait);

    GetPlayerAnimator()->BodyRemoveItem();

    // return to auto-action loop
    return EReturn();
  }

  AutoFallDown(EVoid)
  {
    StartModelAnim(PLAYER_ANIM_BRIDGEFALLPOSE, 0);
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(BODY_ANIM_BRIDGEFALLPOSE, 0);

    autowait(GetActionMarker()->m_tmWait);

    // return to auto-action loop
    return EReturn();
  }

  AutoFallToAbys(EVoid)
  {
    StartModelAnim(PLAYER_ANIM_ABYSSFALL, AOF_LOOPING);
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(BODY_ANIM_ABYSSFALL, AOF_LOOPING);

    autowait(GetActionMarker()->m_tmWait);

    // return to auto-action loop
    return EReturn();
  }

  // auto action - look around
  AutoLookAround(EVoid)
  {
    StartModelAnim(PLAYER_ANIM_BACKPEDAL, 0);
    m_vAutoSpeed = FLOAT3D(0,0,plr_fSpeedForward/4/0.75f);
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(BODY_ANIM_NORMALWALK, 0);

    autowait(GetModelObject()->GetCurrentAnimLength()/2);

    m_vAutoSpeed = FLOAT3D(0, 0, 0);

    // start looking around
    StartModelAnim(PLAYER_ANIM_STAND, 0);
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(BODY_ANIM_LOOKAROUND, 0);

    // wait given time
    autowait(moBody.GetCurrentAnimLength()+0.1f);

    // return to auto-action loop
    return EReturn();
  }

  AutoTeleport(EVoid)
  {
    // teleport there
    TeleportToAutoMarker(GetActionMarker());

    // return to auto-action loop
    return EReturn();
  }

  AutoAppear(EVoid)
  {
    // hide the model
    SwitchToEditorModel();

    // put it at marker
    Teleport(GetActionMarker()->GetPlacement());
    // make it rotate in spawnpose
    SetPhysicsFlags(GetPhysicsFlags() & ~(EPF_TRANSLATEDBYGRAVITY|EPF_ORIENTEDBYGRAVITY));
    m_ulFlags|=PLF_AUTOMOVEMENTS;
    SetDesiredRotation(ANGLE3D(60,0,0));
    StartModelAnim(PLAYER_ANIM_SPAWNPOSE, AOF_LOOPING);
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(BODY_ANIM_SPAWNPOSE, AOF_LOOPING);

    // start stardust appearing
    m_tmSpiritStart = _pTimer->CurrentTick();
    // wait till it appears
    autowait(5);

    // start model appearing
    SwitchToModel();
    m_tmFadeStart = _pTimer->CurrentTick();
    // wait till it appears
    autowait(5);
    // fixate full opacity
    COLOR colAlpha = GetModelObject()->mo_colBlendColor;
    GetModelObject()->mo_colBlendColor = colAlpha|0xFF;

    // put it to normal state
    SetPhysicsFlags(GetPhysicsFlags() | EPF_TRANSLATEDBYGRAVITY|EPF_ORIENTEDBYGRAVITY);
    SetDesiredRotation(ANGLE3D(0,0,0));
    m_ulFlags&=~PLF_AUTOMOVEMENTS;

    // play animation to fall down
    StartModelAnim(PLAYER_ANIM_SPAWN_FALLDOWN, 0);
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(BODY_ANIM_SPAWN_FALLDOWN, 0);

    autowait(GetModelObject()->GetCurrentAnimLength());

    // play animation to get up
    StartModelAnim(PLAYER_ANIM_SPAWN_GETUP, AOF_SMOOTHCHANGE);
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(BODY_ANIM_SPAWN_GETUP, AOF_SMOOTHCHANGE);

    autowait(GetModelObject()->GetCurrentAnimLength());

    // return to auto-action loop
    return EReturn();
  }

  TravellingInBeam()
  {
    // put it at marker
    Teleport(GetActionMarker()->GetPlacement());

    // make it rotate in spawnpose
    SetPhysicsFlags(GetPhysicsFlags() & ~(EPF_TRANSLATEDBYGRAVITY|EPF_ORIENTEDBYGRAVITY));
    m_ulFlags|=PLF_AUTOMOVEMENTS;
    SetDesiredRotation(ANGLE3D(60,0,0));
    SetDesiredTranslation(ANGLE3D(0,20.0f,0));
    StartModelAnim(PLAYER_ANIM_SPAWNPOSE, AOF_LOOPING);
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(BODY_ANIM_SPAWNPOSE, AOF_LOOPING);

    // wait till it appears
    autowait(8.0f);

    // switch to model
    SwitchToEditorModel();

    // return to auto-action loop
    return EReturn();
  }

  LogoFireMinigun(EVoid)
  {
    // put it at marker
    CPlacement3D pl = GetActionMarker()->GetPlacement();
    pl.pl_PositionVector += FLOAT3D(0, 0.01f, 0)*GetActionMarker()->en_mRotation;
    Teleport(pl);
    en_plViewpoint.pl_OrientationAngle(1) = 20.0f;
    en_plLastViewpoint.pl_OrientationAngle = en_plViewpoint.pl_OrientationAngle;

    // stand in pose
    StartModelAnim(PLAYER_ANIM_INTRO, AOF_LOOPING);

    // remember time for rotating view start
    m_tmMinigunAutoFireStart = _pTimer->CurrentTick();

    // wait some time for fade in and to look from left to right with out firing
    //autowait(0.75f);
    GetWeapons(E_HAND_MAIN)->SendEvent(EFireWeapon());
    autowait(2.5f);
    GetWeapons(E_HAND_MAIN)->SendEvent(EReleaseWeapon());

    // stop minigun shaking
    CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
    moBody.PlayAnim(BODY_ANIM_MINIGUN_STAND, 0);

    autowait(0.5f);

    // ---------- Apply shake
    CWorldSettingsControllerEntity *pwsc = NULL;

    // obtain bcg viewer
    CBackgroundViewerEntity *penBcgViewer = (CBackgroundViewerEntity *) GetWorld()->GetBackgroundViewer();
    if (penBcgViewer != NULL)
    {
      pwsc = (CWorldSettingsControllerEntity *) &*penBcgViewer->m_penWorldSettingsController;
      pwsc->m_tmShakeStarted = _pTimer->CurrentTick();
      pwsc->m_vShakePos = GetPlacement().pl_PositionVector;
      pwsc->m_fShakeFalloff = 250.0f;
      pwsc->m_fShakeFade = 3.0f;

      pwsc->m_fShakeIntensityZ = 0.1f*2.0f;
      pwsc->m_tmShakeFrequencyZ = 5.0f;
      pwsc->m_fShakeIntensityY = 0.0f;
      pwsc->m_fShakeIntensityB = 0.0f;

      pwsc->m_bShakeFadeIn = FALSE;

      /*
      pwsc->m_fShakeIntensityY = 0.1f*2.0f;
      pwsc->m_tmShakeFrequencyY = 5.0f;
      pwsc->m_fShakeIntensityB = 2.5f*1.5f;
      pwsc->m_tmShakeFrequencyB = 7.2f;
      */
    }

    // stop rotating body
    m_tmMinigunAutoFireStart = -1;
    autowait(5.0f);
    IFeel_StopEffect(NULL);
    autowait(5.0f);

    return EReturn();
  }

  AutoStoreWeapon(EVoid)
  {
    // store current weapon slowly
    GetPlayerAnimator()->BodyAnimationTemplate(BODY_ANIM_WAIT,
      BODY_ANIM_COLT_REDRAWSLOW, BODY_ANIM_SHOTGUN_REDRAWSLOW, BODY_ANIM_MINIGUN_REDRAWSLOW,
      0);
    autowait(GetPlayerAnimator()->m_fBodyAnimTime);

    m_iAutoOrgWeapon = GetWeapons(E_HAND_MAIN)->GetCurrentWeapon();
    GetWeapons(E_HAND_MAIN)->SetCurrentWeapon(E_WEAPON_NONE);
    GetWeapons(E_HAND_MAIN)->SetWantedWeapon(E_WEAPON_NONE);
    m_soWeaponAmbient.Stop();

    // sync apperances
    GetPlayerAnimator()->SyncWeapon();

    // remove weapon attachment
    GetPlayerAnimator()->m_iWeaponLast = m_iAutoOrgWeapon;
    GetPlayerAnimator()->RemoveWeapon();
    GetPlayerAnimator()->SyncWeapon();

    // temporary set old weapon to play correct deactive->walk transition
    GetWeapons(E_HAND_MAIN)->SetCurrentWeapon((WeaponIndex) m_iAutoOrgWeapon);
    GetPlayerAnimator()->BodyAnimationTemplate(BODY_ANIM_WAIT, BODY_ANIM_COLT_DEACTIVATETOWALK,
      BODY_ANIM_SHOTGUN_DEACTIVATETOWALK, BODY_ANIM_MINIGUN_DEACTIVATETOWALK, AOF_SMOOTHCHANGE);
    GetWeapons(E_HAND_MAIN)->SetCurrentWeapon(E_WEAPON_NONE);

    autowait(GetPlayerAnimator()->m_fBodyAnimTime);

    // return to auto-action loop
    return EReturn();
  }

  // perform player auto actions
  DoAutoActions(EVoid)
  {
    // don't look up/down
    en_plViewpoint.pl_OrientationAngle = ANGLE3D(0,0,0);

    // disable playeranimator animating
    GetPlayerAnimator()->m_bDisableAnimating = TRUE;

    // while there is some marker
    while (m_penActionMarker!=NULL && IsOfClass(m_penActionMarker, "PlayerActionMarker"))
    {
      // if should wait
      if (GetActionMarker()->m_paaAction==PAA_WAIT) {
        // play still anim
        CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
        moBody.PlayAnim(BODY_ANIM_WAIT, AOF_NORESTART|AOF_LOOPING);
        // wait given time
        autowait(GetActionMarker()->m_tmWait);

      } else if (GetActionMarker()->m_paaAction==PAA_STOPANDWAIT) {
        // play still anim
        StartModelAnim(PLAYER_ANIM_STAND, 0);
        CModelObject &moBody = GetModelObject()->GetAttachmentModel(PLAYER_ATTACHMENT_TORSO)->amo_moModelObject;
        moBody.PlayAnim(BODY_ANIM_WAIT, AOF_NORESTART|AOF_LOOPING);
        // wait given time
        autowait(GetActionMarker()->m_tmWait);

      // if should teleport here
      } else if (GetActionMarker()->m_paaAction==PAA_APPEARING) {
        autocall AutoAppear() EReturn;

      } else if (GetActionMarker()->m_paaAction==PAA_TRAVELING_IN_BEAM) {
        autocall TravellingInBeam() EReturn;

      } else if (GetActionMarker()->m_paaAction==PAA_INTROSE_SELECT_WEAPON) {
        // order playerweapons to select weapon
        ESelectWeapon eSelect;
        eSelect.iWeapon = 1;
        GetWeapons(E_HAND_MAIN)->SendEvent(eSelect);

      } else if (GetActionMarker()->m_paaAction==PAA_LOGO_FIRE_INTROSE) {
        autocall LogoFireMinigun() EReturn;

      } else if (GetActionMarker()->m_paaAction==PAA_LOGO_FIRE_MINIGUN) {
        autocall LogoFireMinigun() EReturn;

      // if should appear here
      } else if (GetActionMarker()->m_paaAction==PAA_TELEPORT) {
        autocall AutoTeleport() EReturn;

      // if should wait for trigger
      } else if (GetActionMarker()->m_paaAction==PAA_WAITFOREVER) {
        // wait forever
        wait()
        {
          on (EBegin) : { resume; }
          otherwise() : { pass; }
        }

      // if should store weapon
      } else if (GetActionMarker()->m_paaAction==PAA_STOREWEAPON) {
        autocall AutoStoreWeapon() EReturn;

      // if should draw weapon
      } else if (GetActionMarker()->m_paaAction==PAA_DRAWWEAPON) {
        // order playerweapons to select best weapon
        ESelectWeapon eSelect;
        eSelect.iWeapon = -4;
        GetWeapons(E_HAND_MAIN)->SendEvent(eSelect);

      // if should wait
      } else if (GetActionMarker()->m_paaAction==PAA_LOOKAROUND) {
        autocall AutoLookAround() EReturn;

      // if should use item
      } else if (GetActionMarker()->m_paaAction==PAA_USEITEM) {
        // use it
        autocall AutoUseItem() EReturn;

      // if should pick item
      } else if (GetActionMarker()->m_paaAction==PAA_PICKITEM) {
        // pick it
        autocall AutoPickItem() EReturn;

      // if falling from bridge
      } else if (GetActionMarker()->m_paaAction==PAA_FALLDOWN) {
        // fall
        autocall AutoFallDown() EReturn;

      // if releasing player
      } else if (GetActionMarker()->m_paaAction==PAA_RELEASEPLAYER) {
        if (m_penCamera!=NULL) {
          ((CCameraEntity*)&*m_penCamera)->m_bStopMoving=TRUE;
        }

        m_penCamera = NULL;

        // if currently not having any weapon in hand
        if (GetWeapons(E_HAND_MAIN)->GetCurrentWeapon() == E_WEAPON_NONE) {
          // order playerweapons to select best weapon
          ESelectWeapon eSelect;
          eSelect.iWeapon = -4;
          m_penWeaponsFirst->SendEvent(eSelect);
        }

        // sync weapon, just in case
        m_ulFlags |= PLF_SYNCWEAPON;
        m_tmSpiritStart = 0;

      // if start computer
      } else if (GetActionMarker()->m_paaAction==PAA_STARTCOMPUTER) {
        // mark that
        if (_pNetwork->IsPlayerLocal(this) && GetSP()->sp_bSinglePlayer) {
          cmp_ppenPlayer = this;
          cmp_bInitialStart = TRUE;
        }

      // if start introscroll
      } else if (GetActionMarker()->m_paaAction==PAA_STARTINTROSCROLL) {
        _pShell->Execute("sam_iStartCredits=1;");

      // if start credits
      } else if (GetActionMarker()->m_paaAction==PAA_STARTCREDITS) {
        _pShell->Execute("sam_iStartCredits=2;");

      // if stop scroller
      } else if (GetActionMarker()->m_paaAction==PAA_STOPSCROLLER) {
        _pShell->Execute("sam_iStartCredits=-1;");

      // if should run to the marker
      } else if (GetActionMarker()->m_paaAction==PAA_RUN) {
        // go to it
        m_fAutoSpeed = plr_fSpeedForward*GetActionMarker()->m_fSpeed;
        autocall AutoGoToMarker() EReturn;

      // if should run to the marker and stop exactly there
      } else if (GetActionMarker()->m_paaAction==PAA_RUNANDSTOP) {
        // go to it
        m_fAutoSpeed = plr_fSpeedForward*GetActionMarker()->m_fSpeed;
        autocall AutoGoToMarkerAndStop() EReturn;

      // if should record end-of-level stats
      } else if (GetActionMarker()->m_paaAction==PAA_RECORDSTATS) {
        BOOL bPlayEntireGame = GetSP()->sp_ulCoopFlags & CPF_PLAY_ENTIRE_GAME;

        if (GetSP()->sp_bSinglePlayer || bPlayEntireGame) {
          // remeber estimated time
          m_tmEstTime = GetActionMarker()->m_tmWait;
          // record stats
          RecordEndOfLevelData();
        } else {
          SetGameEnd();
        }

      // if should show statistics to the player
      } else if (GetActionMarker()->m_paaAction==PAA_SHOWSTATS) {
        // call computer
        if (cmp_ppenPlayer==NULL && _pNetwork->IsPlayerLocal(this) && GetSP()->sp_bSinglePlayer) {
          m_bEndOfLevel = TRUE;
          cmp_ppenPlayer = this;
          m_ulFlags|=PLF_DONTRENDER;

          while (m_bEndOfLevel)
          {
            wait(_pTimer->TickQuantum)
            {
              on (ETimer) : { stop; }
              on (EReceiveScore) : { pass; }
              on (EKilledEnemy) : { pass; }
              on (ECenterMessage) : { pass; }

              on (EPostLevelChange) : {
                m_ulFlags&=!PLF_DONTRENDER;
                m_bEndOfLevel = FALSE;
                pass;
              }

              otherwise() : { resume; }
            }
          }

          m_ulFlags&=!PLF_DONTRENDER;
        }

      // if end of entire game
      } else if (GetActionMarker()->m_paaAction==PAA_ENDOFGAME) {

        // record stats
        jump TheEnd();
      } else if (GetActionMarker()->m_paaAction==PAA_NOGRAVITY) {
        SetPhysicsFlags(GetPhysicsFlags() & ~(EPF_TRANSLATEDBYGRAVITY|EPF_ORIENTEDBYGRAVITY));

        if (GetActionMarker()->GetParent() != NULL)
        {
          SetParent(GetActionMarker()->GetParent());
        }

      } else if (GetActionMarker()->m_paaAction==PAA_TURNONGRAVITY) {
        SetPhysicsFlags(GetPhysicsFlags()|EPF_TRANSLATEDBYGRAVITY|EPF_ORIENTEDBYGRAVITY);
        SetParent(NULL);

      } else if (TRUE) {
        ASSERT(FALSE);
      }

      // if marker points to a trigger
      if (GetActionMarker()->m_penTrigger!=NULL &&
          GetActionMarker()->m_paaAction!=PAA_PICKITEM) {
        // trigger it
        SendToTarget(GetActionMarker()->m_penTrigger, EET_TRIGGER, this);
      }

      // get next marker
      m_penActionMarker = GetActionMarker()->m_penTarget;
    }

    // disable auto speed
    m_fAutoSpeed = 0.0f;

    // must clear marker, in case it was invalid
    m_penActionMarker = NULL;

    // enable playeranimator animating
    GetPlayerAnimator()->m_bDisableAnimating = FALSE;

    // return to main loop
    return EVoid();
  }
/************************************************************
 *                        M  A  I  N                        *
 ************************************************************/
  Main(EVoid evoid)
  {
    // remember start time
    time((time_t*)&m_iStartTime);

    m_ctUnreadMessages = 0;
    SetFlags(GetFlags()|ENF_CROSSESLEVELS|ENF_NOTIFYLEVELCHANGE);
    InitAsEditorModel();

    // set default model for physics etc
    CTString strDummy;
    SetPlayerAppearance(GetModelObject(), NULL, strDummy, /*bPreview=*/FALSE);

    // set your real appearance if possible
    ValidateCharacter();
    SetPlayerAppearance(&m_moRender, GetCharacter(), strDummy, FALSE);
    ParseGender(strDummy);

    // if unsuccessful
    if (GetModelObject()->GetData()==NULL) {
      // never proceed with initialization - player cannot work
      return;
    }

    //const FLOAT fSize = 2.1f/1.85f;
    //GetModelObject()->StretchModel(FLOAT3D(fSize, fSize, fSize));
    ModelChangeNotify();

    // do not use predictor if not yet initialized
    if (IsPredictor()) { // !!!!####
      Destroy();
      return;
    }

    // appear
    SwitchToModel();
    m_ulFlags|=PLF_INITIALIZED;

    // set initial vars
    en_tmMaxHoldBreath = 60.0f;
    en_fDensity = 1000.0f;    // same density as water - to be able to dive freely

    ModelChangeNotify();

    CreateComponents();

    // set sound default parameters
    m_soMouth.Set3DParameters(50.0f, 10.0f, 1.0f, 1.0f);
    m_soFootL.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
    m_soFootR.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
    m_soBody.Set3DParameters(25.0f, 5.0f, 1.0f, 1.0f);
    m_soMessage.Set3DParameters(25.0f, 5.0f, 1.0f, 1.0f);
    m_soSniperZoom.Set3DParameters(25.0f, 5.0f, 1.0f, 1.0f);

    // setup light source
    SetupLightSource();

    // set light animation if available
    try {
      m_aoLightAnimation.SetData_t(CTFILENAME("Animations\\BasicEffects.ani"));
    } catch (char *strError) {
      WarningMessage(TRANS("Cannot load Animations\\BasicEffects.ani: %s"), strError);
    }

    PlayLightAnim(LIGHT_ANIM_NONE, 0);

    wait()
    {
      on (EBegin) : { call FirstInit(); }
      on (ERebirth) : { call Rebirth(); }
      on (EDeath eDeath) : { call Death(eDeath); }
      on (EDamage eDamage) : { call Wounded(eDamage); }

      on (EPreLevelChange) : {
        m_ulFlags&=~PLF_INITIALIZED;
        m_ulFlags|=PLF_CHANGINGLEVEL;
        m_ulFlags &= ~PLF_LEVELSTARTED;
        resume;
      }

      on (EPostLevelChange) :
      {
        if (GetSP()->sp_bSinglePlayer || (GetFlags()&ENF_ALIVE)) {
          call WorldChange();
        } else {
          call WorldChangeDead();
        }
      }

      on (ETakingBreath eTakingBreath ) :
      {
        SetDefaultMouthPitch();

        if (eTakingBreath.fBreathDelay<0.2f) {
          PlaySound(m_soMouth, GenderSound(SOUND_INHALE0), SOF_3D);
        } else if (eTakingBreath.fBreathDelay<0.8f) {
          PlaySound(m_soMouth, GenderSound(SOUND_INHALE1), SOF_3D);
        } else {
          PlaySound(m_soMouth, GenderSound(SOUND_INHALE2), SOF_3D);
        }
        resume;
      }

      on (ECameraStart eStart) :
      {
        m_penCamera = eStart.penCamera;

        // stop player
        if (m_penActionMarker==NULL) {
          SetDesiredTranslation(FLOAT3D(0.0f, 0.0f, 0.0f));
          SetDesiredRotation(ANGLE3D(0.0f, 0.0f, 0.0f));
        }

        // stop firing
        ReleaseWeapon(E_HAND_BOTH);
        resume;
      }

      on (ECameraStop eCameraStop) :
      {
        if (m_penCamera==eCameraStop.penCamera) {
          m_penCamera = NULL;
        }

        resume;
      }

      on (ECenterMessage eMsg) :
      {
        m_strCenterMessage = eMsg.strMessage;
        m_tmCenterMessageEnd = _pTimer->CurrentTick()+eMsg.tmLength;

        if (eMsg.mssSound==MSS_INFO) {
          m_soMessage.Set3DParameters(25.0f, 5.0f, 1.0f, 1.0f);
          PlaySound(m_soMessage, SOUND_INFO, SOF_3D|SOF_VOLUMETRIC|SOF_LOCAL);
        }

        resume;
      }

      on (EComputerMessage eMsg) :
      {
        ReceiveComputerMessage(eMsg.fnmMessage, CMF_ANALYZE);
        resume;
      }

      on (EVoiceMessage eMsg) :
      {
        SayVoiceMessage(eMsg.fnmMessage);
        resume;
      }

      on (EAutoAction eAutoAction) :
      {
        // remember first marker
        m_penActionMarker = eAutoAction.penFirstMarker;
        // do the actions
        call DoAutoActions();
      }

      on (EReceiveScore eScore) :
      {
        m_psLevelStats.ps_iScore += eScore.iPoints;
        m_psGameStats.ps_iScore += eScore.iPoints;
        m_iMana  += eScore.iPoints*GetSP()->sp_fManaTransferFactor;
        CheckHighScore();
        resume;
      }

      on (EKilledEnemy) :
      {
        m_psLevelStats.ps_iKills += 1;
        m_psGameStats.ps_iKills += 1;
        resume;
      }

      on (ESecretFound) :
      {
        m_psLevelStats.ps_iSecrets += 1;
        m_psGameStats.ps_iSecrets += 1;
        resume;
      }

      on (EWeaponChanged) :
      {
        // make sure we discontinue zooming (even if not changing from sniper)
        ResetWeaponZoom();
        PlaySound(m_soSniperZoom, SOUND_SILENCE, SOF_3D);
        if (_pNetwork->IsPlayerLocal(this)) {IFeel_StopEffect("SniperZoom");}
        resume;
      }
      // EEnd should not arrive here
      on (EEnd) :
      {
        ASSERT(FALSE);
        resume;
      }

      // if player is disconnected
      on (EDisconnected) :
      {
        // exit the loop
        stop;
      }

      // support for jumping using bouncers
      on (ETouch eTouch) :
      {
        if (IsOfClass(eTouch.penOther, "Bouncer")) {
          JumpFromBouncer(this, eTouch.penOther);

          // play jump sound
          SetDefaultMouthPitch();
          PlaySound(m_soMouth, GenderSound(SOUND_JUMP), SOF_3D);
          if (_pNetwork->IsPlayerLocal(this)) {IFeel_PlayEffect("Jump");}
        }

        resume;
      }

      on (EToggleCheat eCheat) :
      {
        if (m_ulCheatFlags & eCheat.iFlag) {
          m_ulCheatFlags &= ~eCheat.iFlag;
        } else {
          m_ulCheatFlags |= eCheat.iFlag;
        }

        resume;
      }
      
      on (EGiveAllCheat eCheat) :
      {
        OnGiveAllCheat(eCheat.iFlags);
        
        resume;
      }

      on (EGiveWeaponCheat eCheat) :
      {
        GetInventory()->ReceiveWeapon((WeaponIndex)eCheat.iWeapon);
        
        resume;
      }
      
      on (EGoToMarkerCheat eCheat) :
      {
        m_iGoToMarker = eCheat.iMarker;

        resume;
      }
    }

    // we get here if the player is disconnected from the server

    // if we have some keys
    if (!IsPredictor() && GetInventory()->m_ulKeys != 0) {
      TransferKeys();
    }

    // spawn teleport effect
    SpawnTeleport();

    // cease to exist
    DestroyComponents();

    Destroy();
    return;
  };
};
