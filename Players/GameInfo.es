/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

409
%{
  #include "StdH.h"
  #include "Entities/Players/PlayerPawn.h"
  #include "Entities/Players/PlayerInventory.h"
%}

%{
  #define MAX_TEAMS 4
%}

class export CGameInfoEntity : CEntity {
name      "GameInfo";
thumbnail "";
features "CanBePredictable";

properties:

  1 INDEX m_ulSharedKeys = 0,
  2 INDEX m_ulStoredKeys = 0,
  3 INDEX m_iSharedLives = 0,
  4 INDEX m_iSharedMoney = 0,

 20 INDEX m_iScoreForLifeAccum = 0,

 30 CUSTOMDATA m_cdTeamScore,

{
  CStaticArray<INDEX> m_TeamScore;
}

resources:

functions:

  void CGameInfoEntity(void)
  {
    m_TeamScore.New(MAX_TEAMS);
  }
  
  void Copy(CEntity &enOther, ULONG ulFlags)
  {
    CEntity::Copy(enOther, ulFlags); // Don't forget to call it for parent class!
  }

  virtual void ReadCustomData_t(CTStream &istrm, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();

    if (slPropertyOffset == offsetof(CGameInfoEntity, m_cdTeamScore)) {
      ReadIndexArray_t(istrm, m_TeamScore);
    }
  };

  virtual void WriteCustomData_t(CTStream &ostrm, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();

    if (slPropertyOffset == offsetof(CGameInfoEntity, m_cdTeamScore)) {
      WriteIndexArray_t(ostrm, m_TeamScore);
    }
  };
  
  virtual void CopyCustomData(CEntity &enOther, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();
    CGameInfoEntity *penOther = static_cast<CGameInfoEntity *>(&enOther);

    if (slPropertyOffset == offsetof(CGameInfoEntity, m_cdTeamScore)) {
      m_TeamScore.CopyArray(penOther->m_TeamScore);
    }
  };

  BOOL HandleEvent(const CEntityEvent &ee)
  {
    switch (ee.ee_slEvent)
    {
      case EVENTCODE_EPreLevelChange: return PreWorldChange();
      case EVENTCODE_EPostLevelChange: return PostWorldChange();
    }

    return CEntity::HandleEvent(ee);
  }

  INDEX GetSharedKeys()
  {
    return m_ulSharedKeys;
  }

  INDEX GetSharedLives()
  {
    return m_iSharedLives;
  }
  
  INDEX GetSharedMoney()
  {
    return m_iSharedMoney;
  }
  
  void OnAddMoney(INDEX iMoney)
  {
    m_iSharedMoney += iMoney;
  }

  void OnAddLives(INDEX iLives)
  {
    m_iSharedLives += iLives;
  }
  
  void SetSharedKeys(INDEX iKeys)
  {
    m_ulSharedKeys = iKeys;
  }

  void GiveStoredKeys(CPlayerPawnEntity *pen)
  {   
    if (pen == NULL || m_ulStoredKeys == 0) {
      return;
    }

    for (INDEX i = 0; i < 32; i++)
    {
      if (m_ulStoredKeys & (1 << i)) {
        pen->GetInventory()->ReceiveKey(i);
      }
    }

    CInfoF("%s received stored keys (0x%08X)!\n", pen->GetPlayerName(), m_ulStoredKeys);
    m_ulStoredKeys = 0;
  }
  
  void StoreKeys(INDEX iKeys)
  {
    m_ulStoredKeys = iKeys;
  }
  
  void DrainLife()
  {
    m_iSharedLives -= 1;
  }

  BOOL PreWorldChange()
  { 
    return TRUE;
  }
  
  BOOL PostWorldChange()
  {
    return TRUE;
  }
  
  void ClearTeamScore()
  {
    for (INDEX i = 0; i < MAX_TEAMS; i++)
    {
      m_TeamScore[i] = 0;
    }
  }
  
  void ResetAllToDefault()
  {
    m_iSharedLives = GetSP()->sp_ctInitialLives;
    m_ulSharedKeys = 0;
    
    ClearTeamScore();
  }

procedures:

  Main()
  {
    InitAsVoid();
    SetFlags(GetFlags()|ENF_CROSSESLEVELS|ENF_NOTIFYLEVELCHANGE);

    ResetAllToDefault();
    return;
  };

};