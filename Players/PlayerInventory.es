/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

408
%{
  #include "StdH.h"

  #include <Entities/Items/AmmoPack.h>
  #include "Entities/Common/PlayerControls.h"
%}

uses "Entities/Players/PlayerWeapon";

event EInventoryInit
{
  CEntityPointer penOwner,
};

%{
  WeaponIndex GetWeaponIndexForType(WeaponType eWeaponType)
  {
    switch (eWeaponType)
    {
      case WEAPON_KNIFE: return E_WEAPON_KNIFE;
      case WEAPON_CHAINSAW: return E_WEAPON_CHAINSAW;
      case WEAPON_COLT: case WEAPON_DOUBLECOLT: return E_WEAPON_REVOLVER;
      case WEAPON_SINGLESHOTGUN: return E_WEAPON_SHOTGUN;
      case WEAPON_DOUBLESHOTGUN: return E_WEAPON_SUPERSHOTGUN;
      case WEAPON_TOMMYGUN: return E_WEAPON_MACHINEGUN;
      case WEAPON_MINIGUN: return E_WEAPON_CHAINGUN;
      case WEAPON_LASER: return E_WEAPON_PLASMAGUN;
      case WEAPON_FLAMER: return E_WEAPON_FLAMER;
      case WEAPON_ROCKETLAUNCHER: return E_WEAPON_ROCKETLAUNCHER;
      case WEAPON_GRENADELAUNCHER: return E_WEAPON_GRENADELAUNCHER;
      case WEAPON_SNIPER: return E_WEAPON_SNIPER;
      case WEAPON_IRONCANNON: return E_WEAPON_CANNON;
      case WEAPON_GHOSTBUSTER: return E_WEAPON_BEAMGUN;
      case WEAPON_PLASMATHROWER: return E_WEAPON_PLASMATHROWER;
      case WEAPON_MINELAYER: return E_WEAPON_MINELAYER;
    }

    return E_WEAPON_NONE;
  }
%}

class export CPlayerInventoryEntity : CEntity {
name      "CPlayerInventoryEntity";
thumbnail "";
features "CanBePredictable";

properties:

  1 CEntityPointer m_penPlayer,       // player which owns it
  
  5 INDEX m_ulKeys = 0,                       // mask for all picked-up keys
  
  6 CUSTOMDATA m_cdWeaponInventory features(EPROPF_SIMULATIONONLY),
  7 CUSTOMDATA m_cdAmmoInventory features(EPROPF_SIMULATIONONLY),
  8 CUSTOMDATA m_cdGadgetInventory features(EPROPF_SIMULATIONONLY),
  9 CUSTOMDATA m_cdCurrencyInventory features(EPROPF_SIMULATIONONLY),
 10 CUSTOMDATA m_cdStatusPool features(EPROPF_SIMULATIONONLY),

 11 CUSTOMDATA m_cdCurrentWeapon features(EPROPF_SIMULATIONONLY),
 12 CUSTOMDATA m_cdWantedWeapon features(EPROPF_SIMULATIONONLY),
 13 CUSTOMDATA m_cdFavoriteWeapon features(EPROPF_SIMULATIONONLY),

 // Slots.
 20 INDEX m_iCurrentGadget = E_GADGET_INVALID,

{
  CItemContainer m_Weapons;
  CStackableItemContainer m_Ammo;
  CStackableItemContainer m_Gadgets;
  CStackableItemContainer m_Currencies;
  CStatusEffectPool m_StatusEffects;

  // Slots
  TStaticArray<INDEX> m_aiCurrentWeapon;
  TStaticArray<INDEX> m_aiWantedWeapon;
  TStaticArray<INDEX> m_aiFavoriteWeapon;
}

resources:

functions:

  //! Constructor.
  void CPlayerInventoryEntity(void)
  {
    m_aiCurrentWeapon.New(2);
    m_aiWantedWeapon.New(2);
    m_aiFavoriteWeapon.New(MAX_FAVORITE_WEAPONS);

    for (INDEX i = 0; i < m_aiCurrentWeapon.Count(); i++)
    {
      m_aiCurrentWeapon[i] = E_WEAPON_NONE;
    }

    for (INDEX i = 0; i < m_aiWantedWeapon.Count(); i++)
    {
      m_aiWantedWeapon[i] = E_WEAPON_NONE;
    }

    for (INDEX i = 0; i < m_aiFavoriteWeapon.Count(); i++)
    {
      m_aiFavoriteWeapon[i] = E_WEAPON_NONE;
    }
  }

  //! Clone the entity.
  void Copy(CEntity &enOther, ULONG ulFlags)
  {
    CEntity::Copy(enOther, ulFlags); // Don't forget to call it for parent class!
  }
  
  void Read_t(CTStream *istr) // throw char *
  {
    CEntity::Read_t(istr);
  }
  
  void Write_t(CTStream *ostr) // throw char *
  {
    CEntity::Write_t(ostr);
  }

  virtual void ReadCustomData_t(CTStream &istrm, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();
    
    if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdAmmoInventory)) {
      m_Ammo.Read_t(istrm);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdWeaponInventory)) {
      m_Weapons.Read_t(istrm);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdGadgetInventory)) {
      m_Gadgets.Read_t(istrm);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdCurrencyInventory)) {
      m_Currencies.Read_t(istrm);  
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdStatusPool)) {
      m_StatusEffects.Read_t(istrm);  
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdCurrentWeapon)) {
      ReadIndexArray_t(istrm, m_aiCurrentWeapon);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdWantedWeapon)) {
      ReadIndexArray_t(istrm, m_aiWantedWeapon);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdFavoriteWeapon)) {
      ReadIndexArray_t(istrm, m_aiFavoriteWeapon);
    }
  };

  virtual void WriteCustomData_t(CTStream &ostrm, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();

    if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdAmmoInventory)) {
      m_Ammo.Write_t(ostrm);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdWeaponInventory)) {
      m_Weapons.Write_t(ostrm);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdGadgetInventory)) {
      m_Gadgets.Write_t(ostrm);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdCurrencyInventory)) {
      m_Currencies.Write_t(ostrm);  
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdStatusPool)) {
      m_StatusEffects.Write_t(ostrm);  
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdCurrentWeapon)) {
      WriteIndexArray_t(ostrm, m_aiCurrentWeapon);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdWantedWeapon)) {
      WriteIndexArray_t(ostrm, m_aiWantedWeapon);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdFavoriteWeapon)) {
      WriteIndexArray_t(ostrm, m_aiFavoriteWeapon);
    }
  };
  
  virtual void CopyCustomData(CEntity &enOther, const CEntityProperty &ep)
  {
    const SLONG slPropertyOffset = ep.GetOffset();
    CPlayerInventoryEntity *penOther = static_cast<CPlayerInventoryEntity *>(&enOther);

    if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdAmmoInventory)) {
      m_Ammo.Copy(penOther->m_Ammo);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdWeaponInventory)) {
      m_Weapons.Copy(penOther->m_Weapons);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdGadgetInventory)) {
      m_Gadgets.Copy(penOther->m_Gadgets);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdCurrencyInventory)) {
      m_Currencies.Copy(penOther->m_Currencies);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdStatusPool)) {
      m_StatusEffects.Copy(penOther->m_StatusEffects);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdCurrentWeapon)) {
      m_aiCurrentWeapon.CopyArray(penOther->m_aiCurrentWeapon);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdWantedWeapon)) {
      m_aiWantedWeapon.CopyArray(penOther->m_aiWantedWeapon);
    } else if (slPropertyOffset == offsetof(CPlayerInventoryEntity, m_cdFavoriteWeapon)) {
      m_aiFavoriteWeapon.CopyArray(penOther->m_aiFavoriteWeapon);
    }
  };

  //! Mark all dependent entities for prediction.
  void AddDependentsToPrediction(void)
  {
    m_penPlayer->AddToPrediction();
  }

  //! Returns pointer to owner.
  CPlayerPawnEntity *GetPlayer(void)
  {
    ASSERT(m_penPlayer != NULL);
    return static_cast<CPlayerPawnEntity *>(&*m_penPlayer);
  }

  WeaponIndex GetCurrentWeapon(PlayerHand eHand) const
  {
    if (eHand == E_HAND_MAIN) {
      return (WeaponIndex)m_aiCurrentWeapon[0];
    }

    return (WeaponIndex)m_aiCurrentWeapon[1];
  }

  void SetCurrentWeapon(PlayerHand eHand, WeaponIndex eCurrentWeapon)
  {
    if (eHand == E_HAND_MAIN) {
      m_aiCurrentWeapon[0] = eCurrentWeapon;
      return;
    }

    m_aiCurrentWeapon[1] = eCurrentWeapon;
  }

  WeaponIndex GetWantedWeapon(PlayerHand eHand)
  {
    if (eHand == E_HAND_MAIN) {
      return (WeaponIndex)m_aiWantedWeapon[0];
    }

    return (WeaponIndex)m_aiWantedWeapon[1];
  }

  void SetWantedWeapon(PlayerHand eHand, WeaponIndex eWantedWeapon)
  {
    if (eHand == E_HAND_MAIN) {
      m_aiWantedWeapon[0] = eWantedWeapon;
      return;
    }

    m_aiWantedWeapon[1] = eWantedWeapon;
  }

  WeaponIndex GetFavoriteWeapon(INDEX iSlot)
  {
    ASSERT(iSlot >= 0);

    if (iSlot < m_aiFavoriteWeapon.Count()) {
      return (WeaponIndex)m_aiFavoriteWeapon[iSlot];
    }

    return E_WEAPON_NONE;
  }

  void SetFavoriteWeapon(INDEX iSlot, WeaponIndex eWeapon)
  {
    ASSERT(iSlot >= 0);

    if (iSlot < m_aiFavoriteWeapon.Count()) {
      m_aiFavoriteWeapon[iSlot] = eWeapon;
    }
  }

  GadgetIndex GetCurrentGadget() const
  {
    return (GadgetIndex)m_iCurrentGadget;
  }

  void SetCurrentGadget(GadgetIndex eGadgetIndex)
  {
    m_iCurrentGadget = eGadgetIndex;
  }

  //! Returns amount of specific ammo type.
  INDEX GetAmmoQty(enum AmmoIndex eTypeId)
  {
    if (GetSP()->sp_bInfiniteAmmo) {
      return GetMaxAmmo(eTypeId);
    }

    return m_Ammo.GetItemQty(eTypeId);
  }

  //! Returns weapons mask for precaching.
  INDEX GetWeaponsMask()
  {
    INDEX iWeapons = 0;

    FOREACHINDYNAMICCONTAINER(m_Weapons.GetObjects(), FItemInstance, itt)
    {
      FItemInstance &ii = itt.Current();
      iWeapons |= (1 << (ii.GetTypeId() - 1));
    }

    return iWeapons;
  }
  
  //! Checks if we have any weapon in inventory.
  BOOL HasAnyWeapon()
  {
    return m_Weapons.GetObjects().Count() > 0;
  }

  //! Returns maximum duration for specific status effect.
  FLOAT GetStatusEffectMaxDuration(StatusEffectIndex eStatusEffect)
  {
    switch (eStatusEffect)
    {
      case E_STEF_INVISIBILITY: return 30.0F;
      case E_STEF_INVULNERABILITY: return 30.0F;
      case E_STEF_DAMAGE: return 40.0F;
      case E_STEF_SPEED: return 20.0F;
      case E_STEF_STRENGTH: return 40.0F;
      case E_STEF_JUMP_BOOST: return 20.0F;
      case E_STEF_REGENERATION: return 30.0F;
      case E_STEF_RESISTANCE: return 30.0F;
      case E_STEF_FIRE_RESISTANCE: return 30.0F;
      case E_STEF_SPAWN_PROTECTION: return GetSP()->sp_tmSpawnInvulnerability;
      case E_STEF_INFINITE_AMMO: return 40.0F;
    }

    return 0.0F;
  }

  //! Returns gadget count for specific type.
  INDEX GetGadgetQty(GadgetIndex eTypeId)
  {
    return m_Gadgets.GetItemQty(eTypeId);
  }

  //! Returns maximum amount of specific gadget type.
  INDEX GetGadgetMax(GadgetIndex eTypeId)
  {
    const INDEX iMaxQty = GetItemDef(E_ITEM_GADGET, eTypeId)->GetMaxQty();
    return iMaxQty;
  }

  //! Returns maximum amount of specific ammo type.
  INDEX GetMaxAmmo(enum AmmoIndex eTypeId)
  {
    const FLOAT fModifier = ClampDn(GetSP()->sp_fAmmoQuantity, 1.0f);
    const INDEX iMaxQty = GetItemDef(E_ITEM_AMMO, eTypeId)->GetMaxQty();

    return ClampUp(INDEX(ceil(iMaxQty * fModifier)), INDEX(999));
  }

  //! Returns currency count for specific type.
  INDEX GetCurrencyQty(CurrencyIndex eTypeId)
  {
    return m_Currencies.GetItemQty(eTypeId);
  }

  //! Check if have specific weapon.
  BOOL HasWeapon(enum WeaponIndex eWeapon)
  {
    const Func_CheckTypeId<FItemInstance> checkFunc(eWeapon);
    TDynamicContainer<FItemInstance> cItemsFound;
    m_Weapons.GetObjects().Find(cItemsFound, checkFunc);
    
    return cItemsFound.Count() > 0;
  }
  
  BOOL HasKey(UBYTE ubTypeId)
  {
    // TODO: Implement shared keys.
    
    return m_ulKeys & (1 << ubTypeId);
  }

  //! Remove specific weapon.
  void RemoveWeapon(enum WeaponIndex eWeapon)
  {
    m_Weapons.RemoveObjects(eWeapon); // Remove all weapons of this type.
  }
  
  //! Remove specicic ammo type from inventory.
  void RemoveAmmo(enum AmmoIndex eTypeId)
  {
    m_Ammo.RemoveObjects(eTypeId);
  }
  
  void RemoveKey(UBYTE ubTypeId)
  {
    m_ulKeys &= ~ubTypeId;
  }
  
  //! Add weapon to inventory.
  BOOL ReceiveWeapon(enum WeaponIndex eWeapon)
  {
    ASSERT(eWeapon > E_WEAPON_NONE);

    //CDebugF("ReceiveWeapon(%d)\n", eWeapon);

    if (HasWeapon(eWeapon)) {
      return FALSE;
    }

    CWeaponItemInstance wiiNew(eWeapon);
    m_Weapons.CreateInstance(wiiNew);
    return TRUE;
  }

  //! Add ammo of specific type.
  BOOL ReceiveAmmo(enum AmmoIndex eAmmo, INDEX iQuantity)
  {
    ASSERT(eAmmo > E_AMMO_INVALID);

    // if infinite ammo is on
    if (GetSP()->sp_bInfiniteAmmo) {
      // pick all items anyway (items that exist in this mode are only those that
      // trigger something when picked - so they must be picked)
      return TRUE;
    }

    return m_Ammo.ReceiveItem(eAmmo, iQuantity, GetMaxAmmo(eAmmo));
  }

  //! Add ammo of several types.
  BOOL ReceivePackAmmo(const CEntityEvent &ee)
  {
    ASSERT(ee.ee_slEvent == EVENTCODE_EAmmoPackPickup);

    EAmmoPackPickup &evt = (EAmmoPackPickup&)ee;

    INDEX ctAmmoTypes = 0;
    ctAmmoTypes += ReceiveAmmo(E_AMMO_SHELLS, evt.iShells) ? 1 : 0;
    ctAmmoTypes += ReceiveAmmo(E_AMMO_HEAVY_ROUNDS, evt.iBullets) ? 1 : 0;
    ctAmmoTypes += ReceiveAmmo(E_AMMO_SNIPER_ROUNDS, evt.iSniperBullets) ? 1 : 0;
    ctAmmoTypes += ReceiveAmmo(E_AMMO_ENERGY, evt.iElectricity) ? 1 : 0;
    ctAmmoTypes += ReceiveAmmo(E_AMMO_FUEL, evt.iNapalm) ? 1 : 0;
    ctAmmoTypes += ReceiveAmmo(E_AMMO_ROCKETS, evt.iRockets) ? 1 : 0;
    ctAmmoTypes += ReceiveAmmo(E_AMMO_GRENADES, evt.iGrenades) ? 1 : 0;
    ctAmmoTypes += ReceiveAmmo(E_AMMO_IRON_BALLS, evt.iIronBalls) ? 1 : 0;

    return ctAmmoTypes > 0;
  }

  //! Returns strongest instance for specified effect type.
  CStatusEffectInstance *GetStrongestStatusEffectInstance(enum StatusEffectIndex eEffectIndex)
  {
    return m_StatusEffects.GetStrongestInstance(eEffectIndex);
  }

  //! Get amplifier of strongest status effect for specified type.
  INDEX GetStrongestStatusEffectAmp(enum StatusEffectIndex eEffectIndex)
  {
    return m_StatusEffects.GetStrongestAmp(eEffectIndex);
  }

  //! Add new effect into pool.
  BOOL AddStatusEffect(CStatusEffectInstance &e, FLOAT fMaxDuration)
  {
    return m_StatusEffects.AddInstance(e, fMaxDuration) != NULL;
  }
  
  //! Add gadget of specific type.
  BOOL ReceiveGadget(GadgetIndex eTypeId, INDEX iQuantity)
  {
    ASSERT(eTypeId > E_GADGET_INVALID);

    return m_Gadgets.ReceiveItem(eTypeId, iQuantity, GetGadgetMax(eTypeId));
  }
  
  //! Add currency of specific type.
  BOOL ReceiveCurrency(enum CurrencyIndex eTypeId, INDEX iQuantity)
  {
    ASSERT(eTypeId > E_CURRENCY_INVALID);

    return m_Currencies.ReceiveItem(eTypeId, iQuantity, 0);
  }
  
  BOOL ReceiveKey(UBYTE ubTypeId)
  {
    ULONG ulKey = 1 << ubTypeId;
    
    // if key is already in inventory then ignore it
    if (m_ulKeys & ulKey) {
      return FALSE;
    }
    
    m_ulKeys |= ulKey;
    
    return TRUE;
  }
  
  //! Decrement gadget of specific type.
  void DrainGadget(GadgetIndex eTypeId)
  {
    ASSERT(eTypeId > E_GADGET_INVALID);

    m_Gadgets.DrainItem(eTypeId, 1);
  }
  
  //! Decrement currency of specific type.
  void DrainCurrency(CurrencyIndex eCurrencyIndex, INDEX iQuantity)
  {
    ASSERT(eCurrencyIndex > E_CURRENCY_INVALID);
    
    m_Currencies.DrainItem(eCurrencyIndex, iQuantity);
  }

  //! Sets ammo to (Ratio x Max Ammo)
  void AssureAmmoRatio(enum AmmoIndex eTypeId, FLOAT fRatio)
  {
    ASSERT(eTypeId > E_AMMO_INVALID);
    
    m_Ammo.RemoveObjects(eTypeId);
    ReceiveAmmo(eTypeId, fRatio * GetMaxAmmo(eTypeId));
  }

  //! Decrement ammo of specific type.
  void DrainAmmo(enum AmmoIndex eTypeId, INDEX iQuantity)
  {
    ASSERT(eTypeId > E_AMMO_INVALID);

    INDEX iInfiniteAmmoAmp = GetStrongestStatusEffectAmp(E_STEF_INFINITE_AMMO);
    
    BOOL bCheat = GetPlayer()->m_ulCheatFlags & CHT_FLAG_INFINITE_AMMO;

    if (GetSP()->sp_bInfiniteAmmo || bCheat || iInfiniteAmmoAmp) {
      return;
    }

    m_Ammo.DrainItem(eTypeId, iQuantity);
  }

  //! Remove all the ammo.
  void ClearAmmo()
  {
    m_Ammo.ClearContent();
  }
  
  //! Remove all the gadgets.
  void ClearGadgets()
  {
    m_Gadgets.ClearContent();
  }
  
  //! Remove all the currencies.
  void ClearCurrencies()
  {
    m_Currencies.ClearContent();
  }

  //! Remove ammo using mask from player marker.
  void TakeAmmo(INDEX iTakeAmmo)
  {
    if (iTakeAmmo & (1 << AMMO_BULLETS))       { RemoveAmmo(E_AMMO_HEAVY_ROUNDS); }
    if (iTakeAmmo & (1 << AMMO_SHELLS))        { RemoveAmmo(E_AMMO_SHELLS); }
    if (iTakeAmmo & (1 << AMMO_ROCKETS))       { RemoveAmmo(E_AMMO_ROCKETS); }
    if (iTakeAmmo & (1 << AMMO_GRENADES))      { RemoveAmmo(E_AMMO_GRENADES); }
    if (iTakeAmmo & (1 << AMMO_NAPALM))        { RemoveAmmo(E_AMMO_FUEL); }
    if (iTakeAmmo & (1 << AMMO_ELECTRICITY))   { RemoveAmmo(E_AMMO_ENERGY); }
    if (iTakeAmmo & (1 << AMMO_IRONBALLS))     { RemoveAmmo(E_AMMO_IRON_BALLS); }
    if (iTakeAmmo & (1 << AMMO_SNIPERBULLETS)) { RemoveAmmo(E_AMMO_SNIPER_ROUNDS); }
  }

  //! Remove weapons using mask from player marker.
  void TakeWeapons(INDEX iTakeWeapons)
  {
    for (INDEX iWeapon = WEAPON_KNIFE; iWeapon < WEAPON_LAST; iWeapon++)
    {
      const WeaponIndex eWeaponIndex = GetWeaponIndexForType((WeaponType)iWeapon);
      BOOL bShouldRemove = iTakeWeapons & (1 << (iWeapon - 1));
      
      if (!bShouldRemove || iWeapon == WEAPON_DOUBLECOLT) {
        continue;
      }
      
      RemoveWeapon(eWeaponIndex);
    }
  }

  //! Give weapons using mask from player marker.
  void GiveWeapons(INDEX iGiveWeapons)
  {
    for (INDEX iWeapon = WEAPON_KNIFE; iWeapon < WEAPON_LAST; iWeapon++)
    {
      BOOL bShouldGive = iGiveWeapons & (1 << (iWeapon - 1));
      
      if (!bShouldGive) {
        continue;
      }

      if (iWeapon == WEAPON_DOUBLECOLT) {
        ReceiveWeapon(E_WEAPON_REVOLVER);
        continue;
      }

      WeaponIndex eWeaponIndex = GetWeaponIndexForType((WeaponType)iWeapon);
      ReceiveWeapon(eWeaponIndex);
    }
  }

  //! Initialize weapons from player marker.
  void InitializeWeapons(INDEX iGiveWeapons, INDEX iTakeWeapons, INDEX iTakeAmmo, FLOAT fMaxAmmoRatio)
  {    
    ULONG ulOldWeapons = GetWeaponsMask();

    // give/take weapons
    TakeWeapons(iTakeWeapons);
    iGiveWeapons |= 0x03;
    GiveWeapons(iGiveWeapons);

    // find which weapons are new
    ULONG ulNewWeapons = GetWeaponsMask() & ~ulOldWeapons;

    // for each new weapon
    for (INDEX iWeapon = E_WEAPON_KNIFE; iWeapon < E_WEAPON_MAX; iWeapon++)
    {
      if (ulNewWeapons & (1 << (iWeapon - 1))) {
        AmmoIndex eAmmoIndex = GetWeaponParams((WeaponIndex)iWeapon)->GetDefaultAmmoType();

        // Skip weapon types with no ammo.
        if (eAmmoIndex == E_AMMO_INVALID) {
          continue;
        }

        AssureAmmoRatio(eAmmoIndex, fMaxAmmoRatio);

        if (GetAmmoQty(eAmmoIndex) <= 0) {
          ReceiveAmmo(eAmmoIndex, GetWeaponParams((WeaponIndex)iWeapon)->GetDefaultAmmoQty());
        }
      }
    }

    TakeAmmo(iTakeAmmo);
  }

  //! Reset weapons to default.
  void ClearWeapons()
  {
    m_Weapons.ClearContent();
    ReceiveWeapon(E_WEAPON_KNIFE);
    ReceiveWeapon(E_WEAPON_REVOLVER);
  }

  //! Remove all the status effect times.
  void ResetStatusEffects()
  {
    m_StatusEffects.ClearContent();
  }

  //! Reset everything to default.
  void ResetAllToDefault()
  {
    const BOOL bUseLives = GetSP()->sp_ulCoopFlags & CPF_USE_LIVES;
    const BOOL bSharedLives = GetSP()->sp_ulCoopFlags & CPF_SHARED_LIVES;

    ClearAmmo();
    ClearWeapons();
    ClearGadgets();
    ClearCurrencies();

    if (bUseLives && !bSharedLives) {
      ReceiveCurrency(E_CURRENCY_EXTRA_LIFE, GetSP()->sp_ctInitialLives);
    }
  }
  
  //! Called when player uses 'Give All' cheat.
  void OnGiveAllCheat(ULONG ulFlags)
  {
    const BOOL bGiveNewWeapons = ulFlags & GACF_NEW_WEAPONS;
    const BOOL bGiveNewAmmo = ulFlags & GACF_NEW_AMMO;
    
    if (ulFlags & GACF_WEAPONS) {
      for (INDEX iWeapon = E_WEAPON_NONE + 1; iWeapon < E_WEAPON_MAX; iWeapon++)
      {
        const WeaponIndex eGiveIndex = (WeaponIndex)iWeapon;
        const FTokenDefinition *pItemDef = GetItemDef(E_ITEM_WEAPON, eGiveIndex);

        if (!bGiveNewWeapons && pItemDef->IsNew()) {
          continue;
        }

        ReceiveWeapon(eGiveIndex);
      }
    }

    if (ulFlags & GACF_AMMO) {
      for (INDEX iAmmo = E_AMMO_INVALID + 1; iAmmo < E_AMMO_MAX; iAmmo++)
      {
        const AmmoIndex eGiveIndex = (AmmoIndex)iAmmo;
        const FTokenDefinition *pItemDef = GetItemDef(E_ITEM_AMMO, eGiveIndex);

        if (!bGiveNewAmmo && pItemDef->IsNew()) {
          continue;
        }

        AssureAmmoRatio(eGiveIndex, 1.0F);
      }
    }
    
    if (ulFlags & GACF_GADGETS) {
      for (INDEX iGadget = E_GADGET_INVALID + 1; iGadget < E_GADGET_MAX; iGadget++)
      {
        const GadgetIndex eGiveIndex = (GadgetIndex)iGadget;

        ReceiveGadget(eGiveIndex, GetGadgetMax(eGiveIndex));
      }
    }

    if (ulFlags & GACF_KEYS) {
      m_ulKeys = 0xFFFFFFFF;
    }
  }

  //! Called every tick.
  void OnStep()
  {
    m_StatusEffects.OnStep(_pTimer->TickQuantum);
  }

procedures:

  Main(EInventoryInit eInit)
  {
    InitAsVoid();
    SetFlags(GetFlags()|ENF_CROSSESLEVELS);
    m_penPlayer = eInit.penOwner;
    
    ResetAllToDefault();
    return;
  };

};