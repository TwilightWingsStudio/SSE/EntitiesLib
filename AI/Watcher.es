/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

700
%{
  #include "StdH.h"

  #include "Entities/Characters/EnemyBase.h"
%}

// input parameter for watcher
event EWatcherInit
{
  CEntityPointer penOwner,        // who owns it
};

// entity is seen
event EWatch
{
  CEntityPointer penSeen,
};

class export CWatcherEntity : CRationalEntity {
name      "Watcher";
thumbnail "";
features  "CanBePredictable";

properties:

  1 CEntityPointer m_penOwner,  // entity which owns it
  2 FLOAT m_tmDelay = 5.0f,     // delay between checking moments - set depending on distance of closest player

 20 FLOAT m_fClosestPlayer = UpperLimit(0.0f),  // distance from closest player to owner of this watcher
 21 INDEX m_iPlayerToCheck = 0,   // sequence number for checking next player in each turn

resources:

functions:

  class CEnemyBaseEntity *GetOwner(void)
  {
    ASSERT(m_penOwner != NULL);
    return static_cast<CEnemyBaseEntity *>(&*m_penOwner);
  }

  //! Find one player number by random.
  INDEX GetRandomPlayer(void)
  {
//    CDebugF("Getting random number... ");

    // get maximum number of players in game
    INDEX ctMaxPlayers = GetMaxPlayers();

    // find actual number of players
    INDEX ctActivePlayers = 0;

    for (INDEX i = 0; i < ctMaxPlayers; i++)
    {
      if (GetPlayerEntity(i) != NULL) {
        ctActivePlayers++;
      }
    }

//    CDebugF("active players %d, ", ctActivePlayers);

    // if none then return first index anyway
    if (ctActivePlayers == 0) {
      return 0;
    }

    // choose one by random
    INDEX iChosenActivePlayer = IRnd()%ctActivePlayers;

//    CDebugF("chosen %d, ", iChosenActivePlayer);

    // find its physical index
    INDEX iActivePlayer = 0;

    for (INDEX i = 0; i < ctMaxPlayers; i++)
    {
      CEntity *penPlayer = GetPlayerEntity(i);
      
      if (penPlayer == NULL) {
        continue;
      }
      
      if (iActivePlayer == iChosenActivePlayer) {
//      CDebugF("actual index %d\n", iActivePlayer);
        return i;
      }

      iActivePlayer++;
    }

    ASSERT(FALSE);
    return 0;
  }
  
  BOOL IsLegalTarget(CEntity *pen)
  {
    if (pen == NULL) {
      return FALSE;
    }

    // Acquire various flags.
    BOOL bIsAlive = pen->GetFlags()&ENF_ALIVE;
    BOOL bIsInvisible = pen->GetFlags()&ENF_INVISIBLE;
    
    // Skip dead and invisible players.
    if (!bIsAlive || bIsInvisible) {
      return FALSE;
    }
    
    // Skip template enemies.
    if (IsDerivedFromClass(pen, &CEnemyBaseEntity_DLLClass)) {
      CEnemyBaseEntity &enEB = (CEnemyBaseEntity&)*pen;
      
      if (enEB.m_bTemplate) {
        return FALSE;
      }
      
      return FALSE; // TODO: Temporary!
    }

    // TODO: Check factions/attack groups etc.

    return TRUE;
  }

  CEntity *FindClosestPawn(CEntity *penCurrentTarget)
  {
    CEntity *penClosestPawn = NULL;
    FLOAT fClosestPawn = UpperLimit(0.0F);
    FLOAT fSenseRange = GetOwner()->m_fSenseRange;
    
    if (penCurrentTarget != NULL) {
      FLOAT3D vCurrentDelta = penCurrentTarget->GetPlacement().pl_PositionVector - m_penOwner->GetPlacement().pl_PositionVector;
      fClosestPawn = vCurrentDelta.Length();
      penClosestPawn = penCurrentTarget;
    }

    // Cycle through all pawns.
    FOREACHINDYNAMICCONTAINER(GetWorld()->wo_cenPawnEntities, CEntity, iten)
    {
      CEntity *pen = iten;

      // Skip current target if it is present.
      if (penCurrentTarget != NULL && penCurrentTarget == pen) {
        continue;
      }

      // Skip illegal targets.
      if (!IsLegalTarget(pen)) {
        continue;
      }

      // Calculate distance to player.
      FLOAT3D vDelta = pen->GetPlacement().pl_PositionVector - m_penOwner->GetPlacement().pl_PositionVector;
      FLOAT fDistance = vDelta.Length();

      // update if closer
      if (fDistance < fClosestPawn) {
        fClosestPawn = fDistance;
        penClosestPawn = pen;
      }
    }

    // If no pawns found then behave as if very close - must check for new ones.
    if (penClosestPawn == NULL) {
      fClosestPawn = 10.0f;
    }

    m_fClosestPlayer = fClosestPawn;
    return penClosestPawn;
  }

  //! Notify owner that a player has been seen.
  void SendWatchEvent(CEntity *penPlayer)
  {
    EWatch eWatch;
    eWatch.penSeen = penPlayer;
    m_penOwner->SendEvent(eWatch);
  }

  void CheckIfPlayerVisible(void)
  {
    // If the owner is blind then don't even bother checking.
    if (GetOwner()->m_bBlind) {
      return;
    }

    // get maximum number of players in game
    INDEX ctPlayers = GetMaxPlayers();

    // find first one after current sequence
    CEntity *penPlayer = NULL;
    m_iPlayerToCheck = (m_iPlayerToCheck+1)%ctPlayers;
    INDEX iFirstChecked = m_iPlayerToCheck;

    FOREVER {
      penPlayer = GetPlayerEntity(m_iPlayerToCheck);

      if (penPlayer != NULL) {
        break;
      }

      m_iPlayerToCheck++;
      m_iPlayerToCheck %= ctPlayers;

      if (m_iPlayerToCheck == iFirstChecked) {
        return; // we get here if there are no players at all
      }
    }

    // if this one is dead or invisible then do nothing
    if (!(penPlayer->GetFlags()&ENF_ALIVE) || (penPlayer->GetFlags()&ENF_INVISIBLE)) {
      return;
    }

    // if inside view angle and visible then send event to owner
    if (GetOwner()->SeeEntity(penPlayer, Cos(GetOwner()->m_fViewAngle / 2.0f))) {
      SendWatchEvent(penPlayer);
    }
  };

  //! Set new watch time.
  void SetWatchDelays(void)
  {
    const FLOAT tmMinDelay = 0.1f;   // delay at closest distance
    const FLOAT tmSeeDelay = 5.0f;   // delay at see distance
    const FLOAT tmTick = _pTimer->TickQuantum;
    FLOAT fSeeDistance  = GetOwner()->m_fIgnoreRange;
    FLOAT fNearDistance = Min(GetOwner()->m_fStopDistance, GetOwner()->m_fCloseDistance);

    // if closer than near distance
    if (m_fClosestPlayer <= fNearDistance) {
      // always use minimum delay
      m_tmDelay = tmMinDelay;
    // if further than near distance
    } else {
      // interpolate between near and see
      m_tmDelay = tmMinDelay +
        (m_fClosestPlayer - fNearDistance) * (tmSeeDelay - tmMinDelay) / (fSeeDistance - fNearDistance);
      // round to nearest tick
      m_tmDelay = floor(m_tmDelay/tmTick)*tmTick;
    }
  };

  //! Watch.
  void Watch(void)
  {
    // remember original distance
    FLOAT fOrgDistance = m_fClosestPlayer;

    // find closest player
    CEntity *penClosest = FindClosestPawn(NULL);

    FLOAT fSeeDistance  = GetOwner()->m_fIgnoreRange;
    FLOAT fStopDistance = Max(fSeeDistance * 1.5f, GetOwner()->m_fActivityRange);

    // if players exited enemy's scope
    if (fOrgDistance < fStopDistance && m_fClosestPlayer >= fStopDistance) {
      m_penOwner->SendEvent(EStop()); // stop owner
    // if players entered enemy's scope
    } else if (fOrgDistance >= fStopDistance && m_fClosestPlayer < fStopDistance) {
      // start owner
      m_penOwner->SendEvent(EStart());
    }

    // if the closest player is close enough to be seen
    if (m_fClosestPlayer < fSeeDistance) {
      // check for seeing any of the players
      CheckIfPlayerVisible();
    }

    // if the closest player is inside sense range then detect it immediately
    FLOAT fSenseRange = GetOwner()->m_fSenseRange;

    if (penClosest != NULL && fSenseRange > 0 && m_fClosestPlayer < fSenseRange) {
      SendWatchEvent(penClosest);
    }

    // set new watch time
    SetWatchDelays();
  };

  //! This is called directly from enemybase to check if another player has come too close.
  // Called from PerformAttack.
  CEntity *CheckCloserPlayer(CEntity *penCurrentTarget, FLOAT fRange)
  {
    // if the owner is blind then don't even bother checking
    if (GetOwner()->m_bBlind) {
      return NULL;
    }

    CEntity *penClosestPlayer = NULL;
    FLOAT3D vCurrentDelta = penCurrentTarget->GetPlacement().pl_PositionVector - m_penOwner->GetPlacement().pl_PositionVector;
    FLOAT fClosestPlayer = vCurrentDelta.Length();
    fClosestPlayer = Min(fClosestPlayer, fRange);  // this is maximum considered range

    // for all other players
    for (INDEX iPlayer = 0; iPlayer < GetMaxPlayers(); iPlayer++)
    {
      CEntity *penPlayer = GetPlayerEntity(iPlayer);
      
      if (IsDerivedFromClass(penPlayer, &CPlayerControllerEntity_DLLClass)) {
        CPlayerControllerEntity *penController = static_cast<CPlayerControllerEntity *>(penPlayer);
        penPlayer = penController->GetPawn();
      }
      
      // Skip illegal targets.
      if (!IsLegalTarget(penPlayer)) {
        continue;
      }

      // Skip same target.
      if (penPlayer == penCurrentTarget) {
        continue;
      }

      // calculate distance to player
      FLOAT3D vDelta = penPlayer->GetPlacement().pl_PositionVector - m_penOwner->GetPlacement().pl_PositionVector;
      FLOAT fDistance = vDelta.Length();

      // if closer than current and you can see him
      if (fDistance < fClosestPlayer && GetOwner()->SeeEntity(penPlayer, Cos(GetOwner()->m_fViewAngle / 2.0f))) {
        // update
        fClosestPlayer = fDistance;
        penClosestPlayer = penPlayer;
      }
    }

    return penClosestPlayer;
  }

  //! This is called directly from enemybase to attack multiple players (for really big enemies).
  // Called from MaybeSwitchToAnotherPlayer
  CEntity *CheckAnotherPlayer(CEntity *penCurrentTarget)
  {
    // if the owner is blind, or no current target then don't even check
    if (GetOwner()->m_bBlind || penCurrentTarget == NULL) {
      return NULL;
    }

    // get allowed distance
    CEntity *penClosestPlayer = NULL;
    FLOAT3D vCurrentDelta = penCurrentTarget->GetPlacement().pl_PositionVector - m_penOwner->GetPlacement().pl_PositionVector;
    FLOAT fCurrentDistance = vCurrentDelta.Length();
    FLOAT fRange = fCurrentDistance * 1.5f;

    // find a random offset to start searching
    INDEX iOffset = GetRandomPlayer();

    // for all other players
    INDEX ctPlayers = GetMaxPlayers();

    for (INDEX iPlayer = 0; iPlayer < ctPlayers; iPlayer++)
    {
      CEntity *penPlayer = GetPlayerEntity((iPlayer + iOffset) % ctPlayers);
      
      if (IsDerivedFromClass(penPlayer, &CPlayerControllerEntity_DLLClass)) {
        CPlayerControllerEntity *penController = static_cast<CPlayerControllerEntity *>(penPlayer);
        penPlayer = penController->GetPawn();
      }
      
      // Skip illegal targets.
      if (!IsLegalTarget(penPlayer)) {
        continue;
      }

      // Skip same target.
      if (penPlayer == penCurrentTarget) {
        continue;
      }

      // calculate distance to player
      FLOAT3D vDelta = penPlayer->GetPlacement().pl_PositionVector - m_penOwner->GetPlacement().pl_PositionVector;
      FLOAT fDistance = vDelta.Length();

      // if inside allowed range and visible then attack that one.
      if (fDistance < fRange && GetOwner()->SeeEntity(penPlayer, Cos(GetOwner()->m_fViewAngle / 2.0f))) {
        return penPlayer;
      }
    }

    return penCurrentTarget;
  }

  //! Returns bytes of memory used by this object.
  SLONG GetUsedMemory(void)
  {
    return(sizeof(CWatcherEntity) - sizeof(CRationalEntity) + CRationalEntity::GetUsedMemory());
  }

procedures:

  //! Watching
  Active()
  {
    // repeat
    while (TRUE)
    {
      // check all players
      Watch();

      // wait for given delay
      wait (m_tmDelay)
      {
        on (EBegin) : { resume; }

        on (ETimer) : { stop; }

        // stop looking
        on (EStop) : { jump Inactive(); }

        // force re-checking if receiving start or teleport
        on (EStart) : { stop; }

        on (ETeleport) : { stop; }
      }
    }
  };

  //! Not watching
  Inactive(EVoid)
  {
    wait()
    {
      on (EBegin) : { resume; }
      on (EStart) : { jump Active(); }
    }
  };

  // dummy mode
  Dummy(EVoid)
  {
    // ignores all events forever
    wait()
    {
      on (EBegin) : { resume; }
      otherwise() : { resume; };
    };
  }

  Main(EWatcherInit eInit)
  {
    // remember the initial parameters
    ASSERT(eInit.penOwner != NULL);
    m_penOwner = eInit.penOwner;

    // init as nothing
    InitAsVoid();
    SetPhysicsFlags(EPF_MODEL_IMMATERIAL);
    SetCollisionFlags(ECF_IMMATERIAL);

    // if in flyover game mode then go to dummy mode
    if (GetSP()->sp_gmGameMode == CSessionProperties::GM_FLYOVER) {
      jump Dummy();
      // NOTE: must not destroy self, because owner has a pointer
    }

    // generate random number of player to check next
    // (to provide even distribution of enemies among players)
    m_iPlayerToCheck = GetRandomPlayer() - 1;

    // start in disabled state
    autocall Inactive() EEnd;

    // cease to exist
    Destroy();

    return;
  };
};