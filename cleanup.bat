@echo off

Call :CleanupPath ""
Call :CleanupPath "AI\"
Call :CleanupPath "Brushes\"
Call :CleanupPath "Characters\"
Call :CleanupPath "Effects\"
Call :CleanupPath "Items\"
Call :CleanupPath "Light\"
Call :CleanupPath "Models\"
Call :CleanupPath "Players\"
Call :CleanupPath "Tools\"
Call :CleanupPath "Weapons\"

goto :end

:CleanupPath
  del "%~1*.cpp"
  del "%~1"*.h"
  del "%~1*_tables.h"

:end