/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

500
%{
  #include "StdH.h"
%}

uses "Entities/Effects/BasicEffects";

%{
%}

class export CProjectileBaseEntity : CMovableModelEntity {
name      "ProjectileBase";
thumbnail "";

properties:

  1 CEntityPointer m_penLauncher,     // who lanuched it
  2 CEntityPointer m_penTarget,       // target entity
  3 INDEX m_iHealth = 0,

 14 INDEX m_iDamageAmount = 0,               // damage amount when hit something
 15 INDEX m_iRangeDamageAmount = 0,          // range damage amount
 16 FLOAT m_fDamageHotSpotRange = 0.0f,      // hot spot range damage for exploding projectile
 17 FLOAT m_fDamageFallOffRange = 0.0f,      // fall off range damage for exploding projectile

{
}

resources:


functions:

  virtual CEntity *GetLauncher()
  {
    return &*m_penLauncher;
  }
  
  virtual CEntity *GetTarget()
  {
    return &*m_penTarget;
  }

  virtual INDEX GetVitality(VitalityType eType) const
  {
    return eType == VT_HEALTH ? m_iHealth : 0;
  }

  virtual void SetVitality(VitalityType eType, INDEX iPoints)
  {
    if (eType == VT_HEALTH) {
      m_iHealth = iPoints;
    }
  }

  virtual INDEX GetTouchDamage() const
  {
    return m_iDamageAmount * HEALTH_VALUE_MULTIPLIER;
  }
  
  virtual INDEX GetSplashDamage() const
  {
    return m_iRangeDamageAmount * HEALTH_VALUE_MULTIPLIER;
  }

/************************************************************
 *                   P R O C E D U R E S                    *
 ************************************************************/
procedures:

  Main(EVoid)
  {
    ASSERTALWAYS("DON'T INSTANTIATE THIS CLASS");
    Destroy();
    return;
  }
};
