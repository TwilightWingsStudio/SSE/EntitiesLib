/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

501
%{
  #include "StdH.h"

  #include "Models/Weapons/Laser/Projectile/LaserProjectile.h"
  #include "Entities/Characters/EnemyBase.h"
  //#include "Entities/Dragonman.h"
  #include "Models/Enemies/Elementals/Projectile/IcePyramid.h"
  #include "Models/Enemies/ElementalLava/Projectile/LavaStone.h"
  #include "Models/Enemies/ElementalLava/Projectile/LavaBomb.h"
  #include "Models/Enemies/Headman/Projectile/Blade.h"
  #include "Models/Enemies/Huanman/Projectile/Projectile.h"
  #include "Models/Enemies/Cyborg/Projectile/LaserProjectile.h"

  #include "ModelsMP/Enemies/Grunt/Projectile/GruntProjectile.h"
  #include "ModelsMP/Enemies/Guffy/Projectile/GuffyProjectile.h"

  #include "ModelsMP/Enemies/ExotechLarva/Weapons/PlasmaGun.h"

  #include "Entities/Players/PlayerWeapon.h"
  #include "Entities/Tools/Shooter.h"

  #define DEVIL_LASER_SPEED 100.0f
  #define DEVIL_ROCKET_SPEED 60.0f
%}

uses "Entities/Weapons/ProjectileBase";
uses "Entities/Effects/BasicEffects";
uses "Entities/Light/Light";
uses "Entities/Weapons/Flame";

enum ProjectileType
{
  0 PRT_ROCKET                "Rocket",   // player rocket
  1 PRT_GRENADE               "Grenade",   // player grenade
  2 PRT_FLAME                 "Flame",   // player flamer flame
  3 PRT_LASER_RAY             "Laser",   // player laser ray
  4 PRT_WALKER_ROCKET         "WalkerRocket",   // walker rocket

 10 PRT_CATMAN_FIRE           "Catman",   // catman fire

 11 PRT_HEADMAN_FIRECRACKER   "Firecracker",   // headman firecracker
 12 PRT_HEADMAN_ROCKETMAN     "Rocketman",   // headman rocketman
 13 PRT_HEADMAN_BOMBERMAN     "Bomberman",   // headman bomberman

 14 PRT_BONEMAN_FIRE          "Boneman",   // boneman fire

 15 PRT_WOMAN_FIRE            "Woman",   // woman fire

 16 PRT_DRAGONMAN_FIRE        "Dragonman",   // dragonman fire
 17 PRT_DRAGONMAN_STRONG_FIRE "Dragonman Strong",   // dragonman strong fire

 18 PRT_STONEMAN_FIRE         "Stoneman",   // stoneman fire rock
 19 PRT_STONEMAN_BIG_FIRE     "Stoneman Big",   // stoneman big fire rock
 20 PRT_STONEMAN_LARGE_FIRE   "Stoneman Large",   // stoneman large fire rock
 21 PRT_LAVAMAN_BIG_BOMB      "Lavaman Big Bomb",   // lavaman big bomb
 22 PRT_LAVAMAN_BOMB          "Lavaman Bomb",   // lavaman bomb
 23 PRT_LAVAMAN_STONE         "Lavaman Stone",   // lavaman rock projectile
 27 PRT_ICEMAN_FIRE           "Iceman",   // iceman ice cube
 28 PRT_ICEMAN_BIG_FIRE       "Iceman Big",   // iceman big ice cube
 29 PRT_ICEMAN_LARGE_FIRE     "Iceman Large",   // iceman large ice cube

 41 PRT_HUANMAN_FIRE          "Huanman",   // huanman fire

 42 PRT_FISHMAN_FIRE          "Fishman",   // fishman fire

 43 PRT_MANTAMAN_FIRE         "Mantaman",   // mantaman fire

 44 PRT_CYBORG_LASER          "Cyborg Laser",   // cyborg laser
 45 PRT_CYBORG_BOMB           "Cyborg Bomb",   // cyborg bomb

 50 PRT_LAVA_COMET            "Lava Comet",  // lava comet
 51 PRT_BEAST_PROJECTILE      "Beast Projectile",   // beast projectile
 52 PRT_BEAST_BIG_PROJECTILE  "Beast Big Projectile",   // big beast projectile
 53 PRT_BEAST_DEBRIS          "Beast Debris",   // beast projectile's debris
 54 PRT_BEAST_BIG_DEBRIS      "Beast Big Debris",   // big beast projectile's debris
 55 PRT_DEVIL_LASER           "Devil Laser",   // devil laser
 56 PRT_DEVIL_ROCKET          "Devil Rocket",   // devil rocket
 57 PRT_DEVIL_GUIDED_PROJECTILE "Devil Guided Projectile",   // devil guided projectile
 60 PRT_GRUNT_PROJECTILE_SOL  "Grunt Soldier Laser",  // grunt laser
 64 PRT_GRUNT_PROJECTILE_COM  "Grunt Commander Laser", // grunt commander laser
 61 PRT_GUFFY_PROJECTILE      "Guffy Projectile", // guffy rocket
 62 PRT_DEMON_FIREBALL        "Demon Fireball", // demon fireball
 63 PRT_DEMON_FIREBALL_DEBRIS "Demon Fireball Debris", // demon fireball debris
 70 PRT_SHOOTER_WOODEN_DART   "Shooter Wooden Dart", // shooter's wooden dart
 71 PRT_SHOOTER_FIREBALL      "Shooter Fireball", // shooter's fireball
 72 PRT_SHOOTER_FLAME         "Shooter Flame", // shooter's flame
 73 PRT_LARVA_PLASMA          "ExotechLarva Plasma", //exotech larva plasma gun
 74 PRT_LARVA_TAIL_PROJECTILE "ExotechLarva Tail Projectile", //exotech larva tail projectile
 75 PRT_AIRELEMENTAL_WIND     "Air Elemental Wind Blast", //air elemental wind blast
 76 PRT_AFTERBURNER_DEBRIS    "Afterburner debris",
 77 PRT_METEOR                "Meteor",
};

enum ProjectileMovingType
{
  0 PMT_FLYING        "",      // flying through space
  1 PMT_SLIDING       "",      // sliding on floor
  2 PMT_GUIDED        "",      // guided projectile
  3 PMT_GUIDED_FAST    "",     // fast guided projectile
  4 PMT_FLYING_REBOUNDING "",  // flying and rebounding from walls a few times
  5 PMT_GUIDED_SLIDING "",     // sliding on floor and guided at the same time
};

// input parameter for launching the projectile
event ELaunchProjectile
{
  CEntityPointer penLauncher,     // who launched it
  enum ProjectileType prtType,    // type of projectile
  FLOAT fSpeed,                   // optional - projectile speed (only for some projectiles)
  FLOAT fStretch,                 // optional - projectile stretch (only for some projectiles)
};

%{
  #define DRAGONMAN_NORMAL 0
  #define DRAGONMAN_STRONG 1

  #define ELEMENTAL_LARGE   2
  #define ELEMENTAL_BIG     1
  #define ELEMENTAL_NORMAL  0

  #define ELEMENTAL_STONEMAN 0
  #define ELEMENTAL_LAVAMAN  1
  #define ELEMENTAL_ICEMAN   2

  INDEX ModelForProjectile(ProjectileType eProjectileType)
  {
    switch (eProjectileType)
    {
      case PRT_ROCKET:
      case PRT_WALKER_ROCKET:
      case PRT_DEVIL_ROCKET:
        return MODEL_PROJECTILE_ROCKET_CFG;

      case PRT_GRENADE:
        return MODEL_PROJECTILE_GRENADE_CFG;

      case PRT_FLAME:
      case PRT_SHOOTER_FLAME:
        return MODEL_PROJECTILE_FLAME_CFG;
        
      case PRT_LASER_RAY:
        return MODEL_PROJECTILE_LASER_GREEN_CFG;

      case PRT_CATMAN_FIRE:
        return MODEL_PROJECTILE_CATMAN_CFG;

      case PRT_HEADMAN_FIRECRACKER:
        return MODEL_PROJECTILE_HM_FIRECRACKER_CFG;

      case PRT_HEADMAN_ROCKETMAN:
        return MODEL_PROJECTILE_HM_ROCKET_CFG;

      case PRT_HEADMAN_BOMBERMAN:
        return MODEL_PROJECTILE_HM_BOMB_CFG;

      case PRT_BONEMAN_FIRE:
        return MODEL_PROJECTILE_BONEMAN_CFG;

      case PRT_WOMAN_FIRE:
        return MODEL_PROJECTILE_WOMAN_CFG;

      case PRT_DRAGONMAN_FIRE:
        return MODEL_PROJECTILE_DRAGONMAN_CFG;

      case PRT_DRAGONMAN_STRONG_FIRE:
        return MODEL_PROJECTILE_DRAGONMAN_STRONG_CFG;

      case PRT_STONEMAN_FIRE:
      case PRT_STONEMAN_BIG_FIRE:
      case PRT_STONEMAN_LARGE_FIRE:
        return MODEL_PROJECTILE_ELEM_STONE_CFG;

      case PRT_LAVAMAN_BOMB:
      case PRT_LAVAMAN_BIG_BOMB:
        return MODEL_PROJECTILE_ELEM_LAVA_BOMB_CFG;

      case PRT_LAVAMAN_STONE:
        return MODEL_PROJECTILE_ELEM_LAVA_STONE_CFG;

      case PRT_ICEMAN_FIRE:
      case PRT_ICEMAN_BIG_FIRE:
      case PRT_ICEMAN_LARGE_FIRE:
        return MODEL_PROJECTILE_ELEM_ICE_CFG;

      case PRT_HUANMAN_FIRE:
        return MODEL_PROJECTILE_HUANMAN_CFG;

      case PRT_FISHMAN_FIRE:
        return MODEL_PROJECTILE_FISHMAN_CFG;

      case PRT_MANTAMAN_FIRE:
        return MODEL_PROJECTILE_MANTAMAN_CFG;

      case PRT_CYBORG_BOMB:
        return MODEL_PROJECTILE_CYBORG_BOMB_CFG;

      case PRT_LAVA_COMET:
        return MODEL_PROJECTILE_LAVA_CFG;

      case PRT_BEAST_PROJECTILE:
      case PRT_BEAST_DEBRIS:
        return MODEL_PROJECTILE_BEAST_CFG;

      case PRT_BEAST_BIG_PROJECTILE:
      case PRT_BEAST_BIG_DEBRIS:
      case PRT_SHOOTER_FIREBALL:
        return MODEL_PROJECTILE_BEAST_BIG_CFG;

      case PRT_CYBORG_LASER:
      case PRT_DEVIL_LASER:
        return MODEL_PROJECTILE_LASER_BLUE_CFG;

      case PRT_DEVIL_GUIDED_PROJECTILE:
        return MODEL_PROJECTILE_BEAST_BIG_CFG;

      case PRT_GRUNT_PROJECTILE_SOL:
        return MODEL_PROJECTILE_GRUNT_SOLDIER_CFG;

      case PRT_GRUNT_PROJECTILE_COM:
        return MODEL_PROJECTILE_GRUNT_COMMANDER_CFG;

      case PRT_GUFFY_PROJECTILE:
        return MODEL_PROJECTILE_GUFFY_CFG;

      case PRT_DEMON_FIREBALL:
      case PRT_DEMON_FIREBALL_DEBRIS:
        return MODEL_PROJECTILE_DEMON_CFG;

      case PRT_SHOOTER_WOODEN_DART:
        return MODEL_PROJECTILE_SHTR_WOODEN_DART;

      case PRT_LARVA_PLASMA:
        return MODEL_PROJECTILE_LARVA_PLASMA_CFG;

      case PRT_LARVA_TAIL_PROJECTILE:
        return MODEL_PROJECTILE_LARVA_TAIL_CFG;

      case PRT_AIRELEMENTAL_WIND:
        return MODEL_PROJECTILE_AIRELEMENTAL_WINDBLAST_CFG;

      case PRT_AFTERBURNER_DEBRIS:
      case PRT_METEOR:
        return MODEL_PROJECTILE_ELEM_LAVA_BOMB_CFG;
    }

    return -1;
  }

  void CProjectileEntity_OnInitClass(void)
  {
  }

  void CProjectileEntity_OnPrecache(CLibEntityClass *pdec, INDEX iUser)
  {
    const INDEX idModelConfig = ModelForProjectile((ProjectileType)iUser);

    if (idModelConfig != -1) {
      pdec->PrecacheModelConfig(idModelConfig);
    }

    switch ((ProjectileType)iUser)
    {
      // Rockets
      case PRT_ROCKET                :
      case PRT_WALKER_ROCKET         :
      case PRT_DEVIL_ROCKET          :
        pdec->PrecacheSound(SOUND_FLYING  );
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_ROCKET);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_EXPLOSIONSTAIN);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_SHOCKWAVE);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_ROCKET_PLANE);
        break;

      case PRT_GRENADE:
        pdec->PrecacheSound(SOUND_GRENADE_BOUNCE);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_GRENADE);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_EXPLOSIONSTAIN);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_SHOCKWAVE);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_GRENADE_PLANE);
        break;

      case PRT_FLAME:
      case PRT_SHOOTER_FLAME:
        pdec->PrecacheClass(CLASS_FLAME);
        break;

      case PRT_LASER_RAY:
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_LASERWAVE);
        break;

      case PRT_GRUNT_PROJECTILE_SOL:
      case PRT_GRUNT_PROJECTILE_COM:
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_LASERWAVE);
        break;

      case PRT_HEADMAN_BOMBERMAN:
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_BOMB);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_EXPLOSIONSTAIN);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_GRENADE_PLANE);
        break;

      case PRT_LAVAMAN_BIG_BOMB:
      case PRT_LAVAMAN_BOMB:
      case PRT_LAVAMAN_STONE:
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_SHOCKWAVE);
        pdec->PrecacheClass(CLASS_BLOOD_SPRAY);
        break;

      case PRT_METEOR:
        pdec->PrecacheSound(SOUND_FLYING  );
        pdec->PrecacheSound(SOUND_METEOR_BLAST  );
        pdec->PrecacheClass(CLASS_BLOOD_SPRAY);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_CANNON);
        break;

      case PRT_CYBORG_LASER:
      case PRT_CYBORG_BOMB:
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_BOMB);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_EXPLOSIONSTAIN);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_GRENADE_PLANE);
        break;

      case PRT_LAVA_COMET:
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_SHOCKWAVE);
        pdec->PrecacheClass(CLASS_BLOOD_SPRAY);
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_SHOCKWAVE);
        pdec->PrecacheClass(CLASS_BLOOD_SPRAY);
        break;

      case PRT_BEAST_PROJECTILE:
      case PRT_BEAST_DEBRIS:
        pdec->PrecacheSound(SOUND_BEAST_FLYING  );
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_CANNON);
        break;

      case PRT_GUFFY_PROJECTILE:
        pdec->PrecacheSound(SOUND_FLYING                   );
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_ROCKET );
        break;

      case PRT_BEAST_BIG_PROJECTILE:
      case PRT_DEVIL_GUIDED_PROJECTILE:
      case PRT_BEAST_BIG_DEBRIS:
      case PRT_SHOOTER_FIREBALL:
        pdec->PrecacheSound(SOUND_BEAST_FLYING  );
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_LIGHT_CANNON);
        break;

      case PRT_DEMON_FIREBALL:
      case PRT_DEMON_FIREBALL_DEBRIS:
        pdec->PrecacheSound(SOUND_DEMON_FLYING  );
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_LIGHT_CANNON);
        break;

      case PRT_LARVA_PLASMA:
        pdec->PrecacheSound(SOUND_DEMON_FLYING  );
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_LIGHT_CANNON);
        break;

      case PRT_LARVA_TAIL_PROJECTILE:
        pdec->PrecacheSound(SOUND_LARVETTE  );
        pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_CANNON);
        break;
    }
  }
%}

class export CProjectileEntity : CProjectileBaseEntity {
name      "Projectile";
thumbnail "";
features "ImplementsOnInitClass", "ImplementsOnPrecache", "CanBePredictable";

properties:

  2 enum ProjectileType m_prtType = PRT_ROCKET,       // type of the projectile
  3 enum ProjectileMovingType m_pmtMove = PMT_FLYING, // projectile moving type
  4 CEntityPointer m_penParticles,    // another entity for particles
  6 CEntityPointer m_penLastDamaged,  // last entity this projectile damaged

 10 FLOAT m_fSpeed = 0.0f,                   // projectile speed (optional, only for some projectiles)
 11 FLOAT m_fIgnoreTime = 0.0f,              // time when laucher will be ignored
 12 FLOAT m_fFlyTime = 0.0f,                 // fly time before explode/disappear
 13 FLOAT m_fStartTime = 0.0f,               // start time when launched
 18 FLOAT m_fSoundRange = 0.0f,              // sound range where explosion can be heard
 19 BOOL m_bExplode = FALSE,                 // explode -> range damage
 20 BOOL m_bLightSource = FALSE,             // projectile is also light source
 21 BOOL m_bCanHitHimself = FALSE,           // projectile can him himself
 22 BOOL m_bCanBeDestroyed = FALSE,          // projectile can be destroyed from something else
 23 FLOAT m_fWaitAfterDeath = 0.0f,          // wait after death for particles
 24 FLOAT m_aRotateSpeed = 0.0f,             // speed of rotation for guided projectiles
 25 FLOAT m_tmExpandBox = 0.0f,              // expand collision after a few seconds
 26 FLOAT m_tmInvisibility = 0.0f,           // don't render before given time
 27 INDEX m_iRebounds = 0,                   // how many times to rebound
 28 FLOAT m_fStretch=1.0f,                   // stretch

 30 CSoundObject m_soEffect,          // sound channel
 31 CSoundObject m_soExplosion,       // sound channel

 35 FLOAT m_fGuidedMaxSpeedFactor = 30.0f,   // speed factor for guided projectiles

 50 BOOL bLockedOn = TRUE,
 51 BOOL m_bLeftFlame = FALSE,

{
  CLightSource m_lsLightSource;
}

resources:

  1 class   CLASS_BASIC_EFFECT  "Classes\\BasicEffect.ecl",
  2 class   CLASS_LIGHT         "Classes\\Light.ecl",
  3 class   CLASS_PROJECTILE    "Classes\\Projectile.ecl",
  4 class   CLASS_BLOOD_SPRAY   "Classes\\BloodSpray.ecl",
  5 class   CLASS_FLAME         "Classes\\Flame.ecl",

// ********* MODELS *********
 20 modelcfg MODEL_PROJECTILE_ROCKET_CFG           "Models\\Projectiles\\Rocket\\RocketProjectile.vmc",
 21 modelcfg MODEL_PROJECTILE_GRENADE_CFG          "Models\\Projectiles\\Grenade\\GrenadeProjectile.vmc",
 22 modelcfg MODEL_PROJECTILE_FLAME_CFG            "Models\\Projectiles\\Flame\\FlameProjectile.vmc",
 23 modelcfg MODEL_PROJECTILE_CATMAN_CFG           "Models\\Projectiles\\Catman\\CatmanProjectile.vmc",
 24 modelcfg MODEL_PROJECTILE_HM_FIRECRACKER_CFG   "Models\\Projectiles\\Headman\\HeadmanProjectile_Firecracker.vmc",
 25 modelcfg MODEL_PROJECTILE_HM_ROCKET_CFG        "Models\\Projectiles\\Headman\\HeadmanProjectile_Rocket.vmc",
 26 modelcfg MODEL_PROJECTILE_HM_BOMB_CFG          "Models\\Projectiles\\Headman\\HeadmanProjectile_Bomb.vmc",
 27 modelcfg MODEL_PROJECTILE_LAVA_CFG             "Models\\Projectiles\\Lava\\LavaProjectile.vmc",
 28 modelcfg MODEL_PROJECTILE_LASER_GREEN_CFG      "Models\\Projectiles\\Laser\\LaserProjectile_Green.vmc",
 29 modelcfg MODEL_PROJECTILE_LASER_BLUE_CFG       "Models\\Projectiles\\Laser\\LaserProjectile_Blue.vmc",
 30 modelcfg MODEL_PROJECTILE_BONEMAN_CFG          "Models\\Projectiles\\Boneman\\BonemanProjectile.vmc",
 31 modelcfg MODEL_PROJECTILE_WOMAN_CFG            "Models\\Projectiles\\Woman\\WomanProjectile.vmc",
 32 modelcfg MODEL_PROJECTILE_DRAGONMAN_CFG        "Models\\Projectiles\\Dragonman\\DragonmanProjectile.vmc",
 33 modelcfg MODEL_PROJECTILE_DRAGONMAN_STRONG_CFG "Models\\Projectiles\\Dragonman\\DragonmanProjectile_Strong.vmc",
 34 modelcfg MODEL_PROJECTILE_ELEM_STONE_CFG       "Models\\Projectiles\\Elemental\\ElementalProjectile_Stone.vmc",
 35 modelcfg MODEL_PROJECTILE_ELEM_ICE_CFG         "Models\\Projectiles\\Elemental\\ElementalProjectile_Ice.vmc",
 36 modelcfg MODEL_PROJECTILE_ELEM_LAVA_BOMB_CFG   "Models\\Projectiles\\Elemental\\ElementalProjectile_LavaBomb.vmc",
 37 modelcfg MODEL_PROJECTILE_ELEM_LAVA_STONE_CFG  "Models\\Projectiles\\Elemental\\ElementalProjectile_LavaStone.vmc",
 38 modelcfg MODEL_PROJECTILE_HUANMAN_CFG          "Models\\Projectiles\\Huanman\\HuanmanProjectile.vmc",
 39 modelcfg MODEL_PROJECTILE_FISHMAN_CFG          "Models\\Projectiles\\Fishman\\FishmanProjectile.vmc",
 40 modelcfg MODEL_PROJECTILE_MANTAMAN_CFG         "Models\\Projectiles\\Mantaman\\MantamanProjectile.vmc",
 41 modelcfg MODEL_PROJECTILE_CYBORG_BOMB_CFG      "Models\\Projectiles\\Cyborg\\CyborgProjectile_Bomb.vmc",
 42 modelcfg MODEL_PROJECTILE_GRUNT_SOLDIER_CFG    "Models\\Projectiles\\Grunt\\GruntProjectile_Soldier.vmc",
 43 modelcfg MODEL_PROJECTILE_GRUNT_COMMANDER_CFG  "Models\\Projectiles\\Grunt\\GruntProjectile_Commander.vmc",
 44 modelcfg MODEL_PROJECTILE_BEAST_CFG            "Models\\Projectiles\\Beast\\BeastProjectile.vmc",
 45 modelcfg MODEL_PROJECTILE_BEAST_BIG_CFG        "Models\\Projectiles\\Beast\\BeastProjectile_Big.vmc",
 46 modelcfg MODEL_PROJECTILE_DEMON_CFG            "Models\\Projectiles\\Demon\\DemonProjectile.vmc",
 47 modelcfg MODEL_PROJECTILE_SHTR_WOODEN_DART     "Models\\Projectiles\\Shooter\\WoodenDartProjectile.vmc",
 48 modelcfg MODEL_PROJECTILE_GUFFY_CFG            "Models\\Projectiles\\Guffy\\GuffyProjectile.vmc",
 49 modelcfg MODEL_PROJECTILE_LARVA_PLASMA_CFG     "Models\\Projectiles\\ExotechLarva\\LarvaProjectile_Plasma.vmc",
 50 modelcfg MODEL_PROJECTILE_LARVA_TAIL_CFG       "Models\\Projectiles\\ExotechLarva\\LarvaProjectile_Tail.vmc",
 51 modelcfg MODEL_PROJECTILE_AIRELEMENTAL_WINDBLAST_CFG "Models\\Projectiles\\AirElemental\\WindblastProjectile.vmc",

// ********* SOUNDS *********
100 sound   SOUND_FLYING        "Sounds\\Weapons\\RocketFly.wav",
101 sound   SOUND_BEAST_FLYING  "Sounds\\Weapons\\ProjectileFly.wav",
102 sound   SOUND_GRENADE_BOUNCE  "Models\\Weapons\\GrenadeLauncher\\Sounds\\Bounce.wav",
103 sound   SOUND_DEMON_FLYING      "SoundsMP\\Weapons\\ProjectileFly.wav",
104 sound   SOUND_LARVETTE            "ModelsMP\\Enemies\\ExotechLarva\\Sounds\\Squeak.wav",
106 sound   SOUND_METEOR_BLAST       "SoundsMP\\Weapons\\MeteorBlast.wav",

220 model   MODEL_MARKER     "Models\\Editor\\Axis.mdl",
221 texture TEXTURE_MARKER   "Models\\Editor\\Vector.tex",

functions:

  // premoving
  void PreMoving(void)
  {
    if (m_tmExpandBox>0) {
      if (_pTimer->CurrentTick()>m_fStartTime+m_tmExpandBox) {
        ChangeCollisionBoxIndexWhenPossible(1);
        m_tmExpandBox = 0;
      }
    }

    CMovableModelEntity::PreMoving();
  }

  // postmoving
  void PostMoving(void)
  {
    CMovableModelEntity::PostMoving();

    // if flamer flame
    if (m_prtType==PRT_FLAME || m_prtType==PRT_SHOOTER_FLAME) {
      // if came to water
      CContentType &ctDn = GetWorld()->wo_actContentTypes[en_iDnContent];

      // stop existing
      if (!(ctDn.ct_ulFlags&CTF_BREATHABLE_LUNGS)) {
        m_fWaitAfterDeath = 0.0f;   // immediate stop
        SendEvent(EEnd());
      }
    }
  };

  /* Read from stream. */
  void Read_t(CTStream *istr) // throw char *
  {
    CMovableModelEntity::Read_t(istr);

    // setup light source
    if (m_bLightSource) {
      SetupLightSource(TRUE);
    }
  }

  // dump sync data to text file
  export void DumpSync_t(CTStream &strm, INDEX iExtensiveSyncCheck)  // throw char *
  {
    CMovableModelEntity ::DumpSync_t(strm, iExtensiveSyncCheck);
    strm.FPrintF_t("projectile type: %d\n", m_prtType);
    strm.FPrintF_t("launcher:");

    if (m_penLauncher!=NULL) {
      strm.FPrintF_t("id:%05d '%s'(%s) (%g, %g, %g)\n",
        m_penLauncher->en_ulID,
        m_penLauncher->GetName(), m_penLauncher->GetClass()->ec_pdecDLLClass->dec_strName,
        m_penLauncher->GetPlacement().pl_PositionVector(1),
        m_penLauncher->GetPlacement().pl_PositionVector(2),
        m_penLauncher->GetPlacement().pl_PositionVector(3));
    } else {
      strm.FPrintF_t("<none>\n");
    }
  }

  /* Get static light source information. */
  CLightSource *GetLightSource(void)
  {
    if (m_bLightSource && !IsPredictor()) {
      return &m_lsLightSource;
    } else {
      return NULL;
    }
  }

  export void Copy(CEntity &enOther, ULONG ulFlags)
  {
    CMovableModelEntity::Copy(enOther, ulFlags);
    CProjectileEntity *penOther = (CProjectileEntity *)(&enOther);

    if (ulFlags&COPY_PREDICTOR) {
      //m_lsLightSource;
      //SetupLightSource(); //? is this ok !!!!
      m_bLightSource = FALSE;
    }
  }

  BOOL AdjustShadingParameters(FLOAT3D &vLightDirection, COLOR &colLight, COLOR &colAmbient)
  {
    // if time now is inside invisibility time, don't render model
    CModelObject *pmo = GetModelObject();

    if ((pmo != NULL) && (_pTimer->GetLerpedCurrentTick() < (m_fStartTime+m_tmInvisibility))) {
      // make it invisible
      pmo->mo_colBlendColor = 0;
    } else {
      // make it visible
      pmo->mo_colBlendColor = C_WHITE|CT_OPAQUE;
    }

    return CEntity::AdjustShadingParameters(vLightDirection, colLight, colAmbient);
  }

  // Setup light source
  void SetupLightSource(BOOL bLive)
  {
    // setup light source
    CLightSource lsNew;
    lsNew.ls_ulFlags = LSF_NONPERSISTENT|LSF_DYNAMIC;
    lsNew.ls_rHotSpot = 0.0f;

    switch (m_prtType)
    {
      case PRT_ROCKET:
      case PRT_WALKER_ROCKET:
      case PRT_DEVIL_ROCKET:
        if (bLive)
        {
          lsNew.ls_colColor = 0xA0A080FF;
        }
        else
        {
          lsNew.ls_colColor = C_BLACK|CT_OPAQUE;
        }
        lsNew.ls_rFallOff = 5.0f;
        lsNew.ls_plftLensFlare = &_lftYellowStarRedRingFar;
        break;

      case PRT_GUFFY_PROJECTILE:
        lsNew.ls_colColor = C_BLUE;
        lsNew.ls_rFallOff = 1.5f;
        lsNew.ls_plftLensFlare = NULL;

      case PRT_GRENADE:
        lsNew.ls_colColor = 0x2F1F0F00;
        lsNew.ls_rFallOff = 2.0f;
        lsNew.ls_rHotSpot = 0.2f;
        lsNew.ls_plftLensFlare = &_lftYellowStarRedRingFar;
        break;

      case PRT_FLAME:
        lsNew.ls_colColor = C_dORANGE;
        lsNew.ls_rFallOff = 1.0f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_LASER_RAY:
        lsNew.ls_colColor = C_vdGREEN;
        lsNew.ls_rFallOff = 1.5f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_GRUNT_PROJECTILE_SOL:
        lsNew.ls_colColor = C_vdRED;
        lsNew.ls_rFallOff = 1.5f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_GRUNT_PROJECTILE_COM:
        lsNew.ls_colColor = C_vdRED;
        lsNew.ls_rFallOff = 1.5f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_CATMAN_FIRE:
        lsNew.ls_colColor = C_BLUE;
        lsNew.ls_rFallOff = 3.5f;
        lsNew.ls_plftLensFlare = &_lftCatmanFireGlow;
        break;

      case PRT_HEADMAN_FIRECRACKER:
        lsNew.ls_colColor = C_ORANGE;
        lsNew.ls_rFallOff = 1.5f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_HEADMAN_ROCKETMAN:
        lsNew.ls_colColor = C_YELLOW;
        lsNew.ls_rFallOff = 1.5f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_WOMAN_FIRE:
        lsNew.ls_colColor = C_WHITE;
        lsNew.ls_rFallOff = 3.5f;
        lsNew.ls_plftLensFlare = &_lftCatmanFireGlow;
        break;

      case PRT_DRAGONMAN_FIRE:
        lsNew.ls_colColor = C_YELLOW;
        lsNew.ls_rFallOff = 3.5f;
        lsNew.ls_plftLensFlare = &_lftProjectileYellowBubbleGlow;
        break;

      case PRT_DRAGONMAN_STRONG_FIRE:
        lsNew.ls_colColor = C_RED;
        lsNew.ls_rFallOff = 3.5f;
        lsNew.ls_plftLensFlare = &_lftProjectileStarGlow;
        break;

      case PRT_HUANMAN_FIRE:
        lsNew.ls_colColor = C_lBLUE;
        lsNew.ls_rFallOff = 2.0f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_FISHMAN_FIRE:
        lsNew.ls_colColor = C_lBLUE;
        lsNew.ls_rFallOff = 2.0f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_MANTAMAN_FIRE:
        lsNew.ls_colColor = C_lBLUE;
        lsNew.ls_rFallOff = 2.0f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_CYBORG_LASER:
        lsNew.ls_colColor = C_dBLUE;
        lsNew.ls_rFallOff = 1.5f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      case PRT_DEVIL_LASER:
        lsNew.ls_colColor = C_dBLUE;
        lsNew.ls_rFallOff = 5.0f;
        lsNew.ls_plftLensFlare = &_lftYellowStarRedRingFar;
        break;

      case PRT_LARVA_PLASMA:
        lsNew.ls_colColor = C_dBLUE;
        lsNew.ls_rFallOff = 5.0f;
        lsNew.ls_plftLensFlare = &_lftCatmanFireGlow;
        break;

      case PRT_SHOOTER_FIREBALL:
        lsNew.ls_colColor = C_dORANGE;
        lsNew.ls_rFallOff = 5.0f;
        lsNew.ls_plftLensFlare = &_lftYellowStarRedRingFar;
        break;

      case PRT_SHOOTER_FLAME:
        lsNew.ls_colColor = C_dORANGE;
        lsNew.ls_rFallOff = 1.0f;
        lsNew.ls_plftLensFlare = NULL;
        break;

      default:
        ASSERTALWAYS("Unknown light source");
    }

    lsNew.ls_ubPolygonalMask = 0;
    lsNew.ls_paoLightAnimation = NULL;

    m_lsLightSource.ls_penEntity = this;
    m_lsLightSource.SetLightSource(lsNew);
  }

  // render particles
  void RenderParticles(void)
  {
    switch (m_prtType)
    {
      case PRT_ROCKET:
      case PRT_WALKER_ROCKET: Particles_RocketTrail(this, 1.0f); break;
      case PRT_DEVIL_ROCKET: Particles_RocketTrail(this, 8.0f); break;
      case PRT_GUFFY_PROJECTILE: break;// Particles_RocketTrail(this, 1.0f); break;
      case PRT_GRENADE: {
        //Particles_GrenadeTrail(this);
        FLOAT fSpeedRatio = en_vCurrentTranslationAbsolute.Length()/140.0f;
        Particles_CannonBall(this, fSpeedRatio);
        break;
      }

      case PRT_FLAME: {
        // elapsed time
        FLOAT fLeaderLiving, fFollowerLiving, fInFrontLiving;
        fInFrontLiving=0.05f;
        fLeaderLiving = _pTimer->GetLerpedCurrentTick() - m_fStartTime;

        // not NULL or deleted
        if (m_penParticles!=NULL && !(m_penParticles->GetFlags()&ENF_DELETED)) {
          FLOAT3D vDirLeader=en_vCurrentTranslationAbsolute;
          vDirLeader.Normalize();
          // if last is not flame thrower pipe
          if (IsOfClass(m_penParticles, &CProjectileEntity_DLLClass))
          {
            CProjectileEntity &prLast=(CProjectileEntity &)*m_penParticles;
            // if pre last is flame thrower pipe
            if (IsOfClass(prLast.m_penParticles, "Player Weapons"))
            {
              CPlayerWeaponEntity &plw=(CPlayerWeaponEntity&)*prLast.m_penParticles;
              if (!(plw.GetPlayer()->GetFlags()&ENF_ALIVE))
              {
                return;
              }
              CPlacement3D plPipe, plInFrontOfPipe;
              ((CPlayerWeaponEntity&)*prLast.m_penParticles).GetFlamerSourcePlacement(plPipe, plInFrontOfPipe);
              fFollowerLiving = _pTimer->GetLerpedCurrentTick() - ((CProjectileEntity&)*m_penParticles).m_fStartTime;
              FLOAT3D vDirPipeFront;
              AnglesToDirectionVector(plInFrontOfPipe.pl_OrientationAngle, vDirPipeFront);
              vDirPipeFront.Normalize();
              Particles_FlameThrower(GetLerpedPlacement(), plInFrontOfPipe,
                                     vDirLeader, vDirPipeFront,
                                     fLeaderLiving, fInFrontLiving, en_ulID, FALSE);
            }
            // draw particles with another projectile
            else
            {
              fFollowerLiving = _pTimer->GetLerpedCurrentTick() - ((CProjectileEntity&)*m_penParticles).m_fStartTime;
              FLOAT3D vDirFollower = ((CMovableModelEntity*)(CEntity*)m_penParticles)->en_vCurrentTranslationAbsolute;
              vDirFollower.Normalize();
              Particles_FlameThrower(GetLerpedPlacement(), m_penParticles->GetLerpedPlacement(),
                                     vDirLeader, vDirFollower, fLeaderLiving, fFollowerLiving, en_ulID, FALSE);
            }
          // draw particles with player weapons
          } else if (IsOfClass(m_penParticles, "Player Weapons")) {
            CPlayerWeaponEntity &plw=(CPlayerWeaponEntity&)*m_penParticles;
            if (!(plw.GetPlayer()->GetFlags()&ENF_ALIVE))
            {
              return;
            }
            CPlacement3D plPipe, plInFrontOfPipe;
            plw.GetFlamerSourcePlacement(plPipe, plInFrontOfPipe);
            FLOAT3D vDirPipeFront;
            AnglesToDirectionVector(plInFrontOfPipe.pl_OrientationAngle, vDirPipeFront);
            FLOAT3D vViewDir;
            AnglesToDirectionVector(plPipe.pl_OrientationAngle, vViewDir);
            FLOAT3D vDirFollower = vViewDir.Normalize();

            /*
            Particles_FlameThrower(GetLerpedPlacement(), plPipe,
              vDirLeader, vDirFollower,
              fLeaderLiving, 0.0f, en_ulID, TRUE);
              */

            Particles_FlameThrower(plInFrontOfPipe, plPipe,
              vDirPipeFront, vDirFollower,
              fInFrontLiving, 0.0f, en_ulID, TRUE);

            Particles_FlameThrowerStart(plPipe, plw.m_tmFlamerStart, plw.m_tmFlamerStop);
          }
        }

        break;
      }

      case PRT_CATMAN_FIRE: Particles_RocketTrail(this, 1.0f); break;
      case PRT_HEADMAN_FIRECRACKER: Particles_FirecrackerTrail(this); break;
      case PRT_HEADMAN_ROCKETMAN: Particles_Fireball01Trail(this); break;
      case PRT_HEADMAN_BOMBERMAN: Particles_BombTrail(this); break;
      case PRT_LAVA_COMET: Particles_LavaTrail(this); break;
      case PRT_LAVAMAN_BIG_BOMB: Particles_LavaBombTrail(this, 4.0f); break;
      case PRT_LAVAMAN_BOMB: Particles_LavaBombTrail(this, 1.0f); break;
      case PRT_BEAST_PROJECTILE: Particles_BeastProjectileTrail(this, 2.0f, 0.25f, 48); break;
      case PRT_BEAST_BIG_PROJECTILE:
      case PRT_DEMON_FIREBALL:
        Particles_BeastBigProjectileTrail(this, 4.0f, 0.25f, 0.0f, 64);
        Particles_AfterBurner(this, m_fStartTime, 1.0f);
        break
;
      case PRT_DEVIL_GUIDED_PROJECTILE:
        Particles_BeastBigProjectileTrail(this, 6.0f, 0.375f, 0.0f, 64);
        break;

      case PRT_BEAST_DEBRIS: Particles_BeastProjectileDebrisTrail(this, 0.20f); break;
      case PRT_BEAST_BIG_DEBRIS: Particles_BeastProjectileDebrisTrail(this, 0.25f); break;
      case PRT_SHOOTER_WOODEN_DART: Particles_RocketTrail(this, 0.25f); break;
      case PRT_SHOOTER_FIREBALL: Particles_Fireball01Trail(this); break;
      case PRT_SHOOTER_FLAME: {
        // elapsed time
        FLOAT fTimeElapsed, fParticlesTimeElapsed;
        fTimeElapsed = _pTimer->GetLerpedCurrentTick() - m_fStartTime;

        // not NULL or deleted
        if (m_penParticles!=NULL && !(m_penParticles->GetFlags()&ENF_DELETED)) {
          // draw particles with another projectile
          if (IsOfClass(m_penParticles, &CProjectileEntity_DLLClass)) {
            fParticlesTimeElapsed = _pTimer->GetLerpedCurrentTick() - ((CProjectileEntity&)*m_penParticles).m_fStartTime;
            Particles_ShooterFlame(GetLerpedPlacement(), m_penParticles->GetLerpedPlacement(),
                                   fTimeElapsed, fParticlesTimeElapsed);
          } else if (IsOfClass(m_penParticles, "Shooter")) {
            Particles_ShooterFlame(GetLerpedPlacement(),
              ((CShooterEntity&)*m_penParticles).GetPlacement(),
                                   fTimeElapsed, 0.0f);
          }
        }

        break;
      }

      case PRT_METEOR:
        Particles_MeteorTrail(this, m_fStretch, 1.0f, en_vCurrentTranslationAbsolute);
        Particles_AfterBurner(this, m_fStartTime, m_fStretch*4.0f, 2);
        break;

      case PRT_AFTERBURNER_DEBRIS:
        Particles_AfterBurner(this, m_fStartTime, m_fStretch);
        break;

      case PRT_AIRELEMENTAL_WIND:
        Particles_Windblast(this, m_fStretch/4.0f, m_fStartTime+3.0f);
        break;
    }
  }

/************************************************************
 *              PLAYER ROCKET / GRENADE                     *
 ************************************************************/
void PlayerRocket(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);

  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));

  // play the flying sound
  m_soEffect.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_FLYING, SOF_3D|SOF_LOOP);
  m_fFlyTime = 30.0f;

  if (GetSP()->sp_bCooperative) {
    m_iDamageAmount = 100;
    m_iRangeDamageAmount = 50;
  } else {
    m_iDamageAmount = 75;
    m_iRangeDamageAmount = 75;
  }

  m_fDamageHotSpotRange = 4.0f;
  m_fDamageFallOffRange = 8.0f;
  m_fSoundRange = 50.0f;
  m_bExplode = TRUE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = TRUE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 1.125f;
  m_tmExpandBox = 0.1f;
  m_tmInvisibility = 0.05f;
  SetHealthInit(5 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_FLYING;
};

void WalkerRocket(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);

  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));

  // play the flying sound
  m_soEffect.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_FLYING, SOF_3D|SOF_LOOP);
  m_fFlyTime = 30.0f;

  if (GetSP()->sp_gdGameDifficulty<=CSessionProperties::GD_EASY) {
    m_iDamageAmount = 40;
    m_iRangeDamageAmount = 20;
  } else {
    m_iDamageAmount = 100;
    m_iRangeDamageAmount = 50;
  }

  m_fDamageHotSpotRange = 4.0f;
  m_fDamageFallOffRange = 8.0f;
  m_fSoundRange = 50.0f;
  m_bExplode = TRUE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = TRUE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 1.125f;
  m_tmExpandBox = 0.1f;
  m_tmInvisibility = 0.05f;
  SetHealthInit(5 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_FLYING;
};

void WalkerRocketExplosion(void)
{
  PlayerRocketExplosion();
}

void PlayerRocketExplosion(void)
{
  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  // explosion
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_ROCKET;
  ese.vStretch = FLOAT3D(1,1,1);
  SpawnEffect(GetPlacement(), ese);

  // spawn sound event in range
  if (m_penLauncher != NULL && m_penLauncher->IsPlayerControlled()) {
    SpawnRangeSound(m_penLauncher, this, SNDT_PLAYER, m_fSoundRange);
  }

  // explosion debris
  ese.betType = BET_EXPLOSION_DEBRIS;
  SpawnEffect(GetPlacement(), ese);

  // explosion smoke
  ese.betType = BET_EXPLOSION_SMOKE;
  SpawnEffect(GetPlacement(), ese);

  // on plane
  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge)) {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f) {
      // stain
      ese.betType = BET_EXPLOSIONSTAIN;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
      // shock wave
      ese.betType = BET_SHOCKWAVE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
      // second explosion on plane
      ese.betType = BET_ROCKET_PLANE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint+ese.vNormal/50.0f, ANGLE3D(0, 0, 0)), ese);
    }
  }
};


void PlayerGrenade(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_BOUNCING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);

  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsFreeProjectile(FLOAT3D(0.0f, 5.0f, -m_fSpeed), (CMovableEntity*)&*m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, FRnd()*120.0f+120.0f, FRnd()*250.0f-125.0f));
  en_fBounceDampNormal   = 0.75f;
  en_fBounceDampParallel = 0.6f;
  en_fJumpControlMultiplier = 0.0f;
  en_fCollisionSpeedLimit = 45.0f;
  en_fCollisionDamageFactor = 10.0f;
  m_fFlyTime = 3.0f;
  m_iDamageAmount = 75;
  m_iRangeDamageAmount = 100;
  m_fDamageHotSpotRange = 4.0f;
  m_fDamageFallOffRange = 8.0f;
  m_fSoundRange = 50.0f;
  m_bExplode = TRUE;
  en_fDeceleration = 25.0f;
  m_bLightSource = TRUE;
  m_bCanHitHimself = TRUE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  SetHealthInit(20 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_SLIDING;
  m_tmInvisibility = 0.05f;
  m_tmExpandBox = 0.1f;
};

void PlayerGrenadeExplosion(void)
{
  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  // explosion
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_GRENADE;
  ese.vStretch = FLOAT3D(1,1,1);
  SpawnEffect(GetPlacement(), ese);

  // spawn sound event in range
  if (m_penLauncher != NULL && m_penLauncher->IsPlayerControlled()) {
    SpawnRangeSound(m_penLauncher, this, SNDT_PLAYER, m_fSoundRange);
  }

  // on plane
  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge)) {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f) {
      // wall stain
      ese.betType = BET_EXPLOSIONSTAIN;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
      // shock wave
      ese.betType = BET_SHOCKWAVE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
      // second explosion on plane
      ese.betType = BET_GRENADE_PLANE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint+ese.vNormal/50.0f, ANGLE3D(0, 0, 0)), ese);
    }
  }
};



/************************************************************
 *                    PLAYER FLAME                          *
 ************************************************************/
void PlayerFlame(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsEditorModel();
  SetPhysicsFlags(EPF_MODEL_SLIDING&~EPF_TRANSLATEDBYGRAVITY&~EPF_ORIENTEDBYGRAVITY);
  //SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);

  SetAnyModel(idModelConfig);

  // add player's forward velocity to flame
  CMovableEntity *penPlayer = (CMovableEntity*)(CEntity*)m_penLauncher;
  FLOAT3D vDirection = penPlayer->en_vCurrentTranslationAbsolute;
  FLOAT3D vFront = -GetRotationMatrix().GetColumn(3);
  FLOAT fSpeedFwd = ClampDn(vDirection%vFront, 0.0f);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -(25.0f+fSpeedFwd)), penPlayer);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 1.0f;
  m_iDamageAmount = (GetSP()->sp_bCooperative) ? 10 : 4;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.3f;
  m_tmExpandBox = 0.1f;
  m_pmtMove = PMT_SLIDING;
};


/************************************************************
 *                    PLAYER LASER                          *
 ************************************************************/
void PlayerLaserRay(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetFlags(GetFlags() | ENF_SEETHROUGH);

  SetAnyModel(idModelConfig);

  CModelObject *pmo = GetModelObject();
  if (pmo != NULL)
  {
    pmo->PlayAnim(LASERPROJECTILE_ANIM_GROW, 0);
  }

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -120.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 3.0f;
  m_iDamageAmount = 20;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_tmExpandBox = 0.1f;

  // time when laser ray becomes visible
  m_tmInvisibility = 0.025f;
  m_pmtMove = PMT_FLYING;
};

void PlayerLaserWave(void)
{
  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  // on plane
  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge)) {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f) {
      // shock wave
      ese.colMuliplier = C_dRED|CT_OPAQUE;
      ese.betType = BET_LASERWAVE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
    }
  }
};

/************************************************************
 *                   CATMAN PROJECTILE                      *
 ************************************************************/
void CatmanProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -15.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 5;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

/************************************************************
 *                   HEADMAN PROJECTILE                     *
 ************************************************************/
void HeadmanFirecracker(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_SLIDING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(0.75f, 0.75f, 0.75f));
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -25.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, FRnd()*20.0f-10.0f));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 4;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_SLIDING;
};

void HeadmanRocketman(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(0.5f, 0.5f, 0.5f));
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 5;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

void HeadmanBomberman(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_BOUNCING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsFreeProjectile(FLOAT3D(0.0f, 0.0f, -m_fSpeed), (CMovableEntity*)&*m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, FRnd()*360.0f-180.0f, FRnd()*360.0f-180.0f));
  m_fFlyTime = 2.5f;
  m_iDamageAmount = 10;
  m_iRangeDamageAmount = 15;
  m_fDamageHotSpotRange = 1.0f;
  m_fDamageFallOffRange = 6.0f;
  m_fSoundRange = 25.0f;
  m_bExplode = TRUE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = TRUE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  SetHealthInit(5 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_FLYING;
};

void HeadmanBombermanExplosion(void)
{
  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  // explosion
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_BOMB;
  ese.vStretch = FLOAT3D(1.0f,1.0f,1.0f);
  SpawnEffect(GetPlacement(), ese);

  // on plane
  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge)) {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f) {
      // wall stain
      ese.betType = BET_EXPLOSIONSTAIN;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
      ese.betType = BET_GRENADE_PLANE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint+ese.vNormal/50.0f, ANGLE3D(0, 0, 0)), ese);
    }
  }
};

void CyborgBombExplosion(void)
{
  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  // explosion
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_BOMB;
  ese.vStretch = FLOAT3D(1.0f,1.0f,1.0f);
  SpawnEffect(GetPlacement(), ese);

  // on plane
  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge)) {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f) {
      // wall stain
      ese.betType = BET_EXPLOSIONSTAIN;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
      ese.betType = BET_GRENADE_PLANE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint+ese.vNormal/50.0f, ANGLE3D(0, 0, 0)), ese);
    }
  }
};

/************************************************************
 *                  BONEMAN PROJECTILE                      *
 ************************************************************/
void BonemanProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 10;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_SLIDING;
};

/************************************************************
 *                   WOMAN PROJECTILE                       *
 ************************************************************/
void WomanProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 8;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

/************************************************************
 *                  DRAGONMAN PROJECTILE                    *
 ************************************************************/
void DragonmanProjectile(INDEX iType)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_ONBLOCK_SLIDE|EPF_PUSHABLE|EPF_MOVABLE);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);

  // start moving
  if (iType==DRAGONMAN_STRONG) {
    LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -40.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
    m_iDamageAmount = 14;
  } else {
    LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
    m_iDamageAmount = 7;
  }

  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

/************************************************************
 *                  ELEMENTAL PROJECTILE                    *
 ************************************************************/
void ElementalRock(INDEX iSize, INDEX iType)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_ONBLOCK_SLIDE|EPF_PUSHABLE|EPF_MOVABLE);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);

  SetAnyModel(idModelConfig);

  if (iSize==ELEMENTAL_LARGE) {
    GetModelObject()->StretchModel(FLOAT3D(2.25f, 2.25f, 2.25f));
  } else if (iSize==ELEMENTAL_BIG) {
    GetModelObject()->StretchModel(FLOAT3D(0.75f, 0.75f, 0.75f));
  } else {
    GetModelObject()->StretchModel(FLOAT3D(0.4f, 0.4f, 0.4f));
  }

  ModelChangeNotify();

  // start moving
  if (iSize==ELEMENTAL_LARGE) {
    LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -80.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
    m_iDamageAmount = 20;
    SetHealthInit(40 * HEALTH_VALUE_MULTIPLIER);
  } else if (iSize==ELEMENTAL_BIG) {
    LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -50.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
    m_iDamageAmount = 13; // 12.5f;
    SetHealthInit(20 * HEALTH_VALUE_MULTIPLIER);
  } else {
    LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
    m_iDamageAmount = 7;
    SetHealthInit(10 * HEALTH_VALUE_MULTIPLIER);
  }

  SetDesiredRotation(ANGLE3D(0, 0, FRnd()*1800.0f-900.0f));
  en_fCollisionSpeedLimit = 1000.0f;
  en_fCollisionDamageFactor = 0.0f;
  m_fFlyTime = 5.0f;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_SLIDING;
};

void LavaManBomb(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_BOUNCING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);

  if (m_prtType == PRT_LAVAMAN_BIG_BOMB)
  {
    GetModelObject()->StretchModel(FLOAT3D(6.0f, 6.0f, 6.0f));
    m_iDamageAmount = 20;
    m_iRangeDamageAmount = 10;
    m_fDamageHotSpotRange = 7.5f;
    m_fDamageFallOffRange = 15.0f;
    SetHealthInit(30 * HEALTH_VALUE_MULTIPLIER);
  } else if (m_prtType == PRT_LAVAMAN_BOMB) {
    GetModelObject()->StretchModel(FLOAT3D(1.5f, 1.5f, 1.5f));
    m_iDamageAmount = 10;
    m_iRangeDamageAmount =  5;
    m_fDamageHotSpotRange = 5.0f;
    m_fDamageFallOffRange = 10.0f;
    SetHealthInit(10 * HEALTH_VALUE_MULTIPLIER);
  }

  ModelChangeNotify();

  // start moving
  LaunchAsFreeProjectile(FLOAT3D(0.0f, 0.0f, -m_fSpeed), (CMovableEntity*)&*m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, FRnd()*360.0f-180.0f, 0.0f));
  m_fFlyTime = 20.0f;
  m_fSoundRange = 50.0f;
  m_bExplode = TRUE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_pmtMove = PMT_FLYING;
  m_fWaitAfterDeath = 4.0f;

  if (m_prtType == PRT_LAVAMAN_BIG_BOMB)
  {
    // spawn particle debris
    CPlacement3D plSpray = GetPlacement();
    CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
    penSpray->SetParent(this);
    ESpawnSpray eSpawnSpray;
    eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
    eSpawnSpray.fDamagePower = 4.0f;
    eSpawnSpray.fSizeMultiplier = 0.5f;
    eSpawnSpray.sptType = SPT_LAVA_STONES;
    eSpawnSpray.vDirection = FLOAT3D(0,-0.5f,0);
    eSpawnSpray.penOwner = this;
    penSpray->Initialize(eSpawnSpray);
  }
}

void LavamanBombExplosion(void)
{
  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge))
  {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f)
    {
      // shock wave
      ese.colMuliplier = C_WHITE|CT_OPAQUE;
      ese.betType = BET_SHOCKWAVE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
    }
  }

  // shock wave
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_LIGHT_CANNON;
  ese.vStretch = FLOAT3D(4,4,4);
  SpawnEffect(GetPlacement(), ese);

  // spawn particle debris
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 4.0f;
  eSpawnSpray.fSizeMultiplier = 0.5f;
  eSpawnSpray.sptType = SPT_LAVA_STONES;
  eSpawnSpray.vDirection = en_vCurrentTranslationAbsolute/32.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);

  // spawn smaller lava bombs
  for (INDEX iDebris=0; iDebris<3+IRnd()%3; iDebris++)
  {
    FLOAT fHeading = (FRnd()-0.5f)*180.0f;
    FLOAT fPitch = 10.0f+FRnd()*40.0f;
    FLOAT fSpeed = 10.0+FRnd()*50.0f;

    // launch
    CPlacement3D pl = GetPlacement();
    pl.pl_PositionVector(2) += 2.0f;
    pl.pl_OrientationAngle = m_penLauncher->GetPlacement().pl_OrientationAngle;
    pl.pl_OrientationAngle(1) += AngleDeg(fHeading);
    pl.pl_OrientationAngle(2) = AngleDeg(fPitch);

    CEntityPointer penProjectile = CreateEntity(pl, CLASS_PROJECTILE);
    ELaunchProjectile eLaunch;
    eLaunch.penLauncher = this;
    eLaunch.prtType = PRT_LAVAMAN_BOMB;
    eLaunch.fSpeed = fSpeed;
    penProjectile->Initialize(eLaunch);

    // spawn particle debris
    CPlacement3D plSpray = pl;
    CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
    penSpray->SetParent(penProjectile);
    ESpawnSpray eSpawnSpray;
    eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
    eSpawnSpray.fDamagePower = 1.0f;
    eSpawnSpray.fSizeMultiplier = 0.5f;
    eSpawnSpray.sptType = SPT_LAVA_STONES;
    eSpawnSpray.vDirection = FLOAT3D(0,-0.5f,0);
    eSpawnSpray.penOwner = penProjectile;
    penSpray->Initialize(eSpawnSpray);
  }
};

void LavamanBombDebrisExplosion(void)
{
  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  // spawn shock wave
  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge))
  {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f)
    {
      ese.colMuliplier = C_WHITE|CT_OPAQUE;
      ese.betType = BET_SHOCKWAVE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
    }
  }

  // spawn explosion
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_LIGHT_CANNON;
  ese.vStretch = FLOAT3D(2,2,2);
  SpawnEffect(GetPlacement(), ese);

  // spawn particle debris
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fSizeMultiplier = 4.0f;
  eSpawnSpray.fDamagePower = 2.0f;
  eSpawnSpray.sptType = SPT_LAVA_STONES;
  eSpawnSpray.vDirection = en_vCurrentTranslationAbsolute/16.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);
}

/************************************************************
 *                   HUANMAN PROJECTILE                     *
 ************************************************************/
void HuanmanProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(0.5f, 0.5f, 0.5f));
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 10;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

/************************************************************
 *                   BEAST PROJECTILE                       *
 ************************************************************/
void BeastProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // we need target for guied misile
  if (IsDerivedFromClass(m_penLauncher, &CEnemyBaseEntity_DLLClass)) {
    m_penTarget = ((CEnemyBaseEntity *) &*m_penLauncher)->m_penEnemy;
  }
  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_FREE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(1.5f, 1.5f, 1.5f));

  ModelChangeNotify();

  // play the flying sound
  m_soEffect.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_BEAST_FLYING, SOF_3D|SOF_LOOP);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -60.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 10.0f;
  m_iDamageAmount = 10;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_GUIDED;
  m_fGuidedMaxSpeedFactor = 30.0f;
  m_aRotateSpeed = 175.0f;
  SetHealthInit(10 * HEALTH_VALUE_MULTIPLIER);
};

void BeastBigProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // we need target for guided misile
  if (IsDerivedFromClass(m_penLauncher, &CEnemyBaseEntity_DLLClass)) {
    m_penTarget = ((CEnemyBaseEntity *) &*m_penLauncher)->m_penEnemy;
  }

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_FREE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(2.5f, 2.5f, 2.5f));

  ModelChangeNotify();

  // play the flying sound
  m_soEffect.Set3DParameters(50.0f, 2.0f, 1.0f, 0.75f);
  PlaySound(m_soEffect, SOUND_BEAST_FLYING, SOF_3D|SOF_LOOP);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -60.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 10.0f;
  m_iDamageAmount = 20;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_GUIDED_FAST;
  m_fGuidedMaxSpeedFactor = 90.0f;
  SetHealthInit(10000 * HEALTH_VALUE_MULTIPLIER);
  m_aRotateSpeed = 100.0f;
};

void BeastDebris(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_BOUNCING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(0.75f, 0.75f, 0.75f));
  GetModelObject()->StartAnim(1+(ULONG)FRnd()*5.0f);

  ModelChangeNotify();

  // start moving
  LaunchAsFreeProjectile(FLOAT3D(0.0f, 0.0f, -20.0f), (CMovableEntity*)&*m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 10.0f;
  m_iDamageAmount = 0;
  m_fSoundRange = 0;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
  SetHealthInit(1 * HEALTH_VALUE_MULTIPLIER);
  m_aRotateSpeed = 100.0f;
};

void BeastBigDebris(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_BOUNCING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(1.0f, 1.0f, 1.0f));
  GetModelObject()->StartAnim(1+(ULONG)FRnd()*5.0f);

  ModelChangeNotify();

  // start moving
  LaunchAsFreeProjectile(FLOAT3D(0.0f, 0.0f, -20.0f), (CMovableEntity*)&*m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 10.0f;
  m_iDamageAmount = 0;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
  SetHealthInit(1 * HEALTH_VALUE_MULTIPLIER);
  m_aRotateSpeed = 100.0f;
};

void BeastDebrisExplosion(void)
{
  // explosion
  ESpawnEffect ese;
  ese.colMuliplier = C_GREEN|CT_OPAQUE;
  ese.betType = BET_LIGHT_CANNON;
  ese.vStretch = FLOAT3D(0.75,0.75,0.75);
  SpawnEffect(GetPlacement(), ese);

  // spawn particles
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 2.0f;
  eSpawnSpray.fSizeMultiplier = 0.75f;
  eSpawnSpray.sptType = SPT_BEAST_PROJECTILE_SPRAY;
  eSpawnSpray.vDirection = en_vCurrentTranslationAbsolute/64.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);
}

void BeastBigDebrisExplosion(void)
{
  // explosion
  ESpawnEffect ese;
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_LIGHT_CANNON;
  ese.vStretch = FLOAT3D(1,1,1);
  SpawnEffect(GetPlacement(), ese);

  // spawn particles
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 2.0f;
  eSpawnSpray.fSizeMultiplier = 1.0f;
  eSpawnSpray.sptType = SPT_LAVA_STONES;
  eSpawnSpray.vDirection = en_vCurrentTranslationAbsolute/64.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);
}

void BeastProjectileExplosion(void)
{
  // explosion
  ESpawnEffect ese;
  ese.colMuliplier = C_GREEN|CT_OPAQUE;
  ese.betType = BET_LIGHT_CANNON;
  ese.vStretch = FLOAT3D(1.25,1.25,1.25);
  SpawnEffect(GetPlacement(), ese);

  // particles
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 2.0f;
  eSpawnSpray.fSizeMultiplier = 1.0f;
  eSpawnSpray.sptType = SPT_BEAST_PROJECTILE_SPRAY;
  eSpawnSpray.vDirection = en_vCurrentTranslationAbsolute/64.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);

  FLOAT fHeading = 20.0f+(FRnd()-0.5f)*60.0f;

  // debris
  for (INDEX iDebris=0; iDebris<2; iDebris++)
  {
    FLOAT fPitch = 10.0f+FRnd()*10.0f;
    FLOAT fSpeed = 5.0+FRnd()*20.0f;

    // launch
    CPlacement3D pl = GetPlacement();
    pl.pl_OrientationAngle(1) += AngleDeg(fHeading);

    // turn to other way
    fHeading = -fHeading;
    pl.pl_OrientationAngle(2) = AngleDeg(fPitch);

    CEntityPointer penProjectile = CreateEntity(pl, CLASS_PROJECTILE);
    ELaunchProjectile eLaunch;
    eLaunch.penLauncher = this;
    eLaunch.prtType = PRT_BEAST_DEBRIS;
    eLaunch.fSpeed = fSpeed;
    penProjectile->Initialize(eLaunch);

    // spawn particle debris
    CPlacement3D plSpray = pl;
    CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
    penSpray->SetParent(penProjectile);
    ESpawnSpray eSpawnSpray;
    eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
    eSpawnSpray.fDamagePower = 0.5f;
    eSpawnSpray.fSizeMultiplier = 0.25f;
    eSpawnSpray.sptType = SPT_BEAST_PROJECTILE_SPRAY;
    eSpawnSpray.vDirection = FLOAT3D(0,-0.5f,0);
    eSpawnSpray.penOwner = penProjectile;
    penSpray->Initialize(eSpawnSpray);
  }
}

void BeastBigProjectileExplosion(void)
{
  // explosion
  ESpawnEffect ese;
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_LIGHT_CANNON;
  ese.vStretch = FLOAT3D(2,2,2);
  SpawnEffect(GetPlacement(), ese);

  // particles
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 4.0f;
  eSpawnSpray.fSizeMultiplier = 0.5f;
  eSpawnSpray.sptType = SPT_LAVA_STONES;
  eSpawnSpray.vDirection = en_vCurrentTranslationAbsolute/32.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);

  // debris
  for (INDEX iDebris=0; iDebris<3+IRnd()%2; iDebris++)
  {
    FLOAT fHeading = (FRnd()-0.5f)*180.0f;
    FLOAT fPitch = 10.0f+FRnd()*40.0f;
    FLOAT fSpeed = 10.0+FRnd()*50.0f;

    // launch
    CPlacement3D pl = GetPlacement();
    pl.pl_OrientationAngle(1) += AngleDeg(fHeading);
    pl.pl_OrientationAngle(2) += AngleDeg(fPitch);

    CEntityPointer penProjectile = CreateEntity(pl, CLASS_PROJECTILE);
    ELaunchProjectile eLaunch;
    eLaunch.penLauncher = this;
    eLaunch.prtType = PRT_BEAST_BIG_DEBRIS;
    eLaunch.fSpeed = fSpeed;
    penProjectile->Initialize(eLaunch);

    // spawn particle debris
    CPlacement3D plSpray = pl;
    CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
    penSpray->SetParent(penProjectile);
    ESpawnSpray eSpawnSpray;
    eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
    eSpawnSpray.fDamagePower = 1.0f;
    eSpawnSpray.fSizeMultiplier = 0.5f;
    eSpawnSpray.sptType = SPT_LAVA_STONES;
    eSpawnSpray.vDirection = FLOAT3D(0,-0.5f,0);
    eSpawnSpray.penOwner = penProjectile;
    penSpray->Initialize(eSpawnSpray);
  }
}

/************************************************************
 *                   FISHMAN PROJECTILE                     *
 ************************************************************/
void FishmanProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);
  
  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 5;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

/************************************************************
 *                   MANTAMAN PROJECTILE                    *
 ************************************************************/
void MantamanProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -35.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 7;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

/************************************************************
 *               DEVIL PROJECTILES                          *
 ************************************************************/
void DevilLaser(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(4.0f, 4.0f, 2.0f));
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -DEVIL_LASER_SPEED), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 10;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

void DevilRocket(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(12.0f, 12.0f, 8.0f));
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -DEVIL_ROCKET_SPEED), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));

  // play the flying sound
  m_soEffect.Set3DParameters(100.0f, 2.0f, 1.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_FLYING, SOF_3D|SOF_LOOP);
  m_fFlyTime = 50.0f;
  m_iDamageAmount = 50;
  m_iRangeDamageAmount = 50;
  m_fDamageHotSpotRange = 2.0f;
  m_fDamageFallOffRange = 10.0f;
  m_fSoundRange = 100.0f;
  m_bExplode = TRUE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = TRUE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 1.125f;
  m_tmExpandBox = 10000.0f;
  m_tmInvisibility = 0.05f;
  SetHealthInit(25 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_FLYING;
};

void DevilRocketExplosion(void)
{
  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  // explosion
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_GRENADE;
  ese.vStretch = FLOAT3D(2,2,2);
  SpawnEffect(GetPlacement(), ese);

  // spawn sound event in range
  if (m_penLauncher != NULL && m_penLauncher->IsPlayerControlled()) {
    SpawnRangeSound(m_penLauncher, this, SNDT_PLAYER, m_fSoundRange);
  }

  // on plane
  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge)) {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f) {
      // stain
      ese.betType = BET_EXPLOSIONSTAIN;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      ese.vStretch = FLOAT3D(2,2,2);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);

      // shock wave
      ese.betType = BET_SHOCKWAVE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      ese.vStretch = FLOAT3D(2,2,2);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);

      // second explosion on plane
      ese.betType = BET_GRENADE_PLANE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      ese.vStretch = FLOAT3D(2,2,2);
      SpawnEffect(CPlacement3D(vPoint+ese.vNormal/50.0f, ANGLE3D(0, 0, 0)), ese);
    }
  }
};

void DevilGuidedProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // we need target for guied misile
  if (IsDerivedFromClass(m_penLauncher, &CEnemyBaseEntity_DLLClass)) {
    m_penTarget = ((CEnemyBaseEntity *) &*m_penLauncher)->m_penEnemy;
  }

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_FREE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(2.5f, 2.5f, 2.5f));
  ModelChangeNotify();

  // play the flying sound
  m_soEffect.Set3DParameters(250.0f, 2.0f, 1.0f, 0.75f);
  PlaySound(m_soEffect, SOUND_FLYING, SOF_3D|SOF_LOOP);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -80.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 20.0f;
  m_iDamageAmount = 20;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_GUIDED;
  m_fGuidedMaxSpeedFactor = 30.0f;
  SetHealthInit(30 * HEALTH_VALUE_MULTIPLIER);
  m_aRotateSpeed = 100.0f;
};

void DevilGuidedProjectileExplosion(void)
{
  // explosion
  ESpawnEffect ese;
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_LIGHT_CANNON;
  ese.vStretch = FLOAT3D(4,4,4);
  SpawnEffect(GetPlacement(), ese);

  // particles
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower =  8.0f;
  eSpawnSpray.fSizeMultiplier = 1.0f;
  eSpawnSpray.sptType = SPT_LAVA_STONES;
  eSpawnSpray.vDirection = en_vCurrentTranslationAbsolute/32.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);

  // debris
  for (INDEX iDebris=0; iDebris<3+IRnd()%2; iDebris++)
  {
    FLOAT fHeading = (FRnd()-0.5f)*180.0f;
    FLOAT fPitch = 10.0f+FRnd()*40.0f;
    FLOAT fSpeed = 10.0+FRnd()*50.0f;

    // launch
    CPlacement3D pl = GetPlacement();
    pl.pl_OrientationAngle(1) += AngleDeg(fHeading);
    pl.pl_OrientationAngle(2) += AngleDeg(fPitch);

    CEntityPointer penProjectile = CreateEntity(pl, CLASS_PROJECTILE);
    ELaunchProjectile eLaunch;
    eLaunch.penLauncher = this;
    eLaunch.prtType = PRT_BEAST_BIG_DEBRIS;
    eLaunch.fSpeed = fSpeed;
    penProjectile->Initialize(eLaunch);

    // spawn particle debris
    CPlacement3D plSpray = pl;
    CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
    penSpray->SetParent(penProjectile);
    ESpawnSpray eSpawnSpray;
    eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
    eSpawnSpray.fDamagePower = 2.0f;
    eSpawnSpray.fSizeMultiplier = 1.0f;
    eSpawnSpray.sptType = SPT_LAVA_STONES;
    eSpawnSpray.vDirection = FLOAT3D(0,-0.5f,0);
    eSpawnSpray.penOwner = penProjectile;
    penSpray->Initialize(eSpawnSpray);
  }
}

/************************************************************
 *               CYBORG LASER / PROJECTILE                  *
 ************************************************************/
void CyborgLaser(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -60.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 4.0f;
  m_iDamageAmount = 5;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

void CyborgBomb(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_BOUNCING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  ModelChangeNotify();

  // just freefall
  LaunchAsFreeProjectile(FLOAT3D(0.0f, 0.0f, -m_fSpeed), (CMovableEntity*)&*m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 2.5f;
  m_iDamageAmount = 10;
  m_iRangeDamageAmount = 15;
  m_fDamageHotSpotRange = 1.0f;
  m_fDamageFallOffRange = 6.0f;
  m_fSoundRange = 25.0f;
  m_bExplode = TRUE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = TRUE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  SetHealthInit(5 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_FLYING;
};

/************************************************************
 *                        LAVA BALL                         *
 ************************************************************/
void LavaBall(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_FALL);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsFreeProjectile(FLOAT3D(0.0f, 0.0f, -m_fSpeed), (CMovableEntity*)&*m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, FRnd()*360.0f-180.0f, FRnd()*360.0f-180.0f));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 5;
  m_iRangeDamageAmount = 5;
  m_fDamageHotSpotRange = 1.0f;
  m_fDamageFallOffRange = 4.0f;
  m_fSoundRange = 0.0f;
  m_bExplode = TRUE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_FLYING;
};

void LavaBallExplosion(void)
{
  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge)) {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f) {
      // shock wave
      ese.colMuliplier = C_WHITE|CT_OPAQUE;
      ese.betType = BET_SHOCKWAVE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);
    }
  }
};

/************************************************************
 *                 G R U N T   L A S E R                    *
 ************************************************************/

void GruntSoldierLaser(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);

  CModelObject *pmo = GetModelObject();

  if (pmo != NULL)
  {
    pmo->PlayAnim(GRUNTPROJECTILE_ANIM_DEFAULT, 0);
  }

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -45.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 3.0f;
  m_iDamageAmount = 10;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_tmExpandBox = 0.1f;

  // time when laser ray becomes visible
  m_tmInvisibility = 0.025f;
  m_pmtMove = PMT_FLYING;
};

void GruntCommanderLaser(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);

  CModelObject *pmo = GetModelObject();
  if (pmo != NULL)
  {
    pmo->PlayAnim(GRUNTPROJECTILE_ANIM_DEFAULT, 0);
  }

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -55.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 3.0f;
  m_iDamageAmount = 10;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_tmExpandBox = 0.1f;

  // time when laser ray becomes visible
  m_tmInvisibility = 0.025f;
  m_pmtMove = PMT_FLYING;
};

/************************************************************
 *                G U F F Y   R O C K E T                   *
 ************************************************************/

void GuffyProjectile(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(1.0f, 1.0f, 1.0f));

  CModelObject *pmo = GetModelObject();
  if (pmo != NULL)
  {
    pmo->PlayAnim(GUFFYPROJECTILE_ANIM_ROTATE01, AOF_LOOPING);
  }

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -50.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));

  // play the flying sound
  m_soEffect.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_FLYING, SOF_3D|SOF_LOOP);
  m_fFlyTime = 30.0f;
  m_iDamageAmount = 10;
  m_iRangeDamageAmount = 10;
  m_fDamageHotSpotRange = 4.0f;
  m_fDamageFallOffRange = 8.0f;
  m_fSoundRange = 50.0f;
  m_bExplode = TRUE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 1.125f;
  m_tmExpandBox = 0.1f;
  m_tmInvisibility = 0.05f;
  SetHealthInit(10000 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_FLYING;
};

void GuffyProjectileExplosion(void)
{
  PlayerRocketExplosion();
}

/************************************************************
 *                D E M O N   F I R E B A L L               *
 ************************************************************/
void DemonFireball(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // we need target for guided misile
  if (IsDerivedFromClass(m_penLauncher, &CEnemyBaseEntity_DLLClass)) {
    m_penTarget = ((CEnemyBaseEntity *) &*m_penLauncher)->m_penEnemy;
  }
  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_FREE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(2.5f, 2.5f, 2.5f));

  ModelChangeNotify();

  // play the flying sound
  m_soEffect.Set3DParameters(50.0f, 2.0f, 1.0f, 0.75f);
  PlaySound(m_soEffect, SOUND_BEAST_FLYING, SOF_3D|SOF_LOOP);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -100.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 12.0f;
  m_iDamageAmount = 20;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_GUIDED_FAST;
  m_fGuidedMaxSpeedFactor = 90.0f;
  SetHealthInit(10000 * HEALTH_VALUE_MULTIPLIER);
  m_aRotateSpeed = 200.0f;
};

void DemonFireballExplosion(void)
{
  // explosion
  ESpawnEffect ese;
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_LIGHT_CANNON;
  ese.vStretch = FLOAT3D(2,2,2);
  SpawnEffect(GetPlacement(), ese);

  // particles
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 4.0f;
  eSpawnSpray.fSizeMultiplier = 0.5f;
  eSpawnSpray.sptType = SPT_LAVA_STONES;
  eSpawnSpray.vDirection = en_vCurrentTranslationAbsolute/32.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);
}

/************************************************************
 *              L A R V A   P R O J E C T I L E S           *
 ************************************************************/

void LarvaPlasma(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(2.5f, 2.5f, 2.5f));
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -60.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));

  // play the flying sound
  m_soEffect.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_FLYING, SOF_3D|SOF_LOOP);
  m_fFlyTime = 30.0f;

  if (GetSP()->sp_bCooperative) {
    m_iDamageAmount = 30;
    m_iRangeDamageAmount = 30;
  } else {
    m_iDamageAmount = 25;
    m_iRangeDamageAmount = 25;
  }

  m_fDamageHotSpotRange = 4.0f;
  m_fDamageFallOffRange = 8.0f;
  m_fSoundRange = 50.0f;
  m_bExplode = TRUE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.05f;
  m_tmExpandBox = 0.1f;
  m_tmInvisibility = 0.05f;
  SetHealthInit(100 * HEALTH_VALUE_MULTIPLIER);
  m_iRebounds = 4;
  m_pmtMove = PMT_FLYING_REBOUNDING;
}

void LarvaPlasmaExplosion(void)
{
  // explosion
  ESpawnEffect ese;
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_LIGHT_CANNON;
  ese.vStretch = FLOAT3D(2,2,2);
  SpawnEffect(GetPlacement(), ese);

  // particles
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 1.0f;
  eSpawnSpray.fSizeMultiplier = 0.25f;
  eSpawnSpray.sptType = SPT_PLASMA;
  eSpawnSpray.vDirection = FLOAT3D(0.0f, 2.5f, 0.0f);
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);
}

void LarvaTail(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // we need target for guied misile
  if (IsDerivedFromClass(m_penLauncher, &CEnemyBaseEntity_DLLClass)) {
    m_penTarget = ((CEnemyBaseEntity *) &*m_penLauncher)->m_penEnemy;
  }

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_MODEL_SLIDING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(4.0f, 4.0f, 4.0f));

  ModelChangeNotify();

  // play the flying sound
  m_soEffect.Set3DParameters(50.0f, 10.0f, 1.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_LARVETTE, SOF_3D|SOF_LOOP);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 12.0f;
  m_iDamageAmount = 10;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_GUIDED_SLIDING;
  m_aRotateSpeed = 275.0f;
  SetHealthInit(10 * HEALTH_VALUE_MULTIPLIER);
}

void LarvaTailExplosion(void)
{
  PlayerRocketExplosion();
}

/*****************************************************************
 *       A I R   E L E M E N T A L   P R O J E C T I L E S       *
 *****************************************************************/

void WindBlast(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsEditorModel();
  SetPhysicsFlags(EPF_MODEL_SLIDING);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(3.0f, 3.0f, 3.0f));
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -50.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 5.0f;
  m_iDamageAmount = 20;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.0f;
  m_pmtMove = PMT_SLIDING;
}

/************************************************************
 *                    M E T E O R                           *
 ************************************************************/
void Meteor()
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(m_fStretch, m_fStretch, m_fStretch));
  ModelChangeNotify();

  Particles_AfterBurner_Prepare(this);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -m_fSpeed), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));

  // play the flying sound
  m_soEffect.Set3DParameters(250.0f, 10.0f, 2.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_FLYING, SOF_3D|SOF_LOOP);
  m_fFlyTime = 30.0f;
  m_iDamageAmount = 100;
  m_iRangeDamageAmount = 100;
  m_fDamageHotSpotRange = 15.0f;
  m_fDamageFallOffRange = 30.0f;
  m_fSoundRange = 100.0f;
  m_bExplode = TRUE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = TRUE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = GetSoundLength(SOUND_METEOR_BLAST)+0.25f;
  m_tmExpandBox = 0.1f;
  m_tmInvisibility = 0.05f;
  SetHealthInit(100 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_FLYING;
}

void MeteorExplosion()
{
  //LavamanBombExplosion();
  //PlayerRocketExplosion();
  // spawn particle debris
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 4.0f;
  eSpawnSpray.fSizeMultiplier = 0.5f;
  eSpawnSpray.sptType = SPT_LAVA_STONES;
  eSpawnSpray.vDirection = en_vCurrentTranslationAbsolute/32.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);

  ESpawnEffect ese;
  FLOAT3D vPoint;
  FLOATplane3D vPlaneNormal;
  FLOAT fDistanceToEdge;

  // explosion
  ese.colMuliplier = C_WHITE|CT_OPAQUE;
  ese.betType = BET_CANNON;
  ese.vStretch = FLOAT3D(5,5,5);
  SpawnEffect(GetPlacement(), ese);

  // spawn sound event in range
  if (m_penLauncher != NULL && m_penLauncher->IsPlayerControlled()) {
    SpawnRangeSound(m_penLauncher, this, SNDT_PLAYER, m_fSoundRange);
  }

  // explosion debris
  ese.betType = BET_EXPLOSION_DEBRIS;
  SpawnEffect(GetPlacement(), ese);

  // explosion smoke
  ese.betType = BET_EXPLOSION_SMOKE;
  SpawnEffect(GetPlacement(), ese);

  // on plane
  if (GetNearestPolygon(vPoint, vPlaneNormal, fDistanceToEdge)) {
    if ((vPoint-GetPlacement().pl_PositionVector).Length() < 3.5f) {
      // stain
      ese.betType = BET_EXPLOSIONSTAIN;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);

      // shock wave
      ese.betType = BET_SHOCKWAVE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint, ANGLE3D(0, 0, 0)), ese);

      // second explosion on plane
      ese.betType = BET_ROCKET_PLANE;
      ese.vNormal = FLOAT3D(vPlaneNormal);
      SpawnEffect(CPlacement3D(vPoint+ese.vNormal/50.0f, ANGLE3D(0, 0, 0)), ese);
    }
  }

  m_soExplosion.Set3DParameters(150.0f, 10.0f, 1.5f, 1.0f);
  PlaySound(m_soExplosion, SOUND_METEOR_BLAST, SOF_3D);
}


/************************************************************
 *                    S H O O T E R S                       *
 ************************************************************/

void ShooterWoodenDart(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));

  /*// play the flying sound
  m_soEffect.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_FLYING, SOF_3D|SOF_LOOP);*/
  m_fFlyTime = 10.0f;
  if (GetSP()->sp_gdGameDifficulty<=CSessionProperties::GD_EASY) {
    m_iDamageAmount = 5;
  } else {
    m_iDamageAmount = 10;
  }

  m_bExplode = FALSE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 1.125f;
  m_tmExpandBox = 0.1f;
  m_tmInvisibility = 0.05f;
  SetHealthInit(5 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_FLYING;
};

void ShooterWoodenDartExplosion()
{
  // particles
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 0.5f;
  eSpawnSpray.fSizeMultiplier = 0.1f;
  eSpawnSpray.sptType = SPT_WOOD;
  eSpawnSpray.vDirection = -en_vCurrentTranslationAbsolute/32.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);
};

void ShooterFireball(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetAnyModel(idModelConfig);
  GetModelObject()->StretchModel(FLOAT3D(0.25f, 0.25f, 0.25f));
  ModelChangeNotify();

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -30.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));

  /*// play the flying sound
  m_soEffect.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
  PlaySound(m_soEffect, SOUND_FLYING, SOF_3D|SOF_LOOP);*/
  m_fFlyTime = 10.0f;
  if (GetSP()->sp_gdGameDifficulty<=CSessionProperties::GD_EASY) {
    m_iDamageAmount = 8; // 7.5f;
  } else {
    m_iDamageAmount = 15;
  }

  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = TRUE;
  m_fWaitAfterDeath = 0.125f;
  m_tmExpandBox = 0.1f;
  m_tmInvisibility = 0.05f;
  SetHealthInit(5 * HEALTH_VALUE_MULTIPLIER);
  m_pmtMove = PMT_FLYING;
};

void ShooterFireballExplosion()
{
  // particles
  CPlacement3D plSpray = GetPlacement();
  CEntityPointer penSpray = CreateEntity(plSpray, CLASS_BLOOD_SPRAY);
  penSpray->SetParent(this);
  ESpawnSpray eSpawnSpray;
  eSpawnSpray.colBurnColor=C_WHITE|CT_OPAQUE;
  eSpawnSpray.fDamagePower = 1.0f;
  eSpawnSpray.fSizeMultiplier = 0.5f;
  eSpawnSpray.sptType = SPT_LAVA_STONES;
  eSpawnSpray.vDirection = -en_vCurrentTranslationAbsolute/32.0f;
  eSpawnSpray.penOwner = this;
  penSpray->Initialize(eSpawnSpray);
};

void ShooterFlame(void)
{
  const INDEX idModelConfig = ModelForProjectile(m_prtType);

  // set appearance
  InitAsModel();
  SetPhysicsFlags(EPF_PROJECTILE_FLYING);
  SetCollisionFlags(ECF_PROJECTILE_SOLID);
  SetFlags(GetFlags() | ENF_SEETHROUGH);
  SetAnyModel(idModelConfig);

  // start moving
  LaunchAsPropelledProjectile(FLOAT3D(0.0f, 0.0f, -10.0f), (CMovableEntity*)(CEntity*)m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, 0, 0));
  m_fFlyTime = 1.0f;
  m_iDamageAmount = 3;
  m_fSoundRange = 0.0f;
  m_bExplode = FALSE;
  m_bLightSource = TRUE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 0.3f;
  m_pmtMove = PMT_FLYING;
};

void AfterburnerDebris(void)
{
  Particles_AfterBurner_Prepare(this);

  // set appearance
  InitAsEditorModel();
  SetPhysicsFlags(EPF_MODEL_FALL);
  SetCollisionFlags(ECF_PROJECTILE_MAGIC);
  SetModel(MODEL_MARKER);
  SetModelMainTexture(TEXTURE_MARKER);

  // start moving
  LaunchAsFreeProjectile(FLOAT3D(0.0f, 0.0f, -m_fSpeed), (CMovableEntity*)&*m_penLauncher);
  SetDesiredRotation(ANGLE3D(0, FRnd()*360.0f-180.0f, FRnd()*360.0f-180.0f));
  m_fFlyTime = 10.0f;
  m_iDamageAmount = 0;
  m_iRangeDamageAmount = 0;
  m_fDamageHotSpotRange = 0.0f;
  m_fDamageFallOffRange = 0.0f;
  m_fSoundRange = 0.0f;
  m_bExplode = TRUE;
  m_bLightSource = FALSE;
  m_bCanHitHimself = FALSE;
  m_bCanBeDestroyed = FALSE;
  m_fWaitAfterDeath = 2.0f;
  m_pmtMove = PMT_FLYING;
}

/************************************************************
 *             C O M M O N   F U N C T I O N S              *
 ************************************************************/
// projectile touch his valid target
void ProjectileTouch(CEntityPointer penHit)
{
  // explode if needed
  ProjectileHit();

  // direct damage
  FLOAT3D vDirection;
  FLOAT fTransLen = en_vIntendedTranslation.Length();

  if (fTransLen>0.5f) {
    vDirection = en_vIntendedTranslation/fTransLen;
  } else {
    vDirection = -en_vGravityDir;
  }

  // spawn flame
  const FLOAT fDamageMul = GetSeriousDamageMultiplier(m_penLauncher);
  INDEX iDamageAmount = GetTouchDamage() * fDamageMul;

  if ((m_prtType==PRT_FLAME||m_prtType==PRT_SHOOTER_FLAME) && m_fWaitAfterDeath>0.0f) {
    // don't burn the same entity twice while passing through it
    if (m_penLastDamaged==penHit) {
      return;
    } else {
      m_penLastDamaged=penHit;
    }

    // don't spawn flame on AirElemental
    BOOL bSpawnFlame=TRUE;
    BOOL bInflictDamage=TRUE;
    if (IsOfClass(penHit, "AirElemental"))
    {
      bSpawnFlame=FALSE;
    }

    EntityInfo *pei=(EntityInfo *)penHit->GetEntityInfo();
    if (pei!=NULL && pei->Eeibt==EIBT_ICE)
    {
      bSpawnFlame=FALSE;
      bInflictDamage=FALSE;
    }

    if (bSpawnFlame) {
      SpawnFlame(m_penLauncher, penHit, GetPlacement().pl_PositionVector);
    }

    if (bInflictDamage) {
      FLOAT3D vHitPoint = GetPlacement().pl_PositionVector;
      InflictDirectDamage(penHit, m_penLauncher, DMT_BURNING, iDamageAmount, vHitPoint, vDirection);
    }

  // don't damage the same entity twice (wind blast)
  } else if (m_prtType==PRT_AIRELEMENTAL_WIND) {
    if (penHit==m_penLastDamaged) {
      return;
    } else  {
      m_penLastDamaged=penHit;
    }

    FLOAT3D vHitPoint = GetPlacement().pl_PositionVector;
    InflictDirectDamage(penHit, m_penLauncher, DMT_PROJECTILE, iDamageAmount, vHitPoint, vDirection);

  // other projectiles
  } else {
    FLOAT3D vHitPoint = GetPlacement().pl_PositionVector;
    InflictDirectDamage(penHit, m_penLauncher, DMT_PROJECTILE, iDamageAmount, vHitPoint, vDirection);
  }
};

// projectile hit (or time expired or can't move any more)
void ProjectileHit(void)
{
  // explode ...
  if (m_bExplode) {
    const FLOAT fDamageMul = GetSeriousDamageMultiplier(m_penLauncher);
    FLOAT3D vHitPoint = GetPlacement().pl_PositionVector;
    INDEX iDamageAmount = GetSplashDamage() * fDamageMul;
    InflictRangeDamage(m_penLauncher, DMT_EXPLOSION, iDamageAmount, vHitPoint, m_fDamageHotSpotRange, m_fDamageFallOffRange);
  }

  // sound event
  if (m_fSoundRange>0.0f && m_penLauncher != NULL && m_penLauncher->IsPlayerControlled())
  {
    ESound eSound;
    eSound.EsndtSound = SNDT_EXPLOSION;
    eSound.penTarget = m_penLauncher;
    SendEventInRange(eSound, FLOATaabbox3D(GetPlacement().pl_PositionVector, m_fSoundRange));
  }
};

// spawn effect
void SpawnEffect(const CPlacement3D &plEffect, const ESpawnEffect &eSpawnEffect)
{
  CEntityPointer penEffect = CreateEntity(plEffect, CLASS_BASIC_EFFECT);
  penEffect->Initialize(eSpawnEffect);
};

/************************************************************
 *                      S O U N D S                         *
 ************************************************************/
void BounceSound(void)
{
  switch (m_prtType)
  {
    case PRT_GRENADE:
      if (en_vCurrentTranslationAbsolute.Length() > 3.0f) {
        m_soEffect.Set3DParameters(20.0f, 2.0f, 1.0f, 1.0f);
        PlaySound(m_soEffect, SOUND_GRENADE_BOUNCE, SOF_3D);
      }

      break;
  }
};

// Calculate current rotation speed to rich given orientation in future
ANGLE GetRotationSpeed(ANGLE aWantedAngle, ANGLE aRotateSpeed, FLOAT fWaitFrequency)
{
  ANGLE aResult;

  // if desired position is smaller
  if (aWantedAngle<-aRotateSpeed*fWaitFrequency) {
    aResult = -aRotateSpeed; // start decreasing

  // if desired position is bigger
  } else if (aWantedAngle>aRotateSpeed*fWaitFrequency) {
    aResult = +aRotateSpeed; // start increasing

  // if desired position is more-less ahead
  } else {
    aResult = aWantedAngle/fWaitFrequency;
  }

  return aResult;
}


/* Receive damage */
void ReceiveDamage(CEntity *penInflictor, INDEX iDamageType,
                   INDEX iDamageAmount, const FLOAT3D &vHitPoint, const FLOAT3D &vDirection)
{
  // cannonball immediately destroys demons fireball
  if (m_prtType==PRT_DEMON_FIREBALL && iDamageType==DMT_CANNONBALL) {
    iDamageAmount *= 10001.0f;
  }

  if (m_prtType==PRT_FLAME && IsOfClass(penInflictor, "Moving Brush")) {
    Destroy();
  }

  CMovableModelEntity::ReceiveDamage(penInflictor, iDamageType, iDamageAmount, vHitPoint, vDirection);
}

/************************************************************
 *                   P R O C E D U R E S                    *
 ************************************************************/
procedures:

  // --->>> PROJECTILE FLY IN SPACE
  ProjectileFly(EVoid)
  {
    // if already inside some entity
    CEntity *penObstacle;

    if (CheckForCollisionNow(0, &penObstacle)) {
      // explode now
      ProjectileTouch(penObstacle);
      // if flame, continue existing
      /*if (m_prtType==PRT_FLAME && ((CEntity &)*&penObstacle).en_RenderType==RT_MODEL) {
        resume;
      }*/
      return EEnd();
    }

    // fly loop
    wait(m_fFlyTime)
    {
      on (EBegin) : { resume; }

      on (EPass epass) : {
        BOOL bHit;
        // ignore launcher within 1 second
        bHit = epass.penOther!=m_penLauncher || _pTimer->CurrentTick()>m_fIgnoreTime;
        // ignore another projectile of same type
        bHit &= !((!m_bCanHitHimself && IsOfClass(epass.penOther, &CProjectileEntity_DLLClass) &&
                ((CProjectileEntity*)&*epass.penOther)->m_prtType==m_prtType));
        // ignore twister
        bHit &= !IsOfClass(epass.penOther, "Twister");

        if (bHit) {
          ProjectileTouch(epass.penOther);
          // player flame passes through enemies
          //if (m_prtType==PRT_FLAME && IsDerivedFromClass((CEntity *)&*(epass.penOther), &CEnemyBaseEntity_DLLClass)) { resume; }
          stop;
        }

        resume;
      }

      on (ETouch etouch) : {
        // clear time limit for launcher
        m_fIgnoreTime = 0.0f;
        // ignore another projectile of same type
        BOOL bHit;
        bHit = !((!m_bCanHitHimself && IsOfClass(etouch.penOther, &CProjectileEntity_DLLClass) &&
                 ((CProjectileEntity*)&*etouch.penOther)->m_prtType==m_prtType));

        if (bHit) {
          ProjectileTouch(etouch.penOther);
          stop;
        }

        resume;
      }

      on (EDeath) : {
        if (m_bCanBeDestroyed) {
          ProjectileHit();
          stop;
        }
        resume;
      }

      on (ETimer) : {
        ProjectileHit();
        stop;
      }
    }

    return EEnd();
  };

  // --->>> GUIDED PROJECTILE FLY IN SPACE
  ProjectileGuidedFly(EVoid)
  {
    // if already inside some entity
    CEntity *penObstacle;

    if (CheckForCollisionNow(0, &penObstacle)) {
      // explode now
      ProjectileTouch(penObstacle);
      return EEnd();
    }

    // fly loop
    while (_pTimer->CurrentTick()<(m_fStartTime+m_fFlyTime))
    {
      FLOAT fWaitFrequency = 0.1f;
      // beast big projectile destroys soon after passing near the player
      /*if (m_prtType==PRT_BEAST_BIG_PROJECTILE &&
          DistanceTo(this, m_penTarget)<20.0f &&
          (m_fStartTime+m_fFlyTime-_pTimer->CurrentTick())>1.5f)
      {
        m_fFlyTime = _pTimer->CurrentTick() - m_fStartTime + 1.5f;
      }*/

      if (m_penTarget!=NULL) {
        // calculate desired position and angle
        EntityInfo *pei= (EntityInfo*) (m_penTarget->GetEntityInfo());
        FLOAT3D vDesiredPosition;
        GetEntityInfoPosition(m_penTarget, pei->vSourceCenter, vDesiredPosition);
        FLOAT3D vDesiredDirection = (vDesiredPosition-GetPlacement().pl_PositionVector).Normalize();

        // for heading
        ANGLE aWantedHeading = GetRelativeHeading(vDesiredDirection);
        /*if (m_prtType==PRT_BEAST_BIG_PROJECTILE && m_fStartTime+m_fFlyTime-_pTimer->CurrentTick()<1.5f)
        {
          m_aRotateSpeed = 10.0f;
        }*/
        ANGLE aHeading = GetRotationSpeed(aWantedHeading, m_aRotateSpeed, fWaitFrequency);

        // factor used to decrease speed of projectiles oriented opposite of its target
        FLOAT fSpeedDecreasingFactor = ((180-Abs(aWantedHeading))/180.0f);

        // factor used to increase speed when far away from target
        FLOAT fSpeedIncreasingFactor = (vDesiredPosition-GetPlacement().pl_PositionVector).Length()/100;
        fSpeedIncreasingFactor = ClampDn(fSpeedIncreasingFactor, 1.0f);

        // decrease speed acodring to target's direction
        FLOAT fMaxSpeed = m_fGuidedMaxSpeedFactor*fSpeedIncreasingFactor;
        FLOAT fMinSpeedRatio = 0.5f;
        FLOAT fWantedSpeed = fMaxSpeed*(fMinSpeedRatio+(1-fMinSpeedRatio)*fSpeedDecreasingFactor);

        // adjust translation velocity
        SetDesiredTranslation(FLOAT3D(0, 0, -fWantedSpeed));

        // adjust rotation speed
        m_aRotateSpeed = 75.0f*(1+0.5f*fSpeedDecreasingFactor);

        // calculate distance factor
        FLOAT fDistanceFactor = (vDesiredPosition-GetPlacement().pl_PositionVector).Length()/50.0;
        fDistanceFactor = ClampUp(fDistanceFactor, 4.0f);
        FLOAT fRNDHeading = (FRnd()-0.5f)*180*fDistanceFactor;
        FLOAT fRNDPitch = (FRnd()-0.5f)*90*fDistanceFactor;

        // if we are looking near direction of target
        if (Abs(aWantedHeading) < 30.0f)
        {
          // calculate pitch speed
          ANGLE aWantedPitch = GetRelativePitch(vDesiredDirection);
          ANGLE aPitch = GetRotationSpeed(aWantedPitch, m_aRotateSpeed*1.5f, fWaitFrequency);
          // adjust heading and pich
          SetDesiredRotation(ANGLE3D(aHeading+fRNDHeading,aPitch+fRNDPitch,0));

        // just adjust heading
        } else {
          SetDesiredRotation(ANGLE3D(aHeading,fDistanceFactor*40,0));
        }
      }

      wait(fWaitFrequency)
      {
        on (EBegin) : { resume; }

        on (EPass epass) : {
          BOOL bHit;
          // ignore launcher within 1 second
          bHit = epass.penOther!=m_penLauncher || _pTimer->CurrentTick()>m_fIgnoreTime;
          // ignore another projectile of same type
          bHit &= !((!m_bCanHitHimself && IsOfClass(epass.penOther, &CProjectileEntity_DLLClass) &&
                  ((CProjectileEntity*)&*epass.penOther)->m_prtType==m_prtType));
          // ignore twister
          bHit &= !IsOfClass(epass.penOther, "Twister");
          if (bHit) {
            ProjectileTouch(epass.penOther);
            return EEnd();
          }
          resume;
        }

        on (EDeath) :
        {
          if (m_bCanBeDestroyed)
          {
            ProjectileHit();
            return EEnd();
          }
          resume;
        }

        on (ETimer) :
        {
          stop;
        }
      }
    }

    return EEnd();
  };

  ProjectileGuidedFastFly(EVoid)
  {
    // if already inside some entity
    CEntity *penObstacle;

    if (CheckForCollisionNow(0, &penObstacle)) {
      // explode now
      ProjectileTouch(penObstacle);
      return EEnd();
    }

    // fly loop
    while (_pTimer->CurrentTick()<(m_fStartTime+m_fFlyTime))
    {
      FLOAT fWaitFrequency = 0.1f;

      // beast big projectile destroys soon after passing near the player
      if (m_prtType==PRT_BEAST_BIG_PROJECTILE &&
          DistanceTo(this, m_penTarget)<20.0f &&
          (m_fStartTime+m_fFlyTime-_pTimer->CurrentTick())>1.5f)
      {
        m_fFlyTime = _pTimer->CurrentTick() - m_fStartTime + 1.5f;
      }

      if (m_penTarget!=NULL) {
        // calculate desired position and angle
        EntityInfo *pei= (EntityInfo*) (m_penTarget->GetEntityInfo());
        FLOAT3D vDesiredPosition;
        GetEntityInfoPosition(m_penTarget, pei->vSourceCenter, vDesiredPosition);
        FLOAT3D vDesiredDirection = (vDesiredPosition-GetPlacement().pl_PositionVector).Normalize();

        // for heading
        ANGLE aWantedHeading = GetRelativeHeading(vDesiredDirection);
        ANGLE aHeading = GetRotationSpeed(aWantedHeading, 5.0f/*m_aRotateSpeed*/, fWaitFrequency);

        // factor used to decrease speed of projectiles oriented opposite of its target
        FLOAT fSpeedDecreasingFactor = ((180-Abs(aWantedHeading))/180.0f);

        // factor used to increase speed when far away from target
        FLOAT fSpeedIncreasingFactor = (vDesiredPosition-GetPlacement().pl_PositionVector).Length()/100;
        fSpeedIncreasingFactor = ClampDn(fSpeedIncreasingFactor, 1.0f);

        // decrease speed acording to target's direction
        FLOAT fMaxSpeed = m_fGuidedMaxSpeedFactor*fSpeedIncreasingFactor;
        FLOAT fMinSpeedRatio = 10.0f;
        FLOAT fWantedSpeed = fMaxSpeed*(fMinSpeedRatio+(1-fMinSpeedRatio)*fSpeedDecreasingFactor);

        // adjust translation velocity
        SetDesiredTranslation(FLOAT3D(0, 0, -fWantedSpeed));

        // adjust rotation speed
        m_aRotateSpeed = 110.0f*(1+0.5f*fSpeedDecreasingFactor);

        // calculate distance factor
        FLOAT fDistanceFactor = (vDesiredPosition-GetPlacement().pl_PositionVector).Length()/50.0;
        fDistanceFactor = ClampUp(fDistanceFactor, 4.0f);

        // if we are looking near direction of target
        if (Abs(aWantedHeading) < 30.0f) {
          bLockedOn = TRUE;

          // calculate pitch speed
          ANGLE aWantedPitch = GetRelativePitch(vDesiredDirection);
          ANGLE aPitch = GetRotationSpeed(aWantedPitch, m_aRotateSpeed*1.5f, fWaitFrequency);

          // adjust heading and pitch
          SetDesiredRotation(ANGLE3D(aHeading, aPitch, 0));

        // just adjust heading
        } else {
          if (bLockedOn) // we just missed the player
          {
            ANGLE3D aBankingUp;
            aBankingUp = GetPlacement().pl_OrientationAngle;
            aBankingUp(3) = 0.0f;
            SetPlacement(CPlacement3D(GetPlacement().pl_PositionVector, aBankingUp));
          }

          bLockedOn = FALSE;
          //SetDesiredRotation(ANGLE3D(aHeading,fDistanceFactor*40,0));
          SetDesiredRotation(ANGLE3D(aHeading,400,0));
        }
      }

      wait(fWaitFrequency)
      {
        on (EBegin) : { resume; }

        on (ETouch etouch) : {
          // clear time limit for launcher
          m_fIgnoreTime = 0.0f;

          // ignore itself and the demon
          BOOL bHit;
          bHit = !((!m_bCanHitHimself && IsOfClass(etouch.penOther, &CProjectileEntity_DLLClass) &&
            ((CProjectileEntity*)&*etouch.penOther)->m_prtType==m_prtType));
          bHit &= !IsOfClass(etouch.penOther, "Demon");
          FLOAT3D vTrans = en_vCurrentTranslationAbsolute;
          bHit &= Abs(vTrans.Normalize() % FLOAT3D(etouch.plCollision)) > 0.35;

          if (bHit) {
            ProjectileTouch(etouch.penOther);
            return EEnd();
          }

          resume;
        }

        on (EPass epass) : {
          BOOL bHit;
          // ignore launcher within 1 second
          bHit = epass.penOther!=m_penLauncher || _pTimer->CurrentTick()>m_fIgnoreTime;
          // ignore another projectile of same type
          bHit &= !((!m_bCanHitHimself && IsOfClass(epass.penOther, &CProjectileEntity_DLLClass) &&
                  ((CProjectileEntity*)&*epass.penOther)->m_prtType==m_prtType));
          // ignore twister
          bHit &= !IsOfClass(epass.penOther, "Twister");
          // if demons projectile, ignore all other projectiles
          bHit &= !(m_prtType==PRT_DEMON_FIREBALL && IsOfClass(epass.penOther, &CProjectileEntity_DLLClass));
          bHit &= !(m_prtType==PRT_BEAST_BIG_PROJECTILE && IsOfClass(epass.penOther, &CProjectileEntity_DLLClass));

          if (bHit) {
            ProjectileTouch(epass.penOther);
            return EEnd();
          }

          resume;
        }

        on (EDeath) :
        {
          if (m_bCanBeDestroyed)
          {
            ProjectileHit();
            return EEnd();
          }
          resume;
        }

        on (ETimer) :
        {
          stop;
        }
      }
    }

    return EEnd();
  };


  ProjectileGuidedSlide(EVoid)
  {
    // if already inside some entity
    CEntity *penObstacle;
    if (CheckForCollisionNow(0, &penObstacle)) {
      // explode now
      ProjectileTouch(penObstacle);
      return EEnd();
    }

    // fly loop
    while (_pTimer->CurrentTick()<(m_fStartTime+m_fFlyTime))
    {
      FLOAT fWaitFrequency = 0.1f;

      if (m_penTarget!=NULL) {
        // calculate desired position and angle
        EntityInfo *pei= (EntityInfo*) (m_penTarget->GetEntityInfo());
        FLOAT3D vDesiredPosition;
        GetEntityInfoPosition(m_penTarget, pei->vSourceCenter, vDesiredPosition);
        FLOAT3D vDesiredDirection = (vDesiredPosition-GetPlacement().pl_PositionVector).Normalize();

        // for heading
        ANGLE aWantedHeading = GetRelativeHeading(vDesiredDirection);
        ANGLE aHeading = GetRotationSpeed(aWantedHeading, m_aRotateSpeed, fWaitFrequency);

        // factor used to decrease speed of projectiles oriented opposite of its target
        FLOAT fSpeedDecreasingFactor = ((180-Abs(aWantedHeading))/180.0f);

        // factor used to increase speed when far away from target
        FLOAT fSpeedIncreasingFactor = (vDesiredPosition-GetPlacement().pl_PositionVector).Length()/100;
        fSpeedIncreasingFactor = ClampDn(fSpeedIncreasingFactor, 1.0f);

        // decrease speed acodring to target's direction
        FLOAT fMaxSpeed = 30.0f*fSpeedIncreasingFactor;
        FLOAT fMinSpeedRatio = 0.5f;
        FLOAT fWantedSpeed = fMaxSpeed*(fMinSpeedRatio+(1-fMinSpeedRatio)*fSpeedDecreasingFactor);

        // adjust translation velocity
        SetDesiredTranslation(FLOAT3D(0, 0, -fWantedSpeed));

        // adjust rotation speed
        m_aRotateSpeed = 75.0f*(1+0.5f*fSpeedDecreasingFactor);

        // calculate distance factor
        FLOAT fDistanceFactor = (vDesiredPosition-GetPlacement().pl_PositionVector).Length()/50.0;
        fDistanceFactor = ClampUp(fDistanceFactor, 4.0f);
        FLOAT fRNDHeading = (FRnd()-0.5f)*180*fDistanceFactor;

        // if we are looking near direction of target
        if (Abs(aWantedHeading) < 30.0f) {
          // adjust heading and pich
          SetDesiredRotation(ANGLE3D(aHeading+fRNDHeading,0,0));
        // just adjust heading
        } else {
          SetDesiredRotation(ANGLE3D(aHeading,0,0));
        }
      }

      wait(fWaitFrequency)
      {
        on (EBegin) : { resume; }

        on (EPass epass) : {
          BOOL bHit;
          // ignore launcher within 1 second
          bHit = epass.penOther!=m_penLauncher || _pTimer->CurrentTick()>m_fIgnoreTime;

          // ignore another projectile of same type
          bHit &= !((!m_bCanHitHimself && IsOfClass(epass.penOther, &CProjectileEntity_DLLClass) &&
                  ((CProjectileEntity*)&*epass.penOther)->m_prtType==m_prtType));

          // ignore twister
          bHit &= !IsOfClass(epass.penOther, "Twister");
          if (bHit) {
            ProjectileTouch(epass.penOther);
            return EEnd();
          }
          resume;
        }

        on (EDeath) :
        {
          if (m_bCanBeDestroyed)
          {
            ProjectileHit();
            return EEnd();
          }

          resume;
        }

        on (ETimer) :
        {
          stop;
        }
      }
    }
    return EEnd();
  };

  // --->>> PROJECTILE SLIDE ON BRUSH
  ProjectileSlide(EVoid)
  {
    // if already inside some entity
    CEntity *penObstacle;

    if (CheckForCollisionNow(0, &penObstacle)) {
      // explode now
      ProjectileTouch(penObstacle);
      return EEnd();
    }

    // fly loop
    wait(m_fFlyTime)
    {
      on (EBegin) : { resume; }

      on (EPass epass) : {
        BOOL bHit;

        // ignore launcher within 1 second
        bHit = epass.penOther!=m_penLauncher || _pTimer->CurrentTick()>m_fIgnoreTime;

        // ignore another projectile of same type
        bHit &= !((!m_bCanHitHimself && IsOfClass(epass.penOther, &CProjectileEntity_DLLClass) &&
                ((CProjectileEntity*)&*epass.penOther)->m_prtType==m_prtType));

        // ignore twister
        bHit &= !IsOfClass(epass.penOther, "Twister");
        if (epass.penOther!=m_penLauncher) {
          bHit = bHit ;
        }

        if (bHit) {
          ProjectileTouch(epass.penOther);

          // player flame passes through enemies
          if (m_prtType==PRT_FLAME && IsDerivedFromClass((CEntity *)&*(epass.penOther), &CEnemyBaseEntity_DLLClass)) {
            resume;
          }

          // wind blast passes through movable entities
          if (m_prtType==PRT_AIRELEMENTAL_WIND && IsDerivedFromClass((CEntity *)&*(epass.penOther), &CMovableEntity_DLLClass)) {
            resume;
          }

          stop;
        }

        resume;
      }

      on (ETouch etouch) : {
        // clear time limit for launcher
        m_fIgnoreTime = 0.0f;

        // ignore brushes
        BOOL bHit;
        bHit = !(etouch.penOther->GetRenderType() & RT_BRUSH);
        if (m_prtType==PRT_FLAME && !bHit && !m_bLeftFlame)
        {
          SpawnFlame(m_penLauncher, etouch.penOther, GetPlacement().pl_PositionVector);
          m_bLeftFlame=TRUE;
        }
        if (!bHit) { BounceSound(); }

        // ignore another projectile of same type
        bHit &= !((!m_bCanHitHimself && IsOfClass(etouch.penOther, &CProjectileEntity_DLLClass) &&
                  ((CProjectileEntity*)&*etouch.penOther)->m_prtType==m_prtType));
        if (bHit) {
          ProjectileTouch(etouch.penOther);
          stop;
        }

        // projectile is moving to slow (stuck somewhere) -> kill it
        if (en_vCurrentTranslationAbsolute.Length() < 0.25f*en_vDesiredTranslationRelative.Length()) {
          ProjectileHit();
          stop;
        }

        resume;
      }

      on (EDeath) : {
        if (m_bCanBeDestroyed) {
          ProjectileHit();
          stop;
        }

        resume;
      }

      on (ETimer) : {
        ProjectileHit();
        stop;
      }
    }
    return EEnd();
  };

  // --->>> PROJECTILE FLY IN SPACE WITH REBOUNDING
  ProjectileFlyRebounding(EVoid)
  {
    // if already inside some entity
    CEntity *penObstacle;

    if (CheckForCollisionNow(0, &penObstacle)) {
      // explode now
      ProjectileTouch(penObstacle);
      return EEnd();
    }

    // fly loop
    wait(m_fFlyTime)
    {
      on (EBegin) : { resume; }

      on (EPass epass) : {
        BOOL bHit;

        // ignore launcher within 1 second
        bHit = epass.penOther!=m_penLauncher || _pTimer->CurrentTick()>m_fIgnoreTime;

        // ignore another projectile of same type
        bHit &= !((!m_bCanHitHimself && IsOfClass(epass.penOther, &CProjectileEntity_DLLClass) &&
                ((CProjectileEntity*)&*epass.penOther)->m_prtType==m_prtType));

        // ignore twister
        bHit &= !IsOfClass(epass.penOther, "Twister");
        if (bHit) {
          ProjectileTouch(epass.penOther);
          stop;
        }
        resume;
      }

      on (ETouch etouch) : {
        // clear time limit for launcher
        m_fIgnoreTime = 0.0f;

        BOOL bHit;

        // if brush hit
        bHit = (etouch.penOther->GetRenderType() == RT_BRUSH);

        if (bHit && m_iRebounds>0) {
          //reverse direction
          ReflectDirectionVectorByPlane(etouch.plCollision, en_vCurrentTranslationAbsolute);
          ReflectRotationMatrixByPlane_cols(etouch.plCollision, en_mRotation);
          m_iRebounds--;
        } else {
          // ignore another projectile of same type
          bHit = !((!m_bCanHitHimself && IsOfClass(etouch.penOther, &CProjectileEntity_DLLClass) &&
                   ((CProjectileEntity*)&*etouch.penOther)->m_prtType==m_prtType));

          if (bHit) {
            ProjectileTouch(etouch.penOther);
            stop;
          }
        }

        resume;
      }

      on (EDeath) : {
        if (m_bCanBeDestroyed) {
          ProjectileHit();
          stop;
        }
        resume;
      }

      on (ETimer) : {
        ProjectileHit();
        stop;
      }
    }

    return EEnd();
  };

  // --->>> MAIN
  Main(ELaunchProjectile eLaunch)
  {
    // remember the initial parameters
    ASSERT(eLaunch.penLauncher!=NULL);
    m_penLauncher = eLaunch.penLauncher;
    m_prtType = eLaunch.prtType;
    m_fSpeed = eLaunch.fSpeed;
    m_fStretch=eLaunch.fStretch;
    SetPredictable(TRUE);

    // remember lauching time
    m_fIgnoreTime = _pTimer->CurrentTick() + 1.0f;
    m_penLastDamaged = NULL;

    switch (m_prtType)
    {
      case PRT_DEVIL_ROCKET:
      case PRT_WALKER_ROCKET:
      case PRT_ROCKET:
      case PRT_SHOOTER_WOODEN_DART:
        {
          Particles_RocketTrail_Prepare(this);
          break;
        }

      case PRT_GUFFY_PROJECTILE: break; //Particles_RocketTrail_Prepare(this); break;
      case PRT_GRENADE: Particles_GrenadeTrail_Prepare(this); break;
      case PRT_CATMAN_FIRE: Particles_RocketTrail_Prepare(this); break;
      case PRT_HEADMAN_FIRECRACKER: Particles_FirecrackerTrail_Prepare(this); break;
      case PRT_HEADMAN_ROCKETMAN: Particles_Fireball01Trail_Prepare(this); break;
      case PRT_HEADMAN_BOMBERMAN: Particles_BombTrail_Prepare(this); break;
      case PRT_LAVA_COMET: Particles_LavaTrail_Prepare(this); break;
      case PRT_LAVAMAN_BIG_BOMB: Particles_LavaBombTrail_Prepare(this); break;
      case PRT_LAVAMAN_BOMB: Particles_LavaBombTrail_Prepare(this); break;
      case PRT_BEAST_PROJECTILE: Particles_Fireball01Trail_Prepare(this); break;
      case PRT_BEAST_BIG_PROJECTILE:
      case PRT_DEVIL_GUIDED_PROJECTILE:
      case PRT_DEMON_FIREBALL:
      //case PRT_METEOR:
         Particles_FirecrackerTrail_Prepare(this);
         break;

      case PRT_SHOOTER_FIREBALL: Particles_Fireball01Trail_Prepare(this); break;
    }

    // projectile initialization
    switch (m_prtType)
    {
      case PRT_WALKER_ROCKET: WalkerRocket(); break;
      case PRT_ROCKET: PlayerRocket(); break;
      case PRT_GRENADE: PlayerGrenade(); break;
      case PRT_FLAME: PlayerFlame(); break;
      case PRT_LASER_RAY: PlayerLaserRay(); break;
      case PRT_CATMAN_FIRE: CatmanProjectile(); break;
      case PRT_HEADMAN_FIRECRACKER: HeadmanFirecracker(); break;
      case PRT_HEADMAN_ROCKETMAN: HeadmanRocketman(); break;
      case PRT_HEADMAN_BOMBERMAN: HeadmanBomberman(); break;
      case PRT_BONEMAN_FIRE: BonemanProjectile(); break;
      case PRT_WOMAN_FIRE: WomanProjectile(); break;
      case PRT_DRAGONMAN_FIRE: DragonmanProjectile(DRAGONMAN_NORMAL); break;
      case PRT_DRAGONMAN_STRONG_FIRE: DragonmanProjectile(DRAGONMAN_STRONG); break;
      case PRT_STONEMAN_FIRE: ElementalRock(ELEMENTAL_NORMAL, ELEMENTAL_STONEMAN); break;
      case PRT_STONEMAN_BIG_FIRE: ElementalRock(ELEMENTAL_BIG, ELEMENTAL_STONEMAN); break;
      case PRT_STONEMAN_LARGE_FIRE: ElementalRock(ELEMENTAL_LARGE, ELEMENTAL_STONEMAN); break;
      case PRT_LAVAMAN_BIG_BOMB: LavaManBomb(); break;
      case PRT_LAVAMAN_BOMB: LavaManBomb(); break;
      case PRT_LAVAMAN_STONE: ElementalRock(ELEMENTAL_NORMAL, ELEMENTAL_LAVAMAN); break;
      case PRT_ICEMAN_FIRE: ElementalRock(ELEMENTAL_NORMAL, ELEMENTAL_ICEMAN); break;
      case PRT_ICEMAN_BIG_FIRE: ElementalRock(ELEMENTAL_BIG, ELEMENTAL_ICEMAN); break;
      case PRT_ICEMAN_LARGE_FIRE: ElementalRock(ELEMENTAL_LARGE, ELEMENTAL_ICEMAN); break;
      case PRT_HUANMAN_FIRE: HuanmanProjectile(); break;
      case PRT_FISHMAN_FIRE: FishmanProjectile(); break;
      case PRT_MANTAMAN_FIRE: MantamanProjectile(); break;
      case PRT_CYBORG_LASER: CyborgLaser(); break;
      case PRT_CYBORG_BOMB: CyborgBomb(); break;
      case PRT_LAVA_COMET: LavaBall(); break;
      case PRT_BEAST_PROJECTILE: BeastProjectile(); break;
      case PRT_BEAST_BIG_PROJECTILE: BeastBigProjectile(); break;
      case PRT_BEAST_DEBRIS: BeastDebris(); break;
      case PRT_BEAST_BIG_DEBRIS: BeastBigDebris(); break;
      case PRT_DEVIL_LASER: DevilLaser(); break;
      case PRT_DEVIL_ROCKET: DevilRocket(); break;
      case PRT_DEVIL_GUIDED_PROJECTILE: DevilGuidedProjectile(); break;
      case PRT_GRUNT_PROJECTILE_SOL: GruntSoldierLaser(); break;
      case PRT_GRUNT_PROJECTILE_COM: GruntCommanderLaser(); break;
      case PRT_GUFFY_PROJECTILE: GuffyProjectile(); break;
      case PRT_DEMON_FIREBALL: DemonFireball(); break;
      case PRT_LARVA_PLASMA: LarvaPlasma(); break;
      case PRT_LARVA_TAIL_PROJECTILE: LarvaTail(); break;
      case PRT_SHOOTER_WOODEN_DART: ShooterWoodenDart(); break;
      case PRT_SHOOTER_FIREBALL: ShooterFireball(); break;
      case PRT_SHOOTER_FLAME: ShooterFlame(); break;
      case PRT_AFTERBURNER_DEBRIS: AfterburnerDebris(); break;
      case PRT_AIRELEMENTAL_WIND: WindBlast(); break;
      case PRT_METEOR: Meteor(); break;
      default: ASSERTALWAYS("Unknown projectile type");
    }

    // setup light source
    if (m_bLightSource) { SetupLightSource(TRUE); }

    // fly
    m_fStartTime = _pTimer->CurrentTick();

    // if guided projectile
    if (m_pmtMove == PMT_GUIDED) {
      autocall ProjectileGuidedFly() EEnd;
    } else if (m_pmtMove==PMT_GUIDED_FAST) {
      autocall ProjectileGuidedFastFly() EEnd;
    } else if (m_pmtMove==PMT_FLYING) {
      autocall ProjectileFly() EEnd;
    } else if (m_pmtMove==PMT_SLIDING) {
      autocall ProjectileSlide() EEnd;
    } else if (m_pmtMove==PMT_FLYING_REBOUNDING) {
      autocall ProjectileFlyRebounding() EEnd;
    } else if (m_pmtMove==PMT_GUIDED_SLIDING) {
      autocall ProjectileGuidedSlide() EEnd;
    }

    // projectile explosion
    switch (m_prtType)
    {
      case PRT_WALKER_ROCKET: WalkerRocketExplosion(); break;
      case PRT_ROCKET: PlayerRocketExplosion(); break;
      case PRT_GRENADE: PlayerGrenadeExplosion(); break;
      case PRT_LASER_RAY: PlayerLaserWave(); break;
      case PRT_HEADMAN_BOMBERMAN: HeadmanBombermanExplosion(); break;
      case PRT_CYBORG_BOMB: CyborgBombExplosion(); break;
      case PRT_LAVA_COMET: LavamanBombDebrisExplosion(); break;
      case PRT_LAVAMAN_BIG_BOMB: LavamanBombExplosion(); break;
      case PRT_LAVAMAN_BOMB: LavamanBombDebrisExplosion(); break;
      case PRT_BEAST_BIG_PROJECTILE: BeastBigProjectileExplosion(); break;
      case PRT_BEAST_PROJECTILE: BeastProjectileExplosion(); break;
      case PRT_BEAST_DEBRIS: BeastDebrisExplosion(); break;
      case PRT_BEAST_BIG_DEBRIS: BeastBigDebrisExplosion(); break;
      case PRT_DEVIL_ROCKET: DevilRocketExplosion(); break;
      case PRT_DEVIL_GUIDED_PROJECTILE: DevilGuidedProjectileExplosion(); break;
      case PRT_GUFFY_PROJECTILE: GuffyProjectileExplosion(); break;
      case PRT_DEMON_FIREBALL: DemonFireballExplosion(); break;
      case PRT_LARVA_PLASMA: LarvaPlasmaExplosion(); break;
      case PRT_LARVA_TAIL_PROJECTILE: LarvaTailExplosion(); break;
      case PRT_SHOOTER_WOODEN_DART: ShooterWoodenDartExplosion(); break;
      case PRT_SHOOTER_FIREBALL: ShooterFireballExplosion(); break;
      case PRT_METEOR: MeteorExplosion(); break;
    }

    // wait after death
    if (m_fWaitAfterDeath>0.0f) {
      SwitchToEditorModel();
      ForceFullStop();
      SetCollisionFlags(ECF_IMMATERIAL);
      // kill light source
      if (m_bLightSource) { SetupLightSource(FALSE); }
      autowait(m_fWaitAfterDeath);
    }

    Destroy();

    return;
  }
};
