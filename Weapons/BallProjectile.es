/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

506
%{
  #include "StdH.h"

  #include "Models/Weapons/Cannon/Projectile/Cannonball.h"
  #include "Entities/Brushes/MovingBrush.h"
  #include "Entities/Brushes/DestroyableArchitecture.h"
%}

uses "Entities/Weapons/ProjectileBase";
uses "Entities/Effects/BasicEffects";
uses "Entities/Light/Light";
uses "Entities/Players/PlayerWeapon";
uses "Entities/Characters/EnemyBase";

enum BallProjectileType
{
  0 BPT_IRON    "",
  1 BPT_NUKE    "",
};

// input parameter for launching the projectile
event ELaunchBallProjectile
{
  CEntityPointer penLauncher,     // who launched it
  enum BallProjectileType bptType,    // type of cannon ball
  FLOAT fLaunchPower,             // how fast will projectile be launched
  FLOAT fSize,                    // the size of the ball
};

event EForceExplode {};

%{

  // projectile solid
  #define ECF_CANNON_BALL (\
    ((ECBI_MODEL|ECBI_BRUSH|ECBI_PROJECTILE_SOLID|ECBI_CORPSE|ECBI_MODEL_HOLDER|ECBI_MODEL_HOLDER)<<ECB_TEST) |\
    ((ECBI_PROJECTILE_SOLID)<<ECB_IS) |\
    ((ECBI_MODEL|ECBI_MODEL_HOLDER|ECBI_CORPSE)<<ECB_PASS) )

  #define IRON_LIFE_TIME  10.0f
  #define NUKE_LIFE_TIME  5.0f

  //#define CANNONBALL_STRETCH 3.0f

  // damages
  #define IRON_DAMAGE_MIN 400 * HEALTH_VALUE_MULTIPLIER
  #define IRON_DAMAGE_MAX 750 * HEALTH_VALUE_MULTIPLIER
  #define IRON_RANGE_DAMAGE (25.0f/4) * HEALTH_VALUE_MULTIPLIER   // because we have 4 explosions //50
  #define IRON_RANGE_HOTSPOT 2.0f  //2
  #define IRON_RANGE_FALLOFF 16.0f //8

  #define NUKE_DAMAGE_MIN 600 * HEALTH_VALUE_MULTIPLIER
  #define NUKE_DAMAGE_MAX 800 * HEALTH_VALUE_MULTIPLIER
  #define NUKE_RANGE_DAMAGE (1000.0f/13) * HEALTH_VALUE_MULTIPLIER   // because we have 13 explosions
  #define NUKE_RANGE_HOTSPOT 15.0f
  #define NUKE_RANGE_FALLOFF 50.0f
  #define SOUND_RANGE 250.0f

  #define STRETCH_0  FLOAT3D(0.0f,0.0f,0.0f)
  #define STRETCH_1  FLOAT3D(1.0f,1.0f,1.0f)
  #define STRETCH_2  FLOAT3D(2.0f,2.0f,2.0f)
  #define STRETCH_3  FLOAT3D(3.0f,3.0f,3.0f)
  #define STRETCH_4  FLOAT3D(4.0f,4.0f,4.0f)
  #define STRETCH_6  FLOAT3D(6.0f,6.0f,6.0f)
  #define STRETCH_8  FLOAT3D(8.0f,8.0f,8.0f)
  #define STRETCH_10 FLOAT3D(10.0f,10.0f,10.0f)

  INDEX ModelForBallProjectile(BallProjectileType eProjectileType)
  {
    switch (eProjectileType)
    {
      case BPT_IRON: return MODEL_PROJECTILE_CANNONBALL_IRON_CFG;
      case BPT_NUKE: return MODEL_PROJECTILE_CANNONBALL_NUKE_CFG;
    }

    return -1;
  }

  void CBallProjectileEntity_OnPrecache(CLibEntityClass *pdec, INDEX iUser)
  {
    pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_CANNON);
    pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_CANNONEXPLOSIONSTAIN);
    pdec->PrecacheClass(CLASS_BASIC_EFFECT, BET_CANNONSHOCKWAVE);

    pdec->PrecacheModelConfig(MODEL_PROJECTILE_CANNONBALL_IRON_CFG);
    pdec->PrecacheModelConfig(MODEL_PROJECTILE_CANNONBALL_NUKE_CFG);

    pdec->PrecacheSound(SOUND_BALL_BOUNCE);
  }

%}

class export CBallProjectileEntity : CProjectileBaseEntity {
name      "Ball Projectile";
thumbnail "";
features "ImplementsOnPrecache";

properties:

  2 FLOAT m_fLaunchPower = 0.0f,      // how fast will ball be launched
  3 FLOAT m_fCannonBallSize = 0.0f,   // the size of the ball
 10 FLOAT m_fIgnoreTime = 0.0f,       // time when laucher will be ignored
 11 FLOAT m_fStartTime = 0.0f,        // start time when launched

 12 INDEX m_iNextChannel = 0,         // next channel to play sound on

 13 BOOL m_bSelfExploded = FALSE,     // if ball exploded because of time, not because of impact

 // sound channels for bouncing sound
 20 CSoundObject m_soBounce0,
 21 CSoundObject m_soBounce1,
 22 CSoundObject m_soBounce2,
 23 CSoundObject m_soBounce3,
 24 CSoundObject m_soBounce4,

 30 enum BallProjectileType m_bptType = BPT_IRON,
 40 FLOAT m_tmInvisibility = 0.0f,           // don't render before given time
 41 FLOAT m_tmExpandBox = 0.0f,              // expand collision after a few seconds
 42 FLOAT m_tmForceExplode = 0.0f,                 // force explosion at given moment

/*
{
  CLightSource m_lsLightSource;
}*/

resources:

  1 class   CLASS_BASIC_EFFECT  "Classes\\BasicEffect.ecl",
  2 class   CLASS_LIGHT         "Classes\\Light.ecl",

// ********* PLAYER ROCKET *********
 20 modelcfg MODEL_PROJECTILE_CANNONBALL_IRON_CFG           "Models\\Projectiles\\CannonBall\\CannonBallProjectile_Iron.vmc",
 21 modelcfg MODEL_PROJECTILE_CANNONBALL_NUKE_CFG           "Models\\Projectiles\\CannonBall\\CannonBallProjectile_Nuke.vmc",

 40 sound   SOUND_BALL_BOUNCE     "Models\\Weapons\\Cannon\\Sounds\\Bounce.wav",

functions:

  // premoving
  void PreMoving(void)
  {
    if (m_tmExpandBox>0) {
      if (_pTimer->CurrentTick()>m_fStartTime+m_tmExpandBox) {
        ChangeCollisionBoxIndexWhenPossible(1);
        m_tmExpandBox = 0;
      }
    }

    CMovableModelEntity::PreMoving();
  }

  void PostMoving(void)
  {
    CMovableModelEntity::PostMoving();

    if (en_vCurrentTranslationAbsolute.Length()<1.0f ||         // if very slow, allmost standing
        _pTimer->CurrentTick()>=m_tmForceExplode ||             // if forced explosion
        (GetCollisionBoxIndex()==0 &&                           // if unable to change collision box for some time
        (_pTimer->CurrentTick()>m_fStartTime+m_tmExpandBox+0.5f)))
    {
      SendEvent(EForceExplode());
    }
  }
  /* Read from stream. */
  void Read_t(CTStream *istr) // throw char *
  {
    CMovableModelEntity::Read_t(istr);
    // setup light source
//    SetupLightSource();
  }

  /* Get static light source information. */
/*
  CLightSource *GetLightSource(void)
  {
//    if (!IsPredictor()) {
//      return &m_lsLightSource;
//    } else {
      return NULL;
//    }
  }
*/

  BOOL AdjustShadingParameters(FLOAT3D &vLightDirection, COLOR &colLight, COLOR &colAmbient)
  {
    // if time now is inside invisibility time, don't render model
    CModelObject *pmo = GetModelObject();

    if ((pmo != NULL) && (_pTimer->GetLerpedCurrentTick() < (m_fStartTime+m_tmInvisibility) ) ) {
      // make it invisible
      pmo->mo_colBlendColor = 0;
    } else {
      // make it visible
      pmo->mo_colBlendColor = C_WHITE|CT_OPAQUE;
    }

    return CEntity::AdjustShadingParameters(vLightDirection, colLight, colAmbient);
  }

/*  // Setup light source
  void SetupLightSource(void)
  {
    // setup light source
    CLightSource lsNew;
    lsNew.ls_ulFlags = LSF_NONPERSISTENT|LSF_DARKLIGHT|LSF_DYNAMIC;
    lsNew.ls_rHotSpot = 0.0f;
    lsNew.ls_colColor = RGBToColor(128, 128, 128);
    lsNew.ls_rFallOff = 5.0f;
    lsNew.ls_plftLensFlare = NULL;
    lsNew.ls_ubPolygonalMask = 0;
    lsNew.ls_paoLightAnimation = NULL;

    m_lsLightSource.ls_penEntity = this;
    m_lsLightSource.SetLightSource(lsNew);
  }
  */

  // render particles
  void RenderParticles(void)
  {
    // no particles when not existing
    if (GetRenderType()!=CEntity::RT_MODEL) {
      return;
    }

    FLOAT fSpeedRatio = Min(en_vCurrentTranslationAbsolute.Length()/140.0f, 1.0f);
    INDEX ctFireParticles = INDEX((Max(fSpeedRatio-0.5f, 0.0f)*2.0f)*128);
    //CDebugF("fSpeedRatio=%g, ctFireParticles=%d\n", fSpeedRatio, ctFireParticles);

    if (_pTimer->GetLerpedCurrentTick()-m_fStartTime > 0.075)
    {
      Particles_BeastBigProjectileTrail(this, 2.0f, 1.0f, 0.75f, ctFireParticles);
    }
  }

  void Initialize(void)
  {
    const INDEX idModelConfig = ModelForBallProjectile(m_bptType);

    // set appearance
    InitAsModel();
    SetPhysicsFlags(EPF_MODEL_BOUNCING);
    SetCollisionFlags(ECF_CANNON_BALL);
    SetAnyModel(idModelConfig);

    // stretch it
    GetModelObject()->StretchModel(FLOAT3D(m_fCannonBallSize, m_fCannonBallSize, m_fCannonBallSize));
    ModelChangeNotify();

    // start moving
    LaunchAsFreeProjectile(FLOAT3D(0.0f, 0.0f, -m_fLaunchPower), (CMovableEntity*)(CEntity*)m_penLauncher);
    en_fBounceDampNormal   = 0.5f;
    en_fBounceDampParallel = 0.75f;
    en_fAcceleration = 0.0f;
    en_fDeceleration = 5.0f;
    en_fCollisionSpeedLimit = 40.0f;
    en_fCollisionDamageFactor = 10.0f;
    SetHealthInit(50000 * HEALTH_VALUE_MULTIPLIER);
    GetModelObject()->PlayAnim(CANNONBALL_ANIM_FIRESLOW, 0);
  };

  INDEX CalculateDamageToInflict(void)
  {
    INDEX iMaxDamage = IRON_DAMAGE_MAX;

    if (m_bptType == BPT_NUKE) {
      iMaxDamage = IRON_DAMAGE_MAX;
    }

    // speed can range from
    FLOAT fSpeedRatio = en_vCurrentTranslationAbsolute.Length()/140.0f;

    // apply damage to range from 0 to damage max
    return Clamp(INDEX(fSpeedRatio * iMaxDamage), INDEX(0), iMaxDamage);
  }

  void Explosion(FLOAT3D vCenter,
                 const FLOAT3D &vStretchExplosion,
                 const FLOAT3D &vStretchShockwave,
                 const FLOAT3D &vStretchStain,
                 BOOL bHasExplosion,
                 BOOL bHasShockWave,
                 BOOL bHasStain,
                 BOOL bHasLight)
  {
    ESpawnEffect ese;
    FLOAT3D vOnPlane;
    FLOATplane3D vPlaneNormal;
    FLOAT fDistanceToEdge;

    // explosion
    if (bHasExplosion)
    {
      ese.colMuliplier = C_WHITE|CT_OPAQUE;
      ese.betType = bHasLight ? BET_CANNON : BET_CANNON_NOLIGHT;
      ese.vStretch = vStretchExplosion;
      CPlacement3D plHandle = GetPlacement();
      plHandle.pl_PositionVector+=vCenter;
      SpawnEffect(plHandle, ese);

      // spawn sound event in range
      if (m_penLauncher != NULL && m_penLauncher->IsPlayerControlled()) {
        SpawnRangeSound(m_penLauncher, this, SNDT_PLAYER, 100.0f);
      }
    }

    // on plane
    if (GetNearestPolygon(vOnPlane, vPlaneNormal, fDistanceToEdge)) {
      if ((vOnPlane-GetPlacement().pl_PositionVector).Length() < 3.5f) {
        if (bHasStain)
        {
          // wall stain
          ese.colMuliplier = C_WHITE|CT_OPAQUE;
          ese.betType = BET_CANNONEXPLOSIONSTAIN;
          ese.vNormal = FLOAT3D(vPlaneNormal);
          ese.vStretch = vStretchShockwave;
          SpawnEffect(CPlacement3D(vOnPlane, ANGLE3D(0, 0, 0)), ese);
        }

        if (bHasShockWave)
        {
          // shock wave horizontal
          ese.colMuliplier = C_WHITE|CT_OPAQUE;
          ese.betType = BET_CANNONSHOCKWAVE;
          ese.vNormal = FLOAT3D(vPlaneNormal);
          ese.vStretch = vStretchShockwave;
          SpawnEffect(CPlacement3D(vOnPlane, ANGLE3D(0, 0, 0)), ese);
        }
        // shock wave vertical
        /*
        ese.colMuliplier = C_WHITE|CT_OPAQUE;
        ese.betType = BET_CANNONSHOCKWAVE;
        ese.vNormal = FLOAT3D(vPlaneNormal);
        ese.vStretch = vStretchShockwave;
        SpawnEffect(CPlacement3D(vOnPlane, ANGLE3D(0, 0.0f, 0)), ese);
        */
        // second explosion on plane
        /*
        ese.colMuliplier = C_WHITE|CT_OPAQUE;
        ese.betType = BET_CANNON_PLANE;
        ese.vNormal = FLOAT3D(vPlaneNormal);
        ese.vStretch = vStretchExplosion;
        SpawnEffect(CPlacement3D(vOnPlane+ese.vNormal/50.0f, ANGLE3D(0, 0, 0)), ese);
        */
      }
    }

    RangeDamage();
  };

/************************************************************
 *             C O M M O N   F U N C T I O N S              *
 ************************************************************/

  // ball touch his valid target
  BOOL BallTouchExplode(CEntityPointer penHit)
  {
    FLOAT fApplyDamage = CalculateDamageToInflict();

    // obtain touched entity health
    FLOAT fHealth = 100;
    BOOL bForceCannonballToExplode = FALSE;

    if (penHit->GetPhysicsFlags()&EPF_MOVABLE) {
      fHealth = ((CMovableEntity&)*penHit).GetHealth();

      if (IsDerivedFromClass(penHit, &CEnemyBaseEntity_DLLClass))
      {
        bForceCannonballToExplode = ((CEnemyBaseEntity&)*penHit).ForcesCannonballToExplode();
      }

    } else {

      if (IsOfClass(penHit, "ModelHolder2") || IsOfClass(penHit, "ExotechLarvaBattery")) {
        fHealth = ((CLiveEntity&)*penHit).GetHealth();
      } else {
        return FALSE;
      }
    }

    if (IsOfClass(penHit, "ModelHolder2"))
    {
      bForceCannonballToExplode=TRUE;
    }

    // TODO: Fix it!
    if (IsDerivedFromClass(penHit, &CPlayerPawnEntity_DLLClass)) {
      fHealth += ((CPlayerPawnEntity&)*penHit).GetArmor() * 2.0f  * 0.01F; // TODO: Remove 0.01 multiplication when integer-based health will be done.
    }

    // inflict direct damage to kill hitted entity
    FLOAT3D vHitPoint = GetPlacement().pl_PositionVector;
    FLOAT3D vDirection = en_vCurrentTranslationAbsolute;
    vDirection.Normalize();
  //  CDebugF("Applied damage %g\n", fApplyDamage);
    const FLOAT fDamageMul = GetSeriousDamageMultiplier(m_penLauncher);
    
    InflictDirectDamage(penHit, m_penLauncher, DMT_CANNONBALL, fApplyDamage*fDamageMul, vHitPoint, vDirection);

    return (fApplyDamage <= fHealth || bForceCannonballToExplode);
  };


  // infilict range damage by cannonball
  void RangeDamage(void)
  {
    const FLOAT fDamageMul = GetSeriousDamageMultiplier(m_penLauncher);
    FLOAT3D vSource = GetPlacement().pl_PositionVector;

    if (m_bptType == BPT_IRON) {
      InflictRangeDamage(m_penLauncher, DMT_CANNONBALL_EXPLOSION, IRON_RANGE_DAMAGE * fDamageMul,
          vSource, IRON_RANGE_HOTSPOT, IRON_RANGE_FALLOFF);
    } else {
      // nuclear explosion ...
      InflictRangeDamage(m_penLauncher, DMT_CANNONBALL_EXPLOSION, NUKE_RANGE_DAMAGE * fDamageMul,
          vSource, NUKE_RANGE_HOTSPOT, NUKE_RANGE_FALLOFF);
    }
  };

  // spawn effect
  void SpawnEffect(const CPlacement3D &plEffect, const ESpawnEffect &eSpawnEffect)
  {
    CEntityPointer penEffect = CreateEntity(plEffect, CLASS_BASIC_EFFECT);
    penEffect->Initialize(eSpawnEffect);
  };

/************************************************************
 *                      S O U N D S                         *
 ************************************************************/
  void BounceSound(FLOAT fSpeed)
  {
    FLOAT fVolume = Clamp(fSpeed/6.0f, 0.0f, 1.0f);

    if (fVolume < 0.1f) {
      return;
    }

    CSoundObject &so = (&m_soBounce0)[m_iNextChannel];
    m_iNextChannel = (m_iNextChannel+1)%5;
    so.Set3DParameters(70.0f, 10.0f, fVolume, 1.0f);
    PlaySound(so, SOUND_BALL_BOUNCE, SOF_3D);
  };

/************************************************************
 *                   P R O C E D U R E S                    *
 ************************************************************/
procedures:

  Bounce(EVoid)
  {
    // if already inside some entity
    CEntity *penObstacle;

    if (CheckForCollisionNow(0, &penObstacle)) {
      // explode now
      return EEnd();
    }

    FLOAT fWaitTime = IRON_LIFE_TIME;

    // if this is nuke ball
    if (m_bptType == BPT_NUKE)
    {
      fWaitTime = NUKE_LIFE_TIME;
    }

    // bounce loop
    wait(fWaitTime)
    {
      on (EBegin) : { resume; }

      on (EPass epass) :
      {
        BOOL bHit;
        // ignore launcher within 1 second
        bHit = epass.penOther!=m_penLauncher || _pTimer->CurrentTick()>m_fIgnoreTime;
        // ignore twister
        bHit &= !IsOfClass(epass.penOther, "Twister");

        if (bHit)
        {
          if (BallTouchExplode(epass.penOther)) { stop; }
        }
        resume;
      }

      on (ETouch etouch) :
      {
        // explode if touched another cannon ball
        if (IsOfSameClass(etouch.penOther, this))
        {
          stop;
        }

        if (IsOfClass(etouch.penOther, &CMovingBrushEntity_DLLClass))
        {
          CMovingBrushEntity &br = (CMovingBrushEntity &) *etouch.penOther;

          if (br.m_fHealth > 0)
          {
            FLOAT3D vHitPoint = GetPlacement().pl_PositionVector;
            FLOAT3D vDirection = en_vCurrentTranslationAbsolute;
            vDirection.Normalize();
            InflictDirectDamage(etouch.penOther, m_penLauncher, DMT_CANNONBALL, CalculateDamageToInflict(),
                       vHitPoint, vDirection);
            m_bSelfExploded = FALSE;
            stop;
          }
        }

        if (IsOfClass(etouch.penOther, &CDestroyableArchitectureEntity_DLLClass))
        {
          CDestroyableArchitectureEntity &br = (CDestroyableArchitectureEntity &) *etouch.penOther;

          if (br.m_fHealth > 0)
          {
            FLOAT3D vHitPoint = GetPlacement().pl_PositionVector;
            FLOAT3D vDirection = en_vCurrentTranslationAbsolute;
            vDirection.Normalize();
            InflictDirectDamage(etouch.penOther, m_penLauncher, DMT_CANNONBALL, CalculateDamageToInflict(),
                       vHitPoint, vDirection);
            m_bSelfExploded = FALSE;
            stop;
          }
        }

        // clear time limit for launcher
        //m_fIgnoreTime = 0.0f;
        BounceSound(((FLOAT3D&)etouch.plCollision) % en_vCurrentTranslationAbsolute);
        resume;
      }

      on (EForceExplode) : { stop; }
      on (EDeath) : { stop; }
      on (ETimer) : { stop; }
    }

    m_bSelfExploded = TRUE;
    return EEnd();
  };

  // --->>> MAIN
  Main(ELaunchBallProjectile eLaunch)
  {
    // remember the initial parameters
    ASSERT(eLaunch.penLauncher!=NULL);
    m_penLauncher = eLaunch.penLauncher;
    m_fLaunchPower = eLaunch.fLaunchPower;
    m_bptType = eLaunch.bptType;
    m_fCannonBallSize = eLaunch.fSize;
    m_tmInvisibility = 0.05f;
    m_bSelfExploded = FALSE;
    m_tmExpandBox = 0.0001f;

    // setup time for forced expolding
    m_tmForceExplode=_pTimer->CurrentTick()+30.0f;

    // initialization
    Initialize();

    SendEvent(EReturn());

    wait()
    {
      on (EBegin) : { resume;}
      on (EReturn) : { stop;}
    }

    // cast ray to check possible collision
    FLOAT tmCastCoverPath = _pTimer->TickQuantum*1.5f;
    CCastRay crRay(m_penLauncher, GetPlacement(), m_fLaunchPower*tmCastCoverPath);
    crRay.cr_bHitTranslucentPortals = FALSE;
    crRay.cr_fTestR = 0.75f/2.0f*m_fCannonBallSize;
    crRay.cr_ttHitModels = CCastRay::TT_COLLISIONBOX;
    GetWorld()->CastRay(crRay);

    // can't hurt player time
    m_fIgnoreTime = _pTimer->CurrentTick() + 0.1f;

    // bounce
    m_fStartTime = _pTimer->CurrentTick();

    if (crRay.cr_penHit!=NULL && crRay.cr_penHit->GetRenderType()==RT_MODEL)
    {
      if (BallTouchExplode(crRay.cr_penHit))
      {
        m_tmForceExplode = _pTimer->CurrentTick()+tmCastCoverPath;
      }
    }

    autocall Bounce() EEnd;

    // dissapear
    SwitchToEditorModel();

    // stop in place
    ForceFullStop();

    if (m_penLauncher != NULL && m_penLauncher->IsPlayerControlled()) {
      // sound event
      ESound eSound;
      eSound.EsndtSound = SNDT_EXPLOSION;
      eSound.penTarget = m_penLauncher;
      SendEventInRange(eSound, FLOATaabbox3D(GetPlacement().pl_PositionVector, SOUND_RANGE));
    }

    if (m_bptType == BPT_IRON)
    {
   //   Explosion(FLOAT3D(0.0f,0.0f,0.0f),   STRETCH_3, STRETCH_3, STRETCH_3, TRUE, TRUE,  TRUE, TRUE);
   //   autowait(0.15f);
      Explosion(FLOAT3D(0.0f,0.0f,0.0f),   STRETCH_3, STRETCH_3, STRETCH_4, TRUE, TRUE,  TRUE, TRUE);
      Explosion(FLOAT3D(1.0f,1.5f,1.5f),   STRETCH_3, STRETCH_3, STRETCH_4, TRUE, FALSE, FALSE, FALSE);
      Explosion(FLOAT3D(-2.0f,1.0f,-1.5f), STRETCH_3, STRETCH_3, STRETCH_4, TRUE, FALSE, FALSE, FALSE);
      Explosion(FLOAT3D(-1.0f,0.5f,1.0f),  STRETCH_4, STRETCH_4, STRETCH_4, TRUE, FALSE, FALSE, FALSE);
    }
    else if (m_bptType == BPT_NUKE)
    {
      Explosion(FLOAT3D(0.0f,0.0f,0.0f),   STRETCH_6, STRETCH_6, STRETCH_10, TRUE, TRUE,  TRUE, TRUE);
      autowait(0.15f);
      Explosion(FLOAT3D(4.0f,5.0f,5.0f),   STRETCH_4, STRETCH_6, STRETCH_10, TRUE, FALSE, FALSE, FALSE);
      autowait(0.2f);
      Explosion(FLOAT3D(-5.0f,3.0f,-4.0f), STRETCH_4, STRETCH_6, STRETCH_10, TRUE, FALSE, FALSE, FALSE);
      autowait(0.15f);
      Explosion(FLOAT3D(-3.0f,2.0f,3.0f),  STRETCH_4, STRETCH_6, STRETCH_10, TRUE, FALSE, FALSE, FALSE);
      autowait(0.15f);
      Explosion(FLOAT3D(2.0f,1.0f,4.0f),   STRETCH_4, STRETCH_6, STRETCH_10, TRUE, TRUE,  FALSE, FALSE);
      autowait(0.2f);
      Explosion(FLOAT3D(-2.0f,5.0f,-4.0f), STRETCH_4, STRETCH_6, STRETCH_10, TRUE, FALSE, FALSE, FALSE);
      autowait(0.18f);
      Explosion(FLOAT3D(-3.0f,2.0f,2.0f),  STRETCH_4, STRETCH_6, STRETCH_10, TRUE, FALSE, FALSE, FALSE);
      autowait(0.25f);
      Explosion(FLOAT3D(0.0f,4.0f,-1.0f),  STRETCH_4, STRETCH_6, STRETCH_10, TRUE, FALSE, FALSE, FALSE);
      autowait(0.15f);
      Explosion(FLOAT3D(2.0f,0.0f,-3.0f),  STRETCH_4, STRETCH_6, STRETCH_10, TRUE, TRUE,  FALSE, FALSE);
      autowait(0.25f);
      Explosion(FLOAT3D(-1.0f,2.0f,0.0f),  STRETCH_4, STRETCH_6, STRETCH_10, TRUE, FALSE, FALSE, FALSE);
      autowait(0.125f);
      Explosion(FLOAT3D(3.0f,1.0f,1.0f),   STRETCH_4, STRETCH_6, STRETCH_10, TRUE, FALSE, FALSE, FALSE);
      autowait(0.1f);
      Explosion(FLOAT3D(3.0f,2.0f,2.0f),   STRETCH_4, STRETCH_6, STRETCH_10, TRUE, FALSE, FALSE, FALSE);
      autowait(0.125f);
      Explosion(FLOAT3D(3.0f,2.0f,2.0f),   STRETCH_4, STRETCH_6, STRETCH_10, TRUE, TRUE,  FALSE, FALSE);
    }

    // cease to exist
    Destroy();

    return;
  }
};
