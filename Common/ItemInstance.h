/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ITEMINSTANCE_H
#define SE_INCL_ITEMINSTANCE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

//! Abstract object within TObjectContainer each has typeid.
class FItemInstance
{
  public:
    enum InstanceType
    {
      //! Just item without all this data. Suitable for keys.
      IT_GENERIC = 0,

      //! Stackable stuff like currencies and ammo.
      IT_STACKABLE,

      //! Weapons carried with you.
      IT_WEAPON,

      //! Status effect.
      IT_STATUS,
    };

  protected:
    ULONG _idItemType;
    ULONG _ulFlags;
    
  public:
    //! Default constructor.
    inline FItemInstance()
    {
      _idItemType = 0;
      _ulFlags = 0;
    }

    //! Constructor with typeid.
    inline FItemInstance(ULONG idItemType)
    {
      _idItemType = idItemType;
      _ulFlags = 0;
    }

    virtual void Copy(const FItemInstance &other)
    {
      _idItemType = other.GetTypeId();
    }

    //! Returns object typeid.
    inline ULONG GetTypeId() const
    {
      return _idItemType;
    }

    //! Sets object typeid.
    inline void SetTypeId(ULONG idItemType)
    {
      _idItemType = idItemType;
    }

    //! Get object flags.
    inline ULONG GetFlags() const
    {
      return _ulFlags;
    }

    //! Setup object flags.
    inline void SetFlags(ULONG ulFlags)
    {
      _ulFlags = ulFlags;
    }

    //! Returns item quantity.
    virtual INDEX GetQty() const
    {
      return 1;
    }

    //! Setup item quantity.
    virtual void SetQty(INDEX iQuantity) {}

    virtual InstanceType GetInstanceType() const
    {
      return IT_GENERIC;
    }

    inline BOOL IsGeneric() const
    {
      return GetInstanceType() == IT_GENERIC;
    }

    inline BOOL IsStackable() const
    {
      return GetInstanceType() == IT_STACKABLE;
    }

    inline BOOL IsWeapon() const
    {
      return GetInstanceType() == IT_WEAPON;
    }

    inline BOOL IsStatus() const
    {
      return GetInstanceType() == IT_STATUS;
    }

    virtual void Read_t(CTStream &strm)
    {
      strm >> _idItemType;
      strm >> _ulFlags;
    }

    virtual void Write_t(CTStream &strm)
    {
      strm << _idItemType;
      strm << _ulFlags;
    }
};

struct Comp_ItemMatchesBase : public TObjectComparator<BOOL, FItemInstance>
{
  BOOL operator()(const FItemInstance &tFirst, const FItemInstance &tSecond) const override
  {
    if (tFirst.GetInstanceType() != tSecond.GetInstanceType()) {
      return FALSE;
    }

    if (tFirst.GetTypeId() != tSecond.GetTypeId()) {
      return FALSE;
    }

    if (tFirst.GetFlags() != tSecond.GetFlags()) {
      return FALSE;
    }

    return TRUE;
  }
};

#endif