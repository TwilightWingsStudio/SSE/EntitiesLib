/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

CStatusEffectInstance::CStatusEffectInstance()
{
  Clear();
}

CStatusEffectInstance::CStatusEffectInstance(const CStatusEffectInstance &other)
{
  Copy(other);
}

CStatusEffectInstance::CStatusEffectInstance(ULONG idItemType, UBYTE ubAmplifier, FLOAT tmDuration)
{
  Clear();

  _idItemType = idItemType;
  _ubAmplifier = ubAmplifier;
  _tmDuration = tmDuration;
}

void CStatusEffectInstance::Copy(const FItemInstance &other)
{
  ASSERT(other.GetInstanceType() == GetInstanceType());

  const CStatusEffectInstance &mother = (CStatusEffectInstance &)other;

  _idItemType = mother.GetTypeId();
  _ubAmplifier = mother.GetAmplifier();
  _iValueA = mother.GetValueA();
  _iValueB = mother.GetValueB();
  _tmDuration = mother.GetDuration();
  _tmPeriodicTimer = mother.GetPeriodicTimer();
}

void CStatusEffectInstance::Clear()
{
  _idItemType = 0;
  _ubAmplifier = 0;
  _iValueA = 0;
  _iValueB = 0;
  _tmDuration = -1;
  _tmPeriodicTimer = 0.0F;
}

void CStatusEffectInstance::Read_t(CTStream &strm)
{
  strm >> _idItemType;
  strm >> _ubAmplifier;
  strm >> _iValueA;
  strm >> _iValueB;
  strm >> _tmDuration;
  strm >> _tmPeriodicTimer;
}

void CStatusEffectInstance::Write_t(CTStream &strm)
{
  strm << _idItemType;
  strm << _ubAmplifier;
  strm << _iValueA;
  strm << _iValueB;
  strm << _tmDuration;
  strm << _tmPeriodicTimer;
}

void CStatusEffectInstance::SetAmplifier(UBYTE ubAmplifier)
{
  _ubAmplifier = ubAmplifier;
}

void CStatusEffectInstance::SetValueA(INDEX iValue)
{
  _iValueA = iValue;
}

void CStatusEffectInstance::SetValueB(INDEX iValue)
{
  _iValueB = iValue;
}

void CStatusEffectInstance::SetDuration(FLOAT tmDuration)
{
  _tmDuration = tmDuration;
}

void CStatusEffectInstance::ClampDuration(FLOAT tmMaxDuration)
{
  _tmDuration = ClampUp(_tmDuration, tmMaxDuration);
}