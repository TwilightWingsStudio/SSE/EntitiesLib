/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"
#include "TokenRegistry.h"
#include <Core/Templates/NameTable.cpp>

class Entities_Items : public FTokenRegistry
{
  public:
    //! Constructor
    Entities_Items()
    {
      // Weapons.
      Register("wpn_none", new FWeaponParams(E_AMMO_INVALID, 0, 0, 0, 0, 0.0F, PWPFF_SOLO), TDF_WEAPON); // E_WEAPON_NONE
      Register("wpn_knife", new FWeaponParams(E_AMMO_INVALID, 0, 0, 1, 100, 0.3f, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_KNIFE
      Register("wpn_chainsaw", new FWeaponParams(E_AMMO_INVALID, 0, 0, 1, 150, 0.15f, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_CHAINSAW
      Register("wpn_revolver", new FWeaponParams(E_AMMO_INVALID, 0, 1, 2, 200, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_REVOLVER
      Register("wpn_shotgun", new FWeaponParams(E_AMMO_SHELLS, 10, 1, 3, 300, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_SHOTGUN
      Register("wpn_supershotgun", new FWeaponParams(E_AMMO_SHELLS, 20, 2, 3, 350, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_SUPERSHOTGUN
      Register("wpn_machinegun", new FWeaponParams(E_AMMO_HEAVY_ROUNDS, 50, 1, 4, 400, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_MACHINEGUN
      Register("wpn_chaingun", new FWeaponParams(E_AMMO_HEAVY_ROUNDS, 100, 1, 4, 450, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_CHAINGUN
      Register("wpn_rocketlauncher", new FWeaponParams(E_AMMO_ROCKETS, 4, 1, 5, 500, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_ROCKETLAUNCHER
      Register("wpn_grenadelauncher", new FWeaponParams(E_AMMO_GRENADES, 5, 1, 5, 550, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_GRENADELAUNCHER
      Register("wpn_flamer", new FWeaponParams(E_AMMO_FUEL, 50, 1, 7, 700, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_FLAMER
      Register("wpn_plasmagun", new FWeaponParams(E_AMMO_ENERGY, 50, 1, 6, 750, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_PLASMAGUN
      Register("wpn_beamgun", new FWeaponParams(E_AMMO_ENERGY, 50, 1, 6, 775, 0.0F, PWPFF_SOLO), TDF_WEAPON); // E_WEAPON_BEAMGUN
      Register("wpn_sniper", new FWeaponParams(E_AMMO_SNIPER_ROUNDS, 15, 1, 7, 800, 0.0F, PWPFF_ZOOM), TDF_WEAPON); // E_WEAPON_SNIPER
      Register("wpn_cannon", new FWeaponParams(E_AMMO_IRON_BALLS, 1, 1, 8, 1000, 0.0F, PWPFF_NONE), TDF_WEAPON); // E_WEAPON_CANNON
      Register("wpn_plasmathrower", new FWeaponParams(E_AMMO_ENERGY, 50, 1, 8, 950, 0.0F, PWPFF_NONE), TDF_WEAPON|TDF_NEW); // E_WEAPON_PLASMATHROWER
      Register("wpn_minelayer", new FWeaponParams(E_AMMO_GRENADES, 5, 1, 2, 100, 0.0F, PWPFF_NONE), TDF_WEAPON|TDF_NEW); // E_WEAPON_MINELAYER

      // Ammo.
      Register("a_shell", new FTokenDefinition(50), TDF_AMMO);  // E_AMMO_SHELLS
      Register("a_light_round", new FTokenDefinition(250), TDF_AMMO|TDF_NEW); // E_AMMO_LIGHT_ROUNDS
      Register("a_heavy_round", new FTokenDefinition(250), TDF_AMMO); // E_AMMO_HEAVY_ROUNDS
      Register("a_sniper_round", new FTokenDefinition(25), TDF_AMMO);  // E_AMMO_SNIPER_ROUNDS
      Register("a_energy", new FTokenDefinition(200), TDF_AMMO); // E_AMMO_ENERGY
      Register("a_heavy_energy", new FTokenDefinition(200), TDF_AMMO|TDF_NEW); // E_AMMO_HEAVY_ENERGY
      Register("a_fuel", new FTokenDefinition(250), TDF_AMMO); // E_AMMO_FUEL
      Register("a_rocket", new FTokenDefinition(25), TDF_AMMO);  // E_AMMO_ROCKETS
      Register("a_grenade", new FTokenDefinition(25), TDF_AMMO);  // E_AMMO_GRENADES
      Register("a_iron_ball", new FTokenDefinition(15), TDF_AMMO);  // E_AMMO_IRON_BALLS
      Register("a_arrow", new FTokenDefinition(25), TDF_AMMO|TDF_NEW);  // E_AMMO_ARROWS

      // Gadgets.
      Register("g_nuke", new FTokenDefinition(3), TDF_GADGET); // E_GADGET_NUKE
      Register("g_stimpack", new FTokenDefinition(4), TDF_GADGET); // E_GADGET_STIMPACK
      Register("g_medkit", new FTokenDefinition(3), TDF_GADGET); // E_GADGET_MEDKIT
      Register("g_shield_cell", new FTokenDefinition(4), TDF_GADGET); // E_GADGET_SHIELD_CELL
      Register("g_shield_battery", new FTokenDefinition(3), TDF_GADGET); // E_GADGET_SHIELD_BATTERY

      // Currencies.
      Register("c_money", new FTokenDefinition(ULONG(-1)), TDF_CURRENCY); // E_CURRENCY_MONEY
      Register("c_extra_life", new FTokenDefinition(ULONG(-1)), TDF_CURRENCY); // E_CURRENCY_EXTRA_LIFE
      Register("c_skill_point", new FTokenDefinition(ULONG(-1)), TDF_CURRENCY); // E_CURRENCY_SKILL_POINT
      Register("c_material", new FTokenDefinition(ULONG(-1)), TDF_CURRENCY); // E_CURRENCY_MATERIALS
      Register("c_expertience", new FTokenDefinition(ULONG(-1)), TDF_CURRENCY); // E_CURRENCY_EXPERIENCE
    }
};

Entities_Items _Items;

FTokenDefinition *GetItemDef(ItemType eItemType, ULONG idItemType)
{
  switch (eItemType)
  {
    case E_ITEM_WEAPON: return &_Items.cTokens[idItemType];
    case E_ITEM_AMMO: return &_Items.cTokens[idItemType + (E_WEAPON_MAX - 1)];
    case E_ITEM_GADGET: return &_Items.cTokens[idItemType + (E_WEAPON_MAX - 1) + (E_AMMO_MAX - 1)];
    case E_ITEM_CURRENCY: return &_Items.cTokens[idItemType + (E_WEAPON_MAX - 1) + (E_AMMO_MAX - 1) + (E_GADGET_MAX - 1)];
  }

  return &_Items.cTokens[0];
}

FWeaponParams *GetWeaponParams(WeaponIndex eWeaponIndex)
{
  return static_cast<FWeaponParams *>(GetItemDef(E_ITEM_WEAPON, eWeaponIndex));
}

const char *FTokenDefinition::GetIdentifier(AmmoIndex eAmmoIndex)
{
  static const char *AMMO_IDENTIFIERS[E_AMMO_MAX] = {
    "a_invalid",      // E_AMMO_INVALID
    "a_shell",        // E_AMMO_SHELLS
    "a_light_round",  // E_AMMO_LIGHT_ROUNDS
    "a_heavy_round",  // E_AMMO_HEAVY_ROUNDS
    "a_sniper_round", // E_AMMO_SNIPER_ROUNDS
    "a_energy",       // E_AMMO_ENERGY
    "a_heavy_energy", // E_AMMO_HEAVY_ENERGY
    "a_fuel",         // E_AMMO_FUEL
    "a_rocket",       // E_AMMO_ROCKETS
    "a_grenade",      // E_AMMO_GRENADES
    "a_iron_ball",    // E_AMMO_IRON_BALLS
    "a_arrow",        // E_AMMO_ARROWS
  };
  
  if (eAmmoIndex > E_AMMO_INVALID && eAmmoIndex < E_AMMO_MAX) {
    return AMMO_IDENTIFIERS[eAmmoIndex];
  }

  return AMMO_IDENTIFIERS[E_AMMO_INVALID];
}
