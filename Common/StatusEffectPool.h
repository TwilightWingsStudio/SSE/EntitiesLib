/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATUSEFFECTPOOL_H
#define SE_INCL_STATUSEFFECTPOOL_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class DECL_DLL CStatusEffectPool : public CItemContainer
{    
  public:
    CStatusEffectInstance *GetStrongestInstance(enum StatusEffectIndex eEffectIndex);
    INDEX GetStrongestAmp(enum StatusEffectIndex eEffectIndex);
    CStatusEffectInstance *AddInstance(CStatusEffectInstance &e, FLOAT fMaxDuration);
    void OnStep(FLOAT tmDelta);

    //! Returns default instance type for this container.
    UBYTE GetDefaultInstanceType() const override
    {
      return FItemInstance::IT_STATUS;
    }
};

#endif