/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATUSEFFECTDEFINITION_H
#define SE_INCL_STATUSEFFECTDEFINITION_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class DECL_DLL CStatusEffectDefinition : public FTokenDefinition
{
  public:
    enum Type
    {
      TYPE_NEUTRAL = 0,
      TYPE_BENEFICIAL,
      TYPE_HARMFUL,
    };

  protected:
    enum Type _eType;
  
  public:
    //! Default constructor.
    inline CStatusEffectDefinition()
    {
      _iMaxQty = 1;
      _pMatches = NULL;
      _eType = TYPE_NEUTRAL;
    }

    //! Constructor.
    inline CStatusEffectDefinition(Type eType)
    {
      _iMaxQty = 1;
      _pMatches = NULL;
      _eType = eType;
    }
    
    //! Returns effect type.
    inline Type GetType() const
    {
      return _eType;
    }

    //! Check if effect is beneficial.
    inline BOOL IsBeneficial() const
    {
      return GetType() == TYPE_BENEFICIAL;
    }

    //! Check if effect is harmful.
    inline BOOL IsHarmful() const
    {
      return GetType() == TYPE_HARMFUL;
    }
};

#endif