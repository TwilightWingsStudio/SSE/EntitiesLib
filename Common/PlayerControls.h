/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_PLAYERCONTROLS_H
#define SE_INCL_PLAYERCONTROLS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

// defines representing flags used to fill player buttoned actions
enum PlayerActionFlags
{
  PLACT_FIRE              = (1L << 0),
  PLACT_SECONDARY_FIRE    = (1L << 1),
  PLACT_RELOAD            = (1L << 2),
  PLACT_WEAPON_NEXT       = (1L << 3),
  PLACT_WEAPON_PREV       = (1L << 4),
  PLACT_AIM_DOWN_SIGHT    = (1L << 5),
  PLACT_USE               = (1L << 6),
  PLACT_SECONDARY_USE     = (1L << 7),
  PLACT_3RD_PERSON_VIEW   = (1L << 8),
  PLACT_CENTER_VIEW       = (1L << 9),
  PLACT_COMPUTER          = (1L << 10),
  PLACT_SNIPER_ZOOMIN     = (1L << 11),
  PLACT_SNIPER_ZOOMOUT    = (1L << 12),
  PLACT_WEAPON_FLIP       = (1L << 13),
  PLACT_GADGET            = (1L << 14),
  PLACT_SELECT_WEAPON_MOD = (1L << 15),
};

#define PLACT_SELECT_WEAPON_SHIFT (16)
#define PLACT_SELECT_WEAPON_MASK  (0x1FL << PLACT_SELECT_WEAPON_SHIFT) // use five bits

#define PLACT_FAVORITE_WEAPON_SHIFT (PLACT_SELECT_WEAPON_SHIFT + 5)
#define PLACT_FAVORITE_WEAPON_MASK  (0x1FL << PLACT_FAVORITE_WEAPON_SHIFT) // use five bits

#define MAX_WEAPONS 30
#define MAX_FAVORITE_WEAPONS 4

struct PlayerControls
{
  FLOAT3D aRotation;
  FLOAT3D aViewRotation;
  FLOAT3D vTranslation;

  BOOL bMoveForward;
  BOOL bMoveBackward;
  BOOL bMoveLeft;
  BOOL bMoveRight;
  BOOL bMoveUp;
  BOOL bMoveDown;

  BOOL bTurnLeft;
  BOOL bTurnRight;
  BOOL bTurnUp;
  BOOL bTurnDown;
  BOOL bTurnBankingLeft;
  BOOL bTurnBankingRight;
  BOOL bCenterView;

  BOOL bLookLeft;
  BOOL bLookRight;
  BOOL bLookUp;
  BOOL bLookDown;
  BOOL bLookBankingLeft;
  BOOL bLookBankingRight;

  BOOL bSelectWeapon[MAX_WEAPONS+1];
  BOOL bWeaponNext;
  BOOL bWeaponPrev;
  BOOL bWeaponFlip;
  
  BOOL bWeaponSelectionModifier;

  BOOL bWalk;
  BOOL bStrafe;
  BOOL bFire;
  BOOL bSecondaryFire;
  BOOL bReload;
  BOOL bUse;
  BOOL bSecondaryUse;
  BOOL bComputer;
  BOOL bUseOrComputer;
  BOOL bUseOrComputerLast;  // for internal use
  BOOL b3rdPersonView;
  BOOL bAimDownSight;

  BOOL bSniperZoomIn;
  BOOL bSniperZoomOut;
  BOOL bGadget;

  BOOL bFavoriteWeapon[MAX_WEAPONS + 1];
};

#endif