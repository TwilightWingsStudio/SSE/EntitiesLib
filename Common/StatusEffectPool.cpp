/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

static int qsort_CompareAmplifier_Descending(const void *ppPEN0, const void *ppPEN1)
{
  CStatusEffectInstance &ar0 = **(CStatusEffectInstance**)ppPEN0;
  CStatusEffectInstance &ar1 = **(CStatusEffectInstance**)ppPEN1;

  ULONG id0 = ar0.GetAmplifier();
  ULONG id1 = ar1.GetAmplifier();

  if (id0 < id1) return +1;
  else if (id0 > id1) return -1;
  else              return  0;
}

CStatusEffectInstance *CStatusEffectPool::GetStrongestInstance(enum StatusEffectIndex eEffectIndex)
{
  const Func_CheckTypeId<FItemInstance> checkFunc(eEffectIndex);
  TDynamicContainer<FItemInstance> cObjectsFound;

  m_cObjects.Find(cObjectsFound, checkFunc);
  cObjectsFound.Sort(&qsort_CompareAmplifier_Descending);

  if (cObjectsFound.Count() == 0) {
    return 0;
  }

  return (CStatusEffectInstance *)cObjectsFound.Pointer(0);
}

INDEX CStatusEffectPool::GetStrongestAmp(enum StatusEffectIndex eEffectIndex)
{
  CStatusEffectInstance *pInstance = GetStrongestInstance(eEffectIndex);

  if (pInstance == nullptr) {
    return 0;
  }

  return pInstance->GetAmplifier() + 1;
}

CStatusEffectInstance *CStatusEffectPool::AddInstance(CStatusEffectInstance &e, FLOAT fMaxDuration)
{
  //CInfoF("CStatusEffectPool::AddInstance (%d - %d - %f/%f)\n", e.GetTypeId(), e.GetAmplifier(), e.GetDuration(), fMaxDuration);

  const Func_CheckTypeId<FItemInstance> checkFunc(e.GetTypeId());
  TDynamicContainer<FItemInstance> cFound;
  m_cObjects.Find(cFound, checkFunc);
  CStatusEffectInstance *pFound = nullptr;

  if (cFound.Count() > 0) {
    FOREACHINDYNAMICCONTAINER(cFound, FItemInstance, itt)
    {
      CStatusEffectInstance &sei = (CStatusEffectInstance &)itt.Current();

      if (sei.GetAmplifier() == e.GetAmplifier()) {
        pFound = &sei;
        break;
      }
    }
  }

  if (pFound) {
    pFound->SetDuration(pFound->GetDuration() + e.GetDuration());

    if (fMaxDuration > 0.0F) {
      pFound->ClampDuration(fMaxDuration);
    }

    //CInfoF("Updated effect (%d - %d)\n", pFound->GetTypeId(), pFound->GetAmplifier());
    return pFound;
  }

  FItemInstance *pNew = CreateInstance(e);
  CStatusEffectInstance &seiNewEffect = *(CStatusEffectInstance *)pNew;

  //CInfoF("Created effect (%d - %d - %f)\n", seiNewEffect.GetTypeId(), seiNewEffect.GetAmplifier(), seiNewEffect.GetDuration());

  if (fMaxDuration > 0.0F) {
    seiNewEffect.ClampDuration(fMaxDuration);
  }

  return &seiNewEffect;
}

void CStatusEffectPool::OnStep(FLOAT tmDelta)
{
  TDynamicContainer<FItemInstance> cEffectsToDelete;

  FOREACHINDYNAMICCONTAINER(m_cObjects, FItemInstance, itt)
  {
    CStatusEffectInstance &sei = (CStatusEffectInstance &)itt.Current();
    sei.SetDuration(sei.GetDuration() - tmDelta);

    if (sei.GetDuration() <= 0) {
      //CInfoF("Effect (%d - %d) expired!\n", sei.GetTypeId(), sei.GetAmplifier());
      sei.Clear();
      cEffectsToDelete.Add(&sei);
    }
  }

  FOREACHINDYNAMICCONTAINER(cEffectsToDelete, FItemInstance, itEffect)
  {
    RemoveObject(&itEffect.Current());
  }
}