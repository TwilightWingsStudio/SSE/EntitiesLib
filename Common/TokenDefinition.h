/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */


#ifndef SE_INCL_TOKENDEFINITION_H
#define SE_INCL_TOKENDEFINITION_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

enum TokenDefinitionFlags
{
  //! Thingies to see dead people.
  TDF_WEAPON = (1L << 0),

  //! Used with weapons.
  TDF_AMMO = (1L << 1),

  //! Consumable items.
  TDF_GADGET = (1L << 2),

  //! Different kinds of money.
  TDF_CURRENCY = (1L << 3),

  //! WIP content.
  TDF_NEW = (1L << 31),
};

//! Prototype for some collectible stuff.
class DECL_DLL FTokenDefinition
{
  public:
    ULONG _ulFlags;
    INDEX _iMaxQty;
    TObjectComparator<BOOL, FItemInstance> *_pMatches;

  public:
    //! Default constructor.
    inline FTokenDefinition()
    {
      _ulFlags = 0;
      _iMaxQty = 1;
      _pMatches = NULL;
    }

    //! Default constructor.
    inline FTokenDefinition(INDEX iMaxQty)
    {
      _ulFlags = 0;
      _iMaxQty = iMaxQty;
      _pMatches = NULL;
    }

    inline ULONG GetFlags() const
    {
      return _ulFlags;
    }

    inline void SetFlags(ULONG ul)
    {
      _ulFlags = ul;
    }

    inline BOOL IsNew() const
    {
      return GetFlags() & TDF_NEW;
    }

    inline INDEX GetMaxQty() const
    {
      return _iMaxQty;
    }

    inline const TObjectComparator<BOOL, FItemInstance> *GetMatchesFunc() const
    {
      return _pMatches;
    }

    inline void SetMatchesFunc(TObjectComparator<BOOL, FItemInstance> *pFunc)
    {
      _pMatches = pFunc;
    }

    static const char *GetIdentifier(AmmoIndex eAmmoIndex);
};

#endif