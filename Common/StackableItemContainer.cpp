/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

BOOL CStackableItemContainer::ReceiveItem(ULONG idItemType, INDEX iQuantity, INDEX iMaxQty)
{
  ASSERT(idItemType > 0);

  if (iQuantity <= 0) {
    return FALSE;
  }

  const Func_CheckTypeId<FItemInstance> checkFunc(idItemType);
  FItemInstance *pInstance = NULL;
  TDynamicContainer<FItemInstance> cStacksFound;

  m_cObjects.Find(cStacksFound, checkFunc);

  // If we have no stacks - make a new one.
  if (cStacksFound.Count() == 0) {
    CStackableItemInstance siiNew(idItemType, iQuantity);
    pInstance = CreateInstance(siiNew);
  } else {
    pInstance = cStacksFound.Pointer(0); // Use existing stack.
  }

  if (iMaxQty > 0 && pInstance->GetQty() >= iMaxQty) {
    return FALSE;
  }

  //CInfoF("Test %d\n", pInstance->GetQty());

  INDEX iNewQty = pInstance->GetQty() + iQuantity;
  if (iNewQty > 0) {
    iNewQty = ClampUp(iNewQty, iMaxQty);
  }

  pInstance->SetQty(iNewQty);
  return TRUE;
}

void CStackableItemContainer::DrainItem(ULONG idItemType, INDEX iQuantity)
{
  ASSERT(idItemType > 0);

  if (iQuantity <= 0) {
    return;
  }
  
  const Func_CheckTypeId<FItemInstance> checkFunc(idItemType);
  TDynamicContainer<FItemInstance> cStacksFound;
  FItemInstance *pInstance = NULL;

  m_cObjects.Find(cStacksFound, checkFunc);

  if (cStacksFound.Count() == 0) {
    return;
  }

  pInstance = cStacksFound.Pointer(0);

  INDEX iNewQty = ClampDn(pInstance->GetQty() - iQuantity, INDEX(0));

  if (iNewQty == 0) {
    RemoveObject(pInstance); // Remove item and memory.
    return;
  }

  pInstance->SetQty(iNewQty);
}