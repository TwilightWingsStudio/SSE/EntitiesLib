/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATUSEFFECTINSTANCE_H
#define SE_INCL_STATUSEFFECTINSTANCE_H

class DECL_DLL CStatusEffectInstance : public FItemInstance
{
  protected:
    UBYTE _ubAmplifier;
    INDEX _iValueA;
    INDEX _iValueB;
    FLOAT _tmDuration;
    FLOAT _tmPeriodicTimer;

  public:
    //! Default constructor.
    CStatusEffectInstance();

    //! Copy constructor.
    CStatusEffectInstance(const CStatusEffectInstance &other);
    
    //! Construtor.
    CStatusEffectInstance(ULONG idItemType, UBYTE ubAmplifier, FLOAT tmDuration);
    
    void Copy(const FItemInstance &other) override;
    void Clear();
    void Read_t(CTStream &strm);
    void Write_t(CTStream &strm);
    void SetAmplifier(UBYTE ubAmplifier);
    void SetValueA(INDEX iValue);
    void SetValueB(INDEX iValue);
    void SetDuration(FLOAT tmDuration);
    void SetPeriodicTimer(FLOAT tmPeriodicTimer);

    void ClampDuration(FLOAT tmMaxDuration);

    inline UBYTE GetAmplifier() const
    {
      return _ubAmplifier;
    }
    
    inline INDEX GetValueA() const
    {
      return _iValueA;
    }
    
    inline INDEX GetValueB() const
    {
      return _iValueB;
    }

    inline FLOAT GetDuration() const 
    {
      return _tmDuration;
    }

    inline FLOAT GetPeriodicTimer() const
    {
      return _tmPeriodicTimer;
    }

    virtual InstanceType GetInstanceType() const
    {
      return IT_STATUS;
    }
};

#endif