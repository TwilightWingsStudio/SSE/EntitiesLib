/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_TOKENREGISTRY_H
#define SE_INCL_TOKENREGISTRY_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

#include <Core/Templates/NameTable.h>

class DECL_DLL FTokenRegistrySlot
{
  public:
    CTString _strItem;
    FTokenDefinition *_pItem;

  public:
    //! Constructor.
    FTokenRegistrySlot(const CTString &strName, FTokenDefinition *pObject)
    {
      _strItem = strName;
      _pItem = pObject;
    }

    inline const CTString &GetName(void)
    {
      return _strItem;
    }

    inline FTokenDefinition *GetItem()
    {
      return _pItem;
    }
};

class DECL_DLL FTokenRegistry
{
  public:
    TDynamicContainer<FTokenDefinition> cTokens;
    TNameTable<FTokenRegistrySlot> ntTokens;

  public:
    //! Constructor
    inline FTokenRegistry()
    {
      ntTokens.SetAllocationParameters(50, 10, 10);
    }

    void Register(const CTString &strName, FTokenDefinition *pObject, ULONG ulFlags)
    {
      pObject->SetFlags(ulFlags);

      cTokens.Push() = pObject;
      ntTokens.Add(new FTokenRegistrySlot(strName, pObject));
    }
};

#endif