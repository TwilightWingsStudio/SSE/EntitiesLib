/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

#include <Core/Templates/NameTable.h>
#include "TokenRegistry.h"
#include <Core/Templates/NameTable.cpp>

class Entities_StatusEffects : public FTokenRegistry
{
  public:
    //! Constructor
    Entities_StatusEffects()
    {
      Register("invalid", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_NEUTRAL), 0);    // E_STEF_INVALID
      Register("invisibility", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_NEUTRAL), 0);    // E_STEF_INVISIBILITY
      Register("invulnerability", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_INVULNERABILITY
      Register("damage", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_DAMAGE
      Register("speed", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_SPEED
      Register("strength", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_STRENGTH
      Register("jump_boost", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_JUMP_BOOST
      Register("regeneration", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_REGENERATION
      Register("resistance", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_RESISTANCE
      Register("fire_resistance", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_FIRE_RESISTANCE
      Register("spawn_protection", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_SPAWN_PROTECTION
      Register("infinite_ammo", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_BENEFICIAL), 0); // E_STEF_INFINITE_AMMO
      Register("slowness", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_HARMFUL), 0);    // E_STEF_SLOWNESS
      Register("weakness", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_HARMFUL), 0);   // E_STEF_WEAKNESS
      Register("poison", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_HARMFUL), 0);   // E_STEF_POISON
      Register("corrosion", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_HARMFUL), 0);   // E_STEF_CORROSION
      Register("freeze", new CStatusEffectDefinition(CStatusEffectDefinition::TYPE_HARMFUL), 0);   // E_STEF_FREEZE
    }
};

Entities_StatusEffects se;

const char *GetStatusEffectNameForIndex(StatusEffectIndex eStatusEffectIndex)
{
  switch (eStatusEffectIndex)
  {
    case E_STEF_INVISIBILITY: return TRANS("^cABE3FFInvisibility"); break;
    case E_STEF_INVULNERABILITY: return TRANS("^c00B440Invulnerability"); break;
    case E_STEF_DAMAGE: return TRANS("^cFF0000Serious Damage!"); break;
    case E_STEF_SPEED: return TRANS("^cFF9400Serious Speed"); break;
    case E_STEF_STRENGTH: return TRANS("^cFF0000Strength!"); break;
    case E_STEF_JUMP_BOOST: return TRANS("^cFF9400Jump Boost"); break;
    case E_STEF_REGENERATION: return TRANS("^cE3FFE3Jump Boost"); break;
    case E_STEF_RESISTANCE: return TRANS("^cAAAAAAResistance"); break;
    case E_STEF_FIRE_RESISTANCE: return TRANS("^cFFFFAAResistance"); break;
    case E_STEF_SPAWN_PROTECTION: return TRANS("Spawn Protection"); break;
    case E_STEF_INFINITE_AMMO: return TRANS("^cABE3FFInfinite Ammo"); break;
  }

  return TRANS("<unknown powerup>");
}

const char *GetStatusEffectIdentifier(StatusEffectIndex eStatusEffectIndex)
{
  static const char *EFFECT_IDENTIFIERS[E_STEF_MAX] = {
    "invalid"           // E_STEF_INVALID
    "invisibility",     // E_STEF_INVISIBILITY
    "invulnerability",  // E_STEF_INVULNERABILITY
    "damage",           // E_STEF_DAMAGE
    "speed",            // E_STEF_SPEED
    "strength",         // E_STEF_STRENGTH
    "jump_boost",       // E_STEF_JUMP_BOOST
    "regeneration",     // E_STEF_REGENERATION
    "resistance",       // E_STEF_RESISTANCE
    "fire_resistance",  // E_STEF_FIRE_RESISTANCE
    "spawn_protection", // E_STEF_SPAWN_PROTECTION
    "infinite_ammo",    // E_STEF_INFINITE_AMMO
    "slowness",         // E_STEF_SLOWNESS
    "weakness",         // E_STEF_WEAKNESS
    "poison",           // E_STEF_POISON
    "freeze",           // E_STEF_FREEZE
  };

  if (eStatusEffectIndex > E_STEF_INVALID && eStatusEffectIndex < E_STEF_MAX) {
    return EFFECT_IDENTIFIERS[eStatusEffectIndex];
  }

  return EFFECT_IDENTIFIERS[0];
}

CStatusEffectDefinition *GetStatusEffectDefinition(StatusEffectIndex eStatusEffectIndex)
{
  if (eStatusEffectIndex > E_STEF_INVALID && eStatusEffectIndex < E_STEF_MAX) {
    return (CStatusEffectDefinition *)&se.cTokens[eStatusEffectIndex];
  }

  return (CStatusEffectDefinition *)&se.cTokens[0];
}