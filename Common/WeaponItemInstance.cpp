/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

CWeaponItemInstance::CWeaponItemInstance(ULONG idItemType)
{
  Clear();

  _idItemType = idItemType;
}

CWeaponItemInstance::CWeaponItemInstance()
{
  Clear();
}

CWeaponItemInstance::CWeaponItemInstance(const CWeaponItemInstance &other)
{
  Copy(other);
}

void CWeaponItemInstance::Copy(const FItemInstance &other)
{
  ASSERT(other.GetInstanceType() == GetInstanceType());

  const CWeaponItemInstance &mother = (CWeaponItemInstance &)other;

  _idItemType = mother.GetTypeId();
  _ulFlags = mother.GetFlags();
  _idMagAmmo = mother.GetMagAmmoId();
  _iMagAmmoQty = mother.GetMagAmmoQty();
}

void CWeaponItemInstance::Clear()
{
  _idItemType = 0;
  _ulFlags = 0;
  _idMagAmmo = 0;
  _iMagAmmoQty = 0;
}

void CWeaponItemInstance::Read_t(CTStream &strm)
{
  strm >> _idItemType;
  strm >> _ulFlags;
  strm >> _idMagAmmo;
  strm >> _iMagAmmoQty;
}

void CWeaponItemInstance::Write_t(CTStream &strm)
{
  strm << _idItemType;
  strm << _ulFlags;
  strm << _idMagAmmo;
  strm << _iMagAmmoQty;
}

void CWeaponItemInstance::SetMagAmmoId(ULONG idMagAmmo)
{
  _idMagAmmo = idMagAmmo;
}

void CWeaponItemInstance::SetMagAmmoQty(INDEX iQty)
{
  _iMagAmmoQty = iQty;
}
