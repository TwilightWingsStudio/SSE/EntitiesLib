/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

CWeaponPositionParams::CWeaponPositionParams(FLOAT3D vPos, ANGLE3D aRot, FLOAT fFieldOfView, FLOAT fFrontClipDistance, FLOAT2D vFirePos)
{
  _vPosition = vPos;
  _aOrientation = aRot;
  _fFieldOfView = fFieldOfView;
  _fFrontClipDistance = fFrontClipDistance;
  _vFirePos = vFirePos;
}

CWeaponPositionParams _aWeaponPositionParams[E_WEAPON_MAX] = {
  CWeaponPositionParams(FLOAT3D(0.0F, 0.0F, 0.0F), ANGLE3D(10.0F, 0.0F, 0.0F), 2.0F, 0, FLOAT2D(0, 0)), // E_WEAPON_NONE
  CWeaponPositionParams(FLOAT3D(0.23F, -0.28F, -0.44F), ANGLE3D(-1.0F, 6.0F, 6.0F), 41.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_KNIFE
  CWeaponPositionParams(FLOAT3D(0.125F, -0.29F, -0.405F), ANGLE3D(5.0F, 10.0F, -1.0F), 73.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_CHAINSAW
  CWeaponPositionParams(FLOAT3D(0.19F, -0.21F, -0.1F), ANGLE3D(-1.0F, 0.0F, 0.0F), 57.0F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_COLT
  CWeaponPositionParams(FLOAT3D(0.12F, -0.22F, -0.34F), ANGLE3D(2.0F, 2.0F, 0.0F), 41.0F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_SHOTGUN
  CWeaponPositionParams(FLOAT3D(0.13F, -0.21F, -0.364F), ANGLE3D(2.0F, 1.0F, 0.0F), 52.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_SUPERSHOTGUN
  CWeaponPositionParams(FLOAT3D(0.15F, -0.213F, -0.285F), ANGLE3D(4.0F, 3.0F, 0.0F), 49.0F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_MACHINEGUN
  CWeaponPositionParams(FLOAT3D(0.2F, -0.24F, -0.328F), ANGLE3D(2.0F, 0.0F, 0.0F), 66.9F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_CHAINGUN
  CWeaponPositionParams(FLOAT3D(0.2F, -0.325F, -0.24F), ANGLE3D(2.0F, 1.0F, 0.0F), 66.0F, 0.1F, FLOAT2D(-0.1f, 0.11f)), // E_WEAPON_ROCKETLAUNCHER
  CWeaponPositionParams(FLOAT3D(0.14F, -0.41F, -0.335001F), ANGLE3D(2.0F, 6.0F, 0.0F), 44.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_GRENADELAUNCHER
  CWeaponPositionParams(FLOAT3D(0.204F, -0.306F, -0.57F), ANGLE3D(4.6F, 2.8F, 0.0F), 50.0F, 0.1F, FLOAT2D(0.05f, 0)), // E_WEAPON_FLAMER
  CWeaponPositionParams(FLOAT3D(0.141F, -0.174F, -0.175F), ANGLE3D(1.0F, 3.0F, 0.0F), 70.5F, 0.1F, FLOAT2D(-0.1f, 0)), // E_WEAPON_PLASMAGUN
  CWeaponPositionParams(FLOAT3D(0.141F, -0.174F, -0.175F), ANGLE3D(1.0F, 3.0F, 0.0F), 70.5F, 0.1F, FLOAT2D(0.21f, -0.26f)), // E_WEAPON_BEAMGUN
  CWeaponPositionParams(FLOAT3D(0.095F, -0.26F, -0.85F), ANGLE3D(4.0F, 2.5F, -0.5F), 23.0F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_SNIPER
  CWeaponPositionParams(FLOAT3D(0.17F, -0.3F, -0.625F), ANGLE3D(2.5F, 6.0F, 0.0F), 50.0F, 0.1F, FLOAT2D(0.25f, -0.5f)), // E_WEAPON_CANNON
  CWeaponPositionParams(FLOAT3D(0.141F, -0.174F, -0.175F), ANGLE3D(1.0F, 3.0F, 0.0F), 70.5F, 0.1F, FLOAT2D(0.141f, -0.174f)), // E_WEAPON_PLASMATHROWER
  CWeaponPositionParams(FLOAT3D(0.14F, -0.5F, -0.8F), ANGLE3D(2.0F, 6.0F, 0.0F), 44.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_MINELAYER
};

CWeaponPositionParams _aWeaponPositionParamsDW[E_WEAPON_MAX] = {
  CWeaponPositionParams(FLOAT3D(0.0F, 0.0F, 0.0F), ANGLE3D(10.0F, 0.0F, 0.0F), 2.0F, 0, FLOAT2D(0, 0)), // E_WEAPON_NONE
  CWeaponPositionParams(FLOAT3D(0.28F, -0.28F, -0.44F), ANGLE3D(-1.0F, 6.0F, 6.0F), 41.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_KNIFE
  CWeaponPositionParams(FLOAT3D(0.22F, -0.29F, -0.405F), ANGLE3D(5.0F, 10.0F, 10.0F), 73.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_CHAINSAW
  CWeaponPositionParams(FLOAT3D(0.19F, -0.21F, -0.1F), ANGLE3D(-1.0F, 0.0F, 0.0F), 57.0F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_COLT
  CWeaponPositionParams(FLOAT3D(0.2F, -0.22F, -0.34F), ANGLE3D(2.0F, 2.0F, 0.0F), 41.0F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_SHOTGUN
  CWeaponPositionParams(FLOAT3D(0.2F, -0.21F, -0.364F), ANGLE3D(2.0F, 1.0F, 0.0F), 52.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_SUPERSHOTGUN
  CWeaponPositionParams(FLOAT3D(0.15F, -0.213F, -0.285F), ANGLE3D(4.0F, 3.0F, 0.0F), 49.0F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_MACHINEGUN
  CWeaponPositionParams(FLOAT3D(0.33F, -0.24F, -0.328F), ANGLE3D(2.0F, 0.0F, 0.0F), 66.9F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_CHAINGUN
  CWeaponPositionParams(FLOAT3D(0.35F, -0.325F, -0.24F), ANGLE3D(2.0F, 1.0F, 0.0F), 66.0F, 0.1F, FLOAT2D(-0.1f, 0.11f)), // E_WEAPON_ROCKETLAUNCHER
  CWeaponPositionParams(FLOAT3D(0.30F, -0.41F, -0.44F), ANGLE3D(2.0F, 6.0F, 0.0F), 44.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_GRENADELAUNCHER
  CWeaponPositionParams(FLOAT3D(0.204F, -0.306F, -0.57F), ANGLE3D(4.6F, 2.8F, 0.0F), 50.0F, 0.1F, FLOAT2D(0.05f, 0.03f)), // E_WEAPON_FLAMER
  CWeaponPositionParams(FLOAT3D(0.25F, -0.174F, -0.175F), ANGLE3D(1.0F, 3.0F, 0.0F), 70.5F, 0.1F, FLOAT2D(0.4f, -0.4f)), // E_WEAPON_PLASMAGUN
  CWeaponPositionParams(FLOAT3D(0.25F, -0.174F, -0.175F), ANGLE3D(1.0F, 3.0F, 0.0F), 70.5F, 0.1F, FLOAT2D(0.21f, -0.26f)), // E_WEAPON_BEAMGUN
  CWeaponPositionParams(FLOAT3D(0.175F, -0.26F, -0.85F), ANGLE3D(4.0F, 2.5F, -0.5F), 23.0F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_SNIPER
  CWeaponPositionParams(FLOAT3D(0.32F, -0.3F, -0.625F), ANGLE3D(2.5F, 6.0F, 0.0F), 50.0F, 0.1F, FLOAT2D(0.25, -0.5)), // E_WEAPON_CANNON
  CWeaponPositionParams(FLOAT3D(0.2F, -0.174F, -0.175F), ANGLE3D(1.0F, 3.0F, 0.0F), 70.5F, 0.1F, FLOAT2D(0.141f, -0.174f)), // E_WEAPON_PLASMATHROWER
  CWeaponPositionParams(FLOAT3D(0.30F, -0.5F, -0.8F), ANGLE3D(2.0F, 6.0F, 0.0F), 44.5F, 0.1F, FLOAT2D(0, 0)), // E_WEAPON_MINELAYER
};

CWeaponPositionParams *GetWeaponPositionParams(WeaponIndex eWeaponIndex, BOOL bDualWield)
{
  if (eWeaponIndex > E_WEAPON_NONE && eWeaponIndex < E_WEAPON_MAX) {
    return bDualWield ? &_aWeaponPositionParamsDW[eWeaponIndex] : &_aWeaponPositionParams[eWeaponIndex];
  }
  
  return bDualWield ? &_aWeaponPositionParamsDW[0] : &_aWeaponPositionParams[0];
}