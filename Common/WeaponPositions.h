/* Copyright (c) 2002-2012 Croteam Ltd. 
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

 
#if 0 // use this part when manually setting weapon positions
  _pShell->DeclareSymbol("persistent user FLOAT wpn_fX[30+1];",    &wpn_fX);
  _pShell->DeclareSymbol("persistent user FLOAT wpn_fY[30+1];",    &wpn_fY);
  _pShell->DeclareSymbol("persistent user FLOAT wpn_fZ[30+1];",    &wpn_fZ);
  _pShell->DeclareSymbol("persistent user FLOAT wpn_fFOV[30+1];",  &wpn_fFOV);
  _pShell->DeclareSymbol("persistent user FLOAT wpn_fFX[30+1];", &wpn_fFX);
  _pShell->DeclareSymbol("persistent user FLOAT wpn_fFY[30+1];", &wpn_fFY);
//_pShell->DeclareSymbol("persistent user FLOAT wpn_fFZ[30+1];", &wpn_fFZ);
#else
  /*
  _pShell->DeclareSymbol("user FLOAT wpn_fFX[30+1];", &wpn_fFX);
  _pShell->DeclareSymbol("user FLOAT wpn_fFY[30+1];", &wpn_fFY);
  */

#pragma warning(disable: 4305)

wpn_fX[E_WEAPON_NONE]=(FLOAT)0.08;
wpn_fX[E_WEAPON_KNIFE]=(FLOAT)0.23;
wpn_fX[E_WEAPON_CHAINSAW]=(FLOAT)0.125;
wpn_fX[E_WEAPON_REVOLVER]=(FLOAT)0.19;
wpn_fX[E_WEAPON_SHOTGUN]=(FLOAT)0.12;
wpn_fX[E_WEAPON_SUPERSHOTGUN]=(FLOAT)0.13;
wpn_fX[E_WEAPON_MACHINEGUN]=(FLOAT)0.15;//0.121;
wpn_fX[E_WEAPON_CHAINGUN]=(FLOAT)0.2;//0.137;
wpn_fX[E_WEAPON_ROCKETLAUNCHER]=(FLOAT)0.2;//0.17;
wpn_fX[E_WEAPON_GRENADELAUNCHER]=(FLOAT)0.14;
wpn_fX[E_WEAPON_FLAMER]=(FLOAT)0.204;
wpn_fX[E_WEAPON_PLASMAGUN]=(FLOAT)0.141;
wpn_fX[E_WEAPON_SNIPER]=(FLOAT)0.095;
wpn_fX[E_WEAPON_CANNON]=(FLOAT)0.17;

wpn_fY[E_WEAPON_NONE]=(FLOAT)0;
wpn_fY[E_WEAPON_KNIFE]=(FLOAT)-0.28;
wpn_fY[E_WEAPON_CHAINSAW]=(FLOAT)-0.29;
wpn_fY[E_WEAPON_REVOLVER]=(FLOAT)-0.21;
wpn_fY[E_WEAPON_SHOTGUN]=(FLOAT)-0.22;
wpn_fY[E_WEAPON_SUPERSHOTGUN]=(FLOAT)-0.21;
wpn_fY[E_WEAPON_MACHINEGUN]=(FLOAT)-0.213;
wpn_fY[E_WEAPON_CHAINGUN]=(FLOAT)-0.24;
wpn_fY[E_WEAPON_ROCKETLAUNCHER]=(FLOAT)-0.325;
wpn_fY[E_WEAPON_GRENADELAUNCHER]=(FLOAT)-0.41;
wpn_fY[E_WEAPON_FLAMER]=(FLOAT)-0.306;
wpn_fY[E_WEAPON_PLASMAGUN]=(FLOAT)-0.174;
wpn_fY[E_WEAPON_SNIPER]=(FLOAT)-0.26;
wpn_fY[E_WEAPON_CANNON]=(FLOAT)-0.3;

wpn_fZ[E_WEAPON_NONE]=(FLOAT)0;
wpn_fZ[E_WEAPON_KNIFE]=(FLOAT)-0.44;
wpn_fZ[E_WEAPON_CHAINSAW]=(FLOAT)-0.405;
wpn_fZ[E_WEAPON_REVOLVER]=(FLOAT)-0.1;
wpn_fZ[E_WEAPON_SHOTGUN]=(FLOAT)-0.34;
wpn_fZ[E_WEAPON_SUPERSHOTGUN]=(FLOAT)-0.364;
wpn_fZ[E_WEAPON_MACHINEGUN]=(FLOAT)-0.285;
wpn_fZ[E_WEAPON_CHAINGUN]=(FLOAT)-0.328;
wpn_fZ[E_WEAPON_ROCKETLAUNCHER]=(FLOAT)-0.24;
wpn_fZ[E_WEAPON_GRENADELAUNCHER]=(FLOAT)-0.335001;
wpn_fZ[E_WEAPON_FLAMER]=(FLOAT)-0.57;
wpn_fZ[E_WEAPON_PLASMAGUN]=(FLOAT)-0.175;
wpn_fZ[E_WEAPON_SNIPER]=(FLOAT)-0.85;
wpn_fZ[E_WEAPON_CANNON]=(FLOAT)-0.625;

wpn_fFOV[E_WEAPON_NONE]=(FLOAT)2;
wpn_fFOV[E_WEAPON_KNIFE]=(FLOAT)41.5;
wpn_fFOV[E_WEAPON_CHAINSAW]=(FLOAT)73.5;
wpn_fFOV[E_WEAPON_REVOLVER]=(FLOAT)57;
wpn_fFOV[E_WEAPON_SHOTGUN]=(FLOAT)41;
wpn_fFOV[E_WEAPON_SUPERSHOTGUN]=(FLOAT)52.5;
wpn_fFOV[E_WEAPON_MACHINEGUN]=(FLOAT)49;
wpn_fFOV[E_WEAPON_CHAINGUN]=(FLOAT)66.9;
wpn_fFOV[E_WEAPON_ROCKETLAUNCHER]=(FLOAT)66;
wpn_fFOV[E_WEAPON_GRENADELAUNCHER]=(FLOAT)44.5;
wpn_fFOV[E_WEAPON_FLAMER]=(FLOAT)50;
wpn_fFOV[E_WEAPON_PLASMAGUN]=(FLOAT)70.5;
wpn_fFOV[E_WEAPON_SNIPER]=(FLOAT)23;
wpn_fFOV[E_WEAPON_CANNON]=(FLOAT)50;

wpn_fFX[E_WEAPON_NONE]=(FLOAT)0;
wpn_fFX[E_WEAPON_KNIFE]=(FLOAT)0;
wpn_fFX[E_WEAPON_CHAINSAW]=(FLOAT)0;
wpn_fFX[E_WEAPON_REVOLVER]=(FLOAT)0;
wpn_fFX[E_WEAPON_SHOTGUN]=(FLOAT)0;
wpn_fFX[E_WEAPON_SUPERSHOTGUN]=(FLOAT)0;
wpn_fFX[E_WEAPON_MACHINEGUN]=(FLOAT)0;
wpn_fFX[E_WEAPON_CHAINGUN]=(FLOAT)0;
wpn_fFX[E_WEAPON_ROCKETLAUNCHER]=(FLOAT)-0.1;
wpn_fFX[E_WEAPON_GRENADELAUNCHER]=(FLOAT)0;
wpn_fFX[E_WEAPON_FLAMER]=(FLOAT)0.05;
wpn_fFX[E_WEAPON_PLASMAGUN]=(FLOAT)-0.1;
wpn_fFX[E_WEAPON_SNIPER]=(FLOAT)0;
wpn_fFX[E_WEAPON_CANNON]=(FLOAT)0.25;

wpn_fFY[E_WEAPON_NONE]=(FLOAT)0;
wpn_fFY[E_WEAPON_KNIFE]=(FLOAT)0;
wpn_fFY[E_WEAPON_CHAINSAW]=(FLOAT)0;
wpn_fFY[E_WEAPON_REVOLVER]=(FLOAT)0;
wpn_fFY[E_WEAPON_SHOTGUN]=(FLOAT)0;
wpn_fFY[E_WEAPON_SUPERSHOTGUN]=(FLOAT)0;
wpn_fFY[E_WEAPON_MACHINEGUN]=(FLOAT)0;
wpn_fFY[E_WEAPON_CHAINGUN]=(FLOAT)0;
wpn_fFY[E_WEAPON_ROCKETLAUNCHER]=(FLOAT)0.11;
wpn_fFY[E_WEAPON_GRENADELAUNCHER]=(FLOAT)0;
wpn_fFY[E_WEAPON_FLAMER]=(FLOAT)0.03;
wpn_fFY[E_WEAPON_PLASMAGUN]=(FLOAT)-0.4;
wpn_fFY[E_WEAPON_SNIPER]=(FLOAT)0;
wpn_fFY[E_WEAPON_CANNON]=(FLOAT)-0.5;

wpn_fFX[E_WEAPON_BEAMGUN] = 0.21f;
wpn_fFY[E_WEAPON_BEAMGUN] = -0.26f;

wpn_fFX[E_WEAPON_PLASMATHROWER] = 0.141F;
wpn_fFY[E_WEAPON_PLASMATHROWER] = -0.174F;

#pragma warning(default: 4305)

#endif

