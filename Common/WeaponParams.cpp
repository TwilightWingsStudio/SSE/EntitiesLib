/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

FWeaponParams::FWeaponParams(enum AmmoIndex eAmmo, INDEX iDefaultAmmoQty, INDEX iAmmoDrain, INDEX iSelectionSlot, INDEX iSelectionValue, FLOAT fMovementSpeedBonus, ULONG ulFeatureFlags)
{
  _eDefaultAmmoType = eAmmo;
  _iDefaultAmmoQty = iDefaultAmmoQty;
  _iAmmoDrain = iAmmoDrain;
  _iSelectionSlot = iSelectionSlot;
  _iSelectionValue = iSelectionValue;
  _fMovementSpeedBonus = fMovementSpeedBonus;
  _ulFeatureFlags = ulFeatureFlags;
}

const char *FWeaponParams::GetIdentifier(WeaponIndex eWeaponIndex)
{
  static const char *WEAPON_IDENTIFIERS[E_WEAPON_MAX] = {
    "none",             // E_WEAPON_NONE
    "knife",            // E_WEAPON_KNIFE
    "chainsaw",         // E_WEAPON_CHAINSAW
    "revolver",         // E_WEAPON_REVOLVER
    "single_shotgun",   // E_WEAPON_SHOTGUN
    "super_shotgun",    // E_WEAPON_SHOTGUN
    "machinegun",       // E_WEAPON_MACHINEGUN
    "chaingun",         // E_WEAPON_CHAINGUN
    "rocket_launcher",  // E_WEAPON_ROCKETLAUNCHER
    "grenade_launcher", // E_WEAPON_ROCKETLAUNCHER
    "flamer",           // E_WEAPON_FLAMER
    "plasmagun",        // E_WEAPON_PLASMAGUN
    "beamgun",          // E_WEAPON_BEAMGUN
    "sniper",           // E_WEAPON_SNIPER
    "cannon",           // E_WEAPON_CANNON
    "plasmathrower",    // E_WEAPON_PLASMATHROWER
    "minelayer",        // E_WEAPON_MINELAYER
  };
  
  if (eWeaponIndex > E_WEAPON_NONE && eWeaponIndex < E_WEAPON_MAX) {
    return WEAPON_IDENTIFIERS[eWeaponIndex];
  }

  return WEAPON_IDENTIFIERS[0];
}
