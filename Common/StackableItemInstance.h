/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STACKABLEITEMINSTANCE_H
#define SE_INCL_STACKABLEITEMINSTANCE_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class DECL_DLL CStackableItemInstance : public FItemInstance
{
  protected:
    INDEX _iQuantity;

  public:
    //! Default constructor.
    CStackableItemInstance();
    
    //! Copy constructor.
    CStackableItemInstance(const CStackableItemInstance &other);
    
    //! Constructor.
    CStackableItemInstance(ULONG idItemType, INDEX iQuantity);

    void Copy(const FItemInstance &other) override;
    void Clear();
    void Read_t(CTStream &strm);
    void Write_t(CTStream &strm);
    void SetQty(INDEX iQuantity) override;

    INDEX GetQty() const override
    {
      return _iQuantity;
    }

    virtual InstanceType GetInstanceType() const
    {
      return IT_STACKABLE;
    }
};

#endif