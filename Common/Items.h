/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ITEMS_H
#define SE_INCL_ITEMS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

enum ItemType
{
  E_ITEM_INVALID = 0,
  E_ITEM_WEAPON,
  E_ITEM_AMMO,
  E_ITEM_GADGET,
  E_ITEM_CURRENCY,
};

FTokenDefinition *GetItemDef(ItemType eStackable, ULONG idItemType);
FWeaponParams *GetWeaponParams(WeaponIndex eWeaponIndex);

#endif