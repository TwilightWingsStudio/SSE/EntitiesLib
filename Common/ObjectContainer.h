/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_OBJECTCONTAINER_H
#define SE_INCL_OBJECTCONTAINER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

template<class Type>
struct Func_CheckTypeId : TObjectFunctor<BOOL, Type>
{
  UBYTE _idObjectType;

  Func_CheckTypeId(UBYTE idObjectType)
  {
    _idObjectType = idObjectType;
  }

  UBYTE GetTypeId() const
  {
    return _idObjectType;
  }

  BOOL operator()(Type *ptObject) const override
  {
    return GetTypeId() == ULONG(-1) || GetTypeId() == ptObject->GetTypeId();
  }
};

//! Template that represents collection of objects with typeid.
template<class Type>
class TObjectContainer
{
  protected:
    TDynamicContainer<Type> m_cObjects;

  public:
    //! Get list of objects.
    inline TDynamicContainer<Type> &GetObjects()
    {
      return m_cObjects;
    }

    //! Removes specific object.
    inline void RemoveObject(Type *pObject);

    //! Remove all stacks with specified typeid.
    inline void RemoveObjects(ULONG idObjectType);

    //! Remove all objects.
    inline void ClearContent();
};

template<class Type>
void TObjectContainer<Type>::RemoveObject(Type *pObject)
{
  m_cObjects.Remove(pObject);
  delete pObject;
}

template<class Type>
void TObjectContainer<Type>::RemoveObjects(ULONG idObjectType)
{
  const Func_CheckTypeId<Type> checkFunc(idObjectType);
  TDynamicContainer<Type> cObjectsToRemove;
  m_cObjects.Find(cObjectsToRemove, checkFunc);

  FOREACHINDYNAMICCONTAINER(cObjectsToRemove, Type, itt)
  {
    m_cObjects.Remove(&itt.Current());
    delete &itt.Current();
  }
}

template<class Type>
inline void TObjectContainer<Type>::ClearContent()
{
  RemoveObjects(ULONG(-1));
}

#endif