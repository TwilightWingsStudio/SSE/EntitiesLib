/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#include "StdH.h"

CStackableItemInstance::CStackableItemInstance()
{
  Clear();
}

CStackableItemInstance::CStackableItemInstance(const CStackableItemInstance &other)
{
  Copy(other);
}

CStackableItemInstance::CStackableItemInstance(ULONG idItemType, INDEX iQuantity)
{
  _idItemType = idItemType;
  _iQuantity = iQuantity;
}

void CStackableItemInstance::Copy(const FItemInstance &other)
{
  ASSERT(other.GetInstanceType() == GetInstanceType());

  const CStackableItemInstance &mother = (CStackableItemInstance &)other;

  _idItemType = other.GetTypeId();
  _iQuantity = other.GetQty();
}

void CStackableItemInstance::Clear()
{
  _idItemType = 0;
  _iQuantity = 0;
}

void CStackableItemInstance::Read_t(CTStream &strm)
{
  strm >> _idItemType;
  strm >> _iQuantity;
}

void CStackableItemInstance::Write_t(CTStream &strm)
{
  strm << _idItemType;
  strm << _iQuantity;
}

void CStackableItemInstance::SetQty(INDEX iQuantity)
{
  _iQuantity = iQuantity;
}