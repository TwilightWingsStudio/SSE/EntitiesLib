/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_ITEMCONTAINER_H
#define SE_INCL_ITEMCONTAINER_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class CItemFactory
{
  public:
    static FItemInstance *New(UBYTE eType)
    {
      switch (eType)
      {
        case FItemInstance::IT_STACKABLE: return new CStackableItemInstance;
        case FItemInstance::IT_WEAPON: return new CWeaponItemInstance;
        case FItemInstance::IT_STATUS: return new CStatusEffectInstance;
      }

      return new FItemInstance;
    }
};

class CItemContainer : public TObjectContainer<FItemInstance>
{
  public:
    //! Default constructor.
    CItemContainer() {};

    //! Clone data from another inventory.
    inline void Copy(CItemContainer &other);

    //! Count all the itemsm for specified type id.
    inline INDEX GetItemQty(ULONG idItemType);

    //! Tries to put given item type.
    BOOL ReceiveItem(ULONG idItemType, INDEX iQuantity, INDEX iMaxQty);

    //! Try to remove item of given type.
    void DrainItem(ULONG idItemType, INDEX iQuantity);

    //! Read from stream.
    inline void Read_t(CTStream &strm);

    //! Write to stream.
    inline void Write_t(CTStream &strm);

    //! Put instance into inventory.
    inline FItemInstance *CreateInstance(FItemInstance &source)
    {
      FItemInstance *pNew = CItemFactory::New(source.GetInstanceType());
      pNew->Copy(source);
      m_cObjects.Add(pNew);
  
      return pNew;
    }

    //! Returns default instance type for this container.
    virtual UBYTE GetDefaultInstanceType() const
    {
      return FItemInstance::IT_GENERIC;
    }
};

inline void CItemContainer::Copy(CItemContainer &other)
{
  FOREACHINDYNAMICCONTAINER(other.m_cObjects, FItemInstance, itt)
  {
    FItemInstance &ii = itt.Current();
    FItemInstance *pCopy = CItemFactory::New(ii.GetInstanceType());
    pCopy->Copy(ii);
    m_cObjects.Add(pCopy);
  }
}

inline INDEX CItemContainer::GetItemQty(ULONG idItemType)
{
  //CInfoF("GetItemQty(%d)\n", idItemType);

  INDEX ctResult = 0;

  const Func_CheckTypeId<FItemInstance> checkFunc(idItemType);
  TDynamicContainer<FItemInstance> cStacksFound;
  m_cObjects.Find(cStacksFound, checkFunc);

  FOREACHINDYNAMICCONTAINER(cStacksFound, FItemInstance, itt)
  {
    ctResult += itt->GetQty();
    //CInfoF("Item %d - %d\n", itt->GetTypeId(), itt->GetQty());
  }

  return ctResult;
}

inline void CItemContainer::Read_t(CTStream &strm)
{
  strm.ExpectID_t("OBJS");

  INDEX ctObjects;
  strm >> ctObjects;

  for (int i = 0; i < ctObjects; i++)
  {
    UBYTE idInstanceType;
    strm >> idInstanceType;
    FItemInstance *pObject = CItemFactory::New(idInstanceType);
    pObject->Read_t(strm);
    m_cObjects.Add(pObject);
  }
}

inline void CItemContainer::Write_t(CTStream &strm)
{
  const INDEX ctObjects = m_cObjects.Count();

  strm.WriteID_t("OBJS");
  strm << ctObjects;
  
  FOREACHINDYNAMICCONTAINER(m_cObjects, FItemInstance, itt)
  {
    FItemInstance &ii = itt.Current();
    strm << UBYTE(ii.GetInstanceType());
    ii.Write_t(strm);
  }
}

#endif