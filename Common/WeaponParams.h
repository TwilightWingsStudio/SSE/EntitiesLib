/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_PLAYERWEAPONPARAMS_H
#define SE_INCL_PLAYERWEAPONPARAMS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

enum PlayerWeaponParamsFeatureFlags
{
  PWPFF_NONE = 0,
  PWPFF_ZOOM = (1L << 0),
  PWPFF_SOLO = (1L << 1),
};

class DECL_DLL FWeaponParams : public FTokenDefinition
{
  protected:
    enum AmmoIndex _eDefaultAmmoType;
    INDEX _iDefaultAmmoQty;
    INDEX _iAmmoDrain;
    INDEX _iSelectionSlot;
    INDEX _iSelectionValue;
    FLOAT _fMovementSpeedBonus;
    ULONG _ulFeatureFlags;

  public:
    FWeaponParams(enum AmmoIndex eAmmo, INDEX iDefaultAmmoQty, INDEX iDefaultAmmoDrain, INDEX iSelectionSlot, INDEX iSelectionValue, FLOAT fMovementSpeedBonus, ULONG ulFeatureFlags);
  
    inline AmmoIndex GetDefaultAmmoType() const
    {
      return _eDefaultAmmoType;
    }
  
    inline INDEX GetDefaultAmmoQty() const
    {
      return _iDefaultAmmoQty;
    }
    
    inline INDEX GetAmmoDrain() const
    {
      return _iAmmoDrain;
    }
    
    inline INDEX GetSelectionSlot() const
    {
      return _iSelectionSlot;
    }
    
    inline INDEX GetSelectionValue() const
    {
      return _iSelectionValue;
    }

    inline FLOAT GetMovementSpeedBonus() const
    {
      return _fMovementSpeedBonus;
    }
  
    inline BOOL IsSolo() const 
    {
      return _ulFeatureFlags & PWPFF_SOLO;
    }
    
    inline BOOL IsZooming() const
    {
      return _ulFeatureFlags & PWPFF_ZOOM;
    }

    static const char *GetIdentifier(WeaponIndex eWeaponIndex);
};

#endif