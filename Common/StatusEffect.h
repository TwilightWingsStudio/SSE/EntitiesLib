/* Copyright (c) 2021-2022 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_STATUSEFFECT_H
#define SE_INCL_STATUSEFFECT_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

enum StatusEffectIndex
{
  E_STEF_INVALID = 0,
  E_STEF_INVISIBILITY,
  E_STEF_INVULNERABILITY,
  E_STEF_DAMAGE,
  E_STEF_SPEED,
  E_STEF_STRENGTH,
  E_STEF_JUMP_BOOST,
  E_STEF_REGENERATION,
  E_STEF_RESISTANCE,
  E_STEF_FIRE_RESISTANCE,
  E_STEF_SPAWN_PROTECTION,
  E_STEF_INFINITE_AMMO,
  E_STEF_SLOWNESS,
  E_STEF_WEAKNESS,
  E_STEF_POISON,
  E_STEF_CORROSION,
  E_STEF_FREEZE,
  E_STEF_MAX,
};

DECL_DLL inline void ClearToDefault(StatusEffectIndex &e) { e = (StatusEffectIndex)0; } ;
const char *GetStatusEffectNameForIndex(StatusEffectIndex eStatusEffectIndex);
const char *GetStatusEffectIdentifier(StatusEffectIndex eStatusEffectIndex);
CStatusEffectDefinition *GetStatusEffectDefinition(StatusEffectIndex eStatusEffectIndex);

#endif