/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

#ifndef SE_INCL_WEAPONPOSITIONPARAMS_H
#define SE_INCL_WEAPONPOSITIONPARAMS_H

#ifdef PRAGMA_ONCE
  #pragma once
#endif

class DECL_DLL CWeaponPositionParams
{
  protected:
    FLOAT3D _vPosition;
    ANGLE3D _aOrientation;
    FLOAT _fFieldOfView;
    FLOAT _fFrontClipDistance;
    FLOAT2D _vFirePos;
    
  public:
    CWeaponPositionParams(FLOAT3D vPos, ANGLE3D aRot, FLOAT fFieldOfView, FLOAT fFrontClipDistance, FLOAT2D vFirePos);
  
    inline FLOAT3D GetPosition() const
    {
      return _vPosition;
    }
    
    inline ANGLE3D GetOrientation() const
    {
      return _aOrientation;
    }

    inline FLOAT GetFieldOfView() const
    {
      return _fFieldOfView;
    }
    
    inline FLOAT GetFrontClipDistance() const
    {
      return _fFrontClipDistance;
    }

    inline FLOAT2D GetFirePos() const
    {
      return _vFirePos;
    }
};

CWeaponPositionParams *GetWeaponPositionParams(WeaponIndex eWeaponIndex, BOOL bDualWield = FALSE);

#endif