/* Copyright (c) 2021-2022 by Dreamy Cecil & ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

250
%{
  #include "StdH.h"
  #include "Engine/Meshes/ModelInstance.h"

  #include "Entities/Models/ModelHolder3.h"
%}

// [SSE] This class is purely for map support from other SE1 games (primarily Nitro Family)
class CMovableSkaModelEntity : CMovableModelEntity {
name      "MovableSkaModel";
thumbnail "Thumbnails\\ModelHolder3.tbn";
features  "HasName", "HasDescription";

properties:
  1 CTFileName m_fnModel  "Model file (.smc)" 'M' = CTFILENAME(""),
  3 FLOAT m_fStretchAll   "StretchAll" 'S' = 1.0f,
  4 ANGLE3D m_vStretchXYZ "StretchXYZ" 'X' = FLOAT3D(1.0f, 1.0f, 1.0f),
  7 CTString m_strName    "Name" 'N' ="",
 12 CTString m_strDescription = "",

  8 BOOL m_bColliding  "Collision" 'L' = FALSE,
 11 enum SkaShadowType m_stClusterShadows "Shadows" 'W' = SST_CLUSTER,
 13 BOOL m_bBackground "Background" 'B' = FALSE,
 21 BOOL m_bTargetable "Targetable" = FALSE,

 // Parameters for custom shading of a model
 14 enum SkaCustomShadingType m_cstCustomShading "Shading mode" 'H' = SCST_NONE,
 15 ANGLE3D m_aShadingDirection "Shade. Light direction" 'D' = ANGLE3D(45.0f, 45.0f, 45.0f),
 16 COLOR m_colLight            "Shade. Light color" 'O' = C_WHITE,
 17 COLOR m_colAmbient          "Shade. Ambient color" 'A' = C_BLACK,

 26 BOOL m_bActive "Active" = TRUE,

 70 FLOAT m_fClassificationStretch "Classification stretch" = 1.0f,
100 FLOAT m_fMaxTessellationLevel "Max tessellation level" = 0.0f,

resources:

functions:
  // Set current model on load
  void Read_t(CTStream *istr) {
    CMovableModelEntity::Read_t(istr);

    /*if (GetModelInstance() != NULL && GetModelInstance()->mi_pInStock != NULL) {
      m_fnModel = GetModelInstance()->mi_pInStock->ser_FileName;
    }*/
  };

  // Handle properties from the original entity
  void HandleUnknownProperty(ULONG ulPropertyType, ULONG ulPropertyId, CVariant &varData) {
    // Remove entity ID
    ulPropertyId -= (0x515 << 8);

    // Remapped Nitro Family properties (CSkaMovingModelHolder)
    switch (ulPropertyId)
    {
      case 1: // 1 Model file (.smc)
        if (ulPropertyType == CEntityProperty::EPT_FILENAME) {
          CTString strPath;
          varData.ToString(strPath);

          m_fnModel = strPath;
        }
        break;

      case 6: // StretchAll
        if (ulPropertyType == CEntityProperty::EPT_FLOAT) {
          m_fStretchAll = varData.ToFloat();
        }
        break;

      case 7: // StretchXYZ
        if (ulPropertyType == CEntityProperty::EPT_ANGLE3D) {
          varData.ToVec3f(m_vStretchXYZ);
        }
        break;

      case 9: // Colliding
        if (ulPropertyType == CEntityProperty::EPT_BOOL) {
          m_bColliding = varData.ToBool();
        }
        break;

      case 11: // Shadows
        if (ulPropertyType == CEntityProperty::EPT_ENUM) {
          m_stClusterShadows = (SkaShadowType)varData.ToInt();
        }
        break;

      case 14: // Custom shading
        if (ulPropertyType == CEntityProperty::EPT_ENUM) {
          m_cstCustomShading = (SkaCustomShadingType)varData.ToInt();
        }
        break;

      case 15: // Light direction
        if (ulPropertyType == CEntityProperty::EPT_ANGLE3D) {
          varData.ToVec3f(m_aShadingDirection);
        }
        break;

      case 16: // Light color
        if (ulPropertyType == CEntityProperty::EPT_COLOR) {
          m_colLight = varData.ToInt();
        }
        break;

      case 17: // Ambient color
        if (ulPropertyType == CEntityProperty::EPT_COLOR) {
          m_colAmbient = varData.ToInt();
        }
        break;
    }
  };

  // Apply damage impact
  void DamageImpact(INDEX iDamageType, INDEX iDamageAmount, const FLOAT3D &vHitPoint, const FLOAT3D &vDirection)
  {
    // Should be at least one point
    if (iDamageAmount < HEALTH_VALUE_MULTIPLIER) {
      return;
    }

    iDamageAmount /= HEALTH_VALUE_MULTIPLIER;
    iDamageAmount = ClampDn(iDamageAmount, INDEX(0));

    FLOAT fKickDamage = iDamageAmount;

    // Explosion damage
    if (iDamageType == DMT_EXPLOSION
     || iDamageType == DMT_IMPACT
     || iDamageType == DMT_CANNONBALL_EXPLOSION) {
      fKickDamage *= 1.5f;
    }

    // Non-projectile damage
    if (iDamageType == DMT_DROWNING
     || iDamageType == DMT_CLOSERANGE
     || iDamageType == DMT_CHAINSAW) {
      fKickDamage /= 10.0f;
    }

    // Fire damage
    if (iDamageType == DMT_BURNING) {
      fKickDamage = 0.01f;
    }

    // Add new damage
    FLOAT3D vDirectionFixed;
    
    if (vDirection.ManhattanNorm() > 0.5f) {
      vDirectionFixed = vDirection;
    } else {
      vDirectionFixed = -en_vGravityDir;
    }

    FLOAT3D vDamage = (vDirectionFixed - en_vGravityDir / 2) * fKickDamage;
    FLOAT fLen = Sqrt(vDamage.Length());

    // Push it back
    GiveImpulseTranslationAbsolute(vDamage / fLen);
  }

  // Receive damage
  void ReceiveDamage(CEntity *penInflictor, INDEX iDamageType, INDEX iDamageAmount,
                     const FLOAT3D &vHitPoint, const FLOAT3D &vDirection)
  {
    // No damage
    if (iDamageAmount == 0) {
      return;
    }

    // Apply game extra damage per enemy and per player
    iDamageAmount *= GetGameDamageMultiplier();

    DamageImpact(iDamageType, iDamageAmount, vHitPoint, vDirection);

    CMovableModelEntity::ReceiveDamage(penInflictor, iDamageType, iDamageAmount, vHitPoint, vDirection);
  };

  // Classification box multiplier
  FLOAT3D GetClassificationBoxStretch(void)
  {
    return FLOAT3D(m_fClassificationStretch, m_fClassificationStretch, m_fClassificationStretch);
  };

  // Maximum allowed tessellation level for this model
  FLOAT GetMaxTessellationLevel(void)
  {
    return m_fMaxTessellationLevel;
  };

  BOOL IsTargetable(void) const
  {
    return m_bTargetable;
  };

  // Adjust model shading parameters if needed
  BOOL AdjustShadingParameters(FLOAT3D &vLightDirection, COLOR &colLight, COLOR &colAmbient)
  {
    switch (m_cstCustomShading) {
      case SCST_FULL_CUSTOMIZED: {
        colLight   = m_colLight;
        colAmbient = m_colAmbient;

        AnglesToDirectionVector(m_aShadingDirection, vLightDirection);
        vLightDirection = -vLightDirection;
        break;
      }

      case SCST_CONSTANT_SHADING: {
        // combine colors with clamp
        UBYTE lR,lG,lB,aR,aG,aB,rR,rG,rB;
        ColorToRGB(colLight,   lR, lG, lB);
        ColorToRGB(colAmbient, aR, aG, aB);
        colLight = 0;
        rR = (UBYTE) Clamp((ULONG)lR+aR, (ULONG)0, (ULONG)255);
        rG = (UBYTE) Clamp((ULONG)lG+aG, (ULONG)0, (ULONG)255);
        rB = (UBYTE) Clamp((ULONG)lB+aB, (ULONG)0, (ULONG)255);
        colAmbient = RGBToColor(rR, rG, rB);
        break;
      }
    }

    return m_stClusterShadows != SST_NONE;
  };

  // Apply mirroring and stretching
  void MirrorAndStretch(FLOAT fStretch, BOOL bMirrorX) {
    m_fStretchAll *= fStretch;

    if (bMirrorX) {
      m_vStretchXYZ(1) = -m_vStretchXYZ(1);
    }
  };

  // Stretch model
  void StretchModel(void)
  {
    // stretch factors must not have extreme values
    if (Abs(m_vStretchXYZ(1))  < 0.01f) { m_vStretchXYZ(1)   = 0.01f;  }
    if (Abs(m_vStretchXYZ(2))  < 0.01f) { m_vStretchXYZ(2)   = 0.01f;  }
    if (Abs(m_vStretchXYZ(3))  < 0.01f) { m_vStretchXYZ(3)   = 0.01f;  }
    if (m_fStretchAll< 0.01f) { m_fStretchAll = 0.01f;  }

    if (Abs(m_vStretchXYZ(1))  >1000.0f) { m_vStretchXYZ(1)   = 1000.0f*Sgn(m_vStretchXYZ(1)); }
    if (Abs(m_vStretchXYZ(2))  >1000.0f) { m_vStretchXYZ(2)   = 1000.0f*Sgn(m_vStretchXYZ(2)); }
    if (Abs(m_vStretchXYZ(3))  >1000.0f) { m_vStretchXYZ(3)   = 1000.0f*Sgn(m_vStretchXYZ(3)); }
    if (m_fStretchAll>1000.0f) { m_fStretchAll = 1000.0f; }

    GetModelInstance()->StretchModel(m_vStretchXYZ*m_fStretchAll );
    ModelChangeNotify();
  };

  // Initialize model holder
  void InitModelHolder(void) {
    // Must not crash when model is removed
    if (m_fnModel == "") {
      m_fnModel = CTFILENAME("Models\\Editor\\Ska\\Axis.smc");
    }

    if (m_bActive) {
      InitAsSkaModel();
    } else {
      InitAsSkaEditorModel();
    }

    // Load the model
    try {
      SetSkaModel_t(m_fnModel);

    // Failed
    } catch (char *strError) {
      WarningMessage(TRANS("Cannot load ska model '%s':\n%s"), (CTString&)m_fnModel, strError);

      // Set default model
      SetSkaModel(CTFILENAME("Models\\Editor\\Ska\\Axis.smc"));
    }

    // Apply stretching
    StretchModel();
    ModelChangeNotify();

    // Collide if active
    if (m_bColliding && m_bActive) {
      SetPhysicsFlags(EPF_MODEL_WALKING);
      SetCollisionFlags(ECF_CORPSE_SOLID);

    } else {
      SetPhysicsFlags(EPF_MODEL_IMMATERIAL);
      SetCollisionFlags(ECF_IMMATERIAL);
    }

    // Enable cluster shadows
    if (m_stClusterShadows == SST_CLUSTER) {
      SetFlags(GetFlags() | ENF_CLUSTERSHADOWS);
    } else {
      SetFlags(GetFlags() & ~ENF_CLUSTERSHADOWS);
    }

    // Background model
    if (m_bBackground) {
      SetFlags(GetFlags() | ENF_BACKGROUND);
    } else {
      SetFlags(GetFlags() & ~ENF_BACKGROUND);
    }

    m_strDescription.PrintF("%s", (CTString&)m_fnModel.FileName());

    return;
  };

procedures:
  // Entry point
  Main()
  {
    InitModelHolder();
    AddToMovers();

    // Wait for events
    wait() {
      on (EBegin) : {
        resume;
      }

      // Show model
      on (EActivate) : {
        SwitchToModel();
        m_bActive = TRUE;

        if (m_bColliding) {
          SetPhysicsFlags(EPF_MODEL_WALKING);
          SetCollisionFlags(ECF_CORPSE_SOLID);
        }

        resume;
      }

      // Hide model
      on (EDeactivate) : {
        SwitchToEditorModel();
        SetPhysicsFlags(EPF_MODEL_IMMATERIAL);
        SetCollisionFlags(ECF_IMMATERIAL);
        ForceFullStop();

        m_bActive = FALSE;
        resume;
      }

      // When the parent is destroyed
      on (ERangeModelDestruction) : {
        // Destroy children
        FOREACHINLIST(CEntity, n_lnInParent, GetChildren(), itenChild)
        {
          itenChild->SendEvent(ERangeModelDestruction());
        }

        // And then self-destruct
        Destroy();
        
        resume;
      }

      // Ignore other events
      otherwise() : {
        resume;
      }
    };
  }
};
