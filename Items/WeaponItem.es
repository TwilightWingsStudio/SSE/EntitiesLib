/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

802
%{
  #include "StdH.h"

  #include "Models/Items/ItemHolder/ItemHolder.h"
  #include "Models/Weapons/Colt/ColtItem.h"
  #include "Models/Weapons/SingleShotgun/SingleShotgunItem.h"
  #include "Models/Weapons/DoubleShotgun/DoubleShotgunItem.h"
  #include "Models/Weapons/TommyGun/TommyGunItem.h"
  #include "Models/Weapons/MiniGun/MiniGunItem.h"
  #include "Models/Weapons/GrenadeLauncher/GrenadeLauncherItem.h"
  #include "Models/Weapons/RocketLauncher/RocketLauncherItem.h"
  #include "ModelsMP/Weapons/Sniper/SniperItem.h"
  #include "ModelsMP/Weapons/Sniper/Body.h"
  #include "ModelsMP/Weapons/Flamer/FlamerItem.h"
  #include "ModelsMP/Weapons/Chainsaw/ChainsawItem.h"
  #include "ModelsMP/Weapons/Chainsaw/BladeForPlayer.h"
  #include "Models/Weapons/Laser/LaserItem.h"
  #include "Models/Weapons/Cannon/Cannon.h"
  #include "Models/Weapons/Beamgun/BeamgunItem.h"

  #include "Entities/Players/PlayerWeapon.h"
%}

uses "Entities/Items/Item";

// weapon type
enum WeaponItemType
{
  0 WIT_KNIFE             "Knife",
  1 WIT_COLT              "Colt",
  2 WIT_SINGLESHOTGUN     "Single shotgun",
  3 WIT_DOUBLESHOTGUN     "Double shotgun",
  4 WIT_TOMMYGUN          "Tommygun",
  5 WIT_MINIGUN           "Minigun",
  6 WIT_ROCKETLAUNCHER    "Rocket launcher",
  7 WIT_GRENADELAUNCHER   "Grenade launcher",
  8 WIT_SNIPER            "Sniper",
  9 WIT_FLAMER            "Flamer",
 10 WIT_LASER             "Laser",
 11 WIT_CHAINSAW          "Chainsaw",
 12 WIT_CANNON            "Cannon",
 13 WIT_BEAMGUN           "Beamgun",
 14 WIT_PLASMATHROWER     "Plasmathrower",
 15 WIT_MINELAYER         "Minelayer",
};

// event for sending through receive item
event EWeaponPickup
{
  WeaponIndex eWeaponIndex,   // weapon collected
  INDEX  iAmmo,     // weapon ammo (used only for leaving weapons, -1 for deafult ammount)
  BOOL bDropped,    // for dropped weapons (can be picked even if weapons stay)
};

%{
  extern void CPlayerWeaponEntity_Precache(ULONG ulAvailable);

  FLOAT GetRespawnTimeForWeaponItem(enum WeaponItemType eType)
  {
    switch (eType)
    {
      case WIT_KNIFE: return 10.0F;
      case WIT_COLT: return 10.0F;
      case WIT_SINGLESHOTGUN: return 25.0F;
      case WIT_DOUBLESHOTGUN: return 60.0F;
      case WIT_TOMMYGUN: return 120.0F;
      case WIT_MINIGUN: return 10.0F;
      case WIT_ROCKETLAUNCHER: return 10.0F;
      case WIT_GRENADELAUNCHER: return 10.0F;
      case WIT_SNIPER: return 10.0F;
      case WIT_FLAMER: return 10.0F;
      case WIT_LASER: return 10.0F;
      case WIT_CHAINSAW: return 10.0F;
      case WIT_CANNON: return 30.0F;
      case WIT_BEAMGUN: return 15.0F;
      case WIT_PLASMATHROWER: return 15.0F;
      case WIT_MINELAYER: return 15.0F;
    }

    return 60.0F;
  }

  WeaponIndex GetWeaponIndexForItem(WeaponItemType eType)
  {
    switch (eType)
    {
      case WIT_KNIFE: return E_WEAPON_KNIFE;
      case WIT_COLT: return E_WEAPON_REVOLVER;
      case WIT_SINGLESHOTGUN: return E_WEAPON_SHOTGUN;
      case WIT_DOUBLESHOTGUN: return E_WEAPON_SUPERSHOTGUN;
      case WIT_TOMMYGUN: return E_WEAPON_MACHINEGUN;
      case WIT_MINIGUN: return E_WEAPON_CHAINGUN;
      case WIT_ROCKETLAUNCHER: return E_WEAPON_ROCKETLAUNCHER;
      case WIT_GRENADELAUNCHER: return E_WEAPON_GRENADELAUNCHER;
      case WIT_SNIPER: return E_WEAPON_SNIPER;
      case WIT_FLAMER: return E_WEAPON_FLAMER;
      case WIT_LASER: return E_WEAPON_PLASMAGUN;
      case WIT_CHAINSAW: return E_WEAPON_CHAINSAW;
      case WIT_CANNON: return E_WEAPON_CANNON;
      case WIT_BEAMGUN: return E_WEAPON_BEAMGUN;
      case WIT_PLASMATHROWER: return E_WEAPON_PLASMATHROWER;
      case WIT_MINELAYER: return E_WEAPON_MINELAYER;
    }

    return E_WEAPON_NONE;
  }

  const char *GetWeaponNameForIndex(WeaponIndex eWeaponIndex)
  {
    switch (eWeaponIndex)
    {
      case E_WEAPON_KNIFE: return TRANS("Military Knife"); break;
      case E_WEAPON_REVOLVER: return TRANS("Shofield .45 w/ TMAR"); break;
      case E_WEAPON_CHAINSAW: return TRANS("'Bonecracker' P-LAH Chainsaw"); break;
      case E_WEAPON_SHOTGUN: return TRANS("12 Gauge Pump Action Shotgun"); break;
      case E_WEAPON_SUPERSHOTGUN: return TRANS("Double Barrel Coach Gun"); break;
      case E_WEAPON_MACHINEGUN: return TRANS("M1-A2 Tommygun"); break;
      case E_WEAPON_CHAINGUN: return TRANS("XM214-A Minigun"); break;
      case E_WEAPON_ROCKETLAUNCHER: return TRANS("XPML21 Rocket Launcher"); break;
      case E_WEAPON_GRENADELAUNCHER: return TRANS("MKIII Grenade Launcher"); break;
      case E_WEAPON_FLAMER: return TRANS("XOP Flamethrower"); break;
      case E_WEAPON_PLASMAGUN: return TRANS("XL2 Lasergun"); break;
      case E_WEAPON_SNIPER: return TRANS("RAPTOR 16mm Sniper"); break;
      case E_WEAPON_CANNON: return TRANS("SBC Cannon"); break;
      case E_WEAPON_BEAMGUN: return TRANS("XL4-P Beam Gun"); break;
      case E_WEAPON_PLASMATHROWER: return TRANS("CDF-ST-005 Plasmathrower"); break;
      case E_WEAPON_MINELAYER: return TRANS("MKII-B Minelayer"); break;
    }

    return TRANS("<unknown weapon>");
  }
%}

class CWeaponItemEntity : CItemEntity {
name      "Weapon Item";
thumbnail "Thumbnails\\WeaponItem.tbn";

properties:

  1 enum WeaponItemType m_EwitType    "Type" 'Y' = WIT_COLT,     // weapon

resources:

  0 class   CLASS_BASE        "Classes\\Item.ecl",

 20 modelcfg MODEL_KNIFE_CFG           "Models\\Weapons\\Knife\\Knife_Item.vmc",
 21 modelcfg MODEL_COLT_CFG            "Models\\Weapons\\Colt\\Colt_Item.vmc",
 22 modelcfg MODEL_SINGLESHOTGUN_CFG   "Models\\Weapons\\SingleShotgun\\SingleShotgun_Item.vmc",
 23 modelcfg MODEL_DOUBLESHOTGUN_CFG   "Models\\Weapons\\DoubleShotgun\\DoubleShotgun_Item.vmc",
 24 modelcfg MODEL_TOMMYGUN_CFG        "Models\\Weapons\\TommyGun\\TommyGun_Item.vmc",
 25 modelcfg MODEL_MINIGUN_CFG         "Models\\Weapons\\MiniGun\\MiniGun_Item.vmc",
 26 modelcfg MODEL_ROCKETLAUNCHER_CFG  "Models\\Weapons\\RocketLauncher\\RocketLauncher_Item.vmc",
 27 modelcfg MODEL_GRENADELAUNCHER_CFG "Models\\Weapons\\GrenadeLauncher\\GrenadeLauncher_Item.vmc",
 28 modelcfg MODEL_SNIPER_CFG          "Models\\Weapons\\Sniper\\Sniper_Item.vmc",
 29 modelcfg MODEL_FLAMER_CFG          "Models\\Weapons\\Flamer\\Flamer_Item.vmc",
 30 modelcfg MODEL_LASER_CFG           "Models\\Weapons\\Laser\\Laser_Item.vmc",
 31 modelcfg MODEL_CHAINSAW_CFG        "Models\\Weapons\\Chainsaw\\Chainsaw_Item.vmc",
 32 modelcfg MODEL_CANNON_CFG          "Models\\Weapons\\Cannon\\Cannon_Item.vmc",
 33 modelcfg MODEL_BEAMGUN_CFG         "Models\\Weapons\\Beamgun\\Beamgun_Item.vmc",
 34 modelcfg MODEL_PLASMATHROWER_CFG   "Models\\Weapons\\Plasmathrower\\Plasmathrower_Item.vmc",
 35 modelcfg MODEL_MINELAYER_CFG       "Models\\Weapons\\Minelayer\\Minelayer_Item.vmc",

// ************** FLARE FOR EFFECT **************
190 texture TEXTURE_FLARE "Models\\Items\\Flares\\Flare.tex",
191 model   MODEL_FLARE "Models\\Items\\Flares\\Flare.mdl",

// ************** SOUNDS **************
213 sound SOUND_PICK             "Sounds\\Items\\Weapon.wav",

functions:

  INDEX ModelForItemType(WeaponItemType eItemType) const
  {
    switch (eItemType)
    {
      case WIT_KNIFE: return MODEL_KNIFE_CFG;
      case WIT_COLT: return MODEL_COLT_CFG;
      case WIT_SINGLESHOTGUN: return MODEL_SINGLESHOTGUN_CFG;
      case WIT_DOUBLESHOTGUN: return MODEL_DOUBLESHOTGUN_CFG;
      case WIT_TOMMYGUN: return MODEL_TOMMYGUN_CFG;
      case WIT_MINIGUN: return MODEL_MINIGUN_CFG;
      case WIT_ROCKETLAUNCHER: return MODEL_ROCKETLAUNCHER_CFG;
      case WIT_GRENADELAUNCHER: return MODEL_GRENADELAUNCHER_CFG;
      case WIT_SNIPER: return MODEL_SNIPER_CFG;
      case WIT_FLAMER: return MODEL_FLAMER_CFG;
      case WIT_LASER: return MODEL_LASER_CFG;
      case WIT_CHAINSAW: return MODEL_CHAINSAW_CFG;
      case WIT_CANNON: return MODEL_CANNON_CFG;
      case WIT_BEAMGUN: return MODEL_BEAMGUN_CFG;
      case WIT_PLASMATHROWER: return MODEL_PLASMATHROWER_CFG;
      case WIT_MINELAYER: return MODEL_MINELAYER_CFG;
    }

    return -1;
  }

  void Precache(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_EwitType);

    if (idModelConfig != -1) {
      PrecacheModelConfig(idModelConfig);
    }

    PrecacheSound(SOUND_PICK);

    switch (m_EwitType)
    {
      case WIT_KNIFE:           CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_KNIFE          )-1)); break;
      case WIT_COLT:            CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_REVOLVER           )-1)); break;
      case WIT_SINGLESHOTGUN:   CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_SHOTGUN  )-1)); break;
      case WIT_DOUBLESHOTGUN:   CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_SUPERSHOTGUN  )-1)); break;
      case WIT_TOMMYGUN:        CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_MACHINEGUN       )-1)); break;
      case WIT_MINIGUN:         CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_CHAINGUN        )-1)); break;
      case WIT_ROCKETLAUNCHER:  CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_ROCKETLAUNCHER )-1)); break;
      case WIT_GRENADELAUNCHER: CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_GRENADELAUNCHER)-1)); break;
      case WIT_SNIPER:          CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_SNIPER         )-1)); break;
      case WIT_FLAMER:          CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_FLAMER         )-1)); break;
      case WIT_CHAINSAW:        CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_CHAINSAW       )-1)); break;
      case WIT_LASER:           CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_PLASMAGUN      )-1)); break;
      case WIT_CANNON:          CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_CANNON         )-1)); break;
      case WIT_BEAMGUN:         CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_BEAMGUN        )-1)); break;
      case WIT_PLASMATHROWER:   CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_PLASMATHROWER  )-1)); break;
      case WIT_MINELAYER:       CPlayerWeaponEntity_Precache(1<<(INDEX(E_WEAPON_MINELAYER      )-1)); break;
    }
  }
  /* Fill in entity statistics - for AI purposes only */
  BOOL FillEntityStatistics(EntityStats *pes)
  {
    pes->es_strName = m_strDescription;
    pes->es_ctCount = 1;
    pes->es_ctAmmount = 1;
    pes->es_fValue = 1;
    pes->es_iScore = 0;//m_iScore;
    return TRUE;
  }

  // render particles
  void RenderParticles(void)
  {
    BOOL bIsModel = GetRenderType() == CEntity::RT_MODEL;
    BOOL bIsNotCoop = GetSP()->sp_gmGameMode > CSessionProperties::GM_COOPERATIVE;

    // no particles when not existing or in DM modes
    if (!bIsModel || bIsNotCoop || !ShowItemParticles()) {
      return;
    }

    Particles_Atomic(this, 1.5f, 1.5f, PT_STAR07, 12);
  }

  // set weapon properties depending on weapon type
  void SetProperties(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_EwitType);

    m_fRespawnTime = (m_fCustomRespawnTime > 0) ? m_fCustomRespawnTime : GetRespawnTimeForWeaponItem(m_EwitType);

    BOOL bDM = FALSE;//m_bRespawn || m_bDropped;
    FLOAT3D vDMStretch = FLOAT3D(2.0f, 2.0f, 2.0f);
    FLOAT3D vItemStretch(1.0F, 1.0F, 1.0F);

    m_strDescription.PrintF("%s", FWeaponParams::GetIdentifier(GetWeaponIndexForItem(m_EwitType)));

    switch (m_EwitType)
    {
      case WIT_KNIFE: {
        vItemStretch = FLOAT3D(4.5F, 4.5F, 4.5F);
      } break;

      case WIT_COLT: {
        vItemStretch = FLOAT3D(4.5f, 4.5f, 4.5f);
      } break;

      case WIT_SINGLESHOTGUN: {
        vItemStretch = FLOAT3D(3.5f, 3.5f, 3.5f);
      } break;

      case WIT_DOUBLESHOTGUN: {
        vItemStretch = FLOAT3D(3.0f, 3.0f, 3.0f);
      } break;

      case WIT_TOMMYGUN: {
        vItemStretch = FLOAT3D(3.0f, 3.0f, 3.0f);
      } break;

      case WIT_MINIGUN: {
        vItemStretch = FLOAT3D(1.75f, 1.75f, 1.75f);
      } break;

      case WIT_ROCKETLAUNCHER: {
        vItemStretch = FLOAT3D(2.5f, 2.5f, 2.5f);
      } break;

      case WIT_GRENADELAUNCHER: {
        vItemStretch = FLOAT3D(2.5f, 2.5f, 2.5f);
      } break;

      case WIT_SNIPER: {
        vItemStretch = FLOAT3D(3.0f, 3.0f, 3.0f);
      } break;

      case WIT_FLAMER: {
        vItemStretch = FLOAT3D(2.5f, 2.5f, 2.5f);
      } break;

      case WIT_CHAINSAW: {
        vItemStretch = FLOAT3D(2.0f, 2.0f, 2.0f);
      } break;

      case WIT_LASER: {
        vItemStretch = FLOAT3D(2.5f, 2.5f, 2.5f);
      } break;

      case WIT_CANNON: {
        vItemStretch = FLOAT3D(3.0f, 3.0f, 3.0f);
      } break;

      case WIT_BEAMGUN: {
        vItemStretch = FLOAT3D(3.0f, 3.0f, 3.0f);
      } break;

      case WIT_PLASMATHROWER: {
        vItemStretch = FLOAT3D(2.5f, 2.5f, 2.5f);
      } break;

      case WIT_MINELAYER: {
        vItemStretch = FLOAT3D(2.5f, 2.5f, 2.5f);
      } break;
    }

    if (idModelConfig != -1) {
      AddVtxChild(this, this, idModelConfig, "Item", ITEMHOLDER_ATTACHMENT_ITEM);
    }

    StretchItem(bDM ? vDMStretch : vItemStretch);

    // add flare
    AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0, 0.6f, 0), FLOAT3D(3, 3, 0.3f));
};

  void AdjustDifficulty(void)
  {
    BOOL bAllowedByRules = GetSP()->sp_ulAllowedItemTypes & AIF_WEAPONS;

    if (!bAllowedByRules && m_penTarget == NULL) {
      Destroy();
    }
  }

procedures:

  ItemCollected(EPass epass) : CItemEntity::ItemCollected
  {
    ASSERT(epass.penOther!=NULL);

    BOOL bItemsStay = GetSP()->sp_ulCoopFlags & CPF_WEAPONS_STAY;

    // if weapons stays
    if (bItemsStay && !(m_bPickupOnce || m_bRespawn)) {
      // if already picked by this player
      BOOL bWasPicked = MarkPickedBy(epass.penOther);

      if (bWasPicked) {
        // don't pick again
        return;
      }
    }

    // send weapon to entity
    EWeaponPickup eWeapon;
    eWeapon.eWeaponIndex = GetWeaponIndexForItem(m_EwitType);
    eWeapon.iAmmo = -1; // use default ammo amount
    eWeapon.bDropped = m_bDropped;

    // if weapon is received
    if (epass.penOther->ReceiveItem(eWeapon)) {
      if (_pNetwork->IsPlayerLocal(epass.penOther)) {IFeel_PlayEffect("PU_Weapon");}

      // play the pickup sound
      m_soPick.Set3DParameters(50.0f, 1.0f, 1.0f, 1.0f);
      PlaySound(m_soPick, SOUND_PICK, SOF_3D);
      m_fPickSoundLen = GetSoundLength(SOUND_PICK);

      if (!bItemsStay || m_bDropped || (m_bPickupOnce || m_bRespawn)) {
        jump CItemEntity::ItemReceived();
      }
    }

    return;
  };

  Main()
  {
    Initialize();     // initialize base class
    StartModelAnim(ITEMHOLDER_ANIM_BIGOSCILATION, AOF_LOOPING|AOF_NORESTART);
    ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_BIG);
    SetProperties();  // set properties

    if (!m_bDropped) {
      jump CItemEntity::ItemLoop();
    } else if (TRUE) {
      wait()
      {
        on (EBegin) : {
          SpawnReminder(this, m_fRespawnTime, 0);
          call CItemEntity::ItemLoop();
        }

        on (EReminder) : {
          SendEvent(EEnd());
          resume;
        }
      }
    }
  };
};
