/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

805
%{
  #include "StdH.h"

  #include "Models/Items/ItemHolder/ItemHolder.h"
%}

uses "Entities/Items/Item";

// key type
enum KeyItemType
{
  0 KIT_BOOKOFWISDOM      "Book of wisdom",
  1 KIT_CROSSWOODEN       "Wooden cross",
  2 KIT_CROSSMETAL        "Silver cross",
  3 KIT_CROSSGOLD         "Gold cross",
  4 KIT_JAGUARGOLDDUMMY   "Gold jaguar",
  5 KIT_HAWKWINGS01DUMMY  "Hawk wings - part 1",
  6 KIT_HAWKWINGS02DUMMY  "Hawk wings - part 2",
  7 KIT_HOLYGRAIL         "Holy grail",
  8 KIT_TABLESDUMMY       "Tablet of wisdom",
  9 KIT_WINGEDLION        "Winged lion",
 10 KIT_ELEPHANTGOLD      "Gold elephant",
 11 KIT_STATUEHEAD01      "Seriously scary ceremonial mask",
 12 KIT_STATUEHEAD02      "Hilariously happy ceremonial mask",
 13 KIT_STATUEHEAD03      "Ix Chel mask",
 14 KIT_KINGSTATUE        "Statue of King Tilmun",
 15 KIT_CRYSTALSKULL      "Crystal Skull",
 
 // TFE
 16 KIT_ANKHWOOD          "Wooden ankh",
 17 KIT_ANKHROCK          "Stone ankh",
 18 KIT_ANKHGOLD          "Gold ankh",
 19 KIT_AMONGOLD          "Gold amon",
 20 KIT_ANKHGOLDDUMMY     "Gold ankh (dummy)",
 21 KIT_ELEMENTEARTH      "Element - Earth",
 22 KIT_ELEMENTWATER      "Element - Water",
 23 KIT_ELEMENTAIR        "Element - Air",
 24 KIT_ELEMENTFIRE       "Element - Fire",
 25 KIT_RAKEY             "Ra Key",
 26 KIT_MOONKEY           "Moon Key",
 27 KIT_EYEOFRA           "Eye of Ra",
 28 KIT_SCARAB            "Scarab",
 29 KIT_COBRA             "Cobra",
 30 KIT_SCARABDUMMY       "Scarab (dummy)",
 31 KIT_HEART             "Gold Heart",
 32 KIT_FEATHER           "Feather of Truth (dummy)",
 33 KIT_SPHINX1           "Sphinx 1 (dummy)",
 34 KIT_SPHINX2           "Sphinx 2 (dummy)",
};

// event for sending through receive item
event EKeyPickup
{
  enum KeyItemType kitType,
};

%{

  const char *GetKeyName(enum KeyItemType kit)
  {
    switch (kit)
    {
      case KIT_BOOKOFWISDOM     :  return TRANS("Book of wisdom"); break;
      case KIT_CROSSWOODEN      :  return TRANS("Wooden cross"); break;
      case KIT_CROSSGOLD        :  return TRANS("Gold cross"); break;
      case KIT_CROSSMETAL       :  return TRANS("Silver cross"); break;
      case KIT_JAGUARGOLDDUMMY  :  return TRANS("Gold jaguar"); break;
      case KIT_HAWKWINGS01DUMMY :  return TRANS("Hawk wings - part 1"); break;
      case KIT_HAWKWINGS02DUMMY :  return TRANS("Hawk wings - part 2"); break;
      case KIT_HOLYGRAIL        :  return TRANS("Holy grail"); break;
      case KIT_TABLESDUMMY      :  return TRANS("Tablet of wisdom"); break;
      case KIT_WINGEDLION       :  return TRANS("Winged lion"); break;
      case KIT_ELEPHANTGOLD     :  return TRANS("Gold elephant"); break;
      case KIT_STATUEHEAD01     :  return TRANS("Seriously scary ceremonial mask"); break;
      case KIT_STATUEHEAD02     :  return TRANS("Hilariously happy ceremonial mask"); break;
      case KIT_STATUEHEAD03     :  return TRANS("Ix Chel mask"); break;
      case KIT_KINGSTATUE       :  return TRANS("Statue of King Tilmun"); break;
      case KIT_CRYSTALSKULL     :  return TRANS("Crystal Skull"); break;
      
      // TFE
      case KIT_ANKHWOOD         :  return TRANS("Wooden ankh"); break;
      case KIT_ANKHROCK         :  return TRANS("Stone ankh"); break;
      case KIT_ANKHGOLD         :
      case KIT_ANKHGOLDDUMMY    :  return TRANS("Gold ankh"); break;
      case KIT_AMONGOLD         :  return TRANS("Gold Amon statue"); break;
      case KIT_ELEMENTEARTH     :  return TRANS("Earth element"); break;
      case KIT_ELEMENTWATER     :  return TRANS("Water element"); break;
      case KIT_ELEMENTAIR       :  return TRANS("Air element"); break;
      case KIT_ELEMENTFIRE      :  return TRANS("Fire element"); break;
      case KIT_RAKEY            :  return TRANS("Ra key"); break;
      case KIT_MOONKEY          :  return TRANS("Moon key"); break;
      case KIT_EYEOFRA          :  return TRANS("Eye of Ra"); break;
      case KIT_SCARAB           :
      case KIT_SCARABDUMMY      :  return TRANS("Scarab"); break;
      case KIT_COBRA            :  return TRANS("Cobra"); break;
      case KIT_HEART            :  return TRANS("Gold Heart"); break;
      case KIT_FEATHER          :  return TRANS("Feather of Truth"); break;
      case KIT_SPHINX1          :
      case KIT_SPHINX2          :  return TRANS("Gold Sphinx"); break;
      default: return TRANS("unknown item"); break;
    };
  }

%}

class CKeyItemEntity : CItemEntity {
name      "KeyItem";
thumbnail "Thumbnails\\KeyItem.tbn";
features  "IsImportant";

properties:

  1 enum KeyItemType m_kitType    "Type" 'Y' = KIT_BOOKOFWISDOM, // key type
  3 INDEX m_iSoundComponent = 0,
  5 FLOAT m_fSize "Size" = 1.0f,

resources:

  0 class   CLASS_BASE        "Classes\\Item.ecl",

// ********* TSE KEYS *********
 20 modelcfg MODEL_BOOKOFWISDOM_CFG "Models\\Items\\Keys\\BookOfWisdom\\Book_Item.vmc",
 21 modelcfg MODEL_CROSSWOODEN_CFG  "Models\\Items\\Keys\\Cross\\CrossWood_Item.vmc",
 22 modelcfg MODEL_CROSSGOLD_CFG    "Models\\Items\\Keys\\GoldCross\\Cross_Item.vmc",
 23 modelcfg MODEL_CROSSMETAL_CFG   "Models\\Items\\Keys\\Cross\\CrossMetal_Item.vmc",
 24 modelcfg MODEL_JAGUARGOLD_CFG   "Models\\Items\\Keys\\GoldJaguar\\Jaguar_Item.vmc",
 25 modelcfg MODEL_HAWKWINGS01_CFG  "Models\\Items\\Keys\\HawkWings\\WingLeft_Item.vmc",
 26 modelcfg MODEL_HAWKWINGS02_CFG  "Models\\Items\\Keys\\HawkWings\\WingRight_Item.vmc",
 27 modelcfg MODEL_HOLYGRAIL_CFG    "Models\\Items\\Keys\\HolyGrail\\Grail_Item.vmc",
 28 modelcfg MODEL_TABLES_CFG       "Models\\Items\\Keys\\TablesOfWisdom\\Tables_Item.vmc",
 29 modelcfg MODEL_WINGEDLION_CFG   "Models\\Items\\Keys\\WingLion\\WingLion_Item.vmc",
 31 modelcfg MODEL_ELEPHANTGOLD_CFG "Models\\Items\\Keys\\GoldElephant\\Elephant_Item.vmc",
 32 modelcfg MODEL_STATUEHEAD01_CFG "Models\\Items\\Keys\\Statue01\\Statue_Item.vmc",
 33 modelcfg MODEL_STATUEHEAD02_CFG "Models\\Items\\Keys\\Statue02\\Statue_Item.vmc",
 34 modelcfg MODEL_STATUEHEAD03_CFG "Models\\Items\\Keys\\Statue03\\Statue_Item.vmc",
 35 modelcfg MODEL_KINGSTATUE_CFG   "Models\\Items\\Keys\\ManStatue\\ManStatue_Item.vmc",
 36 modelcfg MODEL_CRYSTALSKULL_CFG "Models\\Items\\Keys\\CrystalSkull\\CrystalSkull_Item.vmc",

// ********* TFE KEYS *********
 50 modelcfg MODEL_ANKHWOOD_CFG         "Models\\Items\\Keys\\Ankh\\AnkhWood_Item.vmc",
 51 modelcfg MODEL_ANKHROCK_CFG         "Models\\Items\\Keys\\Ankh\\AnkhStone_Item.vmc",
 52 modelcfg MODEL_ANKHGOLD_CFG         "Models\\Items\\Keys\\Ankh\\AnkhGold_Item.vmc",
 53 modelcfg MODEL_AMONGOLD_CFG         "Models\\Items\\Keys\\Amon\\Amon_Item.vmc",
 54 modelcfg MODEL_ELEMENTEARTH_CFG     "Models\\Items\\Keys\\Elements\\Earth_Item.vmc",
 55 modelcfg MODEL_ELEMENTWATER_CFG     "Models\\Items\\Keys\\Elements\\Water_Item.vmc",
 56 modelcfg MODEL_ELEMENTAIR_CFG       "Models\\Items\\Keys\\Elements\\Air_Item.vmc",
 57 modelcfg MODEL_ELEMENTFIRE_CFG      "Models\\Items\\Keys\\Elements\\Fire_Item.vmc",
 58 modelcfg MODEL_RAKEY_CFG            "Models\\Items\\Keys\\RaKey\\RaKey_Item.vmc",
 59 modelcfg MODEL_MOONKEY_CFG          "Models\\Items\\Keys\\MoonSymbol\\MoonSymbol_Item.vmc",
 60 modelcfg MODEL_EYEOFRA_CFG          "Models\\Items\\Keys\\EyeOfRa\\EyeOfRa_Item.vmc",
 61 modelcfg MODEL_SCARAB_CFG           "Models\\Items\\Keys\\Scarab\\Scarab_Item.vmc",
 62 modelcfg MODEL_COBRA_CFG            "Models\\Items\\Keys\\Uaset\\Uaset_Item.vmc",
 63 modelcfg MODEL_HEART_CFG            "Models\\Items\\Keys\\Luxor\\GoldHeart_Item.vmc",
 64 modelcfg MODEL_FEATHER_CFG          "Models\\Items\\Keys\\Luxor\\FeatherOfTruth_Item.vmc",
 65 modelcfg MODEL_SPHINX_CFG           "Models\\Items\\Keys\\GoldSphinx\\GoldSphinx_Item.vmc",

// ********* MISC *********
250 texture TEXTURE_FLARE       "ModelsMP\\Items\\Flares\\Flare.tex",
251 model   MODEL_FLARE         "ModelsMP\\Items\\Flares\\Flare.mdl",

// ************** SOUNDS **************
300 sound   SOUND_KEY         "Sounds\\Items\\Key.wav",

functions:

  INDEX ModelForKeyItemType(KeyItemType eItemType) const
  {
    switch (eItemType)
    {
      // TSE
      case KIT_BOOKOFWISDOM     :  return MODEL_BOOKOFWISDOM_CFG;
      case KIT_CROSSWOODEN      :  return MODEL_CROSSWOODEN_CFG;
      case KIT_CROSSGOLD        :  return MODEL_CROSSGOLD_CFG;
      case KIT_CROSSMETAL       :  return MODEL_CROSSMETAL_CFG;
      case KIT_JAGUARGOLDDUMMY  :  return MODEL_JAGUARGOLD_CFG;
      case KIT_HAWKWINGS01DUMMY :  return MODEL_HAWKWINGS01_CFG;
      case KIT_HAWKWINGS02DUMMY :  return MODEL_HAWKWINGS02_CFG;
      case KIT_HOLYGRAIL        :  return MODEL_HOLYGRAIL_CFG;
      case KIT_TABLESDUMMY      :  return MODEL_TABLES_CFG;
      case KIT_WINGEDLION       :  return MODEL_WINGEDLION_CFG;
      case KIT_ELEPHANTGOLD     :  return MODEL_ELEPHANTGOLD_CFG;
      case KIT_STATUEHEAD01     :  return MODEL_STATUEHEAD01_CFG;
      case KIT_STATUEHEAD02     :  return MODEL_STATUEHEAD02_CFG;
      case KIT_STATUEHEAD03     :  return MODEL_STATUEHEAD03_CFG;
      case KIT_KINGSTATUE       :  return MODEL_KINGSTATUE_CFG;
      case KIT_CRYSTALSKULL     :  return MODEL_CRYSTALSKULL_CFG;

      // TFE
      case KIT_ANKHWOOD         :  return MODEL_ANKHWOOD_CFG;
      case KIT_ANKHROCK         :  return MODEL_ANKHROCK_CFG;
      case KIT_ANKHGOLD         :
      case KIT_ANKHGOLDDUMMY    :  return MODEL_ANKHGOLD_CFG;
      case KIT_AMONGOLD         :  return MODEL_AMONGOLD_CFG;
      case KIT_ELEMENTEARTH     :  return MODEL_ELEMENTEARTH_CFG;
      case KIT_ELEMENTWATER     :  return MODEL_ELEMENTWATER_CFG;
      case KIT_ELEMENTAIR       :  return MODEL_ELEMENTAIR_CFG;
      case KIT_ELEMENTFIRE      :  return MODEL_ELEMENTFIRE_CFG;
      case KIT_RAKEY            :  return MODEL_RAKEY_CFG;
      case KIT_MOONKEY          :  return MODEL_MOONKEY_CFG;
      case KIT_EYEOFRA          :  return MODEL_EYEOFRA_CFG;
      case KIT_SCARAB           :
      case KIT_SCARABDUMMY      :  return MODEL_SCARAB_CFG;
      case KIT_COBRA            :  return MODEL_COBRA_CFG;
      case KIT_HEART            :  return MODEL_HEART_CFG;
      case KIT_FEATHER          :  return MODEL_FEATHER_CFG;
      case KIT_SPHINX1          :
      case KIT_SPHINX2          :  return MODEL_SPHINX_CFG;
    }

    return -1;
  }

  void Precache(void)
  {
    const INDEX idModelConfig = ModelForKeyItemType(m_kitType);

    if (idModelConfig != -1) {
      PrecacheModelConfig(idModelConfig);
    }

    PrecacheSound(SOUND_KEY);
  }

  /* Fill in entity statistics - for AI purposes only */
  BOOL FillEntityStatistics(EntityStats *pes)
  {
    pes->es_strName = GetKeyName(m_kitType);
    pes->es_ctCount = 1;
    pes->es_ctAmmount = 1;
    pes->es_fValue = 1;
    pes->es_iScore = 0;//m_iScore;
    return TRUE;
  }

  // render particles
  void RenderParticles(void)
  {
    BOOL bIsModel = GetRenderType() == CEntity::RT_MODEL;

    // no particles when not existing
    if (bIsModel || !ShowItemParticles()) {
      return;
    }

    switch (m_kitType)
    {
      // TFE
      case KIT_ANKHWOOD:
      case KIT_ANKHROCK:
      case KIT_ANKHGOLD:
      case KIT_ANKHGOLDDUMMY:
        Particles_Stardust(this, 0.9f, 0.70f, PT_STAR08, 32);
        break;
      case KIT_AMONGOLD:
        Particles_Stardust(this, 1.6f, 1.00f, PT_STAR08, 32);
        break; 
      case KIT_ELEMENTEARTH:
      case KIT_ELEMENTWATER:
      case KIT_ELEMENTAIR:
      case KIT_ELEMENTFIRE:
      case KIT_RAKEY:
      case KIT_MOONKEY:
      case KIT_EYEOFRA:
      case KIT_SCARAB:
      case KIT_SCARABDUMMY:
      case KIT_COBRA:
      case KIT_HEART:
      case KIT_FEATHER:
      case KIT_SPHINX1:
      case KIT_SPHINX2:
        Particles_Stardust(this, 0.9f, 0.70f, PT_STAR08, 32);
        break;

      // TSE
      case KIT_BOOKOFWISDOM    :
      case KIT_CRYSTALSKULL    :
      case KIT_HOLYGRAIL       :
        Particles_Stardust(this, 1.0f, 0.5f, PT_STAR08, 64);
        break;
      case KIT_JAGUARGOLDDUMMY :
        Particles_Stardust(this, 2.0f, 2.0f, PT_STAR08, 64);
        break;
      case KIT_CROSSWOODEN     :
      case KIT_CROSSMETAL      :
      case KIT_CROSSGOLD       :
      case KIT_HAWKWINGS01DUMMY:
      case KIT_HAWKWINGS02DUMMY:
      case KIT_TABLESDUMMY     :
      case KIT_WINGEDLION      :
      case KIT_ELEPHANTGOLD    :
      case KIT_STATUEHEAD01    :
      case KIT_STATUEHEAD02    :
      case KIT_STATUEHEAD03    :
      case KIT_KINGSTATUE      :
      default:
        Particles_Stardust(this, 1.5f, 1.1f, PT_STAR08, 64);
        break;
    }
  }



  // set health properties depending on type
  void SetProperties(void)
  {
    const INDEX idModelConfig = ModelForKeyItemType(m_kitType);

    m_fRespawnTime = (m_fCustomRespawnTime > 0) ? m_fCustomRespawnTime : 10.0f;
    m_strDescription = GetKeyName(m_kitType);
    FLOAT3D vItemStretch(1.0F, 1.0F, 1.0F);
    m_iSoundComponent = SOUND_KEY;

    switch (m_kitType)
    {
      // TFE
      case KIT_ANKHWOOD: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_ANKHROCK: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_ANKHGOLD:
      case KIT_ANKHGOLDDUMMY: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_AMONGOLD: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.5f,0), FLOAT3D(2,2,0.3f) );
        vItemStretch = FLOAT3D(2.0f, 2.0f, 2.0f);
      } break;

      case KIT_ELEMENTEARTH: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;
        
      case KIT_ELEMENTWATER: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_ELEMENTAIR: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_ELEMENTFIRE: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_RAKEY: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_MOONKEY: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_EYEOFRA: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_SCARAB:
      case KIT_SCARABDUMMY: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_COBRA: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;
        
      case KIT_HEART: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_FEATHER: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;
        
      case KIT_SPHINX1:
      case KIT_SPHINX2: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
        vItemStretch = FLOAT3D(2.0f, 2.0f, 2.0f);
      } break;
      
      // TSE
      case KIT_BOOKOFWISDOM: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_CROSSWOODEN: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_CROSSMETAL: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_CROSSGOLD: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_JAGUARGOLDDUMMY: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.5f,0), FLOAT3D(2,2,0.3f) );
      } break;

      case KIT_HAWKWINGS01DUMMY: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_HAWKWINGS02DUMMY: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_HOLYGRAIL: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_TABLESDUMMY: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_WINGEDLION: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_ELEPHANTGOLD: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.5f,0), FLOAT3D(2,2,0.3f) );
      } break;

      case KIT_STATUEHEAD01: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_STATUEHEAD02: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_STATUEHEAD03: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_KINGSTATUE: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;

      case KIT_CRYSTALSKULL: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
      } break;
    }

    if (idModelConfig != -1) {
      AddVtxChild(this, this, idModelConfig, "Item", ITEMHOLDER_ATTACHMENT_ITEM);
    }

    StretchItem(vItemStretch);
    GetModelObject()->StretchModel(FLOAT3D(m_fSize, m_fSize, m_fSize));
  };

procedures:

  ItemCollected(EPass epass) : CItemEntity::ItemCollected
  {
    ASSERT(epass.penOther!=NULL);

    // send key to entity
    EKeyPickup eKey;
    eKey.kitType = m_kitType;

    // if health is received
    if (epass.penOther->ReceiveItem(eKey))
    {
      if (_pNetwork->IsPlayerLocal(epass.penOther)) {IFeel_PlayEffect("PU_Key");}

      // play the pickup sound
      m_soPick.Set3DParameters(50.0f, 1.0f, 1.0f, 1.0f);
      PlaySound(m_soPick, m_iSoundComponent, SOF_3D);
      m_fPickSoundLen = GetSoundLength(m_iSoundComponent);
      jump CItemEntity::ItemReceived();
    }

    return;
  };

  Main()
  {
    Initialize();     // initialize base class
    StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
    ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_BIG);
    SetProperties();  // set properties

    jump CItemEntity::ItemLoop();
  };
};
