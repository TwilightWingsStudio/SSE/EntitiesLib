/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

801
%{
  #include "StdH.h"

  #include "Models/Items/ItemHolder/ItemHolder.h"
%}

uses "Entities/Items/Item";

// health type
enum HealthItemType
{
  0 HIT_PILL      "Pill",       // pill health
  1 HIT_SMALL     "Small",      // small health
  2 HIT_MEDIUM    "Medium",     // medium health
  3 HIT_LARGE     "Large",      // large health
  4 HIT_SUPER     "Super",      // super health
  5 HIT_MEGA      "Mega",
};

// event for sending through receive item
event EHealthPickup
{
  INDEX iQuantity,      // health to receive
  BOOL bOverTopHealth,  // can be received over top health
};

%{
  INDEX GetQuantityForHealthItem(enum HealthItemType eType)
  {
    switch (eType)
    {
      case HIT_PILL: return 1;
      case HIT_SMALL: return 10;
      case HIT_MEDIUM: return 25;
      case HIT_LARGE: return 50;
      case HIT_SUPER: return 100;
      case HIT_MEGA: return 200;
    }

    return 1;
  }

  FLOAT GetRespawnTimeForHealthItem(enum HealthItemType eType)
  {
    switch (eType)
    {
      case HIT_PILL: return 10.0F;
      case HIT_SMALL: return 10.0F;
      case HIT_MEDIUM: return 25.0F;
      case HIT_LARGE: return 60.0F;
      case HIT_SUPER: return 120.0F;
      case HIT_MEGA: return 180.0F;
    }

    return 60.0F;
  }
%}

class CHealthItemEntity : CItemEntity {
name      "Health Item";
thumbnail "Thumbnails\\HealthItem.tbn";

properties:

  1 enum HealthItemType m_EhitType    "Type" 'Y' = HIT_SMALL,     // health type
  2 BOOL m_bOverTopHealth             = FALSE,  // can be received over top health
  3 INDEX m_iSoundComponent = 0,

resources:

  0 class   CLASS_BASE        "Classes\\Item.ecl",

 20 modelcfg MODEL_PILL_CFG      "Models\\Items\\Health\\Pill\\Pill_Item.vmc",
 21 modelcfg MODEL_SMALL_CFG     "Models\\Items\\Health\\Small\\Small_Item.vmc",
 22 modelcfg MODEL_MEDIUM_CFG    "Models\\Items\\Health\\Medium\\Medium_Item.vmc",
 23 modelcfg MODEL_LARGE_CFG     "Models\\Items\\Health\\Large\\Large_Item.vmc",
 24 modelcfg MODEL_SUPER_CFG     "Models\\Items\\Health\\Super\\Super_Item.vmc",
 25 modelcfg MODEL_MEGA_CFG      "Models\\Items\\Health\\Mega\\Mega_Item.vmc",

// ********* MISC *********
 55 texture TEXTURE_FLARE "Models\\Items\\Flares\\Flare.tex",
 56 model   MODEL_FLARE "Models\\Items\\Flares\\Flare.mdl",

// ************** SOUNDS **************
301 sound   SOUND_PILL         "Sounds\\Items\\HealthPill.wav",
302 sound   SOUND_SMALL        "Sounds\\Items\\HealthSmall.wav",
303 sound   SOUND_MEDIUM       "Sounds\\Items\\HealthMedium.wav",
304 sound   SOUND_LARGE        "Sounds\\Items\\HealthLarge.wav",
305 sound   SOUND_SUPER        "Sounds\\Items\\HealthSuper.wav",

functions:

  INDEX ModelForItemType(HealthItemType eItemType) const
  {
    switch (eItemType)
    {
      case HIT_PILL:   return MODEL_PILL_CFG;
      case HIT_SMALL:  return MODEL_SMALL_CFG;
      case HIT_MEDIUM: return MODEL_MEDIUM_CFG;
      case HIT_LARGE:  return MODEL_LARGE_CFG;
      case HIT_SUPER:  return MODEL_SUPER_CFG;
      case HIT_MEGA:   return MODEL_MEGA_CFG;
    }

    return -1;
  }

  void Precache(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_EhitType);

    if (idModelConfig != -1) {
      PrecacheModelConfig(idModelConfig);
    }

    switch (m_EhitType)
    {
      case HIT_PILL:   PrecacheSound(SOUND_PILL  ); break;
      case HIT_SMALL:  PrecacheSound(SOUND_SMALL ); break;
      case HIT_MEDIUM: PrecacheSound(SOUND_MEDIUM); break;
      case HIT_LARGE:  PrecacheSound(SOUND_LARGE ); break;
      case HIT_SUPER:  PrecacheSound(SOUND_SUPER ); break;
      case HIT_MEGA:   PrecacheSound(SOUND_SUPER ); break;
    }
  }

  /* Fill in entity statistics - for AI purposes only */
  BOOL FillEntityStatistics(EntityStats *pes)
  {
    pes->es_strName = "Health";
    pes->es_ctCount = 1;
    pes->es_ctAmmount = m_fValue;
    pes->es_fValue = m_fValue;
    pes->es_iScore = 0;//m_iScore;

    switch (m_EhitType)
    {
      case HIT_PILL:  pes->es_strName+=" pill";   break;
      case HIT_SMALL: pes->es_strName+=" small";  break;
      case HIT_MEDIUM:pes->es_strName+=" medium"; break;
      case HIT_LARGE: pes->es_strName+=" large";  break;
      case HIT_SUPER: pes->es_strName+=" super";  break;
      case HIT_MEGA:  pes->es_strName+=" mega";  break;
    }

    return TRUE;
  }

  // render particles
  void RenderParticles(void)
  {
    BOOL bIsModel = GetRenderType() == CEntity::RT_MODEL;
    BOOL bIsNotCoop = GetSP()->sp_gmGameMode > CSessionProperties::GM_COOPERATIVE;

    // no particles when not existing or in DM modes
    if (!bIsModel || bIsNotCoop || !ShowItemParticles()) {
      return;
    }

    switch (m_EhitType)
    {
      case HIT_PILL: {
        Particles_Stardust(this, 0.9f*0.75f, 0.70f*0.75f, PT_STAR08, 32);
      } break;

      case HIT_SMALL: {
        Particles_Stardust(this, 1.0f*0.75f, 0.75f*0.75f, PT_STAR08, 128);
      } break;

      case HIT_MEDIUM: {
        Particles_Stardust(this, 1.0f*0.75f, 0.75f*0.75f, PT_STAR08, 128);
      } break;

      case HIT_LARGE: {
        Particles_Stardust(this, 2.0f*0.75f, 1.0f*0.75f, PT_STAR08, 192);
      } break;

      case HIT_SUPER: {
        Particles_Stardust(this, 2.3f*0.75f, 1.5f*0.75f, PT_STAR08, 320);
      } break;

      case HIT_MEGA: {
        Particles_Stardust(this, 2.3f*0.75f, 1.5f*0.75f, PT_STAR08, 320);
      } break;
    }
  }

  // set health properties depending on health type
  void SetProperties(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_EhitType);

    m_fValue = GetQuantityForHealthItem(m_EhitType);
    m_fRespawnTime = (m_fCustomRespawnTime > 0) ? m_fCustomRespawnTime : GetRespawnTimeForHealthItem(m_EhitType);

    FLOAT3D vItemStretch(1.0F, 1.0F, 1.0F);

    switch (m_EhitType)
    {
      case HIT_PILL: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_SMALL);
        m_bOverTopHealth = TRUE;
        m_strDescription.PrintF("Pill - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );
        vItemStretch *= 0.75F;
        m_iSoundComponent = SOUND_PILL;
      } break;

      case HIT_SMALL: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_bOverTopHealth = FALSE;
        m_strDescription.PrintF("Small - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        //AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.4f,0), FLOAT3D(2,2,0.4f) );
        vItemStretch *= 0.75F;
        m_iSoundComponent = SOUND_SMALL;
      } break;

      case HIT_MEDIUM: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_bOverTopHealth = FALSE;
        m_strDescription.PrintF("Medium - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.6f,0), FLOAT3D(2.5f,2.5f,0.5f) );
        vItemStretch = FLOAT3D(1.5f * 0.75f, 1.5f * 0.75f, 1.5f * 0.75f);
        m_iSoundComponent = SOUND_MEDIUM;
      } break;

      case HIT_LARGE: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_bOverTopHealth = FALSE;
        m_strDescription.PrintF("Large - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.8f,0), FLOAT3D(2.8f,2.8f,1.0f) );
        vItemStretch = FLOAT3D(1.2f * 0.75f, 1.2f * 0.75f, 1.2f * 0.75f);
        m_iSoundComponent = SOUND_LARGE;
      } break;

      case HIT_SUPER: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_bOverTopHealth = TRUE;
        m_strDescription.PrintF("Super - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,1.0f,0), FLOAT3D(3,3,1.0f) );
        vItemStretch *= 0.75F;
        m_iSoundComponent = SOUND_SUPER;
      } break;

      case HIT_MEGA: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_bOverTopHealth = TRUE;
        m_strDescription.PrintF("Mega - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,1.0f,0), FLOAT3D(3,3,1.0f) );
        vItemStretch *= 0.75F;
        m_iSoundComponent = SOUND_SUPER;
      } break;
    }

    if (idModelConfig != -1) {
      AddVtxChild(this, this, idModelConfig, "Item", ITEMHOLDER_ATTACHMENT_ITEM);
    }

    StretchItem(vItemStretch);
  };

  void AdjustDifficulty(void)
  {
    BOOL bAllowedByRules = GetSP()->sp_ulAllowedItemTypes & AIF_HEALTH;

    if (!bAllowedByRules && m_penTarget == NULL) {
      Destroy();
    }
  }

procedures:

  ItemCollected(EPass epass) : CItemEntity::ItemCollected
  {
    ASSERT(epass.penOther!=NULL);

    BOOL bItemsStay = GetSP()->sp_ulCoopFlags & CPF_HEALTH_STAYS;

    // if health stays
    if (bItemsStay && !(m_bPickupOnce || m_bRespawn)) {
      // if already picked by this player
      BOOL bWasPicked = MarkPickedBy(epass.penOther);

      if (bWasPicked) {
        return; // don't pick again
      }
    }

    // send health to entity
    EHealthPickup eHealth;
    eHealth.iQuantity = m_fValue;
    eHealth.bOverTopHealth = m_bOverTopHealth;

    // if health is received
    if (epass.penOther->ReceiveItem(eHealth)) {

      if (_pNetwork->IsPlayerLocal(epass.penOther))
      {
        switch (m_EhitType)
        {
          case HIT_PILL:  IFeel_PlayEffect("PU_HealthPill"); break;
          case HIT_SMALL: IFeel_PlayEffect("PU_HealthSmall"); break;
          case HIT_MEDIUM:IFeel_PlayEffect("PU_HealthMedium"); break;
          case HIT_LARGE: IFeel_PlayEffect("PU_HealthLarge"); break;
          case HIT_SUPER: IFeel_PlayEffect("PU_HealthSuper"); break;
        }
      }

      // play the pickup sound
      m_soPick.Set3DParameters(50.0f, 1.0f, 1.0f, 1.0f);
      PlaySound(m_soPick, m_iSoundComponent, SOF_3D);
      m_fPickSoundLen = GetSoundLength(m_iSoundComponent);

      if (!bItemsStay || (m_bPickupOnce || m_bRespawn)) {
        jump CItemEntity::ItemReceived();
      }
    }

    return;
  };

  Main()
  {
    Initialize();     // initialize base class
    SetProperties();  // set properties

    jump CItemEntity::ItemLoop();
  };
};
