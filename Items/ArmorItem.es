/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

804
%{
  #include "StdH.h"

  #include "Models/Items/ItemHolder/ItemHolder.h"
%}

uses "Entities/Items/Item";

// health type
enum ArmorItemType
{
  0 ARIT_SHARD        "Shard",    // shard
  1 ARIT_SMALL        "Small",    // small armor
  2 ARIT_MEDIUM       "Medium",   // medium armor
  3 ARIT_STRONG       "Strong",   // strong armor
  4 ARIT_SUPER        "Super",    // super armor
  5 ARIT_HELM         "Helm",     // helm
};

// event for sending through receive item
event EArmorPickup
{
  INDEX iQuantity,         // armor to receive
  BOOL bOverTopArmor,   // can be received over top armor
};

%{
  INDEX GetQuantityForArmorItem(enum ArmorItemType eType)
  {
    switch (eType)
    {
      case ARIT_SHARD: return 1;
      case ARIT_SMALL: return 25;
      case ARIT_MEDIUM: return 50;
      case ARIT_STRONG: return 100;
      case ARIT_SUPER: return 200;
      case ARIT_HELM: return 5;
    }

    return 1;
  }

  FLOAT GetRespawnTimeForArmorItem(enum ArmorItemType eType)
  {
    switch (eType)
    {
      case ARIT_SHARD: return 10.0F;
      case ARIT_SMALL: return 10.0F;
      case ARIT_MEDIUM: return 25.0F;
      case ARIT_STRONG: return 60.0F;
      case ARIT_SUPER: return 120.0F;
      case ARIT_HELM: return 10.0F;
    }

    return 60.0F;
  }
%}

class CArmorItemEntity : CItemEntity {
name      "Armor Item";
thumbnail "Thumbnails\\ArmorItem.tbn";

properties:

  1 enum ArmorItemType m_EaitType     "Type" 'Y' = ARIT_SHARD,    // armor type
  2 BOOL m_bOverTopArmor  = FALSE,   // can be received over top armor
  3 INDEX m_iSoundComponent = 0,

resources:

  0 class   CLASS_BASE        "Classes\\Item.ecl",

 20 modelcfg MODEL_SHARD_CFG    "Models\\Items\\Armor\\Armor_1_Item.vmc",
 21 modelcfg MODEL_SMALL_CFG    "Models\\Items\\Armor\\Armor_25_Item.vmc",
 22 modelcfg MODEL_MEDIUM_CFG   "Models\\Items\\Armor\\Armor_50_Item.vmc",
 23 modelcfg MODEL_STRONG_CFG   "Models\\Items\\Armor\\Armor_100_Item.vmc",
 24 modelcfg MODEL_SUPER_CFG    "Models\\Items\\Armor\\Armor_200_Item.vmc",
 25 modelcfg MODEL_HELM_CFG     "Models\\Items\\Armor\\Armor_5_Item.vmc",

// ************** FLARE FOR EFFECT **************
100 texture TEXTURE_FLARE  "Models\\Items\\Flares\\Flare.tex",
101 model   MODEL_FLARE    "Models\\Items\\Flares\\Flare.mdl",

// ************** REFLECTIONS **************
200 texture TEX_REFL_LIGHTMETAL01       "Models\\ReflectionTextures\\LightMetal01.tex",

// ************** SPECULAR **************
210 texture TEX_SPEC_MEDIUM             "Models\\SpecularTextures\\Medium.tex",

// ************** SOUNDS **************
301 sound   SOUND_SHARD        "Sounds\\Items\\ArmourShard.wav",
302 sound   SOUND_SMALL        "Sounds\\Items\\ArmourSmall.wav",
303 sound   SOUND_MEDIUM       "Sounds\\Items\\ArmourMedium.wav",
304 sound   SOUND_STRONG       "Sounds\\Items\\ArmourStrong.wav",
305 sound   SOUND_SUPER        "Sounds\\Items\\ArmourSuper.wav",
306 sound   SOUND_HELM         "SoundsMP\\Items\\ArmourHelm.wav",

functions:

  INDEX ModelForItemType(ArmorItemType eItemType) const
  {
    switch (eItemType)
    {
      case ARIT_SHARD:  return MODEL_SHARD_CFG;
      case ARIT_SMALL:  return MODEL_SMALL_CFG;
      case ARIT_MEDIUM: return MODEL_MEDIUM_CFG;
      case ARIT_STRONG: return MODEL_STRONG_CFG;
      case ARIT_SUPER:  return MODEL_SUPER_CFG;
      case ARIT_HELM:   return MODEL_HELM_CFG;
    }

    return -1;
  }

  void Precache(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_EaitType);

    if (idModelConfig != -1) {
      PrecacheModelConfig(idModelConfig);
    }

    switch (m_EaitType)
    {
      case ARIT_SHARD:  PrecacheSound(SOUND_SHARD); break;
      case ARIT_SMALL:  PrecacheSound(SOUND_SMALL); break;
      case ARIT_MEDIUM: PrecacheSound(SOUND_MEDIUM); break;
      case ARIT_STRONG: PrecacheSound(SOUND_STRONG); break;
      case ARIT_SUPER:  PrecacheSound(SOUND_SUPER); break;
      case ARIT_HELM:   PrecacheSound(SOUND_HELM); break;
    }
  }

  /* Fill in entity statistics - for AI purposes only */
  BOOL FillEntityStatistics(EntityStats *pes)
  {
    pes->es_strName = "Armor";
    pes->es_ctCount = 1;
    pes->es_ctAmmount = m_fValue;
    pes->es_fValue = m_fValue * 2;
    pes->es_iScore = 0;//m_iScore;

    switch (m_EaitType)
    {
      case ARIT_SHARD:  pes->es_strName+=" shard";  break;
      case ARIT_SMALL:  pes->es_strName+=" small";  break;
      case ARIT_MEDIUM: pes->es_strName+=" medium"; break;
      case ARIT_STRONG: pes->es_strName+=" strong"; break;
      case ARIT_SUPER:  pes->es_strName+=" super";  break;
      case ARIT_HELM:   pes->es_strName+=" helm";   break;
    }

    return TRUE;
  }

  // render particles
  void RenderParticles(void)
  {
    BOOL bIsModel = GetRenderType() == CEntity::RT_MODEL;
    BOOL bIsNotCoop = GetSP()->sp_gmGameMode > CSessionProperties::GM_COOPERATIVE;

    // no particles when not existing or in DM modes
    if (!bIsModel || bIsNotCoop || !ShowItemParticles()) {
      return;
    }

    switch (m_EaitType)
    {
      case ARIT_SHARD: {
        Particles_Emanate(this, 0.75f*0.75, 0.75f*0.75, PT_STAR04, 8, 7.0f);
      } break;

      case ARIT_SMALL: {
        Particles_Emanate(this, 1.0f*0.75, 1.0f*0.75, PT_STAR04, 32, 7.0f);
      } break;

      case ARIT_MEDIUM: {
        Particles_Emanate(this, 1.5f*0.75, 1.5f*0.75, PT_STAR04, 64, 7.0f);
      } break;

      case ARIT_STRONG: {
        Particles_Emanate(this, 2.0f*0.75, 1.25f*0.75, PT_STAR04, 96, 7.0f);
      } break;

      case ARIT_SUPER: {
        Particles_Emanate(this, 2.5f*0.75, 1.5f*0.75, PT_STAR04, 128, 7.0f);
      } break;

      case ARIT_HELM: {
        Particles_Emanate(this, 0.875f*0.75, 0.875f*0.75, PT_STAR04, 16, 7.0f);
      } break;
    }
  }

  // set health properties depending on health type
  void SetProperties(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_EaitType);

    m_fValue = GetQuantityForArmorItem(m_EaitType);
    m_fRespawnTime = (m_fCustomRespawnTime > 0) ? m_fCustomRespawnTime : GetRespawnTimeForArmorItem(m_EaitType);

    FLOAT3D vItemStretch(1.0F, 1.0F, 1.0F);

    switch (m_EaitType)
    {
      case ARIT_SHARD: {
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_SMALL);
        m_bOverTopArmor = TRUE;
        m_strDescription.PrintF("Shard - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.4f,0), FLOAT3D(1.0,1.0,0.3f) );
        vItemStretch = FLOAT3D(0.75f * 0.75f, 0.75f * 0.75f, 0.75f * 0.75f);
        m_iSoundComponent = SOUND_SHARD;
      } break;

      case ARIT_SMALL: {
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_bOverTopArmor = FALSE;
        m_strDescription.PrintF("Small - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.6f,0), FLOAT3D(2,2,0.5f) );
        vItemStretch = FLOAT3D(2.0f, 2.0f, 2.0f);
        m_iSoundComponent = SOUND_SMALL;
      } break;

      case ARIT_MEDIUM: {
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_bOverTopArmor = FALSE;
        m_strDescription.PrintF("Medium - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,1.0f,0), FLOAT3D(3,3,0.5f) );
        vItemStretch = FLOAT3D(2.0f, 2.0f, 2.0f);
        m_iSoundComponent = SOUND_MEDIUM;
      } break;

      case ARIT_STRONG: {
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_bOverTopArmor = FALSE;
        m_strDescription.PrintF("Strong - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(3.5,3.5,1.0f) );
        vItemStretch = FLOAT3D(2.5f, 2.5f, 2.5f);
        m_iSoundComponent = SOUND_STRONG;
      } break;

      case ARIT_SUPER: {
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_bOverTopArmor = TRUE;
        m_strDescription.PrintF("Super - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(3,3,1.0f) );
        vItemStretch = FLOAT3D(2.5f, 2.5f, 2.5f);
        m_iSoundComponent = SOUND_SUPER;
      } break;

      case ARIT_HELM: {
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_SMALL);
        m_bOverTopArmor = FALSE;
        m_strDescription.PrintF("Helm - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.5f,0), FLOAT3D(1.5,1.5,0.4f) );
        vItemStretch = FLOAT3D(0.875f * 0.75f, 0.875f * 0.75f, 0.875f * 0.75f);
        m_iSoundComponent = SOUND_HELM;
      } break;
    }

    if (idModelConfig != -1) {
      AddVtxChild(this, this, idModelConfig, "Item", ITEMHOLDER_ATTACHMENT_ITEM);
    }

    StretchItem(vItemStretch);
  };

  void AdjustDifficulty(void)
  {
    BOOL bAllowedByRules = GetSP()->sp_ulAllowedItemTypes & AIF_ARMOR;

    if (!bAllowedByRules && m_penTarget == NULL) {
      Destroy();
    }
  }

procedures:

  ItemCollected(EPass epass) : CItemEntity::ItemCollected
  {
    ASSERT(epass.penOther!=NULL);

    BOOL bItemsStay = GetSP()->sp_ulCoopFlags & CPF_ARMOR_STAYS;

    // if armor stays
    if (bItemsStay && !(m_bPickupOnce || m_bRespawn)) {
      // if already picked by this player
      BOOL bWasPicked = MarkPickedBy(epass.penOther);

      if (bWasPicked) {
        // don't pick again
        return;
      }
    }

    // send health to entity
    EArmorPickup eArmor;
    eArmor.iQuantity = ceil(m_fValue);
    eArmor.bOverTopArmor = m_bOverTopArmor;

    // if health is received
    if (epass.penOther->ReceiveItem(eArmor)) {
      if (_pNetwork->IsPlayerLocal(epass.penOther))
      {
        switch (m_EaitType)
        {
          case ARIT_SHARD:  IFeel_PlayEffect("PU_ArmourShard"); break;
          case ARIT_SMALL:  IFeel_PlayEffect("PU_ArmourSmall"); break;
          case ARIT_MEDIUM: IFeel_PlayEffect("PU_ArmourMedium"); break;
          case ARIT_STRONG: IFeel_PlayEffect("PU_ArmourStrong"); break;
          case ARIT_SUPER:  IFeel_PlayEffect("PU_ArmourSuper"); break;
          case ARIT_HELM:   IFeel_PlayEffect("PU_ArmourHelm"); break;
        }
      }

      // play the pickup sound
      m_soPick.Set3DParameters(50.0f, 1.0f, 1.0f, 1.0f);
      PlaySound(m_soPick, m_iSoundComponent, SOF_3D);
      m_fPickSoundLen = GetSoundLength(m_iSoundComponent);

      if (!bItemsStay || (m_bPickupOnce || m_bRespawn)) {
        jump CItemEntity::ItemReceived();
      }
    }

    return;
  };

  Main()
  {
    Initialize();     // initialize base class
    StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
    SetProperties();  // set properties

    jump CItemEntity::ItemLoop();
  };
};
