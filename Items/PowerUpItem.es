/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

808
%{
  #include "StdH.h"

  #include "Models/Items/ItemHolder/ItemHolder.h"
%}

uses "Entities/Items/Item";
uses "Entities/Players/PlayerPawn";

// powerup type
enum PowerUpItemType
{
  0 PUIT_INVISIB  "Invisibility",
  1 PUIT_INVULNER "Invulnerability",
  2 PUIT_DAMAGE   "SeriousDamage",
  3 PUIT_SPEED    "SeriousSpeed",
  4 PUIT_BOMB     "SeriousBomb",
};

event EGadgetPickup
{
  enum GadgetIndex eGadgetIndex,
};

// event for sending through receive item
event EPowerUpPickup
{
  enum StatusEffectIndex eStatusEffectIndex,
  INDEX iAmplifier,
  FLOAT fDuration,
};

%{
  FLOAT GetRespawnTimeForPowerUp(enum PowerUpItemType eType)
  {
    switch (eType)
    {
      case PUIT_INVISIB: return 40.0F;
      case PUIT_INVULNER: return 60.0F;
      case PUIT_DAMAGE: return 40.0F;
      case PUIT_SPEED: return 40.0F;
      case PUIT_BOMB: return 40.0F;
    }

    return 60.0F;
  }
  
  UBYTE GetAmplifierForPowerUp(StatusEffectIndex eType)
  {
    switch (eType)
    {
      case E_STEF_SPEED: return 3;
    }
    
    return 0;
  }

  StatusEffectIndex GetStatusEffectIndexForItem(PowerUpItemType eType)
  {
    switch (eType)
    {
      case PUIT_INVISIB: return E_STEF_INVISIBILITY;
      case PUIT_INVULNER: return E_STEF_INVULNERABILITY;
      case PUIT_DAMAGE: return E_STEF_DAMAGE;
      case PUIT_SPEED: return E_STEF_SPEED;
    }

    return E_STEF_INVALID;
  }

  const char *GetGadgetNameForIndex(GadgetIndex eGadgetIndex)
  {
    switch (eGadgetIndex)
    {
      case E_GADGET_NUKE: return TRANS("Serious Bomb"); break;
      case E_GADGET_STIMPACK: return TRANS("Stimpack"); break;
      case E_GADGET_MEDKIT: return TRANS("Medkit"); break;
      case E_GADGET_SHIELD_CELL: return TRANS("Shield Cell"); break;
      case E_GADGET_SHIELD_BATTERY: return TRANS("Shield Battery"); break;
    }

    return TRANS("<unknown gadget>");
  }
%}

class CPowerUpItemEntity : CItemEntity
{
name      "PowerUp Item";
thumbnail "Thumbnails\\PowerUpItem.tbn";

properties:

  1 enum PowerUpItemType m_puitType  "Type" 'Y' = PUIT_INVULNER,
//  3 INDEX m_iSoundComponent = 0,

resources:

  0 class   CLASS_BASE      "Classes\\Item.ecl",

 20 modelcfg MODEL_INVISIBILITY_CFG    "Models\\Items\\PowerUps\\Invisibility\\Invisibility_Item.vmc",
 21 modelcfg MODEL_INVULNERABILITY_CFG "Models\\Items\\PowerUps\\Invulnerability\\Invulnerability_Item.vmc",
 22 modelcfg MODEL_DAMAGE_CFG          "Models\\Items\\PowerUps\\SeriousDamage\\SeriousDamage_Item.vmc",
 23 modelcfg MODEL_SPEED_CFG           "Models\\Items\\PowerUps\\SeriousSpeed\\SeriousSpeed_Item.vmc",
 24 modelcfg MODEL_BOMB_CFG            "Models\\Items\\PowerUps\\SeriousBomb\\SeriousBomb_Item.vmc",

// ************** FLARE FOR EFFECT **************
100 texture TEXTURE_FLARE "Models\\Items\\Flares\\Flare.tex",
101 model   MODEL_FLARE   "Models\\Items\\Flares\\Flare.mdl",

// ************** SOUNDS **************
//301 sound   SOUND_INVISIB  "SoundsMP\\Items\\Invisibility.wav",
//302 sound   SOUND_INVULNER "SoundsMP\\Items\\Invulnerability.wav",
//303 sound   SOUND_DAMAGE   "SoundsMP\\Items\\SeriousDamage.wav",
//304 sound   SOUND_SPEED    "SoundsMP\\Items\\SeriousSpeed.wav",
301 sound   SOUND_PICKUP   "SoundsMP\\Items\\PowerUp.wav",
305 sound   SOUND_BOMB     "SoundsMP\\Items\\SeriousBomb.wav",

functions:

  INDEX ModelForItemType(PowerUpItemType eItemType) const
  {
    switch (eItemType)
    {
      case PUIT_INVISIB : return MODEL_INVISIBILITY_CFG;
      case PUIT_INVULNER: return MODEL_INVULNERABILITY_CFG;
      case PUIT_DAMAGE  : return MODEL_DAMAGE_CFG;
      case PUIT_SPEED   : return MODEL_SPEED_CFG;
      case PUIT_BOMB    : return MODEL_BOMB_CFG;
    }

    return -1;
  }

  void Precache(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_puitType);

    if (idModelConfig != -1) {
      PrecacheModelConfig(idModelConfig);
    }

    switch (m_puitType)
    {
      case PUIT_INVISIB :  /*PrecacheSound(SOUND_INVISIB );  break;*/
      case PUIT_INVULNER:  /*PrecacheSound(SOUND_INVULNER);  break; */
      case PUIT_DAMAGE  :  /*PrecacheSound(SOUND_DAMAGE  );  break;*/
      case PUIT_SPEED   :  /*PrecacheSound(SOUND_SPEED   );  break;*/
                           PrecacheSound(SOUND_PICKUP  );  break;
      case PUIT_BOMB    :  PrecacheSound(SOUND_BOMB    );  break;
    }
  }

  /* Fill in entity statistics - for AI purposes only */
  BOOL FillEntityStatistics(EntityStats *pes)
  {
    pes->es_strName = "PowerUp";
    pes->es_ctCount = 1;
    pes->es_ctAmmount = 1;  // !!!!
    pes->es_fValue = 0;     // !!!!
    pes->es_iScore = 0;//m_iScore;

    switch (m_puitType)
    {
      case PUIT_INVISIB :  pes->es_strName += " invisibility";     break;
      case PUIT_INVULNER:  pes->es_strName += " invulnerability";  break;
      case PUIT_DAMAGE  :  pes->es_strName += " serious damage";   break;
      case PUIT_SPEED   :  pes->es_strName += " serious speed";    break;
      case PUIT_BOMB    :  pes->es_strName = "Serious Bomb!";
    }

    return TRUE;
  }

  // render particles
  void RenderParticles(void)
  {
    BOOL bIsModel = GetRenderType() == CEntity::RT_MODEL;
    BOOL bIsNotCoop = GetSP()->sp_gmGameMode > CSessionProperties::GM_COOPERATIVE;

    // no particles when not existing or in DM modes
    if (!bIsModel || bIsNotCoop || !ShowItemParticles()) {
      return;
    }

    switch (m_puitType)
    {
      case PUIT_INVISIB: {
        Particles_Stardust(this, 2.0f*0.75f, 1.00f*0.75f, PT_STAR08, 320);
      } break;

      case PUIT_INVULNER: {
        Particles_Stardust(this, 2.0f*0.75f, 1.00f*0.75f, PT_STAR08, 192);
      } break;

      case PUIT_DAMAGE: {
        Particles_Stardust(this, 1.0f*0.75f, 0.75f*0.75f, PT_STAR08, 128);
      } break;

      case PUIT_SPEED: {
        Particles_Stardust(this, 1.0f*0.75f, 0.75f*0.75f, PT_STAR08, 128);
      } break;

      case PUIT_BOMB: {
        Particles_Atomic(this, 2.0f*0.75f, 2.0f*0.95f, PT_STAR05, 12);
      } break;
    }
  }

  // set health properties depending on health type
  void SetProperties(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_puitType);

    m_fRespawnTime = (m_fCustomRespawnTime > 0) ? m_fCustomRespawnTime : GetRespawnTimeForPowerUp(m_puitType);

    FLOAT3D vItemStretch(1.0F, 1.0F, 1.0F);

    switch (m_puitType)
    {
      case PUIT_INVISIB: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_BIG);
        m_strDescription.PrintF("Invisibility");

        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );  // add flare
        vItemStretch = FLOAT3D(0.75f, 0.75f, 0.75);
      } break;

      case PUIT_INVULNER: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_BIG);
        m_strDescription.PrintF("Invulnerability");
        
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );  // add flare
        vItemStretch = FLOAT3D(0.75f, 0.75f, 0.75);
      } break;

      case PUIT_DAMAGE: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_BIG);
        m_strDescription.PrintF("SeriousDamage");
        
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );  // add flare
        vItemStretch = FLOAT3D(0.75f, 0.75f, 0.75);
      } break;

      case PUIT_SPEED: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_BIG);
        m_strDescription.PrintF("SeriousSpeed");

        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );  // add flare
        vItemStretch = FLOAT3D(0.75f, 0.75f, 0.75);
      } break;

      case PUIT_BOMB: {
        StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_BIG);
        m_strDescription.PrintF("Serious Bomb!");

        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.2f,0), FLOAT3D(1,1,0.3f) );  // add flare
        vItemStretch = FLOAT3D(3.0f, 3.0f, 3.0f);
      } break;
    }

    if (idModelConfig != -1) {
      AddVtxChild(this, this, idModelConfig, "Item", ITEMHOLDER_ATTACHMENT_ITEM);
    }

    StretchItem(vItemStretch);
  };

  void AdjustDifficulty(void)
  {
    BOOL bAllowedByRules = GetSP()->sp_ulAllowedItemTypes & AIF_POWERUPS;
    BOOL bAllowGadgets = GetSP()->sp_ulAllowedItemTypes & AIF_GADGETS;

    if (m_puitType == PUIT_BOMB) {
      if (!bAllowGadgets && m_penTarget == NULL) {
        Destroy();
      }

      return;
    }

    if (!bAllowedByRules && m_penTarget == NULL) {
      Destroy();
    }
  }

procedures:

  ItemCollected(EPass epass) : CItemEntity::ItemCollected
  {
    ASSERT(epass.penOther!=NULL);

    if (!(m_bPickupOnce || m_bRespawn)) {
      // if already picked by this player
      BOOL bWasPicked = MarkPickedBy(epass.penOther);

      if (bWasPicked) {
        return; // don't pick again
      }
    }

    if (m_puitType == PUIT_BOMB) {
      EGadgetPickup eGadgetPickup;
      eGadgetPickup.eGadgetIndex = E_GADGET_NUKE;

      if (epass.penOther->ReceiveItem(eGadgetPickup)) {
        IFeel_PlayEffect("PU_SeriousBomb");

        m_soPick.Set3DParameters(50.0f, 1.0f, 2.0f, 1.0f);
        PlaySound(m_soPick, SOUND_BOMB, SOF_3D);
        m_fPickSoundLen = GetSoundLength(SOUND_BOMB);

        if (m_bPickupOnce || m_bRespawn) {
          jump CItemEntity::ItemReceived();
        }
      }

    } else {
      // send powerup to entity
      EPowerUpPickup ePowerUpPickup;
      ePowerUpPickup.eStatusEffectIndex = GetStatusEffectIndexForItem(m_puitType);
      ePowerUpPickup.iAmplifier = GetAmplifierForPowerUp(ePowerUpPickup.eStatusEffectIndex);

      // if powerup is received
      if (epass.penOther->ReceiveItem(ePowerUpPickup)) {

        if (_pNetwork->IsPlayerLocal(epass.penOther))
        {
          switch (m_puitType)
          {
            case PUIT_INVISIB:  IFeel_PlayEffect("PU_Invulnerability"); break;
            case PUIT_INVULNER: IFeel_PlayEffect("PU_Invulnerability"); break;
            case PUIT_DAMAGE:   IFeel_PlayEffect("PU_Invulnerability"); break;
            case PUIT_SPEED:    IFeel_PlayEffect("PU_FastShoes"); break;
            case PUIT_BOMB:      break;
          }
        }

        // play the pickup sound
        m_soPick.Set3DParameters(50.0f, 1.0f, 2.0f, 1.0f);
        PlaySound(m_soPick, SOUND_PICKUP, SOF_3D);
        m_fPickSoundLen = GetSoundLength(SOUND_PICKUP);

        if ((m_bPickupOnce || m_bRespawn)) {
          jump CItemEntity::ItemReceived();
        }
      }
    }

    return;
  };

  Main()
  {
    Initialize();     // initialize base class
    SetProperties();  // set properties
    jump CItemEntity::ItemLoop();
  };
};
