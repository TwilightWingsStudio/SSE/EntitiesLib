/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

803
%{
  #include "StdH.h"

  #include "Models/Items/ItemHolder/ItemHolder.h"
  #include "Models/Items/Ammo/Shells/Shells.h"
  #include "Models/Items/Ammo/Bullets/Bullets.h"
  #include "Models/Items/Ammo/Rockets/Rockets.h"
  #include "Models/Weapons/RocketLauncher/Projectile/Rocket.h"
  #include "Models/Items/Ammo/Grenades/Grenades.h"
  #include "Models/Items/Ammo/Electricity/Electricity.h"
  #include "Models/Items/Ammo/Cannonball/Cannonball.h"
  #include "Models/Items/Ammo/Cannonball/CannonballQuad.h"
  #include "ModelsMP/Items/Ammo/SniperBullets/SniperBullets.h"
%}

uses "Entities/Items/Item";

// ammo type
enum AmmoItemType
{
  1 AIT_SHELLS          "Shells",
  2 AIT_BULLETS         "Bullets",
  3 AIT_ROCKETS         "Rockets",
  4 AIT_GRENADES        "Grenades",
  5 AIT_ELECTRICITY     "Electricity",
  6 AIT_NUKEBALL        "obsolete",
  7 AIT_IRONBALLS       "IronBalls",
  8 AIT_SERIOUSPACK     "SeriousPack - don't use",
  9 AIT_BACKPACK        "BackPack - don't use",
  10 AIT_NAPALM         "Napalm",
  11 AIT_SNIPERBULLETS  "Sniper bullets"
};

// event for sending through receive item
event EAmmoPickup
{
  enum AmmoIndex eAmmoIndex,
  INDEX iQuantity,                // ammo quantity
};

event EAmmoPackLegacyPickup
{
  BOOL bSerious,
};

%{
  INDEX GetQuantityForAmmoItem(enum AmmoItemType eType)
  {
    switch (eType)
    {
      case AIT_SHELLS: return 10;
      case AIT_BULLETS: return 50;
      case AIT_ROCKETS: return 5;
      case AIT_GRENADES: return 5;
      case AIT_ELECTRICITY: return 50;
      case AIT_IRONBALLS: return 4;
      case AIT_NAPALM: return 100;
      case AIT_SNIPERBULLETS: return 5;
    }

    return 1;
  }

  FLOAT GetRespawnTimeForAmmoItem(enum AmmoItemType eType)
  {
    switch (eType)
    {
      case AIT_SHELLS: return 30.0F;
      case AIT_BULLETS: return 30.0F;
      case AIT_ROCKETS: return 30.0F;
      case AIT_GRENADES: return 30.0F;
      case AIT_ELECTRICITY: return 30.0F;
      case AIT_IRONBALLS: return 30.0F;
      case AIT_NAPALM: return 30.0F;
      case AIT_SNIPERBULLETS: return 30.0F;
    }

    return 60.0F;
  }

  AmmoIndex GetAmmoIndexForItem(AmmoItemType eType)
  {
    switch (eType)
    {
      case AIT_SHELLS: return E_AMMO_SHELLS;
      case AIT_BULLETS: return E_AMMO_HEAVY_ROUNDS;
      case AIT_ROCKETS: return E_AMMO_ROCKETS;
      case AIT_GRENADES: return E_AMMO_GRENADES;
      case AIT_ELECTRICITY: return E_AMMO_ENERGY;
      case AIT_IRONBALLS: return E_AMMO_IRON_BALLS;
      case AIT_NAPALM: return E_AMMO_FUEL;
      case AIT_SNIPERBULLETS: return E_AMMO_SNIPER_ROUNDS;
    }

    return E_AMMO_INVALID;
  }

  const char *GetNameForAmmoIndex(AmmoIndex eAmmoIndex)
  {
    switch (eAmmoIndex)
    {
      case E_AMMO_SHELLS: return TRANS("Shells"); break;
      case E_AMMO_LIGHT_ROUNDS: return TRANS("Light Rounds"); break;
      case E_AMMO_HEAVY_ROUNDS: return TRANS("Heavy Rounds"); break;
      case E_AMMO_SNIPER_ROUNDS: return TRANS("Sniper Rounds"); break;
      case E_AMMO_ENERGY: return TRANS("Energy"); break;
      case E_AMMO_FUEL: return TRANS("Fuel"); break;
      case E_AMMO_ROCKETS: return TRANS("Rockets"); break;
      case E_AMMO_GRENADES: return TRANS("Grenades"); break;
      case E_AMMO_IRON_BALLS: return TRANS("Iron Balls"); break;
    }

    return TRANS("<unknown ammo>");
  }

  INDEX GetManaForAmmoIndex(AmmoIndex eAmmoIndex)
  {
    switch (eAmmoIndex)
    {
      case E_AMMO_SHELLS: return 70;
      case E_AMMO_LIGHT_ROUNDS: return 5;
      case E_AMMO_HEAVY_ROUNDS: return 10;
      case E_AMMO_SNIPER_ROUNDS: return 200;
      case E_AMMO_ENERGY: return 250;
      case E_AMMO_FUEL: return 200;
      case E_AMMO_ROCKETS: return 150;
      case E_AMMO_GRENADES: return 150;
      case E_AMMO_IRON_BALLS: return 700;
    }

    return 0;
  }
%}

class CAmmoItemEntity : CItemEntity {
name      "Ammo Item";
thumbnail "Thumbnails\\AmmoItem.tbn";

properties:

  1 enum AmmoItemType  m_EaitType    "Type" 'Y' = AIT_SHELLS,     // health type

resources:

  0 class   CLASS_BASE        "Classes\\Item.ecl",

 20 modelcfg MODEL_SHELLS_CFG        "Models\\Items\\Ammo\\Shells\\Shells_Item.vmc",
 21 modelcfg MODEL_BULLETS_CFG       "Models\\Items\\Ammo\\Bullets\\Bullets_Item.vmc",
 22 modelcfg MODEL_ROCKETS_CFG       "Models\\Items\\Ammo\\Rockets\\Rockets_Item.vmc",
 23 modelcfg MODEL_GRENADES_CFG      "Models\\Items\\Ammo\\Grenades\\Grenades_Item.vmc",
 24 modelcfg MODEL_ELECTRICITY_CFG   "Models\\Items\\Ammo\\Electricity\\Electricity_Item.vmc",
 25 modelcfg MODEL_CANNONBALLS_CFG   "Models\\Items\\Ammo\\Cannonballs\\Cannonballs_Item.vmc",
 26 modelcfg MODEL_BACKPACK_CFG      "Models\\Items\\PowerUps\\BackPack\\BackPack_Item.vmc",
 27 modelcfg MODEL_SERIOUSPACK_CFG   "Models\\Items\\PowerUps\\SeriousPack\\SeriousPack_Item.vmc",
 28 modelcfg MODEL_NAPALM_CFG        "Models\\Items\\Ammo\\Napalm\\Napalm_Item.vmc",
 29 modelcfg MODEL_SNIPERBULLETS_CFG "Models\\Items\\Ammo\\SniperBullets\\SniperBullets_Item.vmc",

// ************** FLARE FOR EFFECT **************
100 texture TEXTURE_FLARE "Models\\Items\\Flares\\Flare.tex",
101 model   MODEL_FLARE "Models\\Items\\Flares\\Flare.mdl",

// ************** SOUNDS **************
213 sound SOUND_PICK             "Sounds\\Items\\Ammo.wav",
214 sound SOUND_DEFAULT          "Sounds\\Default.wav",

functions:

  INDEX ModelForItemType(AmmoItemType eItemType) const
  {
    switch (eItemType)
    {
      case AIT_SHELLS       : return MODEL_SHELLS_CFG;
      case AIT_BULLETS      : return MODEL_BULLETS_CFG;
      case AIT_ROCKETS      : return MODEL_ROCKETS_CFG;
      case AIT_GRENADES     : return MODEL_GRENADES_CFG;
      case AIT_ELECTRICITY  : return MODEL_ELECTRICITY_CFG;
      case AIT_IRONBALLS    : return MODEL_CANNONBALLS_CFG;
      case AIT_SERIOUSPACK  : return MODEL_SERIOUSPACK_CFG;
      case AIT_BACKPACK     : return MODEL_BACKPACK_CFG;
      case AIT_NAPALM       : return MODEL_NAPALM_CFG;
      case AIT_SNIPERBULLETS: return MODEL_SNIPERBULLETS_CFG;
    }

    return -1;
  }

  void Precache(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_EaitType);

    if (idModelConfig != -1) {
      PrecacheModelConfig(idModelConfig);
    }

    PrecacheSound(SOUND_PICK);
  }

  // render particles
  void RenderParticles(void)
  {
    BOOL bIsModel = GetRenderType() == CEntity::RT_MODEL;
    BOOL bIsNotCoop = GetSP()->sp_gmGameMode > CSessionProperties::GM_COOPERATIVE;

    // no particles when not existing or in DM modes
    if (!bIsModel || bIsNotCoop || !ShowItemParticles()) {
      return;
    }

    switch (m_EaitType)
    {
      case AIT_SHELLS: {
        Particles_Spiral(this, 1.0f*0.75, 1.0f*0.75, PT_STAR04, 4);
      } break;

      case AIT_BULLETS: {
        Particles_Spiral(this, 1.5f*0.75, 1.0f*0.75, PT_STAR04, 6);
      } break;

      case AIT_ROCKETS: {
        Particles_Spiral(this, 1.5f*0.75, 1.25f*0.75, PT_STAR04, 6);
      } break;

      case AIT_GRENADES: {
        Particles_Spiral(this, 2.0f*0.75, 1.25f*0.75, PT_STAR04, 6);
      } break;

      case AIT_ELECTRICITY: {
        Particles_Spiral(this, 1.5f*0.75, 1.125f*0.75, PT_STAR04, 6);
      } break;

      case AIT_NUKEBALL: {
        Particles_Spiral(this, 1.25f*0.75, 1.0f*0.75, PT_STAR04, 4);
      } break;

      case AIT_IRONBALLS: {
        Particles_Spiral(this, 2.0f*0.75, 1.25f*0.75, PT_STAR04, 8);
      } break;

      case AIT_BACKPACK: {
        Particles_Spiral(this, 3.0f*0.5, 2.5f*0.5, PT_STAR04, 10);
      } break;

      case AIT_SERIOUSPACK: {
        Particles_Spiral(this, 3.0f*0.5, 2.5f*0.5, PT_STAR04, 10);
      } break;

      case AIT_NAPALM: {
        Particles_Spiral(this, 3.0f*0.5, 2.5f*0.5, PT_STAR04, 10);
      } break;

      case AIT_SNIPERBULLETS: {
        Particles_Spiral(this, 1.5f*0.75, 1.25f*0.75, PT_STAR04, 6);
      } break;
    }
  }

  /* Fill in entity statistics - for AI purposes only */
  BOOL FillEntityStatistics(EntityStats *pes)
  {
    pes->es_ctCount = 1;
    pes->es_ctAmmount = m_fValue;
    
    pes->es_fValue = m_fValue * GetManaForAmmoIndex(GetAmmoIndexForItem(m_EaitType));
    
    AmmoIndex eAmmoIndex = GetAmmoIndexForItem(m_EaitType); 

    if (eAmmoIndex != E_AMMO_INVALID) {
      pes->es_strName.PrintF("%s: %d", FTokenDefinition::GetIdentifier(eAmmoIndex), (int) m_fValue);
    } 

    switch (m_EaitType)
    {
      case AIT_SERIOUSPACK: {
        pes->es_strName = "SeriousPack";
        pes->es_fValue = m_fValue*100000;
      } break;

      case AIT_BACKPACK: {
        pes->es_strName = "BackPack";
        pes->es_fValue = m_fValue*100000;
      } break;
    }

    pes->es_iScore = 0;//m_iScore;
    return TRUE;
  }

  // set ammo properties depending on ammo type
  void SetProperties(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_EaitType);

    m_fValue = GetQuantityForAmmoItem(m_EaitType);
    m_fRespawnTime = (m_fCustomRespawnTime > 0) ? m_fCustomRespawnTime : GetRespawnTimeForAmmoItem(m_EaitType);

    FLOAT3D vItemStretch(1.0F, 1.0F, 1.0F);
    AmmoIndex eAmmoIndex = GetAmmoIndexForItem(m_EaitType); 
    
    if (eAmmoIndex != E_AMMO_INVALID) {
      m_strDescription.PrintF("%s: %d", FTokenDefinition::GetIdentifier(eAmmoIndex), (int) m_fValue);
    } 

    switch (m_EaitType)
    {
      case AIT_SHELLS: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.25f,0), FLOAT3D(1.5,1.5,0.75f) );
        vItemStretch *= 0.75F;
      } break;

      case AIT_BULLETS: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.6f,0), FLOAT3D(3,3,1.0f) );
        vItemStretch *= 0.75F;
      } break;

      case AIT_ROCKETS: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.6f,0), FLOAT3D(2,2,0.75f) );
        vItemStretch *= 0.75F;
      } break;

      case AIT_GRENADES: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.6f,0), FLOAT3D(4,4,1.0f) );
        vItemStretch *= 0.75F;
      } break;

      case AIT_ELECTRICITY: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.6f,0), FLOAT3D(3,3,0.8f) );
        vItemStretch *= 0.75F;
      } break;

      case AIT_IRONBALLS: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(5,5,1.3f) );
        vItemStretch *= 0.75F;
      } break;

      case AIT_NAPALM: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(3,3,1.0f) );
        vItemStretch *= 1.25F;
      } break;

      case AIT_SERIOUSPACK: {
        m_strDescription.PrintF("SeriousPack: %d", (int) m_fValue);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(2,2,1.3f) );
        vItemStretch *= 0.5F;
      } break;

      case AIT_BACKPACK: {
        m_strDescription.PrintF("BackPack: %d", (int) m_fValue);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(2,2,1.3f) );
        vItemStretch *= 0.5F;
      } break;

      case AIT_SNIPERBULLETS: {
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(3,3,1.0f) );
        vItemStretch *= 1.25F;
      } break;

      default: ASSERTALWAYS("Unknown ammo");
    }

    if (idModelConfig != -1) {
      AddVtxChild(this, this, idModelConfig, "Item", ITEMHOLDER_ATTACHMENT_ITEM);
    }

    StretchItem(vItemStretch);
  };

  void AdjustDifficulty(void)
  {
    m_fValue = ceil(m_fValue * GetSP()->sp_fAmmoPickupQuantity);

    BOOL bAllowedByRules = GetSP()->sp_ulAllowedItemTypes & AIF_AMMO;
    BOOL bInfiniteAmmo = GetSP()->sp_bInfiniteAmmo;

    if ((bInfiniteAmmo || !bAllowedByRules) && m_penTarget == NULL) {
      Destroy();
    }
  }

procedures:

  ItemCollected(EPass epass) : CItemEntity::ItemCollected
  {
    ASSERT(epass.penOther != NULL);

    BOOL bItemsStay = GetSP()->sp_ulCoopFlags & CPF_AMMO_STAYS;

    // if ammo stays
    if (bItemsStay && !(m_bPickupOnce || m_bRespawn)) {
      // if already picked by this player
      BOOL bWasPicked = MarkPickedBy(epass.penOther);

      if (bWasPicked) {
        // don't pick again
        return;
      }
    }

    if (m_EaitType == AIT_BACKPACK || m_EaitType == AIT_SERIOUSPACK) {
      EAmmoPackLegacyPickup eAmmoPickup;
      eAmmoPickup.bSerious = m_EaitType == AIT_SERIOUSPACK ? TRUE : FALSE;

      if (epass.penOther->ReceiveItem(eAmmoPickup)) {
        // play the pickup sound
        if (_pNetwork->IsPlayerLocal(epass.penOther)) {IFeel_PlayEffect("PU_Ammo");}

        m_soPick.Set3DParameters(50.0f, 1.0f, 1.0f, 1.0f);
        PlaySound(m_soPick, SOUND_DEFAULT, SOF_3D);
        m_fPickSoundLen = GetSoundLength(SOUND_PICK);
        CWarningF("^cFF0000^f5Warning!!! Replace old serious pack with new, BackPack entity!^r\n");

        if (!bItemsStay || (m_bPickupOnce || m_bRespawn)) {
          jump CItemEntity::ItemReceived();
        }
      }

      return;
    }

    // send ammo to entity
    EAmmoPickup eAmmoPickup;
    eAmmoPickup.eAmmoIndex = GetAmmoIndexForItem(m_EaitType);
    eAmmoPickup.iQuantity = (INDEX)m_fValue;

    // if health is received
    if (epass.penOther->ReceiveItem(eAmmoPickup)) {
      if (_pNetwork->IsPlayerLocal(epass.penOther)) {IFeel_PlayEffect("PU_Ammo");}

      m_soPick.Set3DParameters(50.0f, 1.0f, 1.0f, 1.0f);
      PlaySound(m_soPick, SOUND_PICK, SOF_3D);
      m_fPickSoundLen = GetSoundLength(SOUND_PICK);

      if (!bItemsStay || (m_bPickupOnce || m_bRespawn)) {
        jump CItemEntity::ItemReceived();
      }
    }

    return;
  };

  Main()
  {
    if (m_EaitType == AIT_NUKEBALL /*|| m_EaitType==AIT_NAPALM*/) {
      m_EaitType = AIT_SHELLS;
    }

    Initialize();     // initialize base class
    StartModelAnim(ITEMHOLDER_ANIM_MEDIUMOSCILATION, AOF_LOOPING|AOF_NORESTART);
    ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
    SetProperties();  // set properties

    jump CItemEntity::ItemLoop();
  };
};
