/* Copyright (c) 2002-2012 Croteam Ltd.
This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

806
%{
  #include "StdH.h"

  #include "Models/Items/ItemHolder/ItemHolder.h"
%}

uses "Entities/Items/Item";

// ammo type
enum AmmoPackType
{
  1 APT_CUSTOM        "Custom pack",
  2 APT_SERIOUS       "Serious pack",
};

// event for sending through receive item
event EAmmoPackPickup
{
  INDEX iShells,
  INDEX iBullets,
  INDEX iRockets,
  INDEX iGrenades,
  INDEX iNapalm,
  INDEX iElectricity,
  INDEX iIronBalls,
  INDEX iSniperBullets,
};

class CAmmoPackEntity : CItemEntity {
name      "Ammo Pack";
thumbnail "Thumbnails\\AmmoPack.tbn";

properties:

  1 enum AmmoPackType  m_aptPackType    "Type" 'Y' = APT_CUSTOM,     // pack type

 10 INDEX m_iShells                "Shells"         'S'   = 0,
 11 INDEX m_iBullets               "Bullets"        'B'   = 0,
 12 INDEX m_iRockets               "Rockets"        'C'   = 0,
 13 INDEX m_iGrenades              "Grenades"       'G'   = 0,
 14 INDEX m_iNapalm                "Napalm"         'P'   = 0,
 15 INDEX m_iElectricity           "Electricity"    'E'   = 0,
 16 INDEX m_iIronBalls             "Iron balls"     'I'   = 0,
 17 INDEX m_iSniperBullets         "Sniper bullets" 'N'   = 0,

resources:

  0 class   CLASS_BASE        "Classes\\Item.ecl",

 20 modelcfg MODEL_BACKPACK_CFG      "Models\\Items\\PowerUps\\BackPack\\BackPack_Item.vmc",
 21 modelcfg MODEL_SERIOUSPACK_CFG   "Models\\Items\\PowerUps\\SeriousPack\\SeriousPack_Item.vmc",

// ************** FLARE FOR EFFECT **************
100 texture TEXTURE_FLARE "Models\\Items\\Flares\\Flare.tex",
101 model   MODEL_FLARE "Models\\Items\\Flares\\Flare.mdl",

// ************** SOUNDS **************
213 sound SOUND_PICK             "Sounds\\Items\\Ammo.wav",

functions:

  INDEX ModelForItemType(AmmoPackType eItemType) const
  {
    switch (eItemType)
    {
      case APT_CUSTOM : return MODEL_BACKPACK_CFG;
      case APT_SERIOUS: return MODEL_SERIOUSPACK_CFG;
    }

    return -1;
  }

  void Precache(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_aptPackType);

    if (idModelConfig != -1) {
      PrecacheModelConfig(idModelConfig);
    }

    PrecacheSound(SOUND_PICK);
  }

  // render particles
  void RenderParticles(void)
  {
    BOOL bIsModel = GetRenderType() == CEntity::RT_MODEL;
    BOOL bIsNotCoop = GetSP()->sp_gmGameMode > CSessionProperties::GM_COOPERATIVE;

    // no particles when not existing or in DM modes
    if (!bIsModel || bIsNotCoop || !ShowItemParticles()) {
      return;
    }

    Particles_Spiral(this, 3.0f*0.5, 2.5f*0.5, PT_STAR04, 10);
  }

  /* Fill in entity statistics - for AI purposes only */
  BOOL FillEntityStatistics(EntityStats *pes)
  {
    pes->es_ctCount = 1;
    pes->es_ctAmmount = 1;
    // compile description
//    pes->es_strName.PrintF("Back pack: %d Shells, %d Bullets, %d Rockets, %d Grenades, %d Electricity, %d Iron balls",
//      m_iShells, m_iBullets, m_iRockets, m_iGrenades, m_iElectricity, m_iIronBalls);
    pes->es_strName.PrintF("Back pack: %d Shells, %d Bullets, %d Rockets, %d Grenades, %d Napalm, %d Electricity, %d Iron balls, %d Sniper bullets",
      m_iShells, m_iBullets, m_iRockets, m_iGrenades, m_iNapalm, m_iElectricity, m_iIronBalls, m_iSniperBullets);

    // calculate value
    pes->es_fValue =
      m_iShells*AV_SHELLS +
      m_iBullets*AV_BULLETS +
      m_iRockets*AV_ROCKETS +
      m_iGrenades*AV_GRENADES +
      m_iNapalm*AV_NAPALM +
      m_iElectricity*AV_ELECTRICITY +
      m_iIronBalls*AV_IRONBALLS +
      m_iSniperBullets*AV_SNIPERBULLETS;

    pes->es_iScore = 0;
    return TRUE;
  }

  // set ammo properties depending on ammo type
  void SetProperties(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_aptPackType);

    FLOAT3D vItemStretch(1.0F, 1.0F, 1.0F);

    switch (m_aptPackType)
    {
      case APT_SERIOUS: {
        m_strDescription = "Serious:";

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(2,2,1.3f) );
        vItemStretch = FLOAT3D(0.5f, 0.5f, 0.5f);
      } break;

      case APT_CUSTOM: {
        m_strDescription = "Custom:";

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(2,2,1.3f) );
        vItemStretch = FLOAT3D(0.5f, 0.5f, 0.5f);
      } break;

      default: ASSERTALWAYS("Uknown ammo");
    }

    if (idModelConfig != -1) {
      AddVtxChild(this, this, idModelConfig, "Item", ITEMHOLDER_ATTACHMENT_ITEM);
    }

    StretchItem(vItemStretch);

    m_fValue = 1.0f;
    m_fRespawnTime = (m_fCustomRespawnTime > 0) ? m_fCustomRespawnTime : 30.0f;

    if (m_iShells != 0) {m_strDescription.PrintF("%s: Shells (%d)", m_strDescription, m_iShells);}
    if (m_iBullets != 0) {m_strDescription.PrintF("%s: Bullets (%d)", m_strDescription, m_iBullets);}
    if (m_iRockets != 0) {m_strDescription.PrintF("%s: Rockets (%d)", m_strDescription, m_iRockets);}
    if (m_iGrenades != 0) {m_strDescription.PrintF("%s: Grenades (%d)", m_strDescription, m_iGrenades);}
    if (m_iNapalm != 0) {m_strDescription.PrintF("%s: Napalm (%d)", m_strDescription, m_iNapalm);}
    if (m_iElectricity != 0) {m_strDescription.PrintF("%s: Electricity (%d)", m_strDescription, m_iElectricity);}
    if (m_iIronBalls != 0) {m_strDescription.PrintF("%s: Iron balls (%d)", m_strDescription, m_iIronBalls);}
    if (m_iSniperBullets != 0) {m_strDescription.PrintF("%s: Sniper bullets (%d)", m_strDescription, m_iSniperBullets);}
  }

  void AdjustDifficulty(void)
  {
    m_fValue = ceil(m_fValue * GetSP()->sp_fAmmoQuantity);

    BOOL bAllowedByRules = GetSP()->sp_ulAllowedItemTypes & AIF_AMMOPACKS;
    BOOL bInfiniteAmmo = GetSP()->sp_bInfiniteAmmo;

    if ((bInfiniteAmmo || !bAllowedByRules) && m_penTarget == NULL) {
      Destroy();
    }
  }

procedures:

  ItemCollected(EPass epass) : CItemEntity::ItemCollected
  {
    ASSERT(epass.penOther!=NULL);

    BOOL bItemsStay = GetSP()->sp_ulCoopFlags & CPF_AMMO_STAYS;

    // if ammo stays
    if (bItemsStay && !(m_bPickupOnce || m_bRespawn)) {
      // if already picked by this player
      BOOL bWasPicked = MarkPickedBy(epass.penOther);

      if (bWasPicked) {
        // don't pick again
        return;
      }
    }

    // send ammo to entity
    EAmmoPackPickup eAmmo;
    eAmmo.iShells = m_iShells;
    eAmmo.iBullets = m_iBullets;
    eAmmo.iRockets = m_iRockets;
    eAmmo.iGrenades = m_iGrenades;
    eAmmo.iNapalm = m_iNapalm;
    eAmmo.iElectricity = m_iElectricity;
    eAmmo.iIronBalls = m_iIronBalls;
    eAmmo.iSniperBullets = m_iSniperBullets;

    // if health is received
    if (epass.penOther->ReceiveItem(eAmmo)) {
      // play the pickup sound
      m_soPick.Set3DParameters(50.0f, 1.0f, 1.0f, 1.0f);
      PlaySound(m_soPick, SOUND_PICK, SOF_3D);
      m_fPickSoundLen = GetSoundLength(SOUND_PICK);

      if (!bItemsStay || (m_bPickupOnce||m_bRespawn)) {
        jump CItemEntity::ItemReceived();
      }
    }

    return;
  };

  Main()
  {
    m_iShells = Clamp(m_iShells, INDEX(0), GetItemDef(E_ITEM_AMMO, E_AMMO_SHELLS)->GetMaxQty());
    m_iBullets = Clamp(m_iBullets, INDEX(0), GetItemDef(E_ITEM_AMMO, E_AMMO_HEAVY_ROUNDS)->GetMaxQty());
    m_iRockets = Clamp(m_iRockets, INDEX(0), GetItemDef(E_ITEM_AMMO, E_AMMO_ROCKETS)->GetMaxQty());
    m_iGrenades = Clamp(m_iGrenades, INDEX(0), GetItemDef(E_ITEM_AMMO, E_AMMO_GRENADES)->GetMaxQty());
    m_iNapalm = Clamp(m_iNapalm, INDEX(0), GetItemDef(E_ITEM_AMMO, E_AMMO_FUEL)->GetMaxQty());
    m_iElectricity = Clamp(m_iElectricity, INDEX(0), GetItemDef(E_ITEM_AMMO, E_AMMO_ENERGY)->GetMaxQty());
    m_iIronBalls = Clamp(m_iIronBalls, INDEX(0), GetItemDef(E_ITEM_AMMO, E_AMMO_IRON_BALLS)->GetMaxQty());
    m_iSniperBullets = Clamp(m_iSniperBullets, INDEX(0), GetItemDef(E_ITEM_AMMO, E_AMMO_SNIPER_ROUNDS)->GetMaxQty());

    Initialize();     // initialize base class
    StartModelAnim(ITEMHOLDER_ANIM_MEDIUMOSCILATION, AOF_LOOPING|AOF_NORESTART);
    ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
    SetProperties();  // set properties

    jump CItemEntity::ItemLoop();
  };
};
