/* Copyright (c) 2021 by ZCaliptium.

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA. */

810
%{
  #include "StdH.h"

  #include "Models/Items/ItemHolder/ItemHolder.h"
%}

uses "Entities/Items/Item";

// health type
enum TreasureItemType
{
  0 TRIT_SMALL       "Small",
  1 TRIT_MEDIUM      "Medium",
  2 TRIT_LARGE       "Large",
};

// event for sending through receive item
event ETreasurePickup
{
  INDEX iQuantity,         // score to receive
};

%{
  INDEX GetQtyForTreasureItem(enum TreasureItemType eType)
  {
    switch (eType)
    {
      case TRIT_SMALL: return 250;
      case TRIT_MEDIUM: return 2500;
      case TRIT_LARGE: return 25000;
    }

    return 1;
  }

  FLOAT GetRespawnTimeForTreasureItem(enum TreasureItemType eType)
  {
    switch (eType)
    {
      case TRIT_SMALL: return 10.0F;
      case TRIT_MEDIUM: return 25.0F;
      case TRIT_LARGE: return 60.0F;
    }

    return 60.0F;
  }
%}

class CTreasureItemEntity : CItemEntity {
name      "Treasure Item";
thumbnail "Thumbnails\\TreasureItem.tbn";

properties:

  1 enum TreasureItemType m_eType     "Type" 'Y' = TRIT_SMALL,    // armor type
  3 INDEX m_iSoundComponent = 0,

resources:

  0 class   CLASS_BASE        "Classes\\Item.ecl",

 20 modelcfg MODEL_COIN_CFG  "Models\\Items\\Treasure\\Coin\\Coin_Item.vmc",
 21 modelcfg MODEL_BAG_CFG   "Models\\Items\\Treasure\\Bag\\Bag_Item.vmc",
 22 modelcfg MODEL_CHEST_CFG "Models\\Items\\Treasure\\Chest\\Chest_Item.vmc",

// ************** FLARE FOR EFFECT **************
100 texture TEXTURE_FLARE  "Models\\Items\\Flares\\Flare.tex",
101 model   MODEL_FLARE    "Models\\Items\\Flares\\Flare.mdl",

// ************** SOUNDS **************
300 sound   SOUND_GENERIC        "Sounds\\Items\\Treasure.wav",

functions:

  INDEX ModelForItemType(TreasureItemType eItemType) const
  {
    switch (eItemType)
    {
      case TRIT_SMALL:  return MODEL_COIN_CFG;
      case TRIT_MEDIUM: return MODEL_BAG_CFG;
      case TRIT_LARGE:  return MODEL_CHEST_CFG;
    }

    return -1;
  }

  void Precache(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_eType);

    if (idModelConfig != -1) {
      PrecacheModelConfig(idModelConfig);
    }

    PrecacheSound(SOUND_GENERIC);
  }

  /* Fill in entity statistics - for AI purposes only */
  BOOL FillEntityStatistics(EntityStats *pes)
  {
    pes->es_strName = "Treasure";
    pes->es_ctCount = 1;
    pes->es_ctAmmount = m_fValue;
    pes->es_fValue = m_fValue * 2;
    pes->es_iScore = 0;//m_iScore;

    switch (m_eType)
    {
      case TRIT_SMALL:  pes->es_strName+=" small";  break;
      case TRIT_MEDIUM: pes->es_strName+=" medium"; break;
      case TRIT_LARGE: pes->es_strName+=" strong"; break;
    }

    return TRUE;
  }

  // render particles
  void RenderParticles(void)
  {
    BOOL bIsModel = GetRenderType() == CEntity::RT_MODEL;
    BOOL bIsNotCoop = GetSP()->sp_gmGameMode > CSessionProperties::GM_COOPERATIVE;

    // no particles when not existing or in DM modes
    if (!bIsModel || bIsNotCoop || !ShowItemParticles()) {
      return;
    }

    switch (m_eType)
    {
      case TRIT_SMALL: {
        Particles_Emanate(this, 1.0f*0.75, 1.0f*0.75, PT_STAR04, 32, 7.0f);
      } break;

      case TRIT_MEDIUM: {
        Particles_Emanate(this, 1.5f*0.75, 1.5f*0.75, PT_STAR04, 64, 7.0f);
      } break;

      case TRIT_LARGE: {
        Particles_Emanate(this, 2.0f*0.75, 1.25f*0.75, PT_STAR04, 96, 7.0f);
      } break;
    }
  }

  // set health properties depending on health type
  void SetProperties(void)
  {
    const INDEX idModelConfig = ModelForItemType(m_eType);

    m_fValue = GetQtyForTreasureItem(m_eType);
    m_fRespawnTime = (m_fCustomRespawnTime > 0) ? m_fCustomRespawnTime : GetRespawnTimeForTreasureItem(m_eType);
    m_iSoundComponent = SOUND_GENERIC;

    FLOAT3D vItemStretch(1.0F, 1.0F, 1.0F);

    switch (m_eType)
    {
      case TRIT_SMALL: {
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_strDescription.PrintF("Small - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.6f,0), FLOAT3D(2,2,0.5f) );
        vItemStretch = FLOAT3D(2.0f, 2.0f, 2.0f);
      } break;

      case TRIT_MEDIUM: {
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_strDescription.PrintF("Medium - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,1.0f,0), FLOAT3D(3,3,0.5f) );
        vItemStretch = FLOAT3D(2.0f, 2.0f, 2.0f);
      } break;

      case TRIT_LARGE: {
        ForceCollisionBoxIndexChange(ITEMHOLDER_COLLISION_BOX_MEDIUM);
        m_strDescription.PrintF("Large - H:%g  T:%g", m_fValue, m_fRespawnTime);

        // set appearance
        AddFlare(MODEL_FLARE, TEXTURE_FLARE, FLOAT3D(0,0.75f,0), FLOAT3D(3.5,3.5,1.0f) );
        vItemStretch = FLOAT3D(2.5f, 2.5f, 2.5f);
      } break;
    }

    if (idModelConfig != -1) {
      AddVtxChild(this, this, idModelConfig, "Item", ITEMHOLDER_ATTACHMENT_ITEM);
    }

    StretchItem(vItemStretch);
  };

  void AdjustDifficulty(void)
  {
    BOOL bAllowedByRules = GetSP()->sp_ulAllowedItemTypes & AIF_ARMOR;

    if (!bAllowedByRules && m_penTarget == NULL) {
      Destroy();
    }
  }

procedures:

  ItemCollected(EPass epass) : CItemEntity::ItemCollected
  {
    ASSERT(epass.penOther!=NULL);

    // if armor stays
    if (!(m_bPickupOnce || m_bRespawn)) {
      // if already picked by this player
      BOOL bWasPicked = MarkPickedBy(epass.penOther);

      if (bWasPicked) {
        // don't pick again
        return;
      }
    }

    // send health to entity
    ETreasurePickup eArmor;
    eArmor.iQuantity = ceil(m_fValue);

    // if health is received
    if (epass.penOther->ReceiveItem(eArmor)) {
      if (_pNetwork->IsPlayerLocal(epass.penOther))
      {
        switch (m_eType)
        {
          case TRIT_SMALL:  IFeel_PlayEffect("PU_ArmourSmall"); break;
          case TRIT_MEDIUM: IFeel_PlayEffect("PU_ArmourMedium"); break;
          case TRIT_LARGE: IFeel_PlayEffect("PU_ArmourStrong"); break;
        }
      }

      // play the pickup sound
      m_soPick.Set3DParameters(50.0f, 1.0f, 1.0f, 1.0f);
      PlaySound(m_soPick, m_iSoundComponent, SOF_3D);
      m_fPickSoundLen = GetSoundLength(m_iSoundComponent);

      if (m_bPickupOnce || m_bRespawn) {
        jump CItemEntity::ItemReceived();
      }
    }

    return;
  };

  Main()
  {
    Initialize();     // initialize base class
    StartModelAnim(ITEMHOLDER_ANIM_SMALLOSCILATION, AOF_LOOPING|AOF_NORESTART);
    SetProperties();  // set properties

    jump CItemEntity::ItemLoop();
  };
};
